var font_name = "";
var dialog_start_hex = "";
var dialog_end_hex = "";
var text_hex_colour = "";
var dialog_close_button_hex = "";
var dialog_close_button_foreground_hex = "";
var display_village_iframe_scrollbar = 0;


var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};


function VillageDetailsView(developmentId,fontfamily_name) {

    var self = this;

    if(fontfamily_name != ""){
        font_name = fontfamily_name;
    }


    $.ajax({

        url: '../admin/getdatainjson/development/' + developmentId,
        type: "GET",
        dataType: "json",
        complete: function(data_response) {
            var rs = data_response.responseJSON;
            var developmentData = rs[0];
            dialog_end_hex =  developmentData.dialog_end_hex;
            dialog_start_hex =  developmentData.dialog_start_hex;
            text_hex_colour = '#' + developmentData.text_hex_colour;
            dialog_close_button_hex = '#' + developmentData.dialog_close_button_background_hex;
            dialog_close_button_foreground_hex = '#' + developmentData.dialog_close_button_foreground_hex;
            display_village_iframe_scrollbar = developmentData.display_village_iframe_scrollbar;

        }

    });


    if((WURFL.form_factor == "Smartphone") || (WURFL.form_factor == "Feature Phone")){

        this.element = $('#village_detail_view_container_panel');
        $(this.element)
        .appendTo(document.body)
        .hide()
        .load('../mpvs/templates/VillageDetailsView.html', function() {
            self.loaded = true;
            if (self.details) {
                self.setLotDetails(self.details)
            }
        });

    }

    else{

        this.element = document.createElement('div');

        $(this.element)
        .appendTo(document.body)
        .hide()
        .load('../mpvs/templates/VillageDetailsView.html', function() {
            self.loaded = true;
            if (self.details) {
                self.setLotDetails(self.details)
            }
        })
        .dialog({
            position: 'center'

        });

        $(this.element)
        .dialog("close");


    }

}

function setPos(varWidth,varHeight){

    var win = $(window);
    var left = (win.width() - varWidth) / 2;


    if(isMobile.any() == "iPad"){

        if($(window).width() <= 768){

            pos = "center";

        }

        else{

            if(window.innerHeight < 650){ //the case that bookmark bar is on in horizontal mode

                pos = [left,69];  

            }

            else{

                pos = [left,82];   

            } 
        }


    }

    else{

        pos = "center";

    }

    return pos;

}


VillageDetailsView.prototype.setVillageDetails = function(display_village_iframe_url) {
      
    if(display_village_iframe_url != ""){

        var pos = setPos(700,500);
        
        var display_iframe = '<iframe src="'+display_village_iframe_url+'" width="100%" height="100%" frameborder="no"></iframe>'; 
        
        var dialog = $("<div class='iframe_site_div'></div>").append($(display_iframe)).dialog({
            width: 700,
            height: 500,
            position: pos,
            open: function(event, ui) {

                $('.stage_container').hide();
                $('#phonenum_container').hide();

                $('.iframe_site_div').css('padding','0');
                
                if(parseInt(display_village_iframe_scrollbar) == 0){

                    $('.ui-dialog .ui-dialog-content').css('overflow','hidden');

                }
                
                 if(WURFL.form_factor == "Tablet"){
                    $(".iframe_site_div").addClass('supportScrollingOnTablet'); 
                }

                $('.ui-dialog .ui-dialog-titlebar').css('background', 'linear-gradient(to bottom, #' + dialog_start_hex + ' 0%, #' + dialog_end_hex + ' 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

                $('.ui-dialog .ui-dialog-titlebar-close').blur();

                $('.ui-dialog .ui-dialog-titlebar-close').css('background-color',dialog_close_button_hex);

                if((bowser.msie && bowser.version < 10) || (bowser.firefox && bowser.version < 16)){

                    $('.ui-dialog .ui-dialog-titlebar').css('background', '#'+dialog_start_hex);

                }

                else{

                    $('.ui-dialog .ui-dialog-titlebar').css('filter', 'progid:DXImageTransform.Microsoft.gradient(startColorstr = #' + dialog_start_hex + ',endColorstr = #' + dialog_end_hex + ')');

                }

            },

            close: function(event, ui){

                $('.stage_container').show();
                $('#phonenum_container').show();

            }

        });

        dialog.show();

        return;

    } 


}
  
module.exports = VillageDetailsView;