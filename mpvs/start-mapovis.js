$(document).ready(function($){

    var ip;

    function checkMode() {
        var o = window.orientation;

        if (o != 90 && o != -90) {
            // here goes code for portrait...


        } else {
            // here goes for landscape...
            $('#map-canvas').css({'position':'absolute','top':'40px','bottom':'44px','width':'100%'});

        }
    }

    // check orientation on page load
    checkMode();

    function get(name){
        if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
            return decodeURIComponent(name[1]);
    }

    var developmentId = get( "developmentId" );
    $.cookie('developmentid',developmentId);
    
    // in case of mobile devices, site redirects into mobile version
            
    if((WURFL.form_factor == "Smartphone") || (WURFL.form_factor == "Feature Phone")){
      window.open('../mapovismobile/index.html?developmentId='+developmentId, '_self');
      return;
    }
    /*
        this function loads js library dynamically.
    */
    loadJS = function(src) {
        var jsLink = $("<script type='text/javascript' src='"+src+"'>");
        jsLink.appendTo($('head'));
    }; 

    /**
    * function to load a given css file 
    */ 

    loadCSS = function(href) {
        var cssLink = $("<link rel='stylesheet' type='text/css' href='"+href+"'>");
        cssLink.appendTo($('head')); 
    };

    $.ajax({
        url: '../admin/getdatainjson/development/'+developmentId,
        type: "GET",
        dataType: "json",  
        complete: function (data_response) {
            var rs = data_response.responseJSON;
            var developmentEntry = rs[0];
            developmentName = developmentEntry.development_name;
            
            loading_gradient_top_hex = '#' + developmentEntry.loading_gradient_top_hex;
            loading_gradient_middle_hex = '#' + developmentEntry.loading_gradient_middle_hex;
            loading_gradient_bottom_hex = '#' + developmentEntry.loading_gradient_bottom_hex;
            
            $('.loadingscreen').css('background','linear-gradient(to bottom, ' + loading_gradient_top_hex +' 0%, ' + loading_gradient_middle_hex + ' 47%, '+ loading_gradient_bottom_hex + ' 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

            
            $('.loadingscreen').show();

            var custom_css_override_url = developmentEntry.custom_css_override_url;

            if(custom_css_override_url != ""){

                loadCSS(custom_css_override_url);

            }
            
            if(WURFL.form_factor == "Tablet"){
                $('.slider_box').hide();
                $('#price_range_dropdown_tablet').show();
                $('#lot_width_checkbox_group_tablet').show();
                $('#lot_size_checkbox_group_tablet').show();
            }

            var coordinate = developmentEntry.entryexit_latitude +","+developmentEntry.entryexit_longitude;

            $('#development_coordinate').val(coordinate);
            $('#origin').val(developmentName);
            $('.development_name').text(developmentName);
            
            
            if(developmentEntry.custom_font_markup != ""){

                var src = developmentEntry.custom_font_markup;

                if(src.indexOf('google') >= 0){

                    loadCSS(src);
                    $('*').not('.fa, .pocp_button_right > i').css("font-family", developmentEntry.custom_google_font_name+", serif");

                }

                else{

                    WebFontConfig = {
                        typekit: { id: src }
                    };

                    var wf = document.createElement('script');
                    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                    '://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js';
                    wf.type = 'text/javascript';
                    wf.async = 'true';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(wf, s);

                    $('*').not('.fa, .pocp_button_right > i').css("font-family", developmentEntry.custom_typekit_font_name+", serif");


                }


            }
            
          
  
        }

    });


});