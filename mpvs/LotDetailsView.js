var panelSlided = false;     // if sliding panel is open, this is true. default is false
var stageplanpdf_name = " ";
var visitor_ip = "";
var font_name = "";
var start_hex_colour = "";
var end_hex_colour = "";
var text_hex_colour = "";
var form_iframe_hiddenfield_lot_number;
var form_iframe_hiddenfield_house_name;
var form_iframe_hiddenfield_builder;
var show_house_matches = 1;
var houseMatchesCount = 0;
var dialog_start_hex = "";
var dialog_end_hex = "";
var dialog_close_button_hex = "";
var developmentName;

var $this;
var spinner = null;
var lot_number;

var facebook_sharing = 0;

var facebook_share_houseland_package_name;
var facebook_share_houseland_package_caption;
var facebook_share_houseland_package_link;
var facebook_share_houseland_package_picture;
var facebook_share_houseland_package_description;

var facebook_share_lot_name;
var facebook_share_lot_caption;
var facebook_share_lot_link;
var facebook_share_lot_picture;
var facebook_share_lot_description;

var house_land_package_downloadpdf_button;
var house_land_package_downloadpdf_button_highlight;
var house_land_package_enquirenow_button;
var house_land_package_enquirenow_button_highlight;
var developmentId;

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

var opts = {
  lines: 13, // The number of lines to draw
  length: 20, // The length of each line
  width: 10, // The line thickness
  radius: 30, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#000', // #rgb or #rrggbb or array of colors
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: '50%', // Top position relative to parent
  left: '50%' // Left position relative to parent
};

function replaceString(str,old,newString){
    var updatedStr = str.replace(old, newString);
    return updatedStr;
}

function LotDetailsView(developmentId,show_pricing,stageplanpdf,LotDetailsTemplate,visitor_ip,fontfamily_name) {
    this.element = document.createElement('div');
    var self = this;

    stageplanpdf_name = stageplanpdf;

    if(fontfamily_name != ""){

        font_name = fontfamily_name;

    }

    $.ajax({
        url: '../admin/getdatainjson/development/' + developmentId,
        type: "GET",
        dataType: "json",
        complete: function(data_response) {
        //    var developmentData = JSON.parse(data_response.responseText);
            var rs = data_response.responseJSON;
            var developmentData = rs[0];
            end_hex_colour = '#' + developmentData.end_hex_colour;
            start_hex_colour = '#' + developmentData.start_hex_colour;  // dialog gradient start hex color
            text_hex_colour = '#' + developmentData.text_hex_colour;
            dialog_start_hex = '#' + developmentData.dialog_start_hex; // dialog title bar background gradient start color hex value
            dialog_end_hex = '#' + developmentData.dialog_end_hex;     // dialog title bar background gradient end color hex value
            form_iframe_hiddenfield_lot_number = developmentData.form_iframe_hiddenfield_lot_number;
            show_house_matches = parseInt(developmentData.show_house_matches);
            
            developmentName = developmentData.development_name;

            form_iframe_hiddenfield_house_name = developmentData.form_iframe_hiddenfield_house_name;
            form_iframe_hiddenfield_builder = developmentData.form_iframe_hiddenfield_builder;

            house_land_package_enquirenow_button = '#'+developmentData.house_land_package_enquirenow_button;
            house_land_package_downloadpdf_button = '#'+developmentData.house_land_package_downloadpdf_button;
            house_land_package_downloadpdf_button_highlight = '#'+developmentData.house_land_package_downloadpdf_button_highlight;
            house_land_package_enquirenow_button_highlight = '#'+developmentData.house_land_package_enquirenow_button_highlight;

            dialog_close_button_hex = '#'+developmentData.dialog_close_button_background_hex;  // dialog close button background gradient hex color.
            facebook_sharing = developmentData.facebook_sharing;
            
            if(facebook_sharing == 0){
                
                $('.col06').hide();
         
            }
            
            else{

                $('.col06').show();

            }
            
            /*
              facebook share
            */
            
            facebook_share_houseland_package_name = developmentData.facebook_share_houseland_package_name;
            
            facebook_share_houseland_package_name = replaceString(facebook_share_houseland_package_name,'*developmentName*',developmentName);
            facebook_share_houseland_package_caption = developmentData.facebook_share_houseland_package_caption;

            facebook_share_houseland_package_link = developmentData.facebook_share_houseland_package_link;
            
            facebook_share_houseland_package_picture = developmentData.facebook_share_houseland_package_picture;
            facebook_share_houseland_package_description = developmentData.facebook_share_houseland_package_description;
            
            facebook_share_lot_name = developmentData.facebook_share_lot__name;

            
            facebook_share_lot_caption = developmentData.facebook_share_lot_caption;
            
            facebook_share_lot_link = developmentData.facebook_share_lot_link;
            facebook_share_lot_picture = developmentData.facebook_share_lot_picture; 
            facebook_share_lot_description = developmentData.facebook_share_lot_description;
            
            /*
              facebook share end
            */
        }

    });


    visitor_ip = visitor_ip;
    developmentId = developmentId;
    
    if(show_pricing == "1"){
        
        if((WURFL.form_factor == "Smartphone") || (WURFL.form_factor == "Feature Phone")){
             
            this.element = $('#lot_detail_view_container_panel');      // on mobile devices, specific div container used. this container is defined on mobile index.html
             $(this.element)
            .hide()
            .load('../mpvs/templates/'+LotDetailsTemplate, function() {
                self.loaded = true;
                if (self.details) {
                    self.setLotDetails(self.details,visitor_ip)
                }
                
                $('#enquire').click(function(e){         // when user clicks ENQUIRE button, jumps directly to enquire form.

                    $('#lot_detail_view_container_panel').animate({
                        scrollTop:  $('#anchorpoint').offset().top  
                    });
                    
                });
                
                 $('#share_button').click(function(e){   
                        e.preventDefault();
                        
                        var milliseconds = new Date().getTime();  // event timestamp
                        $.post( 
                            "../mapovis/save_statistics_info.php",
                            { browserinfo: bowser.name+" "+bowser.version, 
                              timestamp: milliseconds, 
                              ip_address: visitor_ip, 
                              developmentId: developmentId,
                              type: "land", 
                              flag: "statistics_fb_share"  },
                              
                              function(data) {
                                  
                              }
                         );   
                        
                        facebook_share_lot_name = replaceString(facebook_share_lot_name,'*developmentName*',developmentName);
                        facebook_share_lot_caption = replaceString(facebook_share_lot_caption,'*lot_number*',lot_number);

                        facebook_share_lot_description = replaceString(facebook_share_lot_description,'*lot_number*',lot_number);
                        facebook_share_lot_description = replaceString(facebook_share_lot_description,'*developmentName*',developmentName);
                       
                        FB.ui(
                        {
                            method: 'feed',
                            name: facebook_share_lot_name,
                            link: facebook_share_lot_link,
                            picture: facebook_share_lot_picture,
                            caption: facebook_share_lot_caption,
                            description: facebook_share_lot_description,
                            message: ''
                        });
                        
                });
                
            });
        
        }
        
        else{

            $(this.element)
            .hide()
            .load('../mpvs/templates/'+LotDetailsTemplate, function() {     
                self.loaded = true;
                if (self.details) {
                    self.setLotDetails(self.details,visitor_ip)
                }
            })
            .dialog({
                position: 'center'

            });

            $(this.element)
            .dialog("close");
            
        }

    }

    else{

        if((WURFL.form_factor == "Smartphone") || (WURFL.form_factor == "Feature Phone")){
             this.element = $('#lot_detail_view_container_panel');   // on mobile devices, specific div container used. this container is defined on mobile index.html  
             $(this.element)
             .hide()
             .load('../mpvs/templates/'+LotDetailsTemplate, function() {
                self.loaded = true;
                if (self.details) {
                    self.setLotDetails(self.details,visitor_ip)
                }
                
                $('#enquire').click(function(e){         // when user clicks ENQUIRE button, jumps directly to enquire form. 
                    $('#lot_detail_view_container_panel').animate({
                        scrollTop: $('#anchorpoint').offset().top  
                    });
                    
                });
                
                $('#share_button').click(function(e){   
                        e.preventDefault();
                        
                        var milliseconds = new Date().getTime();  // event timestamp
                        $.post( 
                            "../mapovis/save_statistics_info.php",
                            { browserinfo: bowser.name+" "+bowser.version, 
                              timestamp: milliseconds, 
                              ip_address: visitor_ip, 
                              developmentId: developmentId,
                              type: "land", 
                              flag: "statistics_fb_share"  },
                              
                              function(data) {
                                  
                              }
                         );   
                        
                        facebook_share_lot_name = replaceString(facebook_share_lot_name,'*developmentName*',developmentName);
                        facebook_share_lot_caption = replaceString(facebook_share_lot_caption,'*lot_number*',lot_number);

                        facebook_share_lot_description = replaceString(facebook_share_lot_description,'*lot_number*',lot_number);
                        facebook_share_lot_description = replaceString(facebook_share_lot_description,'*developmentName*',developmentName);
                       
                        FB.ui(
                        {
                            method: 'feed',
                            name: facebook_share_lot_name,
                            link: facebook_share_lot_link,
                            picture: facebook_share_lot_picture,
                            caption: facebook_share_lot_caption,
                            description: facebook_share_lot_description,
                            message: ''
                        });
                        
                });
                
            });
        
        }
        
        else{
            
             $(this.element)
            .hide()
            .load('../mpvs/templates/'+LotDetailsTemplate, function() {     
                self.loaded = true;
                if (self.details) {
                    self.setLotDetails(self.details,visitor_ip)
                }
            })
            .dialog({
                position: 'center'

            });

            $(this.element)
            .dialog("close");
             
        }

    }

}

function ReplaceNumberWithCommas(yourNumber) {
    //Seperates the components of the number
    var n= yourNumber.toString().split(".");
    //Comma-fies the first part
    n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //Combines the two sections
    return n.join(".");
}

LotDetailsView.prototype.setPanelSlided = function(value){

    panelSlided = value;

}

LotDetailsView.prototype.setLotDetails = function(lotDetails,form_url,form_height,visitor_ip) {

    var target = document.getElementsByTagName('body')[0];
    spinner = new Spinner(opts).spin(target);

    lot_number = lotDetails.lot_number;
    developmentId = lotDetails.development_id;
    
    if (this.loaded) {
        $(this.element)
        .find("#stageNumber")
        .html(lotDetails.stageNumber);
        $(this.element)
        .find("#lot_number")
        .html(lotDetails.lot_number);
        
        if(lotDetails.status == 'Available'){
           $(this.element)
           .find("#lot_status")
           .html("<span style='color: #3d8e57; font-weight:bold;'>"+lotDetails.status+"</span>"); 
        }
        
        if(lotDetails.status == 'Deposited'){
           $(this.element)
           .find("#lot_status")
           .html("<span style='color: #f7941d; font-weight:bold;'>"+lotDetails.status+"</span>"); 
        }
       
        $(this.element)
        .find("#price_range_min")
        .text(ReplaceNumberWithCommas(lotDetails.price_range_min));

        $(this.element)
        .find("#price_range_max")
        .text(ReplaceNumberWithCommas(lotDetails.price_range_max));  

        $(this.element)
        .find(".price")
        .text(ReplaceNumberWithCommas(lotDetails.price_range_max)); 
        
        $(this.element)
        .find("#lot_depth")
        .text(Number(lotDetails.lot_depth).toFixed(1)); 
        

        $(this.element)
        .find("#lot_square_meters")
        .html(lotDetails.lot_square_meters);

        if(parseInt(lotDetails.lot_corner) == 1 ){
           $(this.element).find('#cornermark').show();  
           $(this.element)
           .find("#lot_width")
           .html(lotDetails.lot_width+"m <span style='font-size:15px; font-weight:normal;'>Frontage</span>");

        }
        else if(parseInt(lotDetails.lot_irregular) == 1){
            $(this.element).find('#cornermark').show(); 
            $(this.element)
           .find("#lot_width")
           .html(lotDetails.lot_width+"m <span style='font-size:15px; font-weight:normal;'>Frontage</span>"); 
           
           $(this.element).find('#cornermark').text("IRREGULAR LOT"); 
        }
        else{
           $(this.element)
           .find("#lot_width")
           .html(lotDetails.lot_width+"m <span style='font-size:15px; font-weight:normal;'>Frontage</span>"); 
            $(this.element).find('#cornermark').hide();
        }
        
        $(this.element)
        .find('#enquiryform').attr({"src":form_url, "height":form_height});

    } else {
        this.details = lotDetails;
    }

    this.loadHousesMatches(lotDetails,visitor_ip); 
    
    var varWidth  = 900;
    var varHeight = 500;

    if(isMobile.any() == "iPad"){


        if($(window).width() <= 768){
            varWidth = 700;
            varHeight = $(window).height()*0.87;
        }

        else{
            varWidth = 830;
            percent = 0.80;
            varHeight = 560;
        }

    }

    if(isMobile.any() == "Android"){


        if($(window).width() <= 980){
            varWidth = 700;
            varHeight = $(window).height()*0.87;
        }

        else{
            varWidth = 830;
            percent = 0.80;
            varHeight = 560;
        }


    }

    var pos = null;
    var top = 0;
    var left = 0;
    var win = $( window );

    if((panelSlided == true) && (isMobile.any() != "iPhone")){

        top = (win.height() - varHeight) / 2;
        left = (win.width() - 380 - varWidth) / 2;


        if($(window).width() <= 1280){

            varWidth = ($(window).width() - 380 ) * 0.9;
            left = ($(window).width() - 380 - varWidth) / 2;

        }

        pos = [left,top];

        if(isMobile.any() == "iPad"){
            if(window.innerHeight < 650){
                pos = [left,50]; 
                varHeight = 540; 
            }
            else{
                pos = [left,55];   
            } 

        }

    }

    else{

        left = (win.width() - varWidth) / 2;
        top = (win.height() - varHeight) / 2;
        pos = [left,top];

        if(isMobile.any() == "iPad"){
            if(window.innerHeight < 650){
                pos = [left,50];  
                varHeight = 540
            }
            else{
                pos = [left,55];   
            } 
        }

    }
    
    if((WURFL.form_factor == "Smartphone") || (WURFL.form_factor == "Feature Phone")){
        
        $(this.element).show();
        
        var f=$('#enquiryform');
        
        f.load(function(){   
            f.contents().find('input[name='+form_iframe_hiddenfield_lot_number+']').val(lotDetails.lot_number);
            f.contents().find('input[name='+form_iframe_hiddenfield_lot_number+']').attr('value',lotDetails.lot_number);
        });
         
            
        return;  
    }


    var oDetail = $(this.element)
    .show()
    .dialog({
        width: varWidth,
        height: varHeight,
        resizable: false,
        autoOpen:true,
        modal:false,
        position:pos,
        dialogClass: 'lot_dlg',
        open: function(event, ui) {

            $(this).scrollTop(0);

            $(this).css('overflow', 'auto'); //this line does the actual scrollbar display
            $('.stage_container').hide();
            $('#phonenum_container').hide();
            /*
              dialog title bar background color linear gradient effect
            */
            $('.ui-dialog .ui-dialog-titlebar').css('background', 'linear-gradient(to bottom, ' + dialog_start_hex + ' 0%, ' + dialog_end_hex + ' 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');                
            $('.ui-dialog .ui-dialog-titlebar-close').css('background-color',dialog_close_button_hex);

            $this = $(this);

            if(font_name != ""){
                $('*').not('.fa, .pocp_button_right > i').css("font-family", font_name+", serif");
            }

            $('.ui-dialog .ui-dialog-titlebar-close').blur();


            $this.find('#enquire').css('color','black');

            $this.find("#enquire" ).hover(
            function() {
                $(this).css('color',start_hex_colour);
            }, function() {
                $( this ).css('color','black');
            }
            );


            $this.find('#enquire').click(function(e){


                if(WURFL.form_factor == "Tablet"){

                    if((houseMatchesCount ==  0) || (show_house_matches == 0)){
                        $this.find('.iframe_con').show();
                        return;
                    }

                    if(show_house_matches == 1){

                        if(houseMatchesCount == 0){
                            $this.find('.iframe_con').show();
                            return;
                        }
                    }

                    $this.find('.row').toggle();
                    $this.find('#housematchoptionsheader').toggle();
                    $this.find('.iframe_con').toggle();
                    $this.scrollTop(0);

                }

                else{

                   // on desktop device and mobile device, jumps directly to enquire form on lot dialog
                   $this.animate({scrollTop: $this.find('#anchorpoint').offset().top}, 'slow');

                }

            });
            
            
            $this.find('#share_button').click(function(e){
                e.preventDefault();
                
                var milliseconds = new Date().getTime();  // event timestamp
                $.post( 
                    "../mapovis/save_statistics_info.php",
                    { browserinfo: bowser.name+" "+bowser.version, 
                      timestamp: milliseconds, 
                      ip_address: visitor_ip, 
                      developmentId: developmentId,
                      type: "land", 
                      flag: "statistics_fb_share"  },
                      
                      function(data) {
                          
                      }
                 );
                
                
                facebook_share_lot_name = replaceString(facebook_share_lot_name,'*developmentName*',developmentName);
                facebook_share_lot_caption = replaceString(facebook_share_lot_caption,'*lot_number*',lot_number);

                facebook_share_lot_description = replaceString(facebook_share_lot_description,'*lot_number*',lot_number);
                facebook_share_lot_description = replaceString(facebook_share_lot_description,'*developmentName*',developmentName);
                
                FB.ui(
                {
                    method: 'feed',
                    name: facebook_share_lot_name,
                    link: facebook_share_lot_link,
                    picture: facebook_share_lot_picture,
                    caption: facebook_share_lot_caption,
                    description: facebook_share_lot_description,
                    message: ''
                });
                
            });

            /*
            setting value to hidden field of iframe form
            */
            var f=$this.find('#enquiryform');
            f.load(function(){ 
                f.contents().find('input[name='+form_iframe_hiddenfield_lot_number+']').val(lotDetails.lot_number);  
                f.contents().find('input[name='+form_iframe_hiddenfield_lot_number+']').attr('value',lotDetails.lot_number);    
            });
            
        },

        close: function(event, ui){

            $('.stage_container').show();
            
            if(parseInt($('.pocp_right').css('right')) == 0){        // if sliding panel is open, phone number is hidden
               $('#phonenum_container').hide();  
            }
            else{
              $('#phonenum_container').show();   // if sliding panel closes, phone number shows.
            }
            
            $('.lot_dlg').remove();

            $this.find('.row').show();
            $this.find('.iframe_con').show();
            $this.find('#housematchoptionsheader').show();

        }
    });


}


LotDetailsView.prototype.loadHousesMatches = function(lotDetails,visitor_ip) {

    var self = this;

    var lotId = lotDetails.lot_id;
    var developmentId = lotDetails.development_id;
    // Whenever lot clicked, its lot id and other informations will be posted to save_statistics_info.php to save in database table.

    var milliseconds = new Date().getTime();  // event timestamp
    
    $.post( 
    "../mapovis/save_statistics_info.php",
    { browserinfo: bowser.name+" "+bowser.version, lotId: lotId, timestamp: milliseconds, ip_address: visitor_ip, developmentId: developmentId, flag: "statistics_lotViews"  },
    function(data) {

    }
    );


    $.ajax({
        url: '../admin/getdatainjson/housematches/' + developmentId +"/"+ lotId,
        type: "GET",
        dataType: "json",  
        complete: function (data_response) {

            var houseMatches = JSON.parse(data_response.responseText);
            houseMatchesCount = houseMatches.length;
            if(houseMatchesCount == 0){
                
                if((WURFL.form_factor == "Smartphone") || (WURFL.form_factor == "Feature Phone")){
                    
                      $('#houses').hide();
                      $('#housematchoptionsheader').hide();
                      $('.iframe_con').show();
                
                }
                
                else{
                
                     $this.find('#houses').hide();
                     $this.find('#housematchoptionsheader').hide();
                     $this.find('.iframe_con').show(); 
                     
                }
                   
            }

            else{

                if(show_house_matches == 0){
                    
                  if(isMobile.any() == "iPhone"){
                      
                    $('#houses').hide();
                    $('#housematchoptionsheader').hide(); 
                    $('.iframe_con').show();
                    
                  }
                  
                  else{
                      
                     $this.find('#houses').hide();
                     $this.find('#housematchoptionsheader').hide(); 
                     $this.find('.iframe_con').show();
                         
                  }
                    
                }
                else{
                    
                    if((WURFL.form_factor == "Smartphone") || (WURFL.form_factor == "Feature Phone")){
                        
                         $('#houses').show(); 
                         $('#housematchoptionsheader').show();
                    
                    }
                    
                    else{
                        
                         $this.find('#houses').show(); 
                         $this.find('#housematchoptionsheader').show();
                        
                    }
                
                   
                    
                    if(isMobile.any() == "iPad"){
                        $this.find('.iframe_con').hide();
                    }
                    
                    else if((isMobile.any() == "Android") && (WURFL.form_factor == "Tablet")){
                        $this.find('.iframe_con').hide();
                    }
                    
                    else if((WURFL.form_factor == "Smartphone") || (WURFL.form_factor == "Feature Phone")){
                        $('.iframe_con').show();
                    }
                    
                    else{
                        $this.find('.iframe_con').show();
                    }

                }

            }

            self.setHouseMatches(houseMatches,developmentId,visitor_ip);

        }

    });


}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

LotDetailsView.prototype.setHouseMatches = function(houseMatches,developmentId,visitor_ip) {

    var houseMatchesDiv = $(this.element).find("#houses");

    houseMatchesDiv.html("");  

    if( stageplanpdf_name != ""){
        $('.stageplanpdf_div').remove(); 
    }

    for (var i = 0; i < houseMatches.length; i++) {

        var houseMatch = houseMatches[i];
        
        var househtml = '<div class="col-md-12 col-lg-12 house_details">'+

        '<div class="col-md-4 col-lg-4"><ul class="rslides" id="slider'+i+'"></ul><img src="../mpvs/images/icon_share_facebook.png" class="fb_share_house_package" house_name='+houseMatch.house_name+' builder_name='+houseMatch.builder_name+' house_image='+houseMatch.house_images[0]+' id="share_button_'+i+'"></div>'+

        '<div class="col-md-8 col-lg-8">'+
        '<div  class="col-md-6 col-lg-6"><div class="housenameandprice">'+
        '<span class="housename" id="house_name">'+houseMatch.house_name+'</span>'+
        '</div>'+
        '<div class="buildername" id="builder_name">by '+houseMatch.builder_name+'</div>'+
        '<div class="propertyfeatures">';
        
        if(parseInt(houseMatch.house_size) != 0){
            househtml += '<span class="squaremeters"><span class="glyphicon glyphicon-fullscreen" style="margin-right:5px;font-size:15px;"></span>'+houseMatch.house_size+'<sup>sq</sup>'+
             '</span>' 
        }
        
        
        househtml += '<dl style="width:120%;"><dt class="icon bed" title="Bed"></dt><dd>'+houseMatch.house_bedrooms+'</dd><dt class="icon bath" title="Bath"></dt><dd>'+houseMatch.house_bathrooms+'</dd>'+
        '<dt class="icon car" title="Car"></dt><dd>'+houseMatch.house_garages+'</dd></dl>'+
        '</div></div>'+ 
        '<div  class="col-md-6 col-lg-6"><div class="meta">'+
        '<dl><dt>House &amp; Land From</dt><dd>$'+numberWithCommas(houseMatch.house_lot_price)+'</dd></dl>'+
        '</div>' + 
        '<a href="../mpvs/templates/lot_house_pdfs/'+houseMatch.file_name+'"  class="btn btn-primary download" target="_blank"><span>DOWNLOAD PDF</span><span class="fa fa-cloud-download" style="margin-left:5px; font-size:16px;vertical-align:middle;"></span></a>' +
        '<a href="#"  class="btn btn-primary enquire_link" id="enquire_link'+i+'" house_name="'+houseMatch.house_name+'" house_builder="'+houseMatch.builder_name+'"><span>ENQUIRE NOW</span></a></div>' +
        '</div>'+
        '</div>';
        
        
        $(househtml).appendTo(houseMatchesDiv);  
        
 
        if(facebook_sharing == 0){
            
            $('.fb_share_house_package').hide();
            
        }
        
        else{
            
            $('.fb_share_house_package').show(); 
            
        }
        
         /*
           on mobile devices, form submit using iframe
         */
        if((WURFL.form_factor == "Smartphone") || (WURFL.form_factor == "Feature Phone")){
            
             $('#enquire_link'+i).click(function(e){

                var house_name = $(this).attr('house_name');
                var house_builder = $(this).attr('house_builder');


                var ff=$('#enquiryform');
                ff.contents().find('input[name='+form_iframe_hiddenfield_house_name+']').val(house_name);
                ff.contents().find('input[name='+form_iframe_hiddenfield_builder+']').val(house_builder);
                
                ff.contents().find('input[name='+form_iframe_hiddenfield_house_name+']').attr('value',house_name);
                ff.contents().find('input[name='+form_iframe_hiddenfield_builder+']').attr('value',house_builder);

                $('#lot_detail_view_container_panel').animate({
                  scrollTop: 10000 
                });

            });
            
            
            $('#share_button_'+i).click(function(e){
                e.preventDefault();
                
                var milliseconds = new Date().getTime();  // event timestamp

                 $.post( 
                    "../mapovis/save_statistics_info.php",
                    { browserinfo: bowser.name+" "+bowser.version, 
                      timestamp: milliseconds, 
                      ip_address: visitor_ip, 
                      developmentId: developmentId,
                      type: "house and land package", 
                      flag: "statistics_fb_share"  },
                      
                      function(data) {
                          
                      }
                 );
                
                var house_name = $(this).attr('house_name');
                var builder_name = $(this).attr('builder_name');
                var house_image = $(this).attr('house_image');
                
                temp = facebook_share_houseland_package_description;
               
                temp = replaceString(temp,'*house_name*',house_name);
                temp = replaceString(temp,'*builder_name*',builder_name);
                temp = replaceString(temp,'*lot_number*',lot_number);
                temp = replaceString(temp,'*developmentName*',developmentName);
                
                caption_temp = facebook_share_houseland_package_caption;
                
                caption_temp = replaceString(caption_temp,'*house_name*',house_name);
                caption_temp = replaceString(caption_temp,'*builder_name*',builder_name);
                
                FB.ui(
                {
                    method: 'feed',
                    name: facebook_share_houseland_package_name,
                    link: facebook_share_houseland_package_link,
                    picture: 'http://app.mapovis.com.au/mpvs/images/dev_houses/'+house_image,
                    caption: temp,
                    description: caption_temp,
                    message: ''
                });
                
            });
        
        }
        
        else{
             
            $this.find('#enquire_link'+i).click(function(e){

                var house_name = $(this).attr('house_name');
                var house_builder = $(this).attr('house_builder');


                var ff=$this.find('#enquiryform');
                ff.contents().find('input[name='+form_iframe_hiddenfield_house_name+']').val(house_name);
                ff.contents().find('input[name='+form_iframe_hiddenfield_builder+']').val(house_builder);   
                
                ff.contents().find('input[name='+form_iframe_hiddenfield_house_name+']').attr('value',house_name);
                ff.contents().find('input[name='+form_iframe_hiddenfield_builder+']').attr('value',house_builder); 

                $this.animate({
                        scrollTop: 10000 
                });
                
            }); 
            
            $this.find('#share_button_'+i).click(function(e){
                e.preventDefault();

                 var milliseconds = new Date().getTime();  // event timestamp

                 $.post( 
                    "../mapovis/save_statistics_info.php",
                    { browserinfo: bowser.name+" "+bowser.version, 
                      timestamp: milliseconds, 
                      ip_address: visitor_ip, 
                      developmentId: developmentId,
                      type: "house and land package", 
                      flag: "statistics_fb_share"  },
                      
                      function(data) {
                          
                      }
                 );
                
                var house_name = $(this).attr('house_name');
                var builder_name = $(this).attr('builder_name');
                var house_image = $(this).attr('house_image');
                
                temp = facebook_share_houseland_package_description;
                
                temp = replaceString(temp,'*house_name*',house_name);
                temp = replaceString(temp,'*builder_name*',builder_name);
                temp = replaceString(temp,'*lot_number*',lot_number);
                temp = replaceString(temp,'*developmentName*',developmentName);
                
                temp_caption = facebook_share_houseland_package_caption;
                
                temp_caption = replaceString(temp_caption,'*house_name*',house_name);
                temp_caption = replaceString(temp_caption,'*builder_name*',builder_name);
                
                FB.ui(
                {
                    method: 'feed',
                    name: facebook_share_houseland_package_name,
                    link: facebook_share_houseland_package_link,
                    picture: 'http://app.mapovis.com.au/mpvs/images/dev_houses/'+house_image,
                    caption: temp_caption,
                    description: temp,
                    message: ''
                });
                
            });
            
            
            
        }

       

        var image_mainpath = "http://app.mapovis.com.au/mpvs/images/dev_houses/";

        var arr = houseMatch.house_images;

        for (var n = 0; n < arr.length; n++) {
            if (arr[n]) {

                var li = "<li><img src='"+image_mainpath+arr[n]+"'></li>";
                $(li).appendTo($('#slider'+i));
            }
        }
      

    }

    if($(this.element).width() < 600){
        $('.col-md-6').addClass('custom-col');
    }
    else{
        $('.col-md-6').removeClass('custom-col');
    }      

    if(stageplanpdf_name != ""){

        var stageplanpdf_div = $("<div class='stageplanpdf_div'><a href='../mapovis/stage-engineering-plans/"+stageplanpdf_name+"' target='_blank' class='stageplanlink'>Click here </a>to download the Stage Engineering Plan</div>").insertAfter('.housematchbottomline');

    }

    /*
      asterisk and download button css style
    */

    $('.asterisk').css('background-color',house_land_package_enquirenow_button);

    $('.download').css({'background-color':house_land_package_downloadpdf_button,'border':'none'});


    $('.download').hover(
    function() {
        $(this).css('background-color',house_land_package_downloadpdf_button_highlight);
    }, function() {
        $( this ).css('background-color',house_land_package_downloadpdf_button);
    }
    );

    $('.enquire_link').css({'background-color':house_land_package_enquirenow_button,'border':'none'});

    $('.enquire_link').hover(
    function() {
        $(this).css('background-color',house_land_package_enquirenow_button_highlight);
    }, function() {
        $( this ).css('background-color',house_land_package_enquirenow_button);
    }
    );

     spinner.stop();
}

module.exports = LotDetailsView;
