function PlaceDetailsViews(place) {
  var detailsView = document.createElement("div");
  detailsView.id = "detailsView";
  detailsView.innerHTML = '<b>' + place.name + '</b>' +
  '\n<br><br> Address: ' +
  place.formatted_address;
  
  if(place.international_phone_number){
      detailsView.innerHTML +=  '\n<br> Phone:' + place.international_phone_number;
  }
  

  // Add Website, if it exists
  if (place.website)
  {
  detailsView.innerHTML +='\n<br><br> Website: <a href="' + place.website + '" target="_blank">' + place.website + '</a><br><br>';
  }



  return detailsView;
}


module.exports = PlaceDetailsViews;