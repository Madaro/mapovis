var font_name = "";
var dialog_start_hex = "";
var dialog_end_hex = "";
var text_hex_colour = "";
var dialog_close_button_hex = "";
var dialog_close_button_foreground_hex = "";
var visitor_ip = "";
var developmentId;

var spinner = null;

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

var opts = {
  lines: 13, // The number of lines to draw
  length: 20, // The length of each line
  width: 10, // The line thickness
  radius: 30, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#000', // #rgb or #rrggbb or array of colors
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: '50%', // Top position relative to parent
  left: '50%' // Left position relative to parent
};



function AmenitiesDetailsView(developmentId,fontfamily_name) {

    this.element = document.createElement('div');
    var self = this;

    if(fontfamily_name != ""){
        font_name = fontfamily_name;
    }


    developmentId = developmentId;


    $.ajax({
        url: '../admin/getdatainjson/development/' + developmentId,
        type: "GET",
        dataType: "json",
        complete: function(data_response) {
            var rs = data_response.responseJSON;
            var developmentData = rs[0];
            dialog_end_hex =  developmentData.dialog_end_hex;
            dialog_start_hex =  developmentData.dialog_start_hex;
            text_hex_colour = '#' + developmentData.text_hex_colour;
            dialog_close_button_hex = '#' + developmentData.dialog_close_button_background_hex;
            dialog_close_button_foreground_hex = '#' + developmentData.dialog_close_button_foreground_hex;


        }

    });


    if((WURFL.form_factor == "Smartphone") || (WURFL.form_factor == "Feature Phone")){
        this.element = $('#amenity_detail_view_container_panel_noiframe');
        $(this.element)
        .appendTo(document.body) 
        .hide()
        .load('../mpvs/templates/AmenityDetails.html', function() {
            self.loaded = true;
            if (self.details) {
                self.setAmenityDetails(self.details)
            }
        });
    }

    else{

        $(this.element)
        .appendTo(document.body) 
        .hide()
        .load('../mpvs/templates/AmenityDetails.html', function() {
            self.loaded = true;
            if (self.details) {
                self.setAmenityDetails(self.details)
            }
        })
        .dialog({

        });
        $(this.element).dialog("close");

    }



}

function setPos(varWidth,varHeight){

    var win = $(window);
    var left = (win.width() - varWidth) / 2;


    if(isMobile.any() == "iPad"){

        if($(window).width() <= 768){

            pos = "center";

        }

        else{

            if(window.innerHeight < 650){ //the case that bookmark bar is on in horizontal mode

                pos = [left,69];  

            }

            else{

                pos = [left,82];   

            } 
        }


    }

    else{

        pos = "center";

    }

    return pos;

}


AmenitiesDetailsView.prototype.setAmenityDetails = function(amenityDetails,visitor_ip) {

    var target = document.getElementsByTagName('body')[0];
    spinner = new Spinner(opts).spin(target);
    
    if(amenityDetails.amenity_iframe_html != ""){

        var pos = setPos(700,500);

        var dialog = $("<div class='iframe_site_div'></div>").append($(amenityDetails.amenity_iframe_html)).dialog({
            width: 700,
            height: 500,
            position: pos,
            open: function(event, ui) {
                
                $('.iframe_site_div').css('padding','0');

                if(amenityDetails.amenity_iframe_scrollbar == 0){

                    $('.ui-dialog .ui-dialog-content').css('overflow','hidden');
                    
                }
                
                if(WURFL.form_factor == "Tablet"){
                    $(".iframe_site_div").addClass('supportScrollingOnTablet'); 
                }
                    

                var milliseconds = new Date().getTime();  // event timestamp

                $.post( 
                "../mapovis/save_statistics_info.php",
                { browser: bowser.name+" "+bowser.version, amenity_id: amenityDetails.amenity_id, timestamp: milliseconds, ip_address: visitor_ip, developmentId: amenityDetails.development_id, flag: "statistics_amenityViews"  },
                function(data) {

                }
                );



                $('.ui-dialog .ui-dialog-titlebar-close').blur();


                $('.ui-dialog .ui-dialog-titlebar').css('background', 'linear-gradient(to bottom, #' + dialog_start_hex + ' 0%, #' + dialog_end_hex + ' 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');



                $('.ui-dialog .ui-dialog-titlebar-close').css('background-color',dialog_close_button_hex);    // dialog bar close button background color setting

                $('.stage_container').hide();
                $('#phonenum_container').hide();

                /*
                  if ms ie version is under 10 or firefox version is under 16, dialog title bar background color
                */
                if((bowser.msie && bowser.version < 10) || (bowser.firefox && bowser.version < 16)){


                    $('.ui-dialog .ui-dialog-titlebar').css('background', '#'+dialog_start_hex);


                }
                // apply gradient effect
                else{

                    $('.ui-dialog .ui-dialog-titlebar').css('filter', 'progid:DXImageTransform.Microsoft.gradient(startColorstr = #' + dialog_start_hex + ',endColorstr = #' + dialog_end_hex + ')');


                }   
                


            },

            close: function(event, ui){

                $('.stage_container').show();
                
                if(parseInt($('.pocp_right').css('right')) == 0){   // if sliding panel is open, hide phone number container
                   $('#phonenum_container').hide();  
                }
                else{
                  $('#phonenum_container').show();  // if sliding panel is not open, show phone number container 
                }

            }

        });

        dialog.show();
        spinner.stop();
        return;

    } 

    var amenity_imagesdiv = $(this.element).find("#amenity_images #amenities_slider");
    amenity_imagesdiv.html("");  

    if (this.loaded) {
        $(this.element)
        .find("#amenity_title")
        .html(amenityDetails.amenity_name);
       
        var image_mainpath = "../mpvs/images/amenities/";

        var arr = [amenityDetails.amenity_picture1, amenityDetails.amenity_picture2, amenityDetails.amenity_picture3, amenityDetails.amenity_picture4, amenityDetails.amenity_picture5]; 

        for (var n = 0; n < arr.length; n++) {

            if (arr[n] != "") {

                var li = "<li><img src='"+image_mainpath+arr[n]+"'></li>";

                $(li).appendTo($('#amenity_images #amenities_slider'));

            }
        }


        $("#amenities_slider").responsiveSlides({
            maxwidth: 350,
            speed: 800
        });

        $(this.element)
        .find("#description")
        .html(amenityDetails.amenity_description);

        if(amenityDetails.amenity_moreinfo_url != ""){
            $(this.element)
            .find("#more_info").css("display","block");

            $(this.element)
            .find("#more_info")
            .attr("href",amenityDetails.amenity_moreinfo_url);
        }

        else{
            $(this.element)
            .find("#more_info").css("display","none");
        }


    } else {
        this.details = amenityDetails; 
    }


    if((WURFL.form_factor == "Smartphone") || (WURFL.form_factor == "Feature Phone")){
        
        $(this.element).show();
        spinner.stop();
    }

    else{

        $(this.element)
        .show()

        // TODO: Jorge, dialog( "open" ); cannot accept any arguments. Therefore I cannot specify the width, height etc and it gets automatically set. See http://api.jqueryui.com/dialog/#method-open. 
        // I therefore changed to using http://api.jqueryui.com/dialog/#event-open. Please check this and see if its OKAY. 
        .dialog({
            width: 700,
            height: 500,
            position: pos,
            open: function(event, ui) {

                $(this).scrollTop(0);

                $('.stage_container').hide();    // When dialog opens, stage container and phone number div hidden.
                $('#phonenum_container').hide();

                $('.ui-dialog .ui-dialog-titlebar-close').blur();
                /*
                  in safari and chrome etc, dialog title bar gradient effect
                */
                $('.ui-dialog .ui-dialog-titlebar').css('background', 'linear-gradient(to bottom, #' + dialog_start_hex + ' 0%, #' + dialog_end_hex + ' 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

                /*
                  In Internet explorer, dialog title bar gradient effect
                
                */
                $('.ui-dialog .ui-dialog-titlebar').css('filter', 'progid:DXImageTransform.Microsoft.gradient(startColorstr = #' + dialog_start_hex + ',endColorstr = #' + dialog_end_hex + ')');


                if(font_name != ""){
                    $('*').not('.fa, .pocp_button_right > i').css("font-family", font_name+", serif");
                }


                var milliseconds = new Date().getTime();  // event timestamp

                $.post( 
                "../mapovis/save_statistics_info.php",
                { browser: bowser.name+" "+bowser.version, amenity_id: amenityDetails.amenity_id, timestamp: milliseconds, ip_address: visitor_ip, developmentId: amenityDetails.development_id, flag: "statistics_amenityViews"  },
                function(data) {
                });

                if(font_name != ""){
                    $('*').not('.fa, .pocp_button_right > i').css("font-family", font_name+", serif");    // custom font family setting such as google font or custom font.
                }
                
            },

            close: function(event, ui){
                $('.stage_container').show();
                
                if(parseInt($('.pocp_right').css('right')) == 0){    // if sliding panel is open, phone number div hidden.
                   $('#phonenum_container').hide();  
                }
                else{
                  $('#phonenum_container').show();        // else, phone number shown.
                }
                
            }
        });
        
        

    }

    spinner.stop();
}

      
module.exports = AmenitiesDetailsView;
