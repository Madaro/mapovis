
function PlacePanelEntryDetailsView(place,map,dev_center,markers,nearby_places_map_marker_default,nearby_places_map_marker_selected,nearby_places_sidebar_icon_colour,nearby_places_sidebar_icon_colour_selected) {
    var detailsView = document.createElement("div");
    detailsView.className = "near_place";
    detailsView.id = "entry"+place.id;
    
    $.fn.raty.defaults.path = '../mpvs/assets/raty/images';     // jquery rate plugin's image path setting.
    
    
    var innerHtml = '<div class="nearplace_contactinfo">'+
    '<p class="place_name">'+place.name+'</p>'+
    '<p class="place_address">'+place.formatted_address+'</p>';
    if(place.formatted_phone_number){
       innerHtml += '<p class="phone_number">'+place.formatted_phone_number+'</p>';  
    }
    
    if(place.website){
        innerHtml +=  '<a class="nearplace_url" href="'+place.website+'" target="_blank">'+place.website+'</a>';
    }
    
    innerHtml += '</div>';
    
    innerHtml += '<div class="near_place_locationinfo">'+

    '<div class="marker_position" location="'+place.geometry.location+'" >'+
    '<i class="fa fa-map-marker"></i>'+
    '<span>Show</span>'+
    '</div>'+

    '<div class="distance_to_dev" location="' + place.geometry.location + '" address ="'+place.formatted_address+'">'+
    '<i class="fa fa-exchange"></i>'+
    '<span>Directions</span>'+
    '</div>';
    
    if(place.rating){
      innerHtml +=  '<span class="rating_val">'+place.rating+'</span><div id="pp'+place.id+'" class="place_review_mark"></div>'; 
       
    } 
    
    if(place.reviews){
       if(place.reviews.length == 1){
             innerHtml += '<a class="place_reviews" href="'+place.url+'" target="_blank">'+place.reviews.length+' review</a>'; 
       }
       else{
           innerHtml += '<a class="place_reviews" href="'+place.url+'" target="_blank">'+place.reviews.length+' reviews</a>'; 
       } 
      
    }
    
    innerHtml +=  '</div>'; 

    detailsView.innerHTML  = innerHtml;
                

    detailsView.getElementsByClassName('distance_to_dev')[0].addEventListener('click', function (event) {

        var $loc = $(this).attr('location');
        var $addr = $(this).attr('address');

        $loc = $loc.replace(/[()]/g,''); 
        var latlng = $loc.split(",");

        pos = latlng[0]+","+latlng[1];

        $('#external_amenity_coordinate').val(pos); 
        $('#external_amenity_address').val($addr);

        $('#destination').val($addr);
        $('#getdirectionsbutton > a').trigger('click'); 
        
        // by above trigger actions,  external_amenity_coordinate , external_amenity_coordinate,   destination values are set as empty.
        // so once again repeated.
        
        $('#external_amenity_coordinate').val(pos); 
        $('#external_amenity_address').val($addr);

        $('#destination').val($addr);

    });
    
     detailsView.getElementsByClassName('marker_position')[0].addEventListener('click', function (event) {
         
        if($(this).hasClass('selected')){
            $(this).removeClass('selected');
            map.setZoom(12);
            map.setCenter(dev_center);
            
            $(this).find('i').css('color',nearby_places_sidebar_icon_colour);  
            $(this).find('span').css('color',nearby_places_sidebar_icon_colour);
            
            if((WURFL.form_factor == "Tablet") || (WURFL.form_factor == "Desktop")){
                    setTimeout(function(){
                        map.panBy(200,0);
                    }, 500);

            }
            
            return;
        }
        
        else{
           $('.marker_position').removeClass('selected'); 
           $(this).addClass('selected'); 
           
        }
        
        var $loc = $(this).attr('location');
        $('.marker_position').find('i').css('color',nearby_places_sidebar_icon_colour);      // all markers icon of list have default color
        $('.marker_position').find('span').css('color',nearby_places_sidebar_icon_colour);
        
        $(this).find('i').css('color',nearby_places_sidebar_icon_colour_selected);            // highlighted marker icon has selected color.
        $(this).find('span').css('color',nearby_places_sidebar_icon_colour_selected); 

        $loc = $loc.replace(/[()]/g,''); 
        var latlng = $loc.split(",");
        
        var coordinate = new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
        
        for(var i = 0; i < markers.length; i++){
            if((markers[i].getPosition().lat() == coordinate.lat()) && (markers[i].getPosition().lng() == coordinate.lng())){ 
                
                var place_icon = new google.maps.MarkerImage(nearby_places_map_marker_selected, null, null, null, new google.maps.Size(20,37));  
                markers[i].setIcon(place_icon);
                markers[i].setZIndex(google.maps.Marker.MAX_ZINDEX + 1);   // set marker's zIndex to up to top so that user can see it apparently.
                map.setZoom(16); 
                google.maps.event.trigger(markers[i], 'click'); 
               
            }
            
            else{
                
                var place_red_icon = new google.maps.MarkerImage(nearby_places_map_marker_default, null, null, null, new google.maps.Size(20,37));
                markers[i].setIcon(place_red_icon);
                markers[i].setZIndex(google.maps.Marker.MAX_ZINDEX);
                
            }
            
        }
          
        map.setCenter(coordinate);
        map.panBy(200,0);
         
    });
    
    $(detailsView).appendTo($('#nearbyplaces_group'));
    $('div#pp'+place.id).raty({ readOnly: true,halfShow : true, score: parseFloat(place.rating) });  // set place's review rate with stars
     
}


module.exports = PlacePanelEntryDetailsView;
