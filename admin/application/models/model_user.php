<?php
Class Model_user extends CI_Model
{
	public function isAdminLoggedIn()
	{
		return ($this->session->userdata('logged_in_id'));
	}

	public function isBuilderLoggedIn()
	{
		return ($this->session->userdata('builder_logged_id'));
	}

	public function isUserLogged()
	{
		return ($this->session->userdata('user_logged_id'));
	}

	public function login($username, $password)
	{
		$safe_username = $this->db->escape($username);
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('(username = '.$safe_username.' OR email = '.$safe_username.')');
		$this->db->where('password', MD5($password));
		$this->db->limit(1);

		$query = $this->db->get();

		if($query->num_rows() == 1)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function getUsers($filters = array(), $builder = FALSE)
	{
		if($builder){
			$this->db->select('users.*, builders.builder_name');
			$this->db->join('builder_users', 'builder_users.user_id = users.user_id', 'left');
			$this->db->join('builders', 'builders.builder_id = builder_users.builder_id', 'left');
		}
		else{
			$this->db->join('developers', 'users.developer = developers.developer_id', 'left');
		}
		if(isset($filters['user_role'])){
			$this->db->where('users.user_role', $filters['user_role']);
		}
		if(isset($filters['user_ids'])){
			$this->db->where_in('users.user_id ', $filters['user_ids']);
		}
		if(isset($filters['no_empty_name']) && $filters['no_empty_name'] == 1 ){
			$this->db->where('users.name != \'\' ', NULL, FALSE);
		}
		$this->db->group_by('users.user_id', 'asc');
		$this->db->order_by('users.name', 'asc');
		$query  = $this->db->get('users');
		$results = array();
		if ($query->num_rows() > 0){
			$results = $query->result();
		}
		return $results;
	}

	public function getUser($user_id, $include_builder_id = FALSE)
	{
		$this->db->join('developers', 'users.developer = developers.developer_id', 'left');
		if($include_builder_id){
			$this->db->join('builder_users', 'builder_users.user_id = users.user_id', 'left');
		}
		$query = $this->db->get_where('users', array('users.user_id' => $user_id));
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->row();
		}
		return $result;
	}

	public function getAdminLoggedIn()
	{
		$user_id = $this->session->userdata('logged_in_id');
		return $this->getUser($user_id);
	}

	public function getUserLogged()
	{
		$user_id = $this->session->userdata('user_logged_id');
		return $this->getUser($user_id);
	}

	public function getBuilderLogged()
	{
		$user_id = $this->session->userdata('builder_logged_id');
		return $this->getUser($user_id);
	}

	function addUser($form_data, $role = 'User')
	{
		// Setup User Data Ready to Insert into Database
		$new_user = array(
			'username'    => $form_data['username'],
			'password'    => md5($form_data['password']),
			'email'       => $form_data['email'],
			'name'        => $form_data['name'],
			'developer'   => isset($form_data['developer'])? $form_data['developer']: 0,
			'office'      => $form_data['office'],
			'officephone' => $form_data['officephone'],
			'mobilephone' => $form_data['mobilephone'],
			'user_role'   => $role
		);

		// Run Database Insert
		$this->db->insert('users', $new_user);
		$user_id = $this->db->insert_id();
		if($user_id && $role == 'Builder' && $form_data['builder_id']){
			$this->load->model('Model_builder_user', '', TRUE);
			$this->Model_builder_user->addBuilderUser($form_data['builder_id'], $user_id);
		}
		return $user_id;
	}

	public function updateUser($user_id, $form_data)
	{
		$fields       = $this->getFields();
		$user_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$user_details[$field] = $form_data[$field];
			}
		}
		if(isset($form_data['password']) && !empty($form_data['password']) && $form_data['password'] == $form_data['confirm_password']){
			$user_details['password'] = md5($form_data['password']);
		}

		$this->db->where('users.user_id', $user_id);
		return $this->db->update('users', $user_details);
	}

	public function setDefaultPagination($user_id, $default_pagination)
	{
		if($default_pagination){
			$this->db->where('users.user_id', $user_id);
			$this->db->update('users', array('default_pagination' => $default_pagination));
		}
	}

	public function deleteUser($user_id)
	{
		$this->db->where('user_id', $user_id);
		return $this->db->delete('users');
	}

	public function findUserEmailAdress($email_address, $user_role = 'User')
	{
		$this->db->where('users.user_role', $user_role);
		$this->db->where('users.email', $email_address);
		$query = $this->db->get('users');
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		return FALSE;
	}

	public function resetPasswordSendNotification($email_address)
	{
		$this->load->model('Model_config', '', TRUE);
		$this->load->library('mapovis_lib');
		$this->load->library('email_sender');

		$user = $this->Model_user->findUserEmailAdress($email_address);
		if($user){
			$new_password = $this->getRandomPassword();
			$this->db->where('users.user_id', $user->user_id);
			if($this->db->update('users', array('password' => MD5($new_password)))){
				$settings          = $this->Model_config->getConfig();
				$from              = (object)array('email' => $settings->support_email, 'name' => 'MAPOVIS');
				$subject           = 'MAPOVIS - Your password has been reset';
				unset($user->password);
				$user->fullname    = $user->name;
				$user->newpassword = $new_password;
				$user->title       = 'You requested a new password';
				$template_name     = 'password-request-user';
				$tags              = 'You requested new password';
				$log_notes         = '';
				$sent              = $this->email_sender->sendEmailNotificaton($subject, $template_name, $from, $user, $user, $tags, $log_notes);
				return $sent;
			}
		}
		return FALSE;
	}

	public function resetBuilderPasswordSendNotification($email_address)
	{
		$this->load->model('Model_config', '', TRUE);
		$this->load->library('mapovis_lib');
		$this->load->library('email_sender');

		$user = $this->Model_user->findUserEmailAdress($email_address, 'Builder');
		if($user){
			$new_password = $this->getRandomPassword();
			$this->db->where('users.user_id', $user->user_id);
			if($this->db->update('users', array('password' => MD5($new_password)))){
				$settings          = $this->Model_config->getConfig();
				$from              = (object)array('email' => $settings->support_email, 'name' => 'MAPOVIS');
				$subject           = 'MAPOVIS - Your password has been reset';
				unset($user->password);
				$user->fullname    = $user->name;
				$user->newpassword = $new_password;
				$user->title       = 'You requested a new password';
				$template_name     = 'password-request-user';
				$tags              = 'You requested new password';
				$log_notes         = '';
				$sent              = $this->email_sender->sendEmailNotificaton($subject, $template_name, $from, $user, $user, $tags, $log_notes);
				return $sent;
			}
		}
		return FALSE;
	}

	private function getRandomPassword()
	{
		$chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-=@$*+,.;:';
		$str   = '';
		$max   = strlen($chars) - 1;
		for ($i=0; $i < 8; $i++){
		  $str .= $chars[mt_rand(0, $max)];
		}
		return $str;
	}

	public function isUnique($email_address, $user_id = 0)
	{
		$this->db->where('users.user_id !=', $user_id);
		$this->db->where('users.email', $email_address);
		$query = $this->db->get('users');
		if ($query->num_rows() > 0)
		{
			return FALSE;
		}
		return TRUE;
	}

	public function isUniqueUsername($username, $user_id = 0)
	{
		$this->db->where('users.user_id !=', $user_id);
		$this->db->where('users.username', $username);
		$query = $this->db->get('users');
		if ($query->num_rows() > 0)
		{
			return FALSE;
		}
		return TRUE;
	}

	private function getFields()
	{
		return array(
			'name',
			'email',
			'developer',
			'office',
			'officephone',
			'mobilephone',
		);
	}
}
?>