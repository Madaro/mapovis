<?php
Class Model_house_lot_order_image extends CI_Model
{
	/**
	 * Returns the house images
	 * @param int $house_id
	 * @return array
	 */
	function getHouseLotImages($house_id, $lot_id)
	{
		$this->db->select('house_images.*, CONCAT(COALESCE(house_lot_order_images.file_order,999), "-", house_images.file_order) AS package_file_order', FALSE);
		$this->db->join('house_lot_order_images', 'house_lot_order_images.house_image_id = house_images.house_image_id AND house_lot_order_images.lot_id = '.$lot_id, 'left');
		$this->db->where('house_images.house_id', $house_id);
		$this->db->where('house_images.file_type', 'facade');
		$this->db->group_by('house_images.house_image_id');
		$this->db->order_by('package_file_order', 'asc');
		$this->db->order_by('house_images.file_order', 'asc');
		$query  = $this->db->get('house_images');
		return ($query)? $query->result(): array();
	}

	/**
	 * Updates the current images or save new ones
	 * 
	 * @param int $house_id
	 * @param int $lot_id
	 * @param array $ordered_images
	 * @return boolean
	 */
	function updatePackageImagesOrder($house_id, $lot_id, $form_data)
	{
		$this->load->model('Model_house_image', '', TRUE);
		$ordered_images = array();
		foreach($form_data['facade_images'] as $image_id_url){
			// if there are new images save them
			$house_image_id = $this->Model_house_image->updateGlobalHouseImage($house_id, $image_id_url, $form_data);
			$ordered_images[] = ($house_image_id)? $house_image_id: $image_id_url;
		}
		
		$current_order_images = $this->Model_house_image->getHouseImages($house_id, array('file_type' => 'facade'));
		$current_images       = array_map(function($v){return $v->house_image_id;}, $current_order_images);
		$diff_order           = array_diff_assoc($ordered_images, $current_images);

		// delete current order
		$this->db->where('lot_id', $lot_id);
		$this->db->where_in('house_image_id', $current_images);
		$this->db->delete('house_lot_order_images');

		// if the order is different to the default add records with new order
		if(!empty($diff_order)){
			$order = 1;
			foreach($ordered_images as $image_id){
				// make sure the image still exists
				if(!in_array($image_id, $current_images)){
					continue;
				}
				$package_image = array(
					'house_image_id' => $image_id,
					'lot_id' => $lot_id,
					'file_order' => $order,
				);
				$this->db->insert('house_lot_order_images', $package_image);
				$order++;
			}
		}
	}

	function deleteHouseImage($house_image_id)
	{
		$this->db->select('house_images.house_id, house_lot_order_images.*');
		$this->db->join('house_images', 'house_lot_order_images.house_image_id = house_images.house_image_id ', 'left');
		$this->db->where('house_lot_order_images.house_image_id', $house_image_id);
		$query          = $this->db->get('house_lot_order_images');
		$image_packages = ($query)? $query->result(): array();
		foreach($image_packages as $image_package){
			// delete house image record
			$this->db->where('house_lot_order_image_id', $image_package->house_lot_order_image_id);
			$this->db->delete('house_lot_order_images');

			// update house images order
			$this->db->set('file_order', 'file_order - 1', FALSE);
			$this->db->where("house_image_id IN (SELECT house_image_id FROM house_images WHERE house_id = {$image_package->house_id})", null);
			$this->db->where('lot_id', $image_package->lot_id);
			$this->db->where('file_order >', $image_package->file_order);
			$this->db->update('house_lot_order_images');
		}
	}

}