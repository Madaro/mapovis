<?php
define('NUMBER_SCHEDULES', 3);

Class Model_schedule extends CI_Model
{
	public function getSchedules($development_id, $active = FALSE)
	{
		$this->db->order_by('schedule_enabled', 'desc');
		$this->db->order_by('schedule_day', 'asc');
		$this->db->order_by('schedule_time', 'asc');
		$this->db->where('schedules.development_id', $development_id);
		if($active){
			$this->db->where('schedules.schedule_enabled', 1);
		}
		$query  = $this->db->get('schedules');
		$result = array();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		if(!$active){
			$fields        = $this->getFields();
			/* Ensure it returns 3 schedules  */
			for($pos = count($result); $pos < NUMBER_SCHEDULES; $pos++){
				$result[] = (object) $fields;
			}
		}
		return $result;
	}

	public function getFormatedSchedule($development_id)
	{
		$schedules    = $this->getSchedules($development_id, TRUE);
		$days_of_week = $this->getDaysOfWeek();
		$when         = array();
		$by           = array();
		foreach($schedules as $schedule){
			$when[] = $days_of_week[$schedule->schedule_day];
			$by[]   = ($schedule->schedule_time % 12).':30 '.(floor($schedule->schedule_time/12)? 'pm': 'am');
		}
		$last_when   = (count($when) > 1)? ' & '. array_pop($when): ''; 

		return array('when' => implode(', ', $when).$last_when, 'by' => implode(', ', $by));
	}

	public function getNextSchedule($development)
	{
		$this->load->library('mapovis_lib');

		$schedules       = $this->getSchedules($development->development_id, TRUE);
		$last_update     = strtotime($development->last_update);
		$days_of_week    = $this->getDaysOfWeek();
		$schedules_dates = array();
		foreach($schedules as $schedule){
			// get current schedule dates
			if(isset($days_of_week[$schedule->schedule_day])){
				$schedules_dates[strtotime("Next {$days_of_week[$schedule->schedule_day]}", strtotime('+8 hours', $last_update))] = $schedule;
			}
		}

		// Use the closest schedule to the last update done to the development
		if(count($schedules_dates)){
			ksort($schedules_dates);
			$next_schedule     = current($schedules_dates);

			$schedule_date     = current(array_keys($schedules_dates)) + $next_schedule->schedule_time * 3600;
			$timezone          = $this->mapovis_lib->getTimeZone($development->state);
			return date_create(date('Y-m-d H:i:s', $schedule_date), timezone_open($timezone));
		}
		return false;
	}

	public function updateSchedules($development_id, $form_data)
	{
		$schedules = $this->getSchedules($development_id);
		$fields    = $this->getFields();
		for($pos = 0; $pos < NUMBER_SCHEDULES; $pos++){
			$schedule_details = array('development_id' => $development_id);
			foreach(array_keys($fields) as $field_name){
				if(isset($form_data[$field_name][$pos])){
					$schedule_details[$field_name] = $form_data[$field_name][$pos];
				}
			}
			// if schedule already exists update the records if not create new record
			if(isset($schedules[$pos]->schedule_id) && $schedules[$pos]->schedule_id){
				$this->db->where('schedules.schedule_id', $schedules[$pos]->schedule_id);
				$this->db->update('schedules', $schedule_details);
			}
			else{
				$this->db->insert('schedules', $schedule_details); 
			}
		}
		return true;
	}

	public function getDaysOfWeek()
	{
		return array(
			1 => 'Monday',
			2 => 'Tuesday',
			3 => 'Wednesday',
			4 => 'Thursday',
			5 => 'Friday',
			6 => 'Saturday',
			0 => 'Sunday'
		);
	}

	private function getFields()
	{
		return array(
			'schedule_enabled' => 0,
			'schedule_day'     => 0,
			'schedule_time'    => 0,
		);
	}
}
?>