<?php
Class Model_stage extends CI_Model
{
	public function getStages($development_id, $precinct_id = null)
	{
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('precincts.development_id', $development_id);
		if($precinct_id){
			$this->db->where('precincts.precinct_id', $precinct_id);
		}
		$this->db->order_by('precincts.precinct_number', 'asc');
		$this->db->order_by('stages.stage_number', 'asc');
		$query  = $this->db->get('stages');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	public function getStagesActiveLots($development_id)
	{
		$this->db->select('stages.*');
		$this->db->join('lots', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->where('lots.status', 'Available');
		$this->db->group_by('stages.stage_id');
		$this->db->order_by('stages.stage_number', 'asc');
		$query  = $this->db->get('stages');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	public function getStage($stage_id)
	{
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$query  = $this->db->get_where('stages', array('stages.stage_id' => $stage_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row();
		}
		return $result;
	}

	public function getStageByNumber($development_id, $stage_number)
	{
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('stages.stage_number', $stage_number);
		$this->db->where('precincts.development_id', $development_id);
		$query  = $this->db->get('stages');
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row();
		}
		return $result;
	}

	public function getStageSoldPercentage($stage_id, $filters = array())
	{
		$this->db->select('COUNT(lots.lot_id) AS total');
		$this->db->where('lots.stage_id', $stage_id);
		$this->db->where('lots.status', 'Available');

		$widths = array();
		if(isset($filters['lot_width']) && !empty($filters['lot_width'])){
			foreach($filters['lot_width'] as $lot_width){
				if(!$lot_width){
					continue;
				}
				@list($min, $max) = explode('|', $lot_width);
				$max_query        = ($max)? " AND lots.lot_width <= {$max}":'';
				$widths[]         = "(lots.lot_width >= {$min} {$max_query})";
			}
			if(count($widths)){
				$this->db->where('('.implode(' OR ', $widths).')');
				$this->db->order_by('lots.lot_width', 'asc');
			}
		}
		$sizes = array();
		if(isset($filters['lot_square_meters']) && !empty($filters['lot_square_meters'])){
			foreach($filters['lot_square_meters'] as $lot_size){
				if(!$lot_size){
					continue;
				}
				@list($min, $max) = explode('|', $lot_size);
				$max_query        = ($max)? " AND lots.lot_square_meters <= {$max}":'';
				$sizes[]          = "(lots.lot_square_meters >= {$min} {$max_query})";
			}
			if(count($sizes)){
				$this->db->where('('.implode(' OR ', $sizes).')');
				$this->db->order_by('lots.lot_square_meters', 'asc');
			}
		}
		if(isset($filters['lot_square_meters_from']) && !empty($filters['lot_square_meters_from'])){
			$this->db->where("lots.lot_square_meters >= {$filters['lot_square_meters_from']}");
		}
		if(isset($filters['lot_square_meters_to']) && !empty($filters['lot_square_meters_to'])){
			$this->db->where("lots.lot_square_meters <= {$filters['lot_square_meters_to']}");
		}
		if(isset($filters['land_price_from']) && !empty($filters['land_price_from'])){
			$this->db->where('lots.price_range_min >= '.(float)$filters['land_price_from']);
		}
		if(isset($filters['land_price_to']) && !empty($filters['land_price_to'])){
			$this->db->where('lots.price_range_max <= '.(float)$filters['land_price_to']);
		}
		$available_lots = $this->db->get('lots')->row()->total;

		$this->db->select('COUNT(lots.lot_id) AS total');
		$this->db->where('lots.stage_id', $stage_id);
		$this->db->where('lots.status', 'Sold');
		if(count($sizes)){
			$this->db->where('('.implode(' OR ', $sizes).')');
		}
		if(count($sizes)){
			$this->db->where('('.implode(' OR ', $sizes).')');
		}
		$sold_lots = $this->db->get('lots')->row()->total;
		return ($sold_lots)? round(100 * $sold_lots/($sold_lots + $available_lots)): 0;
	}

	public function addStage($precinct_id, $form_data)
	{
		$data   = array('precinct_id' => $precinct_id, 'stage_number' => $form_data['stage_number']);
		$fields = $this->getFields();
		foreach($fields as $field_id){
			if(isset($form_data[$field_id])){
				$data[$field_id] = $form_data[$field_id];
			}
		}
		if(!empty($_FILES['stage_pdf_file'])){
			$this->load->model('Model_precinct', '', TRUE);
			$precinct  = $this->Model_precinct->getPrecinct($precinct_id);
			$file_path = BASEPATH.'../../mpvs/templates/dev_stage_pdfs/';
			if (!file_exists("{$file_path}")) {
				mkdir("{$file_path}", 0777, true);
			}
			if($_FILES['stage_pdf_file']['type'] == 'application/pdf'){
				// Save PDF file
				$file_name      = "Dev_{$precinct->development_id}_Stage_plan_".time().'.pdf';
				$file_path_name = $file_path.$file_name;
				if(file_exists($file_path_name)){
					unlink($file_path_name);
				}
				move_uploaded_file($_FILES['stage_pdf_file']['tmp_name'], $file_path_name);
				$data['stage_pdf_file'] = $file_name;
			}
		}
		
		$this->db->insert('stages', $data);
		return $this->db->insert_id();
	}

	public function updateStage($stage_id, $form_data, $salesteam = FALSE)
	{
		$stage  = $this->getStage($stage_id);
		$data   = array();
		$fields = (!$salesteam)? $this->getFields(): array('stage_name', 'title_release');
		foreach($fields as $field_id){
			if(isset($form_data[$field_id])){
				$data[$field_id] = $form_data[$field_id];
			}
		}
		if(!empty($_FILES['stage_pdf_file'])){
			$file_path = BASEPATH.'../../mpvs/templates/dev_stage_pdfs/';
			if (!file_exists("{$file_path}")) {
				mkdir("{$file_path}", 0777, true);
			}
			if($_FILES['stage_pdf_file']['type'] == 'application/pdf'){
				// Save PDF file
				$old_file_name  = (!empty($stage->stage_pdf_file))? $stage->stage_pdf_file: "Dev_{$stage->development_id}_Stage_{$stage_id}_plan.pdf";
				$file_name      = "Dev_{$stage->development_id}_Stage_{$stage_id}_plan.pdf";
				$file_path_name = $file_path.$file_name;
				if(file_exists($file_path.$old_file_name)){
					unlink($file_path_name);
				}
				move_uploaded_file($_FILES['stage_pdf_file']['tmp_name'], $file_path_name);
				$data['stage_pdf_file'] = $file_name;
			}
		}
		$this->db->where('stages.stage_id', $stage_id);
		return $this->db->update('stages', $data);
	}

	public function deleteStage($stage_id)
	{
		$stage = $this->getStage($stage_id);
		if(!empty($stage->stage_pdf_file)){
			$file_path = BASEPATH.'../../mpvs/templates/dev_stage_pdfs/'.$stage->stage_pdf_file;
			if(file_exists($file_path)){
				unlink($file_path);
			}
		}
		$tables = array('change_logs', 'lots', 'stages');
		// delete stage related records
		foreach($tables as $table_name){
			$this->db->where('stage_id', $stage_id);
			$this->db->delete($table_name);
		}
		return TRUE;
	}

	public function isUnique($precinct_id, $stage_number)
	{
		$this->db->where('stages.precinct_id', $precinct_id);
		$this->db->where('stages.stage_number', $stage_number);
		$query = $this->db->get('stages');
		if ($query->num_rows() > 0)
		{
			return FALSE;
		}
		return TRUE;
	}

	private function getFields()
	{
		return array(
			'stage_name',
			'title_release',
			'stage_center_latitude',
			'stage_center_longitute',
			'stage_zoomlevel',
			'stage_icon',
			'stage_code',
			'stage_icon_latitude',
			'stage_icon_longitude',
			'stage_icon_zoomlevel_show',
			'stage_icon_zoomlevel_hide',
			'stage_icon_width',
			'stage_icon_height',
			'display_stage_name',
            'stage_polygon_coords_clickable_at_zoom_level',
            'stage_polygon_coords_unclickable_at_zoom_level'
		);
	}
}
?>