<?php
Class Model_change_log extends CI_Model
{
	public function getChangeLogs($filters = array(), $with_time_formatted = TRUE, $limit =	2000)
	{
		$this->db->select('change_logs.change_log_id, change_logs.user_id, change_logs.user_type, change_logs.lot_id, change_logs.datetime, change_logs.section_type, change_logs.section_id, change_logs.action, change_logs.action_note, change_logs.datetimezone');
		$this->db->select('lots.*, stages.*, precincts.*, developments.development_name, developers.developer AS developer_name, users.name, users.name AS user_name');
		$this->db->join('lots', 'lots.lot_id = change_logs.lot_id');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('developments', 'developments.development_id = precincts.development_id');
		$this->db->join('developers', 'developers.developer_id = developments.developerid', 'left');
		$this->db->join('users', 'users.user_id = change_logs.user_id', 'left');

		if(isset($filters['development_id']) && $filters['development_id']){
			$this->db->where('developments.development_id', $filters['development_id']);
		}
		if(isset($filters['development_ids'])){
			$this->db->where_in('developments.development_id', $filters['development_ids']);
		}
		if(isset($filters['from_datetime']) && $filters['from_datetime']){
			$this->db->where('change_logs.datetimezone >=', $filters['from_datetime']);
		}
		if(isset($filters['notes']) && !empty($filters['notes'])){
			if(is_array($filters['notes'])){
				$this->db->where('(change_logs.action_note LIKE "%'. implode('%" OR change_logs.action_note LIKE "%', $filters['notes']). '%")');
			}
			else{
				$this->db->like('change_logs.notes', $filters['notes']);
			}
		}
		$this->db->order_by('change_logs.datetime', 'desc');

		$query  = $this->db->get('change_logs', $limit);
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		if($with_time_formatted){
			$this->load->library('mapovis_lib');
			$timezone = $this->mapovis_lib->getTimeZone();
			foreach($result as $row){
				// chaning time to default time zone
				$row_datetime   = date_create(date('Y-m-d H:i:s', $row->datetime));
				$row_datetime->setTimeZone(timezone_open($timezone));

				$row->formatted_date = date_format($row_datetime, 'd/m/Y');
				$row->formatted_time = date_format($row_datetime, 'h:i a');
				$row->formatted_diff = $this->mapovis_lib->getTimeDifference($row->datetime);
			}
		}
		return $result;
	}

	public function addChangeLog($action, $section, $changes, $section_id, $ids, $user, $import = FALSE)
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_user', '', TRUE);

		$development_id = (isset($ids->development_id))? $ids->development_id: 0;
		$user_role      = ($import)? 'Imported': $this->Model_development->getDevelopmentRole($development_id, $user);

		$log_data       = array(
			'user_id'        => $user->user_id,
			'user_type'      => $user_role,
			'development_id' => $development_id,
			'precinct_id'    => (isset($ids->precinct_id)? $ids->precinct_id: NULL),
			'stage_id'       => (isset($ids->stage_id)? $ids->stage_id: NULL),
			'lot_id'         => (isset($ids->lot_id)? $ids->lot_id: NULL),
			'datetime'       => time(),
			'section_type'   => $section,
			'section_id'     => $section_id,
			'action'         => $action,
			'action_note'    => implode("\n", $changes),
		);
		$this->db->insert('change_logs', $log_data);
		return $this->db->insert_id();
	}

	function getSoldLotsFormatted($development_id)
	{
		$this->load->library('mapovis_lib');

		$this->db->select("lots.lot_width, from_unixtime(change_logs.datetime, '%M %Y') AS formated_date", FALSE);
		$this->db->select("from_unixtime(change_logs.datetime, '%Y%m') AS month_date", FALSE);
		$this->db->select('COUNT(change_logs.change_log_id) as number_lots, change_logs.lot_id, lots.*, stages.*, precincts.*');

		$this->db->join('lots', 'lots.lot_id = change_logs.lot_id', 'left');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left');

		$this->db->where('action_note LIKE "%Available to Sold%"');
		$this->db->where('precincts.development_id', $development_id);
		// Ignore the data from current month and backtrack to 12 months
		$this->db->where('change_logs.datetime >=', strtotime(date('Y-m-01 00:00:00', strtotime('-12 months'))));
		$this->db->where('change_logs.datetime <=', strtotime(date('Y-m-d 23:59:59', strtotime('last day of previous month'))));
		$this->db->group_by(array('lots.lot_width', "from_unixtime(change_logs.datetime, '%Y%m')"), FALSE);
		
		$this->db->order_by('month_date', 'asc');
		$this->db->order_by('lots.lot_width', 'asc');
		$query            = $this->db->get('change_logs');
		$results          = $query->result();
		$tmp_widths       = $this->getWidths($development_id);
		$formated_resutls = array();
		$all_widths       = array();
		if($results){
			foreach($results as $row){
				$lot_width = (string)$this->mapovis_lib->roundNearestfromarrayvals($tmp_widths, $row->lot_width);
				if(!in_array($lot_width, $all_widths)){
					$all_widths[] = $lot_width;
				}
				if(!isset($formated_resutls[$row->formated_date][$lot_width])){
					$formated_resutls[$row->formated_date][$lot_width] = $row;
				}
				else{
					$formated_resutls[$row->formated_date][$lot_width]->number_lots += $row->number_lots;
				}
			}
		}
		sort($all_widths);
		return array('widths' => $all_widths, 'results' => $formated_resutls);
	}

	private function getWidths($development_id)
	{
		$this->db->select('lots.lot_width');
		$this->db->join('lots', 'lots.lot_id = change_logs.lot_id', 'left');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left');
		$this->db->where('action_note LIKE "%Available to Sold%"');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->where('lots.lot_width % 0.5 = 0 ');
		$this->db->group_by('lots.lot_width');
		$this->db->order_by('lots.lot_width', 'asc');
		$query   = $this->db->get('change_logs');
		$results = $query->result();
		$widths  = array();
		if($results){
			foreach($results as $result){
				$widths[] = (float)$result->lot_width;
			}
		}
		return $widths;
	}

	function deleteDevelopmentLogs($development_id)
	{
		$this->db->where('change_logs.development_id', $development_id);
		return $this->db->delete('change_logs');
	}

}
?>