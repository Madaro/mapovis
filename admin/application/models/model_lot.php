<?php
defined('LOT_MIN_WIDTH') or define('LOT_MIN_WIDTH', 8);
defined('LOT_MAX_WIDTH') or define('LOT_MAX_WIDTH', 18);
defined('LOT_WIDTH_INCREMENT') or define('LOT_WIDTH_INCREMENT', 2);
defined('LOT_MIN_SIZE') or define('LOT_MIN_SIZE', 200);
defined('LOT_MAX_SIZE') or define('LOT_MAX_SIZE', 700);
defined('LOT_SIZE_INCREMENT') or define('LOT_SIZE_INCREMENT', 100);
Class Model_lot extends CI_Model
{
	function getLots($development_id, $filters = array())
	{
		$this->db->select('lots.*, stages.*, precincts.*, street_names.street_name');
		$this->db->join('street_names', 'street_names.street_name_id = lots.street_name_id', 'left');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('precincts.development_id', $development_id);
		if(isset($filters['status']) && $filters['status']){
			$this->db->where('lots.status', $filters['status']);
		}
		$this->db->order_by('precincts.precinct_number', 'asc');
		$this->db->order_by('stages.stage_number', 'asc');
		$this->db->order_by('lots.lot_number', 'asc');
		$query  = $this->db->get('lots');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function getLotsCountMatches($development_id)
	{
		$this->db->select('stages.*, lots.*, precincts.*, COUNT(house_lot_packages.house_id) AS total_matches');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('house_lot_packages', 'house_lot_packages.lot_id = lots.lot_id AND house_lot_packages.active = 1', 'left');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->order_by('precincts.precinct_number', 'asc');
		$this->db->order_by('stages.stage_number', 'asc');
		$this->db->order_by('lots.lot_number', 'asc');
		$this->db->group_by('lots.lot_id');
		$query  = $this->db->get('lots');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function getLotsByStage($stage_id)
	{
		$this->db->select('lots.*, stages.*, precincts.*');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$query  = $this->db->get_where('lots', array('lots.stage_id' => $stage_id));
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function getLot($lot_id)
	{
		$this->db->select('lots.*, stages.*, precincts.*');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$query  = $this->db->get_where('lots', array('lots.lot_id' => $lot_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row();
		}
		return $result;
	}

	function addLot($form_data, $import = false)
	{
		$this->load->model('Model_user', '', TRUE);

		$current_admin = $this->Model_user->getAdminLoggedIn();
		$fields        = $this->getFields();
		$lot_details   = array();
		foreach($fields as $field_id => $field_details){
			if(isset($form_data[$field_id])){
				$lot_details[$field_id] = $form_data[$field_id];
			}
		}
		if(empty($lot_details['street_name_id'])){
			$lot_details['street_name_id'] = null;
		}
		$changes = array('New Lot '.$form_data['lot_number']);
		
		if($this->db->insert('lots', $lot_details)){
			$lot_id  = $this->db->insert_id();
			$new_lot = $this->getLot($lot_id);
			$this->load->model('Model_change_log', '', TRUE);
			$this->Model_change_log->addChangeLog('Add', 'lot', $changes, $lot_id, $new_lot, $current_admin, $import);
			return $lot_id;
		}
		return false;
	}

	function updateLot($lot_id, $form_data, $import = false)
	{
		$this->load->model('Model_change_log', '', TRUE);
		$this->load->model('Model_user', '', TRUE);

		$current_admin     = $this->Model_user->getAdminLoggedIn();
		$old_lot           = $this->getLot($lot_id);
		$fields            = $this->getFields();
		$lot_details       = array();
		if(!isset($form_data['lot_square_meters']) || !$form_data['lot_square_meters']){
			$form_data['lot_square_meters'] = $form_data['lot_width'] * $form_data['lot_depth'];
		}

		$changes_for_log   = array();
		foreach($fields as $field_id => $field_details){
			if(isset($form_data[$field_id]) && $field_id != 'lot_number' 
				&& ($old_lot->$field_id != $form_data[$field_id] && ($field_details['1'] == 't' || (float)$old_lot->$field_id != (float)$form_data[$field_id]))){
				$lot_details[$field_id] = $form_data[$field_id];

				/* This will create a description for the lots change log, depending on the fields changed */
				$this->load->library('mapovis_lib');
				$changes_for_log[] = $this->mapovis_lib->changeLogNote($field_details['0'], $field_details['1'], $old_lot->$field_id, $form_data[$field_id]);
			}
		}
		if(empty($lot_details)){
			return TRUE;
		}
		$this->db->where('lots.lot_id', $lot_id);
		if($this->db->update('lots', $lot_details)){
			$this->Model_change_log->addChangeLog('Edit', 'lot', $changes_for_log, $lot_id, $old_lot, $current_admin, $import);
			return TRUE;
		}
		return FALSE;
	}

	function updateBasicLot($lot_id, $form_data)
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_change_log', '', TRUE);
		$this->load->model('Model_user', '', TRUE);

		$current_user      = $this->Model_user->getUserLogged();
		$old_lot           = $this->getLot($lot_id);
		$fields            = array(
			'status'          => array('', 't'),
			'price_range_min' => array('Min Price', 'n'),
			'price_range_max' => array('Max Price', 'n'),
			'lot_titled'      => array('Titled', 'n'),
		);
		$lot_details       = array();
		$changes_for_log   = array();
		foreach($fields as $field_id => $field_details){
			if(isset($form_data[$field_id]) && $field_id != 'lot_number' && $old_lot->$field_id != $form_data[$field_id]){
				$lot_details[$field_id] = $form_data[$field_id];

				/* This will create a description for the lots change log, depending on the fields changed */
				$this->load->library('mapovis_lib');
				$changes_for_log[] = $this->mapovis_lib->changeLogNote($field_details['0'], $field_details['1'], $old_lot->$field_id, $form_data[$field_id]);
			}
		}
		if(empty($lot_details)){
			return TRUE;
		}
		$this->db->where('lots.lot_id', $lot_id);
		if($this->db->update('lots', $lot_details)){
			$this->Model_change_log->addChangeLog('Edit', 'lot', $changes_for_log, $lot_id, $old_lot, $current_user);
			$this->Model_development->updateLastUpdated($old_lot->development_id);
			return TRUE;
		}
		return FALSE;
	}

	function updateLotTitleByStage($stage_id, $titled)
	{
		$lot_details = array('lot_titled' => $titled);
		$this->db->where('lots.stage_id', $stage_id);
		return $this->db->update('lots', $lot_details);
	}

	public function deleteLot($lot_id)
	{
		$tables = array('change_logs', 'house_lot_packages', 'lots');
		// delete lot related records
		foreach($tables as $table_name){
			$this->db->where('lot_id', $lot_id);
			$this->db->delete($table_name);
		}
		return TRUE;
	}

	public function getStageLots($stage_id, $filters = array(), $show_sold = TRUE, $order_by = 'street_name')
	{
		$this->db->select('lots.*, stages.*, precincts.*, street_names.*');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('street_names', 'street_names.street_name_id = lots.street_name_id', 'left');
		$this->db->where('stages.stage_id', $stage_id);
		if($show_sold){
			$this->db->where_in('lots.status', array('Available', 'Sold'));
		}
		else{
			$this->db->where('lots.status', 'Available');
		}
		if($order_by == '' || $order_by == 'street_name'){
			$this->db->order_by('street_names.street_name', 'asc');
		}
		$width_filter = $this->getFilterWidthsForQuery($filters);
		$size_filter  = $this->getFilterSizesForQuery($filters);
		$price_filter = $this->getFilterPriceForQuery($filters);
		if($price_filter){
			$this->db->where($price_filter);
		}
		if($width_filter){
			$this->db->where($width_filter);
			$this->db->order_by('lots.lot_width', 'asc');
		}
		if($size_filter){
			$this->db->where($size_filter);
			$this->db->order_by('lots.lot_square_meters', 'asc');
		}
		$this->db->order_by('precincts.precinct_number', 'asc');
		$this->db->order_by('stages.stage_number', 'asc');
		$this->db->order_by('lots.lot_number', 'asc');
		$query  = $this->db->get('lots');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	public function getHouseAndLandLots($development_id, $filters = array())
	{
		$development   = $this->Model_development->getDevelopment($development_id);
		$this->db->select('lots.*, MIN(house_lot_packages.house_lot_price) as min_lot_house_cost', 'stages.stage_id = lots.stage_id');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('house_matches_tmp', 'house_matches_tmp.lot_id = lots.lot_id');
		$this->db->join('houses', 'house_matches_tmp.house_id = houses.house_id');
		$this->db->join('house_lot_packages', 'house_lot_packages.lot_id = lots.lot_id AND house_lot_packages.house_id = houses.house_id', 'left');
		$this->db->join('street_names', 'street_names.street_name_id = lots.street_name_id', 'left');

		$this->db->where('precincts.development_id', $development_id);
		if($development->land_show_sold_lots){
			$this->db->where_in('lots.status', array('Available', 'Sold'));
		}
		else{
			$this->db->where('lots.status', 'Available');
		}

		if((isset($filters['houseland_price_from']) && $filters['houseland_price_from']) ||
			(isset($filters['houseland_price_to']) && $filters['houseland_price_to']) ){
			if(isset($filters['houseland_price_from']) && $filters['houseland_price_from']){
				$this->db->where('(house_lot_packages.house_lot_price) >= ', $filters['houseland_price_from']);
			}
			if(isset($filters['houseland_price_to']) && $filters['houseland_price_to']){
				$max_value = (float)$filters['houseland_price_to'];
				$this->db->where("(house_lot_packages.house_lot_price <= {$max_value} OR house_lot_packages.house_lot_price <= {$max_value})");
			}
		}
		
		$size_filter  = $this->getFilterSizesForQuery($filters);
		if($size_filter){
			$this->db->where($size_filter);
		}

		if(isset($filters['house_bedrooms']) && $filters['house_bedrooms']){
			$this->db->where('houses.house_bedrooms', $filters['house_bedrooms']);
		}
		if(isset($filters['house_bathrooms']) && $filters['house_bathrooms']){
			$this->db->where('houses.house_bathrooms', $filters['house_bathrooms']);
		}
		if(isset($filters['builder_id']) && $filters['builder_id']){
			$this->db->where('houses.house_builder_id', $filters['builder_id']);
		}
		if(isset($filters['house_levels']) && $filters['house_levels']){
			$this->db->where('houses.house_levels', $filters['house_levels']);
		}

		$order_by = isset($filters['order_by'])? $filters['order_by']: 'price';
		switch($order_by){
			case 'builder':
				$this->db->join('builders', 'builders.builder_id = houses.house_builder_id', 'left');
				$this->db->order_by('MIN(house_lot_packages.house_lot_price)', 'asc');
				$this->db->order_by('builders.builder_name', 'asc');
				break;
			case 'size':
				$this->db->order_by('lots.lot_square_meters', 'asc');
				$this->db->order_by('MIN(houses.house_size)', 'asc');
				break;
			case 'price':
			default:
				$this->db->order_by('MIN(house_lot_packages.house_lot_price)', 'asc');
				break;
		}

		$this->db->order_by('lots.lot_number', 'asc');
		$this->db->group_by('lots.lot_id');
		$query  = $this->db->get('lots');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	public function getHouseAndLandHouses($development_id, $filters = array(), $stagesIds = FALSE)
	{
		$this->db->select('houses.*, builders.*, MIN(house_lot_packages.house_lot_price) as min_lot_house_cost', 'stages.stage_id = lots.stage_id');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('house_matches_tmp', 'house_matches_tmp.lot_id = lots.lot_id');
		$this->db->join('houses', 'house_matches_tmp.house_id = houses.house_id');
		$this->db->join('house_lot_packages', 'house_lot_packages.lot_id = lots.lot_id AND house_lot_packages.house_id = houses.house_id');
		$this->db->join('street_names', 'street_names.street_name_id = lots.street_name_id', 'left');
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id', 'left');
		if($stagesIds){
			$this->db->where_in('lots.stage_id', $stagesIds);
		}

		$this->db->where('precincts.development_id', $development_id);
		$this->db->where('lots.status', 'Available');

		if((isset($filters['houseland_price_from']) && $filters['houseland_price_from']) ||
			(isset($filters['houseland_price_to']) && $filters['houseland_price_to']) ){
			if(isset($filters['houseland_price_from']) && $filters['houseland_price_from']){
				$this->db->where('(house_lot_packages.house_lot_price) >= ', $filters['houseland_price_from']);
			}
			if(isset($filters['houseland_price_to']) && $filters['houseland_price_to']){
				$max_value = (float)$filters['houseland_price_to'];
				$this->db->where("(house_lot_packages.house_lot_price <= {$max_value} OR house_lot_packages.house_lot_price <= {$max_value})");
			}
		}
		$size_filter  = $this->getFilterSizesForQuery($filters);
		if($size_filter){
			$this->db->where($size_filter);
		}
		if(isset($filters['house_bedrooms']) && $filters['house_bedrooms']){
			$this->db->where('houses.house_bedrooms', $filters['house_bedrooms']);
		}
		if(isset($filters['house_bathrooms']) && $filters['house_bathrooms']){
			$this->db->where('houses.house_bathrooms', $filters['house_bathrooms']);
		}
		if(isset($filters['builder_id']) && $filters['builder_id']){
			$this->db->where('houses.house_builder_id', $filters['builder_id']);
		}
		if(isset($filters['house_levels']) && $filters['house_levels']){
			$this->db->where('houses.house_levels', $filters['house_levels']);
		}
		$order_by_ascdesc = isset($filters['order_by'])? $filters['order_by']: 'price';
		@list($order_by, $asc_desc) = explode('|', $order_by_ascdesc);
		if($asc_desc != 'desc'){
			$asc_desc = 'asc';
		}
		switch($order_by){
			case 'builder':
				$this->db->order_by('MIN(house_lot_packages.house_lot_price)', 'asc');
				$this->db->order_by('builders.builder_name', $asc_desc);
				break;
			case 'size':
				$this->db->order_by('MIN(houses.house_size)', $asc_desc);
				$this->db->order_by('lots.lot_square_meters', $asc_desc);
				break;
			case 'price':
			default:
				$this->db->order_by('MIN(house_lot_packages.house_lot_price)', $asc_desc);
				break;
		}
		$this->db->order_by('houses.house_name', 'asc');
		$this->db->group_by('houses.house_id');
		$query  = $this->db->get('lots');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function getLotHouses($development_id, $filters = array(), &$ids = array(), $group_by_lot = TRUE, $stagesIds = FALSE)
	{
		$join = ($group_by_lot)? 'left': NULL;
		$this->db->select('lots.*, houses.*, builders.*, house_lot_packages.house_lot_price, house_lot_packages.file_name, house_lot_packages.file_name AS original_file_name, street_names.street_name');
		$this->db->join('house_matches_tmp', 'house_matches_tmp.house_id = houses.house_id', $join);
		$this->db->join('lots', 'house_matches_tmp.lot_id = lots.lot_id', $join);
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id', $join);
		$this->db->join('house_lot_packages', 'houses.house_id = house_lot_packages.house_id AND lots.lot_id = house_lot_packages.lot_id',$join);
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('street_names', 'street_names.street_name_id = lots.street_name_id', 'left');
		$this->db->where('precincts.development_id',$development_id);
		$this->db->where('lots.status', 'Available');
		if($stagesIds){
			$this->db->where_in('lots.stage_id', $stagesIds);
		}
		if(isset($filters['house_bedrooms']) && $filters['house_bedrooms']){
			$this->db->where('houses.house_bedrooms', $filters['house_bedrooms']);
		}
		if(isset($filters['house_bathrooms']) && $filters['house_bathrooms']){
			$this->db->where('houses.house_bathrooms', $filters['house_bathrooms']);
		}
		if(isset($filters['builder_id']) && $filters['builder_id']){
			$this->db->where('houses.house_builder_id', $filters['builder_id']);
		}
		if(isset($filters['house_levels']) && $filters['house_levels']){
			$this->db->where('houses.house_levels', $filters['house_levels']);
		}
		$size_filter  = $this->getFilterSizesForQuery($filters);
		if($size_filter){
			$this->db->where($size_filter);
		}
		if((isset($filters['houseland_price_from']) && $filters['houseland_price_from']) ||
			(isset($filters['houseland_price_to']) && $filters['houseland_price_to']) ){
			if(isset($filters['houseland_price_from']) && $filters['houseland_price_from']){
				$this->db->where('house_lot_packages.house_lot_price >= ', $filters['houseland_price_from']);
			}
			if(isset($filters['houseland_price_to']) && $filters['houseland_price_to']){
				$max_value = (float)$filters['houseland_price_to'];
				$this->db->where("(house_lot_packages.house_lot_price <= {$max_value} OR house_lot_packages.house_lot_price <= {$max_value})");
			}
		}
		$order_by_ascdesc = isset($filters['order_by'])? $filters['order_by']: 'price';
		@list($order_by, $asc_desc) = explode('|', $order_by_ascdesc);
		if($asc_desc != 'desc'){
			$asc_desc = 'asc';
		}
		switch($order_by){
			case 'builder':
				$this->db->order_by('builders.builder_name', $asc_desc);
				break;
			case 'size':
				if($group_by_lot){
					$this->db->order_by('houses.house_size', $asc_desc);
				}
				else{
					$this->db->order_by('lots.lot_square_meters', $asc_desc);
				}
				break;
			case 'price':
			default:
				$this->db->order_by('MIN(house_lot_packages.house_lot_price)', $asc_desc);
				break;
		}
		if(!$group_by_lot){
			$this->db->where('lots.lot_id is not null');
		}
		$this->db->group_by(array('houses.house_id', 'lots.lot_id'));
		$query   = $this->db->get('houses');
		$results = $query? $query->result(): array();
		$ordered_results = array();
		if($group_by_lot){
			foreach($results as $result){
				$ordered_results[$result->lot_id][$result->house_id] = $result;
				$ids[] = $result->house_id;
			}
		}
		else{
			foreach($results as $result){
				$ordered_results[$result->house_id][$result->stage_id][$result->lot_id] = $result;
				$ids[] = $result->lot_id;
			}
		}
		$ids = array_unique($ids);
		return $ordered_results;
	}

	public function getStageMinLotPrice($stage_id, $filters = array())
	{
		$this->db->select('MIN(lots.price_range_min) as min_price');
		$this->db->where('lots.stage_id', $stage_id);
		$this->db->where_in('lots.status', array('Available', 'Sold'));
		$width_filter = $this->getFilterWidthsForQuery($filters);
		$size_filter  = $this->getFilterSizesForQuery($filters);
		if($width_filter){
			$this->db->where($width_filter);
			$this->db->order_by('lots.lot_width', 'asc');
		}
		if($size_filter){
			$this->db->where($size_filter);
			$this->db->order_by('lots.lot_square_meters', 'asc');
		}
		$query  = $this->db->get('lots');
		$result = false;
		if ($query->num_rows() > 0)
		{
			$result = $query->row()->min_price;
		}
		return $result;
	}

	public function landSearchParameters($development_id)
	{
		$this->db->select('MIN(FLOOR(lots.price_range_min/10000))* 10000 AS land_price_min, MAX(CEIL(lots.price_range_max/10000))*10000 AS land_price_max, MIN(FLOOR(lots.lot_width)) AS lot_width_from, MAX(CEIL(lots.lot_width)) AS lot_width_to  ');
		$this->db->select('MIN(FLOOR(lots.lot_square_meters/100))* 100 AS lot_size_min, MAX(CEIL(lots.lot_square_meters/100))*100 AS lot_size_max');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->where_in('lots.status', array('Available'));
		$parameters = $this->db->get('lots')->row_array();
		$parameters['lot_square_meters']      = array();
		$parameters['lot_square_meters_from'] = array();
		$parameters['lot_square_meters_to']   = array();
		$parameters['lot_widths']             = array();
		$parameters['lot_widths_from']        = array();
		$parameters['lot_widths_to']          = array();

		$lot_min_width = $parameters['lot_width_from'];
		$lot_max_width = $parameters['lot_width_to'];
		for($width = $lot_min_width; $width < $lot_max_width; $width += LOT_WIDTH_INCREMENT){
			$next_width                                         = $width + LOT_WIDTH_INCREMENT;
			$parameters['lot_widths']["{$width}|{$next_width}"] = "{$width}m - {$next_width}m";
			$parameters['lot_widths_from'][$width]              = "{$width}m";
			$parameters['lot_widths_to'][$next_width]           = "{$next_width}m";
		}
		if(isset($next_width)){
			$parameters['lot_widths'][$width] = "{$width}m+";
		}
		$lot_min_size = $parameters['lot_size_min'];
		$lot_max_size = $parameters['lot_size_max'];
		for($size = $lot_min_size; $size < $lot_max_size; $size += LOT_SIZE_INCREMENT){
			$next_size                                               = $size + LOT_SIZE_INCREMENT;
			$parameters['lot_square_meters']["{$size}|{$next_size}"] = "{$size}m<sup>2</sup> - {$next_size}m<sup>2</sup>";
			$parameters['lot_square_meters_from'][$size]             = "{$size}m<sup>2</sup>";
			$parameters['lot_square_meters_to'][$next_size]          = "{$next_size}m<sup>2</sup>";
		}
		if(isset($next_size)){
			$parameters['lot_square_meters'][$size] = "{$size}m<sup>2</sup>+";
		}
		return $parameters;
	}

	private function getFilterWidthsForQuery($filters)
	{
		$widths = array();
		if(isset($filters['lot_width']) && !empty($filters['lot_width'])){
			foreach($filters['lot_width'] as $lot_width){
				if(!$lot_width){
					continue;
				}
				@list($min, $max) = explode('|', $lot_width);
				$max_query       = ($max)? " AND lots.lot_width <= {$max}":'';
				$widths[]        = "(lots.lot_width >= {$min} {$max_query})";
			}
			if(count($widths)){
				return '('.implode(' OR ', $widths).')';
			}
		}
		$sql_query  = array();
		if(isset($filters['lot_width_from']) && !empty($filters['lot_width_from'])){
			$sql_query[] = 'lots.lot_width >= '.(float)$filters['lot_width_from'];
		}
		if(isset($filters['lot_width_to']) && !empty($filters['lot_width_to'])){
			$sql_query[] = 'lots.lot_width <= '.(float)$filters['lot_width_to'];
		}
		return (count($sql_query))? implode(' AND ', $sql_query): FALSE;
	}

	private function getFilterSizesForQuery($filters)
	{
		$sizes = array();
		if(isset($filters['lot_square_meters']) && !empty($filters['lot_square_meters'])){
			if(!is_array($filters['lot_square_meters'])){
				$filters['lot_square_meters'] = array($filters['lot_square_meters']);
			}
			foreach($filters['lot_square_meters'] as $lot_size){
				if(!$lot_size){
					continue;
				}
				@list($min, $max) = explode('|', $lot_size);
				$max_query       = ($max)? " AND lots.lot_square_meters <= {$max}":'';
				$sizes[]        = "(lots.lot_square_meters >= {$min} {$max_query})";
			}
			if(count($sizes)){
				return '('.implode(' OR ', $sizes).')';
			}
		}
		$sql_query = array();
		if(isset($filters['lot_square_meters_from']) && !empty($filters['lot_square_meters_from'])){
			$sql_query[] = "lots.lot_square_meters >= {$filters['lot_square_meters_from']}";
		}
		if(isset($filters['lot_square_meters_to']) && !empty($filters['lot_square_meters_to'])){
			$sql_query[] = "lots.lot_square_meters <= {$filters['lot_square_meters_to']}";
		}
		return (count($sql_query))? implode(' AND ', $sql_query): FALSE;
	}

	private function getFilterPriceForQuery($filters)
	{
		$sql_query  = array();
		if(isset($filters['land_price_from']) && !empty($filters['land_price_from'])){
			$sql_query[] = 'lots.price_range_min >= '.(float)$filters['land_price_from'];
		}
		if(isset($filters['land_price_to']) && !empty($filters['land_price_to'])){
			$sql_query[] = 'lots.price_range_max <= '.(float)$filters['land_price_to'];
		}
		return (count($sql_query))? implode(' AND ', $sql_query): FALSE;
	}

	public function getLandSearchResults($development_id, $filters){
		$this->load->library('mapovis_lib');
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);
		$this->load->model('Model_statistics_land', '', TRUE);
		$development   = $this->Model_development->getDevelopment($development_id);
		$active_stages = array();
		$tmpstages     = $this->Model_stage->getStages($development_id);
		foreach($tmpstages as $stage){
			$stage->lots            = $this->getStageLots($stage->stage_id, $filters, $development->land_show_sold_lots, $development->land_order_by);
			$stage->sold_percentage = $this->Model_stage->getStageSoldPercentage($stage->stage_id, $filters);
			/* If the stage does not have any lots or all lots are sold do not display */
			if($stage->sold_percentage == 100 || empty($stage->lots)){
				continue;
			}
			$stage->word_of_number  = $this->mapovis_lib->convert_number_to_words($stage->stage_number);
			$stage->min_price       = $this->getStageMinLotPrice($stage->stage_id, $filters);
			$active_stages[]        = $stage;
		}
		$this->Model_statistics_land->addLog($development_id, $filters);
		return $active_stages;
	}

	public function getHouseAndLandSearchResults($development_id, $filters, $group_by_lot = TRUE, $stagesIds = FALSE){
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_house_image', '', TRUE);
		$this->load->model('Model_statistics_house_land', '', TRUE);

		if($group_by_lot){
			$lots      = $this->getHouseAndLandLots($development_id, $filters);
			$house_ids = array();
			$houses    = $this->getLotHouses($development_id, $filters, $house_ids, $group_by_lot);
			$lot_ids   = (count($lots))? array_map(function($v){return $v->lot_id;}, $lots): array();
		}
		else{
			$houses    = $this->getHouseAndLandHouses($development_id, $filters, $stagesIds);
			$house_ids = array_map(function($v){return $v->house_id;}, $houses);
			$lot_ids   = array();
			$lots      = $this->getLotHouses($development_id, $filters, $lot_ids, $group_by_lot, $stagesIds);
		}
		$images_results = (count($house_ids))? $this->Model_house_image->getFirstHousesImages($house_ids, $lot_ids, array('file_type' => 'facade', 'group_by_lot' => $group_by_lot)): array();
		$houses_images  = array();
		if($group_by_lot){
			foreach($images_results as $image){
				$houses_images[$image->lot_id][$image->house_id] = $image;
			}
		}
		else{
			foreach($images_results as $image){
				$houses_images[$image->house_id] = $image;
			}
		}
		$this->Model_statistics_house_land->addLog($development_id, $filters);
		return array('lots' => $lots, 'houses' => $houses, 'houses_images' => $houses_images);
	}

	public function getHouseAndLandSearchResultsActiveLots($development_id, $filters){
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_house_image', '', TRUE);
		$this->load->model('Model_statistics_house_land', '', TRUE);

		$lot_ids        = array();
		$house_ids      = array();
		$lots           = $this->getActiveLotHouses($development_id, $filters, $lot_ids, $house_ids);
		$development    = $this->Model_development->getDevelopment($development_id);
		
		$houses_images  = array();
		if($development->hnl_multi_images){
			$images_results = (count($lots))? $this->Model_house_image->getAllHousesImages($house_ids, $lot_ids, array('file_type' => 'facade', 'group_by_lot' => FALSE)): array();
			foreach($images_results as $image){
				$houses_images[$image->house_id][] = $image;
			}
		}
		else{
			$images_results = (count($lots))? $this->Model_house_image->getFirstHousesImages($house_ids, $lot_ids, array('file_type' => 'facade', 'group_by_lot' => FALSE)): array();
			foreach($images_results as $image){
				$houses_images[$image->house_id] = $image;
			}
		}
		$this->Model_statistics_house_land->addLog($development_id, $filters);
		return array('lots' => $lots, 'houses_images' => $houses_images);
	}

	function getActiveLotHouses($development_id, $filters, &$lot_ids, &$house_ids)
	{
		$this->db->select('lots.*, houses.*, builders.*, ');
		$this->db->select('house_lot_packages.house_lot_price, house_lot_packages.file_name, house_lot_packages.file_name AS original_file_name, house_lot_packages.description, street_names.street_name');
		$this->db->join('house_lot_packages', 'houses.house_id = house_lot_packages.house_id');
		$this->db->join('lots', 'lots.lot_id = house_lot_packages.lot_id');
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('street_names', 'street_names.street_name_id = lots.street_name_id', 'left');
		$this->db->where('precincts.development_id',$development_id);
		$this->db->where('house_lot_packages.active', 1);
		$this->db->where('lots.status', 'Available');
		$filters_flag = (empty($filters))? FALSE : TRUE;
		if(isset($filters['house_bedrooms']) && $filters['house_bedrooms']){
			$this->db->where('houses.house_bedrooms', $filters['house_bedrooms']);
		}
		if(isset($filters['house_bathrooms']) && $filters['house_bathrooms']){
			$this->db->where('houses.house_bathrooms', $filters['house_bathrooms']);
		}
		if(isset($filters['builder_id']) && $filters['builder_id']){
			$this->db->where('houses.house_builder_id', $filters['builder_id']);
		}
		if(isset($filters['house_levels']) && $filters['house_levels']){
			$this->db->where('houses.house_levels', $filters['house_levels']);
		}
		$size_filter  = $this->getFilterSizesForQuery($filters);
		if($size_filter){
			$this->db->where($size_filter);
		}
		if((isset($filters['houseland_price_from']) && $filters['houseland_price_from']) ||
			(isset($filters['houseland_price_to']) && $filters['houseland_price_to']) ){
			if(isset($filters['houseland_price_from']) && $filters['houseland_price_from']){
				$this->db->where('house_lot_packages.house_lot_price >= ', $filters['houseland_price_from']);
			}
			if(isset($filters['houseland_price_to']) && $filters['houseland_price_to']){
				$max_value = (float)$filters['houseland_price_to'];
				$this->db->where("house_lot_packages.house_lot_price <= {$max_value}");
			}
		}
		if((isset($filters['houseland_price']) && $filters['houseland_price'])){
			@list($min_price, $max_price) = explode('|', $filters['houseland_price']);
			if($min_price){
				$this->db->where('house_lot_packages.house_lot_price >= ', $min_price);
			}
			if($max_price){
				$max_value = (float)$max_price;
				$this->db->where("house_lot_packages.house_lot_price <= {$max_value}");
			}
		}
		if(!$filters_flag){
			$this->db->order_by('house_lot_packages.is_feature', 'desc');
		}
		$order_by_ascdesc = isset($filters['order_by'])? $filters['order_by']: 'price';
		@list($order_by, $asc_desc) = explode('|', $order_by_ascdesc);
		if($asc_desc != 'desc'){
			$asc_desc = 'asc';
		}
		switch($order_by){
			case 'builder':
				$this->db->order_by('builders.builder_name', $asc_desc);
				break;
			case 'size':
				$this->db->order_by('lots.lot_square_meters', $asc_desc);
				break;
			case 'price':
			default:
				$this->db->order_by('MIN(house_lot_packages.house_lot_price)', $asc_desc);
				break;
		}
		$this->db->group_by(array('houses.house_id', 'lots.lot_id'));
		$query      = $this->db->get('houses');
		$lots_houses = $query? $query->result(): array();
		foreach($lots_houses as $result){
			$lot_ids[]   = $result->lot_id;
			$house_ids[] = $result->house_id;
		}
		$lot_ids   = array_unique($lot_ids);
		$house_ids = array_unique($house_ids);
		return $lots_houses;
	}
	public function isUnique($stage_id, $lot_number)
	{
		$this->db->where('lots.stage_id', $stage_id);
		$this->db->where('lots.lot_number', $lot_number);
		$query = $this->db->get('lots');
		if ($query->num_rows() > 0)
		{
			return FALSE;
		}
		return TRUE;
	}

	public function getStatus()
	{
		return array('Available', 'Sold', 'Unlisted', 'Deposited');
	}

	private function getFields()
	{
		return array(
			'stage_id'          => array('Stage', 't'),
			'street_name_id'    => array('Street Name', 't'),
			'lot_number'        => array('Lot Number', 't'),
			'status'            => array('', 't'),
			'price_range_min'   => array('Min Price', 'n'),
			'price_range_max'   => array('Max Price', 'n'),
			'lot_latitude'      => array('Latitude', 'f'),
			'lot_longitude'     => array('Longitude', 'f'),
			'lot_width'         => array('Width', 'f'),
			'lot_depth'         => array('Depth', 'f'),
			'lot_square_meters' => array('Square Meters', 't'),
			'exclude_houses'    => array('Exclude Houses', 't'),
			'creation_date'     => array('Creation Date', 't'),
			'polygon_coords'    => array('Polygon Coords', 't'),
			'lot_corner'        => array('Is a Corner Lot', 'n'),
			'lot_titled'        => array('Titled', 'n'),
			'lot_irregular'     => array('Is an Irregular Lot', 'n'),
		);
	}
}
?>