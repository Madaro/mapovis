<?php
Class Model_reminder extends CI_Model
{
	function getReminder($development_id)
	{
		$this->db->where('reminders.development_id', $development_id);
		$query  = $this->db->get('reminders');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->row(); 
		}
		else{
			$fields                   = $this->getFields();
			$fields['development_id'] = $development_id;
			$result                   = (object)$fields;
		}
		return $result;
	}

	function updateReminder($development_id, $form_data)
	{
		$fields           = $this->getFields();
		$reminder_details = array();
		foreach($fields as $field_id => $fiel_def_val){
			if(isset($form_data[$field_id])){
				$reminder_details[$field_id] = $form_data[$field_id];
			}
		}
		// if reminder already exists update the records if not create new record
		$reminder = $this->getReminder($development_id);
		if(isset($reminder->reminder_id)){
			$this->db->where('reminders.reminder_id', $reminder->reminder_id);
			return $this->db->update('reminders', $reminder_details);
		}
		else{
			$reminder_details['development_id'] = $development_id;
			return $this->db->insert('reminders', $reminder_details);
		}
	}

	public function getOverdueDevelopments()
	{
		$this->load->model('Model_schedule', '', TRUE);
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_user', '', TRUE);
		$this->load->library('mapovis_lib');

		$overdue_developments = array();
		$developments         = $this->Model_development->getDevelopments();
		foreach($developments as $development){
			$development_id = $development->development_id;
			$today          = $this->mapovis_lib->getToday($development->state);
			$next_schedule  = $this->Model_schedule->getNextSchedule($development);
			if(!$next_schedule){
				continue;
			}
			$development->next_schedule = $next_schedule;

			$remaining                  = date_diff($today, $development->next_schedule);
			if($remaining->invert == 1){
				$primary_user                    = $this->Model_user->getUser($development->primary_user);
				$development->next_schedule_date = date_format($development->next_schedule, 'l jS \o\f M, \b\y h:ia');
				$development->remaining_time     = $this->mapovis_lib->timeDiffFormat($remaining);
				$development->overdue_by         = $development->remaining_time;
				$development->primary_user_name  = ($primary_user)? $primary_user->name: '-';
				$overdue_developments[$development_id] = $development;
			}
		}
		return $overdue_developments;
	}

	public function getProblemUsers()
	{
		$this->load->model('Model_schedule', '', TRUE);
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_user', '', TRUE);
		$this->load->library('mapovis_lib');

		$overdue_developments = array();
		$developments         = $this->Model_development->getDevelopments();
		foreach($developments as $development){
			$development_id = $development->development_id;
			$today          = $this->mapovis_lib->getToday($development->state);
			$next_schedule  = $this->Model_schedule->getNextSchedule($development);
			if(!$next_schedule){
				continue;
			}
			$development->next_schedule = $next_schedule;

			$remaining                  = date_diff($today, $development->next_schedule);
			$reminder_details           = $this->getReminder($development_id);
			$total_hours_overdue        = $remaining->days * 24 + $remaining->h;
			if($remaining->invert == 1 && $reminder_details->level_4_e <= $total_hours_overdue){
				$primary_user                    = $this->Model_user->getUser($development->primary_user);
				$development->next_schedule_date = date_format($development->next_schedule, 'l jS \o\f M, \b\y h:ia');
				$development->remaining_time     = $this->mapovis_lib->timeDiffFormat($remaining);
				$development->overdue_by         = $development->remaining_time;
				$development->primary_user_name  = ($primary_user)? $primary_user->name: '-';
				$overdue_developments[$development_id] = $development;
			}
		}
		return $overdue_developments;
	}

	private function getFields()
	{
		return array(
			'development_id' => null,
			'level_1_a'      => 1,
			'level_2_b'      => 17,
			'level_2_c'      => 3,
			'level_3_d'      => 72,
			'level_4_e'      => 96,
		);
	}


/***************************************** EMAIL AND SMS NOTIFICATIONS *************************************************/
/**
 * The following functions will validate and send Email and SMS notifications
 * sendRemindersAllDevelopmentsCicleTest($development_id, $number_hours): this function is used to test the script so a period of time can be run
 * sendRemindersAllDevelopments(): This function is called regularly to send notifications to the developments that require it.
 * sendReminders($level, $development, $today): this function contains the templates name and SMS message for each level and who should be notify (primary, secondary user or manager)
 */	

	/**
	 * This function simulates the SMS and Email notifications
	 * Based on the number of hours provided it will iterate from now until the number of hours are completed, 30 minutes on evey check
	 * @param int $development_id
	 * @param int $number_hours
	 * @return boolean
	 */
	public function sendRemindersAllDevelopmentsCicleTest($development_id, $number_hours)
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_reminder_log', '', TRUE);
		$this->load->model('Model_user', '', TRUE);
		$this->load->library('mapovis_lib');
		$this->load->library('email_sender');
		$this->load->library('sms_sender');

		$tmp_development = $this->Model_development->getDevelopment($development_id);
		if(!$tmp_development){
			// invalid development
			return FALSE;
		}

		// Important: mapovis_lib->todaystatic will allow the system to the date store in mapovis_lib->today instead of getting it from time()
		$this->mapovis_lib->todaystatic = true;
		$today           = $this->mapovis_lib->getToday($tmp_development->state);

		echo "Running notifications for Development: {$tmp_development->development_name} - Development State {$tmp_development->state}<br />";
		echo 'Today time: '.date_format($today, 'l jS \o\f M, \b\y h:ia').'<br />';

		$total_minutes   = $number_hours*60;
		for($time_counter = 0; $time_counter <= $total_minutes; $time_counter += 30){

			$this->prepareDataAndSendReminder($tmp_development);

			// Increment TODAY by 30 minutes 
			$this->mapovis_lib->today = $this->mapovis_lib->today->add(date_interval_create_from_date_string('30 minutes'));

			// get again the devleopment, so the latest data will be used for next iteration
			$tmp_development = $this->Model_development->getDevelopment($development_id);
		}
	}

	/**
	 * Runs the script for all current developments to send notifications if necessary
	 */
	public function sendRemindersAllDevelopments()
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_reminder_log', '', TRUE);
		$this->load->model('Model_user', '', TRUE);
		$this->load->library('mapovis_lib');
		$this->load->library('email_sender');
		$this->load->library('sms_sender');

		$developments = $this->Model_development->getDevelopments();
		foreach($developments as $tmp_development){
			$this->prepareDataAndSendReminder($tmp_development);
		}
	}

	/**
	 * this function gets the next schedule_date details (date, overdue/outcoming) and uses the function needSendReminder 
	 * to know if a notification should be sent if that is the case it calls sendReminders which is in charge of sending 
	 * reminders for all levels and Email and SMS
	 * @param object $tmp_development
	 */
	private function prepareDataAndSendReminder($tmp_development)
	{
		$today            = $this->mapovis_lib->getToday($tmp_development->state);
		$development      = $this->getDevelopmentReminderTimes($tmp_development);
		$reminder_details = $this->getReminder($development->development_id);
		$level            = $this->getLevel($development, $development->remaining_hours, $reminder_details);
		if($this->needSendReminder($level, $development, $today, $reminder_details)){
			$this->sendReminders($level, $development, $today);
		}
	}

	/**
	 * Obtains the next schedule date, for given development, the remaining time, 
	 * if it's overdue or upcoming, if there are not schedules in the database yet the values will be empty or FALSE
	 * @param object $development
	 * @return object the development with the new data
	 */
	private function getDevelopmentReminderTimes($development)
	{
		$today                           = $this->mapovis_lib->getToday($development->state);
		$development->next_schedule      = $this->Model_schedule->getNextSchedule($development);
		// default data
		$development->next_schedule_date = '';
		$development->remaining_time     = '';
		$development->overdue            = FALSE;
		$development->upcoming           = FALSE;
		$development->remaining_hours    = FALSE;
		$development->deadline           = '';

		// if there is a schedule determine if is overdue, remaining hours, upcoming, etc
		if($development->next_schedule){
			$remaining                       = date_diff($today, $development->next_schedule);
			$remaining_hours                 = $remaining->days * 24 + $remaining->h;
			$development->next_schedule_date = date_format($development->next_schedule, 'l jS \o\f M, h:ia');
			$development->deadline           = date_format($development->next_schedule, 'l jS h:ia');
			$development->remaining_time     = $this->mapovis_lib->timeDiffFormat($remaining);
			$development->remaining_hours    = $remaining_hours;
			if($remaining->invert == 1){
				$development->overdue  = TRUE;
			}
			elseif($remaining_hours <= 8){
				$development->upcoming = TRUE;
			}
		}
		return $development;
	}

	/**
	 * returns the overdue level based on the hours between Today and the schedule date, if is not overdue it returns 0 (upcoming)
	 * @param object $development
	 * @param in $total_hours
	 * @param object $reminder_details
	 * @return int
	 */
	private function getLevel($development, $total_hours, $reminder_details)
	{
		if($development->overdue && $total_hours != 0){
			switch($total_hours){
				case $reminder_details->level_4_e <= $total_hours:
					$level    = 4;
					break;
				case $reminder_details->level_3_d <= $total_hours:
					$level    = 3;
					break;
				case $reminder_details->level_2_b <= $total_hours:
					$level    = 2;
					break;
				case $reminder_details->level_1_a <= $total_hours:
				default:
					$level    = 1;
					break;
			}
		}
		else{
			$level    = 0;
		}
		return $level;
	}

	/**
	 * Returns the hours set for given level
	 * @param int $level
	 * @param object $reminder_details
	 * @return int
	 */
	private function getLevelHours($level, $reminder_details)
	{
		$total_hours = 0;
		switch($level){
			case 1:
				$total_hours = $reminder_details->level_1_a;
				break;
			case 2:
				$total_hours = $reminder_details->level_2_b;
				break;
			case 3:
				$total_hours = $reminder_details->level_3_d;
				break;
			case 4:
				$total_hours = $reminder_details->level_4_e;
				break;
		}
		return $total_hours;
	}

	/**
	 * Checks if a development needs to be udpate and a notification sent based on:
	 * schedule date, when was the last reminder sent, the reminder level, etc
	 * @param int $level
	 * @param object $development
	 * @param dateTime $today
	 * @param object $reminder_details
	 * @return boolean
	 */
	private function needSendReminder($level, $development, $today, $reminder_details)
	{
		if(($development->overdue || $development->upcoming) && $development->remaining_hours){
			$timezone      = $this->mapovis_lib->getTimeZone($development->state);
			$next_schedule = $development->next_schedule;
			$last_reminder = date_create(date('Y-m-d H:i:s', strtotime($development->last_reminder)));
			$last_reminder->setTimeZone(timezone_open($timezone));

			if($level == 0){
				$schedule_date           = $next_schedule->sub(date_interval_create_from_date_string('8 hours'));
			}
			else{
				$level_hours = $this->getLevelHours($level, $reminder_details);
				if($level_hours){
					$schedule_date = $next_schedule->add(date_interval_create_from_date_string($level_hours.' hours'));
				}
				else{
					$schedule_date = $next_schedule;
				}
				$diff_today_schedule     = date_diff($today, $schedule_date);
			}
			$diff_remind_last_remind = date_diff($schedule_date, $last_reminder);
			$diff_today_schedule     = date_diff($today, $schedule_date);

			if($diff_today_schedule->invert == 1 && $diff_remind_last_remind->invert == 1){
				return TRUE;
			}
			elseif($level == 2 && $reminder_details->level_2_c){
				$next_reminder          = $last_reminder->add(date_interval_create_from_date_string($reminder_details->level_2_c.' hours'));
				$level_3_reminder       = ($reminder_details->level_3_d)
										? $schedule_date->add(date_interval_create_from_date_string(($reminder_details->level_3_d-$level_hours).' hours'))
										: $schedule_date;
				$diff_today_last_remind = date_diff($today, $next_reminder);

				$reminder_day_of_week   = date_format($today, 'N');
				$reminder_hour          = date_format($today, 'G');
				$in_weekday             = $reminder_day_of_week >= 1 && $reminder_day_of_week <= 5;
				$between_9_and_9        = $reminder_hour >= 9 && $reminder_hour < 18;
				$next_level_date        = date_format($level_3_reminder, 'Y-m-d H:i:s');
				$next_reminder_date     = date_format($today, 'Y-m-d H:i:s');

				// exclude weekends and only from 9am to 9pm
				if($diff_today_last_remind->invert == 1 && $in_weekday && $between_9_and_9 && strtotime($next_reminder_date) < strtotime($next_level_date)){
					return TRUE;
				}
			}
		}
		return FALSE;
	}

	/**
	 * Organizes the data to know who should be notify and what method should be used
	 * @param int $level
	 * @param object $development
	 * @param DateFormat $today
	 */
	private function sendReminders($level, $development, $today)
	{
		$this->load->model('Model_user', '', TRUE);
		$primary_user   = $this->Model_user->getUser($development->primary_user);
		$secondary_user = $this->Model_user->getUser($development->secondary_user);
		$manager_user   = $this->Model_user->getUser($development->manager_user);

		$hours_overdue                = $development->remaining_hours.' hour'.($development->remaining_hours > 1? 's': '');
		$development->primaryuser     = ($primary_user)? $primary_user->name: '-';
		$development->secondaryuser   = ($secondary_user)? $secondary_user->name: '-';
		$development->manageruser     = ($manager_user)? $manager_user->name: '-';
		$development->manager         = ($manager_user)? $manager_user->name: '-';
		$development->developmentname = $development->development_name;
		$development->overduehours    = $development->remaining_hours;
		$development->updatedeadline  = $development->next_schedule_date;
		$development->duedate         = $development->next_schedule_date;
		$development->timeoverdue     = $hours_overdue;
		$subject                      = "MAPOVIS - {$development->development_name} is {$hours_overdue} overdue";
		$tags                         = 'Development {:developmentname:} is overdue';

		/* Convert today's date to AEST to store in the database */
		$today->setTimeZone(timezone_open('Australia/NSW'));
		$today_datetime = date_format($today, 'Y-m-d H:i:s');

		switch($level){
			case 1:
				$notifications = array(
					array(
						'user'    => $primary_user,
						'methods' => array(
							'SMS'   => '{:primaryuser:}, the MAPOVIS scheduled update of {:developmentname:} is {:overduehours:} hrs overdue. Please log in & update the development ASAP.',
							'Email' => 'warninglevel1-primaryuser'
						)
					),
				);
				break;
			case 2:
				$notifications = array(
					array(
						'user'    => $primary_user,
						'methods' => array(
							'SMS'   => '{:primaryuser:}, the MAPOVIS scheduled update of {:developmentname:} has still not been completed. It is now {:overduehours:} hrs overdue.',
							'Email' => 'warninglevel2-primaryuser'
						)
					),
					array(
						'user'    => $secondary_user,
						'methods' => array(
							'SMS'   => '{:secondaryuser:}, the MAPOVIS scheduled update of {:developmentname:} has not been completed by {:primaryuser:} & is {:overduehours:} hrs overdue. ',
							'Email' => 'warninglevel2-secondaryuser'
						)
					)
				);
				break;
			case 3:
				$notifications = array(
					array(
						'user'    => $primary_user,
						'methods' => array(
							'SMS'   => '{:primaryuser:}, the MAPOVIS scheduled update of {:developmentname:} has not been completed & is {:overduehours:} hrs overdue. {:manageruser:} has been contacted.',
							'Email' => 'warninglevel3-primaryuser'
						)
					),
					array(
						'user'    => $secondary_user,
						'methods' => array(
							'SMS'   => '{:secondaryuser:}, the MAPOVIS scheduled update of {:developmentname:} has not been completed & is {:overduehours:} hrs overdue. {:manageruser:} has been contacted.',
							'Email' => 'warninglevel3-secondaryuser'
						)
					),
					array(
						'user'    => $manager_user,
						'methods' => array(
							'SMS'   => '{:manageruser:}, the MAPOVIS scheduled update of {:developmentname:} is {:overduehours:} hrs overdue. Please contact {:primaryuser:} & {:secondaryuser:}.',
							'Email' => 'warninglevel3-manager'
						)
					)
				);
				break;
			case 4:
				$notifications = array(
					array(
						'user'    => (object)array('user_id' => '0', 'email' => 'problemusers@mapovis.com.au', 'name' => 'Problem user'),
						'methods' => array(
							'Email' => 'problemuser-admin'
						)
					),
				);
				break;
			case 0:
			default:
				$subject       = "MAPOVIS - {$development->development_name} Update Required by {$development->duedate}";
				$tags          = 'Development {:developmentname:} required to be update ';
				$notifications = array(
					array(
						'user'    => $primary_user,
						'methods' => array(
							'SMS'   => '{:primaryuser:}, {:developmentname:} needs to be updated by {:deadline:}. ',
							'Email' => 'updaterequiredtoday-primaryuser'
						)
					),
				);
				break;
		}
		$development->title           = $subject;

		foreach($notifications as $notification){
			$user_details   = $notification['user'];
			if(!$user_details){
				// ignore if the user could not be retrieved (not defined yet)
				continue;
			}
			if(isset($notification['methods']['SMS'])){
				$this->sendSmsNotification($level, $user_details, $development, $notification['methods']['SMS'], $today_datetime);
			}
			if(isset($notification['methods']['Email'])){
				$this->sendEmailNotification($level, $user_details, $development, $subject, $notification['methods']['Email'], $tags, $today_datetime);
			}
		}
		// update last reminder sent in the development
		$this->Model_development->updateLastReminder($development->development_id, $today_datetime);
	}

	/**
	 * Sends Email notificatios and records a reminder log for future reference
	 * @param int $level
	 * @param object $user
	 * @param object $development
	 * @param string $subject
	 * @param string $template_name
	 * @param string $tags
	 * @param dateTime $today
	 */
	private function sendEmailNotification($level, $user, $development, $subject, $template_name, $tags, $today)
	{
		$this->load->model('Model_config', '', TRUE);
		$this->load->library('email_sender');
		$settings = $this->Model_config->getConfig();

		$from      = (object)array('email' => $settings->support_email, 'name' => 'MAPOVIS');
		$log_notes = '';
		$sent      = $this->email_sender->sendEmailNotificaton($subject, $template_name, $from, $user, $development, $tags, $log_notes);
		if(is_array($log_notes)){
			$log_notes = implode("\n", current($log_notes));
		}
		$this->Model_reminder_log->addReminderLog($development->development_id, $level, 'Email', $user->user_id, $sent, $log_notes, $today);
	}

	/**
	 * Sends SMS notificatios and records a reminder log for future reference
	 * @param int $level
	 * @param object $user
	 * @param object $development
	 * @param string $message
	 * @param dateTime $today
	 */
	private function sendSmsNotification($level, $user, $development, $message, $today)
	{
		$this->load->library('sms_sender');
		$log_notes = '';
		$sent      = $this->sms_sender->sendSms($message, $development, $user->mobilephone, $log_notes);
		$this->Model_reminder_log->addReminderLog($development->development_id, $level, 'SMS', $user->user_id, $sent, $log_notes, $today);
	}

}
?>