<?php
Class Model_amenity extends CI_Model
{
	function getAmenities($development_id)
	{
		$query  = $this->db->get_where('amenities', array('development_id' => $development_id));
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result(); 
		}
		return $result;
	}

	function getAmenity($amenity_id)
	{
		$query  = $this->db->get_where('amenities', array('amenity_id' => $amenity_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row();
		}
		return $result;
	}

	function addAmenity($form_data)
	{
		$fields      = $this->getFields();
		$amenity_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$amenity_details[$field] = $form_data[$field];
			}
		}
		$this->db->insert('amenities', $amenity_details);
		$amenity_id = $this->db->insert_id();
		if($amenity_id){
			$amenity_image_details = array();
			$new_file_path         = BASEPATH."../../mpvs/images/amenities/";
			if (!file_exists("{$new_file_path}amenity_{$amenity_id}/")) {
				mkdir("{$new_file_path}amenity_{$amenity_id}/", 0777, true);
			}	
			for($x=1; $x <=5; $x++){
				if(!isset($form_data['image_names'][$x-1])){
					break;
				}
				$image_url    = $form_data['image_names'][$x-1];
				$file_name    = "amenity_{$amenity_id}/".basename($image_url);
				/* Validate the file name to make sure is unique and is not overwritten  */
				$new_filename = $this->uniqueFileName($new_file_path, $file_name);

				@file_put_contents($new_file_path.$new_filename, file_get_contents($image_url));
				$form_data['image_names'][$x-1] = $new_filename;
				$amenity_image_details['amenity_picture'.$x] = (isset($form_data['image_names'][$x-1]))? $form_data['image_names'][$x-1]: '';
			}
			if(count($amenity_image_details)){
				$this->db->where('amenities.amenity_id', $amenity_id);
				$this->db->update('amenities', $amenity_image_details);
			}
		}
		return $amenity_id;
	}

	function updateAmenity($amenity_id, $form_data)
	{
		$fields          = $this->getFields();
		$amenity_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$amenity_details[$field] = $form_data[$field];
			}
		}
		$new_file_path = BASEPATH."../../mpvs/images/amenities/";
		if (!file_exists("{$new_file_path}amenity_{$amenity_id}/")) {
			mkdir("{$new_file_path}amenity_{$amenity_id}/", 0777, true);
		}	
		for($x=1; $x <=5; $x++){
			if(isset($form_data['image_names'][$x-1]) && filter_var($form_data['image_names'][$x-1], FILTER_VALIDATE_URL)){
				$image_url    = $form_data['image_names'][$x-1];
				$file_name    = "amenity_{$amenity_id}/".basename($image_url);
				/* Validate the file name to make sure is unique and is not overwritten  */
				$new_filename = $this->uniqueFileName($new_file_path, $file_name);
				
				file_put_contents($new_file_path.$new_filename, file_get_contents($image_url));
				$form_data['image_names'][$x-1] = $new_filename;
			}
			$amenity_details['amenity_picture'.$x] = (isset($form_data['image_names'][$x-1]))? $form_data['image_names'][$x-1]: '';
		}
		$this->db->where('amenity_id', $amenity_id);
		return $this->db->update('amenities', $amenity_details);
	}

	function updateAmenitySales($amenity_id, $form_data)
	{
		$this->load->model('Model_development', '', TRUE);
		$amenity         = $this->getAmenity($amenity_id);
		$media_agency    = $this->Model_development->isMediaAGency($amenity->development_id);
		$fields          = array('amenity_name', 'amenity_description', 'amenity_moreinfo_url', 'amenity_iframe_scrollbar');
		if($media_agency){
			$fields[] = 'amenity_iframe_html';
		}
		$amenity_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$amenity_details[$field] = $form_data[$field];
			}
		}
		$new_file_path = BASEPATH."../../mpvs/images/amenities/";
		if (!file_exists("{$new_file_path}amenity_{$amenity_id}/")) {
			mkdir("{$new_file_path}amenity_{$amenity_id}/", 0777, true);
		}	
		for($x=1; $x <=5; $x++){
			if(isset($form_data['image_names'][$x-1]) && filter_var($form_data['image_names'][$x-1], FILTER_VALIDATE_URL)){
				$image_url    = $form_data['image_names'][$x-1];
				$file_name    = "amenity_{$amenity_id}/".basename($image_url);
				/* Validate the file name to make sure is unique and is not overwritten  */
				$new_filename = $this->uniqueFileName($new_file_path, $file_name);
				
				file_put_contents($new_file_path.$new_filename, file_get_contents($image_url));
				$form_data['image_names'][$x-1] = $new_filename;
			}
			$amenity_details['amenity_picture'.$x] = (isset($form_data['image_names'][$x-1]))? $form_data['image_names'][$x-1]: '';
		}

		$this->db->where('amenity_id', $amenity_id);
		return $this->db->update('amenities', $amenity_details);
	}

	function deleteAmenityImage($amenity_id, $delete_image_name)
	{
		$amenity            = $this->getAmenity($amenity_id);
		$new_images_order = array();
		$update_images    = false;
		
		for($x=1; $x <=5; $x++){
			$field_name_old     = 'amenity_picture'.$x;
			if($update_images){
				$field_name_new                    = 'amenity_picture'.($x-1);
				$new_images_order[$field_name_new] = (string)$amenity->$field_name_old;
				continue;
			}
			$current_image_val  = $amenity->$field_name_old;
			if($delete_image_name == $current_image_val){
				$new_images_order[$field_name_old] = '';
				$update_images                     = true;
				$new_images_order['amenity_picture5']  = '';
				// remove the image from folder
				$image_path = BASEPATH."../../mpvs/images/amenities/amenity_{$amenity->amenity_id}/{$delete_image_name}";
				if(file_exists($image_path)){
					unlink($image_path);
				}
			}
		}
		if(count($new_images_order)){
			$this->db->where('amenities.amenity_id', $amenity_id);
			return $this->db->update('amenities', $new_images_order);
		}
		return FALSE;
	}

	function deleteAmenity($amenity_id)
	{
		$this->db->where('amenity_id', $amenity_id);
		return $this->db->delete('amenities');
	}

	function getFields()
	{
		return array(
			'development_id',
			'amenity_type_id',
			'amenity_name',
			'amenity_latitude',
			'amenity_longitude',
			'amenity_description',
			'amenity_moreinfo_url',
			'amenity_iframe_html',
			'amenity_iframe_scrollbar',
			'amentiy_polygon_coords',
		);
	}

	private function uniqueFileName($file_path, $file_name)
	{
		$counter       = 1;
		$new_file_name = $file_name;
		while (file_exists($file_path.$new_file_name)) {
			list($name, $extension) = explode('.', $file_name);
			$new_file_name = "{$name}({$counter}).{$extension}";
			$counter++;
		}
		return $new_file_name;
	}
}
?>