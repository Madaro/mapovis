<?php
Class Model_precinct extends CI_Model
{
	function getPrecincts($development_id)
	{
		$this->db->order_by('precinct_number', 'asc');
		$query  = $this->db->get_where('precincts', array('precincts.development_id' => $development_id));
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function getPrecinct($precinct_id)
	{
		$query  = $this->db->get_where('precincts', array('precincts.precinct_id' => $precinct_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row();
		}
		return $result;
	}

	function addPrecinct($development_id, $precinct_number)
	{
		$data = array('development_id' => $development_id, 'precinct_number' => $precinct_number);
		$this->db->insert('precincts', $data);
		return $this->db->insert_id();
	}

	public function deletePrecinct($precinct_id)
	{
		$this->load->model('Model_stage', '', TRUE);
		$precinct   = $this->getPrecinct($precinct_id);
		$stages     = $this->Model_stage->getStages($precinct->development_id, $precinct_id);
		$stages_ids = array();
		foreach($stages as $stage){
			$stages_ids[] = $stage->stage_id;
		}
		if(count($stages_ids)){
			// delete lots
			$this->db->where_in('stage_id', $stages_ids);
			$this->db->delete('lots');
		}
		
		$tables = array('change_logs', 'stages', 'precincts');
		// delete precinct related records
		foreach($tables as $table_name){
			$this->db->where('precinct_id', $precinct_id);
			$this->db->delete($table_name);
		}
		return TRUE;
	}

	function isUnique($development_id, $precinct_number)
	{
		$this->db->where('precincts.development_id', $development_id);
		$this->db->where('precincts.precinct_number', $precinct_number);
		$query = $this->db->get('precincts');
		if ($query->num_rows() > 0)
		{
			return FALSE;
		}
		return TRUE;
	}
}
?>