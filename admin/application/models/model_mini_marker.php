<?php
Class Model_mini_marker extends CI_Model
{
	function getMiniMarkers($development_id)
	{
		$this->db->select('mapovis_mini_markers.*');
		$this->db->where('development_id', $development_id);
		$query  = $this->db->get('mapovis_mini_markers');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result(); 
		}
		return $result;
	}

	function getMiniMarker($mini_marker_id)
	{
		$query  = $this->db->get_where('mapovis_mini_markers', array('id' => $mini_marker_id));
		return ($query->num_rows() > 0)? $query->row(): null;
	}

	function addMiniMarker($development_id, $form_data)
	{
		$fields       = $this->getFields();
		$data_details = array('development_id' => $development_id);
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$data_details[$field] = $form_data[$field];
			}
		}
		return $this->db->insert('mapovis_mini_markers', $data_details);
	}

	function updateMiniMarker($mini_marker_id, $form_data)
	{
		$fields       = $this->getFields();
		$data_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$data_details[$field] = $form_data[$field];
			}
		}
		if(count($data_details)){
			$this->db->where('id', $mini_marker_id);
			return $this->db->update('mapovis_mini_markers', $data_details);
		}
		return TRUE;
	}

	function deleteMiniMarker($mini_marker_id)
	{
		$this->db->where('id', $mini_marker_id);
		return $this->db->delete('mapovis_mini_markers');
	}

	function getFields()
	{
		return array(
			'marker_longitude',
			'marker_latitude',
			'marker_image_url',
			'marker_height',
			'marker_width',
			'marker_zoomlevel_show_from',
			'marker_zoomlevel_show_to',
			'marker_link',
			'marker_link_target',
		);
	}

}
?>