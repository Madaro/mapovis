<?php
Class Model_builder_user extends CI_Model
{
	public function getAllBuilderUsers()
	{
		$this->db->join('users', 'users.user_id = builder_users.user_id');
		$query        = $this->db->get('builder_users');
		$results      = ($query)? $query->result(): array();
		$ordered_list = array();
		foreach($results as $result){
			$ordered_list[$result->builder_id][$result->user_id] = $result;
		}
		return $ordered_list;
	}

	public function getBuilderUsersPossible($builder_id)
	{
		$this->db->select('users.*, builder_users.builder_user_id, builder_users.builder_id');
		$this->db->join('builder_users', 'users.user_id = builder_users.user_id', 'left');
		$this->db->where('users.name != \'\' ', NULL, FALSE);
		$this->db->where('users.user_role', 'Builder');
		$this->db->where('(builder_users.builder_id = "'.$builder_id.'" OR builder_users.builder_id is null)');
		$query  = $this->db->get('users');
		$results      = ($query->num_rows() > 0)? $query->result(): array();
		$ordered_list = array();
		foreach($results as $result){
			$ordered_list[$result->user_id] = $result;
		}
		return $ordered_list;
	}

	public function addBuilderUser($builder_id, $user_id)
	{
		$this->db->where('builder_users.builder_id', $builder_id);
		$this->db->where('builder_users.user_id', $user_id);
		$query  = $this->db->get('builder_users');
		$result = $query->row();
		if(!$result){
			$user = array('user_id' => $user_id, 'builder_id' => $builder_id);
			return $this->db->insert('builder_users', $user);
		}
		return TRUE;
	}

	public function deleteBuilderUser($user_id)
	{
		$this->db->where('builder_users.user_id', $user_id);
		return $this->db->delete('builder_users');
	}

	public function getLoggedUserBuilder($builder_user_id = null)
	{
		if(!$builder_user_id){
			$this->load->model('Model_user', '', TRUE);
			$current_user    = $this->Model_user->getBuilderLogged();
			$builder_user_id = $current_user->user_id;
		}
		$this->db->join('builder_users', 'builder_users.builder_id = builders.builder_id');
		$this->db->where('builder_users.user_id', $builder_user_id);
		$query  = $this->db->get('builders');
		return  ($query)? $query->row(): FALSE;
	}

}
?>