<?php
Class Model_radius_marker extends CI_Model
{
	function getRadiusMarkers($development_id)
	{
		$this->db->select('radius_markers.*');
		$this->db->where('development_id', $development_id);
		$query  = $this->db->get('radius_markers');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result(); 
		}
		return $result;
	}

	function getRadiusMarker($radius_marker_id)
	{
		$query  = $this->db->get_where('radius_markers', array('radius_marker_id' => $radius_marker_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row(); 
		}
		return $result;
	}

	function addRadiusMarker($development_id, $form_data, $alldata = FALSE)
	{
		if($alldata){
			$data_details                   = $form_data;
			$data_details['development_id'] = $development_id;
		}
		else{
			$fields       = $this->getFields();
			$data_details = array('development_id' => $development_id);
			foreach($fields as $field){
				if(isset($form_data[$field])){
					$data_details[$field] = $form_data[$field];
				}
			}
		}
		return $this->db->insert('radius_markers', $data_details);
	}

	function updateRadiusMarker($radius_marker_id, $form_data)
	{
		$fields       = $this->getFields();
		$data_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$data_details[$field] = $form_data[$field];
			}
		}
		if(count($data_details)){
			$this->db->where('radius_marker_id', $radius_marker_id);
			return $this->db->update('radius_markers', $data_details);
		}
		return TRUE;
	}

	function deleteRadiusMarker($radius_marker_id)
	{
		$this->db->where('radius_marker_id', $radius_marker_id);
		return $this->db->delete('radius_markers');
	}

	function getFields()
	{
		return array(
			'icon',
			'longitude',
			'latitude'
		);
	}

}
?>