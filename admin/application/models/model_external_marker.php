<?php
Class Model_external_marker extends CI_Model
{
	function getExternalMarkers($development_id)
	{
		$this->db->select('external_markers.*');
		$this->db->where('development_id', $development_id);
		$query  = $this->db->get('external_markers');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result(); 
		}
		return $result;
	}

	function getExternalMarker($external_marker_id)
	{
		$query  = $this->db->get_where('external_markers', array('id' => $external_marker_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row(); 
		}
		return $result;
	}

	function addExternalMarker($development_id, $form_data, $alldata = FALSE)
	{
		if($alldata){
			$data_details                   = $form_data;
			$data_details['development_id'] = $development_id;
		}
		else{
			$fields       = $this->getFields();
			$data_details = array('development_id' => $development_id);
			foreach($fields as $field){
				if(isset($form_data[$field])){
					$data_details[$field] = $form_data[$field];
				}
			}
		}
		return $this->db->insert('external_markers', $data_details);
	}

	function updateExternalMarker($external_marker_id, $form_data)
	{
		$fields       = $this->getFields();
		$data_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$data_details[$field] = $form_data[$field];
			}
		}
		if(count($data_details)){
			$this->db->where('id', $external_marker_id);
			return $this->db->update('external_markers', $data_details);
		}
		return TRUE;
	}

	function deleteExternalMarker($external_marker_id)
	{
		$this->db->where('id', $external_marker_id);
		return $this->db->delete('external_markers');
	}

	function getFields()
	{
		return array(
			'icon_url',
            'name',
			'longitude',
			'latitude',
            'iframe_url',
            'show_at_zoom_level',
            'hide_at_zoom_level',
            'icon_width',
            'icon_height'
		);
	}

}
?>