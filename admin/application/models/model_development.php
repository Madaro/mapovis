<?php
Class Model_development extends CI_Model
{
	var $cache_dev = array();

	public function getDevelopments($active = NULL)
	{
		$this->db->select('developments.*, developers.*');
		$this->db->join('developers', 'developments.developerid = developers.developer_id', 'left');
		if($active !== NULL){
			$this->db->where('active', $active);
		}
		$this->db->order_by('development_name', 'asc');
		$query  = $this->db->get('developments');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	public function getDevelopmentsByDeveloper($developer_id)
	{
		$this->db->select('developments.*, developers.background_colour, developers.foreground_colour');
		$this->db->join('developers', 'developments.developerid = developers.developer_id');
		$this->db->where('developments.developerid', $developer_id);
		$this->db->order_by('development_name', 'asc');
		$query  = $this->db->get('developments');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result_array();
		}
		return $result;
	}

	public function getMyDevelopments($user_id, $order_index = FALSE, $schedules = FALSE)
	{
		$this->db->select('developments.*');
		$this->db->join('development_general_users', 'development_general_users.development_id = developments.development_id', 'left');
		$this->db->order_by('development_name', 'asc');
		$this->db->where("(development_general_users.general_user_id = {$user_id} OR developments.primary_user = {$user_id} OR developments.secondary_user = {$user_id} OR developments.manager_user = {$user_id} OR developments.media_agency = {$user_id} )");
		$this->db->group_by('developments.development_id');
		$query   = $this->db->get('developments');
		$results = array();
		if ($query->num_rows() > 0)
		{
			$results = $query->result();
		}
		if($order_index || $schedules){
			$this->load->model('Model_schedule', '', TRUE);
			$tmp_results = array();
			foreach($results as $result){
				if($schedules){
					$result->formated_schedule = $this->Model_schedule->getFormatedSchedule($result->development_id);
				}
				$tmp_results[$result->development_id] = $result;
			}
			$results = $tmp_results;
		}
		return $results;
	}

	public function getDevelopment($development_id)
	{
		if(!isset($this->cache_dev[$development_id])){
			$this->db->select('developments.*, developers.*,multiple_developments_cluster_circles.*, update_messages.*, client_website_links.*, lot_icons.*,');
            $this->db->join('multiple_developments_cluster_circles', 'multiple_developments_cluster_circles.development_id = developments.development_id', 'left'); 
			$this->db->join('developers', 'developments.developerid = developers.developer_id', 'left');
            $this->db->join('update_messages', 'update_messages.development_id = developments.development_id', 'left'); 
            $this->db->join('client_website_links', 'client_website_links.development_id = developments.development_id', 'left'); 
            $this->db->join('lot_icons', 'lot_icons.development_id = developments.development_id', 'left');  
            $query  = $this->db->get_where('developments', array('developments.development_id' => $development_id));
			$result = NULL;
			if ($query->num_rows() > 0)
			{
				$result = $query->row();
			}
			$this->cache_dev[$development_id] = $result;
		}
		return $this->cache_dev[$development_id];
	}

	public function deleteDevelopment($development_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);

		$development = $this->getDevelopment($development_id);
		if(!empty($development->lots_pdf_file)){
			$file_path = BASEPATH.'../../mpvs/templates/dev_pdfs/'.$development->lots_pdf_file;
			if(file_exists($file_path)){
				unlink($file_path);
			}
		}
		$stages      = $this->Model_stage->getStages($development_id);
		$stages_ids  = array();
		foreach($stages as $stage){
			if(!empty($stage->stage_pdf_file)){
				$file_path = BASEPATH.'../../mpvs/templates/dev_stage_pdfs/'.$stage->stage_pdf_file;
				if(file_exists($file_path)){
					unlink($file_path);
				}
			}
			$stages_ids[] = $stage->stage_id;
		}

		$houses     = $this->Model_house->getHouses($development_id);
		foreach($houses as $house){
			$this->Model_house->deleteHouse($house->house_id);
		}

		// delete lots change logs
		$this->db->where('development_id', $development_id);
		$this->db->delete('change_logs');

		if(count($stages_ids)){
			// delete lots
			$this->db->where_in('stage_id', $stages_ids);
			$this->db->delete('lots');

			// delete stages
			$this->db->where_in('stage_id', $stages_ids);
			$this->db->delete('stages');
		}

		$tables = array('reminder_logs', 'available_lots', 'precincts', 'reminders', 'schedules', 'external_amenities', 'amenities', 'houses', 'radius_markers', 'developments');

		// delete development related records
		foreach($tables as $table_name){
			$this->db->where('development_id', $development_id);
			$this->db->delete($table_name);
		}
		return TRUE;
	}

	public function addDevelopment($form_data)
	{
		$this->load->model('Model_precinct', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);

		$fields        = $this->getFields();
		$colour_fields = $this->getColourFields();
		$development   = array();
		foreach($fields as $field_id){
			if(isset($form_data[$field_id])){
				$development[$field_id] = (in_array($field_id, $colour_fields))? str_replace('#', '', $form_data[$field_id]): $form_data[$field_id];
			}
		}

		if(isset($development['house_and_land_view'])&& $development['house_and_land_view'] == ''){
			$development['house_land_order_by'] = 'street_name';
		}
		if(isset($development['land_view'])&& $development['land_view'] == ''){
			$development['land_order_by'] = 'street_name';
		}
		if(isset($development['land_pdf_view'])&& $development['land_pdf_view'] == ''){
			$development['land_pdf_order_by'] = 'street_name';
		}
		if($development['generated_pdf'] == 1 && isset($_FILES['lots_pdf_file']) && !empty($_FILES['lots_pdf_file'])){
			$file_path = BASEPATH.'../../mpvs/templates/dev_pdfs/';
			if (!file_exists("{$file_path}")) {
				mkdir("{$file_path}", 0777, true);
			}
			if($_FILES['lots_pdf_file']['type'] == 'application/pdf'){
				// Save PDF file
				$file_name      =  'pdf_file_dev_'.time().'.pdf';
				$file_path_name = $file_path.$file_name;
				if(file_exists($file_path_name)){
					unlink($file_path_name);
				}
				move_uploaded_file($_FILES['lots_pdf_file']['tmp_name'], $file_path_name);
				$development['lots_pdf_file'] = $file_name;
			}
			else{
				$development['generated_pdf'] = 0;
			}
		}

		$this->db->insert('developments', $development);
		$development_id  = $this->db->insert_id();
		if($development_id){
			/* Insert precincts */
			for($precinct_number = 1; $precinct_number <= (int)$form_data['totalprecincts']; $precinct_number++){
				$precinct_id     = $this->Model_precinct->addPrecinct($development_id, $precinct_number);

				/* Add stages if the user provided them */
				if(isset($form_data['precinct_stages'][$precinct_number]))
				{
					for($stage_number = 1; $stage_number <= (int)$form_data['precinct_stages'][$precinct_number]; $stage_number++){
						$stage_values = array(
							'stage_number'           => $stage_number,
							'stage_center_latitude'  => (isset($form_data["stage_center_latitude_{$precinct_number}_{$stage_number}"]))? $form_data["stage_center_latitude_{$precinct_number}_{$stage_number}"]: null,
							'stage_center_longitute' => (isset($form_data["stage_center_longitute_{$precinct_number}_{$stage_number}"]))? $form_data["stage_center_longitute_{$precinct_number}_{$stage_number}"]: null,
							'stage_zoomlevel'        => (isset($form_data["stage_zoomlevel_{$precinct_number}_{$stage_number}"]))? $form_data["stage_zoomlevel_{$precinct_number}_{$stage_number}"]: null,
						);
						$this->Model_stage->addStage($precinct_id, $stage_values);
					}
				}
			}
			return $development_id;
		}
		return FALSE;
	}

	public function updateDevelopment($development_id, $form_data)
	{
		$development   = $this->getDevelopment($development_id);
		$fields        = $this->getFields();

		$colour_fields = $this->getColourFields();
		$dev_fields    = array();
		foreach($fields as $field_id){
			if(isset($form_data[$field_id])){
				$dev_fields[$field_id] = (in_array($field_id, $colour_fields))? str_replace('#', '', $form_data[$field_id]):$form_data[$field_id];
			}
		}
		if(isset($dev_fields['house_and_land_view'])&& $dev_fields['house_and_land_view'] == ''){
			$dev_fields['house_land_order_by'] = 'street_name';
		}
		if(isset($dev_fields['land_view'])&& $dev_fields['land_view'] == ''){
			$dev_fields['land_order_by'] = 'street_name';
		}
		if(isset($dev_fields['land_pdf_view'])&& $dev_fields['land_pdf_view'] == ''){
			$dev_fields['land_pdf_order_by'] = 'street_name';
		}
		if($dev_fields['generated_pdf'] == 1 && !empty($_FILES['lots_pdf_file'])){
			$file_path = BASEPATH.'../../mpvs/templates/dev_pdfs/';
			if (!file_exists("{$file_path}")) {
				mkdir("{$file_path}", 0777, true);
			}
			if($_FILES['lots_pdf_file']['type'] == 'application/pdf'){
				// Save PDF file
				$file_name      = (!empty($development->lots_pdf_file))? $development->lots_pdf_file: "pdf_file_dev_{$development_id}.pdf";
				$file_path_name = $file_path.$file_name;
				if(file_exists($file_path_name)){
					unlink($file_path_name);
				}
				move_uploaded_file($_FILES['lots_pdf_file']['tmp_name'], $file_path_name);
				$dev_fields['lots_pdf_file'] = $file_name;
			}
		}
        $this->db->set($dev_fields);        
        $this->db->where('d.development_id = m.development_id and d.development_id = u.development_id and d.development_id = c.development_id and d.development_id = l.development_id');
        $this->db->where('d.development_id', $development_id);      
		return  $this->db->update('developments as d, multiple_developments_cluster_circles as m,update_messages as u, client_website_links as c, lot_icons as l'); 
         
	}

	public function updateOfficeDevelopment($development_id, $form_data, $media_agency = FALSE)
	{
		$fields     = array(
			'sales_office_address', 'sales_office_title', 'sales_email_address', 'sales_person_name', 'sales_office_opening_house', 'sales_telephone_number', 'local_sales_telephone_number',
			'pdf_header_html', 'pdf_footer_html', 'builder_pdf_header_html', 'builder_pdf_footer_html', 'terms_and_conditions_builder', 'diclaimer_builder'
		);
		if($media_agency){
			$fields[]= 'development_name';
			$fields[]= 'development_url_icon';
			$fields[]= 'sales_office_googlemaps_directions_url';
			$fields[]= 'access_key';
			$fields[]= 'terms_and_conditions';
			$fields[]= 'dev_domain';
			$fields[]= 'pdf_link';
			$fields[]= 'form_iframe_url';
			$fields[]= 'form_iframe_url_height';
			$fields[]= 'land_form_iframe_url';
			$fields[]= 'land_form_iframe_url_height';
			$fields[]= 'house_and_land_form_iframe_url';
			$fields[]= 'house_and_land_form_iframe_url_height';
			$fields[]= 'start_hex_colour';
			$fields[]= 'end_hex_colour';
			$fields[]= 'text_hex_colour';
			$fields[]= 'lightbox_border_colour';
			$fields[]= 'lightbox_highlight_colour';
			$fields[]= 'zoom_to_stage_button_hex';
			$fields[]= 'zoom_to_stage_header_hex';
			$fields[]= 'loading_gradient_top_hex';
			$fields[]= 'loading_gradient_middle_hex';
			$fields[]= 'loading_gradient_bottom_hex';
			$fields[]= 'dialog_start_hex';
			$fields[]= 'dialog_end_hex';
			$fields[]= 'get_directions_to_example_address';
			$fields[]= 'search_by_lot_price';
			$fields[]= 'search_by_lot_width';
			$fields[]= 'search_by_lot_size';
			$fields[]= 'show_zoom_to_stage';
			$fields[]= 'show_pricing';
			$fields[]= 'land_starting_from';
			$fields[]= 'show_telephone';
			$fields[]= 'show_stage_icons';
			$fields[]= 'custom_googlemaps_style';
			$fields[]= 'small_available_icon';
			$fields[]= 'large_available_icon';
			$fields[]= 'small_sold_icon';
			$fields[]= 'large_sold_icon';
			$fields[]= 'mapovis_not_supported_url';
			$fields[]= 'custom_font_markup';
			$fields[]= 'custom_google_font_name';
			$fields[]= 'custom_typekit_font_name';
			$fields[]= 'sales_office_iframe_html';
			$fields[]= 'generated_pdf';
			$fields[]= 'dialog_close_button_background_hex';
			$fields[]= 'show_download_pdf';
			$fields[]= 'custom_css_override_url';
			$fields[]= 'show_tour';
			$fields[]= 'show_house_matches';
			$fields[]= 'house_land_package_downloadpdf_button';
			$fields[]= 'house_land_package_downloadpdf_button_highlight';
			$fields[]= 'house_land_package_enquirenow_button';
			$fields[]= 'house_land_package_enquirenow_button_highlight';
			$fields[]= 'panel_close_background_colour';
			$fields[]= 'panel_close_foreground_colour';
			$fields[]= 'sales_office_iframe_scrollbar';
			$fields[]= 'sort_house_matches_by';
			$fields[]= 'show_highlighted_amenities';
			$fields[]= 'radius_circle';
			$fields[]= 'google_control_buttons_background';
			$fields[]= 'google_control_buttons_foreground';
			$fields[]= 'show_get_directions_from_external_amenities';
			$fields[]= 'tooltip_background_colour';
			$fields[]= 'tooltip_text_colour';
			$fields[]= 'sales_office_html';
			$fields[]= 'show_external_amenities';
			$fields[]= 'get_directions_from_external_amenities_text_colour';
			$fields[]= 'search_for_nearby_place_placeholder_text';
			$fields[]= 'back_to_button';
			$fields[]= 'show_satellite';
			$fields[]= 'get_directions_from_external_amenities_text_highlight_colour';
			$fields[]= 'directions_summary_background_colour';
			$fields[]= 'directions_summary_text_colour';
			$fields[]= 'directions_get_direction_button_background_colour';
			$fields[]= 'directions_get_direction_button_text_colour';
			$fields[]= 'nearby_places_sidebar_icon_colour';
			$fields[]= 'nearby_places_sidebar_icon_colour_selected';
			$fields[]= 'no_gradient_hex_colour';
			$fields[]= 'header_footer_button_bg_hex_colour';
			$fields[]= 'lot_search_button_hex_colour';
			$fields[]= 'tour_buttons_background_hex_colour';
			$fields[]= 'staging_domain';
			$fields[]= 'font_size';
			$fields[]= 'tooltip_font_size';

			$development   = $this->getDevelopment($development_id);
			if($form_data['generated_pdf'] == 1 && !empty($_FILES['lots_pdf_file'])){
				$file_path = BASEPATH.'../../mpvs/templates/dev_pdfs/';
				if (!file_exists("{$file_path}")) {
					mkdir("{$file_path}", 0777, true);
				}
				if($_FILES['lots_pdf_file']['type'] == 'application/pdf'){
					// Save PDF file
					$file_name      = (!empty($development->lots_pdf_file))? $development->lots_pdf_file: "pdf_file_dev_{$development_id}.pdf";
					$file_path_name = $file_path.$file_name;
					if(file_exists($file_path_name)){
						unlink($file_path_name);
					}
					move_uploaded_file($_FILES['lots_pdf_file']['tmp_name'], $file_path_name);
					$dev_fields['lots_pdf_file'] = $file_name;
				}
			}
		}
		$colour_fields = $this->getColourFields();
		$dev_fields    = array();
		foreach($fields as $field_id){
			if(isset($form_data[$field_id])){
				$dev_fields[$field_id] = (in_array($field_id, $colour_fields))? str_replace('#', '', $form_data[$field_id]): $form_data[$field_id];
			}
		}
		$this->db->where('developments.development_id', $development_id);
		return $this->db->update('developments', $dev_fields);
	}

	public function duplicateDevelopment($development_id, $development_name)
	{
		$this->load->model('Model_precinct', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_house_image', '', TRUE);
		$this->load->model('Model_house_match', '', TRUE);
		$this->load->model('Model_amenity', '', TRUE);
		$this->load->model('Model_external_amenity', '', TRUE);
		$this->load->model('Model_external_amenity_type', '', TRUE);
		$this->load->model('Model_schedule', '', TRUE);
		$this->load->model('Model_reminder', '', TRUE);
		$this->load->model('Model_radius_marker', '', TRUE);

		$query       = $this->db->get_where('developments', array('developments.development_id' => $development_id));
		$development = ($query->num_rows() > 0)? $query->row(): FALSE;
		if(!$development){
			return FALSE;
		}
		
		$new_development                     = (array)clone $development;
		unset($new_development['development_id']);
		$new_development['development_name'] = $development_name;
		$new_development['lots_pdf_file']    = '';

		$development_data   = $new_development;
		if(!empty($development->lots_pdf_file)){
			$file_path     = BASEPATH.'../../mpvs/templates/dev_pdfs/'.$development->lots_pdf_file;
			$new_file_name = 'pdf_file_dev_'.time().'.pdf';
			$new_file_path = BASEPATH.'../../mpvs/templates/dev_pdfs/'.$new_file_name;
			if(file_exists($file_path)){
				$development_data['lots_pdf_file']  = $new_file_name;
				copy($file_path, $new_file_path);
			}
		}

		// Add New Development
		$this->db->insert('developments', $development_data);
		$new_development_id  = $this->db->insert_id();

		$query2               = $this->db->get_where('mapovis_mini_markers', array('mapovis_mini_markers.development_id' => $development_id));
		$mapovis_mini_markers = ($query->num_rows() > 0)? $query2->result(): FALSE;
		if($mapovis_mini_markers){
			foreach($mapovis_mini_markers as $mapovis_mini_marker_item){
				unset($mapovis_mini_marker_item->id);
				$mapovis_mini_marker_item->development_id = $new_development_id;
				$this->db->insert('mapovis_mini_markers', $mapovis_mini_marker_item);
			}
		}

		// Add Houses
		$house_ids   = array();
		$query_house = $this->db->get_where('houses', array('houses.development_id' => $development_id));
		$houses      = ($query_house->num_rows() > 0)? $query_house->result(): array();
		foreach($houses as $house){
			if($house->development_id == NULL){
				$house_ids[$house->house_id]  = $house->house_id;
				continue;
			}
			$house_data                   = (array)$house;
			$house_data['development_id'] = $new_development_id;
			unset($house_data['house_id']);
			$this->db->insert('houses', $house_data);
			$new_house_id                 = $this->db->insert_id();
			$house_ids[$house->house_id]  = $new_house_id;

			$house_images  = $this->Model_house_image->getHouseImages($house->house_id);
			$file_path     = BASEPATH."../../mpvs/images/dev_houses/";
			if (!file_exists("{$file_path}house_{$new_house_id}/")) {
				mkdir("{$file_path}house_{$new_house_id}/", 0777, true);
			}
			// include house images
			foreach($house_images as $house_image){
				$old_image          = $house_image->file_name;
				$old_image_original = $house_image->original_file_name;

				$new_image          = str_replace("house_{$house->house_id}", "house_{$new_house_id}", $house_image->file_name);
				$new_image_original = str_replace("house_{$house->house_id}", "house_{$new_house_id}", $house_image->original_file_name);
				
				$new_filename          = $this->Model_house_image->uniqueFileName($file_path, $new_image);
				$new_filename_original = $this->Model_house_image->uniqueFileName($file_path, $new_image_original);
				if (!file_exists($file_path.$old_image)) {
					continue;
				}
				copy($file_path.$old_image, $file_path.$new_filename);
				$new_image    = array(
					'house_id'           => $new_house_id,
					'file_name'          => $new_filename,
					'original_file_name' => $new_filename,
					'file_type'          => $house_image->file_type,
					'file_order'         => $house_image->file_order,
				);
				if ($old_image != $old_image_original && file_exists($file_path.$old_image_original)) {
					copy($file_path.$old_image_original, $file_path.$new_filename_original);
					$new_image['original_file_name'] = $new_filename_original;
				}
				$this->db->insert('house_images', $new_image);
			}
		}

		// Add Precincts & Stages
		$stage_ids = array();
		$precincts = $this->Model_precinct->getPrecincts($development_id);
		foreach($precincts as $precinct){
			$new_precinct_id = $this->Model_precinct->addPrecinct($new_development_id, $precinct->precinct_number);
			$query_stage     = $this->db->get_where('stages', array('stages.precinct_id' => $precinct->precinct_id));
			$stages          = ($query_stage->num_rows() > 0)? $query_stage->result(): array();
			foreach($stages as $stage){
				$stage_data                   = (array)$stage;
				$stage_data['precinct_id']    = $new_precinct_id;
				unset($stage_data['stage_id']);

				// if the stage has a file copy it manually for the new stage
				$stage_data['stage_pdf_file'] = '';
				if(!empty($stage->stage_pdf_file)){
					$file_path = BASEPATH.'../../mpvs/templates/dev_stage_pdfs/';
					if(file_exists($file_path.$stage->stage_pdf_file)){
						$new_file_name      = "Dev_{$new_development_id}_Stage_plan_".time().'.pdf';
						copy($file_path.$stage->stage_pdf_file, $file_path.$new_file_name);
						$stage_data['stage_pdf_file'] = $new_file_name;
					}
				}
				$this->db->insert('stages', $stage_data);
				$new_stage_id                = $this->db->insert_id();
				$stage_ids[$stage->stage_id] = $new_stage_id;
			}
		}

		// Add Lots
		$this->db->select('lots.*');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('precincts.development_id', $development_id);
		$query_lot = $this->db->get('lots');
		$lots      = ($query_lot->num_rows() > 0)? $query_lot->result(): array();
		foreach($lots as $lot){
			$lot_data              = (array)$lot;
			$lot_data['stage_id']  = $stage_ids[$lot->stage_id];
			unset($lot_data['lot_id']);
			unset($lot_data['creation_date']);
			$this->db->insert('lots', $lot_data);
			$new_lot_id            = $this->db->insert_id();

			// Add Lot House Packages

			$query_package      = $this->db->get_where('house_lot_packages', array('lot_id' => $lot->lot_id, 'active' => 1));
			$lot_house_packages = ($query_package->num_rows() > 0)? $query_package->result(): array();
			foreach($lot_house_packages as $lot_house_package){
				$new_package = (array)$lot_house_package;
				if(isset($house_ids[$lot_house_package->house_id])){
					$new_package['house_id'] = $house_ids[$lot_house_package->house_id];
					$new_package['lot_id']   = $new_lot_id;
					unset($new_package['house_lot_package_id']);
					if(!empty($lot_house_package->file_name)){
						
					}
					$this->db->insert('house_lot_packages', $new_package);
				}
			}
		}

		// Add Amenities
		$query_amenity = $this->db->get_where('amenities', array('amenities.development_id' => $development_id));
		$amenities     = ($query_amenity->num_rows() > 0)? $query_amenity->result(): array();
		foreach($amenities as $amenity){
			$amenity_data                   = (array)$amenity;
			$amenity_data['development_id'] = $new_development_id;
			$amenity_data['image_names']    = array();
			unset($amenity_data['amenity_id']);
			// include house images
			for($index = 1; $index <=5; $index++){
				$image_title   = "amenity_picture{$index}";
				$amenity_image = $amenity->$image_title;
				if(empty($amenity_image)){
					break;
				}
				$amenity_data['image_names'][] = base_url().'../mpvs/images/amenities/'.$amenity_image;
			}
			$this->Model_amenity->addAmenity($amenity_data);
		}

		// Add External Amenities Types
		$query_ext_amenity = $this->db->get_where('external_amenity_types', array('external_amenity_types.development_id' => $development_id));
		$e_amenities_types = ($query_ext_amenity->num_rows() > 0)? $query_ext_amenity->result(): array();
		$new_e_amenities_types = array();
		foreach($e_amenities_types as $e_amenities_type){
			$e_amenity_type_data                   = (array)$e_amenities_type;
			$e_amenity_type_data['development_id'] = $new_development_id;
			unset($e_amenity_type_data['external_amenity_type_id']);

			$this->db->insert('external_amenity_types', $e_amenity_type_data);
			$new_e_a_type_id  = $this->db->insert_id();

			$new_e_amenities_types[$e_amenities_type->external_amenity_type_id] = $new_e_a_type_id;
		}

		// Add External Amenities
		$query_ext_amenity = $this->db->get_where('external_amenities', array('external_amenities.development_id' => $development_id));
		$e_amenities = ($query_ext_amenity->num_rows() > 0)? $query_ext_amenity->result(): array();
		foreach($e_amenities as $e_amenity){
			$e_amenity_data                      = (array)$e_amenity;
			$e_amenity_data['development_id']    = $new_development_id;
			$e_amenity_data['e_amenity_type_id'] = $new_e_amenities_types[$e_amenity->e_amenity_type_id];
			$e_amenity_data['image_names']       = array();
			unset($e_amenity_data['external_amenity_id']);
			// include house images
			for($index = 1; $index <=5; $index++){
				$image_title     = "e_amenity_picture{$index}";
				$e_amenity_image = $e_amenity->$image_title;
				if(empty($e_amenity_image)){
					break;
				}
				$e_amenity_data['image_names'][] = base_url().'../mpvs/images/external_amenities/'.$e_amenity_image;
			}
			$this->Model_external_amenity->addAmenity($e_amenity_data, TRUE);
		}

		// Add radius_marker
		$radius_markers = $this->Model_radius_marker->getRadiusMarkers($development_id);
		foreach($radius_markers as $radius_marker){
			$radius_marker_data                   = (array)$radius_marker;
			$radius_marker_data['development_id'] = $new_development;
			unset($radius_marker_data['radius_marker_id']);
			$this->Model_radius_marker->addRadiusMarker($new_development_id, $radius_marker_data, TRUE);
		}

		// Add Schedules
		$schedules = $this->Model_schedule->getSchedules($development_id);
		foreach($schedules as $schedule){
			$schedule_data                   = (array)$schedule;
			unset($schedule_data['development_id']);
			$this->Model_schedule->updateSchedules($new_development_id, $schedule_data);
		}

		// Add Reminders
		$reminder = $this->Model_reminder->getReminder($development_id);
		$reminder_data                   = (array)$reminder;
		unset($reminder_data['development_id']);
		$this->Model_reminder->updateReminder($new_development_id, $reminder_data);

		return $new_development_id;
	}

	public function updateLastUpdated($development_id)
	{
		$this->db->where('developments.development_id', $development_id);
		return $this->db->update('developments', array('last_update' => date('Y-m-d H:i:s')));
	}

	public function updateLastReminder($development_id, $date)
	{
		$this->db->where('developments.development_id', $development_id);
		return $this->db->update('developments', array('last_reminder' => $date));
	}

	public function updatePrimaryUser($development_id, $primary_user)
	{
		$form_data = array('primary_user' => $this->checkUserToSave($primary_user));
		$this->db->where('developments.development_id', $development_id);
		return $this->db->update('developments', $form_data);
	}

	public function updateSecondaryUser($development_id, $secondary_user)
	{
		$form_data = array('secondary_user' => $this->checkUserToSave($secondary_user));
		$this->db->where('developments.development_id', $development_id);
		return $this->db->update('developments', $form_data);
	}

	public function updateManagerUser($development_id, $manager_user)
	{
		$form_data = array('manager_user' => $this->checkUserToSave($manager_user));
		$this->db->where('developments.development_id', $development_id);
		return $this->db->update('developments', $form_data);
	}

	public function updateMediaAgency($development_id, $media_agency)
	{
		$form_data = array('media_agency' => $this->checkUserToSave($media_agency));
		$this->db->where('developments.development_id', $development_id);
		return $this->db->update('developments', $form_data);
	}

	public function getDevelopmentRole($development_id, $user)
	{   
		$development = $this->getDevelopment($development_id);
		$user_role   = $user->user_role;
		if($development){
			switch($user->user_id){
				case $development->primary_user:
					$user_role   = 'Primary';
					break;
				case $development->secondary_user:
					$user_role   = 'Secondary';
					break;
				case $development->manager_user:
					$user_role   = 'Manager';
					break;
			}
		}
		return $user_role;
	}

	public function HasUserPermissions($development, $user_id)
	{
		if($development->primary_user != $user_id && $development->secondary_user != $user_id 
			&& $development->manager_user != $user_id && $development->media_agency != $user_id){
			$this->load->model('Model_development_general_user', '', TRUE);
			$general_user = $this->Model_development_general_user->getDevelopmentUser($development->development_id, $user_id);
			if($general_user){
				return TRUE;
			}
			return FALSE;
		}
		return TRUE;
	}

	public function validateDevelomentKey($development_id, $developmentkey){
		$development = $this->getDevelopment($development_id);
		if(!$development ||$development->access_key !== $developmentkey){
			return false;
		}
		return true;
	}

	public function houseMatchSortByOptions()
	{
		return array('price','random');
	}

	public function getStates()
	{
		return array('ACT', 'NSW', 'NT', 'QLD', 'SA', 'TAS', 'VIC','WA');
	}

	public function getHouseLandOrderBy()
	{
		return array('lot_number', 'street_name');
	}

	public function getLandView($view_name)
	{
		$this->load->library('mapovis_lib');
		$views = $this->mapovis_lib->getLandViews();
		return (in_array($view_name, $views))? "land/views/{$view_name}": 'land/view_land';
	}

	public function getLandPdfView($view_name)
	{
		$this->load->library('mapovis_lib');
		$views = $this->mapovis_lib->getLandPdfViews();
		return (in_array($view_name, $views))? "land/pdf_views/{$view_name}": 'land/view_land_pdf';
	}

	public function getHouseAndLandView($view_name)
	{
		$this->load->library('mapovis_lib');
		$views = $this->mapovis_lib->getHouseAndLandViews();
		return (in_array($view_name, $views))? "houseandland/views/{$view_name}": 'houseandland/view_houseandland';
	}

	public function getTemplateView($view_name)
	{
		$this->load->library('mapovis_lib');
		$views = $this->mapovis_lib->getTemplateViews();
		return (in_array($view_name, $views))? "templates/views/{$view_name}": 'templates/view_template';
	}

	public function UserDevAvatar()
	{
		$this->load->model('Model_user', '', TRUE);
		$current_user = $this->Model_user->getUserLogged();

		$this->db->where("(developments.primary_user = {$current_user->user_id} OR developments.secondary_user = {$current_user->user_id} OR developments.manager_user = {$current_user->user_id} OR developments.media_agency = {$current_user->user_id} )");
		$this->db->where('development_url_icon !=', '');
		$query   = $this->db->get('developments');
		$result = $query->row();
		return ($result)? $result->development_url_icon: '';
	}

	public function getMyDevelopmentForStatistics($development_id = 0)
	{
		$this->load->model('Model_user', '', TRUE);
		$current_user = $this->Model_user->getUserLogged();
		$uid          = $current_user->user_id;

		$this->db->select('developments.*');
		$this->db->join('development_general_users', 'development_general_users.development_id = developments.development_id', 'left');
		$this->db->where("(((development_general_users.general_user_id = {$uid} OR developments.primary_user = {$uid} OR developments.secondary_user = {$uid} OR developments.manager_user = {$uid}) AND developments.show_statistics_sales_team = 1) OR (developments.media_agency = {$uid}  AND developments.show_statistics_media_agency = 1))");
		$this->db->group_by('developments.development_id');
		if($development_id){
			$this->db->where('developments.development_id', $development_id);
		}
		$query  = $this->db->get('developments');
		$result = $query->result();
		return ($result)? $result: array();
	}

	public function showStatistics($development_id = 0)
	{
		$developments = $this->getMyDevelopmentForStatistics($development_id);
		return (count($developments));
	}

	public function isMediaAGency($development_id)
	{
		$this->load->model('Model_user', '', TRUE);
		$development  = $this->getDevelopment($development_id);
		$current_user = $this->Model_user->getUserLogged();
		return ($development->media_agency == $current_user->user_id);
	}

	public function isManager($development_id)
	{
		$this->load->model('Model_user', '', TRUE);
		$development  = $this->getDevelopment($development_id);
		$current_user = $this->Model_user->getUserLogged();
		return ($development->manager_user == $current_user->user_id);
	}

	private function checkUserToSave($user_id)
	{
		return ($user_id)? $user_id: null;
	}

	private function getFields()
	{
		return array(
			'development_name',
			'development_uri',
			'development_url_icon',
            'multipledevelopmenticon_url',
            'multipledevelopmenticon_width',
            'multipledevelopmenticon_height',
            'multiple_developments_cluster_circle_url',
            'multiple_developments_cluster_circle_width',
            'multiple_developments_cluster_circle_height',
            'multiple_developments_cluster_text_hex_colour',
            'multiple_developments_cluster_text_size',
			'developerid',
            'active',
            'update_underway',
            'update_message',
			'state',
			'overlay_filename',
			'southwest_latitude',
			'southwest_longitude',
			'northeast_latitude',
			'northeast_longitude',
			'centre_latitude',
			'centre_longitude',
			'entryexit_latitude',
			'entryexit_longitude',
			'vic_public_transport_origin',
			'stageplanpdf',
			'ga_tracking_id',
			'start_hex_colour',
			'end_hex_colour',
			'text_hex_colour',
			'lightbox_border_colour',
			'lightbox_highlight_colour',
			'zoom_to_stage_button_hex',
			'zoom_to_stage_header_hex',
			'loading_gradient_top_hex',
			'loading_gradient_middle_hex',
			'loading_gradient_bottom_hex',
			'dialog_start_hex',
			'dialog_end_hex',
			'dialog_close_button_background_hex',
			'dialog_close_button_foreground_hex',
			'dev_domain',
			'sales_office_address',
			'sales_telephone_number',
			'local_sales_telephone_number',
			'sales_office_latitude',
			'sales_office_longitude',
			'sales_office_title',
			'sales_office_html',
			'sales_office_googlemaps_directions_url',
			'sales_email_address',
			'sales_person_name',
			'sales_office_opening_house',
			'pdf_link',
			'access_key',
			'terms_and_conditions',
			'search_by_lot_price',
			'search_by_lot_width',
			'search_by_lot_size',
			'show_zoom_to_stage',
			'show_pricing',
			'custom_googlemaps_style',
			'form_iframe_url',
			'form_iframe_url_height',
			'land_form_iframe_url',
			'land_form_iframe_url_height',
			'house_and_land_form_iframe_url',
			'house_and_land_form_iframe_url_height',
			'get_directions_to_example_address',
			'zoom_to_community_feature_latitude',
			'zoom_to_community_feature_longitude',
			'house_and_land_view',
			'house_land_order_by',
			'land_view',
			'land_show_sold_lots',
			'land_order_by',
			'land_pdf_view',
			'land_pdf_order_by',
			'template_view',
			'generated_pdf',
			'small_available_icon',
			'large_available_icon',
			'small_sold_icon',
			'large_sold_icon',
			'slider_skin_image',
			'overlay_filename_ipad',
			'mapovis_not_supported_url',
			'show_telephone',
			'show_stage_icons',
			'custom_font_markup',
			'show_external_amenities',
			'land_starting_from',
			'custom_google_font_name',
			'custom_typekit_font_name',
			'sales_office_iframe_html',
			'show_statistics_sales_team',
			'show_statistics_media_agency',
			'show_download_pdf',
			'form_iframe_hiddenfield_lot_number',
			'form_iframe_hiddenfield_house_name',
			'form_iframe_hiddenfield_builder',
			'sales_office_icon',
			'custom_css_override_url',
			'mapovis_not_supported_browser_url',
			'show_nearby_developments',
			'show_tour',
			'show_geolocation_button',
			'show_house_matches',
			'house_land_package_downloadpdf_button',
			'house_land_package_downloadpdf_button_highlight',
			'house_land_package_enquirenow_button',
			'house_land_package_enquirenow_button_highlight',
			'panel_close_background_colour',
			'panel_close_foreground_colour',
			'sales_office_iframe_scrollbar',
			'developmenticon',
			'sort_house_matches_by',
			'show_highlighted_amenities',
			'radius_circle',
			'radius_circle_overlay_image',
			'google_control_buttons_background',
			'google_control_buttons_foreground',
			'show_get_directions_from_external_amenities',
			'tooltip_background_colour',
			'tooltip_text_colour',
			'pdf_footer_header_colour',
			'pdf_footer_row_colour',
			'get_directions_from_external_amenities_text_colour',
			'show_lot_search',
			'show_zoom_to',
			'search_for_nearby_place_placeholder_text',
			'show_street_view',
			'back_to_button',
			'mapovis_website_link',
			'show_satellite',
			'staging_domain',
			'get_directions_from_external_amenities_text_highlight_colour',
			'directions_summary_background_colour',
			'directions_summary_text_colour',
			'directions_get_direction_button_background_colour',
			'directions_get_direction_button_text_colour',
			'mobile_centre_latitude',
			'mobile_centre_longitude',
			'mobile_start_zoom_level',
			'mobile_old_ios_link',
			'mobile_old_android_link',
			'mapovis_mini_polygon_strokeColor',
			'mapovis_mini_polygon_strokeOpacity',
			'mapovis_mini_polygon_strokeWeight',
			'mapovis_mini_polygon_fillColor',
			'mapovis_mini_polygon_fillOpacity',
			'nearby_search_radius',
			'no_gradient_hex_colour',
			'mapovis_mini_polygon_fillColor_mouseover',
			'mapovis_mini_open_interactive_masterplan_button_url',
			'mapovis_mini_open_interactive_masterplan_button_width',
			'mapovis_mini_open_interactive_masterplan_button_height',
			'nearby_places_map_marker_default',
			'nearby_places_map_marker_selected',
			'nearby_places_sidebar_icon_colour',
			'nearby_places_sidebar_icon_colour_selected',
			'developmenticon_width',
			'developmenticon_height',
			'display_village_icon_url',
			'display_village_icon_width',
			'display_village_icon_height',
			'display_village_longitude',
			'display_village_latitude',
			'display_village_center_longitude',
			'display_village_center_latitude',
			'display_village_centre_zoom_level',
			'display_village_show_at_zoom_level',
			'display_village_hide_at_zoom_level',
			'sales_office_icon_width',
			'sales_office_icon_height',
			'sales_office_icon_show_at_zoom_level',
			'sales_office_icon_hide_at_zoom_level',
			'directions_search_zoom_level_decrease_by',
			'LotDetailsTemplate',
			'form_iframe_show_scroll_bars',
			'font_size',
			'tooltip_font_size',
			'lot_polygon_border_show',
			'lot_polygon_border_colour',
            'lot_polygon_border_colour_deposited',
			'lot_polygon_border_colour_zoom_to_lot',
			'lot_polygon_border_weight',
			'lot_polygon_border_opacity',
			'lot_polygon_border_show_from_zoomlevel',
			'lot_polygon_border_show_to_zoomlevel',
			'header_footer_button_bg_hex_colour',
			'lot_search_button_hex_colour',
			'header_footer_button_bg_hex_colour_highlight',
			'tour_buttons_background_hex_colour',
			'form_code',
			'show_public_transport_search',
			'land_pdf_fontfamily_css',
			'small_available_icon_highlighted',
			'large_available_icon_highlighted',
			'small_sold_icon_highlighted',
			'large_sold_icon_highlighted',
			'mapovismini_default_zoom_level',
			'mapovis_mini_polygon_strokeColor_mouseover',
			'mapovis_mini_polygon_strokeOpacity_mouseover',
			'mapovis_mini_polygon_strokeWeight_mouseover',
			'display_village_iframe_url',
			'display_village_iframe_scrollbar',
			'facebook_sharing',
			'facebook_share_houseland_package_name',
			'facebook_share_houseland_package_caption',
			'facebook_share_houseland_package_link',
			'facebook_share_houseland_package_description',
			'facebook_share_lot__name',
			'facebook_share_lot_caption',
			'facebook_share_lot_link',
			'facebook_share_lot_picture',
			'facebook_share_lot_description',
			'pdf_header_html',
			'pdf_header_margin',
			'pdf_footer_html',
			'price_list_email_template',
			'pdf_footer_margin',
			'builder_pdf_header_html',
			'builder_pdf_header_margin',
			'builder_pdf_footer_html',
			'builder_pdf_footer_margin',
			'terms_and_conditions_builder',
			'diclaimer_builder',
			'price_list_reply_to_email',
			'default_zoom_level',
			'default_zoom_level_mobile',
			'zoom_to_stage_level',
			'external_amenity_category_icons_margin_right',
			'linked_development_logo',
			'show_compass',
			'show_compass_mobile',
			'compass_width',
			'compass_height',
			'compass_url',
			'max_zoom_level',
			'max_zoom_level_mobile',
			'default_cluster_circle_url',
			'default_cluster_circle_height',
			'default_cluster_circle_width',
			'default_cluster_text_hex_colour',
			'default_cluster_text_size',
			'show_internal_amenity_icon_at_and_above_zoom_level',
			'footer_text_icon_highlight',
			'sales_office_amenity_polygon_coords',
			'lots_pdf_file',
			'last_pdf_update',
			'developmenticon_show_at_zoom_level',
			'developmenticon_hide_at_zoom_level',
			'mapovis_mini_sales_office_marker_show_at',
			'mapovis_mini_sales_office_marker_hide_at',
			'house_and_land_api',
			'house_and_land_api_url',
			'house_and_land_api_development_id',
			'custom_house_facade_width',
			'custom_house_facade_height',
			'hnl_multi_images',
			'hide_global_houses',
            'interactive_master_plan_url',
            'land_for_sale_url',
            'house_and_land_packages_url',
            'mapovis_mini_url',
            'show_icons_at_and_above_zoom_level',
            'available_17',
            'available_18',
            'available_19',
            'sold_17',
            'sold_18',
            'sold_19',
            'deposited_17',
            'deposited_18',
            'deposited_19'
		);
	}

	private function getColourFields()
	{
		return array(
			'start_hex_colour',
			'end_hex_colour',
			'text_hex_colour',
			'lightbox_border_colour',
			'lightbox_highlight_colour',
			'zoom_to_stage_button_hex',
			'zoom_to_stage_header_hex',
			'loading_gradient_top_hex',
			'loading_gradient_middle_hex',
			'loading_gradient_bottom_hex',
            'multiple_developments_cluster_text_hex_colour',
			'dialog_start_hex',
			'dialog_end_hex',
			'dialog_close_button_background_hex',
			'dialog_close_button_foreground_hex',
			'panel_close_background_colour',
			'panel_close_foreground_colour',
			'house_land_package_downloadpdf_button',
			'house_land_package_downloadpdf_button_highlight',
			'house_land_package_enquirenow_button',
			'house_land_package_enquirenow_button_highlight',
			'google_control_buttons_background',
			'google_control_buttons_foreground',
			'tooltip_background_colour',
			'tooltip_text_colour',
			'pdf_footer_header_colour',
			'pdf_footer_row_colour',
			'get_directions_from_external_amenities_text_colour',
			'get_directions_from_external_amenities_text_highlight_colour',
			'directions_summary_background_colour',
			'directions_summary_text_colour',
			'directions_get_direction_button_background_colour',
			'directions_get_direction_button_text_colour',
			'mapovis_mini_polygon_strokeColor',
			'mapovis_mini_polygon_fillColor',
			'no_gradient_hex_colour',
			'mapovis_mini_polygon_fillColor_mouseover',
			'nearby_places_sidebar_icon_colour',
			'nearby_places_sidebar_icon_colour_selected',
			'lot_polygon_border_colour',
            'lot_polygon_border_colour_deposited',
			'lot_polygon_border_colour_zoom_to_lot',
			'header_footer_button_bg_hex_colour',
			'lot_search_button_hex_colour',
			'header_footer_button_bg_hex_colour_highlight',
			'tour_buttons_background_hex_colour',
			'mapovis_mini_polygon_strokeColor_mouseover',
			'default_cluster_text_hex_colour',
		);
	}
}
?>
