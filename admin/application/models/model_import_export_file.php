<?php
Class Model_import_export_file extends CI_Model
{
	public function getCsvLots($development_id = 0){
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_house_match', '', TRUE);
		$lots        = $this->Model_lot->getLots($development_id);
		$file_name   = '/tmp/temp_lots.csv';
		$fp          = fopen($file_name, 'w');
		$csv_headers = array(
			'development_id',
			'precinct_number',
			'stage_number',
			'lot_number',
			'street_name',
			'status',
			'price_range_min',
			'price_range_max',
			'lot_latitude',
			'lot_longitude',
			'lot_width',
			'lot_depth',
			'lot_square_meters',
			'lot_corner',
			'lot_titled',
			'lot_irregular',
			'house_matches'
		);
		fputcsv($fp, $csv_headers);
		foreach($lots as $lot){
			$house_matches = $this->Model_house_match->getHouseMatches($lot->lot_id);
			$csv_data      = array(
				$lot->development_id,
				$lot->precinct_number,
				$lot->stage_number,
				$lot->lot_number,
				$lot->street_name,
				$lot->status,
				$lot->price_range_min,
				$lot->price_range_max,
				$lot->lot_latitude,
				$lot->lot_longitude,
				$lot->lot_width,
				$lot->lot_depth,
				$lot->lot_square_meters,
				$lot->lot_corner,
				$lot->lot_titled,
				$lot->lot_irregular,
				implode(',', array_map('getHouseId', $house_matches))
			);
			fputcsv($fp, $csv_data);
		}
		fclose($fp);
		return file_get_contents($file_name);
	}

	public function getImportedFile($imported_file_id)
	{
		$query  = $this->db->get_where('imported_files', array('imported_files.imported_file_id' => $imported_file_id));
		$result = null;
		if($query->num_rows() > 0)
		{
			$result = $query->row();
		}
		return $result;
	}

	public function addImportFileToUpdate($file_data)
	{
		$this->load->model('Model_user', '', TRUE);

		$user           = $this->Model_user->getAdminLoggedIn();
		$notes          = $this->importFileContentUpdateLots($file_data);
		$formated_notes = '- '.implode("\n- ", $notes);
		$data  = array(
			'filename'     => $file_data['file_name'],
			'file_path'    => $file_data['file_path'],
			'import_date'  => date('Y-m-d'),
			'user_id'      => $user->user_id,
			'import_notes' => $formated_notes,
		);
		$this->db->insert('imported_files', $data);
		return $this->db->insert_id();
	}

	/********************************** IMPORT VALIDATION AND EXECUTION *******************************************/

	public function importFileContentUpdateLots($file_details)
	{
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_house_match', '', TRUE);
		$this->load->model('Model_street_name', '', TRUE);
		$this->load->library('mapovis_lib');
		$lot_all_status   = $this->Model_lot->getStatus();
		$file_content     = $this->getFileContent($file_details);
		$notes            = '';
		$updatable_fields = array(
			'street_name',
			'status',
			'price_range_min',
			'price_range_max',
			'lot_latitude',
			'lot_longitude',
			'lot_width',
			'lot_depth',
			'lot_square_meters',
			'lot_corner',
			'lot_titled',
			'lot_irregular',
		);
		$developments = array();
		$street_names = $this->getStreetNames();
		foreach($file_content['rows'] as $index => $row_content){
			$development_id        = $row_content['development_id'];
			$precinct_number       = $row_content['precinct_number'];
			$stage_number          = $row_content['stage_number'];
			$lot_number            = $row_content['lot_number'];

			if(!isset($developments[$development_id])){
				$developments[$development_id] = $this->getOrderedDevelopments($development_id);
			}
			// if doesn't exists the row will be ignored
			if($developments[$development_id]){
				$row_development = $developments[$development_id];
				if(isset($row_content['status']) && strtolower($row_content['status']) == 'delete'){
					if(isset($row_development->precincts[$precinct_number]) &&
						isset($row_development->precincts[$precinct_number]->stages[$stage_number]) &&
						isset($row_development->precincts[$precinct_number]->stages[$stage_number]->lots[$lot_number]->lot_id)){
						$lot_id   = $row_development->precincts[$precinct_number]->stages[$stage_number]->lots[$lot_number]->lot_id;
						// delete lot
						$this->Model_lot->deleteLot($lot_id);
						unset($row_development->precincts[$precinct_number]->stages[$stage_number]->lots[$lot_number]);
						$notes[] = 'The Lot '.$lot_number.' was DELETED. Development '.$development_id.' Precinct '.$precinct_number.' Stage '.$stage_number;
					}
					else{
						$notes[] = 'Error: The Lot '.$lot_number.' was not found in the system. Development '.$development_id.' Precinct '.$precinct_number.' Stage '.$stage_number;
					}
					continue;
				}
				
				if(!isset($row_development->precincts[$precinct_number])){
					$precinct_id = $this->Model_precinct->addPrecinct($development_id, $precinct_number);
					$notes[]     = 'A Precinct was added. Development '.$development_id.' Precinct '.$precinct_number;
					$row_development->precincts[$precinct_number] = (object)array('precinct_id' => $precinct_id, 'stages' => array());
				}

				$precinct = $row_development->precincts[$precinct_number];
				if(!isset($precinct->stages[$stage_number])){
					$stage_id = $this->Model_stage->addStage($precinct->precinct_id, array('stage_number' => $stage_number));
					$notes[]  = 'A Stage was added. Development '.$development_id.' Precinct '.$precinct_number.' Stage '.$stage_number;
					$precinct->stages[$stage_number] = (object)array('stage_id' => $stage_id, 'lots' => array());
				}
				$stage          = $precinct->stages[$stage_number];
				$lot_id         = null;
				$lot_status     = isset($row_content['status']) && in_array($row_content['status'], $lot_all_status)? $row_content['status']: 'Unlisted';
				$street_name    = trim(strtolower($row_content['street_name']));
				if(empty($street_name)){
					$street_name_id = NULL;
				}
				elseif(isset($street_names[$street_name])){
					$street_name_id = $street_names[$street_name];
				}
				else{
					// add new street
					$street_name_id             = $this->Model_street_name->addStreetName($row_content);
					$street_names[$street_name] = $street_name_id;
					$notes[]                    = 'A Street was added. "'.$row_content['street_name'].'"';
				}
				if(!isset($stage->lots[$lot_number])){
					// insert lot
					$lot_data                 = array(
						'stage_id'          => $stage->stage_id,
						'lot_number'        => $lot_number,
						'street_name_id'    => $street_name_id,
						'status'            => $lot_status,/* If status is not provided Unlisted will be set by default */
						'price_range_min'   => (float)$row_content['price_range_min'],
						'price_range_max'   => (float)$row_content['price_range_max'],
						'lot_latitude'      => $row_content['lot_latitude'],
						'lot_longitude'     => $row_content['lot_longitude'],
						'lot_width'         => (float)$row_content['lot_width'],
						'lot_depth'         => (float)$row_content['lot_depth'],
						'lot_square_meters' => ($row_content['lot_square_meters'])? $row_content['lot_square_meters']: $row_content['lot_width'] * $row_content['lot_depth'],
						'lot_corner'        => (boolean)(isset($row_content['lot_corner']))? $row_content['lot_corner']: FALSE,
						'lot_titled'        => (boolean)(isset($row_content['lot_titled']))? $row_content['lot_titled']: FALSE,
						'lot_irregular'     => (boolean)(isset($row_content['lot_irregular']))? $row_content['lot_irregular']: FALSE,
					);
					$lot_id                   = $this->Model_lot->addLot($lot_data, TRUE);
					$stage->lots[$lot_number] = $lot_id;
					$notes[]                  = 'The Lot '.$lot_number.' was added. Development '.$development_id.' Precinct '.$precinct_number.' Stage '.$stage_number;
				}
				else{
					$lot_id   = $stage->lots[$lot_number]->lot_id;
					// update lot
					$lot_data = array(
						'price_range_min'   => (float)$row_content['price_range_min'],
						'price_range_max'   => (float)$row_content['price_range_max'],
						'lot_latitude'      => $row_content['lot_latitude'],
						'lot_longitude'     => $row_content['lot_longitude'],
						'lot_width'         => (float)$row_content['lot_width'],
						'lot_depth'         => (float)$row_content['lot_depth'],
						'lot_square_meters' => ($row_content['lot_square_meters'])? $row_content['lot_square_meters']: $row_content['lot_width'] * $row_content['lot_depth'],
						'lot_corner'        => (boolean)(isset($row_content['lot_corner']))? $row_content['lot_corner']: FALSE,
						'lot_titled'        => (boolean)(isset($row_content['lot_titled']))? $row_content['lot_titled']: FALSE,
						'lot_irregular'     => (boolean)(isset($row_content['lot_irregular']))? $row_content['lot_irregular']: FALSE,
					);
					if(isset($row_content['status'])){
						$lot_data['status']         = $lot_status;
					}
					if($street_name_id){
						$lot_data['street_name_id'] = $street_name_id;
					}
					$this->Model_lot->updateLot($lot_id, $lot_data, TRUE);

					$notes[]  = 'The Lot '.$lot_number.' was UPDATED. Development '.$development_id.' Precinct '.$precinct_number.' Stage '.$stage_number;
				}
				if($lot_id){
					// Add house matches if provided
					if(isset($row_content['house_matches'])){
						if(empty($row_content['house_matches'])){
							$house_matches = array();
						}
						else{
							$house_matches = array_map('intval', explode(',', $row_content['house_matches']));
						}
						$valid_houses  = array();
						foreach($house_matches as $house_match){
							if(isset($row_development->houses[$house_match])){
								$valid_houses[] = $house_match;
							}
						}
						$this->Model_house_match->updateLotHouses($lot_id, $valid_houses);
					}
				}
			}
			else{
				$notes[] = "The Development {$development_id} was not found. Precinct {$precinct_number} Stage {$stage_number} Lot {$lot_number}";
			}
		}
		return $notes;
	}

	private function getFileContent($file_details)
	{
		$file_path    = $file_details['full_path'];
		$file_data    = array();
		if(file_exists($file_path)){
			$file_handle  = fopen($file_path, 'r');
			/* Taske the first line of content with the headers*/
			$file_headers = fgetcsv($file_handle);
			while (!feof($file_handle) ) {
				$tmp_row     = fgetcsv($file_handle);
				if($tmp_row){
					/* Combine the headers and data to have a relative array header1 => val1, header2 => val2, etc*/
					$file_data[] = array_combine($file_headers, $tmp_row);
				}
			}
			fclose($file_handle);
		}
		return array('headers' => $file_headers, 'rows' => $file_data);
	}

	private function getOrderedDevelopments($development_id){
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_precinct', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);

		$development            = $this->Model_development->getDevelopment($development_id);
		if(!$development){
			return FALSE;
		}
		$development->houses    = $this->getHouses($development->development_id);
		$development->precincts = array();
		$precincts              = $this->Model_precinct->getPrecincts($development->development_id);
		foreach($precincts as &$precinct){
			$precinct->stages = array();
			$stages           = $this->Model_stage->getStages($development->development_id, $precinct->precinct_id);
			foreach($stages as $stage){
				$lots        = $this->Model_lot->getLotsByStage($stage->stage_id);
				$stage->lots = $this->mapovis_lib->orderItemsByKey($lots, 'lot_number');
				$precinct->stages[$stage->stage_number] = $stage;
			}
			$development->precincts[$precinct->precinct_number] = $precinct;
		}
		return $development;
	}

	private function getStreetNames()
	{
		$tmp_street_names = $this->Model_street_name->getStreetNames();
		$street_names     = array();
		foreach($tmp_street_names as $tmp_street_name){
			$street_names[trim(strtolower($tmp_street_name->street_name))] = $tmp_street_name->street_name_id;
		}
		return $street_names;
	}

	private function getHouses($development_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$tmp_houses = $this->Model_house->getHouses($development_id, TRUE);
		$houses     = array();
		foreach($tmp_houses as $tmp_house){
			$houses[$tmp_house->house_id] = $tmp_house;
		}
		return $houses;
	}
}
function getHouseId($object){
	return $object->house_id;
}
?>