<?php
Class Model_external_amenity extends CI_Model
{
	function getAmenities($development_id, $e_amenity_type_id = null, $only_active = FALSE)
	{
		$this->db->select('external_amenities.*, external_amenity_types.e_amenity_type_name');
		$this->db->join('external_amenity_types', 'external_amenity_types.external_amenity_type_id = external_amenities.e_amenity_type_id', 'left');
		$this->db->where('external_amenities.development_id', $development_id);
		if($e_amenity_type_id !== null){
			$this->db->where('external_amenities.e_amenity_type_id', $e_amenity_type_id);
		}
		if($only_active === TRUE){
			$this->db->where('external_amenities.show_e_amenity', 1);
		}
		$query  = $this->db->get('external_amenities');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result(); 
		}
		return $result;
	}

	function getAmenity($external_amenity_id)
	{
		$query  = $this->db->get_where('external_amenities', array('external_amenity_id' => $external_amenity_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row();
		}	
		return $result;
	}

	function addAmenity($form_data, $alldata = FALSE)
	{
		if($alldata){
			$e_amenity_details = $form_data;
			unset($e_amenity_details['image_names']);
		}
		else{
			$fields      = $this->getFields();
			$e_amenity_details = array();
			foreach($fields as $field){
				if(isset($form_data[$field])){
					$e_amenity_details[$field] = $form_data[$field];
				}
			}
		}
		$this->db->insert('external_amenities', $e_amenity_details);
		$amenity_id = $this->db->insert_id();
		if($amenity_id){
			$amenity_image_details = array();
			$subfolder             = "ext_amenity_{$amenity_id}/";
			$new_file_path         = BASEPATH."../../mpvs/images/external_amenities/";
			if (!file_exists("{$new_file_path}{$subfolder}")) {
				mkdir("{$new_file_path}{$subfolder}", 0777, true);
			}	
			for($x=1; $x <=5; $x++){
				if(!isset($form_data['image_names'][$x-1])){
					break;
				}
				$image_url    = $form_data['image_names'][$x-1];
				$file_name    = $subfolder.basename($image_url);
				/* Validate the file name to make sure is unique and is not overwritten  */
				$new_filename = $this->uniqueFileName($new_file_path, $file_name);

				@file_put_contents($new_file_path.$new_filename, file_get_contents($image_url));
				$form_data['image_names'][$x-1] = $new_filename;
				$amenity_image_details['e_amenity_picture'.$x] = (isset($form_data['image_names'][$x-1]))? $form_data['image_names'][$x-1]: '';
			}
			if(count($amenity_image_details)){
				$this->db->where('external_amenities.external_amenity_id', $amenity_id);
				$this->db->update('external_amenities', $amenity_image_details);
			}
		}
		return $amenity_id;
	}

	function updateAmenity($external_amenity_id, $form_data)
	{
		$fields          = $this->getFields();
		$e_amenity_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$e_amenity_details[$field] = $form_data[$field];
			}
		}
		$subfolder     = "ext_amenity_{$external_amenity_id}/";
		$new_file_path = BASEPATH."../../mpvs/images/external_amenities/";
		if (!file_exists("{$new_file_path}{$subfolder}")) {
			mkdir("{$new_file_path}{$subfolder}", 0777, true);
		}	
		for($x=1; $x <=5; $x++){
			if(isset($form_data['image_names'][$x-1]) && filter_var($form_data['image_names'][$x-1], FILTER_VALIDATE_URL)){
				$image_url    = $form_data['image_names'][$x-1];
				$file_name    = $subfolder.basename($image_url);
				/* Validate the file name to make sure is unique and is not overwritten  */
				$new_filename = $this->uniqueFileName($new_file_path, $file_name);
				
				file_put_contents($new_file_path.$new_filename, file_get_contents($image_url));
				$form_data['image_names'][$x-1] = $new_filename;
			}
			$e_amenity_details['e_amenity_picture'.$x] = (isset($form_data['image_names'][$x-1]))? $form_data['image_names'][$x-1]: '';
		}

		$this->db->where('external_amenity_id', $external_amenity_id);
		return $this->db->update('external_amenities', $e_amenity_details);
	}

	function updateAmenitySales($external_amenity_id, $form_data)
	{
		$this->load->model('Model_development', '', TRUE);
		$e_amenity       = $this->getAmenity($external_amenity_id);
		$media_agency    = $this->Model_development->isMediaAGency($e_amenity->development_id);
		$fields          = array('e_amenity_name', 'e_amenity_description', 'e_amenity_moreinfo_url', 'show_e_amenity', 'external_amenity_iframe_scrollbar');
		if($media_agency){
			$fields[] = 'external_amenity_iframe_html';
			$fields[] = 'e_amenity_address';
		}
		$e_amenity_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$e_amenity_details[$field] = $form_data[$field];
			}
		}
		$subfolder     = "ext_amenity_{$external_amenity_id}/";
		$new_file_path = BASEPATH."../../mpvs/images/external_amenities/";
		if (!file_exists("{$new_file_path}{$subfolder}")) {
			mkdir("{$new_file_path}{$subfolder}", 0777, true);
		}	
		for($x=1; $x <=5; $x++){
			if(isset($form_data['image_names'][$x-1]) && filter_var($form_data['image_names'][$x-1], FILTER_VALIDATE_URL)){
				$image_url    = $form_data['image_names'][$x-1];
				$file_name    = $subfolder.basename($image_url);
				/* Validate the file name to make sure is unique and is not overwritten  */
				$new_filename = $this->uniqueFileName($new_file_path, $file_name);
				
				file_put_contents($new_file_path.$new_filename, file_get_contents($image_url));
				$form_data['image_names'][$x-1] = $new_filename;
			}
			$e_amenity_details['e_amenity_picture'.$x] = (isset($form_data['image_names'][$x-1]))? $form_data['image_names'][$x-1]: '';
		}

		$this->db->where('external_amenity_id', $external_amenity_id);
		return $this->db->update('external_amenities', $e_amenity_details);
	}

	function deleteAmenityImage($external_amenity_id, $delete_image_name)
	{
		$amenity            = $this->getAmenity($external_amenity_id);
		$new_images_order = array();
		$update_images    = false;
		
		for($x=1; $x <=5; $x++){
			$field_name_old     = 'e_amenity_picture'.$x;
			if($update_images){
				$field_name_new                    = 'e_amenity_picture'.($x-1);
				$new_images_order[$field_name_new] = (string)$amenity->$field_name_old;
				continue;
			}
			$current_image_val  = $amenity->$field_name_old;
			if($delete_image_name == $current_image_val){
				$new_images_order[$field_name_old] = '';
				$update_images                     = true;
				$new_images_order['e_amenity_picture5']  = '';
				// remove the image from folder
				$image_path = BASEPATH."../../mpvs/images/external_amenities/ext_amenity_{$amenity->external_amenity_id}/{$delete_image_name}";
				if(file_exists($image_path)){
					unlink($image_path);
				}
			}
		}
		if(count($new_images_order)){
			$this->db->where('external_amenities.external_amenity_id', $external_amenity_id);
			return $this->db->update('external_amenities', $new_images_order);
		}
		return FALSE;
	}

	function deleteAmenity($external_amenity_id)
	{
		$this->db->where('external_amenity_id', $external_amenity_id);
		return $this->db->delete('external_amenities');
	}

	function getFields()
	{
		return array(
			'development_id',
			'e_amenity_type_id',
			'e_amenity_name',
			'e_amenity_latitude',
			'e_amenity_longitude',
			'e_amenity_description',
			'e_amenity_moreinfo_url',
			'e_amenity_icon',
			'e_amenity_address',
			'external_amenity_iframe_html',
			'show_e_amenity',
			'external_amenity_iframe_scrollbar',
		);
	}

	private function uniqueFileName($file_path, $file_name)
	{
		$counter       = 1;
		$new_file_name = $file_name;
		while (file_exists($file_path.$new_file_name)) {
			list($name, $extension) = explode('.', $file_name);
			$new_file_name = "{$name}({$counter}).{$extension}";
			$counter++;
		}
		return $new_file_name;
	}
}
?>