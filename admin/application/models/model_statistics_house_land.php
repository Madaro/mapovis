<?php
Class Model_statistics_house_land extends CI_Model
{
	function addLog($development_id, $filters)
	{
		$size_from = (isset($filters['lot_square_meters_from']))? $filters['lot_square_meters_from']: '';
		$size_to   = (isset($filters['lot_square_meters_to']))? '|'.$filters['lot_square_meters_to']: '';
		
		$houseland_price_from = (isset($filters['houseland_price_from']) && $filters['houseland_price_from'])? $filters['houseland_price_from']: NULL;
		$houseland_price_to   = (isset($filters['houseland_price_to']) && $filters['houseland_price_to'])? $filters['houseland_price_to']: NULL;
		$lot_square_meters    = ((isset($filters['lot_square_meters']) && !empty($filters['lot_square_meters'])))?  implode(',', $filters['lot_square_meters']): $size_from.$size_to;
		$house_bedrooms       = (isset($filters['house_bedrooms']) && $filters['house_bedrooms'])? $filters['house_bedrooms']: NULL;
		$house_bathrooms      = (isset($filters['house_bathrooms']) && $filters['house_bathrooms'])? $filters['house_bathrooms']: NULL;
		$builder_id           = (isset($filters['builder_id']) && $filters['builder_id'])? $filters['builder_id']: NULL;
		$house_levels         = (isset($filters['house_levels']) && $filters['house_levels'])? $filters['house_levels']: NULL;
		$order_by             = (isset($filters['order_by']) && $filters['order_by'])? $filters['order_by']: NULL;
		// Create record
		$log = array(
			'development_id'       => $development_id,
			'houseland_price_from' => $houseland_price_from,
			'houseland_price_to'   => $houseland_price_to,
			'lot_square_meters'    => $lot_square_meters,
			'house_bedrooms'       => $house_bedrooms,
			'house_bathrooms'      => $house_bathrooms,
			'builder_id'           => $builder_id,
			'house_levels'         => $house_levels,
			'order_by'             => $order_by,
			'datetime'             => date('Y-m-d H:i:s')
		);
		$this->db->insert('statistics_house_land', $log);
	}
    
    function getViewsByDay($development_id, $start_date, $end_date)
    {
        $this->db->select("COUNT(statistics_house_land.statistics_house_land_id) as views, DATE_FORMAT(statistics_house_land.datetime, '%Y-%m-%d') as views_date, DATE_FORMAT(statistics_house_land.datetime, '%d/%m') as formated_date ", FALSE);
        $this->db->from('statistics_house_land');
        $this->db->where('statistics_house_land.development_id', $development_id);
        $this->db->where('statistics_house_land.datetime  >=', $start_date);
        $this->db->where('statistics_house_land.datetime  <=', $end_date);
        /* change pretect identifiers to codeigniter will not escape %Y-%m-%d*/
        $this->db->_protect_identifiers = FALSE;
        $this->db->group_by("DATE_FORMAT(statistics_house_land.datetime, '%Y-%m-%d')", FALSE);
        $this->db->_protect_identifiers = TRUE;
        $this->db->order_by('views_date', 'asc');
        $query      = $this->db->get();
        $tmp_result = ($query->num_rows() > 0)? $query->result(): array();
        $result     = array();
        foreach($tmp_result as $row){
            $result[$row->views_date] = $row;
        }
        return $result;
    }
}
?>