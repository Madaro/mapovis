<?php
Class Model_import_e_amenities extends CI_Model
{
	public function getCsvAmenities($development_id){
		$this->load->model('Model_external_amenity', '', TRUE);
		$this->load->model('Model_external_amenity_type', '', TRUE);
		$e_amenities            = $this->Model_external_amenity->getAmenities($development_id);
		$file_name   = '/tmp/temp_ext_amenities.csv';
		$fp          = fopen($file_name, 'w');
		$csv_headers = array(
			'ext_amenity_id',
			'ext_amenity_name',
			'ext_amenity_type_id',
			'ext_amenity_type_name',
			'e_amenity_longitude',
			'e_amenity_latitude',
			'e_amenity_description',
			'e_amenity_moreinfo_url',
			'e_amenity_icon',
			'e_amenity_picture1',
			'e_amenity_picture2',
			'e_amenity_picture3',
			'e_amenity_picture4',
			'e_amenity_picture5',
			'external_amenity_iframe_html',
			'external_amenity_iframe_scrollbar',
			'show_e_amenity',
			'e_amenity_address'
		);
		fputcsv($fp, $csv_headers);
		foreach($e_amenities as $e_amenity){
			$csv_data      = array(
				$e_amenity->external_amenity_id,
				$e_amenity->e_amenity_name,
				$e_amenity->e_amenity_type_id,
				$e_amenity->e_amenity_type_name,
				$e_amenity->e_amenity_longitude,
				$e_amenity->e_amenity_latitude,
				$e_amenity->e_amenity_description,
				$e_amenity->e_amenity_moreinfo_url,
				$e_amenity->e_amenity_icon,
				$e_amenity->e_amenity_picture1,
				$e_amenity->e_amenity_picture2,
				$e_amenity->e_amenity_picture3,
				$e_amenity->e_amenity_picture4,
				$e_amenity->e_amenity_picture5,
				$e_amenity->external_amenity_iframe_html,
				$e_amenity->external_amenity_iframe_scrollbar,
				$e_amenity->show_e_amenity,
				$e_amenity->e_amenity_address,
			);
			fputcsv($fp, $csv_data);
		}
		fclose($fp);
		return file_get_contents($file_name);
	}

	public function getImportedFile($imported_file_id)
	{
		$query  = $this->db->get_where('imported_files', array('imported_files.imported_file_id' => $imported_file_id));
		$result = null;
		if($query->num_rows() > 0)
		{
			$result = $query->row();
		}
		return $result;
	}

	public function addImportFileToUpdate($development_id, $file_data)
	{
		$this->load->model('Model_user', '', TRUE);
		$user           = $this->Model_user->getAdminLoggedIn();
		$notes          = $this->importFileContentUpdateAmenities($development_id, $file_data);
		$formated_notes = '- '.implode("\n- ", $notes);
		$data           = array(
			'filename'     => $file_data['file_name'],
			'file_path'    => $file_data['file_path'],
			'import_date'  => date('Y-m-d'),
			'user_id'      => $user->user_id,
			'import_notes' => $formated_notes,
		);
		$this->db->insert('imported_files', $data);
		return $this->db->insert_id();
	}

	/********************************** IMPORT VALIDATION AND EXECUTION *******************************************/

	public function importFileContentUpdateAmenities($development_id, $file_details)
	{
		$this->load->model('Model_external_amenity', '', TRUE);
		$this->load->model('Model_external_amenity_type', '', TRUE);
		$this->load->library('mapovis_lib');
		$file_content           = $this->getFileContent($file_details);
		$e_amenities            = $this->Model_external_amenity->getAmenities($development_id);
		$e_amenity_types        = $this->Model_external_amenity_type->getExternalAmenityTypes($development_id);
		$sorted_amenities       = array_combine(array_map(function($v){return $v->external_amenity_id;}, $e_amenities), array_map(function($v){return $v->e_amenity_name;}, $e_amenities));
		$sorted_types           = array_combine(array_map(function($v){return $v->external_amenity_type_id;}, $e_amenity_types), array_map(function($v){return $v->e_amenity_type_name;}, $e_amenity_types));
		$sorted_amenities_names = array_flip($sorted_amenities);
		$sorted_types_names     = array_flip($sorted_types);
		$notes                  = array();
		$updatable_fields       = array(
			'e_amenity_longitude',
			'e_amenity_latitude',
			'e_amenity_description',
			'e_amenity_moreinfo_url',
			'e_amenity_icon',
			'e_amenity_picture1',
			'e_amenity_picture2',
			'e_amenity_picture3',
			'e_amenity_picture4',
			'e_amenity_picture5',
			'external_amenity_iframe_html',
			'external_amenity_iframe_scrollbar',
			'show_e_amenity',
			'e_amenity_address'
		);
		foreach($file_content['rows'] as $index => $row_content){
			$amenity_id   = @$row_content['ext_amenity_id'];
			$amenity_name = @$row_content['ext_amenity_name'];
			if(empty($amenity_name)){
				$notes[] = 'An External Amenity does not have a name line'.($index+1).'.';
				continue;
			}
			$amenity_data = array(
				'e_amenity_name' => $amenity_name,
			);
			if(isset($sorted_types[@$row_content['ext_amenity_type_id']])){
				$type_id = $row_content['ext_amenity_type_id'];
			}
			elseif(isset($sorted_types_names[@$row_content['ext_amenity_type_name']])){
				$type_id = $sorted_types_names[$row_content['ext_amenity_type_name']];
			}
			else{
				$notes[] = 'An External Amenity type for "'.$amenity_name.'" was not found '.$row_content['ext_amenity_type_id'].' '.$row_content['ext_amenity_type_name'].'.';
				continue;
			}
			foreach($updatable_fields as $field_name){
				if(isset($row_content[$field_name])){
					$amenity_data[$field_name] = $row_content[$field_name];
				}
			}
			$amenity_data['e_amenity_type_id'] = $type_id;
			if(isset($sorted_amenities[$amenity_id])){
				$this->Model_external_amenity->updateAmenity($amenity_id, $amenity_data);
				$notes[] = 'The External Amenity '.$amenity_id.' - "'.$amenity_name.'" was UPDATED.';
			}
			else if(empty($amenity_id) && isset($sorted_amenities_names[$amenity_name])){
				$amenity_id = $sorted_amenities_names[$amenity_name];
				$this->Model_external_amenity->updateAmenity($amenity_id, $amenity_data);
				$notes[]    = 'The External Amenity '.$amenity_id.' "'.$amenity_name.'" was UPDATED.';
			}
			else if(strtolower($amenity_id) == 'add'){
				// insert amenity
				$amenity_data['development_id'] = $development_id;
				$amenity_id                     = $this->Model_external_amenity->addAmenity($amenity_data);
				$sorted_amenities[$amenity_id]  = $amenity_name;
				$notes[]                        = 'The External amenity '.$amenity_id.' - "'.$amenity_name.'" was ADDED.';
			}
			else{
				$notes[] = 'The External Amenity '.$amenity_id.' - "'.$amenity_name.'" was not found .';
			}
		}
		return $notes;
	}

	private function getFileContent($file_details)
	{
		$file_path    = $file_details['full_path'];
		$file_data    = array();
		if(file_exists($file_path)){
			$file_handle  = fopen($file_path, 'r');
			/* Taske the first line of content with the headers*/
			$file_headers = fgetcsv($file_handle);
			while (!feof($file_handle) ) {
				$tmp_row     = fgetcsv($file_handle);
				if($tmp_row){
					/* Combine the headers and data to have a relative array header1 => val1, header2 => val2, etc*/
					$file_data[] = array_combine($file_headers, $tmp_row);
				}
			}
			fclose($file_handle);
		}
		return array('headers' => $file_headers, 'rows' => $file_data);
	}
}
