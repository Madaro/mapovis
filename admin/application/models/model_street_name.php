<?php
Class Model_street_name extends CI_Model
{
	function getStreetNames()
	{
		$this->db->order_by('street_name', 'asc');
		$query  = $this->db->get('street_names');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function getStreetName($street_name_id)
	{
		$query  = $this->db->get_where('street_names', array('street_names.street_name_id' => $street_name_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row();
		}
		return $result;
	}

	function addStreetName($form_data)
	{
		$fields             = $this->getFields();
		$street_name_fields = array();
		foreach($fields as $field_id){
			if(isset($form_data[$field_id])){
				$street_name_fields[$field_id] = $form_data[$field_id];
			}
		}
		$this->db->insert('street_names', $street_name_fields);
		return $this->db->insert_id();
	}

	private function getFields()
	{
		return array('street_name');
	}

}
?>