<?php
Class Model_import_house extends CI_Model
{
	public function getImportedFile($imported_file_id)
	{
		$query  = $this->db->get_where('imported_files', array('imported_files.imported_file_id' => $imported_file_id));
		$result = null;
		if($query->num_rows() > 0)
		{
			$result = $query->row();
		}
		return $result;
	}

	public function addImportFileToUpdate($file_data)
	{
		$this->load->model('Model_user', '', TRUE);
		$user           = $this->Model_user->getAdminLoggedIn();
		$notes          = $this->importFileContentUpdateHouses($file_data);
		$formated_notes = '- '.implode("\n- ", $notes);
		$data  = array(
			'filename'     => $file_data['file_name'],
			'file_path'    => $file_data['file_path'],
			'import_date'  => date('Y-m-d'),
			'user_id'      => $user->user_id,
			'import_notes' => $formated_notes,
		);
		$this->db->insert('imported_files', $data);
		return $this->db->insert_id();
	}

	/********************************** IMPORT VALIDATION AND EXECUTION *******************************************/

	public function importFileContentUpdateHouses($file_details)
	{
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_builder', '', TRUE);
		$this->load->library('mapovis_lib');
		$file_content       = $this->getFileContent($file_details);
		$houses             = $this->Model_house->getHouses(null);
		$houses_formatted   = array_combine(array_map(function($v){return $v->house_name;}, $houses), array_map(function($v){return $v->house_id;}, $houses));
		$notes              = '';
		$updatable_fields   = array(
			'house_builder_id',
			'house_description',
			'house_moreinfo_url',
			'house_bedrooms',
			'house_bathrooms',
			'house_garages',
			'house_size',
			'house_levels',
		);
		foreach($file_content['rows'] as $row_content){
			$house_name = $row_content['house_name'];
			$house_data = array(
				'development_id' => null,
				'house_name'     => $row_content['house_name'],
			);
			foreach($updatable_fields as $field_name){
				if(isset($row_content[$field_name])){
					$house_data[$field_name] = $row_content[$field_name];
				}
			}
			if(isset($houses_formatted[$house_name])){
				$house_id  = $houses_formatted[$house_name];
				if(isset($row_content['status']) && strtolower($row_content['status']) == 'delete'){
					// delete house
					$this->Model_house->deleteHouse($house_id);
					unset($houses_formatted[$house_name]);
					$notes[] = 'The House '.$house_name.' was DELETED.';
					continue;
				}

				// update house
				$this->Model_house->updateHouse($house_id, $house_data);
				$notes[]  = 'The House '.$house_name.' was UPDATED.';
			}
			else{
				$house_id                      = $this->Model_house->addHouse($house_data);
				$houses_formatted[$house_name] = $house_id;
				$notes[]                       = 'The House '.$house_name.' was ADDED.';
			}
		}
		return $notes;
	}

	private function getFileContent($file_details)
	{
		$file_path    = $file_details['full_path'];
		$file_data    = array();
		if(file_exists($file_path)){
			$file_handle  = fopen($file_path, 'r');
			/* Taske the first line of content with the headers*/
			$file_headers = fgetcsv($file_handle);
			while (!feof($file_handle) ) {
				$tmp_row     = fgetcsv($file_handle);
				if($tmp_row){
					/* Combine the headers and data to have a relative array header1 => val1, header2 => val2, etc*/
					$file_data[] = array_combine($file_headers, $tmp_row);
				}
			}
			fclose($file_handle);
		}
		return array('headers' => $file_headers, 'rows' => $file_data);
	}
}
