<?php
Class Model_house_image extends CI_Model
{
	/**
	 * Returns the house images
	 * @param int $house_id
	 * @return array
	 */
	function getHouseImages($house_id, $filter = array())
	{
		$this->db->select('house_images.*');
		$this->db->where('house_images.house_id', $house_id);
		if(isset($filter['file_type']) && $filter['file_type']){
			$this->db->where('house_images.file_type', $filter['file_type']);
		}
		if(isset($filter['lot_id']) && $filter['lot_id']){
			$this->db->select('CONCAT(COALESCE(house_lot_order_images.house_lot_order_image_id,999), "-", house_images.file_order) AS mixed_order', FALSE);
			$this->db->order_by('mixed_order', 'asc');
			$this->db->join('house_lot_order_images', 'house_lot_order_images.house_image_id = house_images.house_image_id AND house_lot_order_images.lot_id = '.$filter['lot_id'], 'left');
		}
		$this->db->order_by('house_images.file_order', 'asc');
		$query  = $this->db->get('house_images');
		return ($query)? $query->result(): array();
	}

	/**
	 * Returns the house images
	 * @param int $house_id
	 * @return array
	 */
	function getFirstHousesImages($house_ids, $lot_ids, $filter = array())
	{
		if(empty($house_ids)){
			return array();
		}
		$this->db->select('house_images.*');
		$this->db->where_in('house_images.house_id', $house_ids);
		$this->db->select('house_images.*, house_lot_packages.lot_id');
		$this->db->join('house_lot_packages', 'house_images.house_id = house_lot_packages.house_id','left');
		$this->db->join('house_lot_order_images', 'house_lot_order_images.house_image_id = house_images.house_image_id AND house_lot_order_images.lot_id = house_lot_packages.lot_id ', 'left');
		$this->db->where_in('house_lot_packages.lot_id', $lot_ids);
		$this->db->where('(house_lot_order_images.file_order = 1 OR (house_lot_order_images.file_order is null AND house_images.file_order = 1)) ');
		if(isset($filter['file_type']) && $filter['file_type']){
			$this->db->where('house_images.file_type', $filter['file_type']);
		}
		if(isset($filter['group_by_lot']) && $filter['group_by_lot']){
			$this->db->group_by('house_lot_packages.lot_id');
		}
		$this->db->order_by('house_images.file_order', 'asc');
		$this->db->group_by('house_images.house_id');
		$query  = $this->db->get('house_images');
		return ($query)? $query->result(): array();
	}

	/**
	 * Returns the house images
	 * @param int $house_id
	 * @return array
	 */
	function getAllHousesImages($house_ids, $lot_ids, $filter = array())
	{
		if(empty($house_ids)){
			return array();
		}
		$this->db->select('house_images.*');
		$this->db->where_in('house_images.house_id', $house_ids);
		$this->db->select('house_images.*, house_lot_packages.lot_id');
		$this->db->join('house_lot_packages', 'house_images.house_id = house_lot_packages.house_id','left');
		$this->db->join('house_lot_order_images', 'house_lot_order_images.house_image_id = house_images.house_image_id AND house_lot_order_images.lot_id = house_lot_packages.lot_id ', 'left');
		$this->db->where_in('house_lot_packages.lot_id', $lot_ids);
		if(isset($filter['file_type']) && $filter['file_type']){
			$this->db->where('house_images.file_type', $filter['file_type']);
		}
		$this->db->group_by('house_images.house_image_id');
		if(isset($filter['group_by_lot']) && $filter['group_by_lot']){
			$this->db->group_by('house_lot_packages.lot_id');
		}
		$this->db->order_by('house_images.house_id');
		$this->db->order_by('house_images.file_order', 'asc');
		$this->db->group_by('house_images.house_id');
		$query  = $this->db->get('house_images');
		return ($query)? $query->result(): array();
	}

	function getHouseImage($house_image_id)
	{
		$this->db->where('house_images.house_image_id', $house_image_id);
		$query  = $this->db->get('house_images');
		return ($query)? $query->row(): FALSE;
	}

	/**
	 * Updates the current images or save new ones
	 * 
	 * @param int $house_id
	 * @param array $form_data
	 * @return boolean
	 */
	function updateHouseImages($house_id, $form_data)
	{
		$new_file_path = BASEPATH."../../mpvs/images/dev_houses/";
		if (!file_exists("{$new_file_path}house_{$house_id}/")) {
			mkdir("{$new_file_path}house_{$house_id}/", 0777, true);
		}
		$image_order = 1;
		// update and insert new images
		if(isset($form_data['image_names_facade'])){
			foreach($form_data['image_names_facade'] as $image_name){
				list($image_id_index, $image_name_url) = explode('||', $image_name);

				// if the image comes from an existing house the files will be copied
				if($image_name_url == 'copyExisting'){
					$copy_house_image   = $this->getHouseImage($image_id_index);
					if(!$copy_house_image){
						continue;
					}
					$new_filename       = str_replace('house_'.$copy_house_image->house_id, 'house_'.$house_id, $copy_house_image->file_name);
					$original_filename  = str_replace('house_'.$copy_house_image->house_id, 'house_'.$house_id, $copy_house_image->original_file_name);
					$new_filename       = $this->uniqueFileName($new_file_path, $new_filename);
					$original_filename  = $this->uniqueFileName($new_file_path, $original_filename);
					copy($new_file_path.$copy_house_image->file_name, $new_file_path.$new_filename);
					if($original_filename != $new_filename){
						copy($new_file_path.$copy_house_image->original_file_name, $new_file_path.$original_filename);
					}
					$new_image          = array(
						'house_id'           => $house_id,
						'file_name'          => $new_filename,
						'original_file_name' => $original_filename,
						'file_type'          => 'facade',
						'file_order'         => $image_order,
					);
					$this->db->insert('house_images', $new_image);
				}
				// if the image comes from an url, Aviary, the file content will be retrieved and stored
				elseif($image_name_url && filter_var($image_name_url, FILTER_VALIDATE_URL)){
					$file_name    = "house_{$house_id}/".basename($image_name_url);
					$new_filename = $this->uniqueFileName($new_file_path, $file_name);
					file_put_contents($new_file_path.$new_filename, file_get_contents($image_name_url));
					$new_image    = array(
						'house_id'           => $house_id,
						'file_name'          => $new_filename,
						'original_file_name' => $new_filename,
						'file_type'          => 'facade',
						'file_order'         => $image_order,
					);
					if(isset($form_data['original_image'][$image_id_index])){
						$tmp_file_path      = BASEPATH.'../../tmp/';
						$tmp_original_image = $form_data['original_image'][$image_id_index];
						list($image_folder, $image_name) = explode('/', $tmp_original_image);
						$original_filename               = $this->uniqueFileName($new_file_path, str_replace(' ', '_', "house_{$house_id}/".$image_name));
						if (file_exists($tmp_file_path.$tmp_original_image) && rename($tmp_file_path.$tmp_original_image, $new_file_path.$original_filename)) {
							$new_image['original_file_name'] = $original_filename;
						}
					}
					$this->db->insert('house_images', $new_image);
				}
				// if the image already exists in this house only the order will be udpated
				else{
					$this->db->where('house_images.house_image_id', $image_id_index);
					$this->db->update('house_images', array('file_order' => $image_order));
				}
				$image_order++;
			}
		}
		// update and insert new Floor plans images
		$image_order = 1;
		if(isset($form_data['image_names_floor'])){
			foreach($form_data['image_names_floor'] as $image_name){
				list($image_id_index, $image_name_url) = explode('||', $image_name);

				if($image_name_url && filter_var($image_name_url, FILTER_VALIDATE_URL)){
					$file_name    = "house_{$house_id}/".basename($image_name_url);
					$new_filename = $this->uniqueFileName($new_file_path, $file_name);
					file_put_contents($new_file_path.$new_filename, file_get_contents($image_name_url));
					$new_image    = array(
						'house_id'           => $house_id,
						'file_name'          => $new_filename,
						'original_file_name' => $new_filename,
						'file_type'          => 'floor_plan',
						'file_order'         => $image_order,
					);
					$this->db->insert('house_images', $new_image);
				}
				else{
					$this->db->where('house_images.house_image_id', $image_id_index);
					$this->db->update('house_images', array('file_order' => $image_order));
				}
				$image_order++;
			}
		}
		// update and insert new gallery images
		$image_order = 1;
		if(isset($form_data['image_names_gallery'])){
			foreach($form_data['image_names_gallery'] as $image_name){
				list($image_id_index, $image_name_url) = explode('||', $image_name);

				if($image_name_url && filter_var($image_name_url, FILTER_VALIDATE_URL)){
					$file_name    = "house_{$house_id}/".basename($image_name_url);
					$new_filename = $this->uniqueFileName($new_file_path, $file_name);
					file_put_contents($new_file_path.$new_filename, file_get_contents($image_name_url));
					$new_image    = array(
						'house_id'           => $house_id,
						'file_name'          => $new_filename,
						'original_file_name' => $new_filename,
						'file_type'          => 'gallery',
						'file_order'         => $image_order,
					);
					$this->db->insert('house_images', $new_image);
				}
				else{
					$this->db->where('house_images.house_image_id', $image_id_index);
					$this->db->update('house_images', array('file_order' => $image_order));
				}
				$image_order++;
			}
		}
		return TRUE;
	}

	function updateGlobalHouseImages($house_id, $form_data)
	{
		if(isset($form_data['image_names_facade'])){
			foreach($form_data['image_names_facade'] as $image_name){
				$this->updateGlobalHouseImage($house_id, $image_name, $form_data);
			}
		}
		return TRUE;
	}

	function updateGlobalHouseImage($house_id, $image_name_facade, $form_data)
	{
		@list($image_id_index, $image_name_url) = explode('||', $image_name_facade);

		// if the image comes from an url, Aviary, the file content will be retrieved and stored otherwise ignored
		if($image_name_url && filter_var($image_name_url, FILTER_VALIDATE_URL)){
			$new_file_path = BASEPATH."../../mpvs/images/dev_houses/";
			if (!file_exists("{$new_file_path}house_{$house_id}/")) {
				mkdir("{$new_file_path}house_{$house_id}/", 0777, true);
			}
			$current_images = $this->getHouseImages($house_id, array('file_type' => 'facade'));
			$image_order    = count($current_images) + 1;
			$file_name      = "house_{$house_id}/".basename($image_name_url);
			$new_filename   = $this->uniqueFileName($new_file_path, $file_name);
			file_put_contents($new_file_path.$new_filename, file_get_contents($image_name_url));
			$new_image      = array(
				'house_id'           => $house_id,
				'file_name'          => $new_filename,
				'original_file_name' => $new_filename,
				'file_type'          => 'facade',
				'file_order'         => $image_order,
			);
			if(isset($form_data['original_image'][$image_id_index])){
				$tmp_file_path      = BASEPATH.'../../tmp/';
				$tmp_original_image = $form_data['original_image'][$image_id_index];
				list($image_folder, $image_name) = explode('/', $tmp_original_image);
				$original_filename               = $this->uniqueFileName($new_file_path, str_replace(' ', '_', "house_{$house_id}/".$image_name));
				if (file_exists($tmp_file_path.$tmp_original_image) && rename($tmp_file_path.$tmp_original_image, $new_file_path.$original_filename)) {
					$new_image['original_file_name'] = $original_filename;
				}
			}
			$this->db->insert('house_images', $new_image);
			return $this->db->insert_id();
		}
		return FALSE;
	}

	function uploadoriginalimage(){
		$folder    = date('Ymd').'/';
		$file_path = BASEPATH.'../../tmp/';
		if (!file_exists($file_path.$folder)) {
			mkdir($file_path.$folder, 0777, true);
		}
		if(isset($_FILES['image_file_facade']['name'])){
			$file_name = $this->uniqueFileName($file_path, $folder.str_replace(' ', '_', $_FILES['image_file_facade']['name']));
			if (file_exists($_FILES['image_file_facade']['tmp_name']) && move_uploaded_file($_FILES['image_file_facade']['tmp_name'], $file_path.$file_name)) {
				return $file_name;
			}
		}
		return FALSE;
	}

	function deleteHouseImage($house_image_id)
	{
		$house_image = $this->getHouseImage($house_image_id);
		$image_order = $house_image->file_order;

		$this->load->model('Model_house_lot_order_image', '', TRUE);
		$this->Model_house_lot_order_image->deleteHouseImage($house_image_id);

		// remove the images from folder
		$image_path          = BASEPATH."../../mpvs/images/dev_houses/{$house_image->file_name}";
		$original_image_path = BASEPATH."../../mpvs/images/dev_houses/{$house_image->original_file_name}";
		if(file_exists($image_path)){
			unlink($image_path);
		}
		if(file_exists($original_image_path)){
			unlink($original_image_path);
		}

		// delete house image record
		$this->db->where('house_image_id', $house_image_id);
		$this->db->delete('house_images');

		// update house images order
		$this->db->set('file_order', 'file_order - 1', FALSE);
		$this->db->where('house_id', $house_image->house_id);
		$this->db->where('file_type', $house_image->file_type);
		$this->db->where('file_order >', $image_order);
		return $this->db->update('house_images');
	}

	public function uniqueFileName($file_path, $file_name)
	{
		$counter       = 1;
		$new_file_name = $file_name;
		while (file_exists($file_path.$new_file_name)) {
			list($name, $extension) = explode('.', $file_name);
			$new_file_name = "{$name}({$counter}).{$extension}";
			$counter++;
		}
		return $new_file_name;
	}
}
?>