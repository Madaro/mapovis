<?php
Class Model_house_match extends CI_Model
{
	/**
	 * Returns house matchess for given development
	 * @param int $development_id
	 * @return array
	 */
	function getHouseMatches($lot_id)
	{
		$this->db->select('builders.*, houses.*');
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id', 'left');
		$this->db->join('house_matches_tmp', 'house_matches_tmp.house_id = houses.house_id');
		$this->db->order_by('house_matches_tmp.order');
		$query  = $this->db->get_where('houses', array('house_matches_tmp.lot_id' => $lot_id));
		$results = array();
		if ($query->num_rows() > 0)
		{
			$results = $query->result();
		}
		$ordered_matches = array();
		foreach($results as $result){
			$ordered_matches[$result->house_id] = $result;
		}
		return $ordered_matches;
	}

	/**
	 * Updates the lot matched houses
	 * 
	 * @param int $lot_id
	 * @param array $house_matches ids
	 * @return boolean
	 */
	function updateLotHouses($lot_id, $house_matches)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);
		$this->db->where('lot_id', $lot_id);
		$this->db->update('house_lot_packages', array('active' => 0));
		foreach($house_matches as $house_id){
			$this->Model_house_lot_package->enableDisableHouseLotPdf($lot_id, $house_id, TRUE, NULL);
		}
		return true;
	}
}
?>