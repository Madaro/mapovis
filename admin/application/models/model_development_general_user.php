<?php
Class Model_development_general_user extends CI_Model
{
	public function getDevelopmentUsers($development_id)
	{
		$this->db->where('development_general_users.development_id', $development_id);
		$query  = $this->db->get('development_general_users');
		$results = array();
		if ($query->num_rows() > 0)
		{
			$results = $query->result();
		}
		$ordered_list = array();
		foreach($results as $result){
			$ordered_list[$result->general_user_id] = $result;
		}
		return $ordered_list;
	}

	public function getDevelopmentUser($development_id, $user_id)
	{
		$this->db->where('development_general_users.development_id', $development_id);
		$this->db->where('development_general_users.general_user_id', $user_id);
		$query  = $this->db->get('development_general_users');
		$result = $query->row();
		return ($result);
	}

	public function addDevelopmentUser($development_id, $user_id)
	{
		$this->db->where('development_general_users.development_id', $development_id);
		$this->db->where('development_general_users.general_user_id', $user_id);
		$query  = $this->db->get('development_general_users');
		$result = $query->row();
		if(!$result){
			$general_user = array('general_user_id' => $user_id, 'development_id' => $development_id);
			return $this->db->insert('development_general_users', $general_user);
		}
		return TRUE;
	}

	public function deleteDevelopmentUser($development_id, $user_id)
	{
		$this->db->where('development_general_users.development_id', $development_id);
		$this->db->where('development_general_users.general_user_id', $user_id);
		return $this->db->delete('development_general_users');
	}
}
?>