<?php
Class Model_hnl_change_log extends CI_Model
{
	public function getHnLChangeLogs($filters = array(), $with_time_formatted = TRUE, $limit =	2000)
	{
		$this->db->select('hnl_change_logs.*, houses.*, lots.*, stages.*, precincts.*');
		$this->db->select('developments.development_name, developments.development_id, developers.developer AS developer_name, users.name AS user_name');
		$this->db->join('houses', 'houses.house_id = hnl_change_logs.house_id', 'left');
		$this->db->join('lots', 'lots.lot_id = hnl_change_logs.lot_id', 'left');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left');
		$this->db->join('developments', 'developments.development_id = precincts.development_id', 'left');
		$this->db->join('developers', 'developers.developer_id = developments.developerid', 'left');
		$this->db->join('users', 'users.user_id = hnl_change_logs.user_id', 'left');

		if(isset($filters['development_id']) && $filters['development_id']){
			$this->db->where('developments.development_id', $filters['development_id']);
		}
		if(isset($filters['development_ids'])){
			$this->db->where_in('developments.development_id', $filters['development_ids']);
		}
		if(isset($filters['from_datetime']) && $filters['from_datetime']){
			$this->db->where('hnl_change_logs.datetimezone >=', $filters['from_datetime']);
		}
		if(isset($filters['notes']) && !empty($filters['notes'])){
			if(is_array($filters['notes'])){
				$this->db->where('(hnl_change_logs.action_note LIKE "%'. implode('%" OR hnl_change_logs.action_note LIKE "%', $filters['notes']). '%")');
			}
			else{
				$this->db->like('hnl_change_logs.notes', $filters['notes']);
			}
		}
		$this->db->order_by('hnl_change_logs.datetime', 'desc');

		$query  = $this->db->get('hnl_change_logs', $limit);
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		if($with_time_formatted){
			$this->load->library('mapovis_lib');
			$timezone = $this->mapovis_lib->getTimeZone();
			foreach($result as $row){
				// chaning time to default time zone
				$row_datetime   = date_create(date('Y-m-d H:i:s', $row->datetime));
				$row_datetime->setTimeZone(timezone_open($timezone));

				$row->formatted_date = date_format($row_datetime, 'd/m/Y');
				$row->formatted_time = date_format($row_datetime, 'h:i a');
				$row->formatted_diff = $this->mapovis_lib->getTimeDifference($row->datetime);
			}
		}
		return $result;
	}

	public function addHnLChangeLog($action, $changes, $ids, $user)
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_user', '', TRUE);

		$development_id = (isset($ids->development_id))? $ids->development_id: 0;
		$user_role      = $this->Model_development->getDevelopmentRole($development_id, $user);

		$log_data       = array(
			'user_id'        => $user->user_id,
			'user_type'      => $user_role,
			'development_id' => $development_id,
			'lot_id'         => (isset($ids->lot_id)? $ids->lot_id: NULL),
			'house_id'       => (isset($ids->house_id)? $ids->house_id: NULL),
			'datetime'       => time(),
			'action'         => $action,
			'action_note'    => $changes,
		);
		$this->db->insert('hnl_change_logs', $log_data);
		return $this->db->insert_id();
	}

	function deleteDevelopmentLogs($development_id)
	{
		$this->db->where('hnl_change_logs.development_id', $development_id);
		return $this->db->delete('hnl_change_logs');
	}

}
?>