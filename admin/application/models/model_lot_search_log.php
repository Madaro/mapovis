<?php
Class Model_lot_search_log extends CI_Model
{
	function getLotSearchLogs()
	{
		$query  = $this->db->get('lot_search_logs');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function adLog($form_data)
	{
		$fields = $this->getFields();
		$data   = array();
		foreach($fields as $field_id){
			if(isset($form_data[$field_id])){
				$data[$field_id] = $form_data[$field_id];
			}
		}
		return $this->db->insert('lot_search_logs', $data);
	}


	private function getFields()
	{
		return array(
			'development_id',
			'datetime',
			'ipaddress',
			'browser',
			'price_from',
			'price_to',
			'size_from',
			'size_to',
			'width_from',
			'width_to',
		);
	}
}
?>