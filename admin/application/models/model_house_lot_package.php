<?php
Class Model_house_lot_package extends CI_Model
{
	function getHouseLotPackages($lot_id, $filters = array())
	{
		$this->db->select('house_lot_packages.*, houses.house_name, houses.development_id');
		$this->db->join('houses', 'houses.house_id = house_lot_packages.house_id');
		$this->db->where('house_lot_packages.lot_id', $lot_id);
		if(isset($filters['builder_id']) && $filters['builder_id']){
			$this->db->where('houses.house_builder_id', $filters['builder_id']);
		}
		$this->db->order_by('houses.house_name', 'asc');
		$query  = $this->db->get_where('house_lot_packages', array('house_lot_packages.lot_id' => $lot_id));
		$results = array();
		if ($query->num_rows() > 0)
		{
			$results = $query->result();
		}
		$sorted_results = array();
		foreach($results as $result){
			$sorted_results[$result->house_id] = $result;
		}
		return $sorted_results;
	}

	function getHouseLotPackagesByBuilder($builder_id)
	{
		$this->db->select('house_lot_packages.*, houses.house_name, lots.lot_number, developments.development_name, stages.stage_number, precincts.precinct_number, builder_sales_people.name AS sales_person');
		$this->db->join('lots', 'lots.lot_id = house_lot_packages.lot_id');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('developments', 'developments.development_id = precincts.development_id');
		$this->db->join('houses', 'houses.house_id = house_lot_packages.house_id');
		$this->db->join('builder_sales_people', 'builder_sales_people.builder_sales_person_id = house_lot_packages.builder_sales_person_id', 'left');
		$this->db->where('houses.house_builder_id', $builder_id);
		$this->db->where('lots.status', 'Available');
		$this->db->where('developments.active', 1);
		$this->db->where('house_lot_packages.active', 1);
		$this->db->order_by('houses.house_name', 'asc');
		$query          = $this->db->get('house_lot_packages');
		$results        = ($query)? $query->result(): array();
		$sorted_results = array();
		foreach($results as $result){
			$sorted_results[$result->house_id] = $result;
		}
		return $sorted_results;
	}

	function getHouseLotPackagesByDevelopment($development_id)
	{
		$this->db->select('house_lot_packages.*, houses.development_id AS house_development_id, houses.house_name, lots.lot_number, builders.builder_name, developments.custom_house_facade_width, developments.custom_house_facade_height');
		$this->db->join('lots', 'lots.lot_id = house_lot_packages.lot_id');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('developments', 'developments.development_id = precincts.development_id');
		$this->db->join('houses', 'houses.house_id = house_lot_packages.house_id');
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id', 'left');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->where('lots.status', 'Available');
		$this->db->where('house_lot_packages.active', 1);
		$this->db->order_by('houses.house_name', 'asc');
		$query          = $this->db->get('house_lot_packages');
		$results        = ($query)? $query->result(): array();
		return $results;
		$sorted_results = array();
		foreach($results as $result){
			$sorted_results[$result->house_id] = $result;
		}
		return $sorted_results;
	}

	function getHouseLotPackage($lot_id, $house_id)
	{
		$this->db->select('house_lot_packages.*, houses.*, builders.builder_name, houses.development_id');
		$this->db->join('houses', 'houses.house_id = house_lot_packages.house_id');
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id', 'left');
		$this->db->where('house_lot_packages.lot_id', $lot_id);
		$this->db->where('house_lot_packages.house_id', $house_id);
		$query  = $this->db->get('house_lot_packages');
		$result = $query->row();
		if($result){
			$result->formated_house_lot_price = number_format($result->house_lot_price);
			$result->formatted_price          = ($result->house_lot_price)? '$'. $result->formated_house_lot_price: 'n/a';
		}
		return $result;
	}

	function updateHouseLot($lot_id, $house_id, $form_data, $user)
	{
		$this->load->model('Model_hnl_change_log', '', TRUE);

		$house_lot_package = $this->getHouseLotPackage($lot_id, $house_id);
		$update_data       = array();
		$log_changes       = array();
		if(isset($form_data['house_lot_price']) && @$house_lot_package->house_lot_price != (float)$form_data['house_lot_price']){
			$update_data['house_lot_price'] = (float)$form_data['house_lot_price'];
			$log_changes[]                  = 'price to $'.  number_format($update_data['house_lot_price']);
		}
		if(isset($form_data['active']) && @$house_lot_package->active != (int)$form_data['active']){
			$update_data['active'] = (int)$form_data['active'];
			$log_changes[]         = 'status to '.($update_data['active'] == 0? 'Disabled': 'Enabled');
		}
		if(isset($form_data['description']) && @$house_lot_package->description != $form_data['description']){
			$update_data['description'] = $form_data['description'];
			$log_changes[]              = 'description to '.$form_data['description'];
		}
		if(isset($form_data['is_feature']) && @$house_lot_package->is_feature != $form_data['is_feature']){
			$update_data['is_feature'] = $form_data['is_feature'];
			$log_changes[]              = ($form_data['is_feature'])? 'Featured': 'No featured';
		}
		if(isset($form_data['builder_sales_person_id']) && @$house_lot_package->builder_sales_person_id != $form_data['builder_sales_person_id']){
			$this->load->model('Model_builder_sales_person', '', TRUE);
			$update_data['builder_sales_person_id'] = ($form_data['builder_sales_person_id'])? $form_data['builder_sales_person_id']: NULL;
			$sales_person  = ($update_data['builder_sales_person_id'])? $this->Model_builder_sales_person->getSalesPerson($update_data['builder_sales_person_id']): FALSE;
			$log_changes[] = 'sales person to '.($sales_person == FALSE? 'none': $sales_person->name);
		}
		if(!empty($_FILES['house_lot_pdf']) && $_FILES['house_lot_pdf']['type'] == 'application/pdf'){
			$this->load->model('Model_lot', '', TRUE);
			$this->load->model('Model_house', '', TRUE);
			$this->load->model('Model_development', '', TRUE);

			$lot         = $this->Model_lot->getLot($lot_id);
			$house       = $this->Model_house->getHouse($house_id);
			$development = $this->Model_development->getDevelopment($lot->development_id);

			$file_path  = BASEPATH."../../mpvs/templates/lot_house_pdfs/";
			if (!file_exists("{$file_path}")) {
				mkdir("{$file_path}/", 0777, true);
			}
			$file_name      = str_replace(' ', '-', "{$development->development_name}-{$house->house_name}-by-{$house->builder_name}-Lot_{$lot->lot_number}.pdf");
			$file_path_name = $file_path.$file_name;
			if(file_exists($file_path_name)){
				unlink($file_path_name);
			}
			// Save PDF file
			$update_data['file_name']          = $file_name;
			$log_changes[]                     = "PDF file {$file_name}";
			move_uploaded_file($_FILES['house_lot_pdf']['tmp_name'], $file_path_name);
		}

		if($house_lot_package && $house_lot_package->house_lot_package_id){
			if(count($update_data)){
				$this->db->where('house_lot_packages.house_lot_package_id', $house_lot_package->house_lot_package_id);
				if($this->db->update('house_lot_packages', $update_data)){
					$newpackage       = $this->getHouseLotPackage($lot_id, $house_id);
					$all_log_changes  = "Edited H&L package for {$newpackage->house_name} ({$user->name}).\nChanged ".  implode("\nChanged ", $log_changes);
					$this->Model_hnl_change_log->addHnLChangeLog('Edit', $all_log_changes, $newpackage, $user);
					return TRUE;
				}
				return FALSE;
			}
			return TRUE;
		}
		else{
			if(empty($update_data)){
				return FALSE;
			}
			$update_data['lot_id']   = $lot_id;
			$update_data['house_id'] = $house_id;
			$this->db->insert('house_lot_packages', $update_data);
			$house_land_id = $this->db->insert_id();
			if($house_land_id){
				$newpackage       = $this->getHouseLotPackage($lot_id, $house_id);
				$all_log_changes  = "Added H&L package for {$newpackage->house_name} ({$user->name}).\n". str_replace(' to ', ': ', implode("\n", $log_changes));
				$this->Model_hnl_change_log->addHnLChangeLog('Add', $all_log_changes, $newpackage, $user);
			}
			return $house_land_id;
		}
	}

	function enableDisableHouseLotPdf($lot_id, $house_id, $enable_disable, $user)
	{
		$this->load->model('Model_hnl_change_log', '', TRUE);
		$house_lot_package = $this->getHouseLotPackage($lot_id, $house_id);
		$update_data       = array('active' => $enable_disable);
		$enable_disabled  = ($enable_disable == TRUE)? 'Enable': 'Disable';
		if($house_lot_package && $house_lot_package->house_lot_package_id){
			$this->db->where('house_lot_packages.house_lot_package_id', $house_lot_package->house_lot_package_id);
			if($this->db->update('house_lot_packages', $update_data)){
				if($user !== NULL){
					$newpackage       = $this->getHouseLotPackage($lot_id, $house_id);
					$log_changes  = "{$enable_disabled}d H&L package for {$newpackage->house_name} ({$user->name}).";
					$this->Model_hnl_change_log->addHnLChangeLog($enable_disabled, $log_changes, $newpackage, $user);
				}
				return TRUE;
			}
			return FALSE;
		}
		else{
			$update_data['lot_id']   = $lot_id;
			$update_data['house_id'] = $house_id;
			$this->db->insert('house_lot_packages', $update_data);
			$house_land_id = $this->db->insert_id();
			if($house_land_id){
				if($user !== NULL){
					$newpackage       = $this->getHouseLotPackage($lot_id, $house_id);
					$log_changes  = "{$enable_disabled}d H&L package for {$newpackage->house_name} ({$user->name}).";
					$this->Model_hnl_change_log->addHnLChangeLog($enable_disabled, $log_changes, $newpackage, $user);
				}
			}
			return $house_land_id;
		}
	}

	function deleteHouseLotPdf($lot_id, $house_id, $user)
	{
		$this->load->model('Model_hnl_change_log', '', TRUE);
		$house_lot_package = $this->getHouseLotPackage($lot_id, $house_id);
		if($house_lot_package){
			$file_path      = BASEPATH."../../mpvs/templates/lot_house_pdfs/";
			$file_path_name = $file_path.$house_lot_package->file_name;
			if(file_exists($file_path_name)){
				unlink($file_path_name);
			}
			$update_data['file_name']          = '';
			$this->db->where('house_lot_packages.house_lot_package_id', $house_lot_package->house_lot_package_id);
			if($this->db->update('house_lot_packages', $update_data)){
				$all_log_changes  = "Delete H&L package PDF File for {$house_lot_package->house_name} ({$user->name}).";
				$this->Model_hnl_change_log->addHnLChangeLog('Delete-File', $all_log_changes, $house_lot_package, $user);
				return TRUE;
			}
			return FALSe;
		}
	}
}
?>