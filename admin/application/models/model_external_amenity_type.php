<?php
Class Model_external_amenity_type extends CI_Model
{
	function getExternalAmenityTypes($development_id, $alwayson = NULL)
	{
		$this->db->select('external_amenity_types.*, COUNT(external_amenities.e_amenity_type_id) AS linked');
		$this->db->join('external_amenities', 'external_amenity_types.external_amenity_type_id = external_amenities.e_amenity_type_id', 'left');
		$this->db->where('external_amenity_types.development_id', $development_id);
		if($alwayson === FALSE){
			$this->db->where('external_amenity_types.e_amenity_type_name !=', 'ALWAYSON');
		}
		elseif($alwayson === TRUE){
			$this->db->where('external_amenity_types.e_amenity_type_name', 'ALWAYSON');
		}
		$this->db->group_by('external_amenity_types.external_amenity_type_id', 'asc');
		// Andrew commented out the below line so that the categories would be sorted by ID number and not in alphabetical order. 
		//$this->db->order_by('external_amenity_types.e_amenity_type_name');
		$query  = $this->db->get('external_amenity_types');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result(); 
		}
		return $result;
	}

	function getExternalAmenityType($amenity_type_id)
	{
		$query  = $this->db->get_where('external_amenity_types', array('external_amenity_type_id' => $amenity_type_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row(); 
		}
		return $result;
	}

	function addExternalAmenityType($development_id, $form_data)
	{
		$fields       = $this->getFields();
		$type_details = array('development_id' => $development_id);
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$type_details[$field] = $form_data[$field];
			}
		}
		if(isset($type_details['e_amenity_cluster_text_hex_colour'])){
			$type_details['e_amenity_cluster_text_hex_colour'] = str_replace('#', '', $type_details['e_amenity_cluster_text_hex_colour']);
		}
		$this->db->insert('external_amenity_types', $type_details);
		return $this->db->insert_id();
	}

	function updateExternalAmenityType($external_amenity_type_id, $form_data)
	{
		$fields       = $this->getFields();
		$type_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$type_details[$field] = $form_data[$field];
			}
		}
		if(isset($type_details['e_amenity_cluster_text_hex_colour'])){
			$type_details['e_amenity_cluster_text_hex_colour'] = str_replace('#', '', $type_details['e_amenity_cluster_text_hex_colour']);
		}
		if(count($type_details)){
			$this->db->where('external_amenity_type_id', $external_amenity_type_id);
			return $this->db->update('external_amenity_types', $type_details);
		}
		return TRUE;
	}

	function deleteExternalAmenityType($external_amenity_type_id)
	{
		$this->db->where('external_amenity_type_id', $external_amenity_type_id);
		return $this->db->delete('external_amenity_types');
	}

	function getFields()
	{
		return array(
			'e_amenity_type_name',
			'e_amenity_icon',
			'e_amenity_icon_unselected',
			'e_amenity_pin',
			'e_amenity_center_longitude',
			'e_amenity_center_latitude',
			'e_amenity_zoom_level',
			'e_amenity_icon_width',
			'e_amenity_icon_height',
			'e_amenity_pin_width',
			'e_amenity_pin_height',
			'e_amenity_cluster_circle_url',
			'e_amenity_cluster_circle_height',
			'e_amenity_cluster_circle_width',
			'e_amenity_cluster_text_hex_colour',
			'e_amenity_cluster_text_size',
		);
	}

}
?>