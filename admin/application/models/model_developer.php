<?php
Class Model_developer extends CI_Model
{
	function getDevelopers($indexID = FALSE)
	{
		$this->db->select('developers.*, COUNT(developments.development_id) as total_developments');
		$this->db->join('developments', 'developments.developerid = developers.developer_id', 'left');
		$this->db->group_by('developers.developer_id');
		$this->db->order_by('developer', 'asc');
		$query  = $this->db->get('developers');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			if($indexID){
				$tmp_results = array();
				foreach($result as $result_row)
				{
					$tmp_results[$result_row->developer_id] = $result_row;
				}
				$result = $tmp_results;
			}
		}
		return $result;
	}

	function getDeveloper($developer_id)
	{
		$this->db->WHERE('developer_id', $developer_id);
		$query  = $this->db->get('developers');
		return $query->row();
	}

	public function addDeveloper($developer_data)
	{
		$data   = array(
			'developer'         => $developer_data['developer'],
			'background_colour' => str_replace('#', '', $developer_data['background_colour']),
			'foreground_colour' => str_replace('#', '', $developer_data['foreground_colour']),
		);
		$this->db->insert('developers', $data);
		return $this->db->insert_id();
	}

	public function updateDeveloper($developer_id, $developer_data){
		$data   = array(
			'developer'         => $developer_data['developer'],
			'background_colour' => str_replace('#', '', $developer_data['background_colour']),
			'foreground_colour' => str_replace('#', '', $developer_data['foreground_colour']),
		);
		$this->db->where('developers.developer_id', $developer_id);
		return $this->db->update('developers', $data);
	}

	public function deleteDeveloper($developer_id)
	{
		$this->db->where('developers.developer_id', $developer_id);
		return $this->db->delete('developers');
	}

}
?>