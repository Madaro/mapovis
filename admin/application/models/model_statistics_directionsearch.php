<?php
Class Model_statistics_directionsearch extends CI_Model
{
	function getViews($development_id, $start_date, $end_date)
	{
		$this->db->select('COUNT(statistics_directionSearches.ID) as views, statistics_directionSearches.developmentID, TRIM(statistics_directionSearches.direction_address) AS direction_address ');
		$this->db->from('statistics_directionSearches');
		$this->db->where('statistics_directionSearches.developmentID', $development_id);
		$this->db->where('statistics_directionSearches.timestamp /1000 >=', strtotime($start_date));
		$this->db->where('statistics_directionSearches.timestamp /1000 <=', strtotime($end_date));
		$this->db->group_by(array('statistics_directionSearches.developmentID', 'TRIM(statistics_directionSearches.direction_address)'));
		$query  = $this->db->get();
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function getViewsByDay($development_id, $start_date, $end_date)
	{
		$this->db->select("COUNT(statistics_directionSearches.ID) as views, from_unixtime(statistics_directionSearches.timestamp/1000, '%Y-%m-%d' ) as views_date, from_unixtime(statistics_directionSearches.timestamp/1000, '%d/%m' ) as formated_date ", FALSE);
		$this->db->from('statistics_directionSearches');
		$this->db->where('statistics_directionSearches.developmentID', $development_id);
		$this->db->where('statistics_directionSearches.timestamp /1000 >=', strtotime($start_date));
		$this->db->where('statistics_directionSearches.timestamp /1000 <=', strtotime($end_date));
		/* change pretect identifiers to codeigniter will not escape %Y-%m-%d*/
		$this->db->_protect_identifiers = FALSE;
		$this->db->group_by('from_unixtime(statistics_directionSearches.timestamp /1000, \'%Y-%m-%d\' ) ', FALSE);
		$this->db->_protect_identifiers = TRUE;
		$this->db->order_by('views_date', 'asc');
		$query      = $this->db->get();
		$tmp_result = ($query->num_rows() > 0)? $query->result(): array();
		$result     = array();
		foreach($tmp_result as $row){
			$result[$row->views_date] = $row;
		}
		return $result;
	}

	function deleteDevelopmentViews($development_id)
	{
		$this->db->where('statistics_directionSearches.developmentID', $development_id);
		return $this->db->delete('statistics_directionSearches');
	}

	function getFields()
	{
		return array(
			'developmentID'     => array('Development', 't'),
			'direction_address' => array('Lot', 't'),
			'timestamp'         => array('Timestamp', 't'),
			'ip_address'        => array('IP Addres', 't'),
			'user_agent'        => array('User Agent', 't'),
		);
	}
}
?>