<?php
Class Model_development_document extends CI_Model
{
	public function getDevelopmentDocuments($development_id)
	{
		$this->db->where('development_documents.development_id', $development_id);
		$this->db->order_by('upload_date', 'desc');
		$this->db->order_by('file_name', 'asc');
		$query     = $this->db->get('development_documents');
		$result    = ($query->num_rows() > 0)? $query->result(): array();
		$documents = array();
		foreach($result as $document){
			$documents[$document->document_type][] = $document;
		}
		return $documents;
	}

	public function getDevelopmentDocument($development_document_id)
	{
		$this->db->where('development_documents.development_document_id', $development_document_id);
		$query  = $this->db->get('development_documents');
		$result = ($query->num_rows() > 0)? $query->row(): FALSE;
		return $result;
	}

	public function addDevelopmentDocument($development_id, $file_data)
	{
		$this->load->model('Model_user', '', TRUE);
		$new_file_path = BASEPATH."../../mpvs/documents/";
		if (!file_exists($new_file_path)) {
			mkdir($new_file_path, 0777, true);
		}
		if(!isset($_FILES['document_file']['name'])){
			return FALSE;
		}
		$file_name = $this->uniqueFileName($new_file_path, str_replace(' ', '_', $_FILES['document_file']['name']));
		if (!move_uploaded_file($_FILES['document_file']['tmp_name'], $new_file_path.$file_name)) {
			return FALSE;
		}

		$user  = $this->Model_user->getUserLogged();
		$data  = array(
			'file_name'      => $file_name,
			'development_id' => $development_id,
			'document_type'  => $file_data['document_type'],
			'description'    => $file_data['description'],
			'upload_date'    => date('Y-m-d'),
			'user_id'        => $user->user_id,
		);
		$this->db->insert('development_documents', $data);
		return $this->db->insert_id();
	}

	public function deleteDevelopmentDocuments($development_document_id)
	{
		$document    = $this->getDevelopmentDocument($development_document_id);
		// delete document DB record
		$this->db->where('development_document_id', $development_document_id);
		$deleted = $this->db->delete('development_documents');
		// if the document was deleted successfully delete the file
		if($deleted && !empty($document->file_name)){
			$image_path = BASEPATH."../../mpvs/documents/{$document->file_name}";
			unlink($image_path);
		}
		return $deleted;
	}

	public function getTypes(){
		return array('plans_of_subdivision','engineering_plans','builder_guidelnes','miscellaneous');
	}

	private function uniqueFileName($file_path, $file_name)
	{
		$counter       = 1;
		$new_file_name = $file_name;
		while (file_exists($file_path.$new_file_name)) {
			list($name, $extension) = explode('.', $file_name);
			$new_file_name = "{$name}({$counter}).{$extension}";
			$counter++;
		}
		return $new_file_name;
	}
}
?>