<?php
Class Model_config extends CI_Model
{
	public function getConfig()
	{
		$query = $this->db->get('global_config');
		return $query->row();
	}

	public function updateConfig($form_data)
	{
		$data = array(
			'number_logs'    => $form_data['number_logs'],
			'support_email'  => $form_data['support_email'],
			'info_email'     => $form_data['info_email'],
			'contact_number' => $form_data['contact_number'],
		);
		return $this->db->update('global_config', $data); 
	}

	public function updateWelcomeMessage($form_data)
	{
		$data = array('welcome_message' => $form_data['welcome_message']);
		return $this->db->update('global_config', $data); 
	}

	public function updateWelcomeMessageBuilder($form_data)
	{
		$data = array('welcome_message_builder' => $form_data['welcome_message_builder']);
		return $this->db->update('global_config', $data); 
	}

	public function getWelcomeMessage($user)
	{
		$config          = $this->getConfig();
		$welcome_message = str_replace('{:username:}', $user->name, $config->welcome_message);
		return $welcome_message;
	}

	public function getWelcomeMessageBuilder($user)
	{
		$config          = $this->getConfig();
		$welcome_message = str_replace('{:username:}', $user->name, $config->welcome_message_builder);
		return $welcome_message;
	}
}
?>