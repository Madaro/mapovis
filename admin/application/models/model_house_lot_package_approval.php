<?php
Class Model_house_lot_package_approval extends CI_Model
{
	function getBuilderPackages($builder_id, $filters = array())
	{
		$this->db->select('house_lot_package_approvals.*, houses.house_name, lots.lot_number, developments.development_name, stages.stage_number, precincts.precinct_number, builder_sales_people.name AS sales_person, users.name AS builder_user');
		$this->db->join('houses', 'houses.house_id = house_lot_package_approvals.house_id');
		$this->db->join('lots', 'lots.lot_id = house_lot_package_approvals.lot_id');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('developments', 'developments.development_id = precincts.development_id');
		$this->db->join('builder_sales_people', 'builder_sales_people.builder_sales_person_id = house_lot_package_approvals.builder_sales_person_id', 'left');
		$this->db->join('users', 'users.user_id = house_lot_package_approvals.builder_user_id');
		$this->db->where('house_lot_package_approvals.builder_id', $builder_id);
		$this->db->order_by('houses.house_name', 'asc');
		if(isset($filters['lot_id']) && $filters['lot_id']){
			$this->db->where('house_lot_package_approvals.lot_id', $filters['lot_id']);
		}
		if(isset($filters['status']) && $filters['status']){
			$this->db->where('house_lot_package_approvals.status', $filters['status']);
		}
		if(isset($filters['development_id']) && $filters['development_id']){
			$this->db->where('precincts.development_id', $filters['development_id']);
		}
		$query   = $this->db->get('house_lot_package_approvals');
		$results = ($query)? $query->result(): array();
		return $results;
		$sorted_results = array();
		foreach($results as $result){
			$sorted_results[$result->house_id] = $result;
		}
		return $sorted_results;
	}

	function getDevelopmentPackages($development_id, $filters = array())
	{
		$this->db->select('house_lot_package_approvals.*, houses.house_name, lots.lot_number, builders.builder_name, stages.stage_number, precincts.precinct_number, builder_sales_people.name AS sales_person, users.name AS builder_user');
		$this->db->select('lots.lot_width, lots.lot_square_meters AS lot_size, houses.house_size');
		$this->db->join('houses', 'houses.house_id = house_lot_package_approvals.house_id');
		$this->db->join('lots', 'lots.lot_id = house_lot_package_approvals.lot_id');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('builder_sales_people', 'builder_sales_people.builder_sales_person_id = house_lot_package_approvals.builder_sales_person_id', 'left');
		$this->db->join('users', 'users.user_id = house_lot_package_approvals.builder_user_id');
		$this->db->join('builders', 'builders.builder_id = house_lot_package_approvals.builder_id');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->order_by('houses.house_name', 'asc');
		if(isset($filters['status']) && $filters['status']){
			$this->db->where('house_lot_package_approvals.status', $filters['status']);
		}
		if(isset($filters['development_id']) && $filters['development_id']){
			$this->db->where('precincts.development_id', $filters['development_id']);
		}
		$query   = $this->db->get('house_lot_package_approvals');
		$results = ($query)? $query->result(): array();
		return $results;
		$sorted_results = array();
		foreach($results as $result){
			$sorted_results[$result->house_id] = $result;
		}
		return $sorted_results;
	}

	function getDevelopmentsTotalPackages($development_ids)
	{
		$this->db->select('COUNT(house_lot_package_approval_id) as total_packages, precincts.development_id');
		$this->db->join('lots', 'lots.lot_id = house_lot_package_approvals.lot_id');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where_in('precincts.development_id', $development_ids);
		$this->db->where('house_lot_package_approvals.status', 'Pending');
		$this->db->group_by('precincts.development_id', 'asc');
		$query   = $this->db->get('house_lot_package_approvals');
		$results = ($query)? $query->result(): array();
		return (count($results))?array_combine(array_map(function($v){return $v->development_id;}, $results), array_map(function($v){return $v->total_packages;}, $results)): array();
	}

	function getHouseLotPackage($lot_id, $house_id)
	{
		$this->db->select('house_lot_package_approvals.*, houses.*, builders.builder_name');
		$this->db->join('houses', 'houses.house_id = house_lot_package_approvals.house_id');
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id', 'left');
		$this->db->where('house_lot_package_approvals.lot_id', $lot_id);
		$this->db->where('house_lot_package_approvals.house_id', $house_id);
		$this->db->where('house_lot_package_approvals.status', 'Pending');
		$query  = $this->db->get('house_lot_package_approvals');
		$result = $query->row();
		if($result){
			$result->formated_house_lot_price = number_format($result->house_lot_price);
			$result->formatted_price          = ($result->house_lot_price)? '$'. $result->formated_house_lot_price: 'n/a';
		}
		return $result;
	}

	function getHouseLotPackageById($package_id)
	{
		$this->db->select('house_lot_package_approvals.*, lots.lot_number, houses.house_name, builders.builder_name, developments.development_name, developments.development_id');
		$this->db->join('houses', 'houses.house_id = house_lot_package_approvals.house_id');
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id', 'left');
		$this->db->join('lots', 'lots.lot_id = house_lot_package_approvals.lot_id');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('developments', 'developments.development_id = precincts.development_id');
		$this->db->where('house_lot_package_approvals.house_lot_package_approval_id', $package_id);
		$query  = $this->db->get('house_lot_package_approvals');
		return $query->row();
	}

	function submitRequestHouseLot($lot_id, $house_id, $form_data)
	{
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_house_image', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_user', '', TRUE);
		$this->load->model('Model_builder_user', '', TRUE);
		$this->load->library('email_sender');
		$this->load->model('Model_hnl_change_log', '', TRUE);

		// create record
		$current_user = $this->Model_user->getBuilderLogged();
		$lot          = $this->Model_lot->getLot($lot_id); 
		$house        = $this->Model_house->getHouse($house_id);
		$development  = $this->Model_development->getDevelopment($lot->development_id);
		$builder      = $this->Model_builder_user->getLoggedUserBuilder();

		$ordered_images = array();
		foreach($form_data['facade_images'] as $image_id_url){
			// if there are new images save them
			$house_image_id = $this->Model_house_image->updateGlobalHouseImage($house_id, $image_id_url, $form_data);
			$ordered_images[] = ($house_image_id)? $house_image_id: $image_id_url;
		}

		$request_data = array(
			'lot_id'                  => $lot_id,
			'house_id'                => $house_id,
			'builder_id'              => $builder->builder_id,
			'builder_user_id'         => $current_user->user_id,
			'house_lot_price'         => (float)$form_data['house_lot_price'],
			'builder_sales_person_id' => ($form_data['builder_sales_person_id'])? $form_data['builder_sales_person_id']: null,
			'images_order'            => (count($ordered_images))? implode(',', $ordered_images): '',
			'description'             => $form_data['description'],
			'status'                  => 'Pending',
			'request_date'            => date('Y-m-d H:i:s'),
		);
		$file_url = '';
		if(!empty($_FILES['house_lot_pdf']) && $_FILES['house_lot_pdf']['type'] == 'application/pdf'){

			$file_path  = BASEPATH."../../mpvs/templates/lot_house_pdfs_approval/";
			if (!file_exists("{$file_path}")) {
				mkdir("{$file_path}/", 0777, true);
			}
			$time = time();
			$file_name      = str_replace(' ', '-', "request_{$development->development_name}-{$house->house_name}-by-{$house->builder_name}-Lot_{$lot->lot_number}_{$time}.pdf");
			$file_path_name = $file_path.$file_name;
			// Save PDF file
			$request_data['file_name']          = $file_name;
			move_uploaded_file($_FILES['house_lot_pdf']['tmp_name'], $file_path_name);
			$file_url = base_url('../mpvs/templates/lot_house_pdfs_approval/'.$file_name);
		}

		if($this->db->insert('house_lot_package_approvals', $request_data)){
			$request_id       = $this->db->insert_id();

			// update previous request
			$this->db->where(array('house_id' => $house_id, 'lot_id' => $lot_id, 'status' => 'Pending', 'house_lot_package_approval_id !=' => $request_id));
			$this->db->update('house_lot_package_approvals', array('status' => 'Replaced', 'approval_rejection_date' => date('Y-m-d H:i:s')));

			$log_ids           = (object)array(
				'development_id' => $development->development_id,
				'lot_id'         => $lot_id,
				'house_id'       => $house_id,
			);
			$all_log_changes  = "{$current_user->name} ({$builder->builder_name}) submitted for approval: {$house->house_name} on lot {$lot->lot_number} ({$development->development_name}).";
			$this->Model_hnl_change_log->addHnLChangeLog('Request', $all_log_changes, $log_ids, $current_user);

			$development_user = $this->Model_user->getUser($development->primary_user);
			$from             = (object)array('email' => $current_user->email, 'name' => $current_user->name);
			$to               = (object)array('name' => $development_user->name, 'email' => $development_user->email);
			$subject          = 'House and Land Package is waiting for approval';
			$email_data       = (object)array(
				'developmentname'  => $development->development_name,
				'developmentid'    => $development->development_id,
				'lotnumber'        => $lot->lot_number,
				'primaryuser'      => $development_user->name,
				'builder'          => $builder->builder_name,
				'housename'        => $house->house_name,
				'builderusername'  => $current_user->name,
				'packagerequestid' => $request_id,
				'title'            => $subject,
				'packageprice'     => number_format($form_data['house_lot_price'],0),
				'pdfurl'           => $file_url,
			);
			// submit email to primary user of the development
			$this->email_sender->sendEmailNotificaton($subject, 'house_land_package_request', $from, $to, $email_data, null, $notes);
			return $request_id;
		}
		return FALSE;
	}

	function updateRequestHouseLot($lot_id, $house_id, $form_data)
	{
		$house_lot_package_approval = $this->getHouseLotPackage($lot_id, $house_id);
		if(empty($house_lot_package_approval)){
			return FALSE;
		}
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_development', '', TRUE);

		// create record
		$lot          = $this->Model_lot->getLot($lot_id); 
		$house        = $this->Model_house->getHouse($house_id);
		$development  = $this->Model_development->getDevelopment($lot->development_id);

		$request_data = array(
			'house_lot_price'         => (float)$form_data['house_lot_price'],
			'builder_sales_person_id' => ($form_data['builder_sales_person_id'])? $form_data['builder_sales_person_id']: null,
			'images_order'            => (count($form_data['facade_images']))? implode(',', $form_data['facade_images']): '',
			'description'             => $form_data['description'],
		);
		$file_url = '';
		if(!empty($_FILES['house_lot_pdf']) && $_FILES['house_lot_pdf']['type'] == 'application/pdf'){

			$file_path  = BASEPATH."../../mpvs/templates/lot_house_pdfs_approval/";
			if (!file_exists("{$file_path}")) {
				mkdir("{$file_path}/", 0777, true);
			}
			$time = time();
			$file_name      = str_replace(' ', '-', "request_{$development->development_name}-{$house->house_name}-by-{$house->builder_name}-Lot_{$lot->lot_number}_{$time}.pdf");
			$file_path_name = $file_path.$file_name;
			// Save PDF file
			$request_data['file_name']          = $file_name;
			move_uploaded_file($_FILES['house_lot_pdf']['tmp_name'], $file_path_name);
			$file_url = base_url('../mpvs/templates/lot_house_pdfs_approval/'.$file_name);
			// remove previous PDF file
			if(!empty($house_lot_package_approval->file_name) && file_exists($file_path.$house_lot_package_approval->file_name)){
				unlink($file_path.$house_lot_package_approval->file_name);
			}
		}

		$this->db->where('house_lot_package_approval_id', $house_lot_package_approval->house_lot_package_approval_id);
		return $this->db->update('house_lot_package_approvals', $request_data);
	}

	function approvePackage($package_id, $current_user)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);
		$pending_package   = $this->getHouseLotPackageById($package_id);
		$house_lot_package = $this->Model_house_lot_package->getHouseLotPackage($pending_package->lot_id, $pending_package->house_id);
		$update_data       = array('active' => '1');

		if($pending_package->house_lot_price){
			$update_data['house_lot_price'] = $pending_package->house_lot_price;
		}
		if($pending_package->builder_sales_person_id){
			$update_data['builder_sales_person_id'] = $pending_package->builder_sales_person_id;
		}
		if($pending_package->description){
			$update_data['description'] = $pending_package->description;
		}
		if($pending_package->file_name){

			$file_path_approval  = BASEPATH."../../mpvs/templates/lot_house_pdfs_approval/";
			$file_path_package   = BASEPATH."../../mpvs/templates/lot_house_pdfs/";

			if(file_exists($file_path_approval.$pending_package->file_name)){
				$new_file_name       = str_replace(' ', '-', "{$pending_package->development_name}-{$pending_package->house_name}-by-{$pending_package->builder_name}-Lot_{$pending_package->lot_number}.pdf");
				$file_path_name      = $file_path_package.$new_file_name;
				// Save PDF file
				$update_data['file_name'] = $new_file_name;
				if(file_exists($file_path_name)){
					unlink($file_path_name);
				}
				$update_data['file_name']          = $new_file_name;
				copy($file_path_approval.$pending_package->file_name, $file_path_name);
			}
		}
		if($house_lot_package && $house_lot_package->house_lot_package_id){
			$this->db->where('house_lot_packages.house_lot_package_id', $house_lot_package->house_lot_package_id);
			$this->db->update('house_lot_packages', $update_data);
		}
		else{
			$update_data['lot_id']   = $pending_package->lot_id;
			$update_data['house_id'] = $pending_package->house_id;
			$this->db->insert('house_lot_packages', $update_data);
			$this->db->insert_id();
		}
		$facade_images = explode(',', $pending_package->images_order);
		if(!empty($facade_images)){
			$this->load->model('Model_house_lot_order_image', '', TRUE);
			$this->Model_house_lot_order_image->updatePackageImagesOrder($pending_package->house_id, $pending_package->lot_id, array('facade_images' => $facade_images));
		}

		$this->db->where(array('status' => 'Pending', 'house_lot_package_approval_id' => $package_id));
		if($this->db->update('house_lot_package_approvals', array('status' => 'Approved', 'approval_rejection_date' => date('Y-m-d H:i:s')))){
			$this->load->model('Model_hnl_change_log', '', TRUE);
			$all_log_changes  = "{$current_user->name} approved: {$pending_package->house_name} on lot {$pending_package->lot_number} ({$pending_package->development_name}).";
			$this->Model_hnl_change_log->addHnLChangeLog('Approve', $all_log_changes, $pending_package, $current_user);

			$this->load->library('email_sender');

			$request_user     = $this->Model_user->getUser($pending_package->builder_user_id);
			$from             = (object)array('email' => $current_user->email, 'name' => $current_user->name);
			$to               = (object)array('name' => $request_user->name, 'email' => $request_user->email);
			$subject          = 'House and Land Package has been approved';
			$file_url         = ($pending_package->file_name)? base_url('../mpvs/templates/lot_house_pdfs_approval/'.$pending_package->file_name): '';
			$email_data       = (object)array(
				'developmentname'  => $pending_package->development_name,
				'developmentid'    => $pending_package->development_id,
				'lotnumber'        => $pending_package->lot_number,
				'username'         => $current_user->name,
				'builder'          => $pending_package->builder_name,
				'housename'        => $pending_package->house_name,
				'builderusername'  => $request_user->name,
				'title'            => $subject,
				'packageprice'     => number_format($pending_package->house_lot_price,0),
				'pdfurl'           => $file_url,
			);
			// submit email to primary user of the development
			$this->email_sender->sendEmailNotificaton($subject, 'house_land_package_approved', $from, $to, $email_data, null, $notes);
			return TRUE;
		}
		return FALSE;
	}

	function rejectPackage($package_id, $current_user)
	{
		$pending_package   = $this->getHouseLotPackageById($package_id);
		$this->db->where(array('status' => 'Pending', 'house_lot_package_approval_id' => $package_id));
		if($this->db->update('house_lot_package_approvals', array('status' => 'Rejected', 'approval_rejection_date' => date('Y-m-d H:i:s')))){
			$this->load->model('Model_hnl_change_log', '', TRUE);
			$all_log_changes  = "{$current_user->name} rejected: {$pending_package->house_name} on lot {$pending_package->lot_number} ({$pending_package->development_name}).";
			$this->Model_hnl_change_log->addHnLChangeLog('Reject', $all_log_changes, $pending_package, $current_user);

			$this->load->library('email_sender');
			$rejection_message = nl2br($this->input->post('rejection_message'));

			$request_user     = $this->Model_user->getUser($pending_package->builder_user_id);
			$from             = (object)array('email' => $current_user->email, 'name' => $current_user->name);
			$to               = (object)array('name' => $request_user->name, 'email' => $request_user->email);
			$subject          = 'House and Land Package has been rejected';
			$file_url         = ($pending_package->file_name)? base_url('../mpvs/templates/lot_house_pdfs_approval/'.$pending_package->file_name): '';
			$email_data       = (object)array(
				'developmentname'  => $pending_package->development_name,
				'developmentid'    => $pending_package->development_id,
				'lotnumber'        => $pending_package->lot_number,
				'username'         => $current_user->name,
				'builder'          => $pending_package->builder_name,
				'housename'        => $pending_package->house_name,
				'builderusername'  => $request_user->name,
				'title'            => $subject,
				'packageprice'     => number_format($pending_package->house_lot_price,0),
				'pdfurl'           => $file_url,
				'rejectionmessage' => (!empty($rejection_message))? 'The reason for the rejection was: <br><br><strong> '.$rejection_message.' </strong><br><br>': '',
			);
			// submit email to primary user of the development
			$this->email_sender->sendEmailNotificaton($subject, 'house_land_package_rejected', $from, $to, $email_data, null, $notes);
			return TRUE;
		}
		return FALSE;
	}

	function deletePackage($package_id)
	{
		$this->db->where(array('status' => 'Pending', 'house_lot_package_approval_id' => $package_id));
		return $this->db->update('house_lot_package_approvals', array('status' => 'Deleted', 'approval_rejection_date' => date('Y-m-d H:i:s')));
	}
}
?>