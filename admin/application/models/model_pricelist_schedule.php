<?php
Class Model_pricelist_schedule extends CI_Model
{
	function getPricelistSchedule($development_id)
	{
		$this->db->where('pricelist_schedules.development_id', $development_id);
		$query  = $this->db->get('pricelist_schedules');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->row(); 
		}
		else{
			$fields                   = $this->getFields();
			$fields['development_id'] = $development_id;
			$fields['last_sent']      = NULL;
			$result                   = (object)$fields;
		}
		$result->days_of_week_array = explode(',', $result->days_of_week);
		return $result;
	}

	function updatePricelistSchedule($development_id, $form_data)
	{
		$fields           = $this->getFields();
		$schedule_details = array('days_of_week' => '');
		foreach(array_keys($fields) as $field_id){
			if(isset($form_data[$field_id])){
				if($field_id == 'days_of_week'){
					$schedule_details[$field_id] = implode(',', $form_data[$field_id]);
				}
				else{
					$schedule_details[$field_id] = $form_data[$field_id];
				}
			}
		}
		// if pricelist_schedule already exists update the records if not create new record
		$pricelist_schedule = $this->getPricelistSchedule($development_id);
		if(isset($pricelist_schedule->pricelist_schedule_id)){
			$this->db->where('pricelist_schedules.pricelist_schedule_id', $pricelist_schedule->pricelist_schedule_id);
			return $this->db->update('pricelist_schedules', $schedule_details);
		}
		else{
			$schedule_details['development_id'] = $development_id;
			return $this->db->insert('pricelist_schedules', $schedule_details);
		}
	}

	private function getFields()
	{
		return array(
			'development_id'    => null,
			'days_of_week'      => '',
			'notification_time' => '8',
			'conditions'        => 'onchange',
		);
	}


/***************************************** EMAIL NOTIFICATIONS *************************************************/

	/**
	 * Organizes the data to know who should be notify and what method should be used
	 * @param int $level
	 * @param object $development
	 * @param DateFormat $today
	 */
	public function sendPricelistToSubscriber($development, $subscriber)
	{
		$this->load->library('mapovis_lib');
		$this->load->model('Model_apipdf', '', TRUE);

		$today       = $this->mapovis_lib->getToday($development->state);
		$pdf_file    = $this->Model_apipdf->devpdfpricelist($development->development_id, TRUE);
		if($pdf_file){
			$subject                      = "{$development->development_name} - Price List";
			$development->developmentname = $development->development_name;
			$development->title           = $subject;
			$development->username        = (empty($subscriber->subscriber_name))? 'Subscriber': $subscriber->subscriber_name;
			$today_datetime               = date_format($today, 'Y-m-d H:i:s');

			$this->sendEmailNotification($subscriber, $development, $subject, $today_datetime, $pdf_file);
		}
	}

	/**
	 * Runs the script for all current developments to send notifications if necessary
	 */
	public function sendPriceListsAllDevelopments()
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_user', '', TRUE);
		$this->load->library('mapovis_lib');
		$this->load->library('email_sender');
		$this->load->library('sms_sender');

		$developments = $this->Model_development->getDevelopments();
		foreach($developments as $development){
			$this->prepareDataAndSendPricelists($development);
		}
	}

	/**
	 * this function gets the next schedule_date details (date, overdue/outcoming) and uses the function needSendPricelistSchedule 
	 * to know if a notification should be sent if that is the case it calls sendPricelistSchedules which is in charge of sending 
	 * pricelist_schedules for all levels and Email and SMS
	 * @param object $tmp_development
	 */
	private function prepareDataAndSendPricelists($development)
	{
		$this->load->model('Model_change_log', '', TRUE);
		$today      = $this->mapovis_lib->getToday($development->state);
		$today_day  = date_format($today, 'N');
		$today_hour = date_format($today, 'G');
		$today_time = strtotime(date_format($today, 'Y-m-d H:i:s'))-60*60;
		$schedule   = $this->getPricelistSchedule($development->development_id);
		if($schedule->conditions == 'onchange' && $schedule->last_sent !== NULL){
			//validate if there were changes
			$log_filters = array(
				'development_id' => $development->development_id,
				'notes'          => array('Changed from', 'Price from', 'New Lot'),
				'from_datetime'  => ($schedule->last_sent)? date('Y-m-d H:i:s', $schedule->last_sent): FALSE
			);
			$change_logs = $this->Model_change_log->getChangeLogs($log_filters, FALSE);
			// if there are not lots change logs in the system then do not send the price list
			if(empty($change_logs)){
				return FALSE;
			}
		}
		if(($schedule->last_sent == NULL || $today_time > $schedule->last_sent) && in_array($today_day, $schedule->days_of_week_array) && $today_hour == $schedule->notification_time){
			// send email notification
			$this->sendPricelistSchedules($development, $today);
		}
	}

	/**
	 * Organizes the data to know who should be notify and what method should be used
	 * @param int $level
	 * @param object $development
	 * @param DateFormat $today
	 */
	private function sendPricelistSchedules($development, $today)
	{
		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$this->load->model('Model_apipdf', '', TRUE);
		$subscribers = $this->Model_pricelist_subscriber->getPricelistSubscribers($development->development_id);
		$pdf_file    = $this->Model_apipdf->devpdfpricelist($development->development_id, TRUE);
		if(count($subscribers) && $pdf_file){
			$subject                      = "{$development->development_name} - Price List";
			$development->developmentname = $development->development_name;
			$development->title           = $subject;
			foreach($subscribers as $subscriber){
				$development->username = (empty($subscriber->subscriber_name))? 'Subscriber': $subscriber->subscriber_name;
				$today_datetime = date_format($today, 'Y-m-d H:i:s');

				$this->sendEmailNotification($subscriber, $development, $subject, $today_datetime, $pdf_file);
			}
			if(isset($today_datetime)){
				$this->updateLastSent($development->development_id, $today_datetime);
			}
		}
	}

	/**
	 * Sends Email notificatios and records a pricelist_schedule log for future reference
	 * @param int $level
	 * @param object $user
	 * @param object $development
	 * @param string $subject
	 * @param string $template_name
	 * @param string $tags
	 * @param dateTime $today
	 */
	private function sendEmailNotification($subscriber, $development, $subject, $today, $pdf_file_path)
	{
		$this->load->model('Model_config', '', TRUE);
		$this->load->library('email_sender');
		$settings = $this->Model_config->getConfig();

		$today = date("d.m.y");

		$from               = (object)array('email' => $settings->support_email, 'name' => 'MAPOVIS');
		$to                 = (object)array('name' => $subscriber->subscriber_name, 'email' => $subscriber->subscriber_email);
		$notes              = '';
		$pdf_attachment     = file_get_contents($pdf_file_path);
		$attachment_encoded = base64_encode($pdf_attachment);
		$file_name          = $today.' - '.$development->development_name.' - Price List.pdf';
		$attachments        = array(
			array(
				'content' => $attachment_encoded,
				'type'    => "application/pdf",
				'name'    => $file_name,
			)
		);
		$email_template     = ($development->price_list_email_template)? $development->price_list_email_template: 'price_list_notification';

		return $this->email_sender->sendEmailNotificaton($subject, $email_template, $from, $to, $development, null, $notes, $attachments, $development->price_list_reply_to_email);
	}

	private function updateLastSent($development_id, $today_time)
	{
		$schedule_details = array('last_sent' => strtotime($today_time));
		// if pricelist_schedule already exists update the records if not create new record
		$pricelist_schedule = $this->getPricelistSchedule($development_id);
		if(isset($pricelist_schedule->pricelist_schedule_id)){
			$this->db->where('pricelist_schedules.pricelist_schedule_id', $pricelist_schedule->pricelist_schedule_id);
			return $this->db->update('pricelist_schedules', $schedule_details);
		}
		else{
			$schedule_details['development_id'] = $development_id;
			return $this->db->insert('pricelist_schedules', $schedule_details);
		}
	}

}
?>