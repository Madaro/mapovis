<?php
Class Model_statistics_placesearch extends CI_Model
{
	function getViews($development_id, $start_date, $end_date)
	{
		$this->db->select('COUNT(statistics_placesSearches.ID) as views, statistics_placesSearches.developmentID, TRIM(statistics_placesSearches.search_phrase) AS search_phrase ');
		$this->db->from('statistics_placesSearches');
		$this->db->where('statistics_placesSearches.developmentID', $development_id);
		$this->db->where('statistics_placesSearches.timestamp /1000 >=', strtotime($start_date));
		$this->db->where('statistics_placesSearches.timestamp /1000 <=', strtotime($end_date));
		$this->db->group_by(array('statistics_placesSearches.developmentID', 'TRIM(statistics_placesSearches.search_phrase)'));
		$query  = $this->db->get();
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function getViewsByDay($development_id, $start_date, $end_date)
	{
		$this->db->select("COUNT(statistics_placesSearches.ID) as views, from_unixtime(statistics_placesSearches.timestamp/1000, '%Y-%m-%d' ) as views_date, from_unixtime(statistics_placesSearches.timestamp/1000, '%d/%m' ) as formated_date ", FALSE);
		$this->db->from('statistics_placesSearches');
		$this->db->where('statistics_placesSearches.developmentID', $development_id);
		$this->db->where('statistics_placesSearches.timestamp /1000 >=', strtotime($start_date));
		$this->db->where('statistics_placesSearches.timestamp /1000 <=', strtotime($end_date));
		/* change pretect identifiers to codeigniter will not escape %Y-%m-%d*/
		$this->db->_protect_identifiers = FALSE;
		$this->db->group_by('from_unixtime(statistics_placesSearches.timestamp /1000, \'%Y-%m-%d\' ) ', FALSE);
		$this->db->_protect_identifiers = TRUE;
		$this->db->order_by('views_date', 'asc');
		$query  = $this->db->get();
		$tmp_result = ($query->num_rows() > 0)? $query->result(): array();
		$result     = array();
		foreach($tmp_result as $row){
			$result[$row->views_date] = $row;
		}
		return $result;
	}

	function deleteDevelopmentViews($development_id)
	{
		$this->db->where('statistics_placesSearches.developmentID', $development_id);
		return $this->db->delete('statistics_placesSearches');
	}

	function getFields()
	{
		return array(
			'developmentID' => array('Development', 't'),
			'search_phrase' => array('Lot', 't'),
			'timestamp'     => array('Timestamp', 't'),
			'ip_address'    => array('IP Addres', 't'),
			'user_agent'    => array('User Agent', 't'),
		);
	}
}
?>