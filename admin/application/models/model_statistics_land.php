<?php
Class Model_statistics_land extends CI_Model
{
	function addLog($development_id, $filters)
	{
		$width_from = (isset($filters['lot_width_from']) && $filters['lot_width_from'])? $filters['lot_width_from']: '';
		$width_to   = (isset($filters['lot_width_to']) && $filters['lot_width_to'])? '|'. $filters['lot_width_to']: '';
		$size_from  = (isset($filters['lot_square_meters_from']))? $filters['lot_square_meters_from']: '';
		$size_to    = (isset($filters['lot_square_meters_to']))? '|'.$filters['lot_square_meters_to']: '';

		$width = ((isset($filters['lot_width']) && !empty($filters['lot_width'])))? implode(',', $filters['lot_width']): $width_from.$width_to;
		$size  = ((isset($filters['lot_square_meters']) && !empty($filters['lot_square_meters'])))?  implode(',', $filters['lot_square_meters']): $size_from.$size_to;
		// Create record
		$log = array(
			'development_id'   => $development_id,
			'lot_width'         => $width,
			'lot_square_meters' => $size,
			'datetime'          => date('Y-m-d H:i:s')
		);
		$this->db->insert('statistics_land', $log);
	}
    
    function getViewsByDay($development_id, $start_date, $end_date)
    {
        $this->db->select("COUNT(statistics_land.statistics_land_id) as views, DATE_FORMAT(statistics_land.datetime, '%Y-%m-%d') as views_date, DATE_FORMAT(statistics_land.datetime, '%d/%m') as formated_date ", FALSE);
        $this->db->from('statistics_land');
        $this->db->where('statistics_land.development_id', $development_id);
        $this->db->where('statistics_land.datetime  >=', $start_date);
        $this->db->where('statistics_land.datetime  <=', $end_date);
        /* change pretect identifiers to codeigniter will not escape %Y-%m-%d*/
        $this->db->_protect_identifiers = FALSE;
        $this->db->group_by("DATE_FORMAT(statistics_land.datetime, '%Y-%m-%d')", FALSE);
        $this->db->_protect_identifiers = TRUE;
        $this->db->order_by('views_date', 'asc');
        $query      = $this->db->get();
        $tmp_result = ($query->num_rows() > 0)? $query->result(): array();
        $result     = array();
        foreach($tmp_result as $row){
            $result[$row->views_date] = $row;
        }
        return $result;
    }
    
    
}
?>