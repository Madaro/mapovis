<?php
Class Model_house extends CI_Model
{
	/**
	 * Returns houses for given development
	 * @param int $development_id
	 * @return array
	 */
	function getHouses($development_id, $include_global = FALSE)
	{
		$this->db->select('houses.*, builders.*');
		$this->db->group_by('houses.house_id');
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id', 'left');
		$this->db->where('houses.development_id', $development_id);
		if($include_global){
			$this->db->or_where('houses.development_id is null');
		}
		$this->db->order_by('houses.house_name', 'asc');
		$query  = $this->db->get('houses');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	/**
	 * Returns houses for given development
	 * @param int $builder_id
	 * @param int $exclude_house_id
	 * @return array
	 */
	function getHousesByBuilder($builder_id, $exclude_house_id = NULL, $with_facades = FALSE)
	{
		$this->db->select('houses.*, builders.*', FALSE);
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id');
		$this->db->where('houses.house_builder_id', $builder_id);
		$this->db->where('houses.development_id is null');
		if($exclude_house_id){
			$this->db->where('houses.house_id !=', $exclude_house_id);
		}
		if($with_facades){
			$this->db->join('house_images', 'house_images.house_id = houses.house_id AND house_images.file_type = "facade"');
			$this->db->having('COUNT(house_images.house_image_id) > 0');
		}
		$this->db->group_by('houses.house_id');
		$this->db->order_by('houses.house_name', 'asc');
		$query  = $this->db->get('houses');
		return ($query)? $query->result(): array();
	}

	public function getBuilderHouses($builder_user_id)
	{
		$this->db->select('houses.*, builders.*');
		$this->db->group_by('houses.house_id');
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id');
		$this->db->join('builder_users', 'builder_users.builder_id = builders.builder_id');
		$this->db->where('houses.development_id', null);
		$this->db->where('builder_users.user_id', $builder_user_id);
		$this->db->order_by('houses.house_name', 'asc');
		$query  = $this->db->get('houses');
		return ($query)? $query->result(): array();
	}

	/**
	 * Returns a house based on given id
	 * @param int $house_id
	 * @return object
	 */
	function getHouse($house_id)
	{
		$this->db->select('houses.*, builders.*');
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id', 'left');
		$query  = $this->db->get_where('houses', array('houses.house_id' => $house_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row();
		}
		return $result;
	}

	/**
	 * Returns TRUE if the house name is not duplciate and false if already exist a house with that name
	 * @param string $house_name
	 * @param int $builder_id
	 * @param int $development_id
	 * @param int $house_id
	 * @return boolean/object
	 */
	function validateDuplicate($house_name, $builder_id, $development_id, $house_id = null)
	{
		$this->db->like('house_name', $house_name, 'none');
		if($development_id){
			$this->db->where('development_id', $development_id);
		}
		else{
			$this->db->where('development_id is null');
		}
		if($house_id){
			$this->db->where('houses.house_id !=', $house_id);
		}
		$this->db->where('houses.house_builder_id', $builder_id);
		$query  = $this->db->get('houses');
		return ($query? $query->row(): FALSE);
	}

	/**
	 * Creates a new record of a house
	 * @param array $form_data
	 * @return int house_id
	 */
	function addHouse($form_data)
	{
		$fields        = $this->getFields();
		$house_details = array();
		foreach($fields as $field){
			if(array_key_exists($field, $form_data)){
				$house_details[$field] = $form_data[$field];
			}
		}
		$this->db->insert('houses', $house_details);
		$house_id = $this->db->insert_id();
		if($house_id){
			// store new images
			$this->load->model('Model_house_image', '', TRUE);
			$this->Model_house_image->updateHouseImages($house_id, $form_data);
		}
		return $house_id;
	}

	/**
	 * Updates the house data
	 * 
	 * @param int $house_id
	 * @param array $form_data
	 * @return boolean
	 */
	function updateHouse($house_id, $form_data)
	{
		$fields        = $this->getFields();
		$house_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$house_details[$field] = $form_data[$field];
			}
		}
		$this->db->where('houses.house_id', $house_id);
		if($this->db->update('houses', $house_details)){

			// update and/or insert new images
			$this->load->model('Model_house_image', '', TRUE);
			$this->Model_house_image->updateHouseImages($house_id, $form_data);
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Updates the house data
	 * 
	 * @param int $house_id
	 * @param array $form_data
	 * @return boolean
	 */
	function requestChange($house_id, $development_id, $form_data)
	{
		$this->load->model('Model_user', '', TRUE);
		$this->load->model('Model_development', '', TRUE);

		$current_user = $this->Model_user->getUserLogged();
		$house        = $this->getHouse($house_id);
		$development  = $this->Model_development->getDevelopment($development_id);
		if(!$house){
			return FALSE;
		}
		$subject                = "Change Requested for house {$house->house_name}";
		$house->requestmessage  = $form_data['request_message'];
		$house->housename       = "{$house->house_name} by {$house->builder_name}";
		$house->houseid         = $house->house_id;
		$house->username        = $current_user->name;
		$house->title           = $subject;
		$house->developmentname = $development->development_name;

		$this->load->model('Model_config', '', TRUE);
		$this->load->library('email_sender');
		$settings = $this->Model_config->getConfig();
		$from     = (object)array('email' => $current_user->email, 'name' => $current_user->name);
		$to       = (object)array('name' => 'MAPOVIS', 'email' => $settings->info_email);
		$notes    = '';

		return $this->email_sender->sendEmailNotificaton($subject, 'request-house-change', $from, $to, $house, null, $notes);
	}

	function deleteHouse($house_id)
	{
		// delete house matches
		$this->db->where('house_id', $house_id);
		$this->db->delete('house_lot_packages');
		
		$this->db->where('house_id', $house_id);
		$deleted = $this->db->delete('houses');
		// if the house was deleted successfully delete the images files and folder
		if($deleted){
			$image_path = BASEPATH."../../mpvs/images/dev_houses/house_{$house_id}/";
			if(file_exists($image_path)){
				foreach(glob($image_path.'*.*') as $v){
					unlink($v);
				}
				rmdir($image_path);
			}
		}
		return $deleted;
	}

	/**
	 * This function will return the house matches for given lot at this stage the development_id is not being used
	 * @param int $development_id
	 * @param int $lot_id
	 * @return array
	 */
	function getLotHouses($development_id, $lot_id)
	{
		$this->load->model('Model_development', '', TRUE);
		$development = $this->Model_development->getDevelopment($development_id);
		$this->db->select('houses.*, house_matches_tmp.*, builders.*, house_lot_packages.house_lot_price, house_lot_packages.file_name, house_lot_packages.file_name AS original_file_name');
		$this->db->join('house_matches_tmp', 'house_matches_tmp.house_id = houses.house_id');
		$this->db->join('builders', 'houses.house_builder_id = builders.builder_id', 'left');
		$this->db->join('house_lot_packages', 'houses.house_id = house_lot_packages.house_id AND house_matches_tmp.lot_id = house_lot_packages.lot_id', 'left');
		$this->db->where('house_matches_tmp.lot_id', $lot_id);
		switch($development->sort_house_matches_by){
			case 'price':
				$this->db->order_by('house_lot_packages.house_lot_price');
				break;
			case 'random':
				$this->db->order_by('RAND()');
				break;
			case 'user_selected_order':
			default:
				$this->db->order_by('house_matches_tmp.order');
				break;
		}
		$houses = $this->db->get('houses')->result_array();
		if(!$houses){
			$houses    = array();
		}
		$this->load->model('Model_house_image', '', TRUE);
		foreach($houses as $index => $house){
			$house_images = $this->Model_house_image->getHouseImages($house['house_id'], array('file_type' => 'facade', 'lot_id' => $lot_id));
			$houses[$index]['house_images'] = array_map(function($v){return $v->file_name;}, $house_images);
		}
		return $houses;
	}

	function houseLandSearchParameters($development_id)
	{
		$this->db->select('MIN(FLOOR(lots.lot_square_meters/100))* 100 AS house_land_size_min, MAX(CEIL(lots.lot_square_meters/100))*100 AS house_land_size_max');
		$this->db->select('MIN(FLOOR(house_lot_packages.house_lot_price/10000))* 10000 AS house_land_price_min, MAX(CEIL(house_lot_packages.house_lot_price/10000))*10000 AS house_land_price_max ');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->join('house_matches_tmp', 'house_matches_tmp.lot_id = lots.lot_id');
		$this->db->join('houses', 'house_matches_tmp.house_id = houses.house_id');
		$this->db->join('house_lot_packages', 'house_lot_packages.lot_id = lots.lot_id AND house_lot_packages.house_id = houses.house_id', 'left');
		$this->db->where_in('lots.status', array('Sold', 'Available'));
		$parameters = $this->db->get('lots')->row_array();
		
		$this->load->model('Model_lot', '', TRUE);
		$other_parameters = $this->Model_lot->landSearchParameters($development_id);
		$parameters['lot_square_meters']      = $other_parameters['lot_square_meters'];
		$parameters['lot_square_meters_from'] = $other_parameters['lot_square_meters_from'];
		$parameters['lot_square_meters_to']   = $other_parameters['lot_square_meters_to'];

		$lot_min_size = $parameters['house_land_size_min'];
		$lot_max_size = $parameters['house_land_size_max'];
		for($size = $lot_min_size; $size < $lot_max_size; $size += LOT_SIZE_INCREMENT){
			$next_size                                               = $size + LOT_SIZE_INCREMENT;
			$parameters['hnl_square_meters']["{$size}|{$next_size}"] = "{$size}m<sup>2</sup> - {$next_size}m<sup>2</sup>";
			$parameters['hnl_square_meters_from'][$size]             = "{$size}m<sup>2</sup>";
			$parameters['hnl_square_meters_to'][$next_size]          = "{$next_size}m<sup>2</sup>";
		}

		// House bedrooms
		$this->db->select('houses.house_bedrooms ');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->join('house_matches_tmp', 'house_matches_tmp.lot_id = lots.lot_id');
		$this->db->join('houses', 'house_matches_tmp.house_id = houses.house_id');
		$this->db->where_in('lots.status', array('Sold', 'Available'));
		$this->db->group_by('houses.house_bedrooms');
		$this->db->order_by('houses.house_bedrooms');
		$parameters['house_bedrooms'] = $this->db->get('lots')->result_array();

		// House bathrooms
		$this->db->select('houses.house_bathrooms');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->join('house_matches_tmp', 'house_matches_tmp.lot_id = lots.lot_id');
		$this->db->join('houses', 'house_matches_tmp.house_id = houses.house_id');
		$this->db->where_in('lots.status', array('Sold', 'Available'));
		$this->db->group_by('houses.house_bathrooms');
		$this->db->order_by('houses.house_bathrooms');
		$parameters['house_bathrooms'] = $this->db->get('lots')->result_array();

		// House levels
		$this->db->select('houses.house_levels');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->join('house_matches_tmp', 'house_matches_tmp.lot_id = lots.lot_id');
		$this->db->join('houses', 'house_matches_tmp.house_id = houses.house_id');
		$this->db->where_in('lots.status', array('Sold', 'Available'));
		$this->db->group_by('houses.house_levels');
		$this->db->order_by('houses.house_levels');
		$parameters['house_levels']    = $this->db->get('lots')->result_array();

		// House builders
		$this->db->select('builders.builder_id, builders.builder_name');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->join('house_matches_tmp', 'house_matches_tmp.lot_id = lots.lot_id');
		$this->db->join('houses', 'house_matches_tmp.house_id = houses.house_id');
		$this->db->join('builders', 'builders.builder_id = houses.house_builder_id', 'left');
		$this->db->where_in('lots.status', array('Available'));
		$this->db->group_by('builders.builder_id');
		$this->db->order_by('builders.builder_id');
		$parameters['builders']    = $this->db->get('lots')->result_array();
		$parameters['order_by']    = array('price', 'builder', 'size');
		return $parameters;
	}

	/**
	 * Returns an array with the list of fields in the database table houses
	 * @return array
	 */
	function getFields()
	{
		return array(
			'development_id',
			'house_name',
			'house_builder_id',
			'house_description',
			'house_moreinfo_url',
			'house_minwidth',
			'house_mindepth',
			'house_maxwidth',
			'house_maxdepth',
			'house_bedrooms',
			'house_bathrooms',
			'house_garages',
			'house_size',
			'house_levels',
			'min_lot_width',
		);
	}

	private function getFilterSizesForQuery($filters)
	{
		$sizes = array();
		if(isset($filters['house_size']) && !empty($filters['house_size'])){
			foreach($filters['house_size'] as $lot_size){
				if(!$lot_size){
					continue;
				}
				@list($min, $max) = explode('|', $lot_size);
				$max_query        = ($max)? " AND houses.house_size <= {$max}":'';
				$sizes[]          = "(houses.house_size >= {$min} {$max_query})";
			}
			if(count($sizes)){
				return '('.implode(' OR ', $sizes).')';
			}
		}
		return false;
	}

}
?>