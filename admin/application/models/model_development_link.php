<?php
Class Model_development_link extends CI_Model
{
	function getDevelopmentLinks($development_id)
	{
		$this->db->select('developments.development_id, developments.*, development_links.distance, development_links.suburb');
		$this->db->join('developments', 'developments.development_id = development_links.development_id1 OR developments.development_id = development_links.development_id2');
		$this->db->where('(development_links.development_id1 = '.(int)$development_id .' OR development_links.development_id2 ='.(int)$development_id.')');
		$this->db->where('developments.development_id != ', $development_id);
		$this->db->order_by('development_links.distance', 'asc');
		$this->db->order_by('developments.development_name', 'asc');
		$query  = $this->db->get('development_links');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function getPossibleDevelopments($development_id)
	{
		$current_developments = $this->getDevelopmentLinks($development_id);
		$developments_ids     = array();
		foreach($current_developments as $current_development){
			$developments_ids[] = $current_development->development_id;
		}
		if(count($developments_ids)){
			$this->db->where_not_in('developments.development_id', $developments_ids);
		}
		$this->db->where('developments.development_id !=', $development_id);
		$this->db->order_by('developments.development_name', 'asc');
		$query  = $this->db->get('developments');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function getDevelopmentLink($development_id1, $development_id2)
	{
		$this->db->select('developments.*, development_links.distance, development_links.suburb');
		$this->db->join('developments', 'developments.development_id = development_links.development_id1 OR developments.development_id = development_links.development_id2');
		$this->db->where('(development_links.development_id1 = '.(int)$development_id1 .' OR development_links.development_id2 ='.(int)$development_id1.')');
		$this->db->where('(development_links.development_id1 = '.(int)$development_id2 .' OR development_links.development_id2 ='.(int)$development_id2.')');
		$this->db->where('developments.development_id !=', $development_id1);
		$query = $this->db->get('development_links');
		return $query->row();
	}

	function updateDevelopmentLink($development_id1, $development_id2, $distance, $suburb)
	{
		$this->deleteDevelopmentlink($development_id1, $development_id2);
		$data = array(
			'development_id1' => $development_id1,
			'development_id2' => $development_id2,
			'distance'        => $distance,
			'suburb'          => $suburb,
		);
		$this->db->insert('development_links', $data);
		return $this->db->insert_id();
	}

	public function deleteDevelopmentlink($development_id1, $development_id2)
	{
		$this->db->where('(development_links.development_id1 = '.(int)$development_id1 .' OR development_links.development_id2 ='.(int)$development_id1.')');
		$this->db->where('(development_links.development_id1 = '.(int)$development_id2 .' OR development_links.development_id2 ='.(int)$development_id2.')');
		return $this->db->delete('development_links');
	}

}
?>