<?php
Class Model_statistics_lotview extends CI_Model
{
	function getViews($development_id, $start_date, $end_date)
	{
		$this->db->select('COUNT(statistics_lotViews.ID) as views, statistics_lotViews.developmentID, statistics_lotViews.lotID, lots.*, stages.*, precincts.*');
		$this->db->from('statistics_lotViews');
		$this->db->join('lots', 'lots.lot_id = statistics_lotViews.lotID', 'left');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('statistics_lotViews.developmentID', $development_id);
		$this->db->where('statistics_lotViews.timestamp /1000 >=', strtotime($start_date));
		$this->db->where('statistics_lotViews.timestamp /1000 <=', strtotime($end_date));
		$this->db->group_by(array('statistics_lotViews.developmentID', 'statistics_lotViews.lotID'));
		$query  = $this->db->get();
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}

	function getViewsByDay($development_id, $start_date, $end_date)
	{
		$this->db->select("COUNT(statistics_lotViews.ID) as views, from_unixtime(statistics_lotViews.timestamp/1000, '%Y-%m-%d' ) as views_date, from_unixtime(statistics_lotViews.timestamp/1000, '%d/%m' ) as formated_date ", FALSE);
		$this->db->from('statistics_lotViews');
		$this->db->where('statistics_lotViews.developmentID', $development_id);
		$this->db->where('statistics_lotViews.timestamp /1000 >=', strtotime($start_date));
		$this->db->where('statistics_lotViews.timestamp /1000 <=', strtotime($end_date));
		/* change pretect identifiers to codeigniter will not escape %Y-%m-%d*/
		$this->db->_protect_identifiers = FALSE;
		$this->db->group_by('from_unixtime(statistics_lotViews.timestamp/1000, \'%Y-%m-%d\' ) ', FALSE);
		$this->db->_protect_identifiers = TRUE;
		$this->db->order_by('views_date', 'asc');
		$query  = $this->db->get();
		$tmp_result = ($query->num_rows() > 0)? $query->result(): array();
		$result     = array();
		foreach($tmp_result as $row){
			$result[$row->views_date] = $row;
		}
		return $result;
	}

	function getWeeklyViews($development_id, $start_date = null, $end_date = null)
	{
		$this->load->library('mapovis_lib');

		$this->db->select("lots.lot_width, YEARWEEK(from_unixtime(statistics_lotViews.timestamp/1000, '%Y-%m-%d'), 1) AS yearweek, COUNT(statistics_lotViews.ID) AS number_lots", FALSE);
		$this->db->select("WEEK(from_unixtime(statistics_lotViews.timestamp/1000, '%Y-%m-%d'), 1) as week_number, from_unixtime(statistics_lotViews.timestamp/1000, '%Y') AS year", FALSE);
		$this->db->from('statistics_lotViews');
		$this->db->join('lots', 'lots.lot_id = statistics_lotViews.lotID', 'left');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('statistics_lotViews.developmentID', $development_id);
		if($start_date){
			$this->db->where('statistics_lotViews.timestamp /1000 >=', strtotime($start_date));
		}
		if($end_date){
			$this->db->where('statistics_lotViews.timestamp /1000 <=', strtotime($end_date));
		}
		$this->db->group_by(array('lots.lot_width', "YEARWEEK(from_unixtime(statistics_lotViews.timestamp/1000, '%Y-%m-%d'), 1)"), FALSE);
		$this->db->order_by('yearweek', 'asc');
		$this->db->order_by('lot_width', 'asc');
		$query            = $this->db->get();
		$result           = $query->result();
		$tmp_widths       = $this->getWidths($development_id, $start_date, $end_date);
		$formated_resutls = array();
		$all_widths       = array();
		if($result)
		{
			foreach($result as $row){
				$lot_width           = (string)$this->mapovis_lib->roundNearestfromarrayvals($tmp_widths, $row->lot_width);
				$date_string         = $row->year . 'W' . sprintf('%02d', $row->week_number);
				$formatted_week      = date('j/n', strtotime($date_string . '7'));
				$row->formatted_date = date('Y-m-d', strtotime($date_string . '7'));;
				$row->formatted_week = $formatted_week;
				if(!in_array($lot_width, $all_widths)){
					$all_widths[] = $lot_width;
				}
				if(!isset($formated_resutls[$formatted_week][$lot_width])){
					$formated_resutls[$formatted_week][$lot_width] = clone($row);
				}
				else{
					$formated_resutls[$formatted_week][$lot_width]->number_lots += $row->number_lots;
				}
			}
		}
		sort($all_widths);
		return array('widths' => $all_widths, 'results' => $formated_resutls);
	}

	private function getWidths($development_id, $start_date = null, $end_date = null)
	{
		$this->db->select('lots.lot_width');
		$this->db->join('lots', 'lots.lot_id = statistics_lotViews.lotID', 'left');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->where('statistics_lotViews.developmentID', $development_id);
		$this->db->where('lots.lot_width % 0.5 = 0 ');
		if($start_date){
			$this->db->where('statistics_lotViews.timestamp /1000 >=', strtotime($start_date));
		}
		if($end_date){
			$this->db->where('statistics_lotViews.timestamp /1000 <=', strtotime($end_date));
		}
		$this->db->group_by('lots.lot_width');
		$this->db->order_by('lot_width', 'asc');

		$query   = $this->db->get('statistics_lotViews');
		$results = $query->result();
		$widths  = array();
		if($results){
			foreach($results as $result){
				$widths[] = (float)$result->lot_width;
			}
		}
		return $widths;
	}

	function addStatisticLotView($form_data)
	{
		$fields        = $this->getFields();
		$statistic_details   = array();
		foreach($fields as $field_id => $field_details){
			if(isset($form_data[$field_id])){
				$statistic_details[$field_id] = $form_data[$field_id];
			}
		}
		return $this->db->insert('statistics_lotViews', $statistic_details);
	}

	function deleteDevelopmentViews($development_id)
	{
		$this->db->where('statistics_lotViews.developmentID', $development_id);
		return $this->db->delete('statistics_lotViews');
	}

	function getFields()
	{
		return array(
			'developmentID' => array('Development', 't'),
			'lotID'         => array('Lot', 't'),
			'timestamp'     => array('Timestamp', 't'),
			'ip_address'    => array('IP Addres', 't'),
			'user_agent'    => array('User Agent', 't'),
		);
	}
}
?>