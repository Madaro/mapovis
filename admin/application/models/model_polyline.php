<?php
Class Model_polyline extends CI_Model
{
	function getPolylines($development_id)
	{
		$this->db->select('polylines.*');
		$this->db->where('developmentID', $development_id);
		$query  = $this->db->get('polylines');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result(); 
		}
		return $result;
	}

	function getPolyline($polyline_id)
	{
		$query  = $this->db->get_where('polylines', array('id' => $polyline_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row(); 
		}
		return $result;
	}

	function updatePolyline($polyline_id, $form_data)
	{
		$fields       = $this->getFields();
		$data_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$data_details[$field] = $form_data[$field];
			}
		}
		if(count($data_details)){
			$this->db->where('id', $polyline_id);
			return $this->db->update('polylines', $data_details);
		}
		return TRUE;
	}

	function deletePolyline($polyline_id)
	{
		$this->db->where('id', $polyline_id);
		return $this->db->delete('polylines');
	}

	function getFields()
	{
		return array(
            'polylinename',
			'stroke_color',
			'stroke_opacity',
            'stroke_weight'
		);
	}

}
?>