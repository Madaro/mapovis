<?php
Class Model_reminder_log extends CI_Model
{
	public function getReminderLogs($filters = array(), $with_time_formatted = TRUE)
	{
		$this->db->select('*, users.name AS user_name');
		$this->db->join('developments', 'developments.development_id = reminder_logs.development_id', 'left');
		$this->db->join('users', 'users.user_id = reminder_logs.user_id', 'left');

		if(isset($filters['development_id']) && $filters['development_id']){
			$this->db->where('developments.development_id', $filters['development_id']);
		}
		if(isset($filters['development_ids'])){
			$this->db->where_in('developments.development_id', $filters['development_ids']);
		}
		$this->db->order_by('reminder_logs.datetime', 'desc');

		$query  = $this->db->get('reminder_logs');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		if($with_time_formatted){
			$this->load->library('mapovis_lib');
			$timezone = $this->mapovis_lib->getTimeZone();
			foreach($result as $row){
				// chaning time to default time zone
				$reminder_datetime   = date_create(date('Y-m-d H:i:s', $row->datetime));
				$reminder_datetime->setTimeZone(timezone_open($timezone));
				$row->formatted_date = date_format($reminder_datetime, 'd/m/Y');
				$row->formatted_time = date_format($reminder_datetime, 'h:ia');
			}
		}
		return $result;
	}

	public function addReminderLog($development_id, $level, $method, $user_id, $sent, $notes, $today_datetime)
	{
		$log_data       = array(
			'user_id'        => $user_id,
			'development_id' => $development_id,
			'level'          => $level,
			'method'         => $method,
			'datetime'       => strtotime($today_datetime),
			'sent'           => $sent,
			'log_notes'      => $notes
		);
		var_dump($log_data);
		$this->db->insert('reminder_logs', $log_data);
		return $this->db->insert_id();
	}
}
?>