<?php
Class Model_pricelist_subscriber extends CI_Model
{
	function getPricelistSubscribers($development_id)
	{
		$this->db->select('pricelist_subscribers.*');
		$this->db->where('development_id', $development_id);
		$query  = $this->db->get('pricelist_subscribers');
		$result = array();
		if ($query->num_rows() > 0)
		{
			$result = $query->result(); 
		}
		return $result;
	}

	function getPricelistSubscriber($pricelist_subscriber_id)
	{
		$query  = $this->db->get_where('pricelist_subscribers', array('pricelist_subscriber_id' => $pricelist_subscriber_id));
		$result = null;
		if ($query->num_rows() > 0)
		{
			$result = $query->row(); 
		}
		return $result;
	}

	function addPricelistSubscriber($development_id, $form_data)
	{
		$fields       = $this->getFields();
		$data_details = array('development_id' => $development_id);
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$data_details[$field] = $form_data[$field];
			}
		}
		return $this->db->insert('pricelist_subscribers', $data_details);
	}

	function updatePricelistSubscriber($pricelist_subscriber_id, $form_data)
	{
		$fields       = $this->getFields();
		$data_details = array();
		foreach($fields as $field){
			if(isset($form_data[$field])){
				$data_details[$field] = $form_data[$field];
			}
		}
		if(count($data_details)){
			$this->db->where('pricelist_subscriber_id', $pricelist_subscriber_id);
			return $this->db->update('pricelist_subscribers', $data_details);
		}
		return TRUE;
	}

	function deletePricelistSubscriber($pricelist_subscriber_id)
	{
		$this->db->where('pricelist_subscriber_id', $pricelist_subscriber_id);
		return $this->db->delete('pricelist_subscribers');
	}

	function getFields()
	{
		return array(
			'subscriber_name',
			'subscriber_company',
			'subscriber_email',
			'office_phone',
			'mobile_phone',
			'job_title',
		);
	}

}
?>