<?php
Class Model_statistics_interactivemasterplan extends CI_Model
{
	
	function getViewsByDay($development_id, $start_date, $end_date)
	{
		$this->db->select("COUNT(statistics.ID) as views, from_unixtime(statistics.timestamp/1000, '%Y-%m-%d' ) as views_date, from_unixtime(statistics.timestamp/1000, '%d/%m' ) as formated_date ", FALSE);
		$this->db->from('statistics');
		$this->db->where('statistics.developmentID_opened', $development_id);
		$this->db->where('statistics.timestamp /1000 >=', strtotime($start_date));
		$this->db->where('statistics.timestamp /1000 <=', strtotime($end_date));
		/* change pretect identifiers to codeigniter will not escape %Y-%m-%d*/
		$this->db->_protect_identifiers = FALSE;
		$this->db->group_by('from_unixtime(statistics.timestamp /1000, \'%Y-%m-%d\' ) ', FALSE);
		$this->db->_protect_identifiers = TRUE;
		$this->db->order_by('views_date', 'asc');
		$query      = $this->db->get();
		$tmp_result = ($query->num_rows() > 0)? $query->result(): array();
		$result     = array();
		foreach($tmp_result as $row){
			$result[$row->views_date] = $row;
		}
		return $result;
	}
    
}
?>