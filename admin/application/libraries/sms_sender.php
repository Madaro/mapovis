<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  SMS sender library
* Created:  01.02.2014
*/

class sms_sender
{
	var $ci   = null;
	var $keys = null;

	/**
	 * __construct
	 *
	 * @return void
	 **/
	public function __construct()
	{
		$this->ci   =& get_instance();
		$this->ci->config->load('sms');
		$this->keys = $this->ci->config->item('sms');
	}

	/**
	 * Sends SMS notifications
	 * @param string $text
	 * @param object $object_values
	 * @param string $mobile_number
	 * @param string $successfail use as a way to provide a message while returning a boolean value
	 * @return boolean
	 */
	public function sendSms($text, $object_values, $mobile_number, &$successfail)
	{
		$text       = urlencode($this->formatMessage($object_values, $text));
		$mobile_num = $this->formatMobileNumber($mobile_number);

		$user       = $this->keys['sms_username'];
		$password   = $this->keys['sms_password'];
		$api_id     = $this->keys['sms_api_id'];
		$baseurl    = $this->keys['sms_baseurl'];

		// auth call
		$url        = "{$baseurl}/http/auth?user={$user}&password={$password}&api_id={$api_id}";

		// do auth call
		$ret        = file($url);

		// explode our response. return string is on first line of the data returned
		$this->session = explode(':',$ret[0]);
		if ($this->session[0] == 'OK')
		{
			$sess_id = trim($this->session[1]); // remove any whitespace
			$url     = "{$baseurl}/http/sendmsg?session_id={$sess_id}&to={$mobile_num}&text={$text}";

			// do sendmsg call
			$ret     = file($url);
			$send    = explode(':',$ret[0]);

			if ($send[0] == 'ID') {
				$successfail = 'successnmessage ID: '. $send[1];
				return TRUE;
			} else {
				$successfail = 'send message failed';
			}
		} else {
			$successfail = 'Authentication failure: '. $ret[0];
		}
		return FALSE;
	}

	/**
	 * convers the mobile number to a valid nunmber if necessary (i.e '0412 345 678' => '61412345678' )
	 * @param type $mobile_number
	 * @return type
	 */
	private function formatMobileNumber($mobile_number)
	{
		$formatted_mobile = str_replace(array('04', ' ', '+', '(', ')'), array('614', '', '', '', ''), $mobile_number);
		return $formatted_mobile;
	}

	/**
	 * Replace key_string values with given data (i.e {:developmentname:})
	 * @param object $object_values
	 * @param string $message
	 * @return string
	 */
	private function formatMessage($object_values, $message)
	{
		$message_keys = array(
			'developmentname',
			'updatedeadline',
			'primaryuser',
			'secondaryuser',
			'manageruser',
			'manager',
			'deadline',
			'duedate',
			'overduehours',
		);
		$message_key_values = array();
		foreach($message_keys as $message_keys){
			if(isset($object_values->$message_keys)){
				$message_key_values["{:{$message_keys}:}"] = $object_values->$message_keys;
			}
		}
		return str_replace(array_keys($message_key_values), array_values($message_key_values), $message);
	}
}
