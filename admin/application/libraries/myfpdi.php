<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(dirname(__FILE__) . '/tcpdf/tcpdf.php');
require_once(dirname(__FILE__) . '/fpdi_1.5.1/fpdi.php');

class MyFpdi extends FPDI
{
	function __construct()
	{
		parent::__construct();
	}
}

/* End of file MyDpdi.php */