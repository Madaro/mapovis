<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Mapovis library
* Created:  20.01.2014
*/

class mapovis_lib
{
	var $todaystatic = FALSE;
	var $today       = NULL;

	/**
	 * __construct
	 *
	 * @return void
	 **/
	public function __construct(){}

	public function getTimeDifference($time_value){
		$diff = time() - $time_value;
		return $this->time_elapsed($diff);
	}

	public function getToday($state = ''){
		$time_zone = $this->getTimeZone($state);
		$today     = date_create($time_zone);
		if($this->todaystatic){
			if(!$this->today){
				$this->today = $today;
			}
			$this->today->setTimeZone(timezone_open($time_zone));
			return $this->today;
		}
		return $today;
	}

	public function formatDate($date_time){
		return date('d/m/Y', $date_time);
	}

	public function changeLogNote($field_title, $field_type, $prev_val, $new_val){
		static $changed = 'Changed ';
		switch($field_type){
			case 'n':
				$values = array(number_format($prev_val, 2), number_format($new_val, 2));
				break;
			case 'f':
				$values = array((float)$prev_val, (float)$new_val);
				break;
			case 't':
			default:
				$values = array($prev_val, $new_val);
				break;
		}
		$change_txt = trim("{$field_title} {$changed}from ". implode(' to ', $values));
		$changed    = '';
		return $change_txt;
	}

	public function formatTime($date_time){
		return date('h:i a', $date_time);
	}

	public function orderItemsByKey($array_values, $key_name)
	{
		$tmp_values = array();
		foreach($array_values as $array_val){
			$tmp_values[$array_val->$key_name] = $array_val;

		}
		return $tmp_values;
	}

	public function logInUser($user_details, $class)
	{
		$class->session->set_userdata('user_logged_id', $user_details->user_id);
		$class->session->set_userdata('user_logged_role', $user_details->user_role);
		$class->session->set_userdata('user_logged_username', $user_details->username);

		// Set Cookie for LatLong Display System 
		$class->load->helper('cookie');
		$cookie = array(
			'name'         => 'user_logged_role',
			'userloggedin' => TRUE,
			'value'        => $user_details->user_role,
			'expire'       => '15000000',
			'prefix'       => '',
		);

		$class->input->set_cookie($cookie); 
	}

	public function logInBuilder($builder_details, $class)
	{
		$class->session->set_userdata('builder_logged_id', $builder_details->user_id);
		$class->session->set_userdata('builder_logged_role', $builder_details->user_role);
		$class->session->set_userdata('builder_logged_username', $builder_details->username);

		// Set Cookie for LatLong Display System 
		$class->load->helper('cookie');
		$cookie = array(
			'name'         => 'builder_logged_role',
			'userloggedin' => TRUE,
			'value'        => $builder_details->user_role,
			'expire'       => '15000000',
			'prefix'       => '',
		);

		$class->input->set_cookie($cookie); 
	}

	public function logInAdmin($user_details, $class)
	{
		$class->session->set_userdata('logged_in_id', $user_details->user_id);
		$class->session->set_userdata('logged_in_role', $user_details->user_role);
		$class->session->set_userdata('logged_in_username', $user_details->username);

		// Set Cookie for LatLong Display System 
		$class->load->helper('cookie');
		$cookie = array(
			'name'          => 'admin_logged_role',
			'value'         => $user_details->user_role,
			'expire'        => '15000000',
			'prefix'        => '',
			'adminloggedin' => TRUE
		);
		$class->input->set_cookie($cookie); 
	}

	private function time_elapsed($secs){
		$bit = array(
			'year'  => $secs / 31556926 % 12,
			'weeks' => $secs / 604800 % 52,
			'days'  => $secs / 86400 % 7,
			'hours' => $secs / 3600 % 24,
			'min'   => $secs / 60 % 60,
			'sec'   => $secs % 60
		);

		foreach($bit as $k => $v){
			if($v > 0){
				$ret[] = "{$v} {$k}";
			}
		}
		return join(' ', $ret);
	}

	public function timeDiffFormat($timediff){
		$bit = array(
			'y' => 'year',
			'm' => 'months',
			'd' => 'days',
			'h' => 'hours',
			'i' => 'min',
		);
		$ret    = array();
		$active = 0;
		foreach($bit as $time_id => $title){
			if($timediff->$time_id > 0){
				$ret[] = $timediff->$time_id.' '.$title;
				$active++;
			}
			if($active >= 2){
				break;
			}
		}
		return implode(' ', $ret);
	}

	public function getTimeZone($state = '')
	{
		$time_zones = $this->getStatesName();
		if(isset($time_zones[$state])){
			return "Australia/{$time_zones[$state]}";
		}
		return 'Australia/NSW';
	}

	public function getStatesName()
	{
		return array(
			'NSW' => 'NSW',
			'VIC' => 'Victoria',
			'ACT' => 'ACT',
			'TAS' => 'Tasmania',
			'NT'  => 'North',
			'SA'  => 'South',
			'QLD' => 'Queensland',
			'WA'  => 'West'
		);
	}

	public function replaceMessageKeys($object_values, $message)
	{
		$replace_keys_values = array(
			'{:development_name:}'     => @$object_values->development_name,
			'{:primary_user_name:}'    => @$object_values->primary_user_name,
			'{:secondary_user_name:}'  => @$object_values->secondary_user_name,
			'{:manager_user_name:}'    => @$object_values->manager_user_name,
			'{:deadline:}'             => @$object_values->deadline,
			'{:duedate:}'              => @$object_values->duedate,
			'{:overduehours:}'         => @$object_values->overduehours,
		);
		return str_replace(array_keys($replace_keys_values), arra_values($replace_keys_values), $message);
	}

	function convert_number_to_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
		);

		if (!is_numeric($number)) {
			return false;
		}

		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			return false;
		}

		if ($number < 0) {
			return $negative . $this->convert_number_to_words(abs($number));
		}

		$string = $fraction = null;

		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}

		switch (true) {
			case $number < 21:
				$string = $dictionary[$number];
				break;
			case $number < 100:
				$tens   = ((int) ($number / 10)) * 10;
				$units  = $number % 10;
				$string = $dictionary[$tens];
				if ($units) {
					$string .= $hyphen . $dictionary[$units];
				}
				break;
			case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
				if ($remainder) {
					$string .= $conjunction . $this->convert_number_to_words($remainder);
				}
				break;
			default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
				if ($remainder) {
					$string .= $remainder < 100 ? $conjunction : $separator;
					$string .= $this->convert_number_to_words($remainder);
				}
				break;
		}

		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	public function getHouseAndLandViews()
	{
		$files_path = BASEPATH.'../application/views/houseandland/views/';
		$files      = scandir($files_path);
		$file_views = array();
		foreach($files as $file){
			$file_details = pathinfo($file);
			if(strtolower($file_details['extension']) == 'php'){
				$file_views[] = $file_details['filename'];
			}
		}
		return $file_views;
	}

	public function getLandViews()
	{
		$files_path = BASEPATH.'../application/views/land/views/';
		$files      = scandir($files_path);
		$file_views = array();
		foreach($files as $file){
			$file_details = pathinfo($file);
			if(strtolower($file_details['extension']) == 'php'){
				$file_views[] = $file_details['filename'];
			}
		}
		return $file_views;
	}

	public function getLandPdfViews()
	{
		$files_path = BASEPATH.'../application/views/land/pdf_views/';
		$files      = scandir($files_path);
		$file_views = array();
		foreach($files as $file){
			$file_details = pathinfo($file);
			if(strtolower($file_details['extension']) == 'php'){
				$file_views[] = $file_details['filename'];
			}
		}
		return $file_views;
	}

	public function getTemplateViews()
	{
		$files_path = BASEPATH.'../application/views/templates/views/';
		$files      = scandir($files_path);
		$file_views = array();
		foreach($files as $file){
			$file_details = pathinfo($file);
			if(strtolower($file_details['extension']) == 'php'){
				$file_views[] = $file_details['filename'];
			}
		}
		return $file_views;
	}

	public function roundNearestfromarrayvals($possible_values, $value_to_round)
	{
		if(in_array(round($value_to_round * 2)/2, $possible_values)){
			$lot_width = round($value_to_round * 2)/2;
		}
		elseif(in_array(floor($value_to_round * 2)/2, $possible_values)){
			$lot_width = floor($value_to_round * 2)/2;
		}
		elseif(in_array(ceil($value_to_round * 2)/2, $possible_values)){
			$lot_width = ceil($value_to_round * 2)/2;
		}
		else{
			$lot_width = (float)$value_to_round;
		}
		return $lot_width;
	}

	public function statisticsColorCodes()
	{
		return array('blue', 'orange', 'brown', 'green', 'red', 'grey', 'purple', 'Teal', 'GoldenRod', 'darkred', 'MediumSeaGreen', 'crimson', 'DarkSlateGray', 'chocolate', 'YellowGreen', 'darkolivegreen', 'OrangeRed');
	}

}
