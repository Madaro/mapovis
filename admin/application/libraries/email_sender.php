<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once FCPATH.'assets'.DIRECTORY_SEPARATOR.'plugins'.DIRECTORY_SEPARATOR.'mandrill-api-php'.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'Mandrill.php';

/**
* Name:  SMS sender library
* Created:  01.02.2014
* This library sends emails using mandrill
*/

class email_sender
{
	var $ci   = null;
	var $keys = null;

	/**
	 * __construct
	 *
	 * @return void
	 **/
	public function __construct()
	{
		$this->ci   =& get_instance();
		$this->ci->config->load('email');
		$this->keys = $this->ci->config->item('mandrill');
	}

	/**
	 * Sends email notifications
	 * @param string $subject
	 * @param string $template_name
	 * @param object $from
	 * @param object $to_user
	 * @param object $object_values
	 * @param string $tags
	 * @param string $notes use as a way to provide a message while returning a boolean value, can be either a success or an error message
	 * @return boolean
	 */
	public function sendEmailNotificaton($subject, $template_name, $from, $to_user, $object_values, $tags, &$notes, $attachments = NULL, $reply_to = NULL)
	{
		$global_merge_vars = $this->getGlobalVars($object_values);
		$subject           = $this->formatMessage($object_values, $subject);

		try {
			$mandrill         = new Mandrill($this->keys['apikey']);
			$template_content = '';
			$message          = array(
				'text'       => $subject,
				'subject'    => $subject,
				'from_email' => $from->email,
				'from_name'  => $from->name,
				'to'         => array(
					array(
						'email' => $to_user->email,
						'name'  => $to_user->name,
						'type'  => 'to'
					)
				),

				'important'           => false,
				'track_opens'         => true,
				'track_clicks'        => true,
				'auto_text'           => null,
				'auto_html'           => null,
				'inline_css'          => true,
				'url_strip_qs'        => null,
				'preserve_recipients' => null,
				'view_content_link'   => null,
				'tracking_domain'     => null,
				'signing_domain'      => null,
				'return_path_domain'  => null,
				'merge'               => true,
				'global_merge_vars'   => $global_merge_vars,
				'tags'                => array($this->formatMessage($object_values, $tags))
			);
			if($reply_to && !empty($reply_to)){
				$message['headers'] =  array(
					'Reply-To' => $reply_to
				);
			}
			if($attachments){
				$message['attachments'] = $attachments;
			}
			$notes = $mandrill->messages->sendTemplate($template_name, $template_content, $message);
			return TRUE;
		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			$notes = 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		}
		return FALSE;
	}
	
	/**
	 * creates an array with global variables to provide to Mandrill to use for the templates
	 * @param object $object_values
	 * @return array
	 */
	private function getGlobalVars($object_values)
	{
		if(!isset($object_values->urlwebsite)){
			$object_values->urlwebsite = str_replace('https', 'http', base_url());
		}
		$global_keys = array(
			'username',
			'urlwebsite',
			'newpassword',
			'fullname',
			'title',
			'developmentname',
			'developmentid',
			'updatedeadline',
			'primaryuser',
			'secondaryuser',
			'manageruser',
			'manager',
			'timeoverdue',
			'housename',
			'houseid',
			'requestmessage',
			'lotnumber',
			'builderusername',
			'packagerequestid',
			'builder',
			'packageprice',
			'pdfurl',
			'rejectionmessage',
		);
		$global_merge_vars = array();
		foreach($global_keys as $global_key){
			if(isset($object_values->$global_key)){
				$global_merge_vars[] = array(
					'name'    => $global_key,
					'content' => $object_values->$global_key,
				);
			}
		}
		return $global_merge_vars;
	}

	/**
	 * Replace key_string values with given data (i.e {:developmentname:})
	 * @param object $object_values
	 * @param string $message
	 * @return string
	 */
	private function formatMessage($object_values, $message)
	{
		$message_keys = array(
			'developmentname',
			'updatedeadline',
			'primaryuser',
			'secondaryuser',
			'manageruser',
			'manager',
			'deadline',
			'duedate',
			'overduehours',
			'timeoverdue',
		);
		$message_key_values = array();
		foreach($message_keys as $message_keys){
			if(isset($object_values->$message_keys)){
				$message_key_values["{:{$message_keys}:}"] = $object_values->$message_keys;
			}
		}
		return str_replace(array_keys($message_key_values), array_values($message_key_values), $message);
	}
}
