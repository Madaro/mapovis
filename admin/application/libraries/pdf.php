<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Pdf extends TCPDF
{
	var $header = '';
	var $footer = '';
	function __construct()
	{
		parent::__construct();
	}

	// Page header
	public function Header() {
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 8, 'This PDF was generated at '.date('h:ia \A\E\S\T \o\n d/m/Y'), 0, true, 'C', 0, '', 0, false, 'T', 'M');
		if(!empty($this->header)){
			$this->writeHTML($this->header, false, false, true, false, '');
		}
	}

	public function setFooterHtml($html_footer) {
		$this->footer = $html_footer;
	}

	public function setHeaderHtml($html_header) {
		$this->header = $html_header;
	}

	// Page footer
	public function Footer() {
		$tmp_margin = $this->bMargin;
		$this->SetAutoPageBreak(false, 0);
		// Page number
		$this->writeHTML($this->footer, false, false, true, false, '');
		$this->SetAutoPageBreak(TRUE, $tmp_margin);
	}
}

/* End of file Pdf.php */