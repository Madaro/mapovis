<?php
$based_url    = base_url();
$folder_url   = $based_url.'application/views/land/';
$first_stage  = true;
$manual_price = ($development->show_pricing == 0);
$land_price   = ($manual_price)? (float)$development->land_starting_from: FALSE;
$include_price = (isset($include_price))? $include_price: FALSE;
?>
<style>
body{
	font-size: 10px;
	font-weight: normal;
}
h2{
	font-size: 18px;
}
h3{
	font-size: 15px;
}
h4{
	font-size: 13px;
}
.content{
	font-family: "gotham-book", "Times New Roman", times;
	font-size: 10px;
	font-weight: normal;
}
table{
	text-align: center;
}
table td{
	padding-left: 7px;
	padding-bottom: 7px;
	padding-top: 5px;
	line-height: 20px;
	background-color: #f3f3f3;
}
table .sold{
	color: #878786;
}
table .sold_row{
	background-color: #f3f3f3;
	color: #c00;
}
table .available_row{
	background-color: #63a04b;
}
.tablex {
	border: 1px solid #dbdbda;
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
}
.tablex th,
.tablex td {
	border: 1px solid #dbdbda;
}
.tablex td {
	font-size: 9px;
}
.btn-danger {
	background-color: #D43F3A;
	color: #FFFFFF;
	font-weight: bold;
}
.danger_row {
	background-color: #F2DEDE;
}
.success_row {
	background-color: #DFF0D8;
}
.btn-success {
	background-color: #3E8F3E;
	color: #FFFFFF;
	font-weight: bold;
}
.progress {
	background-color: #F5F5F5;
}
</style>

	<div class="content">
	<?php if($include_price): ?>
		<h2><?= $development->development_name;?> (<?= $development->developer;?>)</h2>
	<?php endif; ?>
	<?php foreach($stages as $stage):?>
		<?php if(!$first_stage):?>
			<br pagebreak="true"/>
		<?php endif;?>
		<?php $first_stage = FALSE;?>
	<!-- LAND - Stage -->
	<?php if(($manual_price && $land_price) || (!$manual_price && $stage->min_price)):?>
		<!-- <h3>LAND NOW SELLING FROM $<?= number_format(($land_price? $land_price: $stage->min_price));?></h3> -->
	<?php endif;?>
		<h4><?= $development->development_name;?> - Stage <?= ucwords($stage->stage_code);?></h4>

		<table class="tablex" align="center">
		<?php foreach($stage->lots as $lot):?>
			<tr class="<?= strtolower($lot->status);?>">
				<td style="font-size: 9px;">LOT <?= $lot->lot_number;?></td>
				<td style="font-size: 9px;"><b><?= $lot->lot_square_meters;?> m<sup>2</sup></b> Land Size</td>
				<td style="font-size: 9px;"><b><?= round($lot->lot_width, 5);?><sup>m</sup></b> Frontage</td>
				<?php if($include_price): ?>
					<td style="font-size: 9px;"><b>$<?= number_format($lot->price_range_min);?></b></td>
				<?php endif; ?>
				<td style="font-size: 8px;" class="<?= strtolower($lot->status);?>_row">
					<b><?= strtoupper($lot->status);?></b>
				</td>
			</tr>
		<?php endforeach;?>
		</table>
	<br />
	<?php endforeach;?>
	</div>
