<?php
$based_url    = base_url();
$folder_url   = $based_url.'application/views/land/';
$first_stage  = true;
$manual_price = ($development->show_pricing == 0);
$land_price   = ($manual_price)? (float)$development->land_starting_from: FALSE;
$include_price = (isset($include_price))? $include_price: FALSE;
?>
<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<style>
body{
	font-family: "Montserrat", serif;
	font-size: 10px;
	font-weight: normal;
}
h2{
	font-size: 18px;
}
h3{
	font-size: 15px;
}
h4{
	font-size: 13px;
}
.content{
	font-family: "Montserrat", serif;
	font-size: 10px;
	font-weight: normal;
}
table{
	text-align: center;
}
table td{
	padding-left: 7px;
	padding-bottom: 7px;
	padding-top: 5px;
	line-height: 20px;
	background-color: #f3f3f3;
}
table .sold{
	color: #878786;
}
table .sold_row{
	background-color: #f3f3f3;
	color: #c00;
}
table .available_row{
	background-color: #68c7ed;
}
.tablex {
	border: 1px solid #dbdbda;
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
}
.tablex th,
.tablex td {
	border: 1px solid #dbdbda;
}
.tablex td {
	font-size: 9px;
}
.btn-danger {
	background-color: #D43F3A;
	color: #FFFFFF;
	font-weight: bold;
}
.danger_row {
	background-color: #F2DEDE;
}
.success_row {
	background-color: #DFF0D8;
}
.btn-success {
	background-color: #3E8F3E;
	color: #FFFFFF;
	font-weight: bold;
}
.progress {
	background-color: #F5F5F5;
}
.logo
{

}
.disclaimer
{
font-size: 6px;
font-family: "times new roman";	
}
</style>

	<div class="content">
		<div class="logo">
			<img src="http://ca76b878bcfccb1b0472-3dc2e8938bbcef3ef93f347d29e92ddd.r1.cf4.rackcdn.com/pdf/circa-pdf-logo-v2.jpg" height="50px">
		</div>
	<?php if($include_price): ?>
		<h2><?= $development->development_name;?> (<?= $development->developer;?>)</h2>
	<?php endif; ?>
	<?php foreach($stages as $stage):?>
		<?php if(!$first_stage):?>
			<br pagebreak="true"/>
		<?php endif;?>
		<?php $first_stage = FALSE;?>
	<!-- LAND - Stage -->
	<?php if(($manual_price && $land_price) || (!$manual_price && $stage->min_price)):?>
	
	<?php endif;?>
		<h4><?= $stage->stage_name;?></h4>

		<table class="tablex" align="center">
		<?php foreach($stage->lots as $lot):?>
			<tr class="<?= strtolower($lot->status);?>">
				<td style="font-size: 9px;">LOT <?= $lot->lot_number;?></td>
				<td style="font-size: 9px;"><b><?= $lot->lot_square_meters;?> m<sup>2</sup></b> Land Size</td>
				<td style="font-size: 9px;"><b><?= round($lot->lot_width, 5);?><sup>m</sup></b> Frontage</td>
				<td style="font-size: 9px;"><b><?= round($lot->lot_depth, 5);?><sup>m</sup></b> Depth</td>
				<?php if($include_price): ?>
					<td style="font-size: 9px;"><b>$<?= number_format($lot->price_range_min);?></b></td>
				<?php endif; ?>
				<td style="font-size: 8px;" class="<?= strtolower($lot->status);?>_row">
					<b><?= strtoupper($lot->status);?></b>
				</td>
			</tr>
		<?php endforeach;?>
		</table>
	<br />
	<?php endforeach;?>
	</div>

<div class="footer">
<img src="http://ca76b878bcfccb1b0472-3dc2e8938bbcef3ef93f347d29e92ddd.r1.cf4.rackcdn.com/pdf/circa-pdf-footer.jpg" height="40px">
</div>
<div class="disclaimer">
Information contained within this document is believed to be accurate but may change at any time without notice. Maps and plans are indicative only and are not to scale. Plans are subject to final design and council approvals. No warranty is given that the information contained in this document will remain accurate, complete and current. To the extent permitted by law, Anaya Pty Ltd 130 972 253 its related bodies corporate, their officers, employees, and agents exclude all liability for any loss which arises as a result of any reliance on the information contained in this document or otherwise in connection with it. Prospective purchasers should undertake their own enquiries for their own benefit and satisfaction and should rely on the contents of any contract of sale and formal disclosure statements.
</div>