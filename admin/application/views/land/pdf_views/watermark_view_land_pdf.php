<?php
$based_url    = base_url();
$folder_url   = $based_url.'application/views/land/';
$first_stage  = true;
$manual_price = ($development->show_pricing == 0);
$land_price   = ($manual_price)? (float)$development->land_starting_from: FALSE;
$include_price = (isset($include_price))? $include_price: FALSE;
?>
<style>
body{
        font-size: 10px;
        font-weight: normal;
}
h2{
        font-size: 18px;
}
h3{
        font-size: 15px;
}
h4{
        font-size: 13px;
}
.content{
        font-family: "CircularStd-Book";
        font-size: 10px;
        font-weight: normal;
}
table{
        width: 100%; 
        text-align: center;
        font-size: 10px;
        font-weight: bold;
        border-collapse: collapse;
        }

table td{
        padding-left: 7px;
        padding-bottom: 7px;
        padding-top: 5px;
        line-height: 20px;
        background-color: #f3f3f3;
}
table.noformat td{
        background-color: #ffffff;
        font-weight:normal;
}

table td.header{
        background-color: #<?= $development->pdf_footer_header_colour; ?>; 
        font-size: 12px;
        }

table td.header b{
        font-size: 14px;
        }

table td.row{
        background-color: #<?= $development->pdf_footer_row_colour; ?>; 
        }
        

table .sold{
        color: #878786;
}
table .sold_row{
        background-color: #f3f3f3;
        color: #c00;
}
table .available_row{
        background-color: #13a89e;
}



.tablex {
        border: 1px solid #dbdbda;
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
}
.tablex th,
.tablex td {
        border: 1px solid #dbdbda;
}
.tablex td {
        font-size: 9px;
}
.btn-danger {
        background-color: #D43F3A;
        color: #FFFFFF;
        font-weight: bold;
}
.danger_row {
        background-color: #F2DEDE;
}
.success_row {
        background-color: #DFF0D8;
}
.btn-success {
        background-color: #3E8F3E;
        color: #FFFFFF;
        font-weight: bold;
}
.progress {
        background-color: #F5F5F5;
}
.disclaimer
{
font-size: 7px;
font-family: "times new roman";
}
.disclaimer
{
font-size: 10px;
}

.tableheader
{
background-color: #000000;    
}

</style>

        <div class="content">
        <?php foreach($stages as $stage):?>
                <?php if(!$first_stage):?>
                        <br pagebreak="true"/>
                <?php endif;?>
                <?php $first_stage = FALSE;?>

                        <?php if(!$include_price): ?>
                
                            <table class="noformat">
                             <tr>
                                 <td align="left"><h4><?= $stage->stage_name;?></h4></td>
                                 <?php if(!empty($stage->title_release)):?>
                                 <td align="right">Title Release: <?= $stage->title_release;?></td>
                                 <?php endif;?>
                            </tr>
                         </table>
                        <br/>

                        <?php else: // if Builder PDF insert Title Release dates?>
                         <table class="noformat">
                             <tr>
                                 <td align="left"><h4><?= $stage->stage_name;?></h4></td>
                                 <?php if(!empty($stage->title_release)):?>
                                 <td align="right">Title Release: <?= $stage->title_release;?></td>
                                 <?php endif;?>
                            </tr>
                         </table>
                        <br/>
                        <?php endif; ?>
              

                <table class="tablex" align="center">
                            <tr class="tableheader">
                                <td style="font-size: 9px; background-color: #444444; color:#FFFFFF;"><b>LOT NO.</b></td>
                                <td style="font-size: 9px; background-color: #444444; color:#FFFFFF;"><b>AREA (m<sup>2</sup>)</b></td>
                                <td style="font-size: 9px; background-color: #444444; color:#FFFFFF;"><b>FRONTAGE (m)</b></td>
                                <?php if($include_price): ?>
                                        <td style="font-size: 9px; background-color: #444444; color:#FFFFFF;"><b>PRICE</b></td>
                                <?php endif; ?>
                                <td style="font-size: 8px; background-color: #444444; color:#FFFFFF;" class="status"><b>STATUS</b></td>
                             </tr>
                <?php foreach($stage->lots as $lot):?>
                        
                        <?php if ($lot->status == "Available"): ?>
                        <tr nobr="true" class="<?= strtolower($lot->status);?>">
                                <td style="font-size: 9px;">LOT <?= $lot->lot_number;?></td>
                                <td style="font-size: 9px;"><b><?= $lot->lot_square_meters;?> m<sup>2</sup></b> Land Size</td>
                                <td style="font-size: 9px;"><b><?= round($lot->lot_width, 5);?><sup>m</sup></b> Frontage</td>
                                <?php if($include_price): ?>
                                        <td style="font-size: 9px;"><b>$<?= number_format($lot->price_range_min);?></b></td>
                                <?php endif; ?>
                                <td style="font-size: 8px;" class="<?= strtolower($lot->status);?>_row">
                                        <b><?= strtoupper($lot->status);?></b>
                                </td>
                        </tr>
                        <?php endif; ?>

                <?php endforeach;?>
                </table>
        <br />
        <?php endforeach;?>
        </div>

<?php
// Print Disclaimer if it is a builder PDF
 if($include_price): ?>
<br pagebreak="true"/>
<div class="terms">

                <h2>Terms</h2>
                <hr>
                <div style="padding-bottom:10px;"></div>
                <?= $development->terms_and_conditions_builder;?>

</div>

<div class="disclaimer">
        <h2>Disclaimer</h2>
        <hr>
        <div style="padding-bottom:10px;"></div>
        <?= $development->diclaimer_builder;?>
</div>

<?php endif; ?>