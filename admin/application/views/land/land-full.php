<?php
$based_url  = base_url();
$folder_url = $based_url.'application/views/land/';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MAPOVIS - Land Page PROTOTYPE Example</title>

	<!-- Bootstrap Holder JS (for temporary thumbnail display)-->
	<script src="<?= $folder_url;?>assets/bootstrap/js/holder.js"></script>

	<!-- Bootstrap -->
	<link href="<?= $folder_url;?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- Bootstrap theme -->
	<link href="<?= $folder_url;?>assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="<?= $folder_url;?>css/custom-css.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!--[if gte IE 9]>
	<style type="text/css">
	.gradient {
		filter: none;
	}
	</style>
<![endif]-->
</head>

<body>
	<!-- /START CONTAINER -- CURL will pull in from here onwards.
	================================================== -->
	<div class="container">
		<?php $this->load->view($page_view); ?>

	<!-- close container -->
	</div>

	<!-- /END CONTAINER 
	================================================== -->

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?= $folder_url;?>assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>
