<?php
$base_url       = 'http://app.mapovis.com.au/';
$image_base_url = $base_url.'mpvs/images/dev_houses/';
$counter        = 0;
?>
<!-- 	<div class="row">
		<div class="col-md-4 col-xs-12 col-md-push-4">
			<p class="download-caption"><?= $development->development_name;?> Stage Plan Downloads <i class="icon-arrow-down"></i></p>

			<div class="row btns-holder">
				<div class="col-md-10 col-md-push-1 col-xs-12">
					<a class="btn btn-primary gaTracking" data-ga-action="Download" data-ga-category="PDF" data-ga-title="First Release" href="assets/Tulliallan_First_Release.pdf">Download First Release</a>
				</div>
			</div>
		</div>
	</div> -->

	<div class="row">
		<div class="col-md-4 col-xs-12">
			<aside id="sidebar">
				<section class="widget forms-widget">
					<header class="heading">
						<h2>Search Here <span>for your new home</span></h2>

						<ul class="radio-tabs">
							<li><label><input class="tab-change" data-box="#form-1" name="radio-tabs" type="radio">Search For House &amp; Land</label></li>

							<li><label><input checked class="tab-change" data-box="#form-2" name="radio-tabs" type="radio">Search For Land Only</label></li>
						</ul>
					</header>

					<form action="living-options.html" class="form" id="form-1" name="form-1">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
								<label for="select-field-03"><i class="icon-size"></i> Land Size</label>

								<div class="row select-row">
									<div class="col-lg-6 col-md-12 col-xs-6">
										<select class="form-control" id="select-field-03" name="lot_square_meters_from">
											<option value="">From Any</option>
											<?php foreach($search_parameters['lot_square_meters_from'] as $value => $label):?>
												<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_from']) && $form_data['lot_square_meters_from'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
											<?php endforeach;?>
										</select>
									</div>

									<div class="col-lg-6 col-md-12 col-xs-6">
										<select class="form-control" name="lot_square_meters_to">
											<option value="">To Any</option>
											<?php foreach($search_parameters['lot_square_meters_to'] as $value => $label):?>
												<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_to']) && $form_data['lot_square_meters_to'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div><label for="select-field-04"><i class="icon-dollar"></i> Price</label>

								<div class="row select-row">
									<div class="col-lg-6 col-md-12 col-xs-6">
										<select class="form-control" id="select-field-04" name="houseland_price_from">
											<option value="">From Any</option>
											<?php for($min_price = $search_parameters['house_land_price_min']; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
												<option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_from']) && $form_data['houseland_price_from'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
											<?php endfor;?>
										</select>
									</div>

									<div class="col-lg-6 col-md-12 col-xs-6">
										<select class="form-control" name="houseland_price_to">
											<option value="">To Any</option>
											<?php if($search_parameters['house_land_price_max']):?>
												<?php for($min_price = $search_parameters['house_land_price_min']+50000; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
													<option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
												<?php endfor;?>
												<option value="<?= $search_parameters['house_land_price_max'];?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $search_parameters['house_land_price_max'])? 'selected="selected"':'';?>>$<?= number_format(ceil($search_parameters['house_land_price_max']/1000));?>k</option>
											<?php endif;?>
										</select>
									</div>
								</div>

								<div class="row select-row">
									<div class="col-lg-6 col-md-12 col-xs-6">
										<label for="select-05"><i class="icon-bed"></i> Bed</label> 
										<select class="form-control" name="house_bedrooms" id="select-05">
											<option value="">From Any</option>
											<?php foreach($search_parameters['house_bedrooms'] as $item):?>
												<option value="<?= $item['house_bedrooms'];?>" <?= (isset($form_data['house_bedrooms']) && $form_data['house_bedrooms'] == $item['house_bedrooms'])? 'selected="selected"':'';?>><?= $item['house_bedrooms'];?></option>
											<?php endforeach;?>
										</select>
									</div>

									<div class="col-lg-6 col-md-12 col-xs-6">
										<label for="select-06"><i class="icon-bath"></i> BATH</label> 
										<select class="form-control" id="select-06" name="house_bathrooms">
											<option value="">Any</option>
											<?php foreach($search_parameters['house_bathrooms'] as $item):?>
												<option value="<?= $item['house_bathrooms'];?>" <?= (isset($form_data['house_bathrooms']) && $form_data['house_bathrooms'] == $item['house_bathrooms'])? 'selected="selected"':'';?>><?= $item['house_bathrooms'];?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
								<label for="select-07"><i class="icon-builder"></i> Builder</label> 
								<select class="form-control" id="select-07" name="builder_id">
									<option value="">Any</option>
									<?php foreach($search_parameters['builders'] as $item):?>
										<option value="<?= $item['builder_id'];?>" <?= (isset($form_data['builder_id']) && $form_data['builder_id'] == $item['builder_id'])? 'selected="selected"':'';?>><?= $item['builder_name'];?></option>
									<?php endforeach;?>
								</select>
								 <label for="select-08"><i class="icon-levels"></i> Levels</label> 
								<select class="form-control" id="select-08" name="house_levels">
									<option value="">Any</option>
									<?php foreach($search_parameters['house_levels'] as $item):?>
										<option value="<?= $item['house_levels'];?>" <?= (isset($form_data['house_levels']) && $form_data['house_levels'] == $item['house_levels'])? 'selected="selected"':'';?>><?= $item['house_levels'];?></option>
									<?php endforeach;?>
								</select>
								 <label for="select-09"><i class="icon-list"></i> Order Results By</label> 
								<select class="form-control" id="select-09" name="order_by">
									<?php foreach($search_parameters['order_by'] as $item):?>
										<?php if($item == 'builder'):?>
											<option value="<?= $item;?>|asc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|asc')? 'selected="selected"':'';?>><?= ucwords($item);?> Name</option>
										<?php else:?>
											<option value="<?= $item;?>|asc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|asc')? 'selected="selected"':'';?>><?= ucwords($item);?>: Low to High</option>
											<option value="<?= $item;?>|desc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|desc')? 'selected="selected"':'';?>><?= ucwords($item);?>: High to Low</option>
										<?php endif;?>
									<?php endforeach;?>
								</select>
							</div>
						</div><label for="email-field-02"><i class="icon-star"></i> PROJECT Updates</label> <input class="form-control" id="email-field-02" placeholder="Enter email for project updates" type="email">

						<div class="row">
							<div class="col-xs-12">
								<button class="submit btn btn-primary" type="submit">Search Homes</button>
							</div>
						</div>
					</form>

					<form action="living-options-land.html" class="form" id="form-2" name="form-2">
						<div class="row">
							<div class="col-lg-12 col-sm-12 col-xs-12">
								<label for="select-field-01"><i class="icon-size"></i> Land size</label>

								<div class="row select-row">
									<div class="col-lg-6 col-md-12 col-xs-6">
										<select class="form-control" id="select-field-01" name="lot_square_meters_from">
											<option value="">From Any</option>
											<?php foreach($search_parameters['lot_square_meters_from'] as $value => $label):?>
												<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_from']) && $form_data['lot_square_meters_from'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
											<?php endforeach;?>
										</select>
									</div>

									<div class="col-lg-6 col-md-12 col-xs-6">
										<select class="form-control" name="lot_square_meters_to">
											<option value="">To Any</option>
											<?php foreach($search_parameters['lot_square_meters_to'] as $value => $label):?>
												<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_to']) && $form_data['lot_square_meters_to'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div><label for="select-field-02"><i class="icon-size-vertical"></i> FRONTAGE</label>

								<div class="row select-row">
									<div class="col-lg-6 col-md-12 col-xs-6">
										<select class="form-control" id="select-field-02" name="lot_width_from">
											<option value="">From Any</option>
											<?php foreach($search_parameters['lot_widths_from'] as $value => $label):?>
												<option value="<?= $value;?>" <?= (isset($form_data['lot_width_from']) && $form_data['lot_width_from'] == $value)? 'selected="selected"':'';?> ><?= $label;?></option>
											<?php endforeach;?>
										</select>
									</div>

									<div class="col-lg-6 col-md-12 col-xs-6">
										<select class="form-control" name="lot_width_to">
											<option value="">To Any</option>
											<?php foreach($search_parameters['lot_widths_to'] as $value => $label):?>
												<option value="<?= $value;?>"  <?= (isset($form_data['lot_width_to']) && $form_data['lot_width_to'] == $value)? 'selected="selected"':'';?> ><?= $label;?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div><label for="email-field-01"><i class="icon-star"></i> PROJECT Updates</label> <input class="form-control" id="email-field-01" placeholder="Enter email for project updates" type="email">
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12">
								<button class="submit btn btn-primary" type="submit">Search Land</button>
							</div>
						</div>
					</form>
				</section>

				<div class="widget">
					<p class="download-caption"><?= $development->development_name;?> Stage Plan Downloads <i class="icon-arrow-down"></i></p>

					<div class="btns-holder row">
						<?php foreach($stages as $stage):?>
							<div class="col-lg-6 col-md-12 col-xs-6">
								<a class="btn btn-primary" href="http://app.mapovis.com.au/admin/apipdf/devstagepdffile/<?= $development->development_id?>/<?= $stage->stage_number?>" target="_blank">STAGE <?= $stage->stage_code;?> PDF</a>
							</div>
						<?php endforeach;?>
					</div>
				</div>
			</aside>
		</div>

		<div class="col-md-8 col-xs-12">
		<?php if(empty($stages)):?>
			<h5>No land was found with this search criteria.</h5>
		<?php else:?>
			<?php $show = TRUE;?>
			<?php foreach($stages as $stage):?>
			<section class="items-section">
				<?php if($show):?>
				<header class="clearfix">
					<h1 class="pull-left">Land options</h1>
					<a class="link-view-all pull-right lightbox" href="#">View All On Map <i class="icon-arrow-right"></i></a>
				</header>
				<?php $show = FALSE;?>
				<?php endif;?>

				<h3>Stage <?= $stage->word_of_number;?> <span><?= $stage->stage_name;?></span></h3>

				<?php foreach($stage->lots as $lot):?>
				<?php $available = ($lot->status == 'Available');?>
				<div class="item-box">
					<div class="heading-holder">
						<ul class="heading-list">
							<li class="title-col"><a class="masterplan-track lightbox" href="#" id="lot_<?= $lot->lot_number;?>" zoom_to_lot="<?= $lot->lot_number;?>" zoom_level="19" data-placement="top" data-toggle="tooltip" title="View on Masterplan">Lot <?= $lot->lot_number;?></a></li>

							<li><strong class="titles"><a class="masterplan-track lightbox" href="#" id="lot_<?= $lot->lot_number;?>" zoom_to_lot="<?= $lot->lot_number;?>" zoom_level="19" data-placement="top" data-toggle="tooltip" title="View on Masterplan"><i class="icon-size"></i> <?= $lot->lot_square_meters;?> M<sub>2</sub></strong> Size</a></li>

							<li><strong class="titles"><a class="masterplan-track lightbox" href="#" id="lot_<?= $lot->lot_number;?>" zoom_to_lot="<?= $lot->lot_number;?>" zoom_evel="19" data-placement="top" data-toggle="tooltip" title="View on Masterplan"><i class="icon-size-vertical"></i> <?= round($lot->lot_width, 5);?> M</strong> Frontage</a></li>

							<li class="links-col">
								<a class="icon-pin-circle masterplan-track lightbox" href="#" id="<?= $lot->lot_number;?>" zoom_to_lot="<?= $lot->lot_number;?>" zoom_level="19" data-placement="top" data-toggle="tooltip" title="View on Masterplan"></a>
							</li>

							<li class="btn-col">
								<?php if($available):?>
									<a class="btn btn-primary opener" href="#">Enquire</a>
								<?php else:?>
									Sold
								<?php endif;?>
							</li>
						</ul>
					</div>

					<div class="slide">
						<div class="slide-frame">
				<form class="validation register-form" action="http://rpm.runway.com.au/actions/form/globalformaction.jsp" method="post">
					<input type="hidden" name="GroupID" value="021P4W094E1J0V0G940X8G8B1K31">
								<input type="hidden" name="NewContactStatusID" value="2f3c4880-dcd5-102c-8f29-644220d7d8b3">
								<input type="hidden" name="Source" value="Tulliallan Webform">
								<input type="hidden" name="NotificationTemplateID" value="0B1T3Q1C16743R0Z09472F1X430U">
								<input type="hidden" name="TemplateID" value="0S104V2X180F2D587F2Q2Z7E5V04">
								<input type="hidden" name="LocationID" value="0V1T480G4Q0O9I6T6B3C398L2L53">
								<input type="hidden" name="-alwaysnotify" value="true" />
								<input type="hidden" name="-redirect" value="http://tulliallan.com.au/thankyou.html"><!-- SET the value attribute dynamically to the URL of the page you want to redirect on submission of form     -->
								<input type="hidden" name="QuestionID" value="0W194Q1W3J8T4V7J895M9Z1W3073" />
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="input-field-holder">
								<input id="name-01-<?= $counter;?>" type="text" class="form-control required" placeholder="First Name" rel="First Name*" name="FirstName">
								<label class="error-message" for="name-01-<?= $counter;?>">Error - Enter a First Name</label>
							</div>
							<div class="input-field-holder">
								<input id="name-02-<?= $counter;?>" type="text" class="form-control required" placeholder="Last Name" rel="Last Name*" name="LastName">
								<label class="error-message" for="name-02-<?= $counter;?>">Error - Enter a Last Name</label>
							</div>
							<div class="input-field-holder">
								<input id="email-01-<?= $counter;?>" type="email" class="form-control required-email" placeholder="Email" name="Email">
								<label class="error-message" for="email-01-<?= $counter;?>">Error - Enter a valid email</label>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="input-field-holder">
								<input id="phone-01-<?= $counter;?>" type="tel" class="form-control required-number" placeholder="Mobile" name="Mobile">
								<label class="error-message" for="phone-01-<?= $counter;?>">Error - Enter a valid number</label>
							</div>
							<input class="form-control" type="text" name="Suburb" placeholder="Suburb">
							<div class="input-field-holder">
									<select id="select-01-<?= $counter;?>" name="Answer0W194Q1W3J8T4V7J895M9Z1W3073" class="form-control select required-select source-enquiry default-value">
										<option class="hideme" value="">Source of enquiry</option>
										<option value="Online">Online</option>
										<option value="Builder">Builder</option>
										<option value="Signage">Signage</option>
										<option value="Newspaper">Newspaper</option>
										<option value="Word of Mouth">Word of Mouth</option>
										<option value="Other">Other</option>
									</select>
									<label class="error-message" for="select-01-<?= $counter;?>">Error - Select a source of enquiry</label>
								</div>
							</div>
						<div class="col-xs-12 ">
							<input type="text" name="Answer0Q1O4Z143Y9C4H1V33108M3Y9B5I" class="form-control other" placeholder="Please Specify" rel="Please Specify" value="">
							<input type="hidden" name="QuestionID" value="0Q1O4Z143Y9C4H1V33108M3Y9B5I" />
							<input type="hidden" name="Comments" value="" /><!--  Comment -->
							<input type="hidden" name="QuestionID" value="0Q1O4Z143Y9C4H1V33108M3Y9B5I" />

							<!--lot number-->
							<input type="hidden" name="Answer0M134R1F8C9C4S0P4R87865P067D" value="<?= $lot->lot_number;?>">
							<!--House name-->
							<input type="hidden" name="Answer0B1N4C1P81954W0P6Q0Y067S158N" value="">
							<!--builder-->
							<input type="hidden" name="Answer081P42188F934W0O672G4Q5B0183" value="">

							<button data-loading-text="Sending..." type="submit" data-ga-action="submit" data-ga-category="Mapovis Contact" data-ga-title="form submit" class="btn btn-primary submit">SUBMIT ENQUIRY</button>
							<div class="loader"></div>
						</div>
					</div>
				</form>
						</div>
					</div>
				</div>
				<?php $counter++;?>
				<?php endforeach;?>
			</section>
			<?php endforeach;?>
		<?php endif;?>
		</div>
	</div>