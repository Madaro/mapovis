 <div class="vc_row wpb_row section vc_row-fluid grid_section" style=' padding-top:50px; padding-bottom:100px; text-align:left;'>
                                <div class=" section_inner clearfix">
                                    <div class='section_inner_margin clearfix'>
                                        <div class="vc_col-sm-12 wpb_column vc_column_container">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <!--start html here -->

                                                        <div class="search-section clearfix">
                                                            <form href="#land" action="" id="myform" method="get" name="myform">
                                                                <div class="search-section-left clearfix">
                                                                    <div class="search-select land-size-from">
                                                                        <label>Land Size</label> 
																		<select class="selectbox" name="lot_square_meters_from" tabindex="1">
																			<option value="">From</option>
																			<?php foreach($search_parameters['lot_square_meters_from'] as $value => $label):?>
																				<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_from']) && $form_data['lot_square_meters_from'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
																			<?php endforeach;?>
																		</select>
                                                                    </div>

                                                                    <div class="search-select land-size-to">
                                                                        <label>&nbsp;</label> <select class="selectbox" name="lot_square_meters_to" tabindex="2">
																			<option value="">To</option>
																			<?php foreach($search_parameters['lot_square_meters_to'] as $value => $label):?>
																				<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_to']) && $form_data['lot_square_meters_to'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
																			<?php endforeach;?>
                                                                        </select>
                                                                    </div>

                                                                    <div class="search-select frontage">
                                                                        <label>Frontage</label> <select class="selectbox" name="lot_widths_from" tabindex="3">
																			<option value="">From</option>
																			<?php foreach($search_parameters['lot_widths_from'] as $value => $label):?>
																				<option value="<?= $value;?>" <?= (isset($form_data['lot_width_from']) && $form_data['lot_width_from'] == $value)? 'selected="selected"':'';?> ><?= $label;?></option>
																			<?php endforeach;?>
                                                                        </select>
                                                                    </div>

                                                                    <div class="search-select frontage">
                                                                        <label>&nbsp;</label> <select class="selectbox" name="lot_width_to" tabindex="4">
																			<option value="">To</option>
																			<?php foreach($search_parameters['lot_widths_to'] as $value => $label):?>
																				<option value="<?= $value;?>"  <?= (isset($form_data['lot_width_to']) && $form_data['lot_width_to'] == $value)? 'selected="selected"':'';?> ><?= $label;?></option>
																			<?php endforeach;?>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="search-section-right">
                                                                    <input class="reset" tabindex="8" title="Reset" type="reset" value="Reset"> <input class="search-result" tabindex="9" title="Search" type="submit" value="Search">
                                                                </div>
                                                            </form>
                                                        </div>

                                                        <div class="our-plan clearfix">
                                                            <div class="download-stage">
                                                                <label>Download Stage Release:</label> <select class="selectbox" name="download-stage-release" tabindex="10">
                                                                    <option value="0">
                                                                        Choose Release Plan
                                                                    </option>
																	<?php foreach($stages as $stage):?>
																		<option value="http://app.mapovis.com.au/admin/apipdf/devstagepdffile/<?= $development->development_id;?>/<?= $stage->stage_number;?>">
																			<?= $stage->stage_name;?>
																		</option>
																	<?php endforeach;?>
                                                                </select>
                                                            </div>

                                                            <div class="view-plan">
                                                                <a class="view-master-plan lightbox" href="#" title="View Interactive Masterplan">View Interactive Masterplan</a> 

                                                                <a class="lightbox promotional-banner" title="Promotional Banner" href="http://app.mapovis.com.au/mapovis/development.php?developmentId=81&amp;zoom_to_lot=618&amp;zoom_level=19&amp;lightbox[iframe]=true&amp;lightbox[modal]=false&amp;lightbox[move]=false&amp;lightbox[width]=90p&amp;lightbox[height]=882" id="618" zoom_to_lot="618" zoom_level="19">
                                                                    <img class="promotional-banner" src="http://myjubilee.wpengine.com/wp-content/themes/myjubilee/images/land-for-sale-promo.gif">
                                                                </a>
                                                            </div>
                                                        </div>

													<?php if(empty($stages)):?>
														<h5>No land was found with this search criteria.</h5>
													<?php else:?>
														<?php foreach($stages as $stage):?>
                                                        <div class="search-listing-for-sale">
                                                            <h2>Release <?= ucwords($stage->stage_code);?></h2>

                                                            <table class="table-modify responsive-table responsive">
                                                                <tbody>
																	<?php foreach($stage->lots as $lot):?>
																	<?php $available = ($lot->status == 'Available');?>
                                                                    <tr>
																		<td <?= ($available)? 'class="even-td"':'';?>>Lot <?= $lot->lot_number;?></td>

                                                                        <td class="odd-td"><?= $lot->lot_square_meters;?>m<sub>2</sub> Land Size</td>

                                                                        <td><?= round($lot->lot_width, 5);?>m</td>

                                                                        <td class="odd-td">Frontage</td>

																		<?php if($available):?>
																			<td>
																				<a class="enquire-now" href="javascript:void(0)">AVAILABLE</a>
																			</td>
																			<td>
																				<a class="tooltips masterplan-track lightbox" href="#" id="<?= $lot->lot_number;?>" zoom_to_lot="<?= $lot->lot_number;?>" zoom_level="19"><i class="icon-view-inner"></i><span>View on masterplan</span></a>
																			</td>
																			<td class="left-border">
																				<a class="email icon-email" href="mailto:info@myjubilee.com.au" style="font-style: italic"></a>
																			</td>
																		<?php else:?>
																			<td>SOLD</td>
																			<td>&nbsp;</td>
																			<td class="left-border">&nbsp;</td>
																		<?php endif;?>
                                                                    </tr>
																	<?php endforeach;?>
                                                                </tbody>
                                                            </table>
                                                        </div>

														<div style="margin-bottom:40px;"></div>
														<?php endforeach;?>
													<?php endif;?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>