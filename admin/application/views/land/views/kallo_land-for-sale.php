<?php
$based_url    = base_url();
$folder_url   = $based_url.'application/views/land/';
$manual_price = ($development->show_pricing == 0);
$land_price   = ($manual_price)? (float)$development->land_starting_from: FALSE;
?>
	<!--SEARCH BOX -->
	<div class="col-lg-3 col-md-3">
		<a name="search"></a>
		<form action="#" id="search_form" class="search-form" name="refreshForm" method="post">

			<fieldset>
				<h3>Refine Your Search</h3>
				<input type="hidden" name="visited" value="" />
				<fieldset class="block">
					<legend class="icon">
						<span class="glyphicon glyphicon-resize-horizontal"></span>
						LOT WIDTH m
					</legend>
					<div class="col">
						<ul class="radio-holder">
							<?php foreach($search_parameters['lot_widths'] as $item_value => $item_label):?>
								<li>
									<input name="lot_width[]" type="checkbox" id="width_<?= $item_value;?>" value="<?= $item_value;?>" <?= (isset($form_data['lot_width']) && in_array($item_value, $form_data['lot_width']))? 'checked="checked"':'';?>>
									<label for="width_<?= $item_value;?>"><?= $item_label;?></label>
								</li>
							<?php endforeach;?>
						</ul>
					</div>
				</fieldset>
				<fieldset class="block">
					<legend class="icon size">TOTAL SIZE m<sup>2</sup></legend>
					<div class="col">
						<ul class="radio-holder">
							<?php foreach ($search_parameters['lot_square_meters'] as $item_value => $item_label):?>
								<li>
									<input name="lot_square_meters[]" type="checkbox" id="size_<?= $item_value;?>" value="<?= $item_value;?>" <?= (isset($form_data['lot_square_meters']) && in_array($item_value, $form_data['lot_square_meters']))? 'checked="checked"':'';?>>
									<label for="size_<?= $item_value;?>"><?= $item_label;?></label>
								</li>
							<?php endforeach;?>
						</ul>
					</div>
				</fieldset>
				<div class="btn-holder">
					<input type="submit" value="Search" class="btn btn-primary">
				</div>
			</fieldset>
		</form>
	</div>
	<!--END OF SEARCH BOX-->
	<!--Start Result List-->
	<div class="col-lg-9 col-md-9">
	<div class="list-content-holder">
	<!--HEADER of the list for stage x-->
	<?php if(empty($stages)):?>
		<h5>No land was found with this search criteria.</h5>
	<?php else:?>
	<?php foreach($stages as $stage):?>
	<div class="heading">
		<?php if(($manual_price && $land_price) || (!$manual_price && $stage->min_price)):?>
			<!-- <h3>LAND now SELLING FROM $<?= number_format(($land_price? $land_price: $stage->min_price));?></h3> -->
		<?php endif;?>
		<div class="holder">
			<h4>Stage <?= ucwords($stage->stage_code);?> <span>- <?= $stage->stage_name;?></span></h4>
		</div>
	</div>
	<!--END OF HEADER-->
	<ul class="list-content">

	<!--START RESULT 1 - AVAILABLE -->
	<?php foreach($stage->lots as $lot):?>
	<li <?= ($lot->status == 'Sold')? 'class="sold"':''; ?>>
		<ul class="data-holder">
			<li class="col01">
				<span>Lot <?= $lot->lot_number;?></span>
			</li>
			<li class="col02">
				<b class="icon size"><?= $lot->lot_square_meters;?>m<sup>2</sup></b> Land Size
			</li>
			<li class="col03">
				<b class="icon"><span class="glyphicon glyphicon-resize-horizontal"></span> <?= round($lot->lot_width, 5);?>m</b> Frontage
			</li>
			<li class="col04">
				<b class="available"><span class="glyphicon glyphicon-ok"></span> AVAILABLE</b>
				<b class="sold"><span class="glyphicon glyphicon-remove"></span> sold</b>
			</li>
			<li class="col05">
				<span class="icon enquire" title="Enquire">Enquire</span>
			</li>
		</ul>
		<div class="data-content">
			<div class="holder">
				<div class="col">
					<form class="enquire-form validate-form wpcf7-form" method="post" action="http://rpm.runway.com.au/actions/form/globalformaction.jsp">
						<fieldset>
							<b class="title">Enquire About This Land</b>
							<div class="holder">
								<div class="block">
									<span class="wpcf7-form-control-wrap ">
										<input type="text" name="Name" class="wpcf7-text wpcf7-validates-as-required required name" placeholder="Name *">
									</span>
								</div>
								<div class="block">
									<span class="wpcf7-form-control-wrap">
										<input type="text" name="Phone" class="wpcf7-text wpcf7-validates-as-required required" placeholder="Phone *">
									</span>
								</div>
								<div class="block">
									<span class="wpcf7-form-control-wrap">
										<input type="email" name="Email" class="wpcf7-text wpcf7-validates-as-email wpcf7-validates-as-required required-email" placeholder="Email *">
									</span>
								</div>
								<div class="block">
									<span class="wpcf7-form-control-wrap">
										<input type="text" name="Suburb" class="wpcf7-text" placeholder="Suburb">
									</span>
								</div>
								<div class="block">
									<span class="wpcf7-form-control-wrap">
										<select class="wpcf7-select jcf-hidden source-enquiry" name="Answer0W1S4O0O0R2J0T667S8P436W5H7B">
											<option value="">Source of Enquiry</option>
											<option value="Online">Online</option>
											<option value="Signage">Signage</option>
											<option value="Newspaper">Newspaper</option>
											<option value="Word of Mouth">Word of Mouth</option>
											<option value="Other">Other</option>
										</select>
									</span>
								</div>
								<div class="block other">
									<span class="wpcf7-form-control-wrap">
										<input type="text" name="Answer0G1C430Q06250H6X8E6Z2X8E864L" class="wpcf7-text" placeholder="Please Specify">
									</span>
								</div>
								<div class="block">
                                                    <span class="wpcf7-form-control-wrap">
                                                        <select class="wpcf7-select jcf-hidden call-back" name="Answer011U4M047W1T16549K617B8O6V48">
                                                          <!--<option value=""></option>-->
                                                          <option value="">Best Call Back Time</option>
                                                          <option value="Before 9am">Before 9am</option>
                                                          <option value="9am to 12pm">9am to 12pm</option>
                                                          <option value="12pm to 3pm">12pm to 3pm</option>
                                                          <option value="3pm to 5pm">3pm to 5pm</option>
                                                          <option value="After 5pm">After 5pm</option>
                                                        </select>
                                                    </span>
                                </div>								
								<div class="block">
									<div class="btn-submit">
										<input type="submit" class="wpcf7-submit gaTracking" value="ENQUIRE"  data-ga-action="submit" data-ga-category="Enquiry" data-ga-title="Register">
									</div>
								</div>
							</div>
						</fieldset>
						<input type="hidden" name="GroupID" value="0R163N918R6Z6K9C0D2K4N1S3111">
						<input type="hidden" name="NewContactStatusID" value="2f3c4880-dcd5-102c-8f29-644220d7d8b3">                                                                                                                        <input type="hidden" name="LocationID" value="0915309M8K6Z6K9R238Z431Y341V">
						<input type="hidden" name="Source" value="Kallo website">
						<input type="hidden" name="NotificationTemplateID" value="0B1T3Q1C16743R0Z09472F1X430U">
						<input type="hidden" name="TemplateID" value="0N1F3T9I8L9Q1W944R9L493K5E9H" >
						<input type="hidden" name="-redirect"  value="">   
						<input type="hidden" name="-alwaysnotify" value="true" />
						<!-- SET the value attribute dynamically to the URL of the page you want to redirect on submission of form     -->
						<input type="hidden" name="FirstName"  value="" class="FirstName">
						<input type="hidden" name="LastName"  value="" class="LastName">
						<input type="hidden" name="QuestionID" value="011U4M047W1T16549K617B8O6V48" />
                        <input type="hidden" name="Quantifier" value="" />
						<!--lot number-->
						<input type="hidden" name="Answer0L1H400A2C5O3M4N0G7X9J2K3901" value="<?= $lot->lot_number;?>">
					</form>
				</div>
				<div class="col">
					<img src="http://www.kallo.com.au/wp-content/themes/kallo/images/enquiry-banner.png" class="img-responsive"/>
				</div>
			</div>
		</div>
	</li>
	<!--END RESULT 1-->
	<?php endforeach;?>
	</ul>
	<?php endforeach;?>
	<?php endif;?>
	<!--END Result List for stage x-->
	</div>
	</div>




<!-- Terms and Conditions Modal
================================================== -->
<div class="modal fade terms-and-conditions-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myLargeModalLabel">Terms & Conditions</h4>
			</div>
			<div class="modal-body">
				<?= $development->terms_and_conditions;?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>