<?php
$based_url  = base_url();
$folder_url = $based_url.'application/views/land/';
?>
	<div class="container">
	<?php foreach($stages as $stage):?>
	<!-- LAND - Stage -->
	<br/>
	<div class="row">
		<div class="col-md-2">
			<div class="stage-number-title">STAGE <?= strtoupper($stage->word_of_number);?></div>
		</div>

		<div class="col-md-2">
			<div class="stage-name"></div>
		</div>

		<div class="col-md-3" style="margin-top:5px;">
			<div class="label label-info land-starting-from">Land Starting from $<?= number_format($stage->min_price);?></div>
		</div>

		<div class="col-md-5 col-xs-7">
			<div class="lots-sold-progress-bar">
				<div class="progress">
					<div class="progress-bar percentage-sold" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: <?= $stage->sold_percentage;?>%;">
						<?= $stage->sold_percentage;?>% Sold
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12">

			<!-- Disclaimer -->
			<div class="alert alert-warning">
				<span>
					<strong>Indicitive Prices Only</strong>- please <a href="#" data-toggle="modal" data-target=".enquiry-form-modal-lg">enquire</a> for further information
				</span>

				<span class="indicitive-prices-only-terms-and-conditions">
					<span class="legal-star" data-toggle="modal" data-target=".terms-and-conditions-modal-lg">
						<a href="#">
							<span class="glyphicon glyphicon-asterisk"></span>
							<span>Terms & Conditions</span>
						</a>
					</span>
				</span>
			</div>

			<?php 
			$street_id  = -1;
			$open_table = false;
			foreach($stage->lots as $lot):?>
				<?php
					if($lot->street_name_id != $street_id){
						if($open_table){
							?>
								<!-- closing table -->
								</table>
							<!-- closing div table -->
							</div>
							</div>
							<?php
						}
						$street_id  = $lot->street_name_id;
						$open_table = true;
						?><!-- Street -->
						<div class="panel-heading">
							<h3 class="panel-title"><?= ($lot->street_name)? $lot->street_name: 'No street provided';?></h3>
						</div>
						<div class="panel panel-default">
						<div class="table-responsive">
							<table class="table table-bordered">
								<tr>
									<td class="table-titles">
										<span class="glyphicon glyphicon-th-large"></span>
										<b>Lot</b>
									</td>
									<td class="table-titles">
										<span class="glyphicon glyphicon-fullscreen"></span>
										<b>Land Size</b>
									</td>
									<td class="table-titles">
										<span class="glyphicon glyphicon-resize-horizontal"></span>
										<b>Frontage</b>
									</td>
									<td class="table-titles">
										<span class="glyphicon glyphicon-usd"></span>
										<b>Price Range</b>
									</td>
									<td class="table-titles">
										<span class="glyphicon glyphicon-info-sign"></span>
										<b>Status</b>
									</td>
								</tr>
						<?php
					}
				?>
				<tr>
					<td class="">Lot <?= $lot->lot_number;?></td>
					<td class="">
						<?= $lot->lot_square_meters;?> m<sup>2</sup>
					</td>
					<td class="">
						<?= round($lot->lot_width, 5);?><sup>m</sup>
					</td>
					<td class="">
						$<?= number_format($lot->price_range_min);?> - $<?= number_format($lot->price_range_max);?>
					</td>
					<td class="success available">
						<?php
						if($lot->status == 'Available'){
							$icon   = 'ok';
							$button = 'success';
						}
						else{
							$icon   = 'remove';
							$button = 'danger disabled';
						}
						?>
						<a href="#" data-toggle="modal" data-target=".enquiry-form-modal-lg">
							<button type="button" class="btn btn-<?= $button;?>">
								<span class="glyphicon glyphicon-<?= $icon;?>"></span> <?= strtoupper($lot->status);?>
							</button>
						</a>
					</td>
				</tr>
			<?php endforeach;
			if($open_table){
				?>
					</table>
				</div>
				<?php
			}
			?>
			</div>
		</div>
	</div>
	<?php endforeach;?>
		<!-- SEARCH -->
		<br/>
		<div class="panel panel-default">
			<div class="panel-heading">Refine Your Search</div>
			<div class="panel-body">
				<form name="lots-search" action="#" method="post">
					<div class="lot-search-form">

						<div class="input-group enquiry-form-input">
							<span class="lot-frontage-title">
								<span class="glyphicon glyphicon-fullscreen"></span>Lot Frontage:</span>
							<ul class="radio-holder">
								<?php foreach($search_parameters['lot_widths'] as $item_value => $item_label):?>
									<li>
										<input name="lot_width[]" type="checkbox" id="width_<?= $item_value;?>" value="<?= $item_value;?>" <?= (isset($form_data['lot_width']) && in_array($item_value, $form_data['lot_width']))? 'checked="checked"':'';?>>
										<label for="width_<?= $item_value;?>"><?= $item_label;?></label>
									</li>
								<?php endforeach;?>
							</ul>

							<span class="lot-size-title">
								<span class="glyphicon glyphicon-resize-horizontal"></span>Lot Size:</span>
							<ul class="radio-holder">
								<?php foreach ($search_parameters['lot_square_meters'] as $item_value => $item_label):?>
									<li>
										<input name="lot_square_meters[]" type="checkbox" id="size_<?= $item_value;?>" value="<?= $item_value;?>" <?= (isset($form_data['lot_square_meters']) && in_array($item_value, $form_data['lot_square_meters']))? 'checked="checked"':'';?>>
										<label for="size_<?= $item_value;?>"><?= $item_label;?></label>
									</li>
								<?php endforeach;?>
							</ul>
						</div>
						<span>
							<button type="submit" class="btn btn-success">Refine Search</button>
						</span>

					</div>
				</form>
			</div>
		</div>
		<!-- END SEARCH -->

	</div>

	<!-- Enquiry Modal
	================================================== -->

	<div class="modal fade enquiry-form-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myLargeModalLabel">Enquiry</h4>
				</div>

				<div class="modal-body">
					<!-- START: General Enquiry Form -->
					<div class="row" style="border-bottom: none;">
						<div class="col-md-7">
							<iframe src="<?= $development->form_iframe_url;?>" width="100%" height="<?= $development->form_iframe_url_height;?>" frameborder="0"></iframe>
						</div>

						<div class="col-md-5">
							<!--  NOTE: In the future this picture and corresponding link WILL come from the database. For now it is just a placeholder.  -->
							<a href="http://burbank.com.au/docs/homes/Albany/generic/brochures/Albany.pdf" target="_blank">
								<img data-src="holder.js/400x280" class="img-thumbnail" alt="Brochure Link">
							</a>
						</div>
					</div>
					<!-- END: Enquiry Form -->
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>

	<!-- Terms and Conditions Modal
	================================================== -->
	<div class="modal fade terms-and-conditions-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myLargeModalLabel">Terms & Conditions</h4>
				</div>
				<div class="modal-body">
					<?= $development->terms_and_conditions;?>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>