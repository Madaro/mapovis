<a name="land"></a>
<div class="search-cont clearfix">
                	<div class="search-sidebar">
                		<h3>Search land</h3>
                        <form action="#land" class="side-form land-form">
                        	<label class="icon icon-size" >Frontage</label>
                            <fieldset>
                            	<select name="lot_width_from">
									<option value="">From Any</option>
									<?php foreach($search_parameters['lot_widths_from'] as $value => $label):?>
										<option value="<?= $value;?>" <?= (isset($form_data['lot_width_from']) && $form_data['lot_width_from'] == $value)? 'selected="selected"':'';?> ><?= $label;?></option>
									<?php endforeach;?>
                                </select>
                                <select name="lot_width_to">
									<option value="">To Any</option>
									<?php foreach($search_parameters['lot_widths_to'] as $value => $label):?>
										<option value="<?= $value;?>"  <?= (isset($form_data['lot_width_to']) && $form_data['lot_width_to'] == $value)? 'selected="selected"':'';?> ><?= $label;?></option>
									<?php endforeach;?>
                                </select>
                            </fieldset>
                            <label class="icon icon-size2" >Size</label>
                            <fieldset>
                            	<select name="lot_square_meters_from">
									<option value="">From Any</option>
									<?php foreach($search_parameters['lot_square_meters_from'] as $value => $label):?>
										<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_from']) && $form_data['lot_square_meters_from'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
									<?php endforeach;?>
                                </select>
                                <select name="lot_square_meters_to">
									<option value="">To Any</option>
									<?php foreach($search_parameters['lot_square_meters_to'] as $value => $label):?>
										<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_to']) && $form_data['lot_square_meters_to'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
									<?php endforeach;?>
                                </select>
                            </fieldset>
                            <input type="submit" value="Search land" class="button">
                        </form>
                        
                        <a href="#" class="masterplan-btn lightbox">View Interactive<br />Masterplan</a>
                        
                	</div><!--end of search-sidebar-->
                    <div class="search-table">
                		<h3>Your dream <br />build starts here</h3>
                        
						<?php if(empty($stages)):?>
							<h6>No land was found with this search criteria.</h6>
						<?php else:?>
							<?php foreach($stages as $stage):?>
							<h6><?= $stage->stage_name;?></h6>
							<table width="100%" border="0" class="table">
							<?php foreach($stage->lots as $lot):?>
							  <tbody>
								<tr>
								  <td>LOT <?= $lot->lot_number;?></td>
								  <td class="landsize"><span><?= $lot->lot_square_meters;?> <small>M2</small> <span>Size</span></span></td>
								  <td class="frontage"><span><?= round($lot->lot_width, 5);?> <small>M</small> <span>Frontage</span></span></td>
								  <td class="land">
								  	<a class="viewonmasterplan lightbox" href="#" id="<?= $lot->lot_number;?>" zoom_to_lot="<?= $lot->lot_number;?>" zoom_level="19">
								  		<i class="icon-open-masterplan"></i>
								  	</a>
								  </td>
								  
								  <?php if($lot->status == 'Available'):?>
									  <td class="status mail"><a href="#" onclick="return false"></a></td>
								</tr>
								<tr>
									<td colspan="5" style="padding: 0;border-top: 0px;">
										<div class="land_forms">
											 <form method="post" action="http://www.tfaforms.com/responses/processor" class="hintsBelow labelsAbove" id="tfa_0">
												 <div class="half first">
													 <div class="form-row clearfix require" id="tfa_1-D">

													<label id="tfa_1-L" for="tfa_1" class="label preField reqMark">Salutation</label>
						                            <select id="tfa_1" name="tfa_1" class="required" >
						                            <option value="">Salutation</option>
						                              <option value="tfa_9" id="tfa_9">Mr</option>
						                              <option value="tfa_10" id="tfa_10">Mrs</option>
						                              <option value="tfa_11" id="tfa_11">Ms</option>
						                              <option value="tfa_12" id="tfa_12">Miss</option>
                             						</select>

													</div>
													<div class="form-row two-fields clearfix">
														<div id="tfa_2-D" class="oneField    ">
															<label id="tfa_2-L" for="tfa_<?= $lot->lot_id;?>_2" class="label preField reqMark">First Name</label>
															<input name="tfa_2" type="text" class="ff-first-name required" placeholder="First name*" id="tfa_<?= $lot->lot_id;?>_2" >
														</div>
														<div id="tfa_3-D" class="oneField right">
															<label id="tfa_3-L" for="tfa_<?= $lot->lot_id;?>_3" class="label preField reqMark">Surname</label>
															<input name="tfa_3" type="text" class="ff-last-name last required" placeholder="Surname*" id="tfa_<?= $lot->lot_id;?>_3">
														</div>
													</div>
													<div class="form-row clearfix" id="tfa_13-D">
													<label id="tfa_13-L" for="tfa_<?= $lot->lot_id;?>_13" class="label preField reqMark">How did you hear about us?</label>
														<select id="tfa_<?= $lot->lot_id;?>_13" name="tfa_13" >
														  <option value="">How did you hear about us?</option>
															  <option value="tfa_14" id="tfa_14" class="">Google</option>
															  <option value="tfa_15" id="tfa_15" class="">Realestate.com.au</option>
															  <option value="tfa_17" id="tfa_17" class="">Signage</option>
															  <option value="tfa_18" id="tfa_18" class="">Newspaper</option>
															  <option value="tfa_19" id="tfa_19" class="">Oliver Hume Website</option>
								                              <option value="tfa_20" id="tfa_20" class="">Other</option>
														</select>
													</div>
												 </div>
												 <div class="half">
													<div class="form-row two-fields clearfix">
														<div id="tfa_4-D" class="oneField   right ">
															<label id="tfa_4-L" for="tfa_<?= $lot->lot_id;?>_4" class="label preField reqMark">Email</label>
															<input name="tfa_4" type="email" class="ff-email last required" placeholder="Email*" id="tfa_<?= $lot->lot_id;?>_4">
														</div>
														<div id="tfa_5-D" class="oneField    ">
															<label id="tfa_5-L" for="tfa_<?= $lot->lot_id;?>_5" class="label preField reqMark">Phone </label>
															<input name="tfa_5" type="text" class="ff-telephone required" placeholder="Phone*" id="tfa_<?= $lot->lot_id;?>_5">
														</div>

														<div id="tfa_10-D" class="oneField   right ">
															<label id="tfa_10-L" for="tfa_<?= $lot->lot_id;?>_10" class="label preField reqMark">Postcode</label>
															<input name="tfa_10" type="text" class="ff-postcode last required" placeholder="Postcode" id="tfa_<?= $lot->lot_id;?>_10">
														</div>
													</div>
												 </div>
												<div class="actions" id="tfa_0-A"><input type="submit" value="Submit" class="primaryAction"> </div>
												<p class="required">*Required field</p>
												<input type="hidden" value="354280" name="tfa_dbFormId" id="tfa_dbFormId"><input type="hidden" value="" name="tfa_dbResponseId" id="tfa_dbResponseId"><input type="hidden" value="d56bf86eca484f130ed9ceba6e36445f" name="tfa_dbControl" id="tfa_dbControl"><input type="hidden" value="2" name="tfa_dbVersionId" id="tfa_dbVersionId"><input type="hidden" value="" name="tfa_switchedoff" id="tfa_switchedoff">                           
											</form>
										 </div>
									</td>
								  <?php else:?>
									<td class="status"><?= $lot->status;?></td>
								  <?php endif;?>
								</tr>
							  </tbody>
							<?php endforeach;?>
							</table>
							<?php endforeach;?>
						<?php endif;?>
                	</div><!--end of search-table-->
                </div>