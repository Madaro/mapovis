	<section class="tab-area">
		<section class="grid-container">
			<div class="resp-tabs-container">


			<div class="land-sale-content-wrap">
					<div class="cols">
						<div class="col-3">
							<form id="land-filter">
								<fieldset>
									<legend>Refine Your Search</legend>
									<p>LOT WIDTH (m)</p>
									<ul>
										<?php foreach($search_parameters['lot_widths'] as $item_value => $item_label):?>
											<li>
												<input name="lot_width[]" type="checkbox" id="width_<?= $item_value;?>" value="<?= $item_value;?>" <?= (isset($form_data['lot_width']) && in_array($item_value, $form_data['lot_width']))? 'checked="checked"':'';?>> 
												<label for="width_<?= $item_value;?>"><?= $item_label;?></label>
											</li>
										<?php endforeach;?>
									</ul>

									<p>TOTAL SIZE (m)</p>
									<ul>
										<?php foreach ($search_parameters['lot_square_meters'] as $item_value => $item_label):?>
											<li>
												<input name="lot_square_meters[]" type="checkbox" id="size_<?= $item_value;?>" value="<?= $item_value;?>" <?= (isset($form_data['lot_square_meters']) && in_array($item_value, $form_data['lot_square_meters']))? 'checked="checked"':'';?>> 
												<label for="size_<?= $item_value;?>"><?= $item_label;?></label>
											</li>
										<?php endforeach;?>
									</ul>

									<button type="submit">Search</button>
								</fieldset>
							</form>
						</div>
						<div class="col-9">

						<?php if(empty($stages)):?>
							<h5>No land was found with this search criteria.</h5>
						<?php else:?>
							<?php foreach($stages as $stage):?>
							<div class="land-listings">
								<a href="http://app.mapovis.com.au/admin/apipdf/devstagepdffile/<?= $development->development_id?>/<?= $stage->stage_number?>" class="button download-button" target="_blank">Download PDF</a>
								<h3>The <?= $stage->stage_name;?></h3>
	
								<ul>
									<?php foreach($stage->lots as $lot):?>
									<li class="status-<?= ($lot->status == 'Sold')? 'sold':'available'; ?>">
										<span class="lot-number">Lot <b><?= $lot->lot_number;?></b></span>
										<span class="land-size"><b><?= $lot->lot_square_meters;?>m<sup>2</sup></b> Land Size</span>
										<span class="frontage"><b><?= round($lot->lot_width, 5);?>m</b> Frontage</span>
										<?php if($lot->status == 'Sold'):?>
											<span class="status"><b>Sold</b></span>
										<?php else:?>
											<span class="status"><a class="button">Available <i></i></a></span>
											<form action="http://brownpropertygroup.createsend.com/t/d/s/tdjidk/" method="post">
												<h3>Enquire about this land</h3>
												<div class="cols">
													<div class="col-6">
														<p>
															<label for="fieldName">Name</label>
															<input id="fieldName" name="cm-name" type="text" placeholder="Name" required />
														</p>
														<p>
															<label for="fieldyuitil">Phone</label>
															<input id="fieldyuitil" name="cm-f-yuitil" type="text" placeholder="Phone" equired />
														</p>
														<p>
															<label for="fieldEmail">Email</label>
															<input id="fieldEmail" name="cm-tdjidk-tdjidk" type="email" placeholder="Email" required />
														</p>
													</div>
													<div class="col-6">
														<p>
															<label for="fieldyuitir">Suburb</label>
															<input id="fieldyuitir" name="cm-f-yuitir" type="text" placeholder="Suburb" />
														</p>
														<p>
															<label for="fieldyuitiy">Source of Enquiry</label>
															<select id="fieldyuitiy" name="cm-fo-yuitiy">
																<option value="">Source of Enquiry</option>
																<option value="online">Online</option>
																<option value="signage">Signage</option>
																<option value="newspaper">Newspaper</option>
																<option value="word-of-mouth">Word of Mouth</option>
																<option value="other">Other</option>
															</select>
														</p>
														<p>
															<label for="fieldyuitij">Best Call Back Time</label>
															<select id="fieldyuitij" name="cm-fo-yuitij">
																<option value="">Best Call Back Time</option>
																<option value="293798">9am-11am</option>
																<option value="293799">11am-1pm</option>
																<option value="1pm-3pm">1pm-3pm</option>
																<option value="3pm-5pm">3pm-5pm</option>
															</select>
														</p>
													</div>
												</div>
											    <p class="group">
											    	<input id="fieldyuitit" name="cm-f-yuitit" type="hidden" value="Lot <?= $lot->lot_number;?>" />
											        <button type="submit">Enquire</button> 
											        <a href="#" class="button lightbox"  id="lot_<?= $lot->lot_number;?>" zoom_to_lot="<?= $lot->lot_number;?>" zoom_level="19">View on Masterplan</a>
											    </p>
											    
											    <p>
											        <label for="fieldyuitii">Subscribe <input id="fieldyuitii" name="cm-f-yuitii" type="checkbox" value="Yes" checked="checked" /></label>
											    </p>
											</form>
										<?php endif;?>
									</li>
									<?php endforeach;?>
								</ul>
							</div>




							<?php endforeach;?>
						<?php endif;?>
						</div>

					</div><!-- .house-and-land-containter -->
				</div><!-- tab content -->
			</div>
			</section><!-- container -->
	</section><!-- tab area -->
</div><!-- horozontal tab-->