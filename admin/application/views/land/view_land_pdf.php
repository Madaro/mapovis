<?php
$based_url   = base_url();
$folder_url  = $based_url.'application/views/land/';
$first_stage = TRUE;
$include_price = (isset($include_price))? $include_price: FALSE;
?>
<style>
h2{
	font-size: 18px;
}
h3{
	font-size: 15px;
}
h4{
	font-size: 13px;
}
.content{
	font-family: "arial","times","Helvetica","Arial","sans-serif";
	font-size: 10px;
	font-weight: normal;
}
table{
	text-align: center;
}
table td{
	padding-left: 7px;
	padding-bottom: 7px;
	padding-top: 5px;
	line-height: 20px;
}
.alert-warning {
	background-color: #fcf8e3;
	border-color: #faebcc;
	color: #8a6d3b;
}
.land-starting-from {
	background-color: #5BC0DE;
	color: #FFFFFF;
	box-sizing: border-box;
}
.percentage-sold {
	background-color: #428BCA;
	box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.15) inset;
	color: #FFFFFF;
	height: 100%;
	line-height: 20px;
	transition: width 0.6s ease 0s;
}
.table-titles {
	background-color: #F5F5F5;
}
.tablex {
	border: 1px solid #dddddd;
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
}
.tablex th,
.tablex td {
	border: 1px solid #dddddd;
}
.tablex td {
	font-size: 9px;
}
.btn-danger {
	background-color: #D43F3A;
	color: #FFFFFF;
	font-weight: bold;
}
.danger_row {
	background-color: #F2DEDE;
}
.success_row {
	background-color: #DFF0D8;
}
.btn-success {
	background-color: #3E8F3E;
	color: #FFFFFF;
	font-weight: bold;
}
.progress {
	background-color: #F5F5F5;
}
</style>

	<div class="content">
	<?php if($include_price): ?>
		<h2><?= $development->development_name;?> (<?= $development->developer;?>)</h2>
	<?php endif; ?>
	<?php foreach($stages as $stage):?>
		<?php if(!$first_stage):?>
			<br pagebreak="true"/>
		<?php endif;?>
		<?php $first_stage = FALSE;?>
	<!-- LAND - Stage -->
		<h3>STAGE <?= strtoupper($stage->word_of_number);?></h3>
		<!-- Disclaimer -->
		<div class="alert-warning">
			<span><strong>Indicitive Prices Only</strong>- please enquire for further information</span>
		</div>

		<?php 
		$street_id  = -1;
		$open_table = false;
		foreach($stage->lots as $lot):?>
			<?php
				if($lot->street_name_id != $street_id){
					if($open_table){
						?>
							<!-- closing table -->
							</table>
							<br pagebreak="true"/>
						<?php
					}
					$street_id  = $lot->street_name_id;
					$open_table = true;
					?><!-- Street -->
					<h4 class="panel-title"><?= ($lot->street_name)? $lot->street_name: 'No street provided';?></h4>
					<table class="tablex" align="center">
						<tr>
							<td class="table-titles">
								<span class="glyphicon glyphicon-th-large"></span>
								<b>Lot</b>
							</td>
							<td class="table-titles">
								<span class="glyphicon glyphicon-fullscreen"></span>
								<b>Land Size</b>
							</td>
							<td class="table-titles">
								<span class="glyphicon glyphicon-resize-horizontal"></span>
								<b>Frontage</b>
							</td>
							<td class="table-titles">
								<span class="glyphicon glyphicon-usd"></span>
								<b>Price Range</b>
							</td>
							<td class="table-titles">
								<span class="glyphicon glyphicon-info-sign"></span>
								<b>Status</b>
							</td>
						</tr>
					<?php
				}
			?>
			<tr>
				<td>Lot <?= $lot->lot_number;?></td>
				<td><?= $lot->lot_square_meters;?> m<sup>2</sup></td>
				<td><?= round($lot->lot_width, 5);?><sup>m</sup></td>
				<td>$<?= number_format($lot->price_range_min);?> - $<?= number_format($lot->price_range_max);?></td>
				<?php
				if($lot->status == 'Available'){
					$button = 'success';
				}
				else{
					$button = 'danger';
				}
				?>
				<td class="<?= $button;?>_row">
					<button type="button" class="btn-<?= $button;?>"><?= strtoupper($lot->status);?></button>
				</td>
			</tr>
			<?php endforeach;
			if($open_table){
				?>
					</table>
				<?php
			}
			?>
	<br />
	<?php endforeach;?>
	</div>
