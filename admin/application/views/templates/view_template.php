<?php
$based_url    = base_url();
$folder_url   = $based_url.'application/views/land/';
$manual_price = ($development->show_pricing == 0);
$land_price   = ($manual_price)? (float)$development->land_starting_from: FALSE;
?>
	<div class="container">
		<!-- SEARCH -->
		<br/>
		<div class="panel panel-default">
			<div class="panel-heading">TEMPLATE VIEW</div>
			<div class="panel-body">
				<form name="lots-search" action="#" method="post">
					<div class="lot-search-form">

						<div class="input-group enquiry-form-input">
							<span class="lot-frontage-title">
								<span class="glyphicon glyphicon-fullscreen"></span>Lot Frontage:</span>
							<ul class="radio-holder">
								<?php foreach($search_parameters['lot_widths'] as $item_value => $item_label):?>
									<li>
										<input name="lot_width[]" type="checkbox" id="width_<?= $item_value;?>" value="<?= $item_value;?>" <?= (isset($form_data['lot_width']) && in_array($item_value, $form_data['lot_width']))? 'checked="checked"':'';?>>
										<label for="width_<?= $item_value;?>"><?= $item_label;?></label>
									</li>
								<?php endforeach;?>
							</ul>

							<span class="lot-size-title">
								<span class="glyphicon glyphicon-resize-horizontal"></span>Lot Size:</span>
							<ul class="radio-holder">
								<?php foreach ($search_parameters['lot_square_meters'] as $item_value => $item_label):?>
									<li>
										<input name="lot_square_meters[]" type="checkbox" id="size_<?= $item_value;?>" value="<?= $item_value;?>" <?= (isset($form_data['lot_square_meters']) && in_array($item_value, $form_data['lot_square_meters']))? 'checked="checked"':'';?>>
										<label for="size_<?= $item_value;?>"><?= $item_label;?></label>
									</li>
								<?php endforeach;?>
							</ul>
						</div>
						<span>
							<button type="submit" class="btn btn-success">Refine Search</button>
						</span>

					</div>
				</form>
			</div>
		</div>
		<!-- END SEARCH -->


	<!-- Enquiry Modal
	================================================== -->

	<div class="modal fade enquiry-form-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myLargeModalLabel">Enquiry</h4>
				</div>

				<div class="modal-body">
					<!-- START: General Enquiry Form -->
					<div class="row" style="border-bottom: none;">
						<div class="col-md-7">
							<iframe src="<?= $development->form_iframe_url;?>" width="100%" height="<?= $development->form_iframe_url_height;?>" frameborder="0"></iframe>
						</div>

						<div class="col-md-5">
							<!--  NOTE: In the future this picture and corresponding link WILL come from the database. For now it is just a placeholder.  -->
							<a href="http://burbank.com.au/docs/homes/Albany/generic/brochures/Albany.pdf" target="_blank">
								<img data-src="holder.js/400x280" class="img-thumbnail" alt="Brochure Link">
							</a>
						</div>
					</div>
					<!-- END: Enquiry Form -->
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>

	<!-- Terms and Conditions Modal
	================================================== -->
	<div class="modal fade terms-and-conditions-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myLargeModalLabel">Terms & Conditions</h4>
				</div>
				<div class="modal-body">
					<?= $development->terms_and_conditions;?>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>