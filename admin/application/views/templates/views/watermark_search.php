<aside id="sidebar">
    <section class="widget forms-widget">
        <header class="heading">
            <h2 id="title-1" class="tab-title register-title">REGISTER</h2>
            <h2 id="title-2" class="tab-title">SEARCH</h2>
            <h2 id="title-3" class="tab-title">SEARCH</h2>
            <h3>AND TREASURE A BALANCED LIFESTYLE</h3>
        </header>
        <ul class="radio-tabs">
            <li>
                <label class="register-tab">
                    <input type="radio" data-rel="#title-1" data-box="#form-1" name="radio-tabs" checked class="tab-change">
                    <span>Register Now</span>
                </label>
            </li>
            <li>
                <label class="register-tab">
                    <input type="radio" data-rel="#title-2" data-box="#form-2" name="radio-tabs" class="tab-change">
                    <span>Search Land Only</span>
                </label>
            </li>
            <li>
                <label class="register-tab">
                    <input type="radio" data-rel="#title-3" data-box="#form-3" name="radio-tabs" class="tab-change">
                    <span>Search House &amp; Land</span>
                </label>
            </li>
        </ul>
        <form class="form validation" action="http://red23.runway.com.au/actions/form/globalformaction.jsp"  method="post" novalidate name="Registration Form" id="form-1">
            <input class="form-control" type="hidden" name="GroupID" value="0C1J481H8T3W4F9S105R9X7K224P">
            <input class="form-control" type="hidden" name="LocationID" value="0D174019813Q4G8T8I643U5V7L6L">
            <input class="form-control" type="hidden" name="NewContactStatusID" value="9187A1D0501491A19212A181D796C0E5">
            <input class="form-control" type="hidden" name="Source" value="Watermark Webform">
            <input class="form-control" type="hidden" name="-redirect" value="http://www.watermarkgeelong.com.au/thank-you/"><!-- Put the URL of the page you want to redirect to-->
            <input type="hidden" name="NotificationTemplateID" value="0J184V1R9B3S083B6X907M6T501B">
            <input class="form-control" type="hidden" name="sendNotificationTo" value="SALESREP">
            <input type="hidden" name="TemplateID" value="0L1D4C1D9Q212K6V5F3G3I7V0E4D">
            <input class="form-control" type="hidden" name="-alwaysnotify" value="true">
            <div class="row">
                <div class="field-holder col-md-12 col-sm-6">
                    <input class="form-control required" type="text" name="FirstName" placeholder="Name*">
                </div>
                <div class="field-holder col-md-12 col-sm-6">
                    <input class="form-control required-number" type="tel" name="Phone" placeholder="Phone*">
                </div>
                <div class="field-holder col-md-12 col-sm-6">
                    <input class="form-control required-email" type="email" name="Email" placeholder="Email*">
                </div>
                <div class="field-holder col-md-12 col-sm-6">
                    <input class="form-control required-number" name="Postcode" placeholder="Postcode*">
                </div>
                <div class="field-holder col-md-12 col-sm-6">
                    <select name="Answer0E1T441G9T272G3H7T4X3O8F6B0D" class="form-control required-select source-enquiry default-value">
                        <option class="hideme" value="">Source of enquiry*</option>
                        <option value="Online">Online</option>
                        <option value="Builder">Builder</option>
                        <option value="Signage">Signage</option>
                        <option value="Newspaper">Newspaper</option>
                        <option value="Word of Mouth">Word of Mouth</option>
                        <option value="Other">Other</option>
                    </select>
                    <input type="text" name="Answer0Q1O4Z143Y9C4H1V33108M3Y9B5I" class="form-control other" placeholder="Please Specify" rel="Please Specify" value="">
                </div>
                <div class="field-holder col-md-12 col-sm-6">
                    <button id="submit" class="btn btn-primary" data-ga-title="form submit" data-ga-category="Contact" data-ga-action="submit" type="submit" data-loading-text="Sending...">REGISTER NOW</button>
                </div>
            </div>
            <span class="note">* Denotes Required fields</span>
        </form>
        <form class="form" action="/living-options-land-search/" id="form-2">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <label for="select-04">FRONTAGE</label>
                    <div class="clearfix">
                        <div class="select-col">
                            <select name="lot_width_from" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                                <option value="">Any</option>
                                <?php foreach($search_parameters['lot_widths_from'] as $value => $label):?>
                                    <option value="<?= $value;?>" <?= (isset($form_data['lot_width_from']) && $form_data['lot_width_from'] == $value)? 'selected="selected"':'';?> ><?= $label;?></option>
                                    <?php endforeach;?>
                            </select>
                        </div>
                        <div class="select-col">
                            <select name="lot_width_to" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                                <option value="">Any</option>
                                <?php foreach($search_parameters['lot_widths_to'] as $value => $label):?>
                                    <option value="<?= $value;?>"  <?= (isset($form_data['lot_width_to']) && $form_data['lot_width_to'] == $value)? 'selected="selected"':'';?> ><?= $label;?></option>
                                    <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <label for="select-03">LAND SIZE</label>
                    <div class="clearfix">
                        <div class="select-col">
                            <select name="lot_square_meters_from" id="select-03" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                                <option value="">Any</option>
                                <?php foreach($search_parameters['lot_square_meters_from'] as $value => $label):?>
                                    <option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_from']) && $form_data['lot_square_meters_from'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="select-col">
                            <select name="lot_square_meters_to" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                                <option value="">Any</option>
                                <?php foreach($search_parameters['lot_square_meters_to'] as $value => $label):?>
                                    <option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_to']) && $form_data['lot_square_meters_to'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
                                <?php endforeach;?> 
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button class="submit btn btn-primary" type="submit">Search Land</button>
                </div>
            </div>
        </form>
        <form class="form" action="/living-options/" id="form-3">
            <div class="row">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <label for="select-03">LAND SIZE</label>
                    <div class="clearfix">
                        <div class="select-col">
                            <select name="lot_square_meters_from" id="select-03" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                                <option value="">Any</option>
                                <?php foreach($search_parameters['lot_square_meters_from'] as $value => $label):?>
                                    <option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_from']) && $form_data['lot_square_meters_from'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
                                    <?php endforeach;?>
                            </select>
                        </div>
                        <div class="select-col">
                            <select name="lot_square_meters_to" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                                <option value="">Any</option>
                                <?php foreach($search_parameters['lot_square_meters_to'] as $value => $label):?>
                                    <option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_to']) && $form_data['lot_square_meters_to'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
                                    <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <label for="select-04">PRICE</label>
                    <div class="clearfix">
                        <div class="select-col">
                            <select name="houseland_price_from" id="select-04" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                                <option value="">Any</option>
                                <?php for($min_price = $search_parameters['house_land_price_min']; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
                                    <option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_from']) && $form_data['houseland_price_from'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
                                    <?php endfor;?>
                            </select>
                        </div>
                        <div class="select-col">
                            <select name="houseland_price_to" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                                <option value="">Any</option>
                                <?php if($search_parameters['house_land_price_max']):?>
                                    <?php for($min_price = $search_parameters['house_land_price_min']+50000; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
                                        <option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
                                        <?php endfor;?>
                                    <option value="<?= $search_parameters['house_land_price_max'];?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $search_parameters['house_land_price_max'])? 'selected="selected"':'';?>>$<?= number_format(ceil($search_parameters['house_land_price_max']/1000));?>k</option>
                                    <?php endif;?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="select-col wide">
                            <label for="select-05">BEDS</label>
                            <select id="select-05" name="house_bedrooms" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                                <option value="">Any</option>
                                <?php foreach($search_parameters['house_bedrooms'] as $item):?>
                                    <option value="<?= $item['house_bedrooms'];?>" <?= (isset($form_data['house_bedrooms']) && $form_data['house_bedrooms'] == $item['house_bedrooms'])? 'selected="selected"':'';?>><?= $item['house_bedrooms'];?></option>
                                    <?php endforeach;?>
                            </select>
                        </div>
                        <div class="select-col wide">
                            <label for="select-06">BATHS</label>
                            <select name="house_bathrooms" id="select-06" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                                <option value="">Any</option>
                                <?php foreach($search_parameters['house_bathrooms'] as $item):?>
                                    <option value="<?= $item['house_bathrooms'];?>" <?= (isset($form_data['house_bathrooms']) && $form_data['house_bathrooms'] == $item['house_bathrooms'])? 'selected="selected"':'';?>><?= $item['house_bathrooms'];?></option>
                                    <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <label for="select-08">LEVELS</label>
                    <select name="house_levels" id="select-08" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                        <option value="">Any</option>
                        <?php foreach($search_parameters['house_levels'] as $item):?>
                            <option value="<?= $item['house_levels'];?>" <?= (isset($form_data['house_levels']) && $form_data['house_levels'] == $item['house_levels'])? 'selected="selected"':'';?>><?= $item['house_levels'];?></option>
                            <?php endforeach;?>
                    </select>
                    <label for="select-07">BUILDER</label>
                    <select name="builder_id" id="select-07" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                        <option value="">Any</option>
                        <?php foreach($search_parameters['builders'] as $item):?>
                            <option value="<?= $item['builder_id'];?>" <?= (isset($form_data['builder_id']) && $form_data['builder_id'] == $item['builder_id'])? 'selected="selected"':'';?>><?= $item['builder_name'];?></option>
                            <?php endforeach;?>
                    </select>
                    <label for="select-09">ORDER RESULTS</label>
                    <select name="order_by" id="select-09" class="form-control" data-jcf='{"fakeDropInBody": false}'>
                        <?php foreach($search_parameters['order_by'] as $item):?>
                            <?php if($item == 'builder'):?>
                                <option value="<?= $item;?>|asc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|asc')? 'selected="selected"':'';?>><?= ucwords($item);?> Name</option>
                                <?php else:?>
                                <option value="<?= $item;?>|asc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|asc')? 'selected="selected"':'';?>><?= ucwords($item);?>: Low to High</option>
                                <option value="<?= $item;?>|desc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|desc')? 'selected="selected"':'';?>><?= ucwords($item);?>: High to Low</option>
                                <?php endif;?>
                            <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button class="submit btn btn-primary" type="submit">Search Homes</button>
                </div>
            </div>
        </form>
    </section>