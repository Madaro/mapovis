	<section class="widget forms-widget">
		<header class="heading">
			<h2 class="tab-title register-title" id="title-1">REGISTER</h2>
			<h2 class="tab-title" id="title-2">SEARCH</h2>
			<h2 class="tab-title" id="title-3">SEARCH</h2>
			<h3>for your perfect<br class="visible-lg-inline"> <span class="hidden-sm">new</span> HOME</h3>
		</header>
		<ul class="radio-tabs">
			<li>
				<label class="register-tab"><input checked class="tab-change" name="radio-tabs" type="radio" data-box="#form-1" data-rel="#title-1"> Register Now</label>
			</li>
			<li>
				<label><input class="tab-change" name="radio-tabs" type="radio" data-box="#form-2" data-rel="#title-2"> Search Land Only</label>
			</li>
			<li>
				<label><input  class="tab-change" name="radio-tabs" type="radio" data-box="#form-3" data-rel="#title-3"> Search House &amp; Land</label>
			</li>
		</ul>
		<form id="form-1" method="post" action="http://www.tfaforms.com/responses/processor" class="form validation">
			<div class="row">
				<div class="col-lg-12 col-sm-6 col-xs-12">
					<div class="field-holder">
						<select class="form-control required-select default-value" name="tfa_1" id="tfa_1">
							<option class="hideme default-value" value="">Title*</option>
							<option value ="tfa_9" id="tfa_9" class="">Mr</option>
							<option value ="tfa_10" id="tfa_10" class="">Mrs</option>
							<option value ="tfa_11" id="tfa_11" class="">Ms</option>
							<option value ="tfa_12" id="tfa_12" class="">Miss</option>
						</select>
						<label class="error-label" for="select-field-01">Select A Title</label>
					</div>
					<div class="field-holder">
						<input class="form-control required" type="text" name="tfa_2" placeholder="First Name*" id="tfa_2">
						<label class="error-label" for="tfa_2">Enter Your First Name</label>
					</div>
					<div class="field-holder">
						<input class="form-control required" type="text" name="tfa_3" placeholder="Last Name*" id="tfa_3">
						<label class="error-label" for="tfa_3">Enter Your Last Name</label>
					</div>
				</div>
				<div class="col-lg-12 col-sm-6 col-xs-12">
					<div class="field-holder">
						<input class="form-control required-email validate-email required" type="email" name="tfa_4" placeholder="Email*" id="tfa_4">
						<label class="error-label" for="tfa_4">Enter A Valid Email Address</label>
					</div>
					<div class="field-holder">
						<input class="form-control required-number required" type="tel" name="tfa_5" placeholder="Phone*" id="tfa_5">
						<label class="error-label" for="text-field-04">Enter A Valid Contact Number</label>
					</div>
					<div class="field-holder">
						<input class="form-control" type="text" name="tfa_13" placeholder="Postcode" id="text-field-05">
					</div>	
				</div>


				<div class="col-lg-12 col-sm-6 col-xs-12">
					<div class="field-holder">
						<select id="tfa_14" name="tfa_14" class="required form-control required-select default-value drop-blue-style">
							<option value="">How did you hear about us?*</option>
							<option value="tfa_15" id="tfa_15" class="">Agent referral </option>
							<option value="tfa_16" id="tfa_16" class="">Builder referral</option>
							<option value="tfa_17" id="tfa_17" class="">Family Fun Day</option>
							<option value="tfa_18" id="tfa_18" class="">Family/Friend referral</option>
							<option value="tfa_19" id="tfa_19" class="">Google</option>
							<option value="tfa_20" id="tfa_20" class="">Herald Sun</option>
							<option value="tfa_21" id="tfa_21" class="">Realestate.com.au</option>
							<option value="tfa_22" id="tfa_22" class="">Signage</option>
							<option value="tfa_23" id="tfa_23" class="">Radio</option>
						</select>
					</div>
				</div>

				<div class="col-xs-12"><button data-loading-text="Sending..." type="submit" data-ga-action="submit" data-ga-category="Contact" data-ga-title="form submit" class="btn btn-primary" id="submit">Submit</button></div>
				<div class="col-xs-12"><span class="note">*Required Fields</span></div>
				<input type="hidden" value="346567" name="tfa_dbFormId" id="tfa_dbFormId"><input type="hidden" value="" name="tfa_dbResponseId" id="tfa_dbResponseId"><input type="hidden" value="e340a157aaa8f2ccdb59ef863cc0c352" name="tfa_dbControl" id="tfa_dbControl"><input type="hidden" value="1" name="tfa_dbVersionId" id="tfa_dbVersionId"><input type="hidden" value="" name="tfa_switchedoff" id="tfa_switchedoff">

			</div>
		</form>
		<form id="form-2" action="/living-options-land/" class="form">
			<div class="row">
				<div class="col-lg-12 col-sm-12 col-xs-12">
					<label for="select-04"><i class="icon-size2"></i> FRONTAGE</label>
					<div class="clearfix">
						<div class="select-col">
							<select class="form-control" id="select-04" name="lot_width_from">
								<option value="">From Any</option>
								<?php foreach($search_parameters['lot_widths_from'] as $value => $label):?>
									<option value="<?= $value;?>" <?= (isset($form_data['lot_width_from']) && $form_data['lot_width_from'] == $value)? 'selected="selected"':'';?> ><?= $label;?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="select-col">
							<select class="form-control" name="lot_width_to">
								<option value="">To Any</option>
								<?php foreach($search_parameters['lot_widths_to'] as $value => $label):?>
									<option value="<?= $value;?>"  <?= (isset($form_data['lot_width_to']) && $form_data['lot_width_to'] == $value)? 'selected="selected"':'';?> ><?= $label;?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<label for="select-01"><i class="icon-size1"></i> LAND SIZE</label>
					<div class="clearfix">
						<div class="select-col">
							<select class="form-control" id="select-01" name="lot_square_meters_from">
								<option value="">From Any</option>
								<?php foreach($search_parameters['lot_square_meters_from'] as $value => $label):?>
									<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_from']) && $form_data['lot_square_meters_from'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="select-col">
							<select class="form-control" name="lot_square_meters_to">
								<option value="">To Any</option>
								<?php foreach($search_parameters['lot_square_meters_to'] as $value => $label):?>
									<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_to']) && $form_data['lot_square_meters_to'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<label for="email-field-01">PROJECT Updates</label>
					<input id="email-field-01" class="form-control" type="email" placeholder="Enter email for project updates">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12"><button type="submit" class="submit btn btn-primary">Search Land</button></div>
			</div>
		</form>
		<form id="form-3" action="/living-options/" class="form">
			<div class="row">
				<div class="col-lg-12 col-sm-6 col-xs-12">
					<label for="select-03"><i class="icon-size1"></i> Land SiZE</label>
					<div class="clearfix">
						<div class="select-col">
							<select class="form-control" id="select-03" name="lot_square_meters_from">
								<option value="">From Any</option>
								<?php foreach($search_parameters['lot_square_meters_from'] as $value => $label):?>
									<option value="<?= $value;?>"><?= $label;?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="select-col">
							<select class="form-control" name="lot_square_meters_to">
								<option value="">To Any</option>
								<?php foreach($search_parameters['lot_square_meters_to'] as $value => $label):?>
									<option value="<?= $value;?>"><?= $label;?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<label for="select-04"><i class="icon-dollar"></i> Price</label>
					<div class="clearfix">
						<div class="select-col">
							<select class="form-control" id="select-04" name="houseland_price_from">
								<option value="">From Any</option>
								<?php for($min_price = $search_parameters['house_land_price_min']; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
									<option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_from']) && $form_data['houseland_price_from'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
								<?php endfor;?>
							</select>
						</div>
						<div class="select-col">
							<select class="form-control" name="houseland_price_to">
								<option value="">To Any</option>
								<?php if($search_parameters['house_land_price_max']):?>
									<?php for($min_price = $search_parameters['house_land_price_min']+50000; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
										<option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
									<?php endfor;?>
									<option value="<?= $search_parameters['house_land_price_max'];?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $search_parameters['house_land_price_max'])? 'selected="selected"':'';?>>$<?= number_format(ceil($search_parameters['house_land_price_max']/1000));?>k</option>
								<?php endif;?>
							</select>
						</div>
					</div>
					<div class="clearfix">
						<div class="select-col wide">
							<label for="select-05"><i class="icon-bed"></i> Bed</label>
							<select class="form-control" name="house_bedrooms" id="select-05">
								<option value="">From Any</option>
								<?php foreach($search_parameters['house_bedrooms'] as $item):?>
									<option value="<?= $item['house_bedrooms'];?>" <?= (isset($form_data['house_bedrooms']) && $form_data['house_bedrooms'] == $item['house_bedrooms'])? 'selected="selected"':'';?>><?= $item['house_bedrooms'];?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="select-col wide">
							<label for="select-06"><i class="icon-shower"></i> BATH</label>
							<select class="form-control" id="select-06" name="house_bathrooms">
								<option value="">Any</option>
								<?php foreach($search_parameters['house_bathrooms'] as $item):?>
									<option value="<?= $item['house_bathrooms'];?>" <?= (isset($form_data['house_bathrooms']) && $form_data['house_bathrooms'] == $item['house_bathrooms'])? 'selected="selected"':'';?>><?= $item['house_bathrooms'];?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-sm-6 col-xs-12">
					<label for="select-07"><i class="icon-wrench"></i> Builder</label>
					<select class="form-control" id="select-07" name="builder_id">
						<option value="">Any</option>
						<?php foreach($search_parameters['builders'] as $item):?>
							<option value="<?= $item['builder_id'];?>" <?= (isset($form_data['builder_id']) && $form_data['builder_id'] == $item['builder_id'])? 'selected="selected"':'';?>><?= $item['builder_name'];?></option>
						<?php endforeach;?>
					</select>
					<label for="select-08"><i class="icon-levels"></i> Levels</label>
					<select class="form-control" id="select-08" name="house_levels">
						<option value="">Any</option>
						<?php foreach($search_parameters['house_levels'] as $item):?>
							<option value="<?= $item['house_levels'];?>" <?= (isset($form_data['house_levels']) && $form_data['house_levels'] == $item['house_levels'])? 'selected="selected"':'';?>><?= $item['house_levels'];?></option>
						<?php endforeach;?>
					</select>
					<label for="select-09">Order Results By</label>
					<select class="form-control" id="select-09" name="order_by">
						<?php foreach($search_parameters['order_by'] as $item):?>
							<?php if($item == 'builder'):?>
								<option value="<?= $item;?>|asc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|asc')? 'selected="selected"':'';?>><?= ucwords($item);?> Name</option>
							<?php else:?>
								<option value="<?= $item;?>|asc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|asc')? 'selected="selected"':'';?>><?= ucwords($item);?>: Low to High</option>
								<option value="<?= $item;?>|desc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|desc')? 'selected="selected"':'';?>><?= ucwords($item);?>: High to Low</option>
							<?php endif;?>
						<?php endforeach;?>
					</select>
					<label for="email-field-02">PROJECT Updates</label>
					<input id="email-field-02" class="form-control" type="email" placeholder="Enter email for project updates">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12"><button type="submit" class="submit btn btn-primary">Search Homes</button></div>
			</div>
		</form>
	</section>
