<?php
$base_url   = base_url().'builder/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $base_url ?>"><?= $builder->builder_name?></a></h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Live House & Land Packages</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Development</th>
											<th>Lot</th>
											<th>House Name</th>
											<th>Price</th>
											<th>Sales Person</th>
											<th>PDF</th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_development_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_lot" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_house" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_price" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_sales_person" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_sales_pdf" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($house_packages as $package):?>
										<tr>
											<td><?= $package->development_name; ?></td>
											<td><?= $package->lot_number; ?></td>
											<td><?= $package->house_name; ?></td>
											<td><?= ($package->house_lot_price)? '$'.number_format($package->house_lot_price): 'n/a'; ?></td>
											<td><?= $package->sales_person; ?></td>
											<td>
												<?php if(!empty($package->file_name)):?>
													<a title="Download File" href="<?= base_url().'../mpvs/templates/lot_house_pdfs/'.$package->file_name;?>" 
													   target="_blank">
														<span class="icon16 icomoon-icon-file-pdf"></span>
													</a>
												<?php endif?>
											</td>
											<td>
												<a title="Disable Package" href="<?= $base_url.'disablepackage/'.$package->lot_id.'/'.$package->house_id;?>"><button class="disable_package_btn btn btn-danger btn-xs">Disable Package</button></a>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->
			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script type="text/javascript">
	$(document).ready(function() { 	
		$('.disable_package_btn').click(function() {
			if(!confirm('Are you sure you want to disable the package?')){
				return false;
			}
			showLoading();
		});
	});
</script>