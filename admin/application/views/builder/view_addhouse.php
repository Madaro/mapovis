<?php
$base_url           = base_url().'builder/';
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
$house_id           = 0;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3><a href="<?= $base_url ?>"><?= $builder->builder_name?></a></h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Add House</span>
								</h4>
							</div>
							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form id="add_edit_house_form" method="post" class="form-horizontal" action="<?= $base_url; ?>addhouse" role="form" ENCTYPE="multipart/form-data">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">House Name:</label>
									<div class="col-lg-9">
										<input id="house_name" name="house_name" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Description:</label>
									<div class="col-lg-9">
										<div class="form-row">
											<textarea class="tinymce" name="house_description"></textarea>
										 </div>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">More Info Link:</label>
									<div class="col-lg-9">
										<input name="house_moreinfo_url" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Bedrooms:</label>
									<div class="col-lg-2">
									<input name="house_bedrooms" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Bathrooms:</label>
									<div class="col-lg-2">
									<input name="house_bathrooms" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Garages:</label>
									<div class="col-lg-2">
									<input name="house_garages" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">House Size (in squares):</label>
									<div class="col-lg-2">
									<input name="house_size" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label">Levels:</label>
									<div class="col-lg-2">
									<input name="house_levels" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Facade Pictures:</label>
									<div class="col-lg-9">
										<div id="originalFiles" style="display:none"></div>
										<div id="add_image_section_facade">
											<input type="file" name="image_file_facade" id="image_file_facade" class="image_files" value="" accept="image/*" >
											<button type="button" id="add_image_btn_facade" class="add_image_btn btn btn-success" area="_facade" house_id="<?= $house_id;?>" href="#">Add & Edit Photo</button>
											<img id='imageupload_facade' src='' style="display:none"/>
											<br/><br/>
										</div>
										<div class="alert alert-info">
											Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
										</div>

										<div id="img_container_facade" class="img_container"><ul class="sortable" id="sortable_facade"></ul></div>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Floor Plans:</label>
									<div class="col-lg-9">
										<div id="add_image_section_floor">
											<input type="file" name="image_file_floor" id="image_file_floor" class="image_files" value="" accept="image/*" >
											<button type="button" id="add_image_btn_floor" class="add_image_btn btn btn-success" area="_floor" house_id="<?= $house_id;?>" href="#">Add & Edit Photo</button>
											<img id='imageupload_floor' src='' style="display:none"/>
											<br/><br/>
										</div>
										<div class="alert alert-info">
											Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
										</div>

										<div id="img_container_floor" class="img_container"><ul class="sortable" id="sortable_floor"></ul></div>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Gallery Pictures:</label>
									<div class="col-lg-9">
										<div id="add_image_section_gallery">
											<input type="file" name="image_file_gallery" id="image_file_gallery" class="image_files" value="" accept="image/*" >
											<button type="button" id="add_image_btn_gallery" class="add_image_btn btn btn-success" area="_gallery" house_id="<?= $house_id;?>" href="#">Add & Edit Photo</button>
											<img id='imageupload_gallery' src='' style="display:none"/>
											<br/><br/>
										</div>
										<div class="alert alert-info">
											Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
										</div>

										<div id="img_container_gallery" class="img_container"><ul class="sortable" id="sortable_gallery"></ul></div>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group" style="padding-top:10px">
									<div class="col-lg-offset-2 col-lg-9">
										<button id="action_continue_btn" type="submit" class="btn btn-primary save_new_house">Add House & Continue</button>
										<span style="padding-left: 10px;"><button id="action_btn" id="" type="submit" class="btn btn-info save_new_house">Add House & Finish</button></span>
										<span style="padding-left: 10px;"><a href="<?= $base_url.'managehouses'; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
									</div>
								</div><!-- End .form-group  -->
								<input id="action_continue" name="action_continue" type="hidden" value="0">
							</form>

						</div>

					</div><!-- End .span6 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

	</div><!-- End #wrapper -->
<script>
$( document ).ready(function() {
	initilizeHouseView(0);
});
</script>
