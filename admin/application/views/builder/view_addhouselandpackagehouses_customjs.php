<style>
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 500px; }
	.sortable li { margin: 10px 10px 10px 0; padding: 0px; float: left; width: 150px; height: 81px;  text-align: center; display:block; overflow:hidden;}
	.sortable_view li {height: 81px;}
	.deleteimage{margin-top: 7px;}
	.img_container{overflow-y:hidden;}
	.ui-dialog { z-index: 65535; }
</style>
<script type="text/javascript" src="http://feather.aviary.com/js/feather.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/validate/jquery.validate.min.js"></script>
<script>
var featherEditor = new Aviary.Feather({
	apiKey: '84495f70872a7572',
	apiVersion: 3,
	tools: 'all',
	theme: 'light',
	onError: function(errorObj) {
		console.log(errorObj.message);
	}
});
function initialize_tables(table_id){
	var table_data;
	$('#'+table_id).find('thead input').keyup( function () {
	  table_data.fnFilter( this.value, table_data.oApi._fnVisibleToColumnIndex( 
		table_data.fnSettings(), $('#'+table_id).find('thead input').index(this) ) );
	} );
	table_data = $('#'+table_id).dataTable({
	  "aaSorting":[[0,"asc"]],
	  "bPaginate": false,
	  "sDom":"",
	  "bJQueryUI": false,
	  "bAutoWidth": false,
	  "bSortCellsTop": true
	});
	return table_data;
}

var disabled_table = initialize_tables('disabled_table');
var enabled_table = initialize_tables('enabled_table');
var lot_id = '<?= $lot->lot_id;?>';

$('.dataTables_length select').uniform();
$('.dataTables_length select').uniform();
$('.dataTables_paginate > ul').addClass('pagination');
$('.dataTables_filter>label>input').addClass('form-control');

$('#dialog_lot_house').dialog({autoOpen: false});

function submit_house_package(this_vals){
	var table_name = this_vals.attr('table');
	if(table_name == 'enabled'){
		var position = enabled_table.fnGetPosition(this_vals.parents('tr')[0]);
	}
	else{
		var position = disabled_table.fnGetPosition(this_vals.parents('tr')[0]);
	}
	var house_id = this_vals.attr('house_id');
	showLoading();
	$.post('<?= base_url();?>builder/addhouselandpackagedata/'+lot_id+'/'+house_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_lot_house').html(data);
				$('#dialog_lot_house').dialog({
					title: 'Submit Lot and House Package For Approval',
					height: 730,
					width: 900,
					modal: true,
					resizable: true,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){remove_tinymce();$('#dialog_lot_house').html('');}
				});
				$('#dialog_lot_house').dialog('open');
				init_tinymce();
				$("input, textarea, select").not('.nostyle').uniform();

				$('#edit_house_lot_package').submit(function(e){
					e.preventDefault();
					return false;
				})

				$('#edit_house_lot_package').validate({
					rules: {
						house_lot_price: {
							required: true,
							number: true,
							min: 100000,
							max: 1000000,
						},
						house_lot_pdf: {accept: false},
						image_file_facade: {accept: false}
					},
					messages: {
						house_lot_price: {
							required: 'Please provide the House & Land Price',
							number: 'Please provide a valid price, NO special characters',
							min: 'The value of the house & land package has not been entered correctly',
							max: 'The value of the house & land package has not been entered correctly',
						}
					},
					submitHandler: function(form) {
						if($('#download_lot_house_pdf').length == 0 && $('#house_lot_pdf').val() == ''){
							messageAlert('<div title="Fail">Please upload a PDF file.</div>');
							return false;
						}
						submitForm(form)
						return false;
					}
				});
				$('#sortable_facade').sortable({containment: "#img_container_facade",tolerance: "pointer"});
				$('#sortable_facade').disableSelection();
				function submitForm(form){
					showLoading();
					$.ajax( {
						url: '<?= base_url(); ?>builder/submithouselotpackage/'+lot_id+'/'+house_id,
						type: 'POST',
						data: new FormData( form ),
						processData: false,
						contentType: false
						}).done(function(data){
							hideLoading();
							var alert_msg = '';
							var result = jQuery.parseJSON(data);
							if(result.status == 1){
								alert_msg = '<div title="Success">'+result.msg+'</div>';
								var pdf = (result.obj.file_name == '')? '':'<a title="Download File" href="<?= base_url();?>../mpvs/templates/lot_house_pdfs_approval/'+result.obj.file_name+'" target="_blank"><span class="icon16 icomoon-icon-file-pdf"></span></a>';
								var new_row = [
									result.obj.house_name,
									result.obj.house_bedrooms,
									result.obj.house_bathrooms,
									result.obj.house_garages,
									result.obj.house_size,
									result.obj.house_levels,
									result.obj.formatted_price,
									pdf,
									'Pending',
									'<button type="button" house_id="'+house_id+'" id="deletepackage'+house_id+'" class="btn btn-danger btn-xs deletepackage" href="#">Delete</button>'
								]
								if(table_name == 'enabled'){
									enabled_table.fnDeleteRow(position);
								}
								else{
									disabled_table.fnDeleteRow(position);
								}
								enabled_table.fnAddData(new_row);
								$('#deletepackage'+house_id).on('click', function(e){
									deletePackageFunction($(this));
								});
								$('#dialog_lot_house').dialog("close");
							}
							else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
							messageAlert(alert_msg);
						});
					return false;
				};

				$('.add_image_btn').on('click', function(e){
					var area            = $(this).attr('area');
					var house_id        = $(this).attr('house_id');
					var image_field_obj = $('#image_file'+area);
					var input           = image_field_obj[0];
					if (input.files && input.files[0]){
						if(!validateimagefiletype(input.files[0].type)){
							// show dialog message
							return false;
						}
						var reader = new FileReader();
						// set where you want to attach the preview
						reader.target_elem = $(input).parent().find('preview');
						reader.onload = function (e) {
							var force_crop = true;
							var img = document.createElement("img");
							img.onload = function(){
								if(img.width == 244 && img.height == 133){
									force_crop = false;
								}
								$('#imageupload'+area).attr('src', e.target.result);
								launchEditor('imageupload'+area, e.target.result, input.files[0].type, house_id, force_crop,area);
							}
							img.src = e.target.result;
						};
						reader.readAsDataURL(input.files[0]);
					}
				});
				$('#cancel_lot_house_changes').click(function() {
					$('#dialog_lot_house').dialog('close');
				});
				hideLoading();
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
}

$('.deletepackage').on('click', function(e){
	deletePackageFunction($(this));
});

function deletePackageFunction(object){
	var position = enabled_table.fnGetPosition(object.parents('tr')[0]);
	var house_id = object.attr('house_id');
	$("<div><span>Are you sure you want to delete this House and Land package?</span></div>").dialog({
		resizable: false,
		height:140,
		modal: true,
		buttons: {
			"Delete Package": function() {
				showLoading();
				$.post("<?= base_url().'builder/deletepackage/'?>"+lot_id+'/'+house_id,{
						house_id: house_id,
						lot_id: lot_id
					},
					function(data,status){
						hideLoading();
						var alert_msg = '';
						if(status === 'success'){
							var result = jQuery.parseJSON(data);
							if(result.status == 2){
								var pdf = (result.obj.file_name == '')? '':'<a title="Download File" href="<?= base_url();?>../mpvs/templates/lot_house_pdfs_approval/'+result.obj.file_name+'" target="_blank"><span class="icon16 icomoon-icon-file-pdf"></span></a>';
								alert_msg = '<div title="Success">'+result.msg+'</div>';
								var new_row = [
									result.obj.house_name,
									result.obj.house_bedrooms,
									result.obj.house_bathrooms,
									result.obj.house_garages,
									result.obj.house_size,
									result.obj.house_levels,
									result.obj.formatted_price,
									pdf,
									'Active',
									'<button table="enabled" house_id="'+house_id+'" id="submit_house_package'+house_id+'" class="submit_house_package btn btn-xs btn-default">Request Update</button>'
								]
								enabled_table.fnDeleteRow(position);
								enabled_table.fnAddData(new_row);
								$('#submit_house_package'+house_id).click(function(){
									submit_house_package($(this))
								});
							}
							else if(result.status == 1){
								alert_msg = '<div title="Success">'+result.msg+'</div>';
								var new_row = [
									result.obj.house_name,
									result.obj.house_bedrooms,
									result.obj.house_bathrooms,
									result.obj.house_garages,
									result.obj.house_size,
									result.obj.house_levels,
									'<button table="disabled" house_id="'+house_id+'" id="submit_house_package'+house_id+'" class="submit_house_package btn btn-xs btn-success">Create Package</button>'
								]
								enabled_table.fnDeleteRow(position);
								disabled_table.fnAddData(new_row);
								$('#submit_house_package'+house_id).click(function(){
									submit_house_package($(this))
								});
							}
							else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
						}
						else{ alert_msg = '<div title="Fail">'+data+'</div>';}
						messageAlert(alert_msg);
				});
				$(this).dialog("close");
			},
			Cancel: function() {
				$(this).dialog( "close" );
			}
		}
	});
}
function launchEditor(id, src, filetype, house_id, force_crop, area){
	var editor_params = {
		image: id,
		url: src,
		fileFormat: filetype,
		onSave: function(imageID, newURL) {
			addNewImageHtml(house_id, newURL, newURL, area);
			var img = document.getElementById(imageID);
			img.src = newURL;
			featherEditor.close();
		}
	};
	if(force_crop){
		editor_params.forceCropPreset = ['Width: 244px, Height: 133px','244x133'];
		editor_params.initTool = 'crop';
		editor_params.cropPresetsStrict = 'true';
	}
	else{
		editor_params.tools = 'enhance,effects,frames,stickers,focus,brightness,contrast,saturation,warmth,sharpness,colorsplash,draw,text,redeye,whiten,blemish';
	}
	featherEditor.launch(editor_params);
}
var position = 100000;
function addNewImageHtml(house_id, newURL, imagecontent, area){
	position++;
	if(area == '_facade'){
		var image_field_obj = $('#image_file'+area);
		var input           = image_field_obj[0];
		var form_data = new FormData();
		form_data.append('image_file_facade', input.files[0]);
		$.ajax( {
			url: '<?= base_url(); ?>builder/uploadoriginalimage/',
			type: 'POST',
			data: form_data,
			processData: false,
			contentType: false
		}).done(function(data){
			var result = jQuery.parseJSON(data);
			if(result != false){
				var original_image = '<input type="hidden" name="original_image['+position+']" value="'+result+'">';
				$('#originalFiles').append(original_image)
			}
		});
	}

	var button_id = 'delete_img' + new Date().getTime();
	var new_image_html = '<li class="ui-state-default"><input name="facade_images[]" type="text" value="'+position+'||'+newURL+'" style="display:none"><div><img src="'+imagecontent+'" height="81" /></div>';
	new_image_html += '<button type="button" id="'+button_id+'" house_id="'+house_id+'" image_id="'+newURL+'" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button></li>';
	$('#sortable'+area).append(new_image_html);
	$('#'+button_id).on('click', function(e){
		deleteImageFunction($(this));
	});
}

$('.submit_house_package').click(function(){submit_house_package($(this))});
</script>