<?php
$base_url = base_url().'builder/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Documents portal</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4>
										<span class="icon16 icomoon-icon-home-6"></span>
										<span>Select a Development</span>
									</h4>
								</div>
								<div class="panel-body noPad">
									<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" >
										<thead>
										<tr>
											<th>Development Name</th>
											<th>Developer</th>
											<th>State</th>
											<th></th>
										</tr>
											<tr>
												<td><input type="text" name="search_development_name" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_developer" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_state" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td></td>

											</tr>
										</thead>
										<tbody>
											<?php foreach($developments as $development):?>
											<tr>
												<td><?= $development->development_name;?></td>
												<td><?= $development->developer;?></td>
												<td><?= $development->state;?></td>
												<td>
													<div class="controls center">
														<a href="<?php echo $base_url; ?>developmentdocuments/<?= $development->development_id;?>"><button class="btn btn-info btn-xs" href="#">View Documents</button></a>
													</div>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span6 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
