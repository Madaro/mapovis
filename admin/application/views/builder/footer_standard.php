	<!-- Define the default pagination length -->
	<script>
		var default_pagination     = '<?= (isset($default_pagination))? $default_pagination: 10;?>';
		var default_pagination_url = '<?= base_url().'builder/savedefaultpagination'?>';

	</script>
	<!-- Misc plugins -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/misc/nicescroll/jquery.nicescroll.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/misc/qtip/jquery.qtip.min.js"></script><!-- Custom tooltip plugin -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/misc/totop/jquery.ui.totop.min.js"></script> 

	<!-- Charts plugins -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/charts/sparkline/jquery.sparkline.min.js"></script><!-- Sparkline plugin -->

	<!-- Search plugin -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/misc/search/tipuesearch_set.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/misc/search/tipuesearch_data.js"></script><!-- JSON for searched results -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/misc/search/tipuesearch.js"></script>

	<!-- Form plugins -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/elastic/jquery.elastic.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/maskedinput/jquery.maskedinput-1.3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/togglebutton/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/globalize/globalize.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/color-picker/colorpicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/timeentry/jquery.timeentry.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/select/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/dualselect/jquery.dualListBox-1.3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/tiny_mce/tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/supr-theme/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/supr-theme/jquery-ui-sliderAccess.js"></script>

	<!-- Table plugins -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/tables/dataTables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/tables/dataTables/TableTools.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/tables/dataTables/ZeroClipboard.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/tables/responsive-tables/responsive-tables.js"></script><!-- Make tables responsive -->

	<!-- Init plugins -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script><!-- Core js functions -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatable.js"></script><!-- Init plugins only for page -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/forms.js"></script><!-- Init plugins only for page -->