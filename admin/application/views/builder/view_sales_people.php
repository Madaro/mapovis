<?php
$base_url           = base_url().'builder/';
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $base_url ?>"><?= $builder->builder_name?></a></h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span>Builder "<?= $builder->builder_name;?>" Sales People</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Name</th>
											<th>Email</th>
											<th>Telephone</th>
											<th></th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_email" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_telephone" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>

									<?php // Add in the USERS from the DB query to the Table
										foreach ($sales_people as $sales_person):?>
										<tr>
											<td><?= $sales_person->builder_sales_person_id;?></td>
											<td><?= $sales_person->name;?></td>
											<td><?= $sales_person->email;?></td>
											<td><?= $sales_person->telephone;?></td>
											<td class="center">
												<button id="update_btn_<?= $sales_person->builder_sales_person_id?>" builder_sales_person_id="<?= $sales_person->builder_sales_person_id;?>" class="edit_sales_person_btn btn btn-xs btn-default">Edit</button>
											</td>
											<td>
												<?php if($sales_person->packages == 0):?>
													<a href="<?= "{$base_url}deletesalesperson/{$sales_person->builder_sales_person_id}";?>"><button person_name="<?= $sales_person->name;?>" class="delete_btn btn btn-xs btn-danger">Delete</button></a>
												<?php endif;?>
											</td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<div>
					<a href="<?= $base_url; ?>addsalesperson/">
						<button type="button" class="btn btn-info">Add Sales Person</button>
					</a>
				</div>
				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<!-- Image field necessary  to call Aviary and crop the images -->
<img id='imageupload' src='' style="display:none"/>
<!-- Dialog -->
<div id="dialog"></div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript">
	$('.edit_sales_person_btn').on('click', function(){
		var builder_sales_person_id = $(this).attr('builder_sales_person_id');
		showLoading();
		$.post('<?= $base_url;?>editsalesperson/'+builder_sales_person_id,
			function(data,status){
				if(status === 'success'){
					$('#dialog').html(data);
					$('#dialog').dialog({
						title: 'Edit Sales Person for <?= $builder->builder_name;?>',
						height: 600,
						width: 1000,
						modal: true,
						resizable: false,
						dialogClass: 'loading-dialog',
						close: function(ev,ui){$('#dialog').html('');}
					});
					$('#dialog').dialog('open');
					initilizeView(builder_sales_person_id);
					hideLoading();
					$('#cancel_changes').click(function() {
						$('#dialog').dialog('close');
					});
					return true;
				}
				messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
		});
	});
	$('.delete_btn').click(function() {
		var person_name = $(this).attr('person_name');
		if(!confirm('Are you sure you want to delete the sales person "'+person_name+'"?')){
			return false;
		}
		showLoading();
	});
</script>