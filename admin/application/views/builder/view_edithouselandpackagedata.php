<?php
$base_url = base_url().'builder/';
$lot_id   = $house_lot_package->lot_id;
$house_id = $house_lot_package->house_id;
$image_base_url = base_url().'../mpvs/images/dev_houses/';
$index_images   = count($house_images)? array_combine(array_map(function($v){return $v->house_image_id;}, $house_images), $house_images): array();
?>
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<div class="col-lg-11">
			<div class="panel-body">
				<form method="post" class="form-horizontal" id="edit_house_lot_package" action="<?= $base_url; ?>updatehouselotpackage/<?= $lot_id;?>/<?= $house_id;?>" role="form" enctype="multipart/form-data">
					<div class="form-group">
						<label class="col-lg-3 control-label">Lot Number:</label>
						<div class="col-lg-9">
						<?= $lot->lot_number;?>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label">House:</label>
						<div class="col-lg-9">
						<?= $house->house_name;?>
						</div>
					</div><!-- End .form-group  -->

					<?php if($house_lot_package && !empty($house_lot_package->file_name)):?>
						<div style="float:right">
							<a id="download_lot_house_pdf" href="<?= base_url().'../mpvs/templates/lot_house_pdfs_approval/'.$house_lot_package->file_name;?>" 
							   target="_blank">
								<button type="button" class="btn btn-success">Download File</button>
							</a>
						</div>

						<div class="form-group">
							<label class="col-lg-3 control-label">Current PDF:</label>
							<div class="col-lg-4">
							<?= $house_lot_package->file_name;?>
							</div>
						</div><!-- End .form-group  -->
					<?php endif;?>

					<div class="form-group">
						<label class="col-lg-3 control-label">House & Land Price :</label>
						<div class="col-lg-4">
						<input type="text" class="form-control" name="house_lot_price" id="house_lot_price" value="<?= ($house_lot_package)? $house_lot_package->house_lot_price: '';?>">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label">Description :</label>
						<div class="col-lg-8">
							<textarea class="tinymce" name="description">
								<?= ($house_lot_package)? $house_lot_package->description: '';?>
							</textarea>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" >Builder Sales Person:</label>
						<div class="col-lg-5">
							<select name="builder_sales_person_id" id="builder_sales_person_id" class="form-control">
								<option value="">None</option>
								<?php foreach($sales_people as $sales_person):?>
									<option value="<?= $sales_person->builder_sales_person_id?>" <?= ($house_lot_package && $house_lot_package->builder_sales_person_id == $sales_person->builder_sales_person_id)? 'selected="selected"': '';?>><?= $sales_person->name?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div><!-- End .form-group  -->

					<div id="lots_pdf_file_div" class="form-group">
						<label class="col-lg-3 control-label">PDF File:</label>
						<div class="col-lg-9">
							<div id="lots_pdf_file">
								<input type="file" name="house_lot_pdf" id="house_lot_pdf" value="" accept="application/pdf" >
							</div>
						</div>
					</div><!-- End .form-group  -->

					<?php if(count($house_images)):?>
					<div class="form-group">
						<label class="col-lg-3 control-label" >Facade Pictures:</label>
						<div class="col-lg-9">
							<div class="alert alert-info">
								Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
							</div>
							<div id="img_container_facade" class="img_container">
								<ul class="sortable" id="sortable_facade">
									<?php foreach(explode(',', $house_lot_package->images_order) as $house_image_id):
										if(!isset($index_images[$house_image_id])){
											continue;
										}
										$house_image = $index_images[$house_image_id];
										$image_name = $house_image->file_name;
										$image_url  = $image_base_url.$image_name;
									?>
										<li class="ui-state-default" >
											<input name="facade_images[]" type="text" value="<?= $house_image->house_image_id;?>" style="display:none">
											<img src="<?= $image_url;?>" width="150px" height="81px" />
										</li>
									<?php endforeach;?>
								</ul>
							</div>
						</div>
					</div><!-- End .form-group  -->
					<?php endif;?>

					<div class="form-group" style="padding-top:10px">
						<div class="col-lg-offset-3 col-lg-9">
							<button id="save_lot_house_changes" type="submit" class="btn btn-info">Update</button>
							<button id="cancel_lot_house_changes" type="button" class="btn btn-default">Cancel</button>
						</div>
					</div><!-- End .form-group  -->

				</form>
			</div>
	</div><!-- End .span6 -->
