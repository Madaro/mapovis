<style>
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 500px; }
	.sortable li { margin: 10px 10px 10px 0; padding: 0px; float: left; width: 150px; height: 116px;  text-align: center; display:block; overflow:hidden;}
	.deleteimage{margin-top: 7px;}
	.img_container{overflow-y:hidden;}
	.ui-dialog { z-index: 65535; }
	form .btn { margin-left:10px; }
</style>
<?php
$base_url = base_url().'builder/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $base_url ?>"><?= $builder->builder_name?></a></h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>House & Land Packages Awaiting Approval</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table id="packages_table" cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>House Name</th>
											<th>Lot</th>
											<th>Development</th>
											<th>Price</th>
											<th>Sales Person</th>
											<th>PDF</th>
											<th>Submitted On</th>
											<th>Submitted By</th>
											<th></th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_house" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_lot" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_development_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_price" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_sales_person" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_sales_pdf" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_request_on" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_request_by" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($packages_for_approval as $package):?>
										<tr>
											<td><?= $package->house_name; ?></td>
											<td><?= $package->lot_number; ?></td>
											<td><?= $package->development_name; ?></td>
											<td><?= ($package->house_lot_price)? '$'.number_format($package->house_lot_price): 'n/a'; ?></td>
											<td><?= $package->sales_person; ?></td>
											<td>
												<?php if(!empty($package->file_name)):?>
													<a title="Download File" href="<?= base_url().'../mpvs/templates/lot_house_pdfs_approval/'.$package->file_name;?>" 
													   target="_blank">
														<span class="icon16 icomoon-icon-file-pdf"></span>
													</a>
												<?php endif?>
											</td>
											<td><?= date('d-M-Y', strtotime($package->request_date)); ?></td>
											<td><?= $package->builder_user; ?></td>
											<td>
												<button type="button" house_id="<?= $package->house_id;?>" lot_id="<?= $package->lot_id;?>" class="btn btn-default btn-xs editpackage" href="#">Edit</button>
											</td>
											<td>
												<button type="button" house_id="<?= $package->house_id;?>" lot_id="<?= $package->lot_id;?>" class="btn btn-danger btn-xs deletepackage" href="#">Delete</button>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Dialog -->
<div id="dialog_lot_house"></div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/validate/jquery.validate.min.js"></script>
<script type="text/javascript">
$( document ).ready(function() {

	var packages_table = initialize_tables('packages_table');
	$('#dialog_lot_house').dialog({autoOpen: false});

	function initialize_tables(table_id){
		var table_data;
		$('#'+table_id).find('thead input').keyup( function () {
		  table_data.fnFilter( this.value, table_data.oApi._fnVisibleToColumnIndex( 
			table_data.fnSettings(), $('#'+table_id).find('thead input').index(this) ) );
		} );
		table_data = $('#'+table_id).dataTable({
		  "aaSorting":[[0,"asc"]],
		  "bPaginate": false,
		  "sDom":"",
		  "bJQueryUI": false,
		  "bAutoWidth": false,
		  "bSortCellsTop": true
		});
		return table_data;
	}

	function deletePackageFunction(object){
		var position = packages_table.fnGetPosition(object.parents('tr')[0]);
		var house_id = object.attr('house_id');
		var lot_id = object.attr('lot_id');
		$("<div><span>Are you sure you want to delete this House and Land package?</span></div>").dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				"Delete Package": function() {
					showLoading();
					$.post("<?= base_url().'builder/deletepackage/'?>"+lot_id+'/'+house_id,{
							house_id: house_id,
							lot_id: lot_id
						},
						function(data,status){
							hideLoading();
							var alert_msg = '';
							if(status === 'success'){
								var result = jQuery.parseJSON(data);
								if(result.status == 1 || result.status == 2){
									alert_msg = '<div title="Success">'+result.msg+'</div>';
									packages_table.fnDeleteRow(position);
								}
								else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
							}
							else{ alert_msg = '<div title="Fail">'+data+'</div>';}
							messageAlert(alert_msg);
					});
					$(this).dialog("close");
				},
				Cancel: function() {
					$(this).dialog( "close" );
				}
			}
		});
	}

	function editPackageFunction(object){
		var position = packages_table.fnGetPosition(object.parents('tr')[0]);
		var house_id = object.attr('house_id');
		var lot_id = object.attr('lot_id');
		
		$.post('<?= base_url();?>builder/edithouselandpackagedata/'+lot_id+'/'+house_id,
			function(data,status){
				if(status === 'success'){
					$('#dialog_lot_house').html(data);
					$('#dialog_lot_house').dialog({
						title: 'Edit Lot and House Package Awaiting Approval',
						height: 730,
						width: 900,
						modal: true,
						resizable: true,
						dialogClass: 'loading-dialog',
						close: function(ev,ui){remove_tinymce();$('#dialog_lot_house').html('');}
					});
					$('#dialog_lot_house').dialog('open');
					init_tinymce();
					$("input, textarea, select").not('.nostyle').uniform();

					$('#edit_house_lot_package').validate({
						rules: {
							house_lot_price: {
								required: true,
								number: true,
								min: 100000,
								max: 1000000,
							},
							house_lot_pdf: {accept: false}
						},
						messages: {
							house_lot_price: {
								required: 'Please provide the House & Land Price',
								number: 'Please provide a valid price, NO special characters',
								min: 'The value of the house & land package has not been entered correctly',
								max: 'The value of the house & land package has not been entered correctly',
							}
						},
						submitHandler: function(form) {
							if($('#download_lot_house_pdf').length == 0 && $('#house_lot_pdf').val() == ''){
								messageAlert('<div title="Fail">Please upload a PDF file.</div>');
								return false;
							}
							showLoading();
							form.submit();
						}
					});
					$('#sortable_facade').sortable({containment: "#img_container_facade",tolerance: "pointer"});
					$('#sortable_facade').disableSelection();
					$('#cancel_lot_house_changes').click(function() {
						$('#dialog_lot_house').dialog('close');
					});
					hideLoading();
					return true;
				}
				messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
		});
	}

	$('.deletepackage').on('click', function(e){
		deletePackageFunction($(this));
	});

	$('.editpackage').on('click', function(e){
		editPackageFunction($(this));
	});
});

</script>
