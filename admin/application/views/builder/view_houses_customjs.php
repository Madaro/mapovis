<style>
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 500px; }
	.sortable li { margin: 10px 10px 10px 0; padding: 0px; float: left; width: 150px; height: 116px;  text-align: center; display:block; overflow:hidden;}
	.sortable_view li {height: 81px;}
	.deleteimage{margin-top: 7px;}
	.img_container{overflow-y:hidden;}
	.ui-dialog { z-index: 65535; }
</style>
<script type="text/javascript" src="http://feather.aviary.com/js/feather.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/validate/jquery.validate.min.js"></script>
<script>
var featherEditor = new Aviary.Feather({
	apiKey: '84495f70872a7572',
	apiVersion: 3,
	tools: 'all',
	theme: 'light',
	onError: function(errorObj) {
		console.log(errorObj.message);
	}
});

function initilizeHouseView(house_id, height){
	if(house_id > 0){
		init_tinymce(height);
		$("input, textarea, select, button").attr('tabindex', '-1');
		$("input, textarea, select").not('.nostyle').uniform();
	}
	if($('#add_edit_house_form').length > 0){
		$(function(){
			$('#sortable_facade').sortable({containment: "#img_container_facade",tolerance: "pointer"});
			$('#sortable_facade').disableSelection();
			$('#sortable_floor').sortable({containment: "#img_container_floor",tolerance: "pointer"});
			$('#sortable_floor').disableSelection();
			$('#sortable_gallery').sortable({containment: "#img_container_gallery",tolerance: "pointer"});
			$('#sortable_gallery').disableSelection();
		});
		$('.image_files').on('change', function(){
			var input = $(this)[0];
			if (input.files && input.files[0]){
				if(!validateimagefiletype(input.files[0].type)){
					return false;
				}
			}
		});
		$('.deleteimage').on('click', function(e){
			deleteImageFunction($(this), true);
		});
		$('.add_image_btn').on('click', function(e){
			var area            = $(this).attr('area');
			var house_id        = $(this).attr('house_id');
			var image_field_obj = $('#image_file'+area);
			var input           = image_field_obj[0];
			if (input.files && input.files[0]){
				if(!validateimagefiletype(input.files[0].type)){
					// show dialog message
					return false;
				}
				var reader = new FileReader();
				// set where you want to attach the preview
				reader.target_elem = $(input).parent().find('preview');
				reader.onload = function (e) {
					var force_crop = true;
					var img = document.createElement("img");
					img.onload = function(){
						if(area != '_facade' || img.width == 244 && img.height == 133){
							force_crop = false;
						}
						$('#imageupload'+area).attr('src', e.target.result);
						launchEditor('imageupload'+area, e.target.result, input.files[0].type, house_id, force_crop,area);
					}
					img.src = e.target.result;
				};
				reader.readAsDataURL(input.files[0]);
			}
		});

		$('#add_edit_house_form').validate({
			rules: {
				house_name: {
					required: true,
					remote:{
						url: '<?php echo base_url(); ?>builder/validatehousename/',
						type: "post",
						data: {
							house_name: function() {
								return $('#house_name').val();
							},
							house_id: house_id,
						}
					}
				},
				house_bedrooms: {
					required: true,
					digits:true,
					max: 10
				},
				house_bathrooms: {
					required: true,
					digits:true,
					max: 10
				},
				house_garages: {
					required: true,
					digits:true,
					max: 10
				},
				house_size: {
					required: true,
					digits:true,
					max: 100
				},
				house_levels: {
					required: true,
					digits:true,
					max: 10
				},
				image_file_facade:{accept: false},
				image_file_floor:{accept: false},
				image_file_gallery:{accept: false}
			},
			messages: {
				house_name: {
					required: 'Please provide the house name',
				},
				house_bedrooms: {
					max: 'There must be a typo here.',
				},
				house_bathrooms: {
					max: 'There must be a typo here.',
				},
				house_garages: {
					max: 'There must be a typo here.',
				},
				house_size: {
					max: 'There must be a typo here.',
				},
				house_levels: {
					max: 'There must be a typo here.',
				}
			},
			submitHandler: function(form) {
				// check if there is at least one image uploaded
				if($('#sortable_facade').children().length == 0){
					messageAlert('<div title="Fail">Please upload at least one house photo.</div>');
					return false;
				}
				showLoading();
				form.submit();
			}
		});
	}
}
function deleteImageFunction(object, updatedb){
	var imageid = object.attr('image_id');
	var image_obj = object.parent();
	$("<div><span>Are you sure you want to delete this image permanently?</span></div>").dialog({
		resizable: false,
		height:140,
		modal: true,
		buttons: {
			"Delete Image": function() {
				if(updatedb){
					showLoading();
					$.post("<?= base_url().'builder/deletehouseimage'?>",{
							house_image_id: imageid
						},
						function(data,status){
							hideLoading();
							var alert_msg = '';
							if(status === 'success'){
								var result = jQuery.parseJSON(data);
								if(result.status == 1){
									alert_msg = '<div title="Success">'+result.msg+'</div>';
									image_obj.remove();
								}
								else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
							}
							else{ alert_msg = '<div title="Fail">'+data+'</div>';}
							messageAlert(alert_msg);
					});
				}
				else{
					image_obj.remove();
					messageAlert('<div title="Success">The image was deleted successfully.</div>');
				}
				$(this).dialog("close");
			},
			Cancel: function() {
				$('#save_changes').focus();
				$(this).dialog( "close" );
			}
		}
	});
}

function launchEditor(id, src, filetype, house_id, force_crop, area){
	var editor_params = {
		image: id,
		url: src,
		fileFormat: filetype,
		onSave: function(imageID, newURL) {
			addNewImageHtml(house_id, newURL, newURL, area);
			var img = document.getElementById(imageID);
			img.src = newURL;
			featherEditor.close();
		}
	};
	if(force_crop){
		editor_params.forceCropPreset = ['Width: 244px, Height: 133px','244x133'];
		editor_params.initTool = 'crop';
		editor_params.cropPresetsStrict = 'true';
	}
	else{
		editor_params.tools = 'enhance,effects,frames,stickers,focus,brightness,contrast,saturation,warmth,sharpness,colorsplash,draw,text,redeye,whiten,blemish';
	}
	featherEditor.launch(editor_params);
}

var position = 100000;
function addNewImageHtml(house_id, newURL, imagecontent, area){
	position++;
	if(area == '_facade'){
		var image_field_obj = $('#image_file'+area);
		var input           = image_field_obj[0];
		var form_data = new FormData();
		form_data.append('image_file_facade', input.files[0]);
		$.ajax( {
			url: '<?= base_url(); ?>builder/uploadoriginalimage/',
			type: 'POST',
			data: form_data,
			processData: false,
			contentType: false
		}).done(function(data){
			var result = jQuery.parseJSON(data);
			if(result != false){
				var original_image = '<input type="hidden" name="original_image['+position+']" value="'+result+'">';
				$('#originalFiles').append(original_image)
			}
		});
	}

	var button_id = 'delete_img' + new Date().getTime();
	var new_image_html = '<li class="ui-state-default"><input name="image_names'+area+'[]" type="text" value="'+position+'||'+newURL+'" style="display:none"><div><img src="'+imagecontent+'" height="81" /></div>';
	new_image_html += '<button type="button" id="'+button_id+'" house_id="'+house_id+'" image_id="'+newURL+'" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button></li>';
	$('#sortable'+area).append(new_image_html);
	$('#'+button_id).on('click', function(e){
		deleteImageFunction($(this), false);
	});
}


$( document ).ready(function() {
	if($('#action_continue_btn').length > 0){
		$('#action_continue_btn').click(function() {
			document.getElementById('action_continue').value = 1;
		});
		$('#action_btn').click(function() {
			document.getElementById('action_continue').value = 0;
		});
	}

});
</script>
