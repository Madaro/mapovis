		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><?= $development->development_name;?> - Document Portal</h3> 
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<?php foreach($types as $type):?>
				<?php $type_title = ucwords(str_replace('_', ' ', $type));?>
				<div class="row">
					<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4>
										<span class="icon16 icomoon-icon-file-3"></span>
										<span><?= $type_title;?></span>
									</h4>
									<!-- <a href="#" class="minimize">Minimize</a> -->
								</div>
								<div class="panel-body noPad">
									<?php if(isset($documents[$type]) && count($documents)):?>
									<table class="table table-bordered">
										<thead>
										<tr>
											<th>File Name</th>
											<th>Description</th>
											<th>Date Uploaded</th>
											<th>Download</th>
										</tr>
										</thead>
										<tbody>
											<?php foreach($documents[$type] as $document):?>
											<tr>
												<td>
													<a title="Download File" href="<?= base_url('../mpvs/documents/'.$document->file_name);?>" target="_blank">
														<?= $document->file_name;?>
													</a>
												</td>
												<td><?= $document->description;?></td>
												<td><?= date('d-m-Y', strtotime($document->upload_date));?></td>
												<td>
													<a title="Download File" href="<?= base_url('../mpvs/documents/'.$document->file_name);?>" target="_blank"><button class="btn btn-default btn-xs">View Document</button></a>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
									<?php else:?>
										<h6 style="padding-top:10px; padding-left:10px;"> No Documents found</h6>
									<?php endif;?>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span6 -->

				</div><!-- End .row -->
				<?php endforeach;?>

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
