<?php
$base_url       = base_url().'builder/';
$image_base_url = base_url().'../mpvs/images/dev_houses/';
?>

<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
<!-- Dialog -->
<div class="col-lg-10">
	<div class="panel-body">
		<form id="add_edit_house_form" method="post" class="form-horizontal" action="<?= $base_url; ?>updatehouse/<?= $house_id;?>" role="form" ENCTYPE="multipart/form-data">
			<div class="form-group">
				<label class="col-lg-3 control-label" for="textareas">House Name:</label>
				<div class="col-lg-9">
					<input name="house_name" id="house_name" type="text" class="form-control" value="<?= $house->house_name;?>">
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" for="textareas">Description:</label>
				<div class="col-lg-9">
					<div class="form-row">
						<textarea class="tinymce" name="house_description"><?= $house->house_description;?></textarea>
					</div>
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" for="textareas">More Info Link:</label>
				<div class="col-lg-9">
				<input name="house_moreinfo_url" type="text" class="form-control" value="<?= $house->house_moreinfo_url;?>">
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" for="textareas">Bedrooms:</label>
				<div class="col-lg-3">
				<input name="house_bedrooms" type="text" class="form-control" value="<?= $house->house_bedrooms;?>" >
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" for="textareas">Bathrooms:</label>
				<div class="col-lg-3">
				<input name="house_bathrooms" type="text" class="form-control" value="<?= $house->house_bathrooms;?>" >
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" for="textareas">Garages:</label>
				<div class="col-lg-3">
				<input name="house_garages" type="text" class="form-control" value="<?= $house->house_garages;?>" >
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" for="textareas">House Size (in squares):</label>
				<div class="col-lg-3">
				<input name="house_size" id="house_size" type="text" class="form-control" value="<?= $house->house_size;?>" >
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label">Levels:</label>
				<div class="col-lg-3">
				<input name="house_levels" type="text" class="form-control" value="<?= $house->house_levels;?>">
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" >Facade Pictures:</label>
				<div class="col-lg-9">
					<div id="originalFiles" style="display:none"></div>
					<div id="add_image_section<?= $house_id;?>">
						<input type="file" name="image_file_facade" id="image_file_facade" class="image_files" value="" accept="image/*" >
						<button type="button" id="add_image_btn_facade" class="add_image_btn btn btn-success" area="_facade" house_id="<?= $house_id;?>" href="#">Add & Edit Photo</button>
						<img id='imageupload_facade' src='' style="display:none"/>
						<br/><br/>
					</div>
					<div class="alert alert-info">
						Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
					</div>
					<div id="img_container_facade" class="img_container">
						<ul class="sortable" id="sortable_facade">
							<?php foreach($house_images as $house_image):
								$image_name = $house_image->file_name;
								$image_url  = $image_base_url.$image_name;
							?>
								<li class="ui-state-default" >
									<input name="image_names_facade[]" type="text" value="<?= $house_image->house_image_id;?>||" style="display:none">
									<img src="<?= $image_url;?>" width="150px" height="81px" />
									<button type="button" house_id="<?= $house_id;?>" image_id="<?= $house_image->house_image_id;?>" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button>
								</li>
							<?php endforeach;?>
						</ul>
					</div>
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" for="textareas">Floor Plans:</label>
				<div class="col-lg-9">
					<div id="add_image_section<?= $house_id;?>">
						<input type="file" name="image_file_floor" id="image_file_floor" class="image_files" value="" accept="image/*" >
						<button type="button" id="add_image_btn_floor" class="add_image_btn btn btn-success" area="_floor" house_id="<?= $house_id;?>" href="#">Add & Edit Photo</button>
						<img id='imageupload_floor' src='' style="display:none"/>
						<br/><br/>
					</div>
					<div class="alert alert-info">
						Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
					</div>

					<div id="img_container_floor" class="img_container">
						<ul class="sortable" id="sortable_floor">
							<?php foreach($floor_plans as $house_image):
								$image_name = $house_image->file_name;
								$image_url  = $image_base_url.$image_name;
							?>
								<li class="ui-state-default" >
									<input name="image_names_floor[]" type="text" value="<?= $house_image->house_image_id;?>||" style="display:none">
									<div>
										<img src="<?= $image_url;?>" height="81px" />
									</div>
									<button type="button" house_id="<?= $house_id;?>" image_id="<?= $house_image->house_image_id;?>" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button>
								</li>
							<?php endforeach;?>
						</ul>
					</div>
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" for="textareas">Gallery Pictures:</label>
				<div class="col-lg-9">
					<div id="add_image_section<?= $house_id;?>">
						<input type="file" name="image_file_gallery" id="image_file_gallery" class="image_files" value="" accept="image/*" >
						<button type="button" id="add_image_btn_gallery" class="add_image_btn btn btn-success" area="_gallery" house_id="<?= $house_id;?>" href="#">Add & Edit Photo</button>
						<img id='imageupload_gallery' src='' style="display:none"/>
						<br/><br/>
					</div>
					<div class="alert alert-info">
						Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
					</div>

					<div id="img_container_gallery" class="img_container">
						<ul class="sortable" id="sortable_gallery">
							<?php foreach($gallery as $house_image):
								$image_name = $house_image->file_name;
								$image_url  = $image_base_url.$image_name;
							?>
								<li class="ui-state-default" >
									<input name="image_names_gallery[]" type="text" value="<?= $house_image->house_image_id;?>||" style="display:none">
									<div><img src="<?= $image_url;?>" height="81px" /></div>
									<button type="button" house_id="<?= $house_id;?>" image_id="<?= $house_image->house_image_id;?>" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button>
								</li>
							<?php endforeach;?>
						</ul>
					</div>
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group" style="padding-top:10px">
				<div class="col-lg-offset-3 col-lg-9">
					<button id="save_house_changes" type="submit" class="btn btn-info" >Save Changes</button>
					<button id="cancel_house_changes" type="button" class="btn btn-default">Cancel</button>
				</div>
			</div><!-- End .form-group  -->

		</form>
	</div>
</div><!-- End .span6 -->
