<?php
$base_url         = base_url().'builder/';
$image_base_url   = base_url().'../mpvs/images/builder_images/';
$validtion_errors = validation_errors();
$validation_msg   = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<div class="col-lg-10">
		<div class="panel-body">
		<?= $alert_message;?>
		<?= $validation_msg;?>
		<form method="post" id="add_edit_form" class="form-horizontal" action="<?= $base_url; ?>updatesalesperson/<?= $sales_person->builder_sales_person_id;?>" role="form" ENCTYPE="multipart/form-data">
			<div class="form-group">
				<label class="col-lg-2 control-label" for="textareas">Name:</label>
				<div class="col-lg-9">
				<input id="name" name="name" type="text" class="form-control" value="<?= $sales_person->name;?>">
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-2 control-label" for="textareas">Telephone:</label>
				<div class="col-lg-9">
				<input id="telephone" name="telephone" type="text" class="form-control" value="<?= $sales_person->telephone;?>">
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-2 control-label" for="textareas">Mobilephone:</label>
				<div class="col-lg-9">
				<input id="mobilephone" name="mobilephone" type="text" class="form-control" value="<?= $sales_person->mobilephone;?>">
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-2 control-label" for="textareas">Email:</label>
				<div class="col-lg-9">
				<input id="email" name="email" type="text" class="form-control" value="<?= $sales_person->email;?>">
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-2 control-label" for="textareas">Picture:</label>
				<div class="col-lg-9">
					<div id="add_image_section<?= $builder->builder_id;?>" class="add_image_section">
						<input type="file" name="image_file" id="image_file" class="image_files" value="" accept="image/*" >
						<button type="button" id="add_image_btn" class="add_image_btn btn btn-success" area="" href="#">Add & Edit Picture</button>
						<img id='imageupload' src='' style="display:none"/>
						<br/><br/>
					</div>
					<div id="img_container" class="img_container">
						<ul class="sortable" id="sortable">
							<?php if(!empty($sales_person->picture)):
								$image_name = $sales_person->picture;
								$image_url  = $image_base_url.$image_name;
							?>
								<li class="ui-state-default" >
									<input name="image_name" type="text" value="<?= $image_name;?>" style="display:none">
									<img src="<?= $image_url;?>" width="130px" height="130px" />
									<button type="button" sales_person_id="<?= $sales_person->builder_sales_person_id;?>" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button>
								</li>
							<?php endif;?>
						</ul>
					</div>
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group" style="padding-top:10px">
				<div class="col-lg-9" style="padding-left:145px;">
					<button id="save_builder_changes" type="submit" class="btn btn-info">Save Changes</button>
					<button id="cancel_changes" style="margin-left: 20px;" type="button" class="btn btn-default" onclick="$('#dialog').dialog( 'close' );">Cancel</button>
				</div>
			</div><!-- End .form-group  -->
		</form>

	</div><!-- End .span6 -->

</div><!-- End .row -->
