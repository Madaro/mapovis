<?php
$base_url = base_url().'builder/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Dashboard</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-4">
						<div class="well well-lg">
							<?= $welcome_message;?>
						</div>
					</div><!-- End .span4 -->

					<?php if($builder):?>
					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Houses</span>
								</h4>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>managehouses/"><button class="btn btn-primary">View</button></a>
								<a href="<?= $base_url; ?>addhouse/" style="padding-left: 20px;"><button class="btn btn-info">Add</button></a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-5">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>House & Land Packages</span>
								</h4>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>addhouselandpackage/"><button class="btn btn-primary">Create New</button></a>
								<a href="<?= $base_url; ?>houselandpackages/" style="padding-left: 20px;"><button class="btn btn-info">Live Packages</button></a>
								<a href="<?= $base_url; ?>houselandpackageapproval/" style="padding-left: 20px;"><button class="btn btn-primary">Awaiting Approval</button></a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Document Portal</span>
								</h4>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>documentportalmanage/"><button class="btn btn-primary">View Documents</button></a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-users"></span>
									<span>Sales People</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>salespeople/" style="padding-left: 20px;">
									<button type="button" class="btn btn-primary">Manage</button>
								</a>
								<a href="<?= $base_url; ?>addsalesperson/" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Add</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<?php else:?>
					<h4>Not assigned to a builder yet.</h4>
					<?php endif;?>

				</div><!-- End .row -->

			</div><!-- End content wrapper -->
		</div><!-- End #content -->
