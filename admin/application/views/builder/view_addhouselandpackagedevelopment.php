<?php
$base_url       = base_url().'builder/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $base_url ?>"><?= $builder->builder_name?></a></h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Select a Development</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Development Name</th>
											<th>Developer</th>
											<th>State</th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_development_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_developer" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_state" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($developments as $development):?>
										<tr>
											<td><?= $development->development_name; ?></td>
											<td><?= $development->developer; ?></td>
											<td><?= $development->state; ?></td>
											<td class="center">
												<a href="<?= $base_url.'addhouselandpackagelots/'.$development->development_id;?>"><button class="btn btn-xs btn-default view_house_btn">Select</button></a>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Image field necesary to call Aviary and crop the images -->
<img id='imageupload' src='' style="display:none"/>
<!-- Dialog -->
<div id="dialog_house"></div>
