<?php
$base_url         = base_url().'builder/';
$validtion_errors = validation_errors();
$validation_msg   = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3>Manage Account</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
					<ul class="breadcrumb">
						<li>You are here:</li>
						<li>
							<a href="#" class="tip" title="back to dashboard">
								<span class="icon16 icomoon-icon-screen-2"></span>
							</a> 
							<span class="divider">
								<span class="icon16 icomoon-icon-arrow-right-3"></span>
							</span>
						</li>
						<li class="active">Manage Account</li>
					</ul>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-bubble-user"></span>
									<span>Update Your Account</span>
								</h4>
								<a href="#" class="minimize">Minimize</a>
							</div>

							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
								<form method="post" action="<?= $base_url; ?>manageaccount" class="form-horizontal seperator" role="form">
									<div class="form-group">
										<label class="col-lg-2 control-label" for="username">Username:</label>
										<div class="col-lg-10">
											<input class="form-control" id="username" type="text" disabled="disabled" value="<?= $user->username;?>" />
											<span class="hint"><a href="<?php echo $base_url; ?>contactus" class="red">Request a Change</a></span>
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="username">Email:</label>
										<div class="col-lg-10">
											<input name="email" class="form-control" id="email" type="text" value="<?= $user->email;?>" />
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="username">Full Name:</label>
										<div class="col-lg-10">
											<input name="name" class="form-control" id="name" type="text" value="<?= $user->name;?>" />
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="username">Office:</label>
										<div class="col-lg-10">
											<input name="office" class="form-control" id="name" type="text" value="<?= $user->office;?>" />
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="username">Office Telephone:</label>
										<div class="col-lg-10">
											<input name="officephone" class="form-control" id="name" type="text" value="<?= $user->officephone;?>" />
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="username">Mobile Phone:</label>
										<div class="col-lg-10">
											<input name="mobilephone" class="form-control" id="name" type="text" value="<?= $user->mobilephone;?>" />
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-2 control-label" for="username">Password:</label>
										<div class="col-lg-10">
											<input class="form-control" id="password" name="password" type="password" placeholder="Enter a new password if you wish to update it" />
											<input class="form-control" id="passwordConfirm" name="confirm_password" type="password" placeholder="Enter your password again" />
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<div class="col-lg-offset-2">
											<button type="submit" class="btn btn-info marginR10 marginL10">Save changes</button>
											<a href="<?= $base_url; ?>"><button type="button" class="btn btn-danger">Cancel</button></a>
										</div>
									</div><!-- End .form-group  -->
								</form>

							</div><!-- End .span12 -->

						</div><!-- End .panel -->

					</div><!-- End .col-lg-12 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
