<?php
$base_url       = base_url().'builder/';
$image_base_url = base_url().'../mpvs/images/dev_houses/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $base_url ?>"><?= $builder->builder_name?></a></h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span><?= $builder->builder_name?> Houses</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($houses)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>House Name</th>
											<th>Bedrooms</th>
											<th>Bathrooms</th>
											<th>Garages</th>
											<th>Size (sq)</th>
											<th>Levels</th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_house_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_beds" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_baths" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_garages" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_size" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_levels" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($houses as $house):?>
										<tr>
											<td><?= $house->house_name; ?></td>
											<td><?= $house->house_bedrooms; ?></td>
											<td><?= $house->house_bathrooms; ?></td>
											<td><?= $house->house_garages; ?></td>
											<td><?= $house->house_size; ?></td>
											<td><?= $house->house_levels; ?></td>
											<td class="center">
												<button id="opener_<?= $house->house_id?>" house_id="<?= $house->house_id?>" title="<?= $house->house_name?> by <?= $house->builder_name?>" class="btn btn-xs btn-default view_house_btn">Update</button>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not houses in the system.</div>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<div>
					<a href="<?= $base_url; ?>addhouse">
						<button type="button" class="btn btn-info">Add House</button>
					</a>
				</div>
				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Image field necesary to call Aviary and crop the images -->
<img id='imageupload' src='' style="display:none"/>
<!-- Dialog -->
<div id="dialog_house"></div>
