<?php
$base_url = base_url().'salesteam/';
?>
<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
<!-- Dialog -->
<div id="dialog_<?= $lot->lot_id?>" title="Dialog Title">
	<div class="col-lg-9">
		<div class="panel-body">
			<form method="post" class="form-horizontal" action="<?= $base_url; ?>updatelot/<?= $lot->lot_id;?>" role="form">
				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Status:</label>
					<div class="col-lg-9">
						<select name="status" class="form-control" id="status">
						<?php foreach($lot_status as $status):?>
							<option value="<?= $status;?>" <?= ($lot->status == $status)? 'selected="selected"': ''; ?>><?= $status;?></option>
						<?php endforeach;?>
						</select>
					</div>
				</div><!-- End .form-group  -->
				<div class="form-group">
					<label class="col-lg-3 control-label" for="buttons">Price Range:</label>
					<div class="col-lg-9">
						<div class="row">
							<div class="col-lg-4">
								<input name="price_range_min" value="<?= $lot->price_range_min;?>" type="text" class="form-control" id="pricerange-from">
							</div>

							<div class="col-lg-1 pricerange-to">to</div>
							<div class="col-lg-4">
								<input name="price_range_max" value="<?= $lot->price_range_max;?>" type="text" class="form-control" id="pricerange-from">
							</div>
						</div>
					</div>
				</div><!-- End .form-group  -->
				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Tittle:</label>
					<div class="col-lg-9">
						<input type="radio" name="lot_titled" value="1" <?= ($lot->lot_titled == 1? 'checked="checked"': '');?> id="lot_titled1" class="lot_titled"><label for="lot_titled1">&nbsp; Yes </label> &nbsp;&nbsp;
						<input type="radio" name="lot_titled" value="0" <?= ($lot->lot_titled == 0? 'checked="checked"': '');?> id="lot_titled0" class="lot_titled"><label for="lot_titled0">&nbsp; No </label>
					</div>
				</div><!-- End .form-group  --> 
				<div class="form-group" style="padding-top:10px">
					<div class="col-lg-offset-3 col-lg-9">
						<button id="save_lot_changes" type="submit" class="btn btn-info">Save Changes</button>
						<button id="cancel_lot_changes" type="button" class="btn btn-default">Cancel</button>
					</div>
				</div><!-- End .form-group  -->
			</form>
		</div>
	</div><!-- End .span6 -->
</div>