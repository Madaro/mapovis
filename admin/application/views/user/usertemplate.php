<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=$title?></title>

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- Force IE9 to render in normal mode -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<!-- Le styles -->
	<!-- Use new way for google web fonts 
	http://www.smashingmagazine.com/2012/07/11/avoiding-faux-weights-styles-google-web-fonts -->
	<!-- Headings -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
	<!-- Text -->
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' /> 
	<!--[if lt IE 9]>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
	<link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
	<link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
	<![endif]-->

	<!-- Core stylesheets do not remove -->
	<link id="bootstrap" href="<?php echo base_url(); ?>assets/css/bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/supr-theme/jquery.ui.supr.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />

	<!-- Plugins stylesheets -->
	<link href="<?php echo base_url(); ?>assets/plugins/misc/qtip/jquery.qtip.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/plugins/misc/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/plugins/misc/search/tipuesearch.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/forms/uniform/uniform.default.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/misc/prettify/prettify.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/misc/pnotify/jquery.pnotify.default.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/tables/dataTables/jquery.dataTables.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/tables/dataTables/TableTools.css" type="text/css" rel="stylesheet" />   

	<!-- Main stylesheets -->
	<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css" /> 

	<!-- Custom stylesheets ( Put your own changes here ) -->
	<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet" type="text/css" /> 

	<!--[if IE 8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script type="text/javascript" src="js/libs/excanvas.min.js"></script>
	  <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	  <script type="text/javascript" src="js/libs/respond.min.js"></script>
	<![endif]-->

	<!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-144-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-114-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-72-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-57-precomposed.png" />

	<!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
	<meta name="application-name" content="Mapovis Admin"/> 
	<meta name="msapplication-TileColor" content="#3399cc"/> 

	<!-- Load modernizr first -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/libs/modernizr.js"></script>

	<!-- jQuery -->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>



	</head>

	<body>
	<!-- loading animation -->
	<div id="qLoverlay"></div>
	<div id="qLbar"></div>
	<div id="qLoverlaymessage" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessage" class="qLmessage" style="display:none;"></div>

	<div id="header">
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<a class="navbar-brand" href="<?php echo base_url(); ?>salesteam/dashboard"><img src="<?php echo base_url(); ?>assets/images/mapovis-logo-small.png" width="189" height="51" /></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon16 icomoon-icon-arrow-4"></span>
				</button>
			</div> 
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
				</ul>

				<ul class="nav navbar-right usernav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle avatar" data-toggle="dropdown">
							<img src="<?php echo $avatar_icon;?>" width="38px" height="33px" alt="" class="image" /> 
							<span class="txt"><?php echo $this->session->userdata('user_logged_username'); ?></span>
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li class="menu">
								<ul>
									<li><a href="<?php echo base_url(); ?>salesteam/manageaccount"><span class="icon16 icomoon-icon-user-plus"></span>Manage Account</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li><a href="<?php echo base_url(); ?>login/logout"><span class="icon16 icomoon-icon-exit"></span><span class="txt"> Logout</span></a></li>
				</ul>
			</div><!-- /.nav-collapse -->
		</nav><!-- /navbar --> 

	</div><!-- End #header -->

	<div id="wrapper">

		<!--Responsive navigation button-->  
		<div class="resBtn">
			<a href="#"><span class="icon16 minia-icon-list-3"></span></a>
		</div>

		<!--Sidebar background-->
		<div id="sidebarbg"></div>
		<!--Sidebar content-->
		<div id="sidebar">

			<div class="sidenav">
				<div class="mainnav">
					<ul>
						<li><a href="<?php echo base_url(); ?>salesteam/dashboard"><span class="icon16 icomoon-icon-home-4"></span>Home</a></li>
						<li><a href="<?php echo base_url(); ?>salesteam/mydevelopments"><span class="icon16 icomoon-icon-home-7"></span>My Developments</a></li>

						<li>
							<a href="#"><span class="icon16 icomoon-icon-file-3"></span>Reports</a>
							<ul class="sub">
								<?php if($show_statistics):?>
									<li><a href="<?php echo base_url(); ?>salesteam/statisticsalldev"><span class="icon16 icomoon-icon-stats"></span>MAPOVIS Statistics</a></li>
									<li><a href="<?php echo base_url(); ?>salesteam/availablelots"><span class="icon16 icomoon-icon-file-7"></span>Available Lots</a></li>
									<li><a href="<?php echo base_url(); ?>salesteam/lotviewsstatisticsalldev"><span class="icon16 icomoon-icon-stats"></span>Lot Impressions Statistics</a></li>
									<li><a href="<?php echo base_url(); ?>salesteam/availlotstatisticsalldev"><span class="icon16 icomoon-icon-stats"></span>Available Lots Statistics</a></li>
									<li><a href="<?php echo base_url(); ?>salesteam/soldlotstatisticsalldev"><span class="icon16 icomoon-icon-stats"></span>Sales Rate Statistics</a></li>
									<li><a href="<?php echo base_url(); ?>salesteam/averagelotpricesreportalldev"><span class="icon16 icomoon-icon-stats"></span>Avg. Price By Frontage</a></li>
								<?php endif;?>
								<li><a href="<?php echo base_url(); ?>salesteam/changelog"><span class="icon16 icomoon-icon-file-7"></span>Lots Change Log</a></li>
								<li><a href="<?php echo base_url(); ?>salesteam/hnlchangelog"><span class="icon16 icomoon-icon-stats-2"></span>H&L Change Log</a></li>
								
							 </ul>
						</li>

						<li>
							<ul class="sub">
								<li><a href="<?php echo base_url(); ?>salesteam/contactus"><span class="icon16 icomoon-icon-envelop"></span>Contact Us</a></li>
								<li><a href="https://mapovis.zendesk.com" target="_blank"><span class="icon16 icomoon-icon-question"></span>Knowledge Base</a></li>
							 </ul>
						</li>

					</ul>
				</div>
			</div><!-- End sidenav -->

		</div><!-- End #sidebar -->

	<?php $this->load->view($layout_content); ?>

	</div><!-- End #wrapper -->

	<!-- Le javascript
	================================================== -->
	<!-- Important plugins put in all pages -->

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.js"></script>  
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/libs/jRespond.min.js"></script>

	<?php foreach($footer_custom_content_chunks as $footer_custom_content): ?>

		<?php $this->load->view($footer_custom_content); ?>
	<?php endforeach; ?>
	</body>
</html>
