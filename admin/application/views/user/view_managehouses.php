<?php
$base_url           = base_url().'salesteam/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$image_base_url     = base_url().'../mpvs/images/dev_houses/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span><?= $development->development_name;?> Houses</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($houses)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>House Name</th>
											<th>Builder</th>
											<th>Type</th>
											<th></th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_house_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_builder" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_type" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($houses as $house):?>
										<tr>
											<td><?= $house->house_name; ?></td>
											<td><?= $house->builder_name; ?></td>
											<td><?= ($house->development_id)? 'Custom House':'Global House'; ?></td>
											<td class="center">
												<button id="opener_<?= $house->house_id?>" development_id="<?= $development->development_id;?>" dev_house="<?= ($house->development_id? 1:0)?>" house_id="<?= $house->house_id?>" title="House ID <?= $house->house_id?>  [ <?= $house->house_name?> by <?= $house->builder_name?> ]" class="btn btn-xs btn-default view_house_btn"><?= ($house->development_id? 'Update': 'View & Edit Facades');?></button>
											</td>
											<td>
												<?php if($house->development_id):?>
													<a href="<?= "{$base_url}deletehouse/{$house->house_id}"; ?>"><button house_name="<?= $house->house_name;?>" class="delete_house_btns btn btn-xs btn-danger">Delete</button></a>
												<?php else:?>
													<button house_id="<?= $house->house_id?>" development_id="<?= $development->development_id;?>" title="Request a Change [<?= $house->house_name?> by <?= $house->builder_name?> ]" class="request_change_house_btns btn btn-xs btn-info">Request a Change</button><a href="<?= "{$base_url}requestchange/{$house->house_id}"; ?>"></a>
												<?php endif;?>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not houses in the system.</div>
										<a href="<?= $base_url; ?>addhouse/<?= $development->development_id;?>" style="padding-left: 20px;"><button class="btn btn-info">Add House</button></a>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Image field necesary to call Aviary and crop the images -->
<img id='imageupload' src='' style="display:none"/>
<!-- Dialog -->
<div id="dialog_house"></div>
