<?php
$base_url     = base_url().'admin/reports/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<?php if(empty($lot_views_by_width['results'])):?>
					<div class="heading">
						<h3>
							<span class="icon16 icomoon-icon-stats"></span>
							<span><?= $development->development_name;?> Impressions by Lot Frontage</span>
						</h3>
					</div><!-- End .heading-->
					<h4>
						No results found.
					</h4>
				<?php else:?>
				<div class="heading">
					<div class="col-lg-12">
						<div class="header_btn">
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>lotviewsstatisticsreport/<?= $development->development_id;?>" role="form">
								<button type="submit" class="btn btn-primary" style="float:right;">Export CSV</button>
								<input type="hidden" name="export_csv" value="1">
							</form>
						</div>
					</div>
					<h3></h3>
				</div><!-- End .heading-->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 align="center">
									<span class="icon16 icomoon-icon-stats"></span>
									<span><?= $development->development_name;?> Impressions by Lot Frontage</span>
								</h4>
							</div>

							<div class="panel-body">
								<?php
								$lotchart     = new \Kendo\Dataviz\UI\Chart('lotchart');

								$legend       = new \Kendo\Dataviz\UI\ChartLegend();
								$legend->visible(true);
								$lotchart->legend($legend);
								$lotchart->chartArea(array('background' => 'transparent'));

								$valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();
								$lotchart->addValueAxisItem($valueAxis);

								$impressions_categ = array_map(function ($str) { return 'we'.$str; }, array_keys($lot_views_by_width['results']));
								$avail_categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
								$avail_categoryAxis->labels(array('rotation' => -45));
								$avail_categoryAxis->categories($impressions_categ)->majorGridLines(array('visible' => false));
								$lotchart->addCategoryAxisItem($avail_categoryAxis);

								$tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
								$tooltip->visible(true);
								$tooltip->format('{0}%');
								$tooltip->template('#= series.name #: #= value #');
								$lotchart->tooltip($tooltip);

								foreach($lot_views_by_width['widths'] as $width){
									$graph_lot_impressions_width = array();

									foreach($lot_views_by_width['results'] as $week => $week_results){
										$graph_lot_impressions_width[] = (isset($week_results[$width]->number_lots))? (int)$week_results[$width]->number_lots: 0;
									}

									$lotviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
									$lotviews->name($width.'m')->data($graph_lot_impressions_width);
									$lotchart->addSeriesItem($lotviews);
								}

								$lotchart->seriesColors($chart_colors);
								$lotchart->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

								echo $lotchart->render();
								?>
							</div>
							<br>
							<div><h5 align="center">*For the purposes of this graph, lots are grouped together to the closest 0.5m frontage.</h5></div>

						</div><!-- End .panel -->
					</div><!-- End .span8 -->
				</div><!-- End .row -->

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-stats-2"></span>
									<span><?= $development->development_name;?> Impressions by Lot Frontage</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dateDynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Week Ending</th>
											<th>Sortable Week</th>
											<?php foreach($lot_views_by_width['widths'] as $width):?>
												<th><?php echo $width;?>m</th>
											<?php endforeach;?>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($lot_views_by_width['results'] as $week => $week_results):?>
										<tr>
											<?php $total_lots = 0;?>
											<td><?= $week;?></td>
											<td><?= current($week_results)->formatted_date;?></td>
											<?php foreach($lot_views_by_width['widths'] as $width):?>
												<?php $lot_views   = (isset($week_results[$width]->number_lots))? (int)$week_results[$width]->number_lots: 0;?>
												<?php $total_lots += $lot_views;?>
												<td><?= $lot_views;?></td>
											<?php endforeach;?>
												<td><b><?= $total_lots;?></b></td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span12 -->
				</div><!-- End .row -->
				<?php endif;?>
			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script type="text/javascript">
	$(document).ready(function() { 	
	if($('table').hasClass('dateDynamicTable')){
		$('.dateDynamicTable').dataTable( {
			// Sort by second colum
			"aoColumns": [
				{"iDataSort": 1},
				{"bVisible": false},
				<?php for($x = 1; $x <= count($lot_views_by_width['widths']); $x++):?>
					null,
				<?php endfor;?>
				null
			],
			"aaSorting":[[0, "desc"]], 
			"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
			"sPaginationType": "bootstrap",
			"bJQueryUI": false,
			"bAutoWidth": false,
			"iDisplayLength": (( typeof default_pagination !== 'undefined')? default_pagination: 10),
			"oLanguage": {
				"sSearch": "<span></span> _INPUT_",
				"sLengthMenu": "<span>_MENU_</span>",
				"oPaginate": { "sFirst": "First", "sLast": "Last" }
			}
		});
		$('.dataTables_length select').uniform();
		$('.dataTables_paginate > ul').addClass('pagination');
		$('.dataTables_filter>label>input').addClass('form-control');
		if(typeof default_pagination_url !== 'undefined'){
			$('.selector>select').on('change', function(){
				$.post(default_pagination_url,{
					default_pagination: $(this).val()
				});
			});
		}
	}
});
</script>