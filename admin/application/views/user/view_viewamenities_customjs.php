<script>
function launchEditor(id, src, filetype, dialog, amenity_id, force_crop){
	var editor_params = {
		image: id,
		url: src,
		fileFormat: filetype,
		onSave: function(imageID, newURL) {
			addNewImageHtml(amenity_id, newURL, newURL);
			var img = document.getElementById(imageID);
			img.src = newURL;
			showhide_AddNew(amenity_id);
			featherEditor.close();
		},
	};
	if(force_crop){
		editor_params.forceCropPreset = ['Width: 350px, Height: 222px','350x222'];
		editor_params.initTool = 'crop';
		editor_params.cropPresetsStrict = 'true';
	}
	else{
		editor_params.tools = 'enhance,effects,frames,stickers,focus,brightness,contrast,saturation,warmth,sharpness,colorsplash,draw,text,redeye,whiten,blemish';
	}
	featherEditor.launch(editor_params);
}
function deleteImageFunction(object, updatedb){
	var amenityid = object.attr('amenity_id');
	var imagename = object.attr('image_id');
	var image_obj = object.parent();
	$("<div><span>Are you sure you want to delete this image permanently?</span></div>").dialog({
		resizable: false,
		height:140,
		modal: true,
		buttons: {
			"Delete Image": function() {
				if(updatedb){
					showLoading();
					$.post("<?= base_url().'salesteam/deleteamenityimage'?>",{
							amenityid:   amenityid,
							imagename: imagename
						},
						function(data,status){
							hideLoading();
							var alert_msg = '';
							if(status === 'success'){
								var result = jQuery.parseJSON(data);
								if(result.status == 1){
									alert_msg = '<div title="Success">'+result.msg+'</div>';
									image_obj.remove();
									showhide_AddNew(amenityid);
								}
								else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
							}
							else{ alert_msg = '<div title="Fail">'+data+'</div>';}
							messageAlert(alert_msg);
					});
				}
				else{
					image_obj.remove();
					showhide_AddNew(amenityid);
					messageAlert('<div title="Success">The image was deleted successfully.</div>');
				}
				$(this).dialog("close");
			},
			Cancel: function() {
				$('#save_changes').focus();
				$(this).dialog( "close" );
			}
		}
	});
}
function addNewImageHtml(amenity_id, newURL, imagecontent){
	var button_id = 'delete_img' + new Date().getTime();
	var new_image_html = '<li class="ui-state-default"><input name="image_names[]" type="text" value="'+newURL+'" style="display:none"><img src="'+imagecontent+'" width="135" height="96" />';
	new_image_html += '<button type="button" id="'+button_id+'" amenity_id="'+amenity_id+'" image_id="'+newURL+'" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button></li>';
	$('#sortable_'+amenity_id).append(new_image_html);
	$('#'+button_id).on('click', function(e){
		deleteImageFunction($(this), false);
	});
}
function initOpenDialog(amenity_id){
	initViewEntity();
	showhide_AddNew(amenity_id);
	init_tinymce();
	$("input, textarea, select, button").attr('tabindex', '-1');
	$("input, textarea, select").not('.nostyle').uniform();
	$('.add_image_btn').on('click', function(e){
		var image_field = $(this).attr('image_field');
		var amenity_id = $(this).attr('amenity_id');
		var image_field_obj = $('#'+image_field);
		var input = image_field_obj[0];
		if (input.files && input.files[0]) {
			if(!validateimagefiletype(input.files[0].type)){
				/* shows dialog message */
				return false;
			}
			var reader = new FileReader();
			// set where you want to attach the preview
			reader.target_elem = $(input).parent().find('preview');
			reader.onload = function (e) {
				var force_crop = true;
				var img = document.createElement("img");
				img.onload = function(){
					if(img.width == 350 && img.height == 222){
						force_crop = false;
					}
					$("#imageupload").attr('src', e.target.result);
					launchEditor('imageupload', e.target.result, input.files[0].type, $('#dialog_amenity'), amenity_id, force_crop);
				};
				img.src = e.target.result;
			};
			reader.readAsDataURL(input.files[0]);
		}
	});
}
$('#dialog_amenity').dialog({ autoOpen: false });
$('.view_amenity_btn').on('click', function(){
	var amenity_id = $(this).attr('amenity_id');
	showLoading();
	$.post('<?= base_url();?>salesteam/viewamenity/'+amenity_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_amenity').html(data);
				$('#dialog_amenity').dialog({
					title: 'Update Amenity',
					height: 600,
					width: 1000,
					modal: true,
					resizable: false,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_amenity').html('');}
				});
				$('#dialog_amenity').dialog('open');
				initOpenDialog(amenity_id);
				hideLoading();
				$('.save_amenity_changes').click(function() {
					showLoading();
				});
				$('.cancel_amenity_changes').click(function() {
					$('#dialog_amenity').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
</script>