<style>
.k-tooltip{
	font-weight: bold !important;
	color: #FFFFFF !important;
}
</style>
<!-- DateRange Picker -->
<link href="<?php echo base_url(); ?>/assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>/assets/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/daterangepicker/statistics-daterangepicker.js"></script>

<!-- KENDO UI -->
<link href="<?php echo base_url(); ?>/assets/plugins/kendoui/styles/kendo.common.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>/assets/plugins/kendoui/styles/kendo.default.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/plugins/kendoui/js/kendo.all.min.js"></script>

<!-- Load modernizr first -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/libs/modernizr.js"></script>
<script>
$(document).ready(function() { 	
	//--------------- Data tables ------------------//
	if($('table').hasClass('MyDynamicTable')){
		$('.MyDynamicTable').dataTable( {
			// Sort by second colum
			"aaSorting":[[2, "desc"]], 
			"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
			"sPaginationType": "bootstrap",
			"bJQueryUI": false,
			"bAutoWidth": false,
			"iDisplayLength": (( typeof default_pagination !== 'undefined')? default_pagination: 10),
			"oLanguage": {
				"sSearch": "<span></span> _INPUT_",
				"sLengthMenu": "<span>_MENU_</span>",
				"oPaginate": { "sFirst": "First", "sLast": "Last" }
			}
		});
		$('.dataTables_length select').uniform();
		$('.dataTables_paginate > ul').addClass('pagination');
		$('.dataTables_filter>label>input').addClass('form-control');
		if(typeof default_pagination_url !== 'undefined'){
			$('.selector>select').on('change', function(){
				$.post(default_pagination_url,{
						default_pagination: $(this).val()
					});
			});
		}
	}
	$('.action_btn').click(function() {
		$('#actiontype_field').val($(this).attr('actionType'));
		if($(this).attr('type') == 'button'){
			$('#generalsettinsForm').submit();
		}
	});

    
});
</script>
