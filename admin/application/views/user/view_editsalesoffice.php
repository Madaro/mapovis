<?php
$base_url           = base_url().'salesteam/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					 <h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3> 
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4>
										<span class="icon16 entypo-icon-settings"></span>
										<span>Sales Office Settings</span>
									</h4>
								</div>
								<div class="panel-body">
								<?= $validation_msg;?>
								<form id="generalsettinsForm" method="post" class="form-horizontal" action="<?= $base_url; ?>editsalesoffice/<?= $development->development_id;?>" role="form" enctype="multipart/form-data">

								<div class="form-group">
									<label class="col-lg-3 control-label" for="required">Sales Office Title:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" id="sales_office_title" name="sales_office_title" value="<?= $development->sales_office_title;?>">
									</div>
								</div><!-- End .form-group-->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="required">Sales Office Address:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" id="sales_office_address" name="sales_office_address" value="<?= $development->sales_office_address;?>">
									</div>
								</div><!-- End .form-group-->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="required">Sales Telephone Number:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" id="sales_telephone_number" name="sales_telephone_number" value="<?= $development->sales_telephone_number;?>">
									</div>
								</div><!-- End .form-group-->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="required">Local Sales Telephone Number:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" id="local_sales_telephone_number" name="local_sales_telephone_number" value="<?= $development->local_sales_telephone_number;?>">
									</div>
								</div><!-- End .form-group-->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="required">Sales Email Address:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" id="sales_email_address" name="sales_email_address" value="<?= $development->sales_email_address;?>">
									</div>
								</div><!-- End .form-group-->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="required">Sales Person Name:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" id="sales_person_name" name="sales_person_name" value="<?= $development->sales_person_name;?>">
									</div>
								</div><!-- End .form-group-->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="required">Sales Office Opening Hours:</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" id="sales_office_opening_house" name="sales_office_opening_house" value="<?= $development->sales_office_opening_house;?>">
									</div>
								</div><!-- End .form-group-->

								<div class="col-lg-offset-2">
								 <br>
									<button type="submit" class="btn btn-info">Update</button>
									<a href="<?= $manage_development;?>" style="padding-left:10px;"><button type="button" class="btn btn-default">Cancel</button></a>
									<br><br>
								</div>
							</form>
							</div>

							</div><!-- End .panel -->

						</div><!-- End .span3 -->

				</div><!-- End .row --> 

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
