<?php
$base_url           = base_url().'salesteam/';
$formate_start_date = date('d-m-Y', strtotime($start_date));
$formate_end_date   = date('d-m-Y', strtotime($end_date));

/* LOT VIEWS */
$lot_views_total    = 0;
foreach($lot_views_by_lot as $lot_view){
	$lot_views_total += $lot_view->views;
}

$graph_lot_views       = array('lot_views' => array(),'categories' => array());
$graph_place_views     = array('place_views' => array(),'categories' => array());
$graph_direction_views = array('direction_views' => array(),'categories' => array());
foreach($report_dates as $date_index => $date_label){
	// LOTS
	$lot_formated_date = $date_label;
	if(isset($lot_views_by_date[$date_index])){
		$lot_views = $lot_views_by_date[$date_index]->views;
	}
	else{
		$lot_views = 0;
	}
	$graph_lot_views['lot_views'][]  = $lot_views;
	$graph_lot_views['categories'][] = $lot_formated_date;

	// PLACES
	$place_formated_date = $date_label;
	if(isset($place_search_views_by_date[$date_index])){
		$place_views = $place_search_views_by_date[$date_index]->views;
	}
	else{
		$place_views = 0;
	}
	$graph_place_views['place_views'][] = $place_views;
	$graph_place_views['categories'][]  = $place_formated_date;

	// DIRECTIONS
	$direction_formated_date = $date_label;
	if(isset($direction_search_views_by_date[$date_index])){
		$direction_views = $direction_search_views_by_date[$date_index]->views;
	}
	else{
		$direction_views = 0;
	}
	$graph_direction_views['direction_views'][] = $direction_views;
	$graph_direction_views['categories'][]      = $direction_formated_date;
}

/* PLACES SEARCHED */
$place_views_total    = 0;
foreach($place_search_views_by_phrase as $phrase_view){
	$place_views_total += $phrase_view->views;
}

/* DIRECTIONS SEARCHED */
$direction_views_total    = 0;
foreach($direction_search_views_by_phrase as $phrase_view){
	$direction_views_total += $phrase_view->views;
}
?> 
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Statistics for <?= $development->development_name;?> (<?= $development->development_name;?>)</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-calendar-2"></span>
									<span>Date Range for Statistics Report</span>
								</h4>
							</div>

							<form id="generalsettinsForm" method="post" class="form-horizontal" action="<?= $base_url; ?>statisticsreport/<?= $development->development_id;?>" role="form">
							<div class="panel-body">
								<div id="date-range-container" style="margin-left: auto; margin-right: auto;"></div>
								<br/>
								<input name="dates" id="date-range" size="40" value="<?= $start_date?> to <?= $end_date?>" style="display: none;">
								<br/>
								<input type="hidden" id="actiontype_field" name="action_type" value="submit"/>
								<button type="submit" actionType="submit" class="action_btn btn btn-default">Refresh Statistics</button>
							</div>
							</form>
						</div><!-- End .panel -->
					</div><!-- End .span8 -->
				</div><!-- End .row -->

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-stats"></span>
									<span>GRAPH of Lot Views - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
								</h4>
							</div>

							<div class="panel-body">
								<?php
								$lotviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
								$lotviews->name('Lot Views')->data($graph_lot_views['lot_views']);

								$valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();

								$categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
								$categoryAxis->categories($graph_lot_views['categories'])->majorGridLines(array('visible' => false)); 
								$categoryAxis->labels(array('rotation' => -45));

								$tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
								$tooltip->visible(true)
										->format('{0}%')
										->template('#= series.name #: #= value #');


								$legend       = new \Kendo\Dataviz\UI\ChartLegend();
								$legend->visible(false);

								$lotchart     = new \Kendo\Dataviz\UI\Chart('lotchart');
								$lotchart->legend($legend)
									->addSeriesItem($lotviews)
									->addValueAxisItem($valueAxis)
									->addCategoryAxisItem($categoryAxis)
									->tooltip($tooltip)
									->chartArea(array('background' => 'transparent'))
									->seriesColors(array("orange"))
									->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

								echo $lotchart->render();
								?>
							</div>

						</div><!-- End .panel -->
					</div><!-- End .span8 -->
				</div><!-- End .row -->

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h3>
									<button type="button" actionType="export_lotviews" class="action_btn btn btn-primary" style="float:right;margin-right:20px;margin-top:5px;">Export CSV</button>
								</h3>
								<h4>
								   <span class="icon16 icomoon-icon-stats-2"></span>
									<span>Lot Views - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
								</h4>
								</span>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Views</th>
											<th>Percentage</th>
											<th>Lot Number</th>
											<th>Stage</th>
											<th>Precinct</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($lot_views_by_lot as $lot_view):?>
										<tr>
											<td><?= $lot_view->views;?></td>
											<td><?= ($lot_views_total)? round($lot_view->views / $lot_views_total * 100, 1): 0;?>%</td>
											<td><?= $lot_view->lot_number;?></td>
											<td><?= $lot_view->stage_number;?></td>
											<td><?= $lot_view->precinct_number;?></td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span12 -->
				</div><!-- End .row -->

				<br/><br/>

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-stats"></span>
									<span>Places Searches - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
								</h4>
							</div>
							<div class="panel-body">
								<?php
								$placessearches = new \Kendo\Dataviz\UI\ChartSeriesItem();
								$placessearches->name('Places Searches')->data($graph_place_views['place_views']);

								$valueAxis      = new \Kendo\Dataviz\UI\ChartValueAxisItem();

								$categoryAxis   = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
								$categoryAxis->categories($graph_place_views['categories'])->majorGridLines(array('visible' => false)); 
								$categoryAxis->labels(array('rotation' => -45));

								$tooltip        = new \Kendo\Dataviz\UI\ChartTooltip();
								$tooltip->visible(true)
										->format('{0}%')
										->template('#= series.name #: #= value #');

								$legend         = new \Kendo\Dataviz\UI\ChartLegend();
								$legend->visible(false);

								$placeschart    = new \Kendo\Dataviz\UI\Chart('placeschart');
								$placeschart->legend($legend)
									->addSeriesItem($placessearches)
									->addValueAxisItem($valueAxis)
									->addCategoryAxisItem($categoryAxis)
									->tooltip($tooltip)
									->chartArea(array('background' => 'transparent'))
									->seriesColors(array("crimson"))
									->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

								echo $placeschart->render();
								?>
							</div>

						</div><!-- End .panel -->
					</div><!-- End .span8 -->
				</div><!-- End .row -->    
				<!-- Page end here -->

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h3>
									<button type="button" actionType="export_placesearch" class="action_btn btn btn-primary" style="float:right;margin-right:20px;margin-top:5px;">Export CSV</button>
								</h3>
								<h4>
									<span class="icon16 icomoon-icon-stats-2"></span>
									<span>Places Searches - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="MyDynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Place</th>
											<th>Percentage</th>
											<th>Searches</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($place_search_views_by_phrase as $phrase_view):?>
										<tr>
											<td><?= $phrase_view->search_phrase?></td>
											<td><?= ($place_views_total)? round($phrase_view->views/$place_views_total *100, 1): 0?>%</td>
											<td><?= $phrase_view->views?></td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->
				<br/><br/>

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-stats"></span>
									<span>Directions Searches - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
								</h4>
							</div>
							<div class="panel-body">
								<?php
								$directionssearches = new \Kendo\Dataviz\UI\ChartSeriesItem();
								$directionssearches->name('Directions Searches')->data($graph_direction_views['direction_views']);

								$valueAxis          = new \Kendo\Dataviz\UI\ChartValueAxisItem();

								$categoryAxis       = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
								$categoryAxis->categories($graph_direction_views['categories'])->majorGridLines(array('visible' => false)); 
								$categoryAxis->labels(array('rotation' => -45));

								$tooltip            = new \Kendo\Dataviz\UI\ChartTooltip();
								$tooltip->visible(true)
										->format('{0}%')
										->template('#= series.name #: #= value #');

								$legend             = new \Kendo\Dataviz\UI\ChartLegend();
								$legend->visible(false);

								$directionschart    = new \Kendo\Dataviz\UI\Chart('directionschart');
								$directionschart->legend($legend)
									->addSeriesItem($directionssearches)
									->addValueAxisItem($valueAxis)
									->addCategoryAxisItem($categoryAxis)
									->tooltip($tooltip)
									->chartArea(array('background' => 'transparent'))
									->seriesColors(array("darkolivegreen"))
									->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

								echo $directionschart->render();
								?>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span8 -->
				</div><!-- End .row --> 

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h3>
									<button type="button" actionType="export_direcsearch" class="action_btn btn btn-primary" style="float:right;margin-right:20px;margin-top:5px;">Export CSV</button>
								</h3>
								<h4>
								   <span class="icon16 icomoon-icon-stats-2"></span>
									<span>Directions Searches - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
								</h4>
								</span>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="MyDynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Address</th>
											<th>Percentage</th>
											<th>Searches</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($direction_search_views_by_phrase as $phrase_view):?>
										<tr>
											<td><?= $phrase_view->direction_address;?></td>
											<td><?= ($direction_views_total)? round($phrase_view->views / $direction_views_total * 100, 1): 0?>%</td>
											<td><?= $phrase_view->views;?></td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->
					</div><!-- End .span12 -->
				</div><!-- End .row -->
				<br/><br/><br/>
				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
