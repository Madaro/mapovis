<?php
$base_url           = base_url().'salesteam/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
					<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="well well-lg">
							<h4>You are due to update this development<?= $development->overdue? '. '.$development->remaining_time: ' on '.$development->next_schedule_date;?>.</h4>
							<?php if(($development->remaining_hours && $development->remaining_hours <= 8) || $development->overdue):?>
								<br>
								<a href="<?= $base_url?>noupdates/<?= $development->development_id;?>"><button class="btn btn-primary">No Updates Required</button></a>
							<?php endif;?>
						</div>
					</div><!-- End .span4 -->

					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span><?= $development->development_name;?> Lots</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($lots)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Precinct</th>
											<th>Stage</th>
											<th>Lot #</th>
											<th>Status</th>
											<th>Price Range</th>
											<th>Titled</th>
											<th></th>
											</tr>
											<!-- START - Modification by Seb : Adding Column Filtering for DataTables -->
											<tr>
											  <td><input type="text" name="search_precinct" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_stage" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_lot_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_status" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_price_ranges" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_titled" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td></td>
											</tr>
											<!-- END - Modification by Seb -->
									</thead>
									<tbody>
										<?php foreach($lots as $lot):?>
										<tr>
											<td style=""><?= $lot->precinct_number;?></td>
											<td style=""><?= $lot->stage_code;?></td>
											<td style=""><?= $lot->lot_number;?></td>
											<td style=""><?= $lot->status;?></td>
											<td style=""><?= '$'.number_format($lot->price_range_min, 0).' to $'.number_format($lot->price_range_max, 0);?></td>
											<td style=""><?= ($lot->lot_titled)? 'Yes': 'No';?></td>
											<td class="center" style="">
												<button id="update_<?= $lot->lot_id;?>" title="Precinct <?= $lot->precinct_number;?> | Stage <?= $lot->stage_number;?> | Lot <?= $lot->lot_number;?>" lot_id="<?= $lot->lot_id;?>" class="update_btns btn btn-xs btn-default">Update</button>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
								<div class="panel-body">
									<div class="alert alert-warning">There are not lots for this development in the system yet.</div>
								</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

	<!-- Dialog -->
	<div id="dialog_lot"></div>