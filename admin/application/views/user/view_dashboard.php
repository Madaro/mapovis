<?php
$base_url = base_url().'salesteam/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Dashboard</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					

					<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4>
										<span class="icon16 icomoon-icon-home-7"></span>
										<span>Your Developments</span>
									</h4>
								</div>
								<div class="panel-body noPad">
									<table class="table table-bordered">
										<thead>
										<tr>
											<th>Development</th>
											<th>Next Update</th>
											<th>Remaining Time</th>
											<th>H&L Awaiting Validation</th>
											<th></th>
										</tr>
										</thead>
										<tbody>
											<?php foreach($my_developments as $my_development):?>
											<tr>
												<td><?= $my_development->development_name;?></td>
												<td><span class="<?= $my_development->remaining_class;?>"><?= $my_development->next_schedule_date;?></span></td>
												<td><span class="<?= $my_development->remaining_class;?>"><?= $my_development->remaining_time;?></span></td>
												<td><?php if(isset($pending_packages[$my_development->development_id])):?>
													<a href="<?php echo $base_url; ?>packagesforapproval/<?= $my_development->development_id;?>"><button class="btn btn-warning btn-xs" href="#">Validate <?= $pending_packages[$my_development->development_id];?></button></a>
													<?php else:?>
													<button class="btn-disable btn btn-xs" href="#">None</button>
													<?php endif;?>
												</td>
												<td>
													<div class="controls center">
														<a href="<?php echo $base_url; ?>updatedevelopmentid/<?= $my_development->development_id;?>"><button class="btn <?= ($my_development->remaining_class == 'overdue')? 'btn-danger': 'btn-'.($my_development->remaining_class? $my_development->remaining_class: 'info');?> btn-xs" href="#">Update</button></a>

														<a href="<?php echo $base_url; ?>managedevelopmentid/<?= $my_development->development_id;?>"  style="padding-left:10px;"><button class="btn btn btn-info btn-xs" href="#">Manage</button></a>
													</div>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span6 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
