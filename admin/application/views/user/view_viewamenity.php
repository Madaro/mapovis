<?php
$base_url       = base_url().'salesteam/';
$image_base_url = base_url().'../mpvs/images/amenities/';
$amenity_id     = $amenity->amenity_id;
?>
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<!-- Image field necesary to call Aviary and crop the images -->
	<img id='imageupload' src='' style="display:none"/>
	<div class="col-lg-9">
		<div class="panel-body">
			<?php if(!$media_agency && !empty($amenity->amenity_iframe_html)):?>
				<h4><?= $amenity->amenity_name; ?></h4>
				<h5>This content is being managed by a custom web page. To make changes please contact your media agency</h5>
				<div class="form-group" style="padding-top:10px">
					<div class="col-lg-offset-8 col-lg-9">
						<button type="button" class="cancel_amenity_changes btn btn-default" style="margin-left: 10px;">Close</button>
					</div>
				</div><!-- End .form-group  -->
			<?php else:?>
			<form method="post" class="form-horizontal" action="<?= $base_url; ?>updateamenity/<?= $amenity_id;?>" role="form">
				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Name:</label>
					<div class="col-lg-9">
					<input name="amenity_name" type="text" class="form-control" value="<?= $amenity->amenity_name; ?>">
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Description:</label>
					<div class="col-lg-9">
						<div class="form-row">
							<textarea class="tinymce" name="amenity_description"><?= $amenity->amenity_description;?></textarea>
						</div>
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">More Info Link:</label>
					<div class="col-lg-9">
					<input name="amenity_moreinfo_url" type="text" class="form-control" value="<?= $amenity->amenity_moreinfo_url;?>">
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Pictures:</label>
					<div class="col-lg-9">
						<div id="add_image_section<?= $amenity_id;?>">
							<input type="file" name="image_file" id="image_file<?= $amenity_id;?>" class="image_files" value="" accept="image/*" >
							<button type="button" id="add_image_btn" class="add_image_btn btn btn-success" image_field="image_file<?= $amenity_id;?>" amenity_id="<?= $amenity_id;?>" href="#">Add & Edit Photo</button>
							<br/><br/>
						</div>

						<div id="qLoverlaymessage_<?= $amenity_id;?>" class="qLoverlaymessage" style="display:none;"></div>
						<div id="qLmessage_<?= $amenity_id;?>" class="qLmessage" style="display:none;"></div>
						<div class="alert alert-info">
							Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
						</div>
						<div id="img_container">
							<ul class="sortable" id="sortable_<?= $amenity_id;?>">
								<?php for($xyz = 1; $xyz <= 5; $xyz++):
									$image_field = 'amenity_picture'.$xyz;
									$image_name  = $amenity->$image_field;
									if(empty($image_name)){
										break;
									}
									$image_url   = "{$image_base_url}{$image_name}";
								?>
									<li class="ui-state-default" >
										<input name="image_names[]" type="text" value="<?= $image_name;?>" style="display:none">
										<img src="<?= $image_url;?>" width="135" height="96" />
										<button type="button" amenity_id="<?= $amenity_id;?>" image_id="<?= $image_name?>" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button>
									</li>
								<?php endfor;?>
							</ul>
						</div>
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group" style="padding-top:10px">
					<div class="col-lg-offset-3 col-lg-9">
						<button type="submit" class="save_amenity_changes btn btn-info">Save Changes</button>
						<button type="button" class="cancel_amenity_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
					</div>
				</div><!-- End .form-group  -->

				<?php if($media_agency):?>
					<div class="alert alert-warning">If you want to use a completely custom design you can provide an IFrame.</div>
					<div class="form-group">
						<label class="col-lg-3 control-label" for="required">Amenities Iframe HTML:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="amenity_iframe_html" value="<?= htmlspecialchars($amenity->amenity_iframe_html, ENT_QUOTES);?>">
						</div>
					</div><!-- End .form-group-->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="required">Show Scrollbar:</label>
						<div class="col-lg-5">
							<input type="radio" name="amenity_iframe_scrollbar" value="1" <?= ($amenity->amenity_iframe_scrollbar == 1? 'checked="checked"': '');?> id="amenity_iframe_scrollbar1" class="amenity_iframe_scrollbar"><label for="amenity_iframe_scrollbar1"> On </label> &nbsp;&nbsp;
							<input type="radio" name="amenity_iframe_scrollbar" value="0" <?= ($amenity->amenity_iframe_scrollbar == 0? 'checked="checked"': '');?> id="amenity_iframe_scrollbar0" class="amenity_iframe_scrollbar"><label for="amenity_iframe_scrollbar0"> Off </label>
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group" style="padding-top:10px">
						<div class="col-lg-offset-3 col-lg-9">
							<button type="submit" class="save_amenity_changes btn btn-info">Save Changes</button>
							<button type="button" class="cancel_amenity_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
						</div>
					</div><!-- End .form-group  -->
				<?php endif;?>

			</form>
			<?php endif;?>

		</div>
	</div><!-- End .span6 -->
