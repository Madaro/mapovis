<?php
$base_url           = base_url().'salesteam/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
				<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-documents"></span>
									<span>Update Stage</span>
								</h4>
							</div>

							<div class="panel-body">
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>updatestage/<?= $stage->stage_id;?>" role="form" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-lg-2 control-label">Precint #:</label>
									<div class="col-lg-2">
										<?= $stage->precinct_number;?>
									</div>
								</div><!-- End .form-group  -->

								<?php if($media_agency):?>
								<div class="form-group">
									<label class="col-lg-2 control-label">Stage #:</label>
									<div class="col-lg-2">
										<?= $stage->stage_number;?>
									</div>
								</div><!-- End .form-group  -->
								<?php else:?>
								<div class="form-group">
									<label class="col-lg-2 control-label">Stage Code:</label>
									<div class="col-lg-2">
										<?= $stage->stage_code;?>
									</div>
								</div><!-- End .form-group  -->
								<?php endif;?>

								<div class="form-group">
									<label class="col-lg-2 control-label" >Stage Name:</label>
									<div class="col-lg-3">
										<input name="stage_name" type="text" class="form-control" value="<?= $stage->stage_name;?>">
									</div>
								</div><!-- End .form-group  -->

								<?php if($media_agency):?>
								<div class="form-group">
									<label class="col-lg-2 control-label" >Stage Code:</label>
									<div class="col-lg-3">
										<input name="stage_code" type="text" class="form-control" value="<?= $stage->stage_code;?>">
									</div>
								</div><!-- End .form-group  -->
								<?php endif;?>

								<div class="form-group">
									<label class="col-lg-2 control-label" >Title Release:</label>
									<div class="col-lg-3">
										<input name="title_release" type="text" class="form-control" value="<?= $stage->title_release;?>">
									</div>
								</div><!-- End .form-group  -->

								<?php if($media_agency):?>
								<div class="form-group">
									<label class="col-lg-2 control-label">Stage PDF File:</label>
									<div class="col-lg-9">
										<div id="lots_pdf_file">
											<input type="file" name="stage_pdf_file" id="stage_pdf_file_field" value="" accept="application/pdf" >
											<?php if($stage->stage_pdf_file):?>
												<span id="current_file">
													<a href="<?= base_url().'../mpvs/templates/dev_stage_pdfs/'.$stage->stage_pdf_file;?>" title="Download File" download><span class="icon16 icomoon-icon-file-pdf"></span></a>
												</span>
												</br></br>
											<?php endif;?>
										</div>
									</div>
								</div><!-- End .form-group  -->
								<?php endif;?>

								<div class="col-lg-offset-2">
								 <br>
									<button type="submit" class="btn btn-info">Update</button>
									<a href="<?= $base_url.'viewstages/'.$stage->development_id; ?>"><button type="button" class="btn btn-default">Cancel</button></a>
									<br><br>
								</div>
								</form>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span3 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
