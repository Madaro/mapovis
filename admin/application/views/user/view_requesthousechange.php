<?php
$base_url       = base_url().'salesteam/';
?>
<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
<!-- Dialog -->
<div class="col-lg-11">
	<div class="panel-body">
		<form method="post" class="form-horizontal" action="<?= $base_url; ?>submitchangerequest/<?= $house_id;?>/<?= $development_id;?>" role="form">
			<div class="form-group">
				<label class="col-lg-12">If you believe that the information regarding this house is incorrect you can use this form to request that it is updated</label>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-2 control-label">House:</label>
				<div class="col-lg-10">
					<?= $house->house_name;?> by <?= $house->builder_name;?>
				</div>
			</div><!-- End .form-group  -->

			<?php if(!empty($house->house_description)):?>
			<div class="form-group">
				<label class="col-lg-2 control-label">Description:</label>
				<div class="col-lg-10">
					<?= $house->house_description;?>
				</div>
			</div><!-- End .form-group  -->
			<?php endif;?>

			<div class="form-group">
				<label class="col-lg-2 control-label">Change(s):</label>
				<div class="col-lg-10">
					<div class="form-row">
						<textarea class="tinymce" name="request_message"></textarea>
					</div>
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group" style="padding-top:10px">
				<div class="col-lg-offset-2 col-lg-10">
					<button id="" type="submit" class="btn btn-info" >Submit Request</button>
					<button id="cancel_house_changes" type="button" class="btn btn-default" style="margin-left:10px">Cancel</button>
				</div>
			</div><!-- End .form-group  -->

		</form>
	</div>
</div><!-- End .span6 -->
