<style>
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 500px; }
	.sortable li { margin: 5px 30px 15px 0; padding: 0px; float: left; width: 136px; height: 131px;  text-align: center; }
	.deleteimage{margin-top: 7px;}
	#img_container{overflow-y:hidden;}
	.ui-dialog { z-index: 65535; }
</style>
<script type="text/javascript" src="http://feather.aviary.com/js/feather.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
var featherEditor = new Aviary.Feather({
	apiKey: '84495f70872a7572',
	apiVersion: 3,
	tools: 'all',
	theme: 'light',
	onError: function(errorObj) {
		console.log(errorObj.message);
	}
});
/* Show and hide "Add Image" when necessary MAX 5 Images */
function showhide_AddNew(amenity_id){
	var total_images     = $('#sortable_'+amenity_id).children().length;
	var container_height = Math.ceil(total_images/3) * 145;
	$('#img_container').attr('style', 'height:'+container_height+'px;width:505px;')
	if(total_images < 5){
		$('#add_image_section'+amenity_id).show();
	}
	else{
		$('#add_image_section'+amenity_id).hide();
	}
	// point user to 'Save Changes' button
	$('#save_changes'+amenity_id).focus();
}
function initViewEntity(){
	$(function(){
		$('.sortable').sortable({containment: "#img_container",tolerance: "pointer"});
		$('.sortable').disableSelection();
	});
	$('.image_files').on('change', function(){
		var input = $(this)[0];
		console.log(input.files[0])
		if (input.files && input.files[0]){
			if(!validateimagefiletype(input.files[0].type)){
				return false;
			}
		}
	});
	$('.deleteimage').on('click', function(e){
		deleteImageFunction($(this), true);
	});
}
</script>