<?php
$report_url = base_url().'salesteam/'.$actionview.'/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3><?= $titlelabel;?></h3>
				</div><!-- End .heading-->
				<?= $alert_message;?>

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default gradient">
								<div class="panel-heading">
									<h4>
										<span>Developments</span>
									</h4>
								</div>
								<div class="panel-body noPad clearfix">
									<?php if (empty($developments)):
										echo 'No results found';?>
									<?php else:?>
									<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
										<thead>
											<tr>
												<th>Development</th>
												<th>Developer</th>
												<th>State</th>
												<th></th>
											</tr>
											<!-- START - Modification by Seb : Adding Column Filtering for DataTables -->
											<tr>
												<td><input type="text" name="search_dev" placeholder="Search Developments" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_dever" placeholder="Search Developers" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_state" placeholder="Search States" class="search_init" style="width: 100%;" /></td>
												<td></td>
											</tr>
											<!-- END - Modification by Seb -->
										</thead>
										<tbody>
											<?php foreach($developments as $development):?>
											<tr>
												<td style=""><?= $development->development_name;?></td>
												<td style=""><?= isset($developers[$development->developerid])? $developers[$development->developerid]->developer: 'N/A';?></td>
												<td class="center"><?= $development->state;?></td>
												<td class="center" style="">
													<a href="<?= $report_url.$development->development_id;?>"><button id="opener1" class="btn btn-xs btn-default">View Statistics</button></a>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
									<?php endif;?>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span12 -->

					</div><!-- End .row -->

					<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
