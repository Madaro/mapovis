<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
$('#dialog_lot').dialog({ autoOpen: false });
$('.update_btns').on('click', function(){
	var lot_id = $(this).attr('lot_id');
	var title  = $(this).attr('title');
	showLoading();
	$.post('<?= base_url();?>salesteam/viewlot/'+lot_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_lot').html(data);
				$('#dialog_lot').dialog({
					title: title,
					height: 280,
					width: 600,
					modal: true,
					resizable: false,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_lot').html('');}
				});
				$('#dialog_lot').dialog('open');
				hideLoading();
				$('#save_lot_changes').click(function() {
					showLoading();
				});
				$('#cancel_lot_changes').click(function() {
					$('#dialog_lot').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
</script>