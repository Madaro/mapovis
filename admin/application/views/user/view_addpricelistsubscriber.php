<?php
$base_url           = base_url().'salesteam/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
$amenity_id         = 0;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Add Price List Subscriber</span>
								</h4>
							</div>
							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form id="add_subscriber_form" method="post" class="form-horizontal" action="<?= $base_url; ?>addpricelistsubscriber/<?= $development->development_id;?>" role="form">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Name:</label>
									<div class="col-lg-4">
										<input type="text" name="subscriber_name" id="subscriber_name" class="form-control" value="<?= set_value('subscriber_name')?>" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Company:</label>
									<div class="col-lg-4">
										<input type="text" name="subscriber_company" class="form-control" value="<?= set_value('subscriber_company')?>" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Email Address:</label>
									<div class="col-lg-4">
										<input type="text" name="subscriber_email" id="subscriber_email" class="form-control" value="<?= set_value('subscriber_email')?>" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Office Phone:</label>
									<div class="col-lg-4">
										<input type="text" name="office_phone" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Mobile Phone:</label>
									<div class="col-lg-4">
										<input type="text" name="mobile_phone" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Job Title:</label>
									<div class="col-lg-4">
										<input type="text" name="job_title" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group" style="padding-top:10px">
									<div class="col-lg-offset-1 col-lg-9">
										<span><button id="action_continue_btn" type="submit" class="btn btn-primary save_new_amenity">Add Price List Subscriber & Continue</button></span>
										<span style="padding-left: 10px;"><button id="action_btn" type="submit" class="btn btn-info save_new_amenity">Add Price List Subscriber & Finish</button></span>
										<span style="padding-left: 10px;"><a href="<?= $base_url.'managepricelistsubscribers/'.$development->development_id; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
									</div>
								</div><!-- End .form-group  -->
								<input id="action_continue" name="action_continue" type="hidden" value="0">
							</form>
							</div><!-- End .body -->

						</div><!-- End .panel -->
					</div><!-- End .span1 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/validate/jquery.validate.min.js"></script>
<script type="text/javascript">
	$("#add_subscriber_form").validate({
		rules: {
			subscriber_name: {
				required: true
			},
			subscriber_email: {
				required: true,
				email: true
			}
		},
		messages: {
			subscriber_name: {
				required: 'Please provide the name'
			},
			subscriber_email: {
				required: 'Please provide an email address',
				email:"Please privde a valid email address"
			}
		}
	});
</script>