<script>
	$('.view_house_btn').on('click', function(){
		var development_id = $(this).attr('development_id');
		var dev_house = $(this).attr('dev_house');
		var house_id = $(this).attr('house_id');
		var title = $(this).attr('title');
		showLoading();
		$.post('<?= base_url();?>salesteam/viewhouse/'+house_id+'/'+development_id,
			function(data,status){
				if(status === 'success'){
					$('#dialog_house').html(data);
					$('#dialog_house').dialog({
						title: title,
						height: 600,
						width: 1000,
						modal: true,
						resizable: false,
						dialogClass: 'loading-dialog',
						close: function(ev,ui){remove_tinymce();$('#dialog_house').html('');}
					});
					$('#dialog_house').dialog('open');
					initilizeHouseView(dev_house, house_id);
					hideLoading();
					$('#cancel_house_changes').click(function() {
						$('#dialog_house').dialog('close');
					});
					return true;
				}
				messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
		});
	});
	$('.request_change_house_btns').on('click', function(){
		var development_id = $(this).attr('development_id');
		var house_id = $(this).attr('house_id');
		var title = $(this).attr('title');
		showLoading();
		$.post('<?= base_url();?>salesteam/requesthousechange/'+house_id+'/'+development_id,
			function(data,status){
				if(status === 'success'){
					$('#dialog_house').html(data);
					$('#dialog_house').dialog({
						title: title,
						height: 630,
						width: 1000,
						modal: true,
						resizable: false,
						dialogClass: 'loading-dialog',
						close: function(ev,ui){$('#dialog_house').html('');}
					});
					$('#dialog_house').dialog('open');
					initilizeHouseView(false, house_id, 240);
					hideLoading();
					$('#cancel_house_changes').click(function() {
						$('#dialog_house').dialog('close');
					});
					return true;
				}
				messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
		});
	});
	$('.delete_house_btns').click(function() {
		var house_name = $(this).attr('house_name');
		if(!confirm('Are you sure you want to delete the house "'+house_name+'"?')){
			return false;
		}
		showLoading();
	});
</script>