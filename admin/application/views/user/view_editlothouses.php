<?php
$base_url           = base_url().'salesteam/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$manage_lots        = $base_url.'managehousematches/'.$development->development_id;
$disabled_houses    = array();
$enabled_houses     = array();
foreach($houses as $house){
	if(!isset($lot_house_packages[$house->house_id]) || $lot_house_packages[$house->house_id]->active == 0){
		$disabled_houses[$house->house_id] = $house;
	}
	else{
		$enabled_houses[$house->house_id]  = $house;
	}
}
?>
<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
				</div><!-- End .heading-->

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Enabled Packages For Lot <?= $lot->lot_number;?> </span>
								</h4>
							</div>
							<div class="pznel-body noPad clearfix">
								<table id="enabled_table" cellpadding="0" cellspacing="0" border="0" class="enable_green display table table-bordered" width="100%">
									<thead>
										<tr>
											
											<th>House Name</th>
											<th>Builder</th>
											<th>Price</th>
											<th>PDF</th>
											<th></th>
											<th></th>
										</tr>
										<tr>
											
											<td><input type="text" name="search_house_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_builder" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_price" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($enabled_houses as $house):?>
										<?php $house_package = $lot_house_packages[$house->house_id];?>
										<tr>
											
											<td id="house_id_<?= $house->house_id; ?>"><?= $house->house_name; ?></td>
											<td><?= $house->builder_name; ?></td>
											<td><?= ($house_package->house_lot_price)? '$'.number_format($house_package->house_lot_price): 'n/a'; ?></td>
											<td>
												<?php if(!empty($house_package->file_name)):?>
													<a title="Download File" href="<?= base_url().'../mpvs/templates/lot_house_pdfs/'.$house_package->file_name;?>" 
													   target="_blank">
														<span class="icon16 icomoon-icon-file-pdf"></span>
													</a>
												<?php endif?>
											</td>
											<td class="center">
												<button house_id="<?= $house->house_id;?>" class="update_package btn btn-xs btn-default">Edit</button>
											</td>
											<td>
												<button house_id="<?= $house->house_id;?>" class="disable_house_package btn btn-xs btn-danger">Disable</button>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Houses</span>
								</h4>
							</div>
							<div class="pznel-body noPad clearfix">
								<table id="disabled_table" cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" width="100%">
									<thead>
										<tr>
											
											<th>House Name</th>
											<th>Builder</th>
											<th>Type</th>
											<th>Packages</th>
										</tr>
										<tr>
											
											<td><input type="text" name="search_house_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_builder" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_type_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($disabled_houses as $house):?>
										<?php $house_package = (isset($lot_house_packages[$house->house_id]))? $lot_house_packages[$house->house_id]: FALSE;?>
										<tr>
											
											<td><?= $house->house_name; ?></td>
											<td><?= $house->builder_name; ?></td>
											<td><?= ($house->development_id)? 'Custom House':'Global House'; ?></span></td>
											<td>
												<?php $add_label = ($house_package)? 'Re-Enable': 'Enable';?>
												<button house_id="<?= $house->house_id;?>" class="enable_house_package btn btn-xs btn-success"><?= $add_label;?></button>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<!-- Dialog -->
<div id="dialog_lot_house"></div>
<div id="confirm_alert_dialog"></div>