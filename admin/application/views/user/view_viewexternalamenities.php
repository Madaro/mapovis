<?php
$base_url           = base_url().'salesteam/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$image_base_url     = base_url().'../mpvs/images/external_amenities/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-location-2"></span>
									<span><?= $development->development_name;?> External Amenities</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($external_amenities)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Name</th>
											<th>Type</th>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($external_amenities as $amenity):?>
										<tr>
											<td><?= $amenity->e_amenity_name;?></td>
											<td><?= $amenity->e_amenity_type_name;?></td>
											<td class="center">
												<a href="<?= $base_url; ?>../../mapovis/development.php?developmentId=<?= $development->development_id;?>&zoom_to_latitude=<?= ((float)$amenity->e_amenity_latitude);?>&zoom_to_longitude=<?= ((float)$amenity->e_amenity_longitude);?>&zoom_level=19" target="_blank"><button type="button" class="btn btn-xs btn-default">Zoom to Amenity</button></a>
											</td>
											<td class="center">
												<button amenity_id="<?= $amenity->external_amenity_id;?>" class="btn btn-xs btn-default view_amenity_btn">Update</button>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not external amenities in the system.</div>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Image field necesary to call Aviary and crop the images -->
<img id='imageupload' src='' style="display:none"/>
<!-- Dialog -->
<div id="dialog_amenity"></div>