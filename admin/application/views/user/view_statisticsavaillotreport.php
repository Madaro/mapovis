<?php
$base_url     = base_url().'admin/reports/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<?php if(empty($available_lots_by_width['results'])):?>
					<div class="heading">
						<h3>
							<span class="icon16 icomoon-icon-stats"></span>
							<span><?= $development->development_name;?> Available Lots On Hand by Frontage</span>
						</h3>
					</div><!-- End .heading-->
					<h4>
						No results found.
					</h4>
				<?php else:?>
				<div class="heading">
					<div class="col-lg-12">
						<div class="header_btn">
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>availlotstatisticsreport/<?= $development->development_id;?>" role="form">
								<button type="submit" class="btn btn-primary" style="float:right;">Export CSV</button>
								<input type="hidden" name="export_csv" value="1">
							</form>
						</div>
					</div>
					<h3></h3>
				</div><!-- End .heading-->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 align="center">
									<span class="icon16 icomoon-icon-stats"></span>
									<span><?= $development->development_name;?> Available Lots On Hand by Frontage</span>
								</h4>
							</div>
							<div class="panel-body">
								<?php
								$availlotchart     = new \Kendo\Dataviz\UI\Chart('availablelotchart');

								$avail_legend       = new \Kendo\Dataviz\UI\ChartLegend();
								$avail_legend->visible(true);
								$availlotchart->legend($avail_legend);
								$availlotchart->chartArea(array('background' => 'transparent'));

								$avail_valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();
								$availlotchart->addValueAxisItem($avail_valueAxis);

								$available_categ = array_map(function ($str) { return date('M Y', strtotime('-1 month', strtotime($str))); }, array_keys($available_lots_by_width['results']));
								$avail_categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
								$avail_categoryAxis->categories($available_categ)->majorGridLines(array('visible' => false));
								$availlotchart->addCategoryAxisItem($avail_categoryAxis);

								$avail_tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
								$avail_tooltip->visible(true);
								$avail_tooltip->format('{0}%');
								$avail_tooltip->template('#= series.name #: #= value #');
								$availlotchart->tooltip($avail_tooltip);

								foreach($available_lots_by_width['widths'] as $width){
									$graph_available_lot_width = array();

									foreach($available_lots_by_width['results'] as $month => $month_results){
										$graph_available_lot_width[] = (isset($month_results[$width]->number_lots))? (int)$month_results[$width]->number_lots: 0;
									}
									$avail_lotviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
									$avail_lotviews->name($width.'m')->data($graph_available_lot_width);
									$availlotchart->addSeriesItem($avail_lotviews);
								}
								$availlotchart->seriesColors($chart_colors);
								$availlotchart->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

								echo $availlotchart->render();
								?>
							</div>
							<br>
							<div><h5 align="center">*For the purposes of this graph, lots are grouped together to the closest 0.5m frontage.</h5></div>

						</div><!-- End .panel -->
					</div><!-- End .span8 -->
				</div><!-- End .row -->    
				<!-- Page end here -->

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-stats-2"></span>
									<span><?= $development->development_name;?> Available Lots On Hand by Frontage</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dateDynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Month</th>
											<th>Sortable Month</th>
											<?php foreach($available_lots_by_width['widths'] as $width):?>
												<th><?php echo $width;?>m</th>
											<?php endforeach;?>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($available_lots_by_width['results'] as $month => $month_results):?>
										<?php $total_lots = 0;?>
										<tr>
											<!-- Showing the date a month back because it covers the totals of the previous month -->
											<td><?= date('M Y', strtotime('-1 month', strtotime($month)));?></td>
											<td><?= date('Ym', strtotime('-1 month', strtotime($month)));?></td>
											<?php foreach($available_lots_by_width['widths'] as $width):?>
												<?php $lot_views   = (isset($month_results[$width]->number_lots))? (int)$month_results[$width]->number_lots: 0;?>
												<?php $total_lots += $lot_views;?>
												<td><?= $lot_views;?></td>
											<?php endforeach;?>
											<td><b><?= $total_lots;?></b></td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->
				<?php endif;?>
				<!-- Page end here -->
			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script type="text/javascript">
	$(document).ready(function() { 	
	if($('table').hasClass('dateDynamicTable')){
		$('.dateDynamicTable').dataTable( {
			// Sort by second colum
			"aoColumns": [
				{"iDataSort": 1},
				{"bVisible": false},
				<?php for($x = 1; $x <= count($available_lots_by_width['widths']); $x++):?>
					null,
				<?php endfor;?>
				null
			],
			"aaSorting":[[0, "desc"]], 
			"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
			"sPaginationType": "bootstrap",
			"bJQueryUI": false,
			"bAutoWidth": false,
			"iDisplayLength": (( typeof default_pagination !== 'undefined')? default_pagination: 10),
			"oLanguage": {
				"sSearch": "<span></span> _INPUT_",
				"sLengthMenu": "<span>_MENU_</span>",
				"oPaginate": { "sFirst": "First", "sLast": "Last" }
			}
		});
		$('.dataTables_length select').uniform();
		$('.dataTables_paginate > ul').addClass('pagination');
		$('.dataTables_filter>label>input').addClass('form-control');
		if(typeof default_pagination_url !== 'undefined'){
			$('.selector>select').on('change', function(){
				$.post(default_pagination_url,{
					default_pagination: $(this).val()
				});
			});
		}
	}
});
</script>