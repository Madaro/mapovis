<style>
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 500px; }
	.sortable li { margin: 10px 10px 10px 0; padding: 0px; float: left; width: 150px; height: 115px;  text-align: center; display:block; overflow:hidden;}
	.sortable_view li {height: 81px;}
	.deleteimage{margin-top: 7px;}
	.img_container{overflow-y:hidden;}
	.ui-dialog { z-index: 65535; }
</style>
<script type="text/javascript" src="http://feather.aviary.com/js/feather.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/validate/jquery.validate.min.js"></script>
<script>
var featherEditor = new Aviary.Feather({
	apiKey: '84495f70872a7572',
	apiVersion: 3,
	tools: 'all',
	theme: 'light',
	onError: function(errorObj) {
		console.log(errorObj.message);
	}
});
function initialize_tables(table_id){
	var table_data;
	$('#'+table_id).find('thead input').keyup( function () {
	  table_data.fnFilter( this.value, table_data.oApi._fnVisibleToColumnIndex( 
		table_data.fnSettings(), $('#'+table_id).find('thead input').index(this) ) );
	} );
	table_data = $('#'+table_id).dataTable({
	  "aaSorting":[[0,"asc"]],
	  "bPaginate": false,
	  "sDom":"",
	  "bJQueryUI": false,
	  "bAutoWidth": false,
	  "bSortCellsTop": true
	});
	return table_data;
}

var disabled_table = initialize_tables('disabled_table');
var enabled_table = initialize_tables('enabled_table');
var lot_id = '<?= $lot->lot_id;?>';

$('.dataTables_length select').uniform();
$('.dataTables_length select').uniform();
$('.dataTables_paginate > ul').addClass('pagination');
$('.dataTables_filter>label>input').addClass('form-control');

$('#dialog_lot_house').dialog({autoOpen: false});

function disable_house_package(this_vals){
	showLoading();
	reset_search('disabled_table', disabled_table);
	var house_id = this_vals.attr('house_id');
	var position = enabled_table.fnGetPosition(this_vals.parents('tr')[0]);

	$.ajax({
		url: '<?= base_url(); ?>salesteam/disablehouselotpackage/'+lot_id+'/'+house_id,
		type: 'POST',
		processData: false,
		contentType: false
	}).done(function(data){
		hideLoading();
		var alert_msg = '';
		var result = jQuery.parseJSON(data);
		if(result.status == 1){
			var new_row = [
				'<span id="house_id_'+house_id+'">'+result.obj.house_name+'</span>',
				result.obj.builder_name,
				result.obj.house_type,
				'<button house_id="'+house_id+'" class="enable_house_package btn btn-xs btn-success">Re-Enable</button>'
			]
			enabled_table.fnDeleteRow(position);
			disabled_table.fnAddData(new_row);
			$('#house_id_'+house_id).parents('tr').find('.enable_house_package').click(function(){$(this).prop('disabled', true);enable_house_package($(this))});
			alert_msg = '<div title="Success">'+result.msg+'</div>'
		}
		else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
		messageAlert(alert_msg);
	});
}
$('.update_package').click(function() {
	var house_id = $(this).attr('house_id');
	update_lot_house(house_id, false)
});
$('.disable_house_package').click(function(){$(this).prop('disabled', true);disable_house_package($(this))});

function enable_house_package(this_vals){
	showLoading();
	reset_search('enabled_table', enabled_table);
	var house_id = this_vals.attr('house_id');
	var position = disabled_table.fnGetPosition(this_vals.parents('tr')[0]);

	$.ajax({
		url: '<?= base_url(); ?>salesteam/enablehouselotpackage/'+lot_id+'/'+house_id,
		type: 'POST',
		processData: false,
		contentType: false
	}).done(function(data){
		hideLoading();
		var alert_msg = '';
		var result = jQuery.parseJSON(data);
		if(result.status == 1){
			var pdf = (result.obj.file_name == '')? '':'<a title="Download File" href="<?= base_url();?>../mpvs/templates/lot_house_pdfs/'+result.obj.file_name+'" target="_blank"><span class="icon16 icomoon-icon-file-pdf"></span></a>';
			var new_row = [
				'<span id="house_id_'+house_id+'">'+result.obj.house_name+'</span>',
				result.obj.builder_name,
				result.obj.formatted_price,
				pdf,
				'<button house_id="'+result.obj.house_id+'" class="update_package btn btn-xs btn-default">Edit</button>',
				'<button house_id="'+house_id+'" class="disable_house_package btn btn-xs btn-danger">Disable</button>'
			]
			disabled_table.fnDeleteRow(position);
			enabled_table.fnAddData(new_row);
			$('#house_id_'+house_id).parents('tr').find('.disable_house_package').click(function(){$(this).prop('disabled', true);disable_house_package($(this))});
			$('#house_id_'+house_id).parents('tr').find('.update_package').click(function(){update_lot_house(house_id, false)});
			if(result.obj.file_name == ''){
				update_lot_house(house_id, false)
			}
			else{
				$('#confirm_alert_dialog').html('This house and land match ALREADY has a PDF associated with it. Do you want to upload a NEW one?');
				$('#confirm_alert_dialog').dialog({
						title: "Confirmation",
						height: 160,
						modal: true,
						resizable: false,
						buttons: {
							'Yes': function(){$(this).dialog("close");
							update_lot_house(house_id, false);},
							'No': function(){$(this).dialog("close");}}
				});
			}
		}
		else if(result.status == 2){
			update_lot_house(house_id, position)
		}
		else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
		messageAlert(alert_msg);
	});
}
$('.enable_house_package').click(function(){$(this).prop('disabled', true);enable_house_package($(this));$(this).prop('disabled', false)});

function launchEditor(id, src, filetype, house_id, force_crop, area){
	var editor_params = {
		image: id,
		url: src,
		fileFormat: filetype,
		onSave: function(imageID, newURL) {
			addNewImageHtml(house_id, newURL, newURL, area);
			var img = document.getElementById(imageID);
			img.src = newURL;
			featherEditor.close();
		}
	};
	if(force_crop){
		editor_params.forceCropPreset = ['Width: 244px, Height: 133px','244x133'];
		editor_params.initTool = 'crop';
		editor_params.cropPresetsStrict = 'true';
	}
	else{
		editor_params.tools = 'enhance,effects,frames,stickers,focus,brightness,contrast,saturation,warmth,sharpness,colorsplash,draw,text,redeye,whiten,blemish';
	}
	featherEditor.launch(editor_params);
}

var position = 100000;
function addNewImageHtml(house_id, newURL, imagecontent, area){
	position++;
	if(area == '_facade'){
		var image_field_obj = $('#image_file'+area);
		var input           = image_field_obj[0];
		var form_data = new FormData();
		form_data.append('image_file_facade', input.files[0]);
		$.ajax( {
			url: '<?= base_url(); ?>salesteam/uploadoriginalimage/',
			type: 'POST',
			data: form_data,
			processData: false,
			contentType: false
		}).done(function(data){
			var result = jQuery.parseJSON(data);
			if(result != false){
				var original_image = '<input type="hidden" name="original_image['+position+']" value="'+result+'">';
				$('#originalFiles').append(original_image)
			}
		});
	}

	var button_id = 'delete_img' + new Date().getTime();
	var new_image_html = '<li class="ui-state-default"><input name="facade_images[]" type="text" value="'+position+'||'+newURL+'" style="display:none"><div><img src="'+imagecontent+'" height="81" /></div>';
	new_image_html += '<button type="button" id="'+button_id+'" house_id="'+house_id+'" image_id="'+newURL+'" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button></li>';
	$('#sortable'+area).append(new_image_html);
	$('#'+button_id).on('click', function(e){
		deleteImageFunction($(this));
	});
}

function deleteImageFunction(object){
	var image_obj = object.parent();
	$("<div><span>Are you sure you want to delete this image permanently?</span></div>").dialog({
		resizable: false,
		height:140,
		modal: true,
		buttons: {
			"Delete Image": function() {
				image_obj.remove();
				messageAlert('<div title="Success">The image was deleted successfully.</div>');
				$(this).dialog("close");
			},
			Cancel: function() {
				$(this).dialog( "close" );
			}
		}
	});
}

function update_lot_house(house_id, disabledPosition) {
	showLoading();
	$.post('<?= base_url();?>salesteam/edithouselotpackage/'+lot_id+'/'+house_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_lot_house').html(data);
				$('#dialog_lot_house').dialog({
					title: 'Update Lot and House Package',
					height: 670,
					width: 950,
					modal: true,
					resizable: true,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){remove_tinymce();$('#dialog_lot_house').html('');}
				});
				$('#dialog_lot_house').dialog('open');
				init_tinymce();
				$("input, textarea, select").not('.nostyle').uniform();

				$('#edit_house_lot_package').submit(function(e){
					e.preventDefault();
					return false;
				})

				$('#edit_house_lot_package').validate({
					rules: {
						house_lot_price: {
							required: true,
							number: true,
							min: 100000,
							max: 1000000,
						},
						house_lot_pdf: {accept: false},
						image_file_facade: {accept: false}
					},
					messages: {
						house_lot_price: {
							required: 'Please provide the House & Land Price',
							number: 'Please provide a valid price, NO special characters',
							min: 'The value of the house & land package has not been entered correctly',
							max: 'The value of the house & land package has not been entered correctly',
						}
					},
					submitHandler: function(form) {
						if($('#download_lot_house_pdf').length == 0 && $('#house_lot_pdf').val() == ''){
							messageAlert('<div title="Fail">Please upload a PDF file.</div>');
							return false;
						}
						submitForm(form)
						return false;
					}
				});
				$('.image_files').on('change', function(){
					var input = $(this)[0];
					if (input.files && input.files[0]){
						if(!validateimagefiletype(input.files[0].type)){
							return false;
						}
					}
				});
				$('.add_image_btn').on('click', function(e){
					var area            = $(this).attr('area');
					var house_id        = $(this).attr('house_id');
					var image_field_obj = $('#image_file'+area);
					var input           = image_field_obj[0];
					if (input.files && input.files[0]){
						if(!validateimagefiletype(input.files[0].type)){
							// show dialog message
							return false;
						}
						var reader = new FileReader();
						// set where you want to attach the preview
						reader.target_elem = $(input).parent().find('preview');
						reader.onload = function (e) {
							var force_crop = true;
							var img = document.createElement("img");
							img.onload = function(){
								if(img.width == 244 && img.height == 133){
									force_crop = false;
								}
								$('#imageupload'+area).attr('src', e.target.result);
								launchEditor('imageupload'+area, e.target.result, input.files[0].type, house_id, force_crop,area);
							}
							img.src = e.target.result;
						};
						reader.readAsDataURL(input.files[0]);
					}
				});
				function submitForm(form){
					showLoading();
					$.ajax( {
						url: '<?= base_url(); ?>salesteam/updatehouselotpackage/'+lot_id+'/'+house_id,
						type: 'POST',
						data: new FormData( form ),
						processData: false,
						contentType: false
						}).done(function(data){
							hideLoading();
							var alert_msg = '';
							var result = jQuery.parseJSON(data);
							if(result.status == 1){
								alert_msg = '<div title="Success">'+result.msg+'</div>';
								$('#dialog_lot_house').dialog('close');
								if(disabledPosition !== false){
									disabled_table.fnDeleteRow(disabledPosition);
									var new_row = ['<span id="house_id_'+house_id+'">'+result.obj.house_name+'</span>',result.obj.builder_name,result.obj.formatted_price,'','<button house_id="'+result.obj.house_id+'" class="update_package btn btn-xs btn-default">Edit</button>','<button house_id="'+house_id+'" class="disable_house_package btn btn-xs btn-danger">Disable</button>']
									enabled_table.fnAddData(new_row);
									$('#house_id_'+house_id).parents('tr').find('.disable_house_package').click(function(){$(this).prop('disabled', true);disable_house_package($(this))});
									$('#house_id_'+house_id).parents('tr').find('.update_package').click(function(){update_lot_house(house_id, false)});
								}
								display_details(result.obj)
							}
							else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
							messageAlert(alert_msg);
						});
					return false;
				};

				$('#cancel_lot_house_changes').click(function() {
					$('#dialog_lot_house').dialog('close');
				});
				$('#sortable_facade').sortable({containment: "#img_container_facade",tolerance: "pointer"});
				$('#sortable_facade').disableSelection();
				hideLoading();
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
}

function display_details(obj){
	if(obj != false){
		reset_search('enabled_table', enabled_table);
		var position = enabled_table.fnGetPosition($('#house_id_'+obj.house_id).parents('tr')[0])
		var row_data = enabled_table.fnGetData(position);
		row_data[2] = obj.formatted_price;
		row_data[3] = (obj.file_name == '')? '': '<a title="Download File" href="<?= base_url();?>../mpvs/templates/lot_house_pdfs/'+obj.file_name+'" target="_blank"><span class="icon16 icomoon-icon-file-pdf"></span></a>';
		enabled_table.fnUpdate(row_data, position);
		$('#house_id_'+obj.house_id).parents('tr').find('.disable_house_package').click(function(){$(this).prop('disabled', true);disable_house_package($(this))});
		$('#house_id_'+obj.house_id).parents('tr').find('.update_package').click(function(){update_lot_house(obj.house_id, false)});
	}
}
/* reset the search so the events triggers can be added to the new row */
function reset_search(name_table, obj_table){
	var oSettings = obj_table.fnSettings();
	for(var iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
		oSettings.aoPreSearchCols[ iCol ].sSearch = '';
	}
	obj_table.fnDraw();
	$('#'+name_table+' thead input').val('')
}
</script>