<?php
$base_url = base_url().'salesteam/';
?>
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<div class="col-lg-12">
			<div class="panel-body">
				<form id="edit_subscriber_form" method="post" class="form-horizontal" action="<?= $base_url; ?>updatepricelistsubscriber/<?= $pricelist_subscriber->pricelist_subscriber_id;?>" role="form">
					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Name:</label>
						<div class="col-lg-7">
							<input type="text" name="subscriber_name" id="subscriber_name" class="form-control" value="<?= $pricelist_subscriber->subscriber_name?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Company:</label>
						<div class="col-lg-7">
							<input type="text" name="subscriber_company" id="subscriber_company" class="form-control" value="<?= $pricelist_subscriber->subscriber_company?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Email Address:</label>
						<div class="col-lg-7">
							<input type="text" name="subscriber_email" id="subscriber_email" class="form-control" value="<?= $pricelist_subscriber->subscriber_email?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Office Phone:</label>
						<div class="col-lg-7">
							<input type="text" name="office_phone" class="form-control" value="<?= $pricelist_subscriber->office_phone?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Mobile Phone:</label>
						<div class="col-lg-7">
							<input type="text" name="mobile_phone" class="form-control" value="<?= $pricelist_subscriber->mobile_phone?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Job Title:</label>
						<div class="col-lg-7">
							<input type="text" name="job_title" class="form-control" value="<?= $pricelist_subscriber->job_title?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group" style="padding-top:10px">
						<div class="col-lg-offset-3 col-lg-9">
							<button type="submit" class="save_changes btn btn-info">Save Changes</button>
							<button type="button" class="cancel_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
						</div>
					</div><!-- End .form-group  -->
				</form>
			</div>
	</div><!-- End .span6 -->
