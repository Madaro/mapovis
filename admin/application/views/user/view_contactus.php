<?php
$base_url = base_url().'salesteam/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Support</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
							<div class="well well-lg">
								<span class="welcome"><?= $current_user->name;?>, we are here to help! </span>
								</br></br>
								An answer to your question may already be available in the <a href="http://mapovis.zendesk.com" target="_blank">Knowledge Base</a>. Alternatively, contact us through the email form or telephone number below. 
								</br></br>

								<div class="col-lg-4">
									<ul class="list-group">
										<li class="list-group-item"><span class="contactlistheader">Email:</span><a href="mailto:<?= $settings->support_email;?>"> <?= $settings->support_email;?></a></li>
										<li class="list-group-item"><span class="contactlistheader">Telephone:</span><?= $settings->contact_number;?></li>
										<li class="list-group-item"><span class="contactlistheader">Chat:</span><a href="#">click here</a></li>
									</ul>
								</div>
								</br></br></br> </br></br></br> </br>
							</div>

						</div><!-- End .span4 -->

					<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4>
										<span class="icon16 icomoon-icon-envelop"></span>
										<span>Email Support Form</span>
									</h4>
								</div>
								<div class="panel-body">
									<iframe src="https://mapovis.zendesk.com/account/dropboxes/20108040#/dropbox/tickets/new" width="70%" height="450" frameborder="0"></iframe>
								</div>
							</div><!-- End .panel -->
						</div><!-- End .span12 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
