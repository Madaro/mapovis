<?php
$base_url           = base_url().'salesteam/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-location-2"></span>
									<span><?= $development->development_name;?> Price List Subscribers</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($pricelist_subscribers)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Name</th>
											<th>Company</th>
											<th>Email Address</th>
											<th>Office Phone</th>
											<th>Mobile Phone</th>
											<th>Job Title</th>
											<th></th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_pricelist_subscriber_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_company" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_email" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_office_phone" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_mobile_phone" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_job_title" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($pricelist_subscribers as $pricelist_subscriber):?>
										<tr>
											<td><?= $pricelist_subscriber->pricelist_subscriber_id;?></td>
											<td><?= $pricelist_subscriber->subscriber_name;?></td>
											<td><?= $pricelist_subscriber->subscriber_company;?></td>
											<td><?= $pricelist_subscriber->subscriber_email;?></td>
											<td><?= $pricelist_subscriber->office_phone;?></td>
											<td><?= $pricelist_subscriber->mobile_phone;?></td>
											<td><?= $pricelist_subscriber->job_title;?></td>
											<td class="center">
												<button type="button" type_id="<?= $pricelist_subscriber->pricelist_subscriber_id;?>" class="btn btn-xs btn-default view_type_btn">Edit</button>
											</td>
											<td>
												<a href="<?= "{$base_url}deletepricelistsubscriber/{$pricelist_subscriber->pricelist_subscriber_id}"; ?>">
													<button type="button" class="delete_pricelist_subscriber_btn btn btn-xs btn-danger">Delete</button>
												</a>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not price list subscribers in the system.</div>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->
				<div>
					<a href="<?= $base_url; ?>addpricelistsubscriber/<?= $development->development_id;?>">
						<button type="button" class="btn btn-info">Add Price List Subscriber</button>
					</a>
				</div>

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Image field necesary to call Aviary and crop the images -->
<img id='imageupload' src='' style="display:none"/>
<!-- Dialog -->
<div id="dialog_pricelistsubscriber"></div>