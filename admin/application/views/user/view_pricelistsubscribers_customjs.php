<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/validate/jquery.validate.min.js"></script>
<script type="text/javascript">
$('#dialog_pricelistsubscriber').dialog({ autoOpen: false });

$('.view_type_btn').on('click', function(){
	var type_id = $(this).attr('type_id');
	showLoading();
	$.post('<?= base_url();?>salesteam/viewpricelistsubscriber/'+type_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_pricelistsubscriber').html(data);
				$('#dialog_pricelistsubscriber').dialog({
					title: 'Update Price List Subscriber',
					height: 440,
					width: 550,
					modal: true,
					resizable: false,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_pricelistsubscriber').html('');}
				});
				$('#dialog_pricelistsubscriber').dialog('open');
				$("input, textarea, select").not('.nostyle').uniform();
				hideLoading();
				$('#edit_subscriber_form').validate({
					rules: {
						subscriber_name: {
							required: true
						},
						subscriber_email: {
							required: true,
							email: true
						}
					},
					messages: {
						subscriber_name: {
							required: 'Please provide the name'
						},
						subscriber_email: {
							required: 'Please provide an email address',
							email:"Please privde a valid email address"
						}
					},
					submitHandler: function(form) {
						showLoading();
						form.submit();
					}
				});
				$('.cancel_changes').click(function() {
					$('#dialog_pricelistsubscriber').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
$('.delete_pricelist_subscriber_btn').click(function() {
	if(!confirm('Are you sure you want to delete the price list subscriber?')){
		return false;
	}
	showLoading();
});
</script>