<?php
$base_url           = base_url().'salesteam/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"><!-- -->
<style>
	.ui-widget{
		font-size:13px;
		border: 0;
	}
	.ui-tabs {
		position: relative;/* position: relative prevents IE scroll bug (element with position: relative inside container with overflow: auto appear as "fixed") */
		padding: .2em;
	}
	.ui-tabs .ui-tabs-nav li.ui-tabs-active {
		margin-bottom: -1px;
		padding-bottom: 1px;
	}
	.ui-tabs .ui-tabs-nav {
		margin: 0;
		padding: .2em .2em 0;
		border-bottom: 1px solid #aaaaaa;
	}
	.ui-tabs .ui-tabs-nav li {
		list-style: none;
		float: left;
		position: relative;
		top: 0;
		margin: 1px .2em 0 0;
		border-bottom-width: 0;
		padding: 0;
		white-space: nowrap;
	}
	.ui-tabs .ui-tabs-nav .ui-tabs-anchor {
		float: left;
		padding: .5em 1em;
		font-weight: bold;
	}
	.ui-tabs .ui-tabs-panel {
		display: block;
		border: 1px solid #aaaaaa;
		border-top: 0;
		padding: 1em 1.4em;
		background: none;
	}
	.ui-widget-content a {
		font-size:14px;
		color: #222222;
	}
	.ui-widget-header {
		background: #ffffff;
	}
	.ui-state-default{
		background: #cfd6ec;
	}
	.ui-state-active{
		border: 1px solid #aaaaaa;
		background: #ffffff !important;
		color: #212121;
	}
</style>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					 <h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3> 
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4>
										<span class="icon16 entypo-icon-settings"></span>
								<?php if($media_agency):?>
										<span>Development Settings</span>
								<?php else:?>
										<span>PDF Settings</span>
								<?php endif;?>
									</h4>
								</div>
								<div class="panel-body">
								<?= $validation_msg;?>
								<form id="generalsettinsForm" method="post" class="form-horizontal" action="<?= $base_url; ?>editdevelopment/<?= $development->development_id;?>" role="form" enctype="multipart/form-data">

								<?php if($media_agency):?>
								<div id="tabs">
									<ul>
										<li><a href="#tabs-2">Development</a></li>
										<li><a href="#tabs-4">Map & Search</a></li>
										<li><a href="#tabs-3">HTML Style</a></li>
										<li><a href="#tabs-1">Sales Office</a></li>
									</ul>
									<div id="tabs-1">
										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Office Title:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="sales_office_title" name="sales_office_title" value="<?= $development->sales_office_title;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Office Address:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="sales_office_address" name="sales_office_address" value="<?= $development->sales_office_address;?>">
											</div>
										</div><!-- End .form-group-->

								<?php endif;?>
								<?php if(!$media_agency):?>
										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Lots PDF - HTML Header:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="pdf_header_html" name="pdf_header_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->pdf_header_html);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Lots PDF - HTML Footer:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="pdf_footer_html" name="pdf_footer_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->pdf_footer_html);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Builder PDF - HTML Header:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="builder_pdf_header_html" name="builder_pdf_header_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->builder_pdf_header_html);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Builder PDF - HTML Footer:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="builder_pdf_footer_html" name="builder_pdf_footer_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->builder_pdf_footer_html);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Builder - Terms and Conditions:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="terms_and_conditions_builder" name="terms_and_conditions_builder"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->terms_and_conditions_builder);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Builder - Disclaimer:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="diclaimer_builder" name="diclaimer_builder"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->diclaimer_builder);?></textarea>
											</div>
										</div><!-- End .form-group-->

								<?php endif;?>

								<?php if($media_agency):?>

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Office HTML:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="tinymce" name="sales_office_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->sales_office_html);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Telephone Number:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="sales_telephone_number" name="sales_telephone_number" value="<?= $development->sales_telephone_number;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Local Sales Telephone Number:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="local_sales_telephone_number" name="local_sales_telephone_number" value="<?= $development->local_sales_telephone_number;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Email Address:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="sales_email_address" name="sales_email_address" value="<?= $development->sales_email_address;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Person Name:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="sales_person_name" name="sales_person_name" value="<?= $development->sales_person_name;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Office Opening Hours:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="sales_office_opening_house" name="sales_office_opening_house" value="<?= $development->sales_office_opening_house;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Office Googlemaps Directions Url:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="sales_office_googlemaps_directions_url" name="sales_office_googlemaps_directions_url" value="<?= $development->sales_office_googlemaps_directions_url;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="col-lg-11 alert alert-warning">If you want to use a completely custom design you can provide an IFrame.</div>
										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Office Iframe HTML:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="sales_office_iframe_html" value="<?= htmlspecialchars($development->sales_office_iframe_html, ENT_QUOTES);?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Show Scrollbar:</label>
											<div class="col-lg-5">
												<input type="radio" name="sales_office_iframe_scrollbar" value="1" <?= ($development->sales_office_iframe_scrollbar == 1? 'checked="checked"': '');?> id="sales_office_iframe_scrollbar1" class="sales_office_iframe_scrollbar"><label for="sales_office_iframe_scrollbar1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="sales_office_iframe_scrollbar" value="0" <?= ($development->sales_office_iframe_scrollbar == 0? 'checked="checked"': '');?> id="sales_office_iframe_scrollbar0" class="sales_office_iframe_scrollbar"><label for="sales_office_iframe_scrollbar0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 
									</div>

									<div id="tabs-2">
										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Name of the Development:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="nameofthedevelopment" name="development_name" value="<?= set_value('development_name', $development->development_name);?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
												<label class="col-lg-3 control-label" for="required">Access Key:</label>
												<div class="col-lg-4">
													<input type="text" class="form-control" id="access_key" name="access_key" value="<?= $development->access_key;?>">
												</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Wildcard White List Domain:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="dev_domain" name="dev_domain" value="<?= $development->dev_domain;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">PDF Link:</label>
											<div class="col-lg-5">
												<input type="text" class="form-control" id="pdf_link" name="pdf_link" value="<?= $development->pdf_link;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Enquiry Form Iframe URL:</label>
											<div class="col-lg-5">
												<input type="text" class="form-control" name="form_iframe_url" value="<?= $development->form_iframe_url;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Enquiry Form Iframe URL Height:</label>
											<div class="col-lg-2">
												<input type="text" class="form-control" name="form_iframe_url_height" value="<?= $development->form_iframe_url_height;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Land Enquiry Form Iframe URL:</label>
											<div class="col-lg-5">
												<input type="text" class="form-control" name="land_form_iframe_url" value="<?= $development->land_form_iframe_url;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Land Enquiry Form Iframe URL Height:</label>
											<div class="col-lg-2">
												<input type="text" class="form-control" name="land_form_iframe_url_height" value="<?= $development->land_form_iframe_url_height;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Land Starting From:</label>
											<div class="col-lg-2">
												<input type="text" class="form-control" name="land_starting_from" value="<?= round($development->land_starting_from, 2);?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">House & Land Enquiry Form Iframe URL:</label>
											<div class="col-lg-5">
												<input type="text" class="form-control" name="house_and_land_form_iframe_url" value="<?= $development->house_and_land_form_iframe_url;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">House & Land Enquiry Form Iframe URL Height:</label>
											<div class="col-lg-2">
												<input type="text" class="form-control" name="house_and_land_form_iframe_url_height" value="<?= $development->house_and_land_form_iframe_url_height;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Users Avatar Icon URL:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="development_url_icon" value="<?= $development->development_url_icon;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">MAPOVIS Not Supported URL:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="mapovis_not_supported_url" value="<?= $development->mapovis_not_supported_url;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">MAPOVIS Generated PDF:</label>
											<div class="col-lg-5">
												<input type="radio" name="generated_pdf" value="1" <?= ($development->generated_pdf == 1? 'checked="checked"': '');?> id="generated_pdf1" class="generated_pdf"><label for="generated_pdf1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="generated_pdf" value="0" <?= ($development->generated_pdf == 0? 'checked="checked"': '');?> id="generated_pdf0" class="generated_pdf"><label for="generated_pdf0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div id="lots_pdf_file_div" class="form-group" style="display:<?= ($development->generated_pdf == 1)? 'inline': 'none';?>">
											<label class="col-lg-3 control-label">PDF File:</label>
											<div class="col-lg-9">
												<div id="lots_pdf_file">
													<input type="file" name="lots_pdf_file" id="lots_pdf_file_field" value="" accept="application/pdf" >
													<?php if($development->lots_pdf_file):?>
														<span id="current_file">
															<a href="<?= base_url().'../mpvs/templates/dev_pdfs/'.$development->lots_pdf_file;?>" title="Download File" download><span class="icon16 icomoon-icon-file-pdf"></span></a>
														</span>
														</br></br>
													<?php endif;?>
												</div>
											</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Show Download PDF:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_download_pdf" value="1" <?= ($development->show_download_pdf == 1? 'checked="checked"': '');?> id="show_download_pdf1" class="show_download_pdf"><label for="show_download_pdf1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_download_pdf" value="0" <?= ($development->show_download_pdf == 0? 'checked="checked"': '');?> id="show_download_pdf0" class="show_download_pdf"><label for="show_download_pdf0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sort House Matches By:</label>
											<div class="col-lg-4">
												<select name="sort_house_matches_by" class="form-control">
													<?php foreach($house_match_sort_option as $option):?>
													<option value="<?= $option;?>" <?= ($option == $development->sort_house_matches_by)? 'selected="selected"': '';?>><?= ucwords(str_replace('_', ' ', $option));?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Terms and Conditions:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="terms_and_conditions" name="terms_and_conditions"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->terms_and_conditions);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Lots PDF - HTML Header:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="pdf_header_html" name="pdf_header_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->pdf_header_html);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Lots PDF - HTML Footer:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="pdf_footer_html" name="pdf_footer_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->pdf_footer_html);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Builder PDF - HTML Header:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="builder_pdf_header_html" name="builder_pdf_header_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->builder_pdf_header_html);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Builder PDF - HTML Footer:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="builder_pdf_footer_html" name="builder_pdf_footer_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->builder_pdf_footer_html);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Builder - Terms and Conditions:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="terms_and_conditions_builder" name="terms_and_conditions_builder"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->terms_and_conditions_builder);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Builder - Disclaimer:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="diclaimer_builder" name="diclaimer_builder"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->diclaimer_builder);?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">White List Staging Domain:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="staging_domain" value="<?= $development->staging_domain;?>">
											</div>
										</div><!-- End .form-group-->
									</div>

									<div id="tabs-3">
										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Header & Footer - Start Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="start_hex_colour" name="start_hex_colour" value="#<?= $development->start_hex_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="start_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->start_hex_colour;?>;background-color: #<?= $development->start_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Header & Footer - End Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="end_hex_colour" name="end_hex_colour" value="#<?= $development->end_hex_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="end_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->end_hex_colour;?>;background-color: #<?= $development->end_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Text HEX Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="text_hex_colour" name="text_hex_colour" value="#<?= $development->text_hex_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="text_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->text_hex_colour;?>;background-color: #<?= $development->text_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Lightbox Border Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="lightbox_border_colour" name="lightbox_border_colour" value="#<?= $development->lightbox_border_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="lightbox_border_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->lightbox_border_colour;?>;background-color: #<?= $development->lightbox_border_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Lightbox Highlight Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="lightbox_highlight_colour" name="lightbox_highlight_colour" value="#<?= $development->lightbox_highlight_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="lightbox_highlight_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->lightbox_highlight_colour;?>;background-color: #<?= $development->lightbox_highlight_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 



										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Loading Screen - Top Gradient Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" name="loading_gradient_top_hex" value="#<?= $development->loading_gradient_top_hex;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="loading_gradient_top_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->loading_gradient_top_hex;?>;background-color: #<?= $development->loading_gradient_top_hex;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Loading Screen - Middle Gradient Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" name="loading_gradient_middle_hex" value="#<?= $development->loading_gradient_middle_hex;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="loading_gradient_middle_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->loading_gradient_middle_hex;?>;background-color: #<?= $development->loading_gradient_middle_hex;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Loading Screen - Bottom Gradient Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" name="loading_gradient_bottom_hex" value="#<?= $development->loading_gradient_bottom_hex;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="loading_gradient_bottom_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->loading_gradient_bottom_hex;?>;background-color: #<?= $development->loading_gradient_bottom_hex;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 


										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Zoom to Stage Panel - Header:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="zoom_to_stage_header_hex" name="zoom_to_stage_header_hex" value="#<?= $development->zoom_to_stage_header_hex;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="zoom_to_stage_header_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->zoom_to_stage_header_hex;?>;background-color: #<?= $development->zoom_to_stage_header_hex;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Zoom to Stage Panel - Buttons:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="zoom_to_stage_button_hex" name="zoom_to_stage_button_hex" value="#<?= $development->zoom_to_stage_button_hex;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="zoom_to_stage_button_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->zoom_to_stage_button_hex;?>;background-color: #<?= $development->zoom_to_stage_button_hex;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Dialog Box Header - Start Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="dialog_start_hex" name="dialog_start_hex" value="#<?= $development->dialog_start_hex;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="dialog_start_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->dialog_start_hex;?>;background-color: #<?= $development->dialog_start_hex;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Dialog Box Header - End Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="dialog_end_hex" name="dialog_end_hex" value="#<?= $development->dialog_end_hex;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="dialog_end_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->dialog_end_hex;?>;background-color: #<?= $development->dialog_end_hex;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Dialog Close Button - Background Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="dialog_close_button_background_hex" name="dialog_close_button_background_hex" value="#<?= $development->dialog_close_button_background_hex;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="dialog_close_button_background_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->dialog_close_button_background_hex;?>;background-color: #<?= $development->dialog_close_button_background_hex;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Panel Close Button Background Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="panel_close_background_colour" name="panel_close_background_colour" value="#<?= $development->panel_close_background_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="panel_close_background_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->panel_close_background_colour;?>;background-color: #<?= $development->panel_close_background_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Panel Close Button Foreground Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="panel_close_foreground_colour" name="panel_close_foreground_colour" value="#<?= $development->panel_close_foreground_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="panel_close_foreground_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->panel_close_foreground_colour;?>;background-color: #<?= $development->panel_close_foreground_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">House & Land Package - Download PDF Button:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="house_land_package_downloadpdf_button" name="house_land_package_downloadpdf_button" value="#<?= $development->house_land_package_downloadpdf_button;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="house_land_package_downloadpdf_button_disp" style="width:50px !important;border: 2px solid #<?= $development->house_land_package_downloadpdf_button;?>;background-color: #<?= $development->house_land_package_downloadpdf_button;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">House & Land Package - Download PDF Button Highlight:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="house_land_package_downloadpdf_button_highlight" name="house_land_package_downloadpdf_button_highlight" value="#<?= $development->house_land_package_downloadpdf_button_highlight;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="house_land_package_downloadpdf_button_highlight_disp" style="width:50px !important;border: 2px solid #<?= $development->house_land_package_downloadpdf_button_highlight;?>;background-color: #<?= $development->house_land_package_downloadpdf_button_highlight;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">House & Land Package - Enquire Now Button:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="house_land_package_enquirenow_button" name="house_land_package_enquirenow_button" value="#<?= $development->house_land_package_enquirenow_button;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="house_land_package_enquirenow_button_disp" style="width:50px !important;border: 2px solid #<?= $development->house_land_package_enquirenow_button;?>;background-color: #<?= $development->house_land_package_enquirenow_button;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">House & Land Package - Enquire Now Button Highlight:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="house_land_package_enquirenow_button_highlight" name="house_land_package_enquirenow_button_highlight" value="#<?= $development->house_land_package_enquirenow_button_highlight;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="house_land_package_enquirenow_button_highlight_disp" style="width:50px !important;border: 2px solid #<?= $development->house_land_package_enquirenow_button_highlight;?>;background-color: #<?= $development->house_land_package_enquirenow_button_highlight;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Google Control Buttons Background:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="google_control_buttons_background" name="google_control_buttons_background" value="#<?= $development->google_control_buttons_background;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="google_control_buttons_background_disp" style="width:50px !important;border: 2px solid #<?= $development->google_control_buttons_background;?>;background-color: #<?= $development->google_control_buttons_background;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Google Control Buttons Foreground:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="google_control_buttons_foreground" name="google_control_buttons_foreground" value="#<?= $development->google_control_buttons_foreground;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="google_control_buttons_foreground_disp" style="width:50px !important;border: 2px solid #<?= $development->google_control_buttons_foreground;?>;background-color: #<?= $development->google_control_buttons_foreground;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Tooltip Background Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="tooltip_background_colour" name="tooltip_background_colour" value="#<?= $development->tooltip_background_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="tooltip_background_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->tooltip_background_colour;?>;background-color: #<?= $development->tooltip_background_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Tooltip Text Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="tooltip_text_colour" name="tooltip_text_colour" value="#<?= $development->tooltip_text_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="tooltip_text_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->tooltip_text_colour;?>;background-color: #<?= $development->tooltip_text_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Directions From External Amenity Text Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="get_directions_from_external_amenities_text_colour" name="get_directions_from_external_amenities_text_colour" value="#<?= $development->get_directions_from_external_amenities_text_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="get_directions_from_external_amenities_text_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->get_directions_from_external_amenities_text_colour;?>;background-color: #<?= $development->get_directions_from_external_amenities_text_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Directions From External Amenity Text Highlight Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="get_directions_from_external_amenities_text_highlight_colour" name="get_directions_from_external_amenities_text_highlight_colour" value="#<?= $development->get_directions_from_external_amenities_text_highlight_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="get_directions_from_external_amenities_text_highlight_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->get_directions_from_external_amenities_text_highlight_colour;?>;background-color: #<?= $development->get_directions_from_external_amenities_text_highlight_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Directions Summary Background Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="directions_summary_background_colour" name="directions_summary_background_colour" value="#<?= $development->directions_summary_background_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="directions_summary_background_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->directions_summary_background_colour;?>;background-color: #<?= $development->directions_summary_background_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Directions Summary Text Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="directions_summary_text_colour" name="directions_summary_text_colour" value="#<?= $development->directions_summary_text_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="directions_summary_text_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->directions_summary_text_colour;?>;background-color: #<?= $development->directions_summary_text_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Get Direction Button Background Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="directions_get_direction_button_background_colour" name="directions_get_direction_button_background_colour" value="#<?= $development->directions_get_direction_button_background_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="directions_get_direction_button_background_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->directions_get_direction_button_background_colour;?>;background-color: #<?= $development->directions_get_direction_button_background_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Get Direction Button Text Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="directions_get_direction_button_text_colour" name="directions_get_direction_button_text_colour" value="#<?= $development->directions_get_direction_button_text_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="directions_get_direction_button_text_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->directions_get_direction_button_text_colour;?>;background-color: #<?= $development->directions_get_direction_button_text_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Nearby Places Sidebar Icon Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="nearby_places_sidebar_icon_colour" name="nearby_places_sidebar_icon_colour" value="#<?= $development->nearby_places_sidebar_icon_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="nearby_places_sidebar_icon_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->nearby_places_sidebar_icon_colour;?>;background-color: #<?= $development->nearby_places_sidebar_icon_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Nearby Places Sidebar Icon Colour Selected:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="nearby_places_sidebar_icon_colour_selected" name="nearby_places_sidebar_icon_colour_selected" value="#<?= $development->nearby_places_sidebar_icon_colour_selected;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="nearby_places_sidebar_icon_colour_selected_disp" style="width:50px !important;border: 2px solid #<?= $development->nearby_places_sidebar_icon_colour_selected;?>;background-color: #<?= $development->nearby_places_sidebar_icon_colour_selected;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">No Gradient HEX Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="no_gradient_hex_colour" name="no_gradient_hex_colour" value="#<?= $development->no_gradient_hex_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="no_gradient_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->no_gradient_hex_colour;?>;background-color: #<?= $development->no_gradient_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Header Footer Button Background Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="header_footer_button_bg_hex_colour" name="header_footer_button_bg_hex_colour" value="#<?= $development->header_footer_button_bg_hex_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="header_footer_button_bg_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->header_footer_button_bg_hex_colour;?>;background-color: #<?= $development->header_footer_button_bg_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Lot Search Button Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="lot_search_button_hex_colour" name="lot_search_button_hex_colour" value="#<?= $development->lot_search_button_hex_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="lot_search_button_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->lot_search_button_hex_colour;?>;background-color: #<?= $development->lot_search_button_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="buttons">Tour Buttons Background Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="tour_buttons_background_hex_colour" name="tour_buttons_background_hex_colour" value="#<?= $development->tour_buttons_background_hex_colour;?>">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="tour_buttons_background_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->tour_buttons_background_hex_colour;?>;background-color: #<?= $development->tour_buttons_background_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Custom Font - TypeKit ID or Google Font URL:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="custom_font_markup" value="<?= $development->custom_font_markup;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Custom Google Font Name:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="custom_google_font_name" value="<?= $development->custom_google_font_name;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Custom Typekit Font Name:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="custom_typekit_font_name" value="<?= $development->custom_typekit_font_name;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Custom CSS Override URL:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="custom_css_override_url" value="<?= $development->custom_css_override_url;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Font Size:</label>
											<div class="col-lg-1">
												<input type="text" class="form-control" name="font_size" value="<?= $development->font_size;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Tooltip Font Size:</label>
											<div class="col-lg-1">
												<input type="text" class="form-control" name="tooltip_font_size" value="<?= $development->tooltip_font_size;?>">
											</div>
										</div><!-- End .form-group-->

									</div>
									<div id="tabs-4">
										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Get Directions Example Address:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="get_directions_to_example_address" value="<?= $development->get_directions_to_example_address;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Search by Lot Price:</label>
											<div class="col-lg-5">
												<input type="radio" name="search_by_lot_price" value="1" <?= ($development->search_by_lot_price == 1? 'checked="checked"': '');?> id="search_by_price1"><label for="search_by_price1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="search_by_lot_price" value="0" <?= ($development->search_by_lot_price == 0? 'checked="checked"': '');?> id="search_by_price0"><label for="search_by_price0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Search by Lot Width:</label>
											<div class="col-lg-5">
												<input type="radio" name="search_by_lot_width" value="1" <?= ($development->search_by_lot_width == 1? 'checked="checked"': '');?> id="search_by_width1"><label for="search_by_width1"> On </label>&nbsp;&nbsp;
												<input type="radio" name="search_by_lot_width" value="0" <?= ($development->search_by_lot_width== 0? 'checked="checked"': '');?> id="search_by_width0"><label for="search_by_width0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Search by Lot Size:</label>
											<div class="col-lg-5">
												<input type="radio" name="search_by_lot_size" value="1" <?= ($development->search_by_lot_size == 1? 'checked="checked"': '');?> id="search_by_size1"><label for="search_by_size1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="search_by_lot_size" value="0" <?= ($development->search_by_lot_size == 0? 'checked="checked"': '');?> id="search_by_size0"><label for="search_by_size0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Show Zoom to Stage Panel:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_zoom_to_stage" value="1" <?= ($development->show_zoom_to_stage == 1? 'checked="checked"': '');?> id="show_zoom_to_stage1"><label for="show_zoom_to_stage1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_zoom_to_stage" value="0" <?= ($development->show_zoom_to_stage == 0? 'checked="checked"': '');?> id="show_zoom_to_stage0"><label for="show_zoom_to_stage0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Show Prices:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_pricing" value="1" <?= ($development->show_pricing == 1? 'checked="checked"': '');?> id="show_pricing1"><label for="show_pricing1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_pricing" value="0" <?= ($development->show_pricing == 0? 'checked="checked"': '');?> id="show_pricing0"><label for="show_pricing0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Show Telephone:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_telephone" value="1" <?= ($development->show_telephone == 1? 'checked="checked"': '');?> id="show_telephone1"><label for="show_telephone1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_telephone" value="0" <?= ($development->show_telephone == 0? 'checked="checked"': '');?> id="show_telephone0"><label for="show_telephone0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Show Stage Icons:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_stage_icons" value="1" <?= ($development->show_stage_icons == 1? 'checked="checked"': '');?> id="show_stage_icons1"><label for="show_stage_icons1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_stage_icons" value="0" <?= ($development->show_stage_icons == 0? 'checked="checked"': '');?> id="show_stage_icons0"><label for="show_stage_icons0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Show External Amenities:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_external_amenities" value="1" <?= ($development->show_external_amenities == 1? 'checked="checked"': '');?> id="show_external_amenities1"><label for="show_external_amenities1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_external_amenities" value="0" <?= ($development->show_external_amenities == 0? 'checked="checked"': '');?> id="show_external_amenities0"><label for="show_external_amenities0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Show MAPOVIS Tour:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_tour" value="1" <?= ($development->show_tour == 1? 'checked="checked"': '');?> id="show_tour1" class="show_tour"><label for="show_tour1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_tour" value="0" <?= ($development->show_tour == 0? 'checked="checked"': '');?> id="show_tour0" class="show_tour"><label for="show_tour0"> Off </label>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Show House Matches:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_house_matches" value="1" <?= ($development->show_house_matches == 1? 'checked="checked"': '');?> id="show_house_matches1" class="show_house_matches"><label for="show_house_matches1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_house_matches" value="0" <?= ($development->show_house_matches == 0? 'checked="checked"': '');?> id="show_house_matches0" class="show_house_matches"><label for="show_house_matches0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Highlighted Amenities Icons:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_highlighted_amenities" value="1" <?= ($development->show_highlighted_amenities == 1? 'checked="checked"': '');?> id="show_highlighted_amenities1" class="show_highlighted_amenities"><label for="show_highlighted_amenities1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_highlighted_amenities" value="0" <?= ($development->show_highlighted_amenities == 0? 'checked="checked"': '');?> id="show_highlighted_amenities0" class="show_highlighted_amenities"><label for="show_highlighted_amenities0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Radius Circle:</label>
											<div class="col-lg-5">
												<input type="radio" name="radius_circle" value="1" <?= ($development->radius_circle == 1? 'checked="checked"': '');?> id="radius_circle1" class="radius_circle"><label for="radius_circle1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="radius_circle" value="0" <?= ($development->radius_circle == 0? 'checked="checked"': '');?> id="radius_circle0" class="radius_circle"><label for="radius_circle0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Get Directions From External Amenities:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_get_directions_from_external_amenities" value="1" <?= ($development->show_get_directions_from_external_amenities == 1? 'checked="checked"': '');?> id="show_get_directions_from_external_amenities1" class="show_get_directions_from_external_amenities"><label for="show_get_directions_from_external_amenities1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_get_directions_from_external_amenities" value="0" <?= ($development->show_get_directions_from_external_amenities == 0? 'checked="checked"': '');?> id="show_get_directions_from_external_amenities0" class="show_get_directions_from_external_amenities"><label for="show_get_directions_from_external_amenities0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Show Satellite View:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_satellite" value="1" <?= ($development->show_satellite == 1? 'checked="checked"': '');?> id="show_satellite1" class="show_satellite"><label for="show_satellite1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_satellite" value="0" <?= ($development->show_satellite == 0? 'checked="checked"': '');?> id="show_satellite0" class="show_satellite"><label for="show_satellite0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Search for Nearby Places Placeholder Text:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="search_for_nearby_place_placeholder_text" value="<?= $development->search_for_nearby_place_placeholder_text;?>" maxlength="40">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Back To Button:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="back_to_button" value="<?= $development->back_to_button;?>" maxlength="25">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-4 control-label" for="required">Custom Google Maps Style:</label>
											<div class="col-lg-8">
												<textarea style="height: 150px;" class="form-control" name="custom_googlemaps_style"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->custom_googlemaps_style);?></textarea>
											</div>
										</div><!-- End .form-group  --> 

									</div>
								</div>
								<?php endif;?>

								<div class="col-lg-offset-2">
								 <br>
									<button type="submit" class="btn btn-info">Update</button>
									<a href="<?= $manage_development;?>" style="padding-left:10px;"><button type="button" class="btn btn-default">Cancel</button></a>
									<br><br>
								</div>
							</form>
							</div>

							</div><!-- End .panel -->

						</div><!-- End .span3 -->

				</div><!-- End .row --> 

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<link href="<?php echo base_url(); ?>assets/js/chromoselector-2.1.5/chromoselector.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/chromoselector-2.1.5/chromoselector.min.js"></script>
<script>
	$(function(){
		$("#tabs").tabs();
	});

	$(document).ready(function() {
		var updatePreview = function() {
			var color = $(this).chromoselector('getColor');
			var box_id = $(this).attr('name')+'_disp';
			var color_box = color.getHexString();
			$('#'+box_id).attr('style', 'width:50px !important;border: 2px solid '+color_box+';background-color:'+color_box+';')
		};
		jQuery.each($('.change_colour'), function(){
			$(this).chromoselector({
				preview: false,
				update: updatePreview
			});
		});
	});
	$('.generated_pdf').on('change', function(){
		if($(this).val() == 1){
			$('#lots_pdf_file_div').css('display', 'inline');
		}
		else{
			$('#lots_pdf_file_div').css('display', 'none');
		}

	});
</script>