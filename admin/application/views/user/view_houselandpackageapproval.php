<?php
$base_url           = base_url().'salesteam/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>House & Land Packages Awaiting Approval</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Lot Number</th>
											<th>Lot Size</th>
											<th>Lot Width</th>
											<th>House Name</th>
											<th>Builder</th>
											<th>House Size</th>
											<th>Price</th>
											<th>Sales Person</th>
											<th>PDF</th>
											<th>Description</th>
											<th>Submitted</th>
											<th></th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_lot_number" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_lot_size" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_lot_width" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_house" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_builder" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_house_size" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_price" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_sales_person" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_sales_pdf" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_description" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_request" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($packages_for_approval as $package):?>
										<tr>
											<td><?= $package->lot_number; ?></td>
											<td><?= $package->lot_size; ?> m<sup>2</sup></td>
											<td><?= (float)$package->lot_width; ?></td>
											<td><?= $package->house_name; ?></td>
											<td><?= $package->builder_name; ?></td>
											<td><?= $package->house_size; ?> <sub>sq</sub></td>
											<td><?= ($package->house_lot_price)? '$'.number_format($package->house_lot_price): 'n/a'; ?></td>
											<td><?= $package->sales_person; ?></td>
											<td>
												<?php if(!empty($package->file_name)):?>
													<a title="Download File" href="<?= base_url().'../mpvs/templates/lot_house_pdfs_approval/'.$package->file_name;?>" 
													   target="_blank">
														<span class="icon16 icomoon-icon-file-pdf"></span>
													</a>
												<?php endif?>
											</td>
											<td><?= $package->description; ?></td>
											<td><?= $package->builder_user; ?><br><?= date('d-M-Y', strtotime($package->request_date)); ?></td>
											<td>
												<a href="<?= "{$base_url}approvehouselotpackage/{$development->development_id}/{$package->house_lot_package_approval_id}"; ?>"><button class="approve_package_btn btn btn-xs btn-success">Approve</button></a>
											</td>
											<td>
												<button package_id="<?= $package->house_lot_package_approval_id;?>" class="reject_package_btn btn btn-xs btn-danger">Reject</button>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<div>
					<a href="<?= $base_url; ?>addhouse">
						<button type="button" class="btn btn-info">Add House</button>
					</a>
				</div>
				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<div id="dialog-form" title="Create new user">
	<div class="col-lg-10">
		<div class="panel-body">
		<h5>Are you sure you want to reject the house and land?</h5>
		<form id="reject_form" method="post" class="form-horizontal" action="" role="form" >
			<p>You can provide some reasons below:</p>
			<div class="form-group">
				<div class="col-lg-13">
				<textarea name="rejection_message" id="rejection_message" rows="4" class="form-control elastic"></textarea>
				</div>
			</div><!-- End .form-group  -->
		</form>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript">
$('.approve_package_btn').click(function() {
	if(!confirm('Are you sure you want to approve the house and land?')){
		return false;
	}
	showLoading();
});
$(document).ready(function() {
	dialog = $( "#dialog-form" ).dialog({
		autoOpen: false,
		title: 'Reject House & Land Package',
		height: 270,
		width: 480,
		modal: true,
		buttons: {
			"Reject": function(){
				dialog.dialog('close');
				showLoading();
				$('#reject_form').submit();
			},
			Cancel: function(){
				$('#rejection_message').val('');
				dialog.dialog('close');
			}
		}
	});

	$('.reject_package_btn').click(function() {
		$('#reject_form').attr('action', "<?= "{$base_url}rejecthouselotpackage/{$development->development_id}/";?>"+$(this).attr('package_id'));
		dialog.dialog('open');
	});
})
</script>