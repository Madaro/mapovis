<?php
$base_url = base_url().'salesteam/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Available Lots Report</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-7"></span>
									<span>Your Developments</span>
								</h4>
							</div>
							<div class="panel-body noPad">
								<table class="table table-bordered">
									<thead>
									<tr>
										<th>Development</th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($my_developments as $development):?>
									<tr>
										<td><?= $development->development_name;?></td>
										<td>
											<div class="controls center">
												<a href="<?php echo $base_url; ?>availablelotsreport/<?= $development->development_id?>"><button class="btn btn-xs btn-default">View Report</button></a>
											</div>
										</td>
									</tr>
									<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span6 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
