<?php
$base_url = base_url().'salesteam/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Manage <?= "{$development->development_name} ({$development->developer})"; ?></h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<?= $alert_message;?>
				<!-- Build page from here: -->
				<div class="row">
					<?php if($media_agency):?>
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-settings"></span>
									<span>Development Settings</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>editdevelopment/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">Manage</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
					<?php endif;?>

					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-documents"></span>
									<span>Stages</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>viewstages/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">View Stages</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span4 -->

					<div class="col-lg-<?= ($media_agency)? 8:4;?>">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-document-2"></span>
									<span>Lots</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>updatedevelopmentid/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">View Lots</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

				</div><!-- End .row -->

				<div class="row">
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Houses</span>
								</h4>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>managehouses/<?= $development->development_id;?>"><button class="btn btn-primary">View</button></a>
								<a href="<?= $base_url; ?>addhouse/<?= $development->development_id;?>" style="padding-left: 20px;"><button class="btn btn-info">Add</button></a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-5"></span>
									<span>House & Land Packages</span>
								</h4>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>managehousematches/<?= $development->development_id;?>"><button class="btn btn-primary">Manage</button></a>

								<a href="<?= $base_url; ?>listpackages/<?= $development->development_id;?>" style="padding-left: 20px;"><button class="btn btn-info">View Packages</button></a>

								<?php if(count($pending_packages) != 0):?>
									<a href="<?= $base_url; ?>packagesforapproval/<?= $development->development_id;?>" style="padding-left: 20px;" <?= (count($pending_packages) == 0)?'disabled="disabled"':'';?>>
										<button class="btn btn-success">Approve Builder H&L Packages [<?= (int)@current($pending_packages);?>]</button>
									</a>
								<?php else:?>
									<span style="padding-left: 20px;">
									<button class="btn btn-default">Approve Builder H&L Packages [0]</button>
									</span>
								<?php endif;?>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
				</div><!-- End .row -->



				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-alarm"></span>
									<span>Price List Newsletter</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>managepricelistsubscribers/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">Subscribers</button>
								</a>

								<a href="<?= $base_url; ?>pricelistschedule/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Schedule</button>
								</a>

								<a href="<?= $base_url; ?>pricelistpdf/<?= $development->development_id;?>" style="padding-left: 20px;" target="_blank">
									<button type="button" class="btn btn-success">Download Current PDF</button>
								</a>

								<a href="<?= $base_url; ?>pricelistindivemail/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-primary">Send Individual Email</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
					<?php if(!$media_agency):?>
						<div class="col-lg-4">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4>
										<span class="icon16 entypo-icon-settings"></span>
										<span>Sales Office Settings</span>
									</h4>
									<a href="#" class="minimize" style="display: none;">Minimize</a>
								</div>
								<div class="panel-body">
									<a href="<?= $base_url; ?>editsalesoffice/<?= $development->development_id;?>">
										<button type="button" class="btn btn-primary">Manage</button>
									</a>
								</div>
							</div><!-- End .panel -->
						</div><!-- End .span3 -->
					<?php endif;?>
				</div><!-- End .row -->

				<div class="row">
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-alarm"></span>
									<span>Document Portal</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>developmentdocuments/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">Manage</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
                    
                    <?php if($development->developerid == 14) :?>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 icomoon-icon-alarm"></span>
                                    <span>Report</span>
                                </h4>
                                <a href="#" class="minimize" style="display: none;">Minimize</a>
                            </div>
                            <div class="panel-body">
                                <a href="<?= base_url(); ?>villawood_stock_analysis/createReport/<?= $development->development_id;?>">
                                    <button type="button" class="btn btn-primary">Stock Analysis Report Download</button>
                                </a>
                            </div>
                        </div><!-- End .panel -->
                    </div><!-- End .span3 -->
                     <?php endif;?>  
                     
					<?php if($manager || $media_agency):?>
						<div class="col-lg-4">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4>
										<span class="icon16 entypo-icon-settings"></span>
										<span>PDF Settings</span>
									</h4>
									<a href="#" class="minimize" style="display: none;">Minimize</a>
								</div>
								<div class="panel-body">
									<a href="<?= $base_url; ?>editdevelopment/<?= $development->development_id;?>">
										<button type="button" class="btn btn-primary">Manage</button>
									</a>
								</div>
							</div><!-- End .panel -->
						</div><!-- End .span3 -->
					<?php endif;?>
				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->