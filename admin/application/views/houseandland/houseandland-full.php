<?php
$based_url      = base_url().'application/views/houseandland/';
$image_base_url = base_url().'../mpvs/images/dev_houses/';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MAPOVIS - House & Land Page PROTOTYPE Example</title>

	<!-- Bootstrap Holder JS (for temporary thumbnail display)-->
	<script src="<?= $based_url; ?>assets/bootstrap/js/holder.js"></script>

	<!-- Bootstrap -->
	<link href="<?= $based_url; ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- Bootstrap theme -->
	<link href="<?= $based_url; ?>assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="<?= $based_url; ?>css/custom-css.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!--[if gte IE 9]>
  <style type="text/css">
	.gradient {
	   filter: none;
	}
  </style>
<![endif]-->
</head>
<body>
	<!-- THE BEGINNING - everything above this point will be on the developers website. Starting from here is what will be delivered to a developer site via a SimpleHtmlDom (curl) request   ============================= -->

	<div class="container">
	<!-- /START CONTAINER -- CURL will pull in from here onwards.
	================================================== -->
	<?php $this->load->view($page_view); ?>
	<!-- /END CONTAINER 
	================================================== -->
	</div>
	<!-- Bootstrap core JavaScript
	================================================== -->
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?= $based_url; ?>assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>