<?php
$based_url      = "http://app.mapovis.com.au/";
$image_base_url = $based_url.'../mpvs/images/dev_houses/';
$folder_url     = $based_url.'application/views/houseandland/';
?>
<div id="packages">

	<div class="for-sale-tab-header">
		<h1>Home & Land Packages</h1>

		<form action="#" method="GET">
		<section class="filter-bar-wrap">
			<div class="filter-bar-container">
				<section class="filter-item">
				<p>Max Price</p>
					<select id="price-min" name="houseland_price_to">
						<option value="">Any</option>
						<?php for($min_price = $search_parameters['house_land_price_min']+50000; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
							<option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
						<?php endfor;?>
						<option value="<?= $search_parameters['house_land_price_max'];?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $search_parameters['house_land_price_max'])? 'selected="selected"':'';?>>$<?= number_format(ceil($search_parameters['house_land_price_max']/1000));?>k</option>
					</select>
				</section>
				<section class="filter-item">
					<p>Bedrooms</p>
					<select id="bed" name="house_bedrooms">
						<option value="">Any</option>
						<?php foreach($search_parameters['house_bedrooms'] as $item):?>
							<option value="<?= $item['house_bedrooms'];?>" <?= (isset($form_data['house_bedrooms']) && $form_data['house_bedrooms'] == $item['house_bedrooms'])? 'selected="selected"':'';?>><?= $item['house_bedrooms'];?></option>
						<?php endforeach;?>
					</select>
				</section>
				<section class="filter-item" name="house_bathrooms">
					<p>Bathrooms</p>
					<select id="bed">
						<option value="">Any</option>
						<?php foreach($search_parameters['house_bathrooms'] as $item):?>
							<option value="<?= $item['house_bathrooms'];?>" <?= (isset($form_data['house_bathrooms']) && $form_data['house_bathrooms'] == $item['house_bathrooms'])? 'selected="selected"':'';?>><?= $item['house_bathrooms'];?></option>
						<?php endforeach;?>
					</select>
				</section>
				<section class="filter-item">
					<p>Builder</p>
					<select id="bed" name="builder_id">
						<option value="">Any</option>
						<?php foreach($search_parameters['builders'] as $item):?>
							<option value="<?= $item['builder_id'];?>" <?= (isset($form_data['builder_id']) && $form_data['builder_id'] == $item['builder_id'])? 'selected="selected"':'';?>><?= $item['builder_name'];?></option>
						<?php endforeach;?>
					</select>
				</section>
				<section class="filter-item">
					<p>Order Results By</p>
					<select id="bed" name="order_by">
						<?php foreach($search_parameters['order_by'] as $item):?>
						<option value="<?= $item;?>" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item)? 'selected="selected"':'';?>><?= ucwords($item);?></option>
						<?php endforeach;?>
					</select>
				</section>
			</div><!-- .filter-bar-container -->

			<div class="filter-bar-container">
				<p>Land Size</p>
				<ul class="inline-list">
					<?php foreach ($search_parameters['lot_square_meters'] as $item_value => $item_label):?>
						<li>
							<input name="lot_square_meters[]" type="checkbox" id="size_<?= $item_value;?>" value="<?= $item_value;?>" <?= (isset($form_data['lot_square_meters']) && in_array($item_value, $form_data['lot_square_meters']))? 'checked="checked"':'';?>> 
							<label for="size_<?= $item_value;?>"><?= str_replace(' ', '', $item_label);?></label>
						</li>
					<?php endforeach;?>
				</ul>
			</div>
		<div class="filter-bar-container" style="padding-bottom: 20px; text-align: center;">
			<input type="submit" value="Search">
		</div>
		</section>
		</form>
	</div>

	<section class="tab-area">
		<section class="grid-container">
			<div class="resp-tabs-container">
				<div class="tab-content house-and-land-tab-content-wrap">
					<div class="house-and-land-container">
						
					<?php if(empty($lots)):?>
						<h5>No house and land matches were found with this search criteria.</h5>
					<?php else:?>
					<?php foreach($lots as $lot_house):?>
						<?php $house_url = str_replace(' ', '-', strtolower($lot_house->house_name.'-'.$lot_house->builder_name));?>
						<section class="house-and-land-item" style="height: 330px;"
						data-filter-price="<?= $lot_house->house_lot_price;?>"
						data-filter-car="<?= $lot_house->house_garages;?>"
						data-filter-bed="<?= $lot_house->house_bedrooms;?>"
						data-filter-builder="<?= str_replace(' ', '-', strtolower($lot_house->builder_name));?>`" >

							<?php if(isset($houses_images[$lot_house->house_id])):?>
								<img src="<?= $image_base_url.$houses_images[$lot_house->house_id]->file_name;?>" class="fw" />
							<?php endif?>

							<ul class="download-links inline-list">
								<?php if(!empty($lot_house->file_name)):?>
								<li class="d-flyer"><a href="<?= $based_url.'mpvs/templates/lot_house_pdfs/'.$lot_house->file_name;?>" target="_blank">Download Flyer</a></li>
								<?php endif;?>
								<li class="v-map"><a class="icon-pin-circle masterplan-track lightbox" href="#" id="<?= $lot_house->lot_number;?>" zoom_to_lot="<?= $lot_house->lot_number;?>" zoom_level="19" data-placement="top" data-toggle="tooltip" title="View on Masterplan">View on Masterplan</a></li>
							</ul>

							<div style="height: 63px;">
								<h4><?= $lot_house->house_name;?> &#8211; <?= $lot_house->builder_name;?></h4>
								<p>Lot <?= $lot_house->lot_number;?>, Highgrove</p>
							</div>


							<section class="package-specs">
								<span class="bed"> <?= $lot_house->house_bedrooms;?></span>
								<span class="bath"><?= $lot_house->house_bathrooms;?></span>
								<span class="garage"><?= $lot_house->house_garages;?></span>
							</section><!-- .package-specs -->

							<section class="package-price">
								<p class="price">$<?= number_format($lot_house->house_lot_price);?></p>
							</section><!-- .package-price -->
							<hr/>

						</section><!-- .house-and-land-item -->
					<?php endforeach;?>
				<?php endif;?>

				</div><!-- .house-and-land-containter -->
			</div><!-- tab content -->

		</div>
		</section><!-- container -->
	</section><!-- tab area -->
</div><!-- horozontal tab-->