<?php
$base_url       = 'http://app.mapovis.com.au/';
$image_base_url = $base_url.'mpvs/images/dev_houses/';
?>
<a name="house_land"></a>
<div class="search-cont clearfix">
                	<div class="search-sidebar">
                		<h3>Search House & land</h3>                        
                        <form action="#house_land" class="side-form land-form">
                        	<label class="icon icon-price" >Price</label>
                            <fieldset>
                            	<select name="houseland_price_from">
                                  <option value="">From Any</option>
									<?php for($min_price = $search_parameters['house_land_price_min']; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
										<option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_from']) && $form_data['houseland_price_from'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
									<?php endfor;?>
                                </select>
                                <select name="houseland_price_to">
                                  <option value="">To Any</option>
									<?php if($search_parameters['house_land_price_max']):?>
										<?php for($min_price = $search_parameters['house_land_price_min']+50000; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
											<option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
										<?php endfor;?>
										<option value="<?= $search_parameters['house_land_price_max'];?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $search_parameters['house_land_price_max'])? 'selected="selected"':'';?>>$<?= number_format(ceil($search_parameters['house_land_price_max']/1000));?>k</option>
									<?php endif;?>
                                </select>
                            </fieldset>
                            <label class="icon icon-size2" >Land Size</label>
                            <fieldset>
                            	<select name="hnl_lot_square_meters_from">
                                  <option value="">From Any</option>
									<?php foreach($search_parameters['lot_square_meters_from'] as $value => $label):?>
										<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_from']) && $form_data['lot_square_meters_from'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
									<?php endforeach;?>
                                </select>
                                <select name="hnl_lot_square_meters_to">
									<option value="">To Any</option>
									<?php foreach($search_parameters['lot_square_meters_to'] as $value => $label):?>
										<option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_to']) && $form_data['lot_square_meters_to'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
									<?php endforeach;?>
                                </select>
                            </fieldset>
                            <fieldset class="labels">
                            	<label class="icon icon-bed" >Bed</label>
                                <label class="icon icon-bath">Bath</label>
                            </fieldset>
                            <fieldset>
                            	<select name="house_bedrooms">
                                  <option value="">Any</option>
									<?php foreach($search_parameters['house_bedrooms'] as $item):?>
										<option value="<?= $item['house_bedrooms'];?>" <?= (isset($form_data['house_bedrooms']) && $form_data['house_bedrooms'] == $item['house_bedrooms'])? 'selected="selected"':'';?>><?= $item['house_bedrooms'];?></option>
									<?php endforeach;?>
                                </select>
                                <select name="house_bathrooms">
									<option value="">Any</option>
									<?php foreach($search_parameters['house_bathrooms'] as $item):?>
										<option value="<?= $item['house_bathrooms'];?>" <?= (isset($form_data['house_bathrooms']) && $form_data['house_bathrooms'] == $item['house_bathrooms'])? 'selected="selected"':'';?>><?= $item['house_bathrooms'];?></option>
									<?php endforeach;?>
                                </select>
                            </fieldset>
                            <label class="icon icon-builder">Builder</label>
                            <select name="builder_id">
								<option value="">Any</option>
								<?php foreach($search_parameters['builders'] as $item):?>
									<option value="<?= $item['builder_id'];?>" <?= (isset($form_data['builder_id']) && $form_data['builder_id'] == $item['builder_id'])? 'selected="selected"':'';?>><?= $item['builder_name'];?></option>
								<?php endforeach;?>
                            </select>
                            <label class="icon icon-levels">Levels</label>
                            <select name="house_levels">
								<option value="">Any</option>
								<?php foreach($search_parameters['house_levels'] as $item):?>
									<option value="<?= $item['house_levels'];?>" <?= (isset($form_data['house_levels']) && $form_data['house_levels'] == $item['house_levels'])? 'selected="selected"':'';?>><?= $item['house_levels'];?></option>
								<?php endforeach;?>
                            </select>
                            <label class="">Order results by</label>
                            <select name="order_by">
								<?php foreach($search_parameters['order_by'] as $item):?>
									<?php if($item == 'builder'):?>
										<option value="<?= $item;?>|asc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|asc')? 'selected="selected"':'';?>><?= ucwords($item);?> Name</option>
									<?php else:?>
										<option value="<?= $item;?>|asc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|asc')? 'selected="selected"':'';?>><?= ucwords($item);?>: Low to High</option>
										<option value="<?= $item;?>|desc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|desc')? 'selected="selected"':'';?>><?= ucwords($item);?>: High to Low</option>
									<?php endif;?>
								<?php endforeach;?>
                            </select>
                            <input type="submit" value="Search Homes" class="button">
                        </form>
                        
						<a href="#" class="masterplan-btn lightbox">View Interactive<br />Masterplan</a>
                        
                	</div><!--end of search-sidebar--> 
                    <div class="search-table">
                		<h3>Choose a home <br />to suit your lifestyle</h3>
                        
						<?php if(empty($houses)):?>
							<h6>No house and land matches were found with this search criteria.</h6>
						<?php else:?>
						<?php foreach($houses as $house):?>
                      	<div class="house-item clearfix">
                        	<div class="col6">
                            	<h4><?= $house->house_name;?></h4>
                                <span class="houseby">By <?= $house->builder_name;?></span>   
								<?php if(isset($houses_images[$house->house_id])):?>
									<img class="show-mobile" src="<?= $image_base_url.$houses_images[$house->house_id]->file_name;?>" alt=""/>
								<?php endif?>
                                <span class="house-price">From $<?= number_format($house->min_lot_house_cost, 0);?></span> <span class="house-size"><?= ($house->house_size != '0')? $house->house_size.' <sup>Sq</sup>':''; ?></span>
                                <ul class="house-info">
                                	<li class="house-beds"><span><?= $house->house_bedrooms;?></span></li>
                                	<li class="house-baths"><span><?= $house->house_bathrooms;?></span></li>
                                	<li class="house-slots"><span><?= $house->house_garages;?></span></li>
                                </ul>
                                <a href="#" class="button viewland" onclick="return false">View Suitable Land Lots</a>
                            </div> 
                            <div class="col6 hide-mobile">
                            	<img src="<?= $image_base_url.$houses_images[$house->house_id]->file_name;?>" alt=""/>
                            </div> 
                        </div> <!--end of house-item-->  
                         <div class="stage-two clearfix">
							<?php foreach($lots[$house->house_id] as $stage_id => $lots_array):?>
							<?php $stage = $stages[$stage_id];?>
	                         <h6 style="padding-bottom:10px;"><?= $stage->stage_name;?></h6>
	                         
	                         <div class="results">
								<?php foreach($lots_array as $lot):?>
		                         <ul>
			                         <li>LOT <?= $lot->lot_number;?></li>
			                         <li>$<?= number_format($lot->house_lot_price, 0);?></li>
			                         <li><?= $lot->lot_square_meters;?>sqm Size</li>
			                         <li><?= (float)$lot->lot_width;?>sqm Frontage</li>
			                         <li class="icon land">
										<a class="lightbox" href="#" id="<?= $lot->lot_number;?>" zoom_to_lot="<?= $lot->lot_number;?>" zoom_level="19">
											<i class="icon-open-masterplan"></i>
										</a>
									 </li>
									 <li class="icon lot select">
										<?php if(!empty($lot->file_name)):?>
											<a class="icon-download" href="<?= $base_url.'mpvs/templates/lot_house_pdfs/'.$lot->file_name;?>" target="_blank">
												<i class="icon-download-pdf"></i>
											</a> 
										<?php endif;?>
									 </li>
			                         <li class="icon msg"></li>
			                        
		                         </ul>
		                        	 <div class="land_forms">
				                         <form method="post" action="http://www.tfaforms.com/responses/processor" class="hintsBelow labelsAbove" id="tfa_<?= $lot->lot_id;?>_0">                    
											 <div class="half first">

 												<div class="form-row clearfix require" id="tfa_1-D"> 
						                         <label id="tfa_1-L" for="tfa_1" class="label preField reqMark">Salutation</label>
						                            <select id="tfa_1" name="tfa_1" class="required" >
						                            	<option value="">Salutation</option>
						                             	<option value="tfa_9" id="tfa_9">Mr</option>
						                              	<option value="tfa_10" id="tfa_10">Mrs</option>
						                             	<option value="tfa_11" id="tfa_11">Ms</option>
						                             	<option value="tfa_12" id="tfa_12">Miss</option>
						                            </select>
                        						</div>
                        						
						                        <div class="form-row two-fields clearfix">
							                        <div id="tfa_2-D" class="oneField    ">
							                        	<label id="tfa_2-L" for="tfa_<?= $lot->lot_id;?>_2" class="label preField reqMark">First Name</label>
							                           	<input name="tfa_2" type="text" class="ff-first-name required" placeholder="First name*" id="tfa_<?= $lot->lot_id;?>_2" >
							                        </div>
							                        <div id="tfa_3-D" class="oneField right">
							                        	<label id="tfa_3-L" for="tfa_<?= $lot->lot_id;?>_3" class="label preField reqMark">Surname</label>
							                            <input name="tfa_3" type="text" class="ff-last-name last required" placeholder="Surname*" id="tfa_<?= $lot->lot_id;?>_3">
							                        </div>
						                        </div>  
						                        <div class="form-row clearfix" id="tfa_<?= $lot->lot_id;?>_13-D"> 
						                        <label id="tfa_<?= $lot->lot_id;?>_13-L" for="tfa_<?= $lot->lot_id;?>_13" class="label preField reqMark">How did you hear about us?</label>
						                            <select id="tfa_<?= $lot->lot_id;?>_13" name="tfa_13" >
						                              		<option value="">How did you hear about us?</option>
															<option value="tfa_14" id="tfa_14" class="">Google</option>
															<option value="tfa_15" id="tfa_15" class="">Realestate.com.au</option>
															<option value="tfa_17" id="tfa_17" class="">Signage</option>
															<option value="tfa_18" id="tfa_18" class="">Newspaper</option>
															<option value="tfa_19" id="tfa_19" class="">Oliver Hume Website</option>
								                            <option value="tfa_20" id="tfa_20" class="">Other</option>
						                            </select>
						                        </div>
											 </div>
											 <div class="half">
						                        <div class="form-row two-fields clearfix">
						                        	<div id="tfa_<?= $lot->lot_id;?>_4-D" class="oneField   right ">
														<label id="tfa_<?= $lot->lot_id;?>_4-L" for="tfa_<?= $lot->lot_id;?>_4" class="label preField reqMark">Email</label>
														<input name="tfa_4" type="email" class="ff-email last required" placeholder="Email*" id="tfa_<?= $lot->lot_id;?>_4">
						                        	</div>
						                        	<div id="tfa_<?= $lot->lot_id;?>_5-D" class="oneField">
						                        		<label id="tfa_<?= $lot->lot_id;?>_5-L" for="tfa_<?= $lot->lot_id;?>_5" class="label preField reqMark">Phone </label>
						                            	<input name="tfa_5" type="text" class="ff-telephone required" placeholder="Phone*" id="tfa_<?= $lot->lot_id;?>_5">
						                        	</div>
						                        	
						                        	<div id="tfa_<?= $lot->lot_id;?>_10-D" class="oneField right ">
														<label id="tfa_<?= $lot->lot_id;?>_10-L" for="tfa_<?= $lot->lot_id;?>_10" class="label preField reqMark">Postcode</label>
														<input name="tfa_10" type="text" class="ff-postcode last required" placeholder="Postcode" id="tfa_<?= $lot->lot_id;?>_10">
						                        	</div>
						                        </div>                                                
											 </div>
					                        <div class="actions" id="tfa_<?= $lot->lot_id;?>_0-A"><input type="submit" value="Submit" class="primaryAction"> </div>
					                        <p class="required">*Required field</p>
					                        <input type="hidden" value="354280" name="tfa_dbFormId" id="tfa_dbFormId"><input type="hidden" value="" name="tfa_dbResponseId" id="tfa_dbResponseId"><input type="hidden" value="d56bf86eca484f130ed9ceba6e36445f" name="tfa_dbControl" id="tfa_dbControl"><input type="hidden" value="2" name="tfa_dbVersionId" id="tfa_dbVersionId"><input type="hidden" value="" name="tfa_switchedoff" id="tfa_switchedoff">
					                    </form>
			                         </div>
								<?php endforeach;?>

		                         </div><!-- results -->
		                         
								<?php endforeach;?>
	                         </div><!-- stage two -->
							<?php endforeach;?>
						<?php endif;?>

                	</div><!--end of search-table-->                 
                </div>