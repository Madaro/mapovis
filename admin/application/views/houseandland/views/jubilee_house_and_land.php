<?php
$based_url      = "http://app.mapovis.com.au/";
$image_base_url = $based_url.'../mpvs/images/dev_houses/';
$folder_url     = $based_url.'application/views/houseandland/';
?>
                            <div class="vc_row wpb_row section vc_row-fluid grid_section" style=' padding-top:50px; padding-bottom:100px; text-align:left;'>
                                <div class=" section_inner clearfix">
                                    <div class='section_inner_margin clearfix'>
                                        <div class="vc_col-sm-12 wpb_column vc_column_container">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <div class="vc_row wpb_row section vc_row-fluid grid_section" style=" padding-bottom:100px; text-align:left;">
                                                            <div class=" section_inner clearfix">
                                                                <div class="section_inner_margin clearfix">
                                                                    <div class="vc_col-sm-12 wpb_column vc_column_container">
                                                                        <div class="wpb_wrapper">
                                                                            <div class="wpb_text_column wpb_content_element">
                                                                                <div class="wpb_wrapper">
                                                                                    <div class="search-section clearfix">
                                                                                        <form href="#house_and_land" action="" id="myform" method="get" name="myform">
                                                                                            <div class="search-section-left clearfix">
                                                                                                <div class="search-select builder">
                                                                                                    <label>Builder</label> <select class="selectbox" name="builder_id" tabindex="1">
																										<option value="">Any</option>
																										<?php foreach($search_parameters['builders'] as $item):?>
																											<option value="<?= $item['builder_id'];?>" <?= (isset($form_data['builder_id']) && $form_data['builder_id'] == $item['builder_id'])? 'selected="selected"':'';?>><?= $item['builder_name'];?></option>
																										<?php endforeach;?>
                                                                                                    </select>
                                                                                                </div>

                                                                                                <div class="search-select bedrooms">
                                                                                                    <label>Bedrooms</label> <select class="selectbox" name="house_bedrooms" tabindex="2">
																										<option value="">Any</option>
																										<?php foreach($search_parameters['house_bedrooms'] as $item):?>
																											<option value="<?= $item['house_bedrooms'];?>" <?= (isset($form_data['house_bedrooms']) && $form_data['house_bedrooms'] == $item['house_bedrooms'])? 'selected="selected"':'';?>><?= $item['house_bedrooms'];?></option>
																										<?php endforeach;?>
                                                                                                    </select>
                                                                                                </div>

                                                                                                <div class="search-select bathrooms">
                                                                                                    <label>Bathrooms</label> <select class="selectbox" name="house_bathrooms" tabindex="3">
																										<option value="">Any</option>
																										<?php foreach($search_parameters['house_bathrooms'] as $item):?>
																											<option value="<?= $item['house_bathrooms'];?>" <?= (isset($form_data['house_bathrooms']) && $form_data['house_bathrooms'] == $item['house_bathrooms'])? 'selected="selected"':'';?>><?= $item['house_bathrooms'];?></option>
																										<?php endforeach;?>
                                                                                                    </select>
                                                                                                </div>

                                                                                                <div class="search-select levels">
                                                                                                    <label>Levels</label> <select class="selectbox" name="house_levels" tabindex="4">
																										<option value="">Any</option>
																										<?php foreach($search_parameters['house_levels'] as $item):?>
																											<option value="<?= $item['house_levels'];?>" <?= (isset($form_data['house_levels']) && $form_data['house_levels'] == $item['house_levels'])? 'selected="selected"':'';?>><?= $item['house_levels'];?></option>
																										<?php endforeach;?>
                                                                                                    </select>
                                                                                                </div>

                                                                                                <div class="search-select price">
                                                                                                    <label>Price</label> <select class="selectbox" name="houseland_price" tabindex="5">
																										<option value="">Any</option>
																										<?php for($min_price = $search_parameters['house_land_price_min']; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
																										<?php $max_price = $min_price + 50000;?>
																										<?php $price = $min_price.'|'. $max_price;?>
																											<option value="<?= $price;?>" <?= (isset($form_data['houseland_price']) && $form_data['houseland_price'] == $price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k to $<?= number_format(ceil($max_price/1000));?>k</option>
																										<?php endfor;?>
                                                                                                    </select>
                                                                                                </div>

                                                                                                <div class="search-select land-size">
                                                                                                    <label>Land Size</label> <select class="selectbox" name="lot_square_meters[]" tabindex="6">
																										<option value="">Any</option>
																										<?php foreach($search_parameters['hnl_square_meters'] as $item_value => $item_label):?>
																											<option value="<?= $item_value;?>" <?= (isset($form_data['lot_square_meters']) && ($form_data['lot_square_meters'] == $item_value || in_array($item_value, $form_data['lot_square_meters'])))? 'selected="selected"':'';?>><?= $item_label;?></option>
																										<?php endforeach;?>
                                                                                                    </select>
                                                                                                </div>

                                                                                                <div class="search-select order-by">
                                                                                                    <label>Order Results By</label> <select class="selectbox" name="order_by" tabindex="7">
																										<?php foreach($search_parameters['order_by'] as $item):?>
																										<option value="<?= $item;?>" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item)? 'selected="selected"':'';?>><?= ucwords($item);?></option>
																										<?php endforeach;?>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="search-section-right">
                                                                                                <input class="reset" tabindex="8" title="Reset" type="reset" value="Reset"> <input class="search-result" tabindex="9" title="Search" type="submit" value="Search">
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>

                                                                                    <div class="our-plan clearfix">
                                                                                        <div class="download-stage">
                                                                                            <label>Download Stage Release:</label> <select class="selectbox" name="download-stage-release" tabindex="10">
                                                                                                <option value="0">
                                                                                                    Choose Release Plan
                                                                                                </option>
																								<?php foreach($stages as $stage):?>
																									<option value="http://app.mapovis.com.au/admin/apipdf/devstagepdffile/<?= $development->development_id;?>/<?= $stage->stage_number;?>">
																										<?= $stage->stage_name;?>
																									</option>
																								<?php endforeach;?>
                                                                                            </select>
                                                                                        </div>

                                                                                        <div class="view-plan">
                                                                                            <a class="view-master-plan lightbox" href="#" title="View Interactive Masterplan">View Interactive Masterplan</a> 
                                                                                            <a class="lightbox promotional-banner" title="Promotional Banner" href="http://app.mapovis.com.au/mapovis/development.php?developmentId=81&amp;zoom_to_lot=618&amp;zoom_level=19&amp;lightbox[iframe]=true&amp;lightbox[modal]=false&amp;lightbox[move]=false&amp;lightbox[width]=90p&amp;lightbox[height]=882" id="618" zoom_to_lot="618" zoom_level="19">
                                                                                                <img class="promotional-banner" src="http://myjubilee.wpengine.com/wp-content/themes/myjubilee/images/house-and-land-promo.gif">
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="search-listing">
																						<?php if(empty($lots)):?>
																							<h5>No house and land matches were found with this search criteria.</h5>
																						<?php else:?>
																						<?php foreach($lots as $lot_house):?>
                                                                                        <div class="listing-detail clearfix">
                                                                                            <div class="property-image">
                                                                                                <div class="jcarousel-wrapper">
                                                                                                    <div class="jcarousel">
                                                                                                        <ul>
																											<?php if(isset($houses_images[$lot_house->house_id])):?>
																												<?php foreach($houses_images[$lot_house->house_id] as $house_image):?>
																													<li><img alt="property-image" src="<?= $image_base_url.$house_image->file_name;?>" title="property-image"></li>
																												<?php endforeach;?>
																											<?php endif;?>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="property-info">
                                                                                                <h2><?= $lot_house->house_name;?></h2>

                                                                                                <p>Lot <?= $lot_house->lot_number;?> <?= $lot_house->street_name?>, <?=$development->suburb;?> <span>Land Size: <?= $lot_house->lot_square_meters;?>m<sub>2</sub></span></p>
																								<?= $lot_house->description;?>
                                                                                            </div>

                                                                                            <div class="property-price">
                                                                                                <ul class="add-info clearfix">
                                                                                                    <li><span class="number"><?= $lot_house->house_bedrooms;?></span><i class="icon-bed"></i></li>

                                                                                                    <li><span class="number"><?= $lot_house->house_bathrooms;?></span><i class="icon-bath"></i></li>

                                                                                                    <li><span class="number"><?= $lot_house->house_garages;?></span><i class="icon-car"></i></li>

                                                                                                    <li>
                                                                                                        <a class="tooltips masterplan-track lightbox" href="#" id="<?= $lot_house->lot_number;?>" zoom_to_lot="<?= $lot_house->lot_number;?>" zoom_level="19" data-placement="top" data-toggle="tooltip" ><i class="icon-view"></i><span>View on masterplan</span></a>
                                                                                                    </li>
                                                                                                </ul>

                                                                                                <div class="fixed-price">
                                                                                                    <span>Fixed Price House & Land</span> <span class="price-tag">$<?= number_format($lot_house->house_lot_price);?>*</span>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="property-enquire">
																								<?php if(!empty($lot_house->file_name)):?>
                                                                                                <a href="<?= $based_url.'mpvs/templates/lot_house_pdfs/'.$lot_house->file_name;?>" title="Download"  target="_blank">Download</a>
																								<?php endif;?> 
																								<a class="enquire-now" href="javascript:void(0)" title="Enquire Now">Enquire Now</a>
                                                                                            </div>
                                                                                        </div>
																						<?php endforeach;?>
																						<?php endif;?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>