<?php
$based_url      = "http://stage3.mapovis.com.au/";
$image_base_url = base_url().'../mpvs/images/dev_houses/';
$folder_url     = base_url().'application/views/houseandland/';
?>
<div class="mega-list">
<!-- <div class="col-lg-12">
	<h2>House &amp; Land For Sale At Kallo.</h2>
</div> -->

<!--SEARCH BOX -->
<div class="col-lg-3 col-md-3">
	<form action="#" class="search-form" method="post">
		<fieldset>
			<h3>Refine Your Search</h3>
			<fieldset class="block">
				<legend class="icon price">PRICE</legend>
				<div class="cols">
					<div class="col">
						<select class="custom-select" name="houseland_price_from">
							<option value="">From</option>
							<?php for($min_price = $search_parameters['house_land_price_min']; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
								<option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_from']) && $form_data['houseland_price_from'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
							<?php endfor;?>
						</select>
					</div>
					<div class="col">
						<select class="custom-select" name="houseland_price_to">
							<option value="">To</option>
							<?php for($min_price = $search_parameters['house_land_price_min']+50000; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
								<option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
							<?php endfor;?>
							<option value="<?= $search_parameters['house_land_price_max'];?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $search_parameters['house_land_price_max'])? 'selected="selected"':'';?>>$<?= number_format(ceil($search_parameters['house_land_price_max']/1000));?>k</option>
						</select>
					</div>
				</div>
			</fieldset>
			<fieldset class="block">
				<legend class="icon size">LAND SIZE m<sup>2</sup></legend>
				<div class="col">
					<ul class="radio-holder">
						<?php foreach ($search_parameters['lot_square_meters'] as $item_value => $item_label):?>
							<li>
								<input name="lot_square_meters[]" type="checkbox" id="size_<?= $item_value;?>" value="<?= $item_value;?>" <?= (isset($form_data['lot_square_meters']) && in_array($item_value, $form_data['lot_square_meters']))? 'checked="checked"':'';?>>
								<label for="size_<?= $item_value;?>"><?= $item_label;?></label>
							</li>
						<?php endforeach;?>
					</ul>
				</div>
			</fieldset>
			<div class="block">
				<div class="cols">
					<fieldset class="col">
						<legend class="icon bed">BED</legend>
						<select class="custom-select" name="house_bedrooms">
							<option value="">Any</option>
							<?php foreach($search_parameters['house_bedrooms'] as $item):?>
								<option value="<?= $item['house_bedrooms'];?>" <?= (isset($form_data['house_bedrooms']) && $form_data['house_bedrooms'] == $item['house_bedrooms'])? 'selected="selected"':'';?>><?= $item['house_bedrooms'];?></option>
							<?php endforeach;?>
						</select>
					</fieldset>
					<fieldset class="col">
						<legend class="icon bath">BATH</legend>
						<select class="custom-select" name="house_bathrooms">
							<option value="">Any</option>
							<?php foreach($search_parameters['house_bathrooms'] as $item):?>
								<option value="<?= $item['house_bathrooms'];?>" <?= (isset($form_data['house_bathrooms']) && $form_data['house_bathrooms'] == $item['house_bathrooms'])? 'selected="selected"':'';?>><?= $item['house_bathrooms'];?></option>
							<?php endforeach;?>
						</select>
					</fieldset>
				</div>
			</div>


			<div class="block">
                    <fieldset class="col">
                        <legend>BUILDER</legend>
						<select class="custom-select" name="builder_id">
							<option value="">Any</option>
							<?php foreach($search_parameters['builders'] as $item):?>
								<option value="<?= $item['builder_id'];?>" <?= (isset($form_data['builder_id']) && $form_data['builder_id'] == $item['builder_id'])? 'selected="selected"':'';?>><?= $item['builder_name'];?></option>
							<?php endforeach;?>
						</select>
                    </fieldset>
            </div>
            <div class="block">
                    <fieldset class="col">
                        <legend>LEVELS</legend>
                        <select class="custom-select" name="house_levels">
							<option value="">Any</option>
							<?php foreach($search_parameters['house_levels'] as $item):?>
								<option value="<?= $item['house_levels'];?>" <?= (isset($form_data['house_levels']) && $form_data['house_levels'] == $item['house_levels'])? 'selected="selected"':'';?>><?= $item['house_levels'];?></option>
							<?php endforeach;?>
						</select>
                    </fieldset>
           </div>
        </fieldset>



			<div class="block">
				<div class="cols">
					<legend class="order-lable">ORDER RESULTS BY</legend>
					<select class="custom-select" name="order_by">
						<?php foreach($search_parameters['order_by'] as $item):?>
						<option value="<?= $item;?>" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item)? 'selected="selected"':'';?>><?= ucwords($item);?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
			<div class="btn-holder">
				<input type="submit" value="Search" class="btn btn-primary">
			</div>
		</fieldset>
	</form>
</div>
<!--END OF SEARCH BOX-->
<!-- START RESULT LIST -->
<div class="col-lg-9 col-md-9">
	<?php if(empty($lots)):?>
		<h5>No house and land matches were found with this search criteria.</h5>
	<?php else:?>
	<div class="land-block-container">
		<div class="list-content-holder">
			<ul class="list-content">
			<?php foreach($lots as $lot):?>
				<li <?= ($lot->status == 'Sold')? 'class="sold"':''; ?>>
					<ul class="data-holder">
						<li class="col01">
							<span>Lot <?= $lot->lot_number;?></span>
						</li>
						<li class="col02">
							<b class="icon size"><?= $lot->lot_square_meters;?>m<sup>2</sup></b> Land Size
						</li>
						<li class="col03">
							House &amp; Land From<b class="price">$<?= number_format($lot->min_lot_house_cost);?></b>
						</li>
						<li class="col04">
							<b class="available"><span class="glyphicon glyphicon-ok"></span> AVAILABLE</b>
							<b class="sold"><span class="glyphicon glyphicon-remove"></span> sold</b>
						</li>
						<li class="col05">
							<span class="icon enquire" title="Enquire">Enquire</span>
						</li>
					</ul>
					<div class="list-content-slide-add">
						<?php foreach($houses[$lot->lot_id] as $house):?>
						<div class="land-add">
							<section class="land-block">
								<div class="media">
									<?php if(isset($houses_images[$lot->lot_id][$house->house_id])):?>
										<img src="<?= $image_base_url. $houses_images[$lot->lot_id][$house->house_id]->file_name; ?>" alt="<?= $house->house_name;?>">
									<?php endif;?>
								</div>
								<div class="entry-content">
									<div class="heading">
										<h3><!--<a href="<?= $house->house_moreinfo_url;?>" target="_blank">--><?= strtoupper($house->house_name);?><!--</a>--></h3>
										<span class="by">By <!-- <a href="<?= $house->builder_website;?>" target="_blank">--><?= $house->builder_name;?><!--</a>--></span>
									</div>
									<span class="icon size"><?= $house->house_size;?> <sup>sq</sup></span>
									<dl>
										<dt class="icon bed" title="Bed">Bed</dt>
										<dd><?= $house->house_bedrooms;?></dd>
										<dt class="icon bath" title="Bath">Bath</dt>
										<dd><?= $house->house_bathrooms;?></dd>
										<dt class="icon car" title="Car">Car</dt>
										<dd><?= $house->house_garages;?></dd>
									</dl>
								</div>
								<div class="entry-meta">
									<dl>
										<dt>House &amp; Land From</dt>
										<dd>$<?= number_format($house->house_lot_price);?> <span class="asterisk">*</span></dd>
									</dl>
									<?php if(!empty($house->file_name)):?>
									<a href="<?= base_url().'../mpvs/templates/lot_house_pdfs/'.$house->file_name;?>" target="_blank" class="btn btn-primary download" onclick="">
											<span>DOWNLOAD PDF</span>
										</a>
									<?php endif;?>
									<a href="#" class="btn btn-default enquire-now">Enquire now</a>
								</div>
							</section>
							<div class="data-content-add">
								<div class="holder">
									<div class="col">
					<form class="enquire-form validate-form wpcf7-form" method="post" action="http://rpm.runway.com.au/actions/form/globalformaction.jsp">
						<fieldset>
							<b class="title">Enquire Now</b>
							<div class="holder">
								<div class="block">
									<span class="wpcf7-form-control-wrap ">
										<input type="text" name="Name" class="wpcf7-text wpcf7-validates-as-required required name" placeholder="Name *">
									</span>
								</div>
								<div class="block">
									<span class="wpcf7-form-control-wrap">
										<input type="text" name="Phone" class="wpcf7-text wpcf7-validates-as-required required" placeholder="Phone *">
									</span>
								</div>
								<div class="block">
									<span class="wpcf7-form-control-wrap">
										<input type="email" name="Email" class="wpcf7-text wpcf7-validates-as-email wpcf7-validates-as-required required-email" placeholder="Email *">
									</span>
								</div>
								<div class="block">
									<span class="wpcf7-form-control-wrap">
										<input type="text" name="Suburb" class="wpcf7-text" placeholder="Suburb">
									</span>
								</div>
								<div class="block">
									<span class="wpcf7-form-control-wrap">
										<select class="wpcf7-select jcf-hidden source-enquiry" name="Answer0W1S4O0O0R2J0T667S8P436W5H7B">
											<option value="">Source of Enquiry</option>
											<option value="Online">Online</option>
											<option value="Signage">Signage</option>
											<option value="Newspaper">Newspaper</option>
											<option value="Word of Mouth">Word of Mouth</option>
											<option value="Other">Other</option>
										</select>
									</span>
								</div>
								<div class="block">
                                                    <span class="wpcf7-form-control-wrap">
                                                        <select class="wpcf7-select jcf-hidden call-back" name="Answer011U4M047W1T16549K617B8O6V48">
                                                          <!--<option value=""></option>-->
                                                          <option value="">Best Call Back Time</option>
                                                          <option value="Before 9am">Before 9am</option>
                                                          <option value="9am to 12pm">9am to 12pm</option>
                                                          <option value="12pm to 3pm">12pm to 3pm</option>
                                                          <option value="3pm to 5pm">3pm to 5pm</option>
                                                          <option value="After 5pm">After 5pm</option>
                                                        </select>
                                                    </span>
                                                </div>
								<div class="block other">
									<span class="wpcf7-form-control-wrap">
										<input type="text" name="Answer0G1C430Q06250H6X8E6Z2X8E864L" class="wpcf7-text" placeholder="Please Specify">
									</span>
								</div>
								<div class="block">
									<div class="btn-submit">
										<input type="submit" class="wpcf7-submit gaTracking" value="ENQUIRE"  data-ga-action="submit" data-ga-category="Enquiry" data-ga-title="Register" >
									</div>
								</div>
							</div>
						</fieldset>
						<input type="hidden" name="GroupID" value="0R163N918R6Z6K9C0D2K4N1S3111">
						<input type="hidden" name="NewContactStatusID" value="2f3c4880-dcd5-102c-8f29-644220d7d8b3">                                                                                                                        <input type="hidden" name="LocationID" value="0915309M8K6Z6K9R238Z431Y341V">
						<input type="hidden" name="Source" value="Kallo website">
						<input type="hidden" name="NotificationTemplateID" value="0B1T3Q1C16743R0Z09472F1X430U">
						<input type="hidden" name="TemplateID" value="0N1F3T9I8L9Q1W944R9L493K5E9H" >
						<input type="hidden" name="-redirect"  value="">   
						<input type="hidden" name="-alwaysnotify" value="true" />
						<!-- SET the value attribute dynamically to the URL of the page you want to redirect on submission of form     -->
						<input type="hidden" name="FirstName"  value="" class="FirstName">
						<input type="hidden" name="LastName"  value="" class="LastName">
						<input type="hidden" name="QuestionID" value="011U4M047W1T16549K617B8O6V48" />
                        <input type="hidden" name="Quantifier" value="" />

						<!--lot number-->
						<input type="hidden" name="Answer0L1H400A2C5O3M4N0G7X9J2K3901" value="<?= $lot->lot_number;?>">
						<!--house name-->
						<input type="hidden" name="Answer051M43032W5I374E100J6A6Y3V7W" value="<?= $house->house_name;?>">
						<!--builder-->
						<input type="hidden" name="Answer0D1H4L0X2R5B324X1D328V2O706B" value="<?= $house->builder_name;?>">
					</form>
									</div>
									<div class="col">
										<img src="http://www.kallo.com.au/wp-content/themes/kallo/images/enquiry-banner.png" class="img-responsive"/>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach;?>
					</div>
				</li>
			<?php endforeach;?>
			</ul>
		</div>
	</div>
	<?php endif;?>
</div>
<!--END RESULT LIST-->
</div>
