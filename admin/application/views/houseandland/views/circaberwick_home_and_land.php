<?php
    $based_url      = "http://app.mapovis.com.au/";
    $image_base_url = $based_url.'../mpvs/images/dev_houses/';
    $folder_url     = $based_url.'application/views/houseandland/';
?>
<div id="house_land_wrapper">

    <div class="col_search">
        <form>
            <script type="text/javascript" src="http://www.carloscastillo.com.au/client/circa/wp-content/themes/circa/js/jquery.selectbox-0.2_custom.js"></script>
            <script type="text/javascript">
                $(function () {
                    $("#sizeFrom").selectbox();
                    $("#sizeTo").selectbox();
                    $("#priceTo").selectbox();
                    $("#priceFrom").selectbox();
                    $("#bed").selectbox();
                    $("#bath").selectbox();
                    $("#builder").selectbox();
                    $("#levels").selectbox();
                    $("#orderBy").selectbox();
                });
            </script>

            <h3>Search</h3>
            <p class="search_intro">For your perfect <br/>new home</p>

            <h6>Land Size</h6>
            <div class="left">
                <select name="lot_square_meters_from" id="sizeFrom">
                    <option value="">From</option>
                    <?php foreach($search_parameters['lot_square_meters_from'] as $value => $label):?>
                        <option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_from']) && $form_data['lot_square_meters_from'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
                    <?php endforeach;?>
                </select>
            </div><!-- end .left -->

            <div class="right">
                <select name="lot_square_meters_to" id="sizeTo">
                    <option value="">To</option>
                    <?php foreach($search_parameters['lot_square_meters_to'] as $value => $label):?>
                        <option value="<?= $value;?>" <?= (isset($form_data['lot_square_meters_to']) && $form_data['lot_square_meters_to'] == $value)? 'selected="selected"':'';?>><?= $label;?></option>
                    <?php endforeach;?>
                </select>
            </div><!-- end .right -->


            <h6>Price</h6>
            <div class="left">
                <select name="houseland_price_from" id="priceFrom">
                    <option value="">From</option>
                    <?php for($min_price = $search_parameters['house_land_price_min']; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
                        <option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_from']) && $form_data['houseland_price_from'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
                    <?php endfor;?>
                </select>
            </div><!-- end .left -->

            <div class="right">
                <select name="houseland_price_to" id="priceTo">
                    <option value="">To</option>
                    <?php if($search_parameters['house_land_price_max']):?>
                        <?php for($min_price = $search_parameters['house_land_price_min']+50000; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
                            <option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
                        <?php endfor;?>
                        <option value="<?= $search_parameters['house_land_price_max'];?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $search_parameters['house_land_price_max'])? 'selected="selected"':'';?>>$<?= number_format(ceil($search_parameters['house_land_price_max']/1000));?>k</option>
                    <?php endif;?>
                </select>
            </div><!-- end .right -->


            <div class="left">
                <h6>Bed</h6>
                <select name="house_bedrooms" id="bed">
                    <option value="">Any</option>
                    <?php foreach($search_parameters['house_bedrooms'] as $item):?>
                        <option value="<?= $item['house_bedrooms'];?>" <?= (isset($form_data['house_bedrooms']) && $form_data['house_bedrooms'] == $item['house_bedrooms'])? 'selected="selected"':'';?>><?= $item['house_bedrooms'];?></option>
                    <?php endforeach;?>
                </select>
            </div><!-- end .left -->

            <div class="right">

                <h6>Bath</h6>
                <select name="house_bathrooms" id="bath">
                    <option value="">Any</option>
                    <?php foreach($search_parameters['house_bathrooms'] as $item):?>
                        <option value="<?= $item['house_bathrooms'];?>" <?= (isset($form_data['house_bathrooms']) && $form_data['house_bathrooms'] == $item['house_bathrooms'])? 'selected="selected"':'';?>><?= $item['house_bathrooms'];?></option>
                    <?php endforeach;?>
                </select>
            </div><!-- end .right -->


            <h6>Builder</h6>
            <select name="builder_id" id="builder">
                <option value="">Any</option>
                <?php foreach($search_parameters['builders'] as $item):?>
                    <option value="<?= $item['builder_id'];?>" <?= (isset($form_data['builder_id']) && $form_data['builder_id'] == $item['builder_id'])? 'selected="selected"':'';?>><?= $item['builder_name'];?></option>
                <?php endforeach;?>
            </select>


            <h6>Levels</h6>
            <select name="house_levels" id="levels">
                <option value="">Any</option>
                <?php foreach($search_parameters['house_levels'] as $item):?>
                    <option value="<?= $item['house_levels'];?>" <?= (isset($form_data['house_levels']) && $form_data['house_levels'] == $item['house_levels'])? 'selected="selected"':'';?>><?= $item['house_levels'];?></option>
                <?php endforeach;?>
            </select>


            <h6>Order Results</h6>
            <select name="order_by" id="orderBy">
                <?php foreach($search_parameters['order_by'] as $item):?>
                    <?php if($item == 'builder'):?>
                        <option value="<?= $item;?>|asc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|asc')? 'selected="selected"':'';?>><?= ucwords($item);?> Name</option>
                    <?php else:?>
                        <option value="<?= $item;?>|asc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|asc')? 'selected="selected"':'';?>><?= ucwords($item);?>: Low to High</option>
                        <option value="<?= $item;?>|desc" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item.'|desc')? 'selected="selected"':'';?>><?= ucwords($item);?>: High to Low</option>
                    <?php endif;?>
                <?php endforeach;?>
            </select>

            <input type="submit" value="Search Homes">
        </form>
    </div><!-- end .col_search -->

    <div class="col_content">
        <?php if(empty($houses)):?>
            <h5>No house and land matches were found with this search criteria.</h5>
            <?php else:?>
            <?php foreach($houses as $house):?>
                <div class="property_panel">
                    <div class="image_mask">
                        <?php if(isset($houses_images[$house->house_id])):?>
                            <img alt="image description" src="<?= $image_base_url.$houses_images[$house->house_id]->file_name;?>" class="propery_image"/>
                        <?php endif?>
                    </div>
                    <h3><?= $house->house_name; ?></h3>
                    <div class="byline">BY <?= $house->builder_name; ?></div>

                    <div class="property_details">
                        <div class="bed"><?= $house->house_bedrooms; ?></div>
                        <div class="bath"><?= $house->house_bathrooms; ?></div>
                        <div class="car"><?= $house->house_garages; ?></div>
                    </div><!-- end .property_details -->

                    <a href="#<?= $house->house_id; ?>" class="but_view_lots">View Suitable Land Lots</a>

                    <div id="<?= $house->house_id;?>" class="dropdown">
                        <?php foreach($lots[$house->house_id] as $stage_id => $lots_array):?>  
                          <?php $stage = $stages[$stage_id];?>
                          <div class="header"><?= $stage->stage_name;?></div>
                            <?php foreach($lots_array as $lot):?>  
                            <div class="lot_info">
                                <div class="left">
                                    <div class="address">LOT <?= $lot->lot_number?></div>
                                    <div class="price">$<?= number_format($lot->house_lot_price, 0);?></div>

                                    <div class="land_size"><?= $lot->lot_square_meters;?>m2</div><div class="label">Land Size</div>
                                    <div class="frontage"><?= (float)$lot->lot_width;?>m</div><div class="label">Frontage</div>
                                </div><!-- end .left -->

                                <div class="right">
                                    <a href="#" class="but_map lightbox" id="<?= $lot->lot_number;?>" zoom_to_lot="<?= $lot->lot_number;?>" zoom_level="19" title="View Map">View Map</a>
                                    <?php if(!empty($lot->file_name)):?>
                                        <a href="<?= $based_url.'mpvs/templates/lot_house_pdfs/'.$lot->file_name;?>" class="but_download" title="Download Brochure" target="_blank">Download Brochure</a> 
                                    <?php endif;?>    
                                    <a href="javascript:void(0)" class="but_mail" title="Enquire about this Lot">Enquire about this Lot</a>
                                </div><!-- end .right -->
                            </div><!-- end .lot_info -->
                            <?php endforeach; ?>
                    </div><!-- end .dropdown -->
                </div><!-- end .property_info -->
                 <?php endforeach;?>   
                <?php endforeach;?>
            <?php endif;?>   					


    </div><!-- end .col_content -->
	</div><!-- end #house-land_wrapper -->