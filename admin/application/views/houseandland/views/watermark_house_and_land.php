<!--HOUSE LOOP HERE-->
<?php
$base_url       = 'http://app.mapovis.com.au/';
$image_base_url = $base_url.'mpvs/images/dev_houses/';
?>
<?php if(empty($houses)):?>
    <h5><center><b>No house and land matches were found with this search criteria.</b></center></h5>
    <?php else:?>
    <?php foreach($houses as $house):?>   
        <div class="house-box">
            <div class="item">
                <div class="house-bar">
                    <div class="title">
                        <h2><?= $house->house_name;?></h2>
                        <h3><?= $house->builder_name;?></h3>
                        <p class="info"><strong class="price">From $<?= number_format($house->min_lot_house_cost, 0);?></strong> <strong class="land"><i class="ico-size-v"></i><?= $house->house_size;?> sqm</strong></p>
                        <a href="#" class="btn dark-blue btn-lots">View land lots <i class="icon-arrow-bottom"></i></a>
                    </div>
                    <dl class="specs">
                        <dt><i class="ico-bed"></i></dt>
                        <dd><?= $house->house_bedrooms;?></dd>
                        <dt><i class="icon-bath"></i></dt>
                        <dd><?= $house->house_bathrooms;?></dd>
                        <dt><i class="ico-car"></i></dt>
                        <dd><?= $house->house_garages;?></dd>
                    </dl>
                    <div class="image">
                        <?php if(isset($houses_images[$house->house_id])):?> 
                            <img src="<?= $image_base_url.$houses_images[$house->house_id]->file_name;?>" alt="image description" width="273" height="151">
                        <?php endif?> 
                    </div>
                </div>
                <div class="slide">
                    <div class="items-box">
                        <div class="item">
                            <?php foreach($lots[$house->house_id] as $stage_id => $lots_array):?>  
                                <?php $stage = $stages[$stage_id];?>
                                <h2><strong>RELEASE <?= $stage->word_of_number;?></strong> <?= $stage->stage_name;?></span></h2>
                                <?php foreach($lots_array as $lot):?> 
                                    <ul class="lots-list">
                                        <!--LOTS LOOP HERE-->      
                                        <li>
                                            <div class="info-bar">
                                                <div class="cell">
                                                    <span class="label-text">Lot</span>
                                                    <strong class="name">LOT <?= $lot->lot_number?></strong>
                                                </div>
                                                <div class="cell">
                                                    <span class="label-text">Price</span>
                                                    <strong class="price">$<?= number_format($lot->house_lot_price, 0);?></strong>
                                                </div>
                                                <div class="cell">
                                                    <span class="label-text">Land Size</span>
                                                    <strong class="land"><i class="ico-size-v"></i> <em><?= $lot->lot_square_meters;?> M<sup>2</sup></em> Size</strong>
                                                </div>
                                                <div class="cell">
                                                    <span class="label-text">Frontage</span>
                                                    <strong class="frontage"><i class="ico-size-h"></i> <em><?= (float)$lot->lot_width;?> M</em> Frontage</strong>
                                                </div>
                                                <div class="cell">
                                                    <div class="action">
                                                        <?php if(!empty($lot->file_name)):?>
                                                            <a class="btn blue btn-download" href="<?= $base_url.'mpvs/templates/lot_house_pdfs/'.$lot->file_name;?>" data-placement="top" data-toggle="tooltip" data-original-title="Dowload PDF File" target="_blank"><i class="ico-download2"></i></a>
                                                        <?php endif;?>   
                                                        <a class="btn blue btn-view lightbox" id="<?= $lot->lot_number;?>" zoom_to_lot="<?= $lot->lot_number;?>" zoom_level="19" href="#" data-placement="top" data-toggle="tooltip" data-original-title="View On Materplan"><i class="ico-pin"></i></a>
                                                        <a class="btn blue btn-open" href="">ENQUIRE</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="enquire-bar">
                                                <div class="frame">
                                                    <form class="enquire-form validation" action="http://red23.runway.com.au/actions/form/globalformaction.jsp"  method="post" novalidate name="Registration Form">
                                                        <input class="form-control" type="hidden" name="GroupID" value="0C1J481H8T3W4F9S105R9X7K224P">
                                                        <input class="form-control" type="hidden" name="LocationID" value="0D174019813Q4G8T8I643U5V7L6L">
                                                        <input class="form-control" type="hidden" name="NewContactStatusID" value="9187A1D0501491A19212A181D796C0E5">
                                                        <input class="form-control" type="hidden" name="Source" value="Watermark Webform">
                                                        <input class="form-control" type="hidden" name="-redirect" value="http://www.watermarkgeelong.com.au/thank-you.html"><!-- Put the URL of the page you want to redirect to-->
                                                        <input type="hidden" name="NotificationTemplateID" value="0J184V1R9B3S083B6X907M6T501B">
                                                        <input class="form-control" type="hidden" name="sendNotificationTo" value="SALESREP">
                                                        <input type="hidden" name="TemplateID" value="0L1D4C1D9Q212K6V5F3G3I7V0E4D">
                                                        <input class="form-control" type="hidden" name="-alwaysnotify" value="true">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                                <div class="field-holder">
                                                                    <input class="form-control required" type="text" name="FirstName" placeholder="Name*">
                                                                </div>
                                                                <div class="field-holder">
                                                                    <input class="form-control required-number" type="tel" name="Phone" placeholder="Phone*">
                                                                </div>
                                                                <div class="field-holder">
                                                                    <input class="form-control required-email" type="email" name="Email" placeholder="Email*">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                                <div class="field-holder">
                                                                    <input class="form-control required-number" name="Postcode" placeholder="Postcode*">
                                                                </div>
                                                                <div class="field-holder">
                                                                    <select name="Answer0E1T441G9T272G3H7T4X3O8F6B0D" class="form-control required-select source-enquiry default-value">
                                                                        <option class="hideme" value="">Source of enquiry*</option>
                                                                        <option value="Online">Online</option>
                                                                        <option value="Builder">Builder</option>
                                                                        <option value="Signage">Signage</option>
                                                                        <option value="Newspaper">Newspaper</option>
                                                                        <option value="Word of Mouth">Word of Mouth</option>
                                                                        <option value="Other">Other</option>
                                                                    </select>
                                                                </div>
                                                                <div class="field-holder">
                                                                    <input type="text" name="Answer0Q1O4Z143Y9C4H1V33108M3Y9B5I" class="form-control other" placeholder="Please Specify" rel="Please Specify" value="">
                                                                </div>
                                                                <div class="field-holder">
                                                                    <button data-loading-text="Sending..." type="submit" data-ga-action="submit" data-ga-category="Mapovis Contact" data-ga-title="form submit" class="btn btn-primary submit">SUBMIT REGISTRATION</button>
                                                                    <div class="loader"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
                                        <!--END LOTS LOOP-->
                                    </ul>
                                    <?php endforeach;?>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php endforeach;?>   
        <?php endforeach;?>
    <?php endif;?>   
    <!--END HOUSE LOOP HERE-->