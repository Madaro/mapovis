<?php
// INCLUDE SimpleHtmlDom, which will be used below to pull in the results from admin/apihouseandland/houseandland/8
include_once('assets/simplehtmldom/simple_html_dom.php');
$based_url      = base_url();
$folder_url     = $based_url.'application/views/houseandland/';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MAPOVIS - House & Land Page PROTOTYPE Example</title>

	<!-- Bootstrap Holder JS (for temporary thumbnail display)-->
	<script src="<?= $folder_url;?>assets/bootstrap/js/holder.js"></script>

	<!-- Bootstrap -->
	<link href="<?= $folder_url;?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- Bootstrap theme -->
	<link href="<?= $folder_url;?>assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet">

	<!-- CASSETTE's Custom CSS -->
	<link href="<?= $folder_url;?>css/custom-css.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!--[if gte IE 9]>
	  <style type="text/css">
		.gradient {
		   filter: none;
		}
	  </style>
	<![endif]-->
  </head>

  <body>

	<div>
		<p>Hello World, I am the index.php page. 
		<br/><br/>
		I am now going to pull in the HOUSE & LAND content from MAPOVIS as a DOM object using 'simplehtmldom' from <?= $based_url;?>apihouseandland/houseandland/8 and echo it to the browser.</p>
	</div>

	<div class="container">
	<!-- =====================================================
	START: Pull in page from MAPOVIS (using simplehtmldom) -->

	<?php
	$lot_square_meters = (isset($_POST['lot_square_meters']) && is_array($_POST['lot_square_meters']))? implode('&lot_square_meters[]=', $_POST['lot_square_meters']): '';
	// Create a DOM object from a URL
	$html = file_get_html('http://stage3.mapovis.com.au/admin/apihouseandland/houseandland/28/J1m65yoXd9gjzQA/?houseland_price_from='.@$_POST["houseland_price_from"].'&houseland_price_to='.@$_POST["houseland_price_to"].'&house_bedrooms='.@$_POST["house_bedrooms"].'&house_bathrooms='.@$_POST["house_bathrooms"].'&builder_id='.@$_POST["builder_id"].'&house_levels='.@$_POST["house_levels"].'&order_by='.@$_POST["order_by"].'&lot_square_meters[]='.$lot_square_meters);

	echo $html;
	?>

	<!-- END: Pull in page from MAPOVIS (using simplehtmldom)
	===================================================== -->
	</div>

	<div>
		<br/><br/>
		<p>This is the index.php page saying HEY again!</p>
	</div>

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?= $folder_url;?>assets/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>