<?php
$based_url      = "http://app.mapovis.com.au/";
$image_base_url = base_url().'../mpvs/images/dev_houses/';
$folder_url     = base_url().'application/views/houseandland/';
?>

<!-- /START CONTAINER -- CURL will pull in from here onwards.
================================================== -->
<br/>
<div class="container">
	<!-- HOUSE & LAND SEARCH -->
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">Refine Your Search</div>
		<div class="panel-body">
			<form name="lots-search" action="#" method="post">
				<div class="lot-search-form">

					<div class="input-group enquiry-form-input">
						<div class="price-search-header">
							<span class="glyphicon glyphicon-usd"></span>
							<span class="priceheader-title">HOUSE & LAND PRICE</span>
						</div>

						<select class="price-from" name="houseland_price_from">
							<option value="">From</option>
							<?php for($min_price = $search_parameters['house_land_price_min']; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
								<option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_from']) && $form_data['houseland_price_from'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
							<?php endfor;?>
						</select>

						<select class="price-to" name="houseland_price_to">
							<option value="">To</option>
							<?php for($min_price = $search_parameters['house_land_price_min']+50000; $min_price < $search_parameters['house_land_price_max']; $min_price += 50000):?>
								<option value="<?= $min_price;?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $min_price)? 'selected="selected"':'';?>>$<?= number_format(ceil($min_price/1000));?>k</option>
							<?php endfor;?>
							<option value="<?= $search_parameters['house_land_price_max'];?>" <?= (isset($form_data['houseland_price_to']) && $form_data['houseland_price_to'] == $search_parameters['house_land_price_max'])? 'selected="selected"':'';?>>$<?= number_format(ceil($search_parameters['house_land_price_max']/1000));?>k</option>
						</select>

						<div class="bed-bath-search-header">
							<span class="bedrooms-image"><img src="<?= $folder_url;?>images/bedrooms-3.gif"></img></span>
							<span class="beds-title">BEDS</span>

							<span class="bathrooms-image"><img src="<?= $folder_url;?>images/bathrooms-3.gif"></img></span>
							<span class="baths-title">BATHS</span>
						</div>

						<select class="bedrooms-dropdown" name="house_bedrooms">
							<option value="">Any</option>
							<?php foreach($search_parameters['house_bedrooms'] as $item):?>
								<option value="<?= $item['house_bedrooms'];?>" <?= (isset($form_data['house_bedrooms']) && $form_data['house_bedrooms'] == $item['house_bedrooms'])? 'selected="selected"':'';?>><?= $item['house_bedrooms'];?></option>
							<?php endforeach;?>
						</select>

						<select class="bathrooms-dropdown" name="house_bathrooms">
							<option value="">Any</option>
							<?php foreach($search_parameters['house_bathrooms'] as $item):?>
								<option value="<?= $item['house_bathrooms'];?>" <?= (isset($form_data['house_bathrooms']) && $form_data['house_bathrooms'] == $item['house_bathrooms'])? 'selected="selected"':'';?>><?= $item['house_bathrooms'];?></option>
							<?php endforeach;?>
						</select>

						<fieldset class="block">
							<legend class="icon size">TOTAL SIZE m<sup>2</sup></legend>
							<div class="col">
								<ul class="radio-holder">
									<?php foreach ($search_parameters['lot_square_meters'] as $item_value => $item_label):?>
										<li>
											<input name="lot_square_meters[]" type="checkbox" id="size_<?= $item_value;?>" value="<?= $item_value;?>" <?= (isset($form_data['lot_square_meters']) && in_array($item_value, $form_data['lot_square_meters']))? 'checked="checked"':'';?>>
											<label for="size_<?= $item_value;?>"><?= $item_label;?></label>
										</li>
									<?php endforeach;?>
								</ul>
							</div>
						</fieldset>

						<div class="cols">
							<fieldset class="col">
								<legend class="icon">BUILDER</legend>
								<select class="custom-select" name="builder_id">
									<option value="">Any</option>
									<?php foreach($search_parameters['builders'] as $item):?>
										<option value="<?= $item['builder_id'];?>" <?= (isset($form_data['builder_id']) && $form_data['builder_id'] == $item['builder_id'])? 'selected="selected"':'';?>><?= $item['builder_name'];?></option>
									<?php endforeach;?>
								</select>
							</fieldset>
							<fieldset class="col">
								<legend class="icon">LEVELS</legend>
								<select class="custom-select" name="house_levels">
									<option value="">Any</option>
									<?php foreach($search_parameters['house_levels'] as $item):?>
										<option value="<?= $item['house_levels'];?>" <?= (isset($form_data['house_levels']) && $form_data['house_levels'] == $item['house_levels'])? 'selected="selected"':'';?>><?= $item['house_levels'];?></option>
									<?php endforeach;?>
								</select>
							</fieldset>
						</div>
						<div class="cols">
							<fieldset class="col">
								<legend class="order-lable">ORDER RESULTS BY</legend>
								<select class="custom-select" name="order_by">
									<?php foreach($search_parameters['order_by'] as $item):?>
									<option value="<?= $item;?>" <?= (isset($form_data['order_by']) && $form_data['order_by'] == $item)? 'selected="selected"':'';?>><?= ucwords($item);?></option>
									<?php endforeach;?>
								</select>
							</fieldset>
						</div>
					</div>

					<div class="refine-search-button">
						<button type="submit" class="btn btn-success">Refine Search</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- END HOUSE & LAND SEARCH -->

	<?php if(empty($lots)):?>
		<h5>No houses and land were be found with given criteria.</h5>
	<?php else:?>
	<div class="land-block-container">
		<div class="list-content-holder">
			<ul class="list-content">
			<?php foreach($lots as $lot):?>
				<li <?= ($lot->status == 'Sold')? 'class="sold"':''; ?>>
					<ul class="data-holder">
						<li class="col01">
							<span>Lot <?= $lot->lot_number;?></span>
						</li>
						<li class="col02">
							<b class="icon size"><?= $lot->lot_square_meters;?>m<sup>2</sup></b> Land Size
						</li>
						<li class="col03">
							House &amp; Land From <b class="price">$<?= number_format($lot->min_lot_house_cost);?></b>
						</li>
						<li class="col04">
							<b class="available"><span class="glyphicon glyphicon-ok"></span> AVAILABLE</b>
							<b class="sold"><span class="glyphicon glyphicon-remove"></span> sold</b>
						</li>
						<li class="col05">
							<span class="icon enquire" title="Enquire">Enquire</span>
						</li>
					</ul>
					<div class="list-content-slide-add">
						<?php foreach($houses[$lot->lot_id] as $house):?>
						<div class="land-add">
							<section class="land-block">
								<div class="media">
									<?php if(isset($houses_images[$lot->lot_id][$house->house_id])):?>
										<img src="<?= $image_base_url. $houses_images[$lot->lot_id][$house->house_id]->file_name; ?>" alt="<?= $house->house_name;?>">
									<?php endif;?>
								</div>
								<div class="entry-content">
									<div class="heading">
										<h3><a href="<?= $house->house_moreinfo_url;?>" target="_blank"><?= strtoupper($house->house_name);?></a></h3>
										<span class="by">By <a href="<?= $house->builder_website;?>" target="_blank"><?= $house->builder_name;?></a></span>
									</div>
									<span class="icon size"><?= $house->house_size;?> <sup>sq</sup></span>
									<dl>
										<dt class="icon bed" title="Bed">Bed</dt>
										<dd><?= $house->house_bedrooms;?></dd>
										<dt class="icon bath" title="Bath">Bath</dt>
										<dd><?= $house->house_bathrooms;?></dd>
										<dt class="icon car" title="Car">Car</dt>
										<dd><?= $house->house_garages;?></dd>
									</dl>
								</div>
								<div class="entry-meta">
									<dl>
										<dt>House &amp; Land Starting From</dt>
										<dd>$<?= number_format($house->house_lot_price);?> <span class="asterisk">*</span></dd>
									</dl>
									<?php if(!empty($house->file_name)):?>
										<a href="<?= base_url().'../mpvs/templates/lot_house_pdfs/'.$house->file_name;?>" target="_blank" class="btn btn-primary download">
											<span>DOWNLOAD PDF</span>
										</a>
									<?php endif;?>
									<a href="#" class="btn btn-default">Enquire now</a>
									<a href="#" class="btn btn-primary">Enquire now</a>
								</div>
							</section>
							<div class="data-content-add">
								<div class="holder">
									<div class="col">
										<form action="#" class="enquire-form">
											<fieldset>
												<b class="title">Enquire About This Land</b>
												<div class="block">
													<input type="text" placeholder="Full Name*">
												</div>
												<div class="block">
													<input type="tel" placeholder="Contact Phone">
												</div>
												<div class="block">
													<input type="email" placeholder="Email Address*">
												</div>
												<div class="submit-block">
													<div class="btn-submit">
														<input type="submit" value="ENQUIRE now">
													</div>
													<span class="required">* Required fields</span>
												</div>
											</fieldset>
										</form>
									</div>
									<div class="col">
										<img src="http://www.kallo.com.au/wp-content/themes/kallo/images/enquiry-banner.png" class="img-responsive"/>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach;?>
					</div>
				</li>
			<?php endforeach;?>
			</ul>
		</div>
	</div>
	<?php endif;?>

	<!-- /END CONTAINER 
================================================== -->

	<!-- Terms and Conditions Modal
================================================== -->
	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myLargeModalLabel">Terms & Conditions</h4>
				</div>
				<div class="modal-body">
					<?= $development->terms_and_conditions?>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
<!-- THE END - everything below this point will be on the developers website, and not delivered to them via a SimpleHtmlDom (curl) request   ============================= -->
</div>