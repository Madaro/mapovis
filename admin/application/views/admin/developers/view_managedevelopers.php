<?php
$base_url = base_url().'admin/developers/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3>Developers</h3>
				</div><!-- End .heading-->
				<?= $alert_message;?>

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default gradient">
								<div class="panel-heading">
									<h4>
										<span>Manage Developers</span>
									</h4>
								</div>
								<div class="panel-body noPad clearfix">
									<?php if (empty($developers)):
										echo 'No results found';?>
									<?php else:?>
									<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
										<thead>
											<tr>
												<th>ID</th>
												<th>Developer</th>
												<th>Background Colour</th>
												<th>Foreground Colour</th>
												<th></th>
												<th></th>
											</tr>
											<tr>
												<td><input type="text" name="search_dev_id" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_developer_name" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_background_colour" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_foreground_colour" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td></td>
												<td></td>
											</tr>
										</thead>
										<tbody>
											<?php foreach($developers as $developer):?>
											<tr>
												<td style=""><?= $developer->developer_id;?></td>
												<td style=""><?= $developer->developer;?></td>
												<td style="">
													<?php if(!empty($developer->background_colour)):?>
													<?= '#'.$developer->background_colour;?> &nbsp;&nbsp;
													<span style="width:100px !important;border: 1px solid #000000;background-color: #<?= $developer->background_colour;?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
													<?php endif;?>
												</td>
												<td style="">
													<?php if(!empty($developer->foreground_colour)):?>
													<?= '#'.$developer->foreground_colour;?> &nbsp;&nbsp;
													<span style="width:50px !important;border: 1px solid #000000;background-color: #<?= $developer->foreground_colour;?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
													<?php endif;?>
												</td>
												<td class="center" style="">
													<a href="<?= $base_url.'editdeveloper/'.$developer->developer_id;?>"><button type="button" id="opener1" class="btn btn-xs btn-default">Edit</button></a>
												</td>
												<td>
													<?php if($developer->total_developments == 0):?>
														<a href="<?= $base_url.'deletedeveloper/'.$developer->developer_id;?>"><button type="button" id="opener1" class="delete_developer_btn btn btn-xs btn-danger">Delete</button></a>
													<?php endif;?>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
									<?php endif;?>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span12 -->

					</div><!-- End .row -->

					<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
$(document).ready(function() {
	$('.delete_developer_btn').click(function() {
		if(!confirm('Are you sure you want to delete the developer?')){
			return false;
		}
		showLoading();
	});
});
</script>
