<?php
$base_url = base_url().'admin/developers/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Edit Developer</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-documents"></span>
									<span>Edit Developer</span>
								</h4>
							</div>

							<div class="panel-body">
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>editdeveloper/<?= $developer->developer_id;?>" role="form" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-lg-3 control-label" for="textareas">Developer:</label>
									<div class="col-lg-4">
										<input name="developer" type="text" class="form-control" value="<?= $developer->developer;?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="buttons">Background Colour:</label>
									<div class="col-lg-8">
										<div class="row">
											<div class="col-lg-3">
												<div class="input-group">
													<input type="text" class="form-control change_colour" id="background_colour" name="background_colour" value="#<?= $developer->background_colour;?>">
												</div><!-- /input-group -->
											</div>
											<div class="col-lg-3">
												<div class="input-group">
													<div id="background_colour_disp" style="width:50px !important;border: 2px solid #<?= $developer->background_colour;?>;background-color: #<?= $developer->background_colour;?>">&nbsp;&nbsp;&nbsp;</div>
												</div><!-- /input-group -->
											</div>
										</div>
									</div>
								</div><!-- End .form-group--> 

								<div class="form-group">
									<label class="col-lg-3 control-label" for="buttons">Foreground Colour:</label>
									<div class="col-lg-8">
										<div class="row">
											<div class="col-lg-3">
												<div class="input-group">
													<input type="text" class="form-control change_colour" id="foreground_colour" name="foreground_colour" value="#<?= $developer->foreground_colour;?>">
												</div><!-- /input-group -->
											</div>
											<div class="col-lg-3">
												<div class="input-group">
													<div id="foreground_colour_disp" style="width:50px !important;border: 2px solid #<?= $developer->foreground_colour;?>;background-color: #<?= $developer->foreground_colour;?>">&nbsp;&nbsp;&nbsp;</div>
												</div><!-- /input-group -->
											</div>
										</div>
									</div>
								</div><!-- End .form-group--> 

								<div class="col-lg-offset-3">
									<br>
									<span><button type="submit" class="btn btn-info">Update</button></span>
									<span style="padding-left:10px;"><a href="<?= $base_url.'managedevelopers'; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
									<br><br>
								</div>
								</form>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span3 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<link href="<?php echo base_url(); ?>assets/js/chromoselector-2.1.5/chromoselector.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/chromoselector-2.1.5/chromoselector.min.js"></script>
<script>
	$(document).ready(function() {
		var updatePreview = function() {
			var color = $(this).chromoselector('getColor');
			var box_id = $(this).attr('name')+'_disp';
			var color_box = color.getHexString();
			$('#'+box_id).attr('style', 'width:50px !important;border: 2px solid '+color_box+';background-color:'+color_box+';')
		};
		jQuery.each($('.change_colour'), function(){
			$(this).chromoselector({
				preview: false,
				update: updatePreview
			});
		});
	});
</script>
