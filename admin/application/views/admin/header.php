<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Mapovis Admin</title>


	<meta name="application-name" content="Mapovis Admin Area" />

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- Force IE9 to render in normal mode -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<!-- Le styles -->
	<!-- Use new way for google web fonts 
	http://www.smashingmagazine.com/2012/07/11/avoiding-faux-weights-styles-google-web-fonts -->
	<!-- Headings -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
	<!-- Text -->
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' /> 
	<!--[if lt IE 9]>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
	<link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
	<link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
	<![endif]-->

	<!-- Core stylesheets do not remove -->
	<link id="bootstrap" href="<?php echo base_url(); ?>assets/css/bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/supr-theme/jquery.ui.supr.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />

	<!-- Main stylesheets -->
	<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css" />

	<!-- Custom stylesheets ( Put your own changes here ) -->
	<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet" type="text/css" />

	<!--[if IE 8]><link href="<?php echo base_url(); ?>assets/css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/libs/excanvas.min.js"></script>
	  <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/libs/respond.min.js"></script>
	<![endif]-->

	<!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-144-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-114-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-72-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-57-precomposed.png" />

	<!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
	<meta name="application-name" content="Mapovis"/> 
	<meta name="msapplication-TileColor" content="#3399cc"/>

	<!-- Load modernizr first -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/libs/modernizr.js"></script>

	</head>

	<body>
	<!-- loading animation -->
	<div id="qLoverlay"></div>
	<div id="qLbar"></div>

	<div id="header">
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<a class="navbar-brand" href="<?php echo base_url(); ?>admin/admindashboard"><img src="<?php echo base_url(); ?>assets/images/mapovis-logo-small.png" width="189" height="51" /></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon16 icomoon-icon-arrow-4"></span>
				</button>
			</div> 
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">

				</ul>

				<ul class="nav navbar-right usernav">

					<li class="dropdown">
						<a href="#" class="dropdown-toggle avatar" data-toggle="dropdown">
							<img src="<?php echo base_url(); ?>assets/images/mapovis-avatar.gif" alt="" class="image" />
							<span class="txt"><?= $this->session->userdata('logged_in')['username']?></span>
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li class="menu">
								<ul>
									<li><a href="manageaccount.html"><span class="icon16 icomoon-icon-user-plus"></span>Manage Account</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li><a href="<?php echo base_url(); ?>admin/admindashboard/logout"><span class="icon16 icomoon-icon-exit"></span><span class="txt"> Logout</span></a></li>
				</ul>
			</div><!-- /.nav-collapse -->
		</nav><!-- /navbar -->

	</div><!-- End #header -->