<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?= $title ?></title>


	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- Force IE9 to render in normal mode -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<!-- Le styles -->
	<!-- Use new way for google web fonts 
	http://www.smashingmagazine.com/2012/07/11/avoiding-faux-weights-styles-google-web-fonts -->
	<!-- Headings -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
	<!-- Text -->
	<link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />
	<!--[if lt IE 9]>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
	<![endif]-->

	<!-- Core stylesheets do not remove -->
	<link id="bootstrap" href="<?php echo base_url(); ?>assets/css/bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/supr-theme/jquery.ui.supr.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />

	<!-- Plugins stylesheets -->
	<link href="<?php echo base_url(); ?>assets/plugins/forms/uniform/uniform.default.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/tables/dataTables/jquery.dataTables.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/tables/dataTables/TableTools.css" type="text/css" rel="stylesheet" />

	<!-- Main stylesheets -->
	<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css" />

	<!-- Custom stylesheets ( Put your own changes here ) -->
	<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet" type="text/css" />

	<!--[if IE 8]><link href="<?php echo base_url(); ?>assets/css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/libs/excanvas.min.js"></script>
	  <script type="text/javascript" src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/libs/respond.min.js"></script>
	<![endif]-->

	<!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-144-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-114-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-72-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-57-precomposed.png" />

	<!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
	<meta name="application-name" content="Supr"/>
	<meta name="msapplication-TileColor" content="#3399cc"/>

	<!-- Load modernizr first -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/libs/modernizr.js"></script>

	<!-- jQuery -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

	</head>

	<body>
	<!-- loading animation -->
	<div id="qLoverlay"></div>
	<div id="qLbar"></div>
	<div id="qLoverlaymessage" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessage" class="qLmessage" style="display:none;"></div>

	<div id="header">
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<a class="navbar-brand" href="<?php echo base_url(); ?>admin/admindashboard"><img src="<?php echo base_url(); ?>assets/images/mapovis-logo-small.png" width="189" height="51" /></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon16 icomoon-icon-arrow-4"></span>
				</button>
			</div> 
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">

				</ul>

				<ul class="nav navbar-right usernav">

					<li class="dropdown">
						<a href="#" class="dropdown-toggle avatar" data-toggle="dropdown">
							<img src="<?php echo base_url(); ?>assets/images/mapovis-avatar.gif" alt="" class="image" /> 
							<span class="txt"><?php echo $this->session->userdata('logged_in_username'); ?></span>
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li class="menu">
								<ul>
									<li><a href="<?php echo base_url(); ?>admin/settings/manageaccount"><span class="icon16 icomoon-icon-user-plus"></span>Manage Account</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li><a href="<?php echo base_url(); ?>admin/login/logout"><span class="icon16 icomoon-icon-exit"></span><span class="txt"> Logout</span></a></li>
				</ul>
			</div><!-- /.nav-collapse -->
		</nav><!-- /navbar -->

	</div><!-- End #header -->

	<div id="wrapper">
		<!--Responsive navigation button-->
		<div class="resBtn">
			<a href="#"><span class="icon16 minia-icon-list-3"></span></a>
		</div>

		<!--Sidebar background-->
		<div id="sidebarbg"></div>
		<!--Sidebar content-->
		<div id="sidebar">
			<div class="sidenav">
				<div class="mainnav">
					<ul>
						<li><a href="<?php echo base_url(); ?>admin/admindashboard"><span class="icon16 icomoon-icon-home-4"></span>Home</a></li>

						<li>
							<a href="#"><span class="icon16 icomoon-icon-home-7"></span>Developments</a>
							<ul class="sub">
								<li><a href="<?php echo base_url(); ?>admin/developments/managedevelopments"><span class="icon16 icomoon-icon-file-check"></span>Manage Developments</a></li>
								<li><a href="<?php echo base_url(); ?>admin/developments/adddevelopment"><span class="icon16 icomoon-icon-file-plus"></span>Add Development</a></li>
								<li><a href="<?php echo base_url(); ?>admin/developments/impexplotscsv"><span class="icon16 icomoon-icon-file-excel"></span>Imp/Exp Lots</a></li>
							 </ul>
						</li>
						<li>
							<a href="#"><span class="icon16 icomoon-icon-wrench"></span>Developers</a>
							<ul class="sub">
								<li><a href="<?php echo base_url(); ?>admin/developers/managedevelopers"><span class="icon16 icomoon-icon-file-check"></span>Manage Developers</a></li>
								<li><a href="<?php echo base_url(); ?>admin/developers/adddeveloper"><span class="icon16 icomoon-icon-file-plus"></span>Add Developer</a></li>
							 </ul>
						</li>
						<li>
							<a href="#"><span class="icon16 icomoon-icon-home-4"></span>Global Houses</a>
							<ul class="sub">
								<li><a href="<?php echo base_url(); ?>admin/globalhouses/viewhouses"><span class="icon16 icomoon-icon-file-check"></span>Manage Houses</a></li>
								<li><a href="<?php echo base_url(); ?>admin/globalhouses/addhouses"><span class="icon16 icomoon-icon-file-plus"></span>Add House</a></li>
								<li><a href="<?php echo base_url(); ?>admin/globalhouses/impglobalhousescsv"><span class="icon16 icomoon-icon-file-excel"></span>Import Global Houses</a></li>
							</ul>
						</li>

						<li>
							<a href="#"><span class="icon16 icomoon-icon-office"></span>Builders</a>
							<ul class="sub">
								<li><a href="<?php echo base_url(); ?>admin/builders"><span class="icon16 icomoon-icon-list"></span>Manage Builders</a></li>
								<li><a href="<?php echo base_url(); ?>admin/builders/addbuilder"><span class="icon16 icomoon-icon-file-plus"></span>Add Builder</a></li>
							</ul>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/users"><span class="icon16 icomoon-icon-users"></span>Users</a>
						</li>

						<li>
							<a href="#"><span class="icon16 icomoon-icon-file-3"></span>Reports</a>
							<ul class="sub">
								<li><a href="<?php echo base_url(); ?>admin/reports/statisticsalldev"><span class="icon16 icomoon-icon-stats"></span>MAPOVIS Statistics</a></li>
                                <li><a href="<?php echo base_url(); ?>admin/reports/alldevsstatisticsreport"><span class="icon16 icomoon-icon-stats"></span>All Developments Report</a></li>                                                                       
                                <li><a href="<?php echo base_url(); ?>admin/reports/useragentsreport"><span class="icon16 icomoon-icon-stats"></span>User Agents Report</a></li>                                                                       
								<li><a href="<?php echo base_url(); ?>admin/reports/lotviewsstatisticsalldev"><span class="icon16 icomoon-icon-stats"></span>Lot Impressions Statistics</a></li>
								<li><a href="<?php echo base_url(); ?>admin/reports/availlotstatisticsalldev"><span class="icon16 icomoon-icon-stats"></span>Available Lots Statistics</a></li>
								<li><a href="<?php echo base_url(); ?>admin/reports/soldlotstatisticsalldev"><span class="icon16 icomoon-icon-stats"></span>Sales Rate Statistics</a></li>
								<li><a href="<?php echo base_url(); ?>admin/reports/averagelotpricesreportalldev"><span class="icon16 icomoon-icon-stats"></span>Avg. Price By Frontage</a></li>
								<li><a href="<?php echo base_url(); ?>admin/reports/changelog"><span class="icon16 icomoon-icon-file-7"></span>Lots Change Log</a></li>
								<li><a href="<?php echo base_url(); ?>admin/reports/hnlchangelog"><span class="icon16 icomoon-icon-stats-2"></span>H&L Change Log</a></li>
								<li><a href="<?php echo base_url(); ?>admin/reports/reminderlog"><span class="icon16 icomoon-icon-file-7"></span>Reminder Log</a></li>
								<li><a href="<?php echo base_url(); ?>admin/reports/overdueusersreport"><span class="icon16 icomoon-icon-file-7"></span>Overdue User Report</a></li>
								<li><a href="<?php echo base_url(); ?>admin/reports/problemusersreport"><span class="icon16 icomoon-icon-file-7"></span>Problem User Report</a></li>
								<li><a href="<?php echo base_url(); ?>admin/reports/availablelots"><span class="icon16 icomoon-icon-file-7"></span>Available Lots Reports</a></li>
							</ul>
						</li>

						<li>
							<a href="#"><span class="icon16  icomoon-icon-cog-4"></span>Settings</a>
							<ul class="sub">
								<li><a href="<?php echo base_url(); ?>admin/settings/welcomemessage"><span class="icon16 icomoon-icon-bubble-5"></span>Welcome Message</a></li>
								<li><a href="<?php echo base_url(); ?>admin/settings/welcomemessagebuilder"><span class="icon16 icomoon-icon-bubble-5"></span>Welcome for Builders</a></li>
								<li><a href="<?php echo base_url(); ?>admin/settings/globalconfig"><span class="icon16 icomoon-icon-cog-4"></span>General Settings</a></li>
							</ul>
						</li>

					</ul>
				</div>
			</div><!-- End sidenav -->

		</div><!-- End #sidebar -->

	<?php $this->load->view($layout_content); ?>

	</div><!-- End #wrapper -->

	<!-- Le javascript
	================================================== -->
	<!-- Important plugins put in all pages -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/libs/jRespond.min.js"></script>

	<?php foreach($footer_custom_content_chunks as $footer_custom_content): ?>
		<?php $this->load->view($footer_custom_content); ?>
	<?php endforeach; ?>
	</body>
</html>
