<script>
	$('.view_house_btn').on('click', function(){
		var house_id = $(this).attr('house_id');
		var title = $(this).attr('title');
		showLoading();
		$.post('<?= base_url();?>admin/globalhouses/viewhouse/'+house_id,
			function(data,status){
				if(status === 'success'){
					$('#dialog_house').html(data);
					$('#dialog_house').dialog({
						title: title,
						height: 600,
						width: 1000,
						modal: true,
						resizable: false,
						dialogClass: 'loading-dialog',
						close: function(ev,ui){remove_tinymce();$('#dialog_house').html('');}
					});
					$('#dialog_house').dialog('open');
					initilizeHouseView(house_id);
					hideLoading();
					$('#cancel_house_changes').click(function() {
						$('#dialog_house').dialog('close');
					});
					return true;
				}
				messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
		});
	});
	$('.delete_house_btns').click(function() {
		var house_name = $(this).attr('house_name');
		if(!confirm('Are you sure you want to delete the house "'+house_name+'"?')){
			return false;
		}
		showLoading();
	});
	<?php if(isset($default_house) && $default_house):?>
		if($('#update_btn_<?= $default_house;?>').length > 0){
			$('#update_btn_<?= $default_house;?>').click();
		}
	<?php endif;?>
</script>