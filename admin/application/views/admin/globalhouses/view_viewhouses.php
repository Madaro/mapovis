<?php
$base_url = base_url('admin/globalhouses');
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Global Houses</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>All Houses</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($houses)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>House ID</th>
											<th>House Name</th>
											<th>Builder</th>
											<th></th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_house_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_house_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_builder" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($houses as $house):?>
										<tr id="tr_houseid_<?= $house->house_id; ?>">
											<td><?= $house->house_id; ?></td>
											<td><?= $house->house_name; ?></td>
											<td><?= $house->builder_name; ?></td>
											<td class="center">
												<button id="update_btn_<?= $house->house_id?>" house_id="<?= $house->house_id;?>" title="House ID <?= $house->house_id?>  [ <?= $house->house_name?> by <?= $house->builder_name?> ]" class="view_house_btn btn btn-xs btn-default">Update</button>
											</td>
											<td>
												<a href="<?= "{$base_url}/deletehouse/{$house->house_id}"; ?>"><button house_name="<?= $house->house_name;?>" class="delete_house_btns btn btn-xs btn-danger">Delete</button></a>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not houses in the system.</div>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<div>
					<a href="<?= $base_url; ?>/addhouses/">
						<button type="button" class="btn btn-info">Add Houses</button>
					</a>
				</div>
				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Image field necessary to call Aviary and crop the images -->
<img id='imageupload' src='' style="display:none"/>
<!-- Dialog -->
<div id="dialog_house"></div>