    <!-- Misc plugins -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/misc/nicescroll/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/misc/qtip/jquery.qtip.min.js"></script><!-- Custom tooltip plugin -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/misc/totop/jquery.ui.totop.min.js"></script> 

    <!-- Charts plugins -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/charts/sparkline/jquery.sparkline.min.js"></script><!-- Sparkline plugin -->

    <!-- Form plugins -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/togglebutton/jquery.toggle.buttons.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/globalize/globalize.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/select/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/tiny_mce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/supr-theme/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/supr-theme/jquery-ui-sliderAccess.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/wizard/jquery.bbq.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/wizard/jquery.form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/wizard/jquery.form.wizard.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/typeahead/typeahead.min.js"></script>
    
    <!-- Table plugins -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/tables/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/tables/dataTables/TableTools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/tables/dataTables/ZeroClipboard.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/tables/responsive-tables/responsive-tables.js"></script><!-- Make tables responsive -->

    <!-- Gallery plugins -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/gallery/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/gallery/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/gallery/pretty-photo/jquery.prettyPhoto.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script><!-- Core js functions -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatable.js"></script><!-- Init plugins only for page -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/forms.js"></script><!-- Init plugins only for page -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/widgets.js"></script><!-- Init plugins only for page -->