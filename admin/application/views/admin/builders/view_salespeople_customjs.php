<style>
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 500px; }
	.sortable li { margin: 10px 10px 10px 0; padding: 0px; float: left; width: 131px; height: 166px;  text-align: center; display:block; overflow:hidden;}
	.deleteimage{margin-top: 7px;}
	.img_container{overflow-y:hidden;}
	.ui-dialog { z-index: 65535; }
</style>
<script type="text/javascript" src="http://feather.aviary.com/js/feather.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/validate/jquery.validate.min.js"></script>
<script>
var featherEditor = new Aviary.Feather({
	apiKey: '84495f70872a7572',
	apiVersion: 3,
	tools: 'all',
	theme: 'light',
	onError: function(errorObj) {
		console.log(errorObj.message);
	}
});

function initilizeView(builder_id){
	$('.image_files').on('change', function(){
		var input = $(this)[0];
		if (input.files && input.files[0]){
			if(!validateimagefiletype(input.files[0].type)){
				return false;
			}
		}
	});
	$('.deleteimage').on('click', function(e){
		deleteImageFunction($(this), true);
	});
	if(builder_id > 0){
		init_tinymce();
		$("input, textarea, select, button").attr('tabindex', '-1');
		$("input, textarea, select").not('.nostyle').uniform();
	}
	$('.add_image_btn').on('click', function(e){
		var image_field_obj = $('#image_file');
		var input           = image_field_obj[0];
		if (input.files && input.files[0]){
			if(!validateimagefiletype(input.files[0].type)){
				// show dialog message
				return false;
			}
			var reader = new FileReader();
			// set where you want to attach the preview
			reader.target_elem = $(input).parent().find('preview');
			reader.onload = function (e) {
				var force_crop = true;
				var img = document.createElement("img");
				img.onload = function(){
					if(img.width == 200 && img.height == 200){
						force_crop = false;
					}
					$('#imageupload').attr('src', e.target.result);
					launchEditor('imageupload', e.target.result, input.files[0].type, force_crop);
				}
				img.src = e.target.result;
			};
			reader.readAsDataURL(input.files[0]);
		}
	});

	$('#add_edit_form').validate({
		rules: {
			name: {
				required: true
			},
			image_file:{accept: false},
		},
		messages: {
			name: {
				required: 'Please provide the name',
			}
		},
		submitHandler: function(form) {
			showLoading();
			form.submit();
		}
	});
}
function deleteImageFunction(object, updatedb){
	var sales_person_id = object.attr('sales_person_id');
	var image_obj = object.parent();
	$("<div><span>Are you sure you want to delete this image permanently?</span></div>").dialog({
		resizable: false,
		height:140,
		modal: true,
		buttons: {
			"Delete Image": function() {
				if(updatedb){
					showLoading();
					$.post('<?= base_url().'admin/builders/deletesalespersonimage/'?>'+sales_person_id,{
							sales_person_id: sales_person_id
						},
						function(data,status){
							hideLoading();
							var alert_msg = '';
							if(status === 'success'){
								var result = jQuery.parseJSON(data);
								if(result.status == 1){
									alert_msg = '<div title="Success">'+result.msg+'</div>';
									image_obj.remove();
								}
								else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
							}
							else{ alert_msg = '<div title="Fail">'+data+'</div>';}
							messageAlert(alert_msg);
					});
				}
				else{
					image_obj.remove();
					messageAlert('<div title="Success">The image was deleted successfully.</div>');
				}
				$(this).dialog("close");
			},
			Cancel: function() {
				$('#save_changes').focus();
				$(this).dialog( "close" );
			}
		}
	});
}

function launchEditor(id, src, filetype, force_crop){
	var editor_params = {
		image: id,
		url: src,
		fileFormat: filetype,
		onSave: function(imageID, newURL) {
			addNewImageHtml(newURL, newURL);
			var img = document.getElementById(imageID);
			img.src = newURL;
			featherEditor.close();
		}
	};
	if(force_crop){
		editor_params.forceCropPreset = ['Width: 200px, Height: 200px','200x200'];
		editor_params.initTool = 'crop';
		editor_params.cropPresetsStrict = 'true';
	}
	else{
		editor_params.tools = 'enhance,effects,frames,stickers,focus,brightness,contrast,saturation,warmth,sharpness,colorsplash,draw,text,redeye,whiten,blemish';
	}
	featherEditor.launch(editor_params);
}

function addNewImageHtml(newURL, imagecontent){
	var new_image_html = '<li class="ui-state-default"><input name="image_name" type="text" value="'+newURL+'" style="display:none"><div><img src="'+imagecontent+'" height="130" /></div>';
	new_image_html += '<button type="button" image_id="'+newURL+'" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button></li>';
	$('#sortable').html(new_image_html);
	$('.deleteimage').on('click', function(e){
		deleteImageFunction($(this), false);
	});
}


$( document ).ready(function() {
	// create a new builder record
	if($('#action_continue_btn').length > 0){
		$('#action_continue_btn').click(function() {
			document.getElementById('action_continue').value = 1;
		});
		$('#action_btn').click(function() {
			document.getElementById('action_continue').value = 0;
		});
	}

});
</script>