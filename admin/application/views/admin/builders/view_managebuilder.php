<?php
$base_url    = base_url().'admin/builders/';
$reports_url = base_url().'admin/reports/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Manage <?= "{$builder->builder_name}"; ?></h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<?= $alert_message;?>
					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-settings"></span>
									<span>General Settings</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>editbuilder/<?= $builder->builder_id;?>">
									<button type="button" class="btn btn-primary">Manage</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-users"></span>
									<span>Linked Users</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>builderusers/<?= $builder->builder_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-primary">Builder Users</button>
								</a>

								<a href="<?= $base_url; ?>salespeople/<?= $builder->builder_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Sales People</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-settings"></span>
									<span>Other Actions</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url.'deletebuilder/'.$builder->builder_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-danger" onclick="if(!confirm('Are you sure you want to delete the Builder?')){return false;}">Delete Builder</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
