<?php
$base_url = base_url('admin/builders');
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Builders</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>All Builders</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($builders)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Builder ID</th>
											<th>Builder Name</th>
											<th>Users</th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_builder_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_builder_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_builder_users" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($builders as $builder):?>
										<tr>
											<td><?= $builder->builder_id; ?></td>
											<td><?= $builder->builder_name; ?></td>
											<td>
												<?php if(isset($builders_users[$builder->builder_id])):?>
													<?php $users = array_map(function($v){return $v->name;}, $builders_users[$builder->builder_id]); ?>
													<?php echo '* '.implode('<br>* ', $users); ?>
												<?php endif;?>
											</td>
											<td><a href="<?= "{$base_url}/managebuilder/{$builder->builder_id}"; ?>"><button class="view_builder_btns btn btn-xs btn-default">Manage</button></a></td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not builders in the system.</div>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<div>
					<a href="<?= $base_url; ?>/addbuilder/">
						<button type="button" class="btn btn-info">Add Builder</button>
					</a>
				</div>
				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Image field necesary to call Aviary and crop the images -->
<img id='imageupload' src='' style="display:none"/>
<!-- Dialog -->
<div id="dialog_builder"></div>