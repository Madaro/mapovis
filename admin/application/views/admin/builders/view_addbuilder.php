<?php
$base_url         = base_url().'admin/builders/';
$validtion_errors = validation_errors();
$validation_msg   = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
				<h3>Builders</h3> 
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Add Builder</span>
								</h4>
							</div>
							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" id="add_edit_house_form" class="form-horizontal" action="<?= $base_url; ?>addbuilder" role="form" ENCTYPE="multipart/form-data">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Name:</label>
									<div class="col-lg-9">
									<input id="builder_name" name="builder_name" type="text" class="form-control" value="<?= set_value('builder_name');?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Website:</label>
									<div class="col-lg-9">
									<input id="builder_website" name="builder_website" type="text" class="form-control" value="<?= set_value('builder_website');?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Email:</label>
									<div class="col-lg-9">
									<input id="builder_email" name="builder_email" type="text" class="form-control" value="<?= set_value('builder_email');?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Telephone:</label>
									<div class="col-lg-9">
									<input id="builder_telephone" name="builder_telephone" type="text" class="form-control" value="<?= set_value('builder_telephone');?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group" style="padding-top:10px">
									<div class="col-lg-9" style="padding-left:145px;">
										<button id="save_builder_changes" type="submit" class="btn btn-info">Save Changes</button>
										<a href="<?= $base_url.'viewbuilders';?>" style="margin-left:10px"><button id="cancel_builder_changes" type="button" class="btn btn-default" >Cancel</button></a>
									</div>
								</div><!-- End .form-group  -->
							</form>

						</div>

					</div><!-- End .span6 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

	</div><!-- End #wrapper -->
