<?php
$base_url       = base_url().'admin/builders/';
$builder_id     = $builder->builder_id;
$validtion_errors = validation_errors();
$validation_msg   = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
	<div id="content" class="clearfix">
		<div class="contentwrapper"><!--Content wrapper-->
			<div class="heading">
				<h3><a href="<?= $base_url.'managebuilder/'.$builder->builder_id;?>">Manage <?= $builder->builder_name;?></a></h3> 
			</div><!-- End .heading-->
			<div class="row">
				<div class="col-lg-12">
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>
								<span class="icon16 icomoon-icon-home-6"></span>
								<span>Settings</span>
							</h4>
						</div>
						<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" id="add_edit_builder_form" action="<?= $base_url; ?>editbuilder/<?= $builder_id;?>" role="form">
								<div class="form-group">
									<label class="col-lg-2 control-label" >Builder ID:</label>
									<div class="col-lg-5">
									<input type="text" class="form-control" value="<?= $builder_id;?>" disabled>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" >Name:</label>
									<div class="col-lg-5">
									<input name="builder_name" id="builder_name" type="text" class="form-control" value="<?= set_value('builder_name', $builder->builder_name);?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" >Website:</label>
									<div class="col-lg-5">
									<input name="builder_website" id="builder_website" type="text" class="form-control" value="<?= set_value('builder_website', $builder->builder_website);?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" >Email:</label>
									<div class="col-lg-5">
									<input name="builder_email" id="builder_email" type="text" class="form-control" value="<?= set_value('builder_email', $builder->builder_email);?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" >Telephone:</label>
									<div class="col-lg-5">
									<input name="builder_telephone" id="builder_telephone" type="text" class="form-control" value="<?= set_value('builder_telephone', $builder->builder_telephone);?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group" style="padding-top:10px">
									<div class="col-lg-offset-2 col-lg-5">
										<button id="save_builder_changes" type="submit" class="btn btn-info">Save Changes</button>
										<a href="<?= $base_url.'managebuilder/'.$builder->builder_id;?>" style="margin-left:10px"><button id="cancel_builder_changes" type="button" class="btn btn-default" >Cancel</button></a>
									</div>
								</div><!-- End .form-group  -->
								<input type="hidden" value="<?= $builder->builder_id;?>" name="builder_id">

							</form>
						</div>
					</div><!-- End .span6 -->
				</div>
			</div>
		</div>
	</div>