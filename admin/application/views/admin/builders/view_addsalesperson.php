<?php
$base_url         = base_url().'admin/builders/';
$validtion_errors = validation_errors();
$validation_msg   = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
				<h3><a href="<?= $base_url.'managebuilder/'.$builder->builder_id;?>">Manage <?= $builder->builder_name;?></a></h3> 
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Add Builder Sales Person</span>
								</h4>
							</div>
							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" id="add_edit_form" class="form-horizontal" action="<?= $base_url; ?>addsalesperson/<?= $builder->builder_id;?>" role="form" ENCTYPE="multipart/form-data">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Name:</label>
									<div class="col-lg-9">
									<input id="name" name="name" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Telephone:</label>
									<div class="col-lg-9">
									<input id="telephone" name="telephone" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Mobilephone:</label>
									<div class="col-lg-9">
									<input id="mobilephone" name="mobilephone" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Email:</label>
									<div class="col-lg-9">
									<input id="email" name="email" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Picture:</label>
									<div class="col-lg-9">
										<div id="add_image_section<?= $builder->builder_id;?>" class="add_image_section">
											<input type="file" name="image_file" id="image_file" class="image_files" value="" accept="image/*" >
											<button type="button" id="add_image_btn" class="add_image_btn btn btn-success" area="" href="#">Add & Edit Picture</button>
											<img id='imageupload' src='' style="display:none"/>
											<br/><br/>
										</div>
										<div id="img_container" class="img_container"><ul class="sortable" id="sortable"></ul></div>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group" style="padding-top:10px">
									<div class="col-lg-9" style="padding-left:145px;">
										<button id="save_builder_changes" type="submit" class="btn btn-info">Save Changes</button>
										<a href="<?= $base_url.'salespeople/'.$builder->builder_id;?>" style="margin-left:10px"><button id="cancel_builder_changes" type="button" class="btn btn-default" >Cancel</button></a>
									</div>
								</div><!-- End .form-group  -->
							</form>

						</div>

					</div><!-- End .span6 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

	</div><!-- End #wrapper -->
<script type="text/javascript">
$( document ).ready(function() {
	initilizeView(0);
});
</script>