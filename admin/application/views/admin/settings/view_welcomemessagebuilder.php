<?php
$base_url         = base_url().'admin/settings/';
$validtion_errors = validation_errors();
$validation_msg   = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3>Settings</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-bubble-5"></span>
									<span>Welcome Message for Builder Users</span>
								</h4>
							</div>
							<div class="panel-body">

							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" action="<?= $base_url; ?>welcomemessagebuilder" class="form-horizontal" role="form">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Welcome Message HTML</label>
									<div class="col-lg-9">
									<textarea name="welcome_message_builder" id="textarea1" rows="3" class="form-control elastic"><?= $settings->welcome_message_builder;?></textarea>
									</div>
								</div><!-- End .form-group  -->

								<div style="padding-left:50px;">
									<br>
									<button type="submit" class="btn btn-info">Update</button>
									<a href="<?= base_url().'admin'; ?>"><button type="button" class="btn btn-default">Cancel</button></a>
									<br><br>
								</div>
						</form>
					</div>

					</div><!-- End .panel -->

				</div><!-- End .span3 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
