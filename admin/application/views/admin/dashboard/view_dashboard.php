<?php
$base_url = base_url().'admin';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3>Admin Dashboard</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-4">
						<div class="well well-lg">
						   <div class="reminder">
							<h4>Welcome <?= $current_user->name;?></h4>
							<ul>
								<li class="clearfix">
									<div class="icon">
										<span class="icon32 icomoon-icon-support red"></span>
									</div>
									<span class="number"><?= $total_overdue;?></span> 
									<span class="txt">Overdue Users</span>
									<a href="<?= $base_url?>/reports/overdueusersreport" class="btn btn-warning">View</a>
								</li>
								<li class="clearfix">
									<div class="icon">
										<span class="icon32 icomoon-icon-new green"></span>
									</div>
									<span class="number"><?= $total_problem_user;?></span> 
									<span class="txt">Problem Users</span>
									<a href="<?= $base_url?>/reports/problemusersreport"class="btn btn-warning">View</a>
								</li>
								
							</ul>
						</div><!-- End .reminder -->

					</div>

				</div><!-- End .span4 -->

				<div class="col-lg-8">
							<div class="panel panel-default gradient">
								<div class="panel-heading">
									<h4>
										<span class="icon16 icomoon-icon-database-2"></span>
										<span>Recent activity</span>
									</h4>
								</div>
								<div class="panel-body noPad">
									<ul class="activity">
										<?php foreach($recent_logs as $recent_log):?>
										<li>
											<span class="icon16 icomoon-icon-marker gray"></span>
											<strong><?= $recent_log->name; ?></strong> - <?= $recent_log->development_name; ?> [Stage <?= $recent_log->stage_number; ?>/Lot <?= $recent_log->lot_number; ?>] - <?= str_replace("\n", ' - ', $recent_log->action_note); ?>
											<small><?= $recent_log->formatted_diff;?> ago</small>
										</li>
										<?php endforeach;?>
									</ul>
								</div>

							</div><!-- End .panel -->
						</div><!-- End .span6 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
