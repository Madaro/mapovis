<?php
$base_url         = base_url().'admin/users/';
$validtion_errors = validation_errors();
$validation_msg   = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Users</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-user"></span>
									<span>Add User</span>
								</h4>
							</div>

							<div class="panel-body">
								<?= $validation_msg;?>
								<form action="<?= $base_url; ?>add" class="form-horizontal seperator" role="form" method="post">
									<div class="form-group">
										<label class="col-lg-2 control-label" for="name">Full Name:</label>
										<div class="col-lg-10">
											<input class="form-control" id="name" name="name" type="text" value="<?= set_value('name'); ?>" />
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="email">Email Address:</label>
										<div class="col-lg-10">
											<input class="form-control" id="username" name="email" type="text" value="<?= set_value('email'); ?>" />
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="username">Username:</label>
										<div class="col-lg-10">
											<input class="form-control" id="username" name="username" type="text" value="<?= set_value('username'); ?>" placeholder="(Minimum 6 and only alpha-numeric characters)" />
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="password">Password:</label>
										<div class="col-lg-10">
											<input class="form-control" id="password" name="password" type="password" />
										</div>
									</div><!-- End .form-group  -->

									 <div class="form-group">
										 <label class="col-lg-2 control-label">Developer:</label>
										 <div class="col-lg-4">
											 <?php $selected_dev = set_value('developer');?>
											<select id="developer" name="developer" class="form-control">
												<option value=""></option>
												<?php foreach ($developers as $developer):?>
													<option value="<?= $developer->developer_id;?>" <?= ($selected_dev == $developer->developer_id? 'selected="selected"': '');?>><?=$developer->developer;?></option>
												<?php endforeach; ?> 
											</select>
										 </div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label">Office:</label>
										<div class="col-lg-10">
											<input class="form-control" id="office" name="office" type="text" value="<?= set_value('office'); ?>" />
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="officephone">Office Phone:</label>
										<div class="col-lg-10">
											<input class="form-control" id="officetelephone" name="officephone" type="text" value="<?= set_value('officephone'); ?>" />
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="mobilephone">Mobile Phone:</label>
										<div class="col-lg-10">
											<input class="form-control" id="mobilephone" name="mobilephone" type="text" placeholder="+614XXXXXXXX format" value="<?= set_value('mobilephone'); ?>" />
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<div class="col-lg-offset-2">
											<button type="submit" class="btn btn-info marginR10 marginL10">Add User</button>
											<a href="<?= $base_url?>"><button type="button" class="btn btn-danger">Cancel</button></a>
										</div>
									</div><!-- End .form-group  -->

								</form>

							</div><!-- End .span12 -->

						</div><!-- End .panel -->

					</div><!-- End .col-lg-12 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
