<?php
$base_url = base_url().'admin/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Builder Users</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span>View Builder Users</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Builder User ID</th>
											<th>Username</th>
											<th>Name</th>
											<th>Email</th>
											<th>Builder</th>
											<th>Office</th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
										<tr>
										  <td><input type="text" name="search_user_id" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_username" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_name" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_email" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_builder" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_office" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td></td>
										  <td></td>
										  <td></td>
										</tr>
									</thead>
									<tbody>

									<?php // Add in the USERS from the DB query to the Table
										foreach ($users as $user):?>
										<tr>
											<td><?= $user->user_id;?></td>
											<td><?= $user->username;?></td>
											<td><?= $user->name;?></td>
											<td><?= $user->email;?></td>
											<td><?= $user->builder_name;?></td>
											<td><?= $user->office;?></td>
											<td><a href="<?= "{$base_url}users/updatebuilderuser/{$user->user_id}"; ?>"><button class="btn btn-xs btn-default">Update</button></a></td>
											<td><a href="<?= "{$base_url}users/builderproxy/{$user->user_id}"; ?>" target="_blank"><button class="btn btn-xs btn-default">Login As Builder User</button></a></td>
											<td><a href="<?= "{$base_url}users/deletebuilderuser/{$user->user_id}"; ?>"><button type="button" class="btn btn-xs btn-danger" onclick="if(!confirm('Are you sure you want to delete the Builder User?')){return false;}">Delete</button></a></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
