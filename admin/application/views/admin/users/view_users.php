<?php
$base_url = base_url().'admin/users/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Users</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-user"></span>
									<span>Users</span>
								</h4>
							</div>
							<div class="panel-body">
								<a href="<?php echo $base_url; ?>view"><button class="btn btn-primary">
									View</button></a>

								<a href="<?php echo $base_url; ?>add" style="padding-left:10px;"><button class="btn btn-info">
									Add</button></a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-user"></span>
									<span>Builder Users</span>
								</h4>
							</div>

							<div class="panel-body">
								<a href="<?php echo $base_url; ?>viewbuilders"><button class="btn btn-primary">
									View</button></a>

								<a href="<?php echo $base_url; ?>addbuilderuser" style="padding-left:10px;"><button class="btn btn-info">
									Add</button></a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 wpzoom-locked"></span>
									<span>Admins</span>
								</h4>
							</div>

							<div class="panel-body">
								<a href="<?php echo $base_url; ?>viewadmins"><button class="btn btn-primary">
									View</button></a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
