		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Admin Users</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span>View Admins</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Admin ID</th>
											<th>Username</th>
											<th>Name</th>
											<th>Email</th>
											<th>Mobile</th>
										</tr>
										<tr>
										  <td><input type="text" name="search_admin_id" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_username" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_name" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_email" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_mobile" placeholder="" class="search_init" style="width: 100%;" /></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($users as $user):?>
										<tr>
											<td><?= $user->user_id;?></td>
											<td><?= $user->username;?></td>
											<td><?= $user->name;?></td>
											<td><?= $user->email;?></td>
											<td><?= $user->mobilephone;?></td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->
				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
