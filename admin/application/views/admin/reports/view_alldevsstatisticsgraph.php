<?php
$base_url           = base_url().'admin/reports/';
$formate_start_date = date('d-m-Y', strtotime($start_date));
$formate_end_date   = date('d-m-Y', strtotime($end_date));

/* LOT VIEWS */
$graph_lot_views       = array('lot_views' => array(),'categories' => array());

/* DIRECTION SEARCH VIEWS */
$graph_direction_search_views       = array('direction_search_views' => array(),'categories' => array()); 

/* PLACES SEARCH VIEWS*/
$graph_places_search_views       = array('places_search_views' => array(),'categories' => array());
$previous_week_firstday = "0000-00-00";
$lot_views = 0; 
$direction_search_views = 0;
$places_search_views = 0;
$externalamenity_views = 0;
$externalamenitycategory_views = 0;
$interactivemasterplan_views = 0;
$amenity_views = 0;
$land_searches = 0;
$houseland_searches = 0;

foreach($developments as $development){
    foreach($report_dates as $date_index => $date_label){
	    // LOTS
	    $lot_formated_date = $date_label;
        foreach($lot_views_by_date_developments[$development->development_id] as $views_date_index => $views_date){
        
          if(($previous_week_firstday < $views_date_index) && ($views_date_index <= $date_index)){
              if(isset($lot_views_by_date_developments[$development->development_id][$views_date_index])){
                $lot_views += $lot_views_by_date_developments[$development->development_id][$views_date_index]->views;   
              }
              else{
                $lot_views += 0;
              }
              
          }
        
        }

        $graph_lot_views['lot_views'][$development->development_id][]  = $lot_views;
        $graph_lot_views['categories'][$development->development_id][] = $lot_formated_date;
        $lot_views = 0;
        
        // DIRECTIONS
        $direction_formatted_date = $date_label;

        foreach($direction_search_views_by_date_developments[$development->development_id] as $views_date_index => $views_date){
        
          if(($previous_week_firstday < $views_date_index) && ($views_date_index <= $date_index)){
              if(isset($direction_search_views_by_date_developments[$development->development_id][$views_date_index])){
                $direction_search_views += $direction_search_views_by_date_developments[$development->development_id][$views_date_index]->views;   
              }
              else{
                $direction_search_views += 0;
              }
              
          }
        
        }
        $graph_direction_search_views['direction_search_views'][$development->development_id][]  = $direction_search_views;
        $graph_direction_search_views['categories'][$development->development_id][] = $direction_formatted_date;
        $direction_search_views = 0;
        
        // PLACES

        $places_formatted_date = $date_label; 
        
        foreach($place_search_views_by_date_developments[$development->development_id] as $views_date_index => $views_date){
        
          if(($previous_week_firstday < $views_date_index) && ($views_date_index <= $date_index)){
              if(isset($place_search_views_by_date_developments[$development->development_id][$views_date_index])){
                $places_search_views += $place_search_views_by_date_developments[$development->development_id][$views_date_index]->views;   
              }
              else{
                $places_search_views += 0;
              }
              
          }
        
        }
        
        $graph_places_search_views['places_search_views'][$development->development_id][]  = $places_search_views;
        $graph_places_search_views['categories'][$development->development_id][] = $places_formatted_date;
        $places_search_views = 0;
        // 
        
        $externalamenities_formatted_date = $date_label; 
        
        foreach($externalamenity_views_by_date_developments[$development->development_id] as $views_date_index => $views_date){
        
          if(($previous_week_firstday < $views_date_index) && ($views_date_index <= $date_index)){
              if(isset($externalamenity_views_by_date_developments[$development->development_id][$views_date_index])){
                $externalamenity_views += $externalamenity_views_by_date_developments[$development->development_id][$views_date_index]->views;   
              }
              else{
                $externalamenity_views += 0;
              }
              
          }
        
        }
        $graph_externalamenity_views['externalamenity_views'][$development->development_id][]  = $externalamenity_views;
        $graph_externalamenity_views['categories'][$development->development_id][] = $externalamenities_formatted_date;
        $externalamenity_views = 0;
        
        // EXTERNAL AMENITY CATEGORY VIEWS
        
        $externalamenitycategory_formatted_date = $date_label; 
        
        foreach($externalamenitycategory_views_by_date_developments[$development->development_id] as $views_date_index => $views_date){
        
          if(($previous_week_firstday < $views_date_index) && ($views_date_index <= $date_index)){
              if(isset($externalamenitycategory_views_by_date_developments[$development->development_id][$views_date_index])){
                $externalamenitycategory_views += $externalamenitycategory_views_by_date_developments[$development->development_id][$views_date_index]->views;   
              }
              else{
                $externalamenitycategory_views += 0;
              }
              
          }
        
        }
        $graph_externalamenitycategory_views['externalamenitycategory_views'][$development->development_id][]  = $externalamenitycategory_views;
        $graph_externalamenitycategory_views['categories'][$development->development_id][] = $externalamenitycategory_formatted_date;
        $externalamenitycategory_views = 0;
        
        // INTREACTIVE MASTER PLAN VIEWS
        $interactivemasterplan_formatted_date = $date_label; 
        
        foreach($interactivemasterplan_views_by_date_developments[$development->development_id] as $views_date_index => $views_date){
        
          if(($previous_week_firstday < $views_date_index) && ($views_date_index <= $date_index)){
              if(isset($interactivemasterplan_views_by_date_developments[$development->development_id][$views_date_index])){
                $interactivemasterplan_views += $interactivemasterplan_views_by_date_developments[$development->development_id][$views_date_index]->views;   
              }
              else{
                $interactivemasterplan_views += 0;
              }
              
          }
        
        }
        $graph_interactivemasterplan_views['interactivemasterplan_views'][$development->development_id][]  = $interactivemasterplan_views;
        $graph_interactivemasterplan_views['categories'][$development->development_id][] = $interactivemasterplan_formatted_date;
        $interactivemasterplan_views = 0;
        // Internal Amenities Views
        
        $amenities_formatted_date = $date_label; 
        
        foreach($amenity_views_by_date_developments[$development->development_id] as $views_date_index => $views_date){
        
          if(($previous_week_firstday < $views_date_index) && ($views_date_index <= $date_index)){
              if(isset($amenity_views_by_date_developments[$development->development_id][$views_date_index])){
                $amenity_views += $amenity_views_by_date_developments[$development->development_id][$views_date_index]->views;   
              }
              else{
                $amenity_views += 0;
              }
              
          }
        
        }
        $graph_amenity_views['amenity_views'][$development->development_id][]  = $amenity_views;
        $graph_amenity_views['categories'][$development->development_id][] = $amenities_formatted_date;
        $amenity_views = 0;
        // Land For Sale Searches
        
        $landsearches_formatted_date = $date_label; 

        foreach($landsearches_by_date_developments[$development->development_id] as $views_date_index => $views_date){
        
          if(($previous_week_firstday < $views_date_index) && ($views_date_index <= $date_index)){
              if(isset($landsearches_by_date_developments[$development->development_id][$views_date_index])){
                $land_searches += $landsearches_by_date_developments[$development->development_id][$views_date_index]->views;   
              }
              else{
                $land_searches += 0;
              }
              
          }
        
        }
        $graph_landsearches['land_searches'][$development->development_id][]  = $land_searches;
        $graph_landsearches['categories'][$development->development_id][] = $landsearches_formatted_date;
        $land_searches = 0;
        // House & Land Package Searches
        
        $houselandsearches_formatted_date = $date_label; 
      
        foreach($houselandsearches_by_date_developments[$development->development_id] as $views_date_index => $views_date){
        
          if(($previous_week_firstday < $views_date_index) && ($views_date_index <= $date_index)){
              if(isset($houselandsearches_by_date_developments[$development->development_id][$views_date_index])){
                $houseland_searches += $houselandsearches_by_date_developments[$development->development_id][$views_date_index]->views;   
              }
              else{
                $houseland_searches += 0;
              }
              
          }
        
        }
        $graph_houselandsearches['houseland_searches'][$development->development_id][]  = $houseland_searches;
        $graph_houselandsearches['categories'][$development->development_id][] = $houselandsearches_formatted_date;
        $houseland_searches = 0;
        
        $previous_week_firstday = $date_index; 
         
    }        
}


?> 
<style>
    table#developments_list  {
        border: 1px solid #c4c4c4;
        border-collapse: collapse;
        width:425px;
    }
    
    table#developments_list td,table#developments_list th{
        border: 1px solid #c4c4c4; 
        padding: 5px; 
        text-align: center;
    }
    
</style>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Statistics for All Developments</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-calendar-2"></span>
									<span>Date Range for Statistics Report</span>
								</h4>
							</div>

							<form id="generalsettinsForm" method="post" class="form-horizontal" action="<?= $base_url; ?>alldevsstatisticsreport" role="form">
							<div class="panel-body">
								<div id="date-range-container" style="margin-left: auto; margin-right: auto;"></div>
								<br/>
								<input name="dates" id="date-range" size="40" value="<?= $start_date?> to <?= $end_date?>" style="display: none;">
								<br/>
                                <table id="developments_list">
                                <tr>
                                  <th colspan="2">Developments</th>
                                </tr>
                                <?php
                                    foreach($all_active_developments as $development){  ?>
                                      <tr>
                                        <td><?= $development->development_name ?></td>
                                        <td><input type="checkbox"  class="active_development" name="selected_developments[<?= $development->development_id ?>]"></td> 
                                      </tr>
                                <?php
                                    }
                                ?>
                                </table> 
                                <br/>
                                <span>Select all:</span><input type="radio" name="all_devs" value="all" id="selectall_devs">&nbsp;&nbsp;&nbsp;
                                <span>Select none:</span><input type="radio" name="all_devs" value="none" id="unselectall_devs" checked>
                                <br/>
                                <br/>
								<input type="hidden" id="actiontype_field" name="action_type" value="submit"/>
								<button type="submit" actionType="submit" class="action_btn btn btn-default">Refresh Statistics</button>
							</div>
							</form>
						</div><!-- End .panel -->
					</div><!-- End .span8 -->
				</div><!-- End .row -->
                <?php
                 if(count($developments) == 0){
                    echo "There is no data to display.";
                 }
                 else{
                 ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 icomoon-icon-stats"></span>
                                    <span>GRAPH of Interactive Masterplan Views - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
                                </h4>
                            </div>

                            <div class="panel-body">
                                <?php
                         
                                    if(count($developments) == 0){
                                        echo "There is no data to display.";
                                    }
                                    else{
                                        $valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();

                                        $categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
                                        $categoryAxis->categories($graph_interactivemasterplan_views['categories'][$developments[0]->development_id])->majorGridLines(array('visible' => false)); 
                                        $categoryAxis->labels(array('rotation' => -45));

                                        $tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
                                        $tooltip->visible(true)
                                                ->format('{0}%')
                                                ->template('#= series.name #: #= value #');


                                        $legend       = new \Kendo\Dataviz\UI\ChartLegend();
                                        $legend->visible(false);

                                        $interactivemasterplanchart     = new \Kendo\Dataviz\UI\Chart('interactivemasterplanchart');
                                        $interactivemasterplanchart->legend($legend);
                                        foreach($developments as $development){ 
                                             $interactivemasterplanviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
                                             $interactivemasterplanviews->name($development->development_name)->data($graph_interactivemasterplan_views['interactivemasterplan_views'][$development->development_id]);
                                             $interactivemasterplanchart->addSeriesItem($interactivemasterplanviews);
                                        }
                                        
                                              
                                              $interactivemasterplanchart->addValueAxisItem($valueAxis)
                                              ->addCategoryAxisItem($categoryAxis)
                                              ->tooltip($tooltip)
                                              ->chartArea(array('background' => 'transparent'))
                                              ->seriesColors($chart_colors)
                                              ->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

                                        echo $interactivemasterplanchart->render();
                                    }
                                    
                                ?>
                            </div>

                        </div><!-- End .panel -->
                    </div><!-- End .span8 -->
                </div><!-- End .row -->
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 icomoon-icon-stats"></span>
                                    <span>GRAPH of Internal Amenities Views - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
                                </h4>
                            </div>

                            <div class="panel-body">
                                <?php
                         
                                    if(count($developments) == 0){
                                        echo "There is no data to display.";
                                    }
                                    else{
                                        $valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();

                                        $categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
                                        $categoryAxis->categories($graph_amenity_views['categories'][$developments[0]->development_id])->majorGridLines(array('visible' => false)); 
                                        $categoryAxis->labels(array('rotation' => -45));

                                        $tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
                                        $tooltip->visible(true)
                                                ->format('{0}%')
                                                ->template('#= series.name #: #= value #');


                                        $legend       = new \Kendo\Dataviz\UI\ChartLegend();
                                        $legend->visible(false);

                                        $amenitiesviewchart     = new \Kendo\Dataviz\UI\Chart('amenitiesviewchart');
                                        $amenitiesviewchart->legend($legend);
                                        foreach($developments as $development){ 
                                             $amenityviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
                                             $amenityviews->name($development->development_name)->data($graph_amenity_views['amenity_views'][$development->development_id]);
                                             $amenitiesviewchart->addSeriesItem($amenityviews);
                                        }
                                        
                                              
                                              $amenitiesviewchart->addValueAxisItem($valueAxis)
                                              ->addCategoryAxisItem($categoryAxis)
                                              ->tooltip($tooltip)
                                              ->chartArea(array('background' => 'transparent'))
                                              ->seriesColors($chart_colors)
                                              ->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

                                        echo $amenitiesviewchart->render();
                                    }
                          
                                ?>
                            </div>

                        </div><!-- End .panel -->
                    </div><!-- End .span8 -->
                </div><!-- End .row -->
                
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 icomoon-icon-stats"></span>
                                    <span>GRAPH of Land For Sale Searches - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
                                </h4>
                            </div>

                            <div class="panel-body">
                                <?php
                         
                                    if(count($developments) == 0){
                                        echo "There is no data to display.";
                                    }
                                    else{
                                        $valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();

                                        $categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
                                        $categoryAxis->categories($graph_landsearches['categories'][$developments[0]->development_id])->majorGridLines(array('visible' => false)); 
                                        $categoryAxis->labels(array('rotation' => -45));

                                        $tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
                                        $tooltip->visible(true)
                                                ->format('{0}%')
                                                ->template('#= series.name #: #= value #');


                                        $legend       = new \Kendo\Dataviz\UI\ChartLegend();
                                        $legend->visible(false);

                                        $landsearcheschart     = new \Kendo\Dataviz\UI\Chart('landsearcheschart');
                                        $landsearcheschart->legend($legend);
                                        foreach($developments as $development){ 
                                             $landsearchesview     = new \Kendo\Dataviz\UI\ChartSeriesItem();
                                             $landsearchesview->name($development->development_name)->data($graph_landsearches['land_searches'][$development->development_id]);
                                             $landsearcheschart->addSeriesItem($landsearchesview);
                                        }
                                        
                                              
                                              $landsearcheschart->addValueAxisItem($valueAxis)
                                              ->addCategoryAxisItem($categoryAxis)
                                              ->tooltip($tooltip)
                                              ->chartArea(array('background' => 'transparent'))
                                              ->seriesColors($chart_colors)
                                              ->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

                                        echo $landsearcheschart->render();
                                    }
                          
                                ?>
                            </div>

                        </div><!-- End .panel -->
                    </div><!-- End .span8 -->
                </div><!-- End .row -->
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 icomoon-icon-stats"></span>
                                    <span>GRAPH of House & Land Package Searches - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
                                </h4>
                            </div>

                            <div class="panel-body">
                                <?php
                         
                                    if(count($developments) == 0){
                                        echo "There is no data to display.";
                                    }
                                    else{
                                        $valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();

                                        $categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
                                        $categoryAxis->categories($graph_houselandsearches['categories'][$developments[0]->development_id])->majorGridLines(array('visible' => false)); 
                                        $categoryAxis->labels(array('rotation' => -45));

                                        $tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
                                        $tooltip->visible(true)
                                                ->format('{0}%')
                                                ->template('#= series.name #: #= value #');


                                        $legend       = new \Kendo\Dataviz\UI\ChartLegend();
                                        $legend->visible(false);

                                        $houselandsearcheschart     = new \Kendo\Dataviz\UI\Chart('houselandsearcheschart');
                                        $houselandsearcheschart->legend($legend);
                                        foreach($developments as $development){ 
                                             $houselandsearchesview     = new \Kendo\Dataviz\UI\ChartSeriesItem();
                                             $houselandsearchesview->name($development->development_name)->data($graph_houselandsearches['houseland_searches'][$development->development_id]);
                                             $houselandsearcheschart->addSeriesItem($houselandsearchesview);
                                        }
                                        
                                              
                                              $houselandsearcheschart->addValueAxisItem($valueAxis)
                                              ->addCategoryAxisItem($categoryAxis)
                                              ->tooltip($tooltip)
                                              ->chartArea(array('background' => 'transparent'))
                                              ->seriesColors($chart_colors)
                                              ->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

                                        echo $houselandsearcheschart->render();
                                    }
                          
                                ?>
                            </div>

                        </div><!-- End .panel -->
                    </div><!-- End .span8 -->
                </div><!-- End .row -->
                

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-stats"></span>
									<span>GRAPH of Lot Views - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
								</h4>
							</div>

							<div class="panel-body">
								<?php
                                    if(count($developments) == 0){
                                        echo "There is no data to display.";
                                    }
                                    else{
								        $valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();

								        $categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
								        $categoryAxis->categories($graph_lot_views['categories'][$developments[0]->development_id])->majorGridLines(array('visible' => false)); 
								        $categoryAxis->labels(array('rotation' => -45));

								        $tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
								        $tooltip->visible(true)
										        ->format('{0}%')
										        ->template('#= series.name #: #= value #');


								        $legend       = new \Kendo\Dataviz\UI\ChartLegend();
								        $legend->visible(false);

								        $lotchart     = new \Kendo\Dataviz\UI\Chart('lotchart');
                                        $lotchart->legend($legend);
                                        foreach($developments as $development){ 
                                             $lotviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
                                             $lotviews->name($development->development_name)->data($graph_lot_views['lot_views'][$development->development_id]);
                                             $lotchart->addSeriesItem($lotviews) ;
                                        }
								        
									          
									          $lotchart->addValueAxisItem($valueAxis)
									          ->addCategoryAxisItem($categoryAxis)
									          ->tooltip($tooltip)
									          ->chartArea(array('background' => 'transparent'))
									          ->seriesColors($chart_colors)
									          ->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

								        echo $lotchart->render();
                                    }
                          
								?>
							</div>

						</div><!-- End .panel -->
					</div><!-- End .span8 -->
				</div><!-- End .row -->
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 icomoon-icon-stats"></span>
                                    <span>GRAPH of Direction Search Views - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
                                </h4>
                            </div>

                            <div class="panel-body">
                                <?php
                                    if(count($developments) == 0){
                                        echo "There is no data to display.";
                                    }
                                    else{
                                        $valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();

                                        $categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
                                        $categoryAxis->categories($graph_direction_search_views['categories'][$developments[0]->development_id])->majorGridLines(array('visible' => false)); 
                                        $categoryAxis->labels(array('rotation' => -45));

                                        $tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
                                        $tooltip->visible(true)
                                                ->format('{0}%')
                                                ->template('#= series.name #: #= value #');


                                        $legend       = new \Kendo\Dataviz\UI\ChartLegend();
                                        $legend->visible(false);

                                        $directionsearchchart     = new \Kendo\Dataviz\UI\Chart('directionsearchchart');
                                        $directionsearchchart->legend($legend);
                                        foreach($developments as $development){ 
                                             $directionsearchviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
                                             $directionsearchviews->name($development->development_name)->data($graph_direction_search_views['direction_search_views'][$development->development_id]);
                                             $directionsearchchart->addSeriesItem($directionsearchviews) ;
                                        }
                                        
                                              
                                              $directionsearchchart->addValueAxisItem($valueAxis)
                                              ->addCategoryAxisItem($categoryAxis)
                                              ->tooltip($tooltip)
                                              ->chartArea(array('background' => 'transparent'))
                                              ->seriesColors($chart_colors)
                                              ->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

                                        echo $directionsearchchart->render();
                                    }        
                          
                                ?>
                            </div>

                        </div><!-- End .panel -->
                    </div><!-- End .span8 -->
                </div><!-- End .row -->
                
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 icomoon-icon-stats"></span>
                                    <span>GRAPH of Places Search Views - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
                                </h4>
                            </div>

                            <div class="panel-body">
                                <?php
                                    if(count($developments) == 0){
                                        echo "There is no data to display.";
                                    }
                                    else{
                                        $valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();

                                        $categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
                                        $categoryAxis->categories($graph_places_search_views['categories'][$developments[0]->development_id])->majorGridLines(array('visible' => false)); 
                                        $categoryAxis->labels(array('rotation' => -45));

                                        $tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
                                        $tooltip->visible(true)
                                                ->format('{0}%')
                                                ->template('#= series.name #: #= value #');


                                        $legend       = new \Kendo\Dataviz\UI\ChartLegend();
                                        $legend->visible(false);

                                        $placessearchchart     = new \Kendo\Dataviz\UI\Chart('placessearchchart');
                                        $placessearchchart->legend($legend);
                                        foreach($developments as $development){ 
                                             $placessearchviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
                                             $placessearchviews->name($development->development_name)->data($graph_places_search_views['places_search_views'][$development->development_id]);
                                             $placessearchchart->addSeriesItem($placessearchviews) ;
                                        }
                                        
                                              
                                              $placessearchchart->addValueAxisItem($valueAxis)
                                              ->addCategoryAxisItem($categoryAxis)
                                              ->tooltip($tooltip)
                                              ->chartArea(array('background' => 'transparent'))
                                              ->seriesColors($chart_colors)
                                              ->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

                                        echo $placessearchchart->render();
                                    }        
                          
                                ?>
                            </div>

                        </div><!-- End .panel -->
                    </div><!-- End .span8 -->
                </div><!-- End .row -->
                
                  <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 icomoon-icon-stats"></span>
                                    <span>GRAPH of External Amenity Views - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
                                </h4>
                            </div>

                            <div class="panel-body">
                                <?php
                                    if(count($developments) == 0){
                                        echo "There is no data to display.";
                                    }
                                    else{
                                        $valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();

                                        $categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
                                        $categoryAxis->categories($graph_externalamenity_views['categories'][$developments[0]->development_id])->majorGridLines(array('visible' => false)); 
                                        $categoryAxis->labels(array('rotation' => -45));

                                        $tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
                                        $tooltip->visible(true)
                                                ->format('{0}%')
                                                ->template('#= series.name #: #= value #');


                                        $legend       = new \Kendo\Dataviz\UI\ChartLegend();
                                        $legend->visible(false);

                                        $externalamenityviewschart     = new \Kendo\Dataviz\UI\Chart('externalamenityviewschart');
                                        $externalamenityviewschart->legend($legend);
                                        foreach($developments as $development){ 
                                             $externalamenityviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
                                             $externalamenityviews->name($development->development_name)->data($graph_externalamenity_views['externalamenity_views'][$development->development_id]);
                                             $externalamenityviewschart->addSeriesItem($externalamenityviews) ;
                                        }
                                        
                                              
                                              $externalamenityviewschart->addValueAxisItem($valueAxis)
                                              ->addCategoryAxisItem($categoryAxis)
                                              ->tooltip($tooltip)
                                              ->chartArea(array('background' => 'transparent'))
                                              ->seriesColors($chart_colors)
                                              ->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

                                        echo $externalamenityviewschart->render();
                                    }        
                          
                                ?>
                            </div>

                        </div><!-- End .panel -->
                    </div><!-- End .span8 -->
                </div><!-- End .row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 icomoon-icon-stats"></span>
                                    <span>GRAPH of External Amenity Category Clicks - <?= $formate_start_date?> to <?= $formate_end_date?> (<?=$total_days?> days)</span>
                                </h4>
                            </div>

                            <div class="panel-body">
                                <?php
                         
                                    if(count($developments) == 0){
                                        echo "There is no data to display.";
                                    }
                                    else{
                                        $valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();

                                        $categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
                                        $categoryAxis->categories($graph_externalamenitycategory_views['categories'][$developments[0]->development_id])->majorGridLines(array('visible' => false)); 
                                        $categoryAxis->labels(array('rotation' => -45));

                                        $tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
                                        $tooltip->visible(true)
                                                ->format('{0}%')
                                                ->template('#= series.name #: #= value #');


                                        $legend       = new \Kendo\Dataviz\UI\ChartLegend();
                                        $legend->visible(false);

                                        $externalamenitycategoryviewschart     = new \Kendo\Dataviz\UI\Chart('externalamenitycategoryviewschart');
                                        $externalamenitycategoryviewschart->legend($legend);
                                        foreach($developments as $development){ 
                                             $externalamenitycategoryviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
                                             $externalamenitycategoryviews->name($development->development_name)->data($graph_externalamenitycategory_views['externalamenitycategory_views'][$development->development_id]);
                                             $externalamenitycategoryviewschart->addSeriesItem($externalamenitycategoryviews) ;
                                        }
                                        
                                              
                                              $externalamenitycategoryviewschart->addValueAxisItem($valueAxis)
                                              ->addCategoryAxisItem($categoryAxis)
                                              ->tooltip($tooltip)
                                              ->chartArea(array('background' => 'transparent'))
                                              ->seriesColors($chart_colors)
                                              ->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

                                        echo $externalamenitycategoryviewschart->render(); 
                                    }       
                          
                                ?>
                            </div>

                        </div><!-- End .panel -->
                    </div><!-- End .span8 -->
                </div><!-- End .row -->
             <?php } ?>
			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
