<?php
$base_url           = base_url().'admin/reports/';
$manage_development = base_url('admin/developments/managedevelopmentid/'.$development->development_id);
$formate_start_date = date('d-m-Y', strtotime($start_date));
$formate_end_date   = date('d-m-Y', strtotime($end_date));
?> 
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					 <h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3> 
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-calendar-2"></span>
									<span>Date Range for Statistics Report (Word document)</span>
								</h4>
							</div>

							<form id="generalsettinsForm" method="post" class="form-horizontal" action="<?= $base_url; ?>statisticwordreport/<?= $development->development_id;?>" role="form">
							<div class="panel-body">
								<div id="date-range-container" style="margin-left: auto; margin-right: auto;"></div>
								<br/>
								<input name="dates" id="date-range" size="40" value="<?= $start_date?> to <?= $end_date?>" style="display: none;">
								<br/>
								<input type="hidden" id="actiontype_field" name="action_type" value="submit"/>
								<button type="submit" actionType="submit" class="action_btn btn btn-default">Generate Statistics Document</button>
								<?php if(!empty($new_file_url)):?>
									<a href="<?= $new_file_url?>" style="padding-left: 30px;"><button type="button" actionType="submit" class="btn btn-success">Download Word document Report</button></a>
								<?php endif;?>
							</div>
							</form>
						</div><!-- End .panel -->
					</div><!-- End .span8 -->
				</div><!-- End .row -->


				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<!-- DateRange Picker -->
<link href="<?php echo base_url(); ?>/assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>/assets/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/daterangepicker/statistics-daterangepicker.js"></script>
