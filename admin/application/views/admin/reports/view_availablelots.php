<?php
$base_url = base_url().'admin/reports/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Reports</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default gradient">
								<div class="panel-heading">
									<h4>
										<span>Available Lots Reports</span>
									</h4>
								</div>
								<div class="panel-body noPad clearfix">
									<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
										<thead>
											<tr>
												<th>ID</th>
												<th>Development</th>
												<th>Developer</th>
												<th>State</th>
												<th></th>
											</tr>
											<!-- START - Modification by Seb : Adding Column Filtering for DataTables -->
											<tr>
												<td><input type="text" name="search_id" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_development" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_developer" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_state" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td></td>
											</tr>
											<!-- END - Modification by Seb -->
										</thead>
										<tbody>
											<?php foreach($developments as $development):?>
											<tr>
												<td><?= $development->development_id;?></td>
												<td><?= $development->development_name;?></td>
												<td><?= $development->developer;?></td>
												<td><?= $development->state;?></td>
												<td>
													<div class="controls center">
														<a href="<?php echo $base_url; ?>availablelotsreport/<?= $development->development_id?>"><button class="btn btn-xs btn-default">View Report</button></a>
													</div>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span12 -->

					</div><!-- End .row -->

					<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
