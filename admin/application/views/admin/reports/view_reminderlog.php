		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Reports</h3>                    
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default gradient">
								<div class="panel-heading">
									<h4>
										<span>Reminder Log</span>
									</h4>
								</div>
								<div class="panel-body noPad clearfix">
									<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
										<thead>
											<tr>
												<th>Reminder ID</th>
												<th>Date</th>
												<th>Time - AEST</th>
												<th>User</th>
												<th>Overdue</th>
												<th>Level</th>
												<th>Method</th>
												<th>Sent</th>
											</tr>
											<!-- START - Modification by Seb : Adding Column Filtering for DataTables -->
											<tr>
											  <td><input type="text" name="search_reminder_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_date" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_time" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_user" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_overdue" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_level" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_method" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_sent" placeholder="" class="search_init" style="width: 100%;" /></td>
											</tr>
											<!-- END - Modification by Seb -->
										</thead>
										<tbody>
											<?php foreach($reminder_logs as $reminder_log):?>
											<tr>
												<td><?= $reminder_log->reminder_log_id;?></td>
												<td><?= $reminder_log->formatted_date;?></td>
												<td><?= $reminder_log->formatted_time;?></td>
												<td><?= $reminder_log->user_name;?></td>
												<td><?= $reminder_log->development_name;?></td>
												<td><?= $reminder_log->level;?></td>
												<td><?= $reminder_log->method;?></td>
												<td><?= ($reminder_log->sent == 1)? 'Yes':'No';?></td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
								</div>
							</div><!-- End .panel -->

						</div><!-- End .span12 -->

					</div><!-- End .row -->

					<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
