<?php
$base_url = base_url().'admin/reports/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<?php if(empty($avg_price_by_width['results'])):?>
					<div class="heading">
						<h3>
							<span class="icon16 icomoon-icon-stats"></span>
							<span><?= $development->development_name;?> Average Listed Price ($'000) per Lot Frontage</span>
						</h3>
					</div><!-- End .heading-->
					<h4>
						No results found.
					</h4>
				<?php else:?>
				<div class="heading">
					<div class="col-lg-12">
						<div class="header_btn">
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>averagelotpricesreport/<?= $development->development_id;?>" role="form">
								<button type="submit" class="btn btn-primary" style="float:right;">Export CSV</button>
								<input type="hidden" name="export_csv" value="1">
							</form>
						</div>
					</div>
					<h3></h3>
				</div><!-- End .heading-->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 align="center">
									<span class="icon16 icomoon-icon-stats"></span>
									<span><?= $development->development_name;?> Average Listed Price ($'000) per Lot Frontage</span>
								</h4>
							</div>
							<div class="panel-body">
								<?php
								$avglotchart     = new \Kendo\Dataviz\UI\Chart('avgpricelotchart');

								$avg_legend       = new \Kendo\Dataviz\UI\ChartLegend();
								$avg_legend->visible(true);
								$avglotchart->legend($avg_legend);
								$avglotchart->chartArea(array('background' => 'transparent'));

								$avg_valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();
								$avglotchart->addValueAxisItem($avg_valueAxis);

								$avgprice_categ = array_map(function ($str) { return date('M Y', strtotime('-1 month', strtotime($str))); }, array_keys($avg_price_by_width['results']));
								$avg_categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
								$avg_categoryAxis->categories($avgprice_categ)->majorGridLines(array('visible' => false));
								$avglotchart->addCategoryAxisItem($avg_categoryAxis);

								$avg_tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
								$avg_tooltip->visible(true);
								$avg_tooltip->format('{0}%');
								$avg_tooltip->template('#= series.name #: #= value #');
								$avglotchart->tooltip($avg_tooltip);

								foreach($avg_price_by_width['widths'] as $width){
									$graph_avgprice_lot_width = array();

									foreach($avg_price_by_width['results'] as $month => $month_results){
										$graph_avgprice_lot_width[] = (isset($month_results[$width]->average_price))? round($month_results[$width]->average_price/1000): 0;
									}
									$avg_lotviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
									$avg_lotviews->name($width.'m')->data($graph_avgprice_lot_width);
									$avglotchart->addSeriesItem($avg_lotviews);
								}
								$avglotchart->seriesColors($chart_colors);
								$avglotchart->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

								echo $avglotchart->render();
								?>
							</div>
							<br>
							<div><h5 align="center">*For the purposes of this graph, lots are grouped together to the closest 0.5m frontage.</h5></div>

						</div><!-- End .panel -->
					</div><!-- End .span8 -->
				</div><!-- End .row -->    
				<!-- Page end here -->

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-stats-2"></span>
									<span><?= $development->development_name;?> Average Listed Price ($'000) per Lot Frontage</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dateDynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Month</th>
											<th>Sortable Week</th>
											<?php foreach($avg_price_by_width['widths'] as $width):?>
												<th><?php echo $width;?>m</th>
											<?php endforeach;?>
										</tr>
									</thead>
									<tbody>
										<?php foreach($avg_price_by_width['results'] as $month => $month_results):?>
										<tr>
											<!-- Showing the date a month back because it covers the totals of the previous month -->
											<td><?= date('M Y', strtotime('-1 month', strtotime($month)));?></td>
											<td><?= date('Ym', strtotime('-1 month', strtotime($month)));?></td>
											<?php foreach($avg_price_by_width['widths'] as $width):?>
												<?php $lot_views   = (isset($month_results[$width]->average_price))? number_format($month_results[$width]->average_price/1000): 0;?>
												<td><?= $lot_views;?></td>
											<?php endforeach;?>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->
				<?php endif;?>
				<!-- Page end here -->
			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script type="text/javascript">
	$(document).ready(function() { 	
	if($('table').hasClass('dateDynamicTable')){
		$('.dateDynamicTable').dataTable( {
			// Sort by second colum
			"aoColumns": [
				{"iDataSort": 1},
				{"bVisible": false},
				<?php for($x = 1; $x < count($avg_price_by_width['widths']); $x++):?>
					null,
				<?php endfor;?>
				null
			],
			"aaSorting":[[0, "desc"]], 
			"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
			"sPaginationType": "bootstrap",
			"bJQueryUI": false,
			"bAutoWidth": false,
			"iDisplayLength": (( typeof default_pagination !== 'undefined')? default_pagination: 10),
			"oLanguage": {
				"sSearch": "<span></span> _INPUT_",
				"sLengthMenu": "<span>_MENU_</span>",
				"oPaginate": { "sFirst": "First", "sLast": "Last" }
			}
		});
		$('.dataTables_length select').uniform();
		$('.dataTables_paginate > ul').addClass('pagination');
		$('.dataTables_filter>label>input').addClass('form-control');
		if(typeof default_pagination_url !== 'undefined'){
			$('.selector>select').on('change', function(){
				$.post(default_pagination_url,{
					default_pagination: $(this).val()
				});
			});
		}
	}
});
</script>