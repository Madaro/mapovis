<?php
$base_url = base_url().'admin/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Reports</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default gradient">
								<div class="panel-heading">
									<h4>
										<span>Problems Users Report</span>
									</h4>
								</div>
								<div class="panel-body noPad clearfix">
									<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
										<thead>
											<tr>
												<th>User</th>
												<th>Overdue</th>
												<th>Time</th>
											</tr>
											<!-- START - Modification by Seb : Adding Column Filtering for DataTables -->
											<tr>
											  <td><input type="text" name="search_user" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_overdue" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_time" placeholder="" class="search_init" style="width: 100%;" /></td>
											</tr>
											<!-- END - Modification by Seb -->
										</thead>
										<tbody>
											<?php foreach($problem_users as $problem_user):?>
											<tr>
												<td>
													<?php if($problem_user->primary_user):?>
													<a href="<?= $base_url; ?>users/updateuser/<?= $problem_user->primary_user?>"><?= $problem_user->primary_user_name?></a>
													<?php else:?>
													No user
													<?php endif;?>
												</td>
												<td><?= $problem_user->development_name?></td>
												<td><?= $problem_user->overdue_by?></td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span12 -->

					</div><!-- End .row -->

					<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
