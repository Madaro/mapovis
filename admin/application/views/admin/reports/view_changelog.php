		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3>Reports</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default gradient">
								<div class="panel-heading">
									<h4>
										<span>Lots Change Log</span>
									</h4>
								</div>
								<div class="panel-body noPad clearfix">
									<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
										<thead>
											<tr>
												<th>Change ID</th>
												<th>Date</th>
												<th>Time - AEST</th>
												<th>User</th>
												<th>Type</th>
												<th>Development</th>
												<th>Developer</th>
												<th>Precinct</th>
												<th>Stage</th>
												<th>Lot</th>
												<th>Action</th>
											</tr>
											<!-- START - Modification by Seb : Adding Column Filtering for DataTables -->
											<tr>
												<td><input type="text" name="search_change_id" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_date" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_time" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_user" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_type" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_dev" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_dever" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_precinct" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_stage" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_lot" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_action" placeholder="" class="search_init" style="width: 100%;" /></td>
											<!-- END - Modification by Seb -->
											</tr>
										</thead>
										<tbody>
											<?php foreach($change_logs as $change_log):?>
											<tr>
												<td><?= $change_log->change_log_id;?></td>
												<td><?= $change_log->formatted_date;?></td>
												<td><?= $change_log->formatted_time;?> </td>
												<td><?= $change_log->user_name;?></td>
												<td><?= $change_log->user_type;?></td>
												<td><?= $change_log->development_name;?></td>
												<td><?= $change_log->developer_name;?></td>
												<td><?= $change_log->precinct_number;?></td>
												<td><?= $change_log->stage_number;?></td>
												<td><?= $change_log->lot_number;?></td>
												<td><?= nl2br($change_log->action_note);?></td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span12 -->

					</div><!-- End .row -->

					<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

	</div><!-- End #wrapper -->
