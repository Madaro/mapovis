	<!-- KENDO UI -->
	<link href="<?php echo base_url(); ?>assets/plugins/kendoui/styles/kendo.common.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/kendoui/styles/kendo.default.min.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>assets/plugins/kendoui/js/kendo.all.min.js"></script>

<?php
require_once 'assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php';
$greaterthan90days = new \Kendo\Dataviz\UI\ChartSeriesItem();
$greaterthan90days->name('<90 Days');
$greaterthan90days->stack('Listed');
$greaterthan90days->data($formated_results['greaterthan90days']);

$between90and180days = new \Kendo\Dataviz\UI\ChartSeriesItem();
$between90and180days->name('90-180 Days');
$between90and180days->stack('Listed');
$between90and180days->data($formated_results['between90and180days']);

$greaterthan180days = new \Kendo\Dataviz\UI\ChartSeriesItem();
$greaterthan180days->name('>180 Days');
$greaterthan180days->stack('Listed');
$greaterthan180days->data($formated_results['greaterthan180days']);

$valueAxis = new \Kendo\Dataviz\UI\ChartValueAxisItem();

$categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
$categoryAxis->categories(array("Current","1st of December 2013","1st of November 2013","1st of October 2013"));
$categoryAxis->majorGridLines(array('visible' => false));

$tooltip = new \Kendo\Dataviz\UI\ChartTooltip();
$tooltip->visible(true);
$tooltip->template('#= series.name #: #= value #');

$chart = new \Kendo\Dataviz\UI\Chart('chart');
$chart->legend(array('visible' => true));
$chart->addSeriesItem($greaterthan90days, $between90and180days, $greaterthan180days);
$chart->addValueAxisItem($valueAxis);
$chart->addCategoryAxisItem($categoryAxis);
$chart->tooltip($tooltip);
$chart->seriesColors(array('#009bd7', '#4db9e3', '#99d7ef'));
$chart->seriesDefaults(array('type' => 'column'));

?> 
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Available Lots Report(s)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default gradient">
								<div class="panel-heading">
									<h4>
										<span><?= $development->development_name;?> - Available Lots Graph</span>
									</h4>
								</div>
								<div class="panel-body noPad clearfix">
									<?php echo $chart->render(); ?>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span12 -->

						<div class="col-lg-12">
							<div class="panel panel-default gradient">
								<div class="panel-heading">
									<h4>
										<span><?= $development->development_name;?> - Available Lots Report</span>
									</h4>
								</div>
								<div class="panel-body noPad clearfix">
									<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" width="100%">
										<thead>
											<tr>
												<th></th>
												<?php foreach($formated_labels as $label):?>
												<th><?= $label;?></th>
												<?php endforeach?>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>< 90 Days</td>
												<?php foreach($formated_results['greaterthan90days'] as $value):?>
												<td><?= $value;?></td>
												<?php endforeach?>
											</tr>

											<tr>
												<td>90-180 Days</td>
												<?php foreach($formated_results['between90and180days'] as $value):?>
												<td><?= $value;?></td>
												<?php endforeach?>
											</tr>

											<tr>
												<td>> 180 Days</td>
												<?php foreach($formated_results['greaterthan180days'] as $value):?>
												<td><?= $value;?></td>
												<?php endforeach?>
											</tr>

											<tr>
												<td style="font-weight:bold;">Total</td>
												<?php foreach($formated_results['totals'] as $value):?>
												<td style="font-weight:bold;"><?= $value;?></td>
												<?php endforeach?>
											</tr>

										</tbody>

									</table>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span12 -->

					</div><!-- End .row -->

					<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

	 <?php echo $chart->render(); ?>