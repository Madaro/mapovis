<?php
$base_url     = base_url().'admin/reports/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<?php if(empty($sold_lots_by_width['results'])):?>
					<div class="heading">
						<h3>
							<span class="icon16 icomoon-icon-stats"></span>
							<span><?= $development->development_name;?> Sales Rate by Lot Frontage</span>
						</h3>
					</div><!-- End .heading-->
					<h4>
						No results found.
					</h4>
				<?php else:?>
				<div class="heading">
					<div class="col-lg-12">
						<div class="header_btn">
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>soldlotstatisticsreport/<?= $development->development_id;?>" role="form">
								<button type="submit" class="btn btn-primary" style="float:right;">Export CSV</button>
								<input type="hidden" name="export_csv" value="1">
							</form>
						</div>
					</div>
					<h3></h3>
				</div><!-- End .heading-->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 align="center">
									<span class="icon16 icomoon-icon-stats"></span>
									<span><?= $development->development_name;?> Sales Rate by Lot Frontage</span>
								</h4>
							</div>
							<div class="panel-body">
								<?php
								$soldlotchart      = new \Kendo\Dataviz\UI\Chart('soldlotchart');

								$sold_legend       = new \Kendo\Dataviz\UI\ChartLegend();
								$sold_legend->visible(true);
								$soldlotchart->legend($sold_legend);
								$soldlotchart->chartArea(array('background' => 'transparent'));

								$sold_valueAxis    = new \Kendo\Dataviz\UI\ChartValueAxisItem();
								$soldlotchart->addValueAxisItem($sold_valueAxis);

								$sold_categ        = array_map(function ($str) { return date('M Y', strtotime($str)); }, array_keys($sold_lots_by_width['results']));
								$sold_categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
								$sold_categoryAxis->categories($sold_categ)->majorGridLines(array('visible' => false));
								$soldlotchart->addCategoryAxisItem($sold_categoryAxis);

								$sold_tooltip      = new \Kendo\Dataviz\UI\ChartTooltip();
								$sold_tooltip->visible(true);
								$sold_tooltip->format('{0}%');
								$sold_tooltip->template('#= series.name #: #= value #');
								$soldlotchart->tooltip($sold_tooltip);

								foreach($sold_lots_by_width['widths'] as $width){
									$graph_sold_lot_width = array();
									foreach($sold_lots_by_width['results'] as $month => $month_results){
										$graph_sold_lot_width[] = (isset($month_results[$width]->number_lots))? (int)$month_results[$width]->number_lots: 0;
									}
									$sold_lotviews     = new \Kendo\Dataviz\UI\ChartSeriesItem();
									$sold_lotviews->name($width.'m')->data($graph_sold_lot_width);
									$soldlotchart->addSeriesItem($sold_lotviews);
								}
								$soldlotchart->seriesColors($chart_colors);
								$soldlotchart->seriesDefaults(array('type' => 'line', 'style' => 'smooth'));

								echo $soldlotchart->render();
								?>
							</div>
							<br>
							<div><h5 align="center">*For the purposes of this graph, lots are grouped together to the closest 0.5m frontage.</h5></div>
						</div><!-- End .panel -->
					</div><!-- End .span8 -->
				</div><!-- End .row --> 

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
								   <span class="icon16 icomoon-icon-stats-2"></span>
									<span><?= $development->development_name;?> Sales Rate by Lot Frontage</span>
								</h4>
								</span>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dateDynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Month</th>
											<th>Sortable Week</th>
											<?php foreach($sold_lots_by_width['widths'] as $width):?>
												<th><?php echo $width;?>m</th>
											<?php endforeach;?>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($sold_lots_by_width['results'] as $month => $month_results):?>
										<?php $total_lots = 0;?>
										<tr>
											<td><?= date('M Y', strtotime($month));?></td>
											<td><?= date('Ym', strtotime($month));?></td>
											<?php foreach($sold_lots_by_width['widths'] as $width):?>
												<?php $lot_views   = (isset($month_results[$width]->number_lots))? (int)$month_results[$width]->number_lots: 0;?>
												<?php $total_lots += $lot_views;?>
												<td><?= $lot_views;?></td>
											<?php endforeach;?>
											<td><b><?= $total_lots;?></b></td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->
					</div><!-- End .span12 -->
				</div><!-- End .row -->
				<?php endif;?>
				<!-- Page end here -->
			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script type="text/javascript">
	$(document).ready(function() { 	
	if($('table').hasClass('dateDynamicTable')){
		$('.dateDynamicTable').dataTable( {
			// Sort by second colum
			"aoColumns": [
				{"iDataSort": 1},
				{"bVisible": false},
				<?php for($x = 1; $x <= count($sold_lots_by_width['widths']); $x++):?>
					null,
				<?php endfor;?>
				null
			   ],
			"aaSorting":[[0, "desc"]], 
			"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
			"sPaginationType": "bootstrap",
			"bJQueryUI": false,
			"bAutoWidth": false,
			"iDisplayLength": (( typeof default_pagination !== 'undefined')? default_pagination: 10),
			"oLanguage": {
				"sSearch": "<span></span> _INPUT_",
				"sLengthMenu": "<span>_MENU_</span>",
				"oPaginate": { "sFirst": "First", "sLast": "Last" }
			}
		});
		$('.dataTables_length select').uniform();
		$('.dataTables_paginate > ul').addClass('pagination');
		$('.dataTables_filter>label>input').addClass('form-control');
		if(typeof default_pagination_url !== 'undefined'){
			$('.selector>select').on('change', function(){
				$.post(default_pagination_url,{
					default_pagination: $(this).val()
				});
			});
		}
	}
});
</script>