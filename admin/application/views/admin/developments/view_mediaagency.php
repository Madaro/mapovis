<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-user"></span>
									<span>Media Agency</span>
								</h4>
							</div>
							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>mediaagency/<?= $development->development_id;?>" role="form">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Media Agency:</label>
									<div class="col-lg-3">
										<select name="media_agency" class="form-control">
											<option></option>
											<?php foreach($users_list as $user_details):?>
												<option value="<?= $user_details->user_id;?>" <?= ($user_details->user_id == $development->media_agency)? 'selected="selected"': '';?>><?= $user_details->name;?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div><!-- End .form-group  -->

								<div style="padding-left:15px;">
									<br>
									<button type="submit" class="btn btn-info">Update</button>
									<span style="padding-left: 10px;"><a href="<?= $manage_development; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
									<br><br>
								</div>
									<input name="send_form" type="hidden" value="1">
								</form>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span3 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
