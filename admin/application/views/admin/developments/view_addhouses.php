<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
$house_id           = 0;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Add House</span>
								</h4>
							</div>
							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>addhouses/<?= $development->development_id;?>" role="form" id="add_edit_house_form" ENCTYPE="multipart/form-data">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">House Name:</label>
									<div class="col-lg-9">
									<input name="house_name" id="house_name" type="text" class="form-control">
									<label for="house_name" generated="true" class="error" style="display:none;"></label>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Builder: <br><a href="#" id="new_builder">[add new]</a></label>
									<div class="col-lg-9">
										<select id="house_builder_id" name="house_builder_id" class="form-control">
											<option></option>
											<?php foreach($builders as $builder):?>
												<option value="<?= $builder->builder_id?>" <?= ($builder_id == $builder->builder_id? 'selected="selected"': '')?>><?= $builder->builder_name?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Description:</label>
									<div class="col-lg-9">
										<div class="form-row">
											<textarea class="tinymce" name="house_description"></textarea>
										 </div>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">More Info Link:</label>
									<div class="col-lg-9">
										<input name="house_moreinfo_url" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Bedrooms:</label>
									<div class="col-lg-2">
									<input name="house_bedrooms" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Bathrooms:</label>
									<div class="col-lg-2">
									<input name="house_bathrooms" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Garages:</label>
									<div class="col-lg-2">
									<input name="house_garages" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">House Size:</label>
									<div class="col-lg-2">
									<input name="house_size" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label">Levels:</label>
									<div class="col-lg-2">
									<input name="house_levels" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Facade Pictures:</label>
									<div class="col-lg-9">
										<div id="originalFiles" style="display:none"></div>
										<div id="add_image_section<?= $house_id;?>">
											<input type="file" name="image_file_facade" id="image_file_facade" class="image_files" value="" accept="image/*" >
											<button type="button" id="add_image_btn_facade" class="add_image_btn btn btn-success" area="_facade" house_id="<?= $house_id;?>" href="#">Add & Edit Photo</button>
											<img id='imageupload_facade' src='' style="display:none"/>
											<br/><br/>
										</div>
										<div class="alert alert-info">
											Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
										</div>

										<div id="img_container_facade" class="img_container"><ul class="sortable" id="sortable_facade"></ul></div>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Floor Plans:</label>
									<div class="col-lg-9">
										<div id="add_image_section<?= $house_id;?>">
											<input type="file" name="image_file_floor" id="image_file_floor" class="image_files" value="" accept="image/*" >
											<button type="button" id="add_image_btn_floor" class="add_image_btn btn btn-success" area="_floor" house_id="<?= $house_id;?>" href="#">Add & Edit Photo</button>
											<img id='imageupload_floor' src='' style="display:none"/>
											<br/><br/>
										</div>
										<div class="alert alert-info">
											Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
										</div>

										<div id="img_container_floor" class="img_container"><ul class="sortable" id="sortable_floor"></ul></div>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Gallery Pictures:</label>
									<div class="col-lg-9">
										<div id="add_image_section<?= $house_id;?>">
											<input type="file" name="image_file_gallery" id="image_file_gallery" class="image_files" value="" accept="image/*" >
											<button type="button" id="add_image_btn_gallery" class="add_image_btn btn btn-success" area="_gallery" house_id="<?= $house_id;?>" href="#">Add & Edit Photo</button>
											<img id='imageupload_gallery' src='' style="display:none"/>
											<br/><br/>
										</div>
										<div class="alert alert-info">
											Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
										</div>

										<div id="img_container_gallery" class="img_container"><ul class="sortable" id="sortable_gallery"></ul></div>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group" style="padding-top:10px">
									<div class="col-lg-offset-2 col-lg-9">
										<button id="action_continue_btn" type="submit" class="btn btn-primary save_new_house">Add House & Continue</button>
										<span style="padding-left: 10px;"><button id="action_btn" type="submit" class="btn btn-info save_new_house">Add House & Finish</button></span>
										<span style="padding-left: 10px;"><a href="<?= $manage_development; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
									</div>
								</div><!-- End .form-group  -->
								<input id="action_continue" name="action_continue" type="hidden" value="0">
							</form>

						</div>

					</div><!-- End .span6 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

	</div><!-- End #wrapper -->

<!-- Dialog -->
<div id="dialog_builder" title="Dialog Title">
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<div class="col-lg-9">
			<div class="panel-body">
				<?php if(!empty($validtion_errors)):?>
					<div class="alert alert-danger"><?= validation_errors(); ?></div>
				<?php endif;?>
				<form method="post" class="form-horizontal" action="<?= $base_url; ?>addbuilder/<?= $development->development_id;?>" role="form">
					<div class="form-group">
						<label class="col-lg-2 control-label" for="textareas">Name:</label>
						<div class="col-lg-9">
						<input id="builder_name" name="builder_name" type="text" class="form-control">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-2 control-label" for="textareas">Website:</label>
						<div class="col-lg-9">
						<input id="builder_website" name="builder_website" type="text" class="form-control">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-2 control-label" for="textareas">Email:</label>
						<div class="col-lg-9">
						<input id="builder_email" name="builder_email" type="text" class="form-control">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-2 control-label" for="textareas">Telephone:</label>
						<div class="col-lg-9">
						<input id="builder_telephone" name="builder_telephone" type="text" class="form-control">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group" style="padding-top:10px">
						<div class="col-lg-9" style="padding-left:145px;">
							<button id="add_builder_btn" type="button" class="btn btn-info">Save Changes</button>
							<span style="padding-left:10px;"><button type="button" class="btn btn-default" onclick="$('#dialog_builder').dialog( 'close' );">Cancel</button></span>
						</div>
					</div><!-- End .form-group  -->

				</form>
			</div>
	</div><!-- End .span6 -->
</div>
<script>
$( document ).ready(function() {
	initilizeHouseView(0);
});
</script>
