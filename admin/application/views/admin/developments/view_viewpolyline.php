<?php
$base_url = base_url().'admin/developments/';
?>
    <div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
    <div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
    <div class="col-lg-12">
            <div class="panel-body">
                <form method="post" class="form-horizontal" action="<?= $base_url; ?>updatepolyline/<?= $polyline->id;?>" role="form">
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="required">Name:</label>
                        <div class="col-lg-7">
                            <input type="text" name="polylinename" class="form-control" value="<?= $polyline->polylinename?>" >
                        </div>
                    </div><!-- End .form-group  --> 

                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="required">Stroke Color:</label>
                        <div class="col-lg-7">
                            <input type="text" name="stroke_color" class="form-control" value="<?= $polyline->stroke_color?>" >
                        </div>
                    </div><!-- End .form-group  --> 

                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="required">Stroke Opacity:</label>
                        <div class="col-lg-7">
                            <input type="text" name="stroke_opacity" class="form-control" value="<?= $polyline->stroke_opacity?>" >
                        </div>
                    </div><!-- End .form-group  --> 
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="required">Stroke Weight:</label>
                        <div class="col-lg-7">
                            <input type="text" name="stroke_weight" class="form-control" value="<?= (float)$polyline->stroke_weight?>" >
                        </div>
                    </div><!-- End .form-group  --> 

                    <div class="form-group" style="padding-top:10px">
                        <div class="col-lg-offset-3 col-lg-9">
                            <button type="submit" class="save_changes btn btn-info">Save Changes</button>
                            <button type="button" class="cancel_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
                        </div>
                    </div><!-- End .form-group  -->
                </form>
            </div>
    </div><!-- End .span6 -->