<style>
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 500px; }
	.sortable li { margin: 5px 30px 15px 0; padding: 0px; float: left; width: 136px; height: 131px;  text-align: center; }
	.deleteimage{margin-top: 7px;}
	#img_container{overflow-y:hidden;}
	.ui-dialog { z-index: 65535; }
</style>
<script type="text/javascript" src="http://feather.aviary.com/js/feather.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
var featherEditor = new Aviary.Feather({
	apiKey: '84495f70872a7572',
	apiVersion: 3,
	tools: 'all',
	theme: 'light',
	onError: function(errorObj) {
		console.log(errorObj.message);
	}
});
/* Show and hide "Add Image" when necessary MAX 5 Images */
function showhide_AddNew(amenity_id){
	var total_images     = $('#sortable_'+amenity_id).children().length;
	var container_height = Math.ceil(total_images/3) * 145;
	$('#img_container').attr('style', 'height:'+container_height+'px;width:505px;')
	if(total_images < 5){
		$('#add_image_section'+amenity_id).show();
	}
	else{
		$('#add_image_section'+amenity_id).hide();
	}
	// point user to 'Save Changes' button
	$('#save_changes'+amenity_id).focus();
}
function deleteImageFunction(object, updatedb){
	var amenityid = object.attr('amenity_id');
	var imagename = object.attr('image_id');
	var image_obj = object.parent();
	$("<div><span>Are you sure you want to delete this image permanently?</span></div>").dialog({
		resizable: false,
		height:140,
		modal: true,
		buttons: {
			"Delete Image": function() {
				if(updatedb){
					showLoading();
					$.post("<?= base_url().'admin/developments/deleteexternalamenityimage'?>",{
							amenityid: amenityid,
							imagename: imagename
						},
						function(data,status){
							hideLoading();
							var alert_msg = '';
							if(status === 'success'){
								var result = jQuery.parseJSON(data);
								if(result.status == 1){
									alert_msg = '<div title="Success">'+result.msg+'</div>';
									image_obj.remove();
									showhide_AddNew(amenityid);
								}
								else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
							}
							else{ alert_msg = '<div title="Fail">'+data+'</div>';}
							messageAlert(alert_msg);
					});
				}
				else{
					image_obj.remove();
					showhide_AddNew(amenityid);
					messageAlert('<div title="Success">The image was deleted successfully.</div>');
				}
				$(this).dialog("close");
			},
			Cancel: function() {
				$('#save_changes').focus();
				$(this).dialog( "close" );
			}
		}
	});
}
function addNewImageHtml(amenity_id, newURL, imagecontent){
	var button_id = 'delete_img' + new Date().getTime();
	var new_image_html = '<li class="ui-state-default"><input name="image_names[]" type="text" value="'+newURL+'" style="display:none"><img src="'+imagecontent+'" width="135" height="96" />';
	new_image_html += '<button type="button" id="'+button_id+'" amenity_id="'+amenity_id+'" image_id="'+newURL+'" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button></li>';
	$('#sortable_'+amenity_id).append(new_image_html);
	$('#'+button_id).on('click', function(e){
		deleteImageFunction($(this), false);
	});
}
function initViewEntity(){
	$(function(){
		$('.sortable').sortable({containment: "#img_container",tolerance: "pointer"});
		$('.sortable').disableSelection();
	});
	$('.image_files').on('change', function(){
		var input = $(this)[0];
		console.log(input.files[0])
		if (input.files && input.files[0]){
			if(!validateimagefiletype(input.files[0].type)){
				return false;
			}
		}
	});
	$('.deleteimage').on('click', function(e){
		deleteImageFunction($(this), true);
	});
}
</script>