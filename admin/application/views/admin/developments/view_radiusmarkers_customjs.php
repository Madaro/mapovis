<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
$('#dialog_radiusmarker').dialog({ autoOpen: false });

$('.view_type_btn').on('click', function(){
	var type_id = $(this).attr('type_id');
	showLoading();
	$.post('<?= base_url();?>admin/developments/viewradiusmarker/'+type_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_radiusmarker').html(data);
				$('#dialog_radiusmarker').dialog({
					title: 'Update Radius Marker',
					height: 340,
					width: 550,
					modal: true,
					resizable: false,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_radiusmarker').html('');}
				});
				$('#dialog_radiusmarker').dialog('open');
				$("input, textarea, select").not('.nostyle').uniform();
				hideLoading();
				$('.save_changes').click(function() {
					showLoading();
				});
				$('.cancel_changes').click(function() {
					$('#dialog_radiusmarker').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
$('.delete_radius_marker_btn').click(function() {
	if(!confirm('Are you sure you want to delete the radius marker?')){
		return false;
	}
	showLoading();
});
</script>