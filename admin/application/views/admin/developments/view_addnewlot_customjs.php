<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
/* automatically updating the lot square meters */
function updateSquareMeters(){
	var width = document.getElementById('width').value;
	var depth = document.getElementById('depth').value;
	if(width && depth){
		document.getElementById('squaremeters').value = width * depth;
	}
}
$('#add_street_btn').on('click', function(){
	if($('#street_name').val() == ''){
		messageAlert('<div title="Fail">Please provide the Street Name.</div>');
		return false;
	}
	showLoading();
	$.post("<?= base_url().'admin/developments/addstreetname/'.$development->development_id;?>",{
			street_name: $('#street_name').val(),
		},
		function(data,status){
			var alert_msg = '';
			hideLoading();
			if(status === 'success'){
				var result = jQuery.parseJSON(data);
				if(result.status == 1){
					alert_msg = '<div title="Success">'+result.msg+'</div>';
					var new_street_name_html = '<option value="'+result.street_name_id+'" >'+result.street_name+'</option>';
					$('#street_name_id').append(new_street_name_html).val(result.street_name_id).change();
					$('#dialog').dialog('close');
				}
				else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
			}
			else{ alert_msg = '<div title="Fail">'+data+'</div>';}
			messageAlert(alert_msg);
	});
});
$('#cancel_street_btn').click(function() {
	$('#dialog').dialog('close');
});
$('#dialog').dialog({ autoOpen: false });
$('#dialog').dialog({
	title: 'Add New Street Name',
	height: 230,
	width: 750,
	modal: true,
	resizable: false,
	dialogClass: 'loading-dialog'
});

$('#newstreet_btn').click(function() {
	$('#dialog').dialog('open');
});
</script>