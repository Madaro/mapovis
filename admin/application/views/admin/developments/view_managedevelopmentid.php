<?php
$base_url    = base_url().'admin/developments/';
$reports_url = base_url().'admin/reports/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Manage <?= "{$development->development_name} ({$development->developer})"; ?></h3>

					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<?= $alert_message;?>
					<div class="col-lg-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-settings"></span>
									<span>Settings</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>generalsettings/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">Manage</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-settings"></span>
									<span>Markers</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>manageradiusmarkers/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">Radius M.</button>
								</a>
								<a href="<?= base_url('admin/markers/manageminimarkers/'.$development->development_id); ?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Mini M.</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
                    
                    <div class="col-lg-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 entypo-icon-settings"></span>
                                    <span>External Markers</span>
                                </h4>
                                <a href="#" class="minimize" style="display: none;">Minimize</a>
                            </div>
                            <div class="panel-body">
                                <a href="<?= $base_url; ?>manageexternalmarkers/<?= $development->development_id;?>">
                                    <button type="button" class="btn btn-primary">View</button>
                                </a>
                                <a href="<?= base_url('admin/developments/addexternalmarker/'.$development->development_id); ?>" style="padding-left: 20px;">
                                    <button type="button" class="btn btn-info">Add</button>
                                </a>
                            </div>
                        </div><!-- End .panel -->
                    </div><!-- End .span3 -->

					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-users"></span>
									<span>Linked Users</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>primaryuser/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">Primary</button>
								</a>

								<a href="<?= $base_url; ?>secondaryuser/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Secondary</button>
								</a>

								<a href="<?= $base_url; ?>manageruser/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-success">Manager</button>
								</a>

								<a href="<?= $base_url; ?>mediaagency/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-primary">Media Agency</button>
								</a>

								<a href="<?= $base_url; ?>devgeneralusers/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">General Users</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
				</div><!-- End .row -->

				<div class="row">
					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-grid"></span>
									<span>Precincts</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>viewprecincts/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">View Precincts</button>
								</a>

								<a href="<?= $base_url; ?>addprecincts/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Add Precincts</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
                    
                    <div class="col-lg-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 entypo-icon-grid"></span>
                                    <span>Polylines</span>
                                </h4>
                                <a href="#" class="minimize" style="display: none;">Minimize</a>
                            </div>
                            <div class="panel-body">
                                <a href="<?= $base_url; ?>managepolylines/<?= $development->development_id;?>">
                                    <button type="button" class="btn btn-primary">View Polylines</button>
                                </a>
                             
                            </div>
                        </div><!-- End .panel -->
                    </div><!-- End .span3 -->

					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-documents"></span>
									<span>Stages</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>viewstages/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">View Stages</button>
								</a>

								<a href="<?= $base_url; ?>addstages/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Add Stages</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-document-2"></span>
									<span>Lots</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>viewlots/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">View Lots</button>
								</a>

								<a href="<?= $base_url; ?>addlots/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Add Lots</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

				</div><!-- End .row -->

				<div class="row">

					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Houses</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>viewhouses/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">View Houses</button>
								</a>

								<a href="<?= $base_url; ?>addhouses/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Add Houses</button>
								</a>

								<a href="<?= $base_url; ?>managehousematches/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-success">Manage</button>
								</a>

								<a href="<?= $base_url; ?>listpackages/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-primary">View Packages</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-location-3"></span>
									<span>Development Amenities</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>viewamenities/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">View Amenities</button>
								</a>

								<a href="<?= $base_url; ?>addamenities/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Add Amenities</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-location-2"></span>
									<span>External Amenities</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>viewexternalamenities/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">View</button>
								</a>

								<a href="<?= $base_url; ?>addexternalamenities/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Add</button>
								</a>

								<a href="<?= $base_url; ?>manageexternalamenitytypes/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-success">Types</button>
								</a>

								<a href="<?= $base_url; ?>importexternalamenities/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-primary">Export / Import CSV</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

				</div><!-- End .row -->

				<div class="row">
					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-alarm"></span>
									<span>Schedule &nbsp;&amp;&nbsp; Notifications</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>schedule/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">Schedule</button>
								</a>

								<a href="<?= $base_url; ?>reminders/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Reminders</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-settings"></span>
									<span>Documents Portal</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>developmentdocuments/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">Manage</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-stats"></span>
									<span>Statistics Reports</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $reports_url; ?>statisticsreport/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">MAPOVIS Statistics</button>
								</a>

								<a href="<?= $reports_url; ?>lotviewsstatisticsreport/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Lot Impressions</button>
								</a>

								<a href="<?= $reports_url; ?>availlotstatisticsreport/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-success">Available Lots</button>
								</a>

								<a href="<?= $reports_url.'soldlotstatisticsreport/'.$development->development_id;?>" style="padding-left: 20px;">
									<button id="opener1" class="btn btn-primary">Sales Rate</button>
								</a>

								<a href="<?= $reports_url; ?>statisticwordreport/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">MAPOVIS Statistics WORD</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

				</div><!-- End .row -->

				<div class="row">
					<div class="col-lg-5">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-alarm"></span>
									<span>Price Lists</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>managepricelistsubscribers/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">Subscribers</button>
								</a>

								<a href="<?= $base_url; ?>pricelistschedule/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Schedule</button>
								</a>

								<a href="<?= $base_url; ?>pricelistpdf/<?= $development->development_id;?>" style="padding-left: 20px;" target="_blank">
									<button type="button" class="btn btn-success">Price List PDF</button>
								</a>

								<a href="<?= $base_url; ?>pricelistindivemail/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-primary">Send Individual Email</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->

					<div class="col-lg-7">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-settings"></span>
									<span>Other Development Actions</span>
								</h4>
								<a href="#" class="minimize" style="display: none;">Minimize</a>
							</div>
							<div class="panel-body">
								<a href="<?= $base_url; ?>managedevelopmentlinks/<?= $development->development_id;?>">
									<button type="button" class="btn btn-primary">Linked Developments</button>
								</a>

								<?php if(count($possible_links)):?>
								<a href="<?= $base_url; ?>adddevelopmentlink/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-info">Add Developments Link</button>
								</a>
								<?php endif;?>

								<a href="<?= $base_url; ?>duplicatedevelopment/<?= $development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-success">Duplicate</button>
								</a>

								<a href="<?= $base_url.'deletestatisticsdata/'.$development->development_id;?>" style="padding-left: 20px;">
									<button id="opener1" class="btn btn-danger" onclick="if(!confirm('Are you sure you want to delete the statistics data?')){return false;}">Delete Statistics</button>
								</a>

								<a href="<?= $base_url.'deletedevelopment/'.$development->development_id;?>" style="padding-left: 20px;">
									<button type="button" class="btn btn-danger" onclick="if(!confirm('Are you sure you want to delete the Development?')){return false;}">Delete Development</button>
								</a>
							</div>
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
				</div><!-- End .row -->
                
                <div class="row">
                    <div class="col-lg-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span class="icon16 entypo-icon-grid"></span>
                                    <span>Reports</span>
                                </h4>
                                <a href="#" class="minimize" style="display: none;">Minimize</a>
                            </div>
                            <div class="panel-body">
                                <a href="<?= base_url(); ?>villawood_stock_analysis/createReport/<?= $development->development_id;?>">
                                    <button type="button" class="btn btn-primary">Stock Analysis Report</button>
                                </a>
                             
                            </div>
                        </div><!-- End .panel -->
                    </div><!-- End .span3 -->
                </div>

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
