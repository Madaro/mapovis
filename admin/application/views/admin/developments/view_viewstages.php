<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-documents"></span>
									<span><?= $development->development_name;?> Stages</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($stages)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Stage ID</th>
											<th>Precinct</th>
											<th>Stage Number</th>
											<th>Stage Code</th>
											<th>Stage Name</th>
											<th>Title Release</th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
										<tr>
										  <td><input type="text" name="search_stage_id" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_precinct_number" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_stage_number" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_stage_code" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_stage_name" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_stage_title_release" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td></td>
										  <td></td>
										  <td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($stages as $stage):?>
										<tr>
											<td class="center"><?= $stage->stage_id;?></td>
											<td class="center"><?= $stage->precinct_number;?></td>
											<td class="center"><?= $stage->stage_number;?></td>
											<td class="center"><?= $stage->stage_code;?></td>
											<td class="center"><?= $stage->stage_name;?></td>
											<td class="center"><?= $stage->title_release;?></td>
											<td class="center">
												<button type="button" stage_id="<?= $stage->stage_id;?>" class="manage_title_btn btn btn-xs btn-default">Manage Titles</button>
											</td>
											<td class="center">
												<a href="<?= $base_url.'updatestage/'.$stage->stage_id;?>"><button type="button" id="opener1" class="btn btn-xs btn-default">Update</button></a>
											</td>
											<td class="center" style="">
												<a href="<?= $base_url.'deletestage/'.$stage->stage_id;?>"><button type="button" class="btn btn-xs btn-danger" onclick="if(!confirm('Are you sure you want to delete the Stage with all its lots?')){return false;}">Delete</button></a>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not stages in the system.</div>
										<a href="<?= $base_url; ?>addstages/<?= $development->development_id;?>" style="padding-left: 20px;">
											<button type="button" class="btn btn-info">Add Stages</button>
										</a>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

	</div><!-- End #wrapper -->
<div id="dialog_stage"></div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
	$('.manage_title_btn').on('click', function(){
		var stage_id = $(this).attr('stage_id');
		showLoading();
		$.post('<?= $base_url.'managestagetitles/';?>/'+stage_id,
			function(data,status){
				if(status === 'success'){
					$('#dialog_stage').html(data);
					$('#dialog_stage').dialog({
						title: "Manage Titles",
						height: 150,
						width: 480,
						modal: true,
						resizable: false,
						dialogClass: 'loading-dialog',
					});
					$('#dialog_stage').dialog('open');
					hideLoading();
					$('.btn_change').click(function() {
						showLoading();
					});
					return true;
				}
				messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
		});
	});
	$('#dialog_stage').dialog({autoOpen: false});
});
</script>