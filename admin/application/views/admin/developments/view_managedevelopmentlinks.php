<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-document-2"></span>
									<span>Linked Developments</span>
								</h4>
							</div>
							<?= $alert_message;?>
							<div class="panel-body noPad clearfix">
								<?php if(empty($linked_developments)):?>
									<div class="panel-body">
										<div class="alert alert-warning">No linked developments were found.</div>
										<a href="<?= $base_url; ?>adddevelopmentlink/<?= $development->development_id;?>" style="padding-left: 20px;">
											<button type="button" class="btn btn-info">Link Developments</button>
										</a>
									</div>
								<?php else:?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Linked Development</th>
											<th>Distance</th>
											<th>Suburb</th>
											<th></th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_development" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_distance" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_suburb" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
									<?php foreach($linked_developments as $linked_development):?>
										<tr>
											<td><?= $linked_development->development_name;?></td>
											<td><?= round($linked_development->distance,5);?> km</td>
											<td><?= $linked_development->suburb;?></td>
											<td>
												<button title="Link to Development <?= $linked_development->development_name;?>" development_id1="<?= $development->development_id;?>" development_id2="<?= $linked_development->development_id;?>" class="update_link_btn btn btn-xs btn-default">Update Link</button>
											</td>
											<td class="center" style="">
												<a href="<?= $base_url.'deletedevelopmentlink/'.$development->development_id.'/'.$linked_development->development_id;?>"><button type="button" id="opener1" class="btn btn-xs btn-danger" onclick="if(!confirm('Are you sure you want to delete the link?')){return false;}">Delete</button></a>
											</td>
										</tr>
									<?php endforeach;?>
									</tbody>
								</table>
								<?php endif;?>

							</div>

						</div><!-- End .panel -->
					</div><!-- End .span12 -->
				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<!-- Dialog -->
<div id="dialog_link"></div>
