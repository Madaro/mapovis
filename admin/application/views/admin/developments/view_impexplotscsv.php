<?php
$base_url           = base_url().'admin/';
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3>Import & Export Current Lots</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-file-excel"></span>
									<span>Export Current Lots (CSV Format)</span>
								</h4>
							</div>

							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>developments/exportlotscsv" role="form" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-lg-3 control-label">Development:</label>
									<div class="col-lg-5">
										<select name="development_id" class="form-control">
											<?php foreach($developments as $development):?>
												<option value="<?= $development->development_id;?>"><?= $development->development_name;?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div><!-- End .form-group  -->

								<div class="col-lg-offset-2">
									<br>
									<button type="submit" class="btn btn-info">Export</button>
									<br><br>
								</div>

							</form>
							</div><!-- End .panel-body  -->
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-file-excel"></span>
									<span>Import & Edit Current Lots</span>
								</h4>
							</div>

							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>developments/importeditlotscsvfile" role="form" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-lg-3 control-label">Filename:</label>
									<div class="col-lg-3">
										<input type="file" name="csvfilename" size="20" accept=".csv" />
									</div>
								</div><!-- End .form-group  -->

								<div class="col-lg-offset-2">
									<br>
									<button type="submit" class="btn btn-info upload_btn">Upload</button>
									<br><br>
								</div>
							</form>
							</div><!-- End .panel-body  -->
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
				</div><!-- End .row -->
			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
	$('.upload_btn').on('click', function(){
		showLoading();
	});
</script>
