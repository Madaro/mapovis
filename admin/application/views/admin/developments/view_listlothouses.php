<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-document-2"></span>
									<span><?= $development->development_name;?> Lots</span>
								</h4>
							</div>
							<?= $alert_message;?>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Lot Number</th>
											<th>House Name</th>
											<th>Builder</th>
											<th>Price</th>
											<th>Featured</th>
											<th>PDF</th>
											<th></th>
											<th></th>
										</tr>
										<!-- START - Modification by Seb : Adding Column Filtering for DataTables -->
										<tr>
											<td><input type="text" name="search_lot" placeholder="Search Lot Number" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_house" placeholder="Search House Name" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_builder" placeholder="Search Builder" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_price" placeholder="Search Price" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_featured" placeholder="Search Featured" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
											<td></td>
									</tr>
										<!-- END - Modification by Seb -->
									</thead>
									<tbody>
									<?php foreach($lot_house_packages as $lot_house_package):?>
										<tr>
											<td><?= $lot_house_package->lot_number;?></td>
											<td><?= $lot_house_package->house_name;?></td>
											<td><?= $lot_house_package->builder_name;?></td>
											<td>$<?= number_format($lot_house_package->house_lot_price);?></td>
											<td><?= ($lot_house_package->is_feature)? 'Yes' : 'No';?></td>
											<td>
												<?php if(!empty($lot_house_package->file_name)):?>
													<a title="Download File" href="<?= base_url().'../mpvs/templates/lot_house_pdfs/'.$lot_house_package->file_name;?>" 
													   target="_blank">
														<span class="icon16 icomoon-icon-file-pdf"></span>
													</a>
												<?php endif?>
											</td>
											<td class="center">
												<button house_facade_width="<?= ($lot_house_package->house_development_id)? $lot_house_package->custom_house_facade_width:244;?>" house_facade_height="<?= ($lot_house_package->house_development_id)?$lot_house_package->custom_house_facade_height:133;?>" lot_id="<?= $lot_house_package->lot_id;?>" house_id="<?= $lot_house_package->house_id;?>" type="button" class="edit_package btn btn-xs btn-default">Edit</button>
											</td>
											<td class="center">
												<a href="<?php echo "{$base_url}disablehouselotpackage/{$lot_house_package->lot_id}/{$lot_house_package->house_id}/0"; ?>" onclick="if(!confirm('Are you sure you want to disable id?')){return false;}">
													<button type="button" class="btn btn-xs btn-danger">Disable</button>
												</a>
											</td>
										</tr>
									<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->
					</div><!-- End .span12 -->
				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<div id="dialog_lot_house"></div>
<style>
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 500px; }
	.sortable li { margin: 10px 10px 10px 0; padding: 0px; float: left; width: 150px; height: 115px;  text-align: center; display:block; overflow:hidden;}
	.sortable_view li {height: 81px;}
	.deleteimage{margin-top: 7px;}
	.img_container{overflow-y:hidden;}
	.ui-dialog { z-index: 65535; }
</style>
<script type="text/javascript" src="http://feather.aviary.com/js/feather.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript">
var featherEditor = new Aviary.Feather({
	apiKey: '84495f70872a7572',
	apiVersion: 3,
	tools: 'all',
	theme: 'light',
	onError: function(errorObj) {
		console.log(errorObj.message);
	}
});
$('.edit_package').click(function() {
	var house_id = $(this).attr('house_id');
	var lot_id = $(this).attr('lot_id');
	var img_width = $(this).attr('house_facade_width');
	var img_height = $(this).attr('house_facade_height');
	update_lot_house(lot_id, house_id, img_width, img_height)
});
function update_lot_house(lot_id, house_id, img_width, img_height)	{
	showLoading();
	$.post('<?= $base_url;?>edithouselotpackage/'+lot_id+'/'+house_id+'/0/',
		function(data,status){
			if(status === 'success'){
				$('#dialog_lot_house').html(data);
				$('#dialog_lot_house').dialog({
					title: 'Update Lot and House Package',
					height: 670,
					width: 900,
					modal: true,
					resizable: true,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){remove_tinymce();$('#dialog_lot_house').html('');}
				});
				$('#dialog_lot_house').dialog('open');
				init_tinymce();
				$("input, textarea, select").not('.nostyle').uniform();

				$('#edit_house_lot_package').submit(function(e){
					showLoading();
				})
				$('.deleteimage').on('click', function(e){
					deleteImageFunction($(this), true);
				});

				$('.image_files').on('change', function(){
					var input = $(this)[0];
					if (input.files && input.files[0]){
						if(!validateimagefiletype(input.files[0].type)){
							return false;
						}
					}
				});
				$('.add_image_btn').on('click', function(e){
					var area            = $(this).attr('area');
					var house_id        = $(this).attr('house_id');
					var image_field_obj = $('#image_file'+area);
					var input           = image_field_obj[0];
					if (input.files && input.files[0]){
						if(!validateimagefiletype(input.files[0].type)){
							// show dialog message
							return false;
						}
						var reader = new FileReader();
						// set where you want to attach the preview
						reader.target_elem = $(input).parent().find('preview');
						reader.onload = function (e) {
							var force_crop = true;
							var img = document.createElement("img");
							img.onload = function(){
								if(img.width == img_width && img.height == img_height){
									force_crop = false;
								}
								$('#imageupload'+area).attr('src', e.target.result);
								launchEditor('imageupload'+area, e.target.result, input.files[0].type, house_id, force_crop,area, img_width, img_height);
							}
							img.src = e.target.result;
						};
						reader.readAsDataURL(input.files[0]);
					}
				});

				$('#cancel_lot_house_changes').click(function() {
					$('#dialog_lot_house').dialog('close');
				});
				$('#sortable_facade').sortable({containment: "#img_container_facade",tolerance: "pointer"});
				$('#sortable_facade').disableSelection();
				hideLoading();
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
}

function launchEditor(id, src, filetype, house_id, force_crop, area, img_width, img_height){
	var editor_params = {
		image: id,
		url: src,
		fileFormat: filetype,
		onSave: function(imageID, newURL) {
			addNewImageHtml(house_id, newURL, newURL, area);
			var img = document.getElementById(imageID);
			img.src = newURL;
			featherEditor.close();
		}
	};
	if(force_crop){
		editor_params.forceCropPreset = ['Width: '+img_width+'px, Height: '+img_height+'px',img_width+'x'+img_height];
		editor_params.initTool = 'crop';
		editor_params.cropPresetsStrict = 'true';
	}
	else{
		editor_params.tools = 'enhance,effects,frames,stickers,focus,brightness,contrast,saturation,warmth,sharpness,colorsplash,draw,text,redeye,whiten,blemish';
	}
	featherEditor.launch(editor_params);
}

var position = 100000;
function addNewImageHtml(house_id, newURL, imagecontent, area){
	position++;
	if(area == '_facade'){
		var image_field_obj = $('#image_file'+area);
		var input           = image_field_obj[0];
		var form_data = new FormData();
		form_data.append('image_file_facade', input.files[0]);
		$.ajax( {
			url: '<?= base_url(); ?>admin/developments/uploadoriginalimage/',
			type: 'POST',
			data: form_data,
			processData: false,
			contentType: false
		}).done(function(data){
			var result = jQuery.parseJSON(data);
			if(result != false){
				var original_image = '<input type="hidden" name="original_image['+position+']" value="'+result+'">';
				$('#originalFiles').append(original_image)
			}
		});
	}

	var button_id = 'delete_img' + new Date().getTime();
	var new_image_html = '<li class="ui-state-default"><input name="facade_images[]" type="text" value="'+position+'||'+newURL+'" style="display:none"><div><img src="'+imagecontent+'" height="81" /></div>';
	new_image_html += '<button type="button" id="'+button_id+'" house_id="'+house_id+'" image_id="'+newURL+'" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button></li>';
	$('#sortable'+area).append(new_image_html);
	$('#'+button_id).on('click', function(e){
		deleteImageFunction($(this));
	});
}

function deleteImageFunction(object, updatedb){
	var imageid = object.attr('image_id');
	var image_obj = object.parent();
	$("<div><span>Are you sure you want to delete this image permanently?</span></div>").dialog({
		resizable: false,
		height:140,
		modal: true,
		buttons: {
			"Delete Image": function() {
				if(updatedb){
					showLoading();
					$.post("<?= base_url().'admin/developments/deletehouseimage'?>",{
							house_image_id: imageid
						},
						function(data,status){
							hideLoading();
							var alert_msg = '';
							if(status === 'success'){
								var result = jQuery.parseJSON(data);
								if(result.status == 1){
									alert_msg = '<div title="Success">'+result.msg+'</div>';
									image_obj.remove();
								}
								else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
							}
							else{ alert_msg = '<div title="Fail">'+data+'</div>';}
							messageAlert(alert_msg);
					});
				}
				else{
					image_obj.remove();
					messageAlert('<div title="Success">The image was deleted successfully.</div>');
				}
				$(this).dialog("close");
			},
			Cancel: function() {
				$('#save_changes').focus();
				$(this).dialog( "close" );
			}
		}
	});
}

</script>