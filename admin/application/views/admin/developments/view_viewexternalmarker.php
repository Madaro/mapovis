<?php
$base_url = base_url().'admin/developments/';
?>
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<div class="col-lg-12">
			<div class="panel-body">
				<form method="post" class="form-horizontal" action="<?= $base_url; ?>updateexternalmarker/<?= $external_marker->id;?>" role="form">
					<div class="form-group">
                        <label class="col-lg-4 control-label" for="required">Name:</label>
                        <div class="col-lg-7">
                            <input type="text" name="name" class="form-control" value="<?= $external_marker->name;?>" >
                        </div>
                    </div><!-- End .form-group  -->
                    
                    <div class="form-group">
						<label class="col-lg-4 control-label" for="required">Icon:</label>
						<div class="col-lg-7">
							<input type="text" name="icon_url" class="form-control" value="<?= $external_marker->icon_url;?>" >
						</div>
					</div><!-- End .form-group  --> 
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="required">Icon Width:</label>
                        <div class="col-lg-7">
                            <input type="text" name="icon_width" class="form-control" value="<?= $external_marker->icon_width;?>" >
                        </div>
                    </div><!-- End .form-group  --> 
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="required">Icon Height:</label>
                        <div class="col-lg-7">
                            <input type="text" name="icon_height" class="form-control" value="<?= $external_marker->icon_height;?>" >
                        </div>
                    </div><!-- End .form-group  --> 
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="required">Iframe URL:</label>
                        <div class="col-lg-7">
                            <input type="text" name="iframe_url" class="form-control" value="<?= $external_marker->iframe_url;?>" >
                        </div>
                    </div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Latitude:</label>
						<div class="col-lg-7">
							<input type="text" name="latitude" class="form-control" value="<?= (float)$external_marker->latitude;?>" >
						</div>
					</div><!-- End .form-group  -->
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="required">Longitude:</label>
                        <div class="col-lg-7">
                            <input type="text" name="longitude" class="form-control" value="<?= (float)$external_marker->longitude;?>" >
                        </div>
                    </div><!-- End .form-group  --> 
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="required">Show Zoom Level:</label>
                        <div class="col-lg-7">
                            <input type="text" name="show_at_zoom_level" class="form-control" value="<?= $external_marker->show_at_zoom_level;?>" >
                        </div>
                    </div><!-- End .form-group  --> 
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="required">Hide Zoom Level:</label>
                        <div class="col-lg-7">
                            <input type="text" name="hide_at_zoom_level" class="form-control" value="<?= $external_marker->hide_at_zoom_level;?>" >
                        </div>
                    </div><!-- End .form-group  --> 

					<div class="form-group" style="padding-top:10px">
						<div class="col-lg-offset-3 col-lg-9">
							<button type="submit" class="save_changes btn btn-info">Save Changes</button>
							<button type="button" class="cancel_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
						</div>
					</div><!-- End .form-group  -->
				</form>
			</div>
	</div><!-- End .span6 -->
