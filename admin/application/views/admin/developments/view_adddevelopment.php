<?php
$base_url           = base_url().'admin/developments/';
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3>Developments</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $validation_msg;?>
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4><span>Add Development</span></h4>
							</div>
							<div class="panel-body noPad clearfix">

								<form method="post" id="wizard_development" class="form-horizontal" action="<?= $base_url; ?>adddevelopment" enctype="multipart/form-data">
									<div class="msg"></div>
									<div class="wizard-steps clearfix"></div>

									<div class="step" id="first">
										<span class="step-info" data-num="1" data-text="General"></span>

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Name of the Development:</label>
											<div class="col-lg-4">
												<input value="<?= set_value('development_name');?>" type="text" class="form-control" id="nameofthedevelopment" name="development_name">
											</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">URI for the Development:</label>
											<div class="col-lg-4">
												<input name="development_uri" value="<?= set_value('development_uri');?>" type="text" class="form-control" id="uriforthedevelopment" >
											</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Developer:</label>
											<div class="col-lg-4">
												<select name="developerid" class="form-control">
													<option></option>
													<?php foreach($developers as $developer):?>
														<option value="<?= $developer->developer_id;?>" <?= ($developer->developer_id == set_value('developerid'))? 'selected="selected"': '';?>><?= $developer->developer;?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">State:</label>
											<div class="col-lg-4">
												<select name="state" class="form-control">
													<option></option>
													<?php foreach($states as $state):?>
														<option value="<?= $state;?>" <?= ($state == set_value('state'))? 'selected="selected"' : '';?>><?= $state;?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
												<label class="col-lg-3 control-label" for="required">Overlay File Name:</label>
												<div class="col-lg-4">
													<input name="overlay_filename" value="<?= set_value('overlay_filename');?>" type="text" class="form-control" id="overlayfilename" placeholder="e.g. developmentname-stage1.png">
												</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Overlay South West Coordinates:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input name="southwest_latitude" value="<?= set_value('southwest_latitude');?>" type="text" class="form-control" style="width: 200px !important;" id="southwestcoordinates-latitude" placeholder="latitude">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													   <div class="input-group">
															<input name="southwest_longitude" value="<?= set_value('southwest_longitude');?>" type="text" class="form-control" style="width: 200px !important;" id="southwestcoordinates-longitude" placeholder="longitude">
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Overlay North East Coordinates:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input name="northeast_latitude" value="<?= set_value('northeast_latitude');?>" type="text" class="form-control" style="width: 200px !important;" id="northeastcoordinates-latitude" placeholder="latitude">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													   <div class="input-group">
															<input name="northeast_longitude" value="<?= set_value('northeast_longitude');?>" type="text" class="form-control" style="width: 200px !important;" id="northeastcoordinates-longitude" placeholder="longitude">
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Centre of Development Coordinates:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input name="centre_latitude" value="<?= set_value('centre_latitude');?>" type="text" class="form-control" style="width: 200px !important;" id="centrecoordinates-latitude" placeholder="latitude">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													   <div class="input-group">
															<input name="centre_longitude" value="<?= set_value('centre_longitude');?>" type="text" class="form-control" style="width: 200px !important;" id="centrecoordinates-longitude" placeholder="longitude">
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Main Entry/Exit Road Coordinates:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input name="entryexit_latitude" value="<?= set_value('entryexit_latitude');?>" type="text" class="form-control" style="width: 200px !important;" id="entryexitroadcoordinates-latitude" placeholder="latitude">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													   <div class="input-group">
															<input name="entryexit_longitude" value="<?= set_value('entryexit_longitude');?>" type="text" class="form-control" style="width: 200px !important;" id="entryexitroadcoordinates-longitude" placeholder="longitude">
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Zoom to Community Feature:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input name="zoom_to_community_feature_latitude" type="text" class="form-control" style="width: 200px !important;" 
															value="<?= set_value('zoom_to_community_feature_latitude');?>" placeholder="latitude">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<input name="zoom_to_community_feature_longitude" type="text" class="form-control" style="width: 200px !important;" 
															value="<?= set_value('zoom_to_community_feature_longitude');?>" placeholder="longitude">
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">VIC Public Transport Origin:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="vic_public_transport_origin" name="vic_public_transport_origin" value="<?= set_value('vic_public_transport_origin');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Stage Plan PDF:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="stageplanpdf" name="stageplanpdf" value="<?= set_value('stageplanpdf');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">MAPOVIS Generated PDF:</label>
											<div class="col-lg-5">
												<input type="radio" name="generated_pdf" value="1" <?= (set_value('generated_pdf', 1) == 1? 'checked="checked"': '');?> id="generated_pdf1" class="generated_pdf"><label for="generated_pdf1"> On </label>  &nbsp;&nbsp;
												<input type="radio" name="generated_pdf" value="0" <?= (set_value('generated_pdf', 1) == 0? 'checked="checked"': '');?> id="generated_pdf0" class="generated_pdf"><label for="generated_pdf0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div id="lots_pdf_file_div" class="form-group">
											<label class="col-lg-3 control-label">PDF File:</label>
											<div class="col-lg-9">
												<div id="lots_pdf_file">
													<input type="file" name="lots_pdf_file" id="lots_pdf_file_field" value="" accept="application/pdf" >
												</div>
											</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Google Analytics Tracking ID:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="ga_tracking_id" name="ga_tracking_id" value="<?= set_value('ga_tracking_id');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Start HEX Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="start_hex_colour" name="start_hex_colour" value="<?= set_value('start_hex_colour');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="start_hex_colour_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">End HEX Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="end_hex_colour" name="end_hex_colour" value="<?= set_value('end_hex_colour');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="end_hex_colour_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Text HEX Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="text_hex_colour" name="text_hex_colour" value="<?= set_value('text_hex_colour');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="text_hex_colour_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Lightbox Border HEX Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="lightbox_border_colour" name="lightbox_border_colour" value="<?= set_value('lightbox_border_colour');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="lightbox_border_colour_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Lightbox Highlight HEX Colour:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="lightbox_highlight_colour" name="lightbox_highlight_colour" value="<?= set_value('lightbox_highlight_colour');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="lightbox_highlight_colour_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Zoom to Stage - Buttons:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="zoom_to_stage_button_hex" name="zoom_to_stage_button_hex" value="<?= set_value('zoom_to_stage_button_hex');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="zoom_to_stage_button_hex_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Zoom to Stage - Header:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" id="zoom_to_stage_header_hex" name="zoom_to_stage_header_hex" value="<?= set_value('zoom_to_stage_header_hex');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="zoom_to_stage_header_hex_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Loading Gradient Top:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" name="loading_gradient_top_hex" value="<?= set_value('loading_gradient_top_hex');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="loading_gradient_top_hex_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Loading Gradient Middle:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" name="loading_gradient_middle_hex" value="<?= set_value('loading_gradient_middle_hex');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="loading_gradient_middle_hex_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Loading Gradient Bottom:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" name="loading_gradient_bottom_hex" value="<?= set_value('loading_gradient_bottom_hex');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
													 <div class="input-group">
															<div id="loading_gradient_bottom_hex_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Dialog Start Hex:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" name="dialog_start_hex" value="<?= set_value('dialog_start_hex');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="dialog_start_hex_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Dialog End Hex:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input type="text" class="form-control change_colour" name="dialog_end_hex" value="<?= set_value('dialog_end_hex');?>" placeholder="#FFFFFF">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<div id="dialog_end_hex_disp" class="colour_disp">&nbsp;&nbsp;&nbsp;</div>
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group--> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">White List Domain:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="dev_domain" name="dev_domain" value="<?= set_value('dev_domain');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">PDF Link:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="pdf_link" name="pdf_link" value="<?= set_value('pdf_link');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Office Title:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="sales_office_title" name="sales_office_title" value="<?= set_value('sales_office_title');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Office Address:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="sales_office_address" name="sales_office_address" value="<?= set_value('sales_office_address');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Telephone Number:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="sales_telephone_number" name="sales_telephone_number" value="<?= set_value('sales_telephone_number');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Office Googlemaps Directions Url:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="sales_office_googlemaps_directions_url" name="sales_office_googlemaps_directions_url" value="<?= set_value('sales_office_googlemaps_directions_url');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="buttons">Sales Office Coordinates:</label>
											<div class="col-lg-8">
												<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<input name="sales_office_latitude" type="text" class="form-control" style="width: 200px !important;" id="sales_office_latitude"
															value="<?= set_value('sales_office_latitude');?>" placeholder="latitude">
														</div><!-- /input-group -->
													</div>
													<div class="col-lg-3">
														<div class="input-group">
														   <input name="sales_office_longitude" type="text" class="form-control" style="width: 200px !important;" id="sales_office_longitude"
														   value="<?= set_value('sales_office_longitude');?>" placeholder="longitude">
														</div><!-- /input-group -->
													</div>
												</div>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Office HTML:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="sales_office_html" name="sales_office_html"><?= set_value('sales_office_html');?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
												<label class="col-lg-3 control-label" for="required">Access Key:</label>
												<div class="col-lg-4">
													<input name="access_key" value="<?= set_value('access_key');?>" type="text" class="form-control" id="access_key">
												</div>
										</div><!-- End .form-group  -->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Terms and Conditions:</label>
											<div class="col-lg-8">
												<textarea style="height: 200px;" class="form-control" id="terms_and_conditions" name="terms_and_conditions"><?= set_value('terms_and_conditions');?></textarea>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Form Iframe URL:</label>
											<div class="col-lg-5">
												<input type="text" class="form-control" name="form_iframe_url" value="<?= set_value('form_iframe_url');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Form Iframe URL Height:</label>
											<div class="col-lg-2">
												<input type="text" class="form-control" name="form_iframe_url_height" value="<?= set_value('form_iframe_url_height');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Land Form Iframe URL:</label>
											<div class="col-lg-5">
												<input type="text" class="form-control" name="land_form_iframe_url" value="<?= set_value('land_form_iframe_url');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Land Form Iframe URL Height:</label>
											<div class="col-lg-2">
												<input type="text" class="form-control" name="land_form_iframe_url_height" value="<?= set_value('land_form_iframe_url_height');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Land Starting From:</label>
											<div class="col-lg-2">
												<input type="text" class="form-control" name="land_starting_from" value="<?= set_value('land_starting_from');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">House & Land Form Iframe URL:</label>
											<div class="col-lg-5">
												<input type="text" class="form-control" name="house_and_land_form_iframe_url" value="<?= set_value('house_and_land_form_iframe_url');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">House & Land Form Iframe URL Height:</label>
											<div class="col-lg-2">
												<input type="text" class="form-control" name="house_and_land_form_iframe_url_height" value="<?= set_value('house_and_land_form_iframe_url_height');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Get Directions Example Address:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="get_directions_to_example_address" value="<?= set_value('get_directions_to_example_address');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">URL Avatar Icon:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="development_url_icon" value="<?= set_value('development_url_icon');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Small Available Icon:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="small_available_icon" value="<?= set_value('small_available_icon');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Large Available Icon:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="large_available_icon" value="<?= set_value('large_available_icon');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Small Sold Icon:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="small_sold_icon" value="<?= set_value('small_sold_icon');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Large Sold Icon:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="large_sold_icon" value="<?= set_value('large_sold_icon');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Search by Lot Price:</label>
											<div class="col-lg-5">
												<input type="radio" name="search_by_lot_price" value="1" <?= (set_value('search_by_lot_price', 1) == 1? 'checked="checked"': '');?> id="search_by_price1"><label for="search_by_price1"> On </label>  &nbsp;&nbsp;
												<input type="radio" name="search_by_lot_price" value="0" <?= (set_value('search_by_lot_price', 1) == 0? 'checked="checked"': '');?> id="search_by_price0"><label for="search_by_price0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Search by Lot Width:</label>
											<div class="col-lg-5">
												<input type="radio" name="search_by_lot_width" value="1" <?= (set_value('search_by_lot_width', 1) == 1? 'checked="checked"': '');?> id="search_by_width1"><label for="search_by_width1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="search_by_lot_width" value="0" <?= (set_value('search_by_lot_width', 1) == 0? 'checked="checked"': '');?> id="search_by_width0"><label for="search_by_width0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Search by Lot Size:</label>
											<div class="col-lg-5">
												<input type="radio" name="search_by_lot_size" value="1" <?= (set_value('search_by_lot_size', 1) == 1? 'checked="checked"': '');?> id="search_by_size1"><label for="search_by_size1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="search_by_lot_size" value="0" <?= (set_value('search_by_lot_size', 1) == 0? 'checked="checked"': '');?> id="search_by_size0"><label for="search_by_size0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Show Zoom to Stage Panel:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_zoom_to_stage" value="1" <?= (set_value('show_zoom_to_stage', 1) == 1? 'checked="checked"': '');?> id="show_zoom_to_stage1"><label for="show_zoom_to_stage1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_zoom_to_stage" value="0" <?= (set_value('show_zoom_to_stage', 1) == 0? 'checked="checked"': '');?> id="show_zoom_to_stage0"><label for="show_zoom_to_stage0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Show Prices:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_pricing" value="1" <?= (set_value('show_pricing', 1) == 1? 'checked="checked"': '');?> id="show_pricing1"><label for="show_pricing1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_pricing" value="0" <?= (set_value('show_pricing', 1) == 0? 'checked="checked"': '');?> id="show_pricing0"><label for="show_pricing0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Show Telephone:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_telephone" value="1" <?= (set_value('show_telephone',1) == 1? 'checked="checked"': '');?> id="show_telephone1"><label for="show_telephone1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_telephone" value="0" <?= (set_value('show_telephone',1) == 0? 'checked="checked"': '');?> id="show_telephone0"><label for="show_telephone0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Show Stage Icons:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_stage_icons" value="1" <?= (set_value('show_stage_icons',1) == 1? 'checked="checked"': '');?> id="show_stage_icons1"><label for="show_stage_icons1"> On </label> &nbsp;&nbsp;
												<input type="radio" name="show_stage_icons" value="0" <?= (set_value('show_stage_icons',1) == 0? 'checked="checked"': '');?> id="show_stage_icons0"><label for="show_stage_icons0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Show External Amenities:</label>
											<div class="col-lg-5">
												<input type="radio" name="show_external_amenities" value="1" <?= (set_value('show_external_amenities', 1) == 1? 'checked="checked"': '');?> id="show_external_amenities1"><label for="show_external_amenities1"> On </label>  &nbsp;&nbsp;
												<input type="radio" name="show_external_amenities" value="0" <?= (set_value('show_external_amenities', 1) == 0? 'checked="checked"': '');?> id="show_external_amenities0"><label for="show_external_amenities0"> Off </label>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Custom Google Maps Style:</label>
											<div class="col-lg-8">
												<textarea style="height: 150px;" class="form-control" name="custom_googlemaps_style"><?= str_replace('</textarea>', '&lt;/textarea&gt;', set_value('custom_googlemaps_style'));?></textarea>
											</div>
										</div><!-- End .form-group  --> 

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">House & Land View:</label>
											<div class="col-lg-4">
												<select name="house_and_land_view" id="house_and_land_view" class="form-control">
													<option value="">-- Default View --</option>
													<?php foreach($house_and_land_views as $view_name):?>
													<option value="<?= $view_name;?>" <?= ($view_name == set_value('house_and_land_view'))? 'selected="selected"': '';?>><?= $view_name;?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group" id="house_land_order_by_field" <?= (set_value('house_and_land_view') == '')? 'style="display:none"': '';?>>
											<label class="col-lg-3 control-label">House & Land Order By:</label>
											<div class="col-lg-4">
												<select name="house_land_order_by" class="form-control">
													<?php foreach($house_land_order_bys as $house_land_order_by):?>
													<option value="<?= $house_land_order_by;?>" <?= ($house_land_order_by == set_value('house_land_order_by', 'street_name'))? 'selected="selected"': '';?>><?= ucwords(str_replace('_', ' ', $house_land_order_by));?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Land View:</label>
											<div class="col-lg-4">
												<select name="land_view" id="land_view" class="form-control">
													<option value="">-- Default View --</option>
													<?php foreach($land_views as $view_name):?>
													<option value="<?= $view_name;?>" <?= ($view_name == set_value('land_view'))? 'selected="selected"': '';?>><?= $view_name;?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group" id="land_order_by_field" <?= (set_value('land_view') == '')? 'style="display:none"': '';?>>
											<label class="col-lg-3 control-label" for="required">Land Order By:</label>
											<div class="col-lg-4">
												<select name="land_order_by" class="form-control">
													<?php foreach($house_land_order_bys as $land_order_by):?>
													<option value="<?= $land_order_by;?>" <?= ($land_order_by == set_value('land_order_by', 'street_name'))? 'selected="selected"': '';?>><?= ucwords(str_replace('_', ' ', $land_order_by));?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Land PDF View:</label>
											<div class="col-lg-4">
												<select name="land_pdf_view" id="land_pdf_view" class="form-control">
													<option value="">-- Default View --</option>
													<?php foreach($land_pdf_views as $view_name):?>
													<option value="<?= $view_name;?>" <?= ($view_name == set_value('land_pdf_view'))? 'selected="selected"': '';?>><?= $view_name;?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group" id="land_pdf_order_by_field" <?= (set_value('land_pdf_view') == '')? 'style="display:none"': '';?>>
											<label class="col-lg-3 control-label" for="required">Land PDF Order By:</label>
											<div class="col-lg-4">
												<select name="land_pdf_order_by" class="form-control">
													<?php foreach($house_land_order_bys as $land_order_by):?>
													<option value="<?= $land_order_by;?>" <?= ($land_order_by == set_value('land_pdf_order_by', 'street_name'))? 'selected="selected"': '';?>><?= ucwords(str_replace('_', ' ', $land_order_by));?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Slider Skin Image:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="slider_skin_image" value="<?= set_value('slider_skin_image');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Overlay Filename for Ipad:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="overlay_filename_ipad" value="<?= set_value('overlay_filename_ipad');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Mapovis Not Supported URL:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="mapovis_not_supported_url" value="<?= set_value('mapovis_not_supported_url');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Custom Font Markup:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="custom_font_markup" value="<?= set_value('custom_font_markup');?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Custom Google Font Name:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="custom_google_font_name">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Custom Typekit Font Name:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="custom_typekit_font_name">
											</div>
										</div><!-- End .form-group-->

										<div class="col-lg-11 alert alert-warning">If you want to use a completely custom design you can provide an IFrame.</div>
										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Sales Office Iframe HTML:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="sales_office_iframe_html">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Number of Precincts:</label>
											<div class="col-lg-1">
												<input type="text" class="form-control" id="totalprecincts" name="totalprecincts" value="<?= set_value('totalprecincts');?>">
											</div>
										</div><!-- End .form-group  --> 
										<br>
									</div>

									<div class="step" id="second">
										<span class="step-info" data-num="2" data-text="Precincts"></span>

										<div id="allprecincts">
											<?php $totalprecincts = (set_value('totalprecincts'))? set_value('totalprecincts'): 0;?>
											<?php for($x = 1; $x <= $totalprecincts; $x++):?>
												<div class="form-group allprecincts" precinct_id="<?= $x?>">
													<label class="col-lg-2 control-label" for="required">Number of Stages in Precinct <?= $x?>:</label>
													<div class="col-lg-1">
														<input type="text" class="form-control" id="numberofstagesinprecinct1" name="precinct_stages[<?= $x?>]" value="<?= set_value('precinct_stages['.$x.']');?>">
													</div>
												</div><!-- End .form-group  -->
											<?php endfor;?>
										</div>
									</div>

									<div class="step" id="last">
										<span class="step-info" data-num="3" data-text="Stages"></span>
										<div class="col-lg-9">
											<strong>The following precincts and stages will be added. Click on 'Next' to create the new Development.</strong>
										</div>
										<br /><br />
										<div id="allstages"></div>
									</div>

									<div class="wizard-actions full">
										<button class="btn btn-default pull-left" type="reset"> Back </button>
										<button class="btn btn-default pull-right" type="submit"> Next </button>
									</div>
								</form>
							</div>

						</div><!-- End .panel -->
					</div><!-- End .span8 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
