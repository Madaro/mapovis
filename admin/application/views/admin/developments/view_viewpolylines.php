<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-location-2"></span>
									<span><?= $development->development_name;?> Polylines</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($polylines)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>ID</th>
                                            <th>Name</th>
											<th>Stroke Color</th>
                                            <th>Stroke Opacity</th>
                                            <th>Stroke Weight</th> 
											<th></th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_id" placeholder="" class="search_init" style="width: 100%;" /></td>
                                            <td><input type="text" name="search_name" placeholder="" class="search_init" style="width: 100%;" /></td> 
											<td><input type="text" name="search_color" placeholder="" class="search_init" style="width: 100%;" /></td>
                                            <td><input type="text" name="search_opacity" placeholder="" class="search_init" style="width: 100%;" /></td>
                                            <td><input type="text" name="search_weight" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($polylines as $polyline):?>
										<tr>
											<td><?= $polyline->id;?></td>
                                            <td><?= $polyline->polylinename;?></td>
											<td><?= $polyline->stroke_color;?></td>
                                            <td><?= (float)$polyline->stroke_opacity;?></td>
                                            <td><?= $polyline->stroke_weight;?></td>  
											<td class="center">
												<button type="button" type_id="<?= $polyline->id;?>" class="btn btn-xs btn-default view_type_btn">Edit</button>
											</td>
											<td>
												<a href="<?= "{$base_url}deletepolyline/{$polyline->id}"; ?>">
													<button type="button" class="delete_polyline_btn btn btn-xs btn-danger">Delete</button>
												</a>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not polylines in the system.</div>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Image field necesary to call Aviary and crop the images -->
<img id='imageupload' src='' style="display:none"/>
<!-- Dialog -->
<div id="dialog_polyline"></div>
