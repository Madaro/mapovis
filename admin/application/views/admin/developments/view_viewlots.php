<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
				<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default gradient">
								<div class="panel-heading">
									<h4>
										<span class="icon16 entypo-icon-document-2"></span>
										<span><?= $development->development_name;?> Lots</span>
									</h4>
								</div>
								<div class="panel-body noPad clearfix">
									<?php if(count($lots)):?>
									<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
										<thead>
											<tr>
												<th>Lot ID</th>
												<th>Precinct</th>
												<th>Stage</th>
												<th>Lot</th>
												<th>Status</th>
												<th>Titled</th>
												<th></th>
												<th></th>
											</tr>
											<tr>
											  <td><input type="text" name="search_lot_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_precinct" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_stage" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_lot_number" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_status" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_titled" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td></td>
											  <td></td>
											</tr>
										</thead>
										<tbody>
											<?php foreach($lots as $lot):?>
											<?php $lot_id = $lot->lot_id;?>
											<tr>
												<td class="center"><?= $lot_id;?></td>
												<td class="center"><?= $lot->precinct_number;?></td>
												<td class="center"><?= $lot->stage_number;?></td>
												<td class="center"><?= $lot->lot_number;?></td>
												<td class="center"><?= $lot->status;?></td>
												<td class="center"><?= ($lot->lot_titled == 1)? 'Yes': 'No';?></td>
												<td class="center" style="">
													<button title="Lot ID <?= $lot_id;?> [ Precinct <?= $lot->precinct_number;?> | Stage <?= $lot->stage_number;?> | Lot <?= $lot->lot_number;?> ]" lot_id="<?= $lot_id;?>" class="view_lot_btn btn btn-xs btn-default">Update</button>
												</td>
												<td class="center">
													<a href="<?= $base_url.'deletelot/'.$lot->lot_id;?>"><button type="button" class="btn btn-xs btn-danger" onclick="if(!confirm('Are you sure you want to delete the lot?')){return false;}">Delete</button></a>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
									<?php else:?>
										<div class="panel-body">
											<div class="alert alert-warning">There are not lots in the system.</div>
											<a href="<?= $base_url; ?>addlots/<?= $development->development_id;?>" style="padding-left: 20px;">
												<button type="button" class="btn btn-info">Add Lots</button>
											</a>
										</div>
									<?php endif;?>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span12 -->

					</div><!-- End .row -->

					<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

	</div><!-- End #wrapper -->
<!-- Dialog -->
<div id="dialog_lot"></div>
