<?php
$manage_url = base_url().'admin/developments/managedevelopmentid/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3>Developments</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->
				<?= $alert_message;?>

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default gradient">
								<div class="panel-heading">
									<h4>
										<span>Manage Developments</span>
									</h4>
								</div>
								<div class="panel-body noPad clearfix">
									<?php if (empty($developments)):
										echo 'No results found';?>
									<?php else:?>
									<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
										<thead>
											<tr>
												<th>ID</th>
												<th>Development</th>
												<th>Developer</th>
												<th>State</th>
												<th></th>
											</tr>
											<tr>
												<td><input type="text" name="search_dev_id" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_development_name" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_developer" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td><input type="text" name="search_state" placeholder="" class="search_init" style="width: 100%;" /></td>
												<td></td>

											</tr>
										</thead>
										<tbody>
											<?php foreach($developments as $development):?>
											<tr>
												<td style=""><?= $development->development_id;?></td>
												<td style=""><?= $development->development_name;?></td>
												<td style=""><?= isset($developers[$development->developerid])? $developers[$development->developerid]->developer: 'N/A';?></td>
												<td class="center"><?= $development->state;?></td>
												<td class="center" style="">
													<a href="<?= $manage_url.$development->development_id;?>"><button type="button" id="opener1" class="btn btn-xs btn-default">Manage</button></a>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
									<?php endif;?>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span12 -->

					</div><!-- End .row -->

					<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
