<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
$('#dialog_pricelistsubscriber').dialog({ autoOpen: false });

$('.view_type_btn').on('click', function(){
	var type_id = $(this).attr('type_id');
	showLoading();
	$.post('<?= base_url();?>admin/developments/viewpricelistsubscriber/'+type_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_pricelistsubscriber').html(data);
				$('#dialog_pricelistsubscriber').dialog({
					title: 'Update Price List Subscriber',
					height: 440,
					width: 550,
					modal: true,
					resizable: false,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_pricelistsubscriber').html('');}
				});
				$('#dialog_pricelistsubscriber').dialog('open');
				$("input, textarea, select").not('.nostyle').uniform();
				hideLoading();
				$('.save_changes').click(function() {
					showLoading();
				});
				$('.cancel_changes').click(function() {
					$('#dialog_pricelistsubscriber').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
$('.delete_pricelist_subscriber_btn').click(function() {
	if(!confirm('Are you sure you want to delete the price list subscriber?')){
		return false;
	}
	showLoading();
});
</script>