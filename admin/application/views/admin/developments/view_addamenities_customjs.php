<script>
function launchEditor(id, src, filetype, dialog, amenity_id, force_crop){
	var editor_params = {
		image: id,
		url: src,
		fileFormat: filetype,
		onSave: function(imageID, newURL) {
			addNewImageHtml(amenity_id, newURL, newURL);
			var img = document.getElementById(imageID);
			img.src = newURL;
			showhide_AddNew(amenity_id);
			featherEditor.close();
		},
	};
	if(force_crop){
		editor_params.forceCropPreset = ['Width: 350px, Height: 222px','350x222'];
		editor_params.initTool = 'crop';
		editor_params.cropPresetsStrict = 'true';
	}
	else{
		editor_params.tools = 'enhance,effects,frames,stickers,focus,brightness,contrast,saturation,warmth,sharpness,colorsplash,draw,text,redeye,whiten,blemish';
	}
	featherEditor.launch(editor_params);
}
initViewEntity();
$('.add_image_btn').on('click', function(e){
	var image_field = $(this).attr('image_field');
	var amenity_id = $(this).attr('amenity_id');
	var image_field_obj = $('#'+image_field);
	var input = image_field_obj[0];
	if (input.files && input.files[0]) {
		if(!validateimagefiletype(input.files[0].type)){
			/* shows dialog message */
			return false;
		}
		var reader = new FileReader();
		// set where you want to attach the preview
		reader.target_elem = $(input).parent().find('preview');
		reader.onload = function (e) {
			var force_crop = true;
			var img = document.createElement("img");
			img.onload = function(){
				if(img.width == 350 && img.height == 222){
					force_crop = false;
				}
				$("#imageupload").attr('src', e.target.result);
				launchEditor('imageupload', e.target.result, input.files[0].type, $('#dialog_amenity'), amenity_id, force_crop);
			};
			img.src = e.target.result;
		};
		reader.readAsDataURL(input.files[0]);
	}
});
$('#save_new_amenity').click(function() {
	showLoading();
});
$('.delete_house_btns').click(function() {
	var amenity_name = $(this).attr('amenity_name');
	if(!confirm('Are you sure you want to delete amenity "'+amenity_name+'"?')){
		return false;
	}
	showLoading();
});
</script>