<?php
$base_url       = base_url().'admin/developments/';
$image_base_url = base_url().'../mpvs/images/external_amenities/';
$ext_amenity_id = $external_amenity->external_amenity_id;
?>
	<!-- Image field necesary to call Aviary and crop the images -->
	<img id='imageupload' src='' style="display:none"/>
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<div class="col-lg-9">
			<div class="panel-body">
				<form method="post" class="form-horizontal" action="<?= $base_url; ?>updateexternalamenity/<?= $ext_amenity_id;?>" role="form">
					<div class="form-group">
						<label class="col-lg-3 control-label" for="required">Zoom to Amenity:</label>
						<div class="col-lg-5">
							<a href="<?= $base_url; ?>../../../mapovis/development.php?developmentId=<?= $external_amenity->development_id;?>&zoom_to_latitude=<?= ((float)$external_amenity->e_amenity_latitude);?>&zoom_to_longitude=<?= ((float)$external_amenity->e_amenity_longitude);?>&zoom_level=19" target="_blank"><button type="button" class="btn btn-xs btn-default">Zoom Here Now</button></a>
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-3 control-label" for="required">Show External Amenity:</label>
						<div class="col-lg-5">
							<input type="radio" name="show_e_amenity" value="1" <?= ($external_amenity->show_e_amenity == 1? 'checked="checked"': '');?> id="show_e_amenity1"><label for="show_e_amenity1"> On </label> &nbsp;&nbsp;
							<input type="radio" name="show_e_amenity" value="0" <?= ($external_amenity->show_e_amenity == 0? 'checked="checked"': '');?> id="show_e_amenity0"><label for="show_e_amenity0"> Off </label>
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Type:</label>
						<div class="col-lg-9">
							<select name="e_amenity_type_id" class="form-control">
								<?php foreach($amenity_types as $amenity_type):?>
									<option value="<?= $amenity_type->external_amenity_type_id;?>" <?= ($external_amenity->e_amenity_type_id == $amenity_type->external_amenity_type_id)? 'selected="selected"': ''?>><?= $amenity_type->e_amenity_type_name;?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Name:</label>
						<div class="col-lg-9">
						<input name="e_amenity_name" type="text" class="form-control" value="<?= $external_amenity->e_amenity_name; ?>">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Latitude:</label>
						<div class="col-lg-9">
						<input name="e_amenity_latitude" type="text" class="form-control" value="<?= (float)$external_amenity->e_amenity_latitude;?>">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Longitude:</label>
						<div class="col-lg-9">
						<input name="e_amenity_longitude" type="text" class="form-control" value="<?= (float)$external_amenity->e_amenity_longitude;?>">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Description:</label>
						<div class="col-lg-9">
							<div class="form-row">
								<textarea class="tinymce" name="e_amenity_description"><?= $external_amenity->e_amenity_description;?></textarea>
							</div>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">More Info Link:</label>
						<div class="col-lg-9">
						<input name="e_amenity_moreinfo_url" type="text" class="form-control" value="<?= $external_amenity->e_amenity_moreinfo_url;?>">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Icon:</label>
						<div class="col-lg-9">
						<input name="e_amenity_icon" type="text" class="form-control" value="<?= $external_amenity->e_amenity_icon;?>">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label">Address:</label>
						<div class="col-lg-9">
						<input name="e_amenity_address" type="text" class="form-control" value="<?= $external_amenity->e_amenity_address;?>">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Pictures:</label>
						<div class="col-lg-9">
							<div id="add_image_section<?= $ext_amenity_id;?>">
								<input type="file" name="image_file" id="image_file<?= $ext_amenity_id;?>" class="image_files" value="" accept="image/*" >
								<button type="button" id="add_image_btn" class="add_image_btn btn btn-success" image_field="image_file<?= $ext_amenity_id;?>" amenity_id="<?= $ext_amenity_id;?>" href="#">Add & Edit Photo</button>
								<br/><br/>
							</div>
							<div id="qLoverlaymessage_<?= $ext_amenity_id;?>" class="qLoverlaymessage" style="display:none;"></div>
							<div id="qLmessage_<?= $ext_amenity_id;?>" class="qLmessage" style="display:none;"></div>
							<div class="alert alert-info">
								Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
							</div>
							<div id="img_container">
								<ul class="sortable" id="sortable_<?= $ext_amenity_id;?>">
									<?php for($xyz = 1; $xyz <= 5; $xyz++):
										$image_field = 'e_amenity_picture'.$xyz;
										$image_name  = $external_amenity->$image_field;
										if(empty($image_name)){
											break;
										}
										$image_url   = "{$image_base_url}{$image_name}";
									?>
										<li class="ui-state-default" >
											<input name="image_names[]" type="text" value="<?= $image_name;?>" style="display:none">
										  <img src="<?= $image_url;?>" width="135" height="96" />
										  <button type="button" amenity_id="<?= $ext_amenity_id;?>" image_id="<?= $image_name?>" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button>
										</li>
									<?php endfor;?>
								</ul>
							</div>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group" style="padding-top:10px">
						<div class="col-lg-offset-3 col-lg-9">
							<button type="submit" class="save_amenity_changes btn btn-info">Save Changes</button>
							<button type="button" class="cancel_amenity_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
						</div>
					</div><!-- End .form-group  -->

					<div class="alert alert-warning">If you want to use a completely custom design you can provide an IFrame.</div>
					<div class="form-group">
						<label class="col-lg-3 control-label" for="required">External Amenity IFrame HTML:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="external_amenity_iframe_html" value="<?= htmlspecialchars($external_amenity->external_amenity_iframe_html, ENT_QUOTES);?>">
						</div>
					</div><!-- End .form-group-->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="required">Show Scrollbar:</label>
						<div class="col-lg-5">
							<input type="radio" name="external_amenity_iframe_scrollbar" value="1" <?= ($external_amenity->external_amenity_iframe_scrollbar == 1? 'checked="checked"': '');?> id="external_amenity_iframe_scrollbar1" class="external_amenity_iframe_scrollbar"><label for="external_amenity_iframe_scrollbar1"> On </label> &nbsp;&nbsp;
							<input type="radio" name="external_amenity_iframe_scrollbar" value="0" <?= ($external_amenity->external_amenity_iframe_scrollbar == 0? 'checked="checked"': '');?> id="external_amenity_iframe_scrollbar0" class="external_amenity_iframe_scrollbar"><label for="external_amenity_iframe_scrollbar0"> Off </label>
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group" style="padding-top:10px">
						<div class="col-lg-offset-3 col-lg-9">
							<button type="submit" class="save_amenity_changes btn btn-info">Save Changes</button>
							<button type="button" class="cancel_amenity_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
						</div>
					</div><!-- End .form-group  -->

				</form>
			</div>

	</div><!-- End .span6 -->
