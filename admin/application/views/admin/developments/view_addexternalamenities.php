<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
$amenity_id         = 0;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Add External Amenity</span>
								</h4>
							</div>
							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>addexternalamenities/<?= $development->development_id;?>" role="form">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Show External Amenity:</label>
									<div class="col-lg-5">
										<input type="radio" name="show_e_amenity" value="1" checked="checked" id="show_e_amenity1"><label for="show_e_amenity1"> On </label> &nbsp;&nbsp;
										<input type="radio" name="show_e_amenity" value="0" id="show_e_amenity0"><label for="show_e_amenity0"> Off </label>
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Type:</label>
									<div class="col-lg-9">
										<select name="e_amenity_type_id" class="form-control">
											<?php foreach($amenity_types as $amenity_type):?>
												<option value="<?= $amenity_type->external_amenity_type_id;?>"><?= $amenity_type->e_amenity_type_name;?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Name:</label>
									<div class="col-lg-9">
									<input name="e_amenity_name" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Latitude:</label>
									<div class="col-lg-9">
									<input name="e_amenity_latitude" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Longitude:</label>
									<div class="col-lg-9">
									<input name="e_amenity_longitude" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Description:</label>
									<div class="col-lg-9">
										<div class="form-row">
											<textarea class="tinymce" name="e_amenity_description">
											</textarea>
										</div>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">More Info Link:</label>
									<div class="col-lg-9">
									<input name="e_amenity_moreinfo_url" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Icon:</label>
									<div class="col-lg-9">
									<input name="e_amenity_icon" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" >Address:</label>
									<div class="col-lg-9">
									<input name="e_amenity_address" type="text" class="form-control">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="textareas">Pictures:</label>
									<div class="col-lg-9">
										<div id="add_image_section<?= $amenity_id;?>">
											<input type="file" name="image_file" id="image_file<?= $amenity_id;?>" class="image_files" value="" accept="image/*" >
											<button type="button" id="add_image_btn" class="add_image_btn btn btn-success" image_field="image_file<?= $amenity_id;?>" amenity_id="<?= $amenity_id;?>" href="#">Add & Edit Photo</button>
											<br/><br/>
										</div>

										<div class="alert alert-info">
											Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
										</div>
										<div id="img_container">
											<ul class="sortable" id="sortable_<?= $amenity_id;?>"></ul>
										</div>
									</div>
								</div><!-- End .form-group  -->

								<div class="col-lg-11 alert alert-warning">If you want to use a completely custom design you can provide an IFrame.</div>
								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">External Amenity IFrame HTML:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="external_amenity_iframe_html">
									</div>
								</div><!-- End .form-group-->

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Show Scrollbar:</label>
									<div class="col-lg-5">
										<input type="radio" name="external_amenity_iframe_scrollbar" value="1" checked="checked" id="external_amenity_iframe_scrollbar1" class="external_amenity_iframe_scrollbar"><label for="external_amenity_iframe_scrollbar1"> On </label> &nbsp;&nbsp;
										<input type="radio" name="external_amenity_iframe_scrollbar" value="0" id="external_amenity_iframe_scrollbar0" class="external_amenity_iframe_scrollbar"><label for="external_amenity_iframe_scrollbar0"> Off </label>
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group" style="padding-top:10px">
									<div class="col-lg-offset-1 col-lg-9">
										<span><button id="action_continue_btn" type="submit" class="btn btn-primary save_new_amenity">Add Amenity & Continue</button></span>
										<span style="padding-left: 10px;"><button id="action_btn" type="submit" class="btn btn-info save_new_amenity">Add Amenity & Finish</button></span>
										<span style="padding-left: 10px;"><a href="<?= $manage_development; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
									</div>
								</div><!-- End .form-group  -->
								<input id="action_continue" name="action_continue" type="hidden" value="0">
							</form>
							</div><!-- End .body -->

						</div><!-- End .panel -->
					</div><!-- End .span1 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
		<!-- Image field necesary to call Aviary and crop the images -->
		<img id='imageupload' src='' style="display:none"/>
