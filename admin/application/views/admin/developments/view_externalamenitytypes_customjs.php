<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
$('#dialog_amenity').dialog({ autoOpen: false });

$('.view_type_btn').on('click', function(){
	var type_id = $(this).attr('type_id');
	showLoading();
	$.post('<?= base_url();?>admin/developments/viewexternalamenityType/'+type_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_amenity').html(data);
				$('#dialog_amenity').dialog({
					title: 'Update External Amenity Type',
					height: 710,
					width: 600,
					modal: true,
					resizable: false,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_amenity').html('');}
				});
				$('#dialog_amenity').dialog('open');
				$("input, textarea, select").not('.nostyle').uniform();
				$('.change_colour').on('change', function(){
					var color = $(this).val();
					var box_id = $(this).attr('name')+'_disp';
					$('#'+box_id).attr('style', 'width:50px !important;border: 2px solid '+color+';background-color:'+color+';')
				});
				hideLoading();
				$('.save_changes').click(function() {
					showLoading();
				});
				$('.cancel_changes').click(function() {
					$('#dialog_amenity').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
$('.delete_amenity_type_btn').click(function() {
	var type_name = $(this).attr('type_name');
	if(!confirm('Are you sure you want to delete external-amenity type "'+type_name+'"?')){
		return false;
	}
	showLoading();
});
</script>