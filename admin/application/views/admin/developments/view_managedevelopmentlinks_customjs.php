<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
$('.update_link_btn').on('click', function(){
	var dev_id1 = $(this).attr('development_id1');
	var dev_id2 = $(this).attr('development_id2');
	var title   = $(this).attr('title');
	showLoading();
	$.post('<?= base_url();?>admin/developments/editdevelopmentlink/'+dev_id1+'/'+dev_id2,
		function(data,status){
			if(status === 'success'){
				$('#dialog_link').html(data);
				$('#dialog_link').dialog({
					title: title,
					height: 280,
					width: 500,
					modal: true,
					resizable: true,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_link').html('');}
				});
				$('#dialog_link').dialog('open');
				$("input, textarea, select").not('.nostyle').uniform();
				hideLoading();
				$('#save_link_changes').click(function() {
					showLoading();
				});
				$('#cancel_link_changes').click(function() {
					$('#dialog_link').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
$('#dialog_link').dialog({autoOpen: false});
</script>