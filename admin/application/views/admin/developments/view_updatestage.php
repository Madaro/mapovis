<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
				<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-documents"></span>
									<span>Update Stage</span>
								</h4>
							</div>

							<div class="panel-body">
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>updatestage/<?= $stage->stage_id;?>" role="form" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-lg-3 control-label">Precint #:</label>
									<div class="col-lg-2">
										<?= $stage->precinct_number;?>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label">Stage #:</label>
									<div class="col-lg-2">
										<?= $stage->stage_number;?>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="textareas">Stage Code:</label>
									<div class="col-lg-4">
										<input name="stage_code" type="text" class="form-control" value="<?= $stage->stage_code;?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="textareas">Stage Name:</label>
									<div class="col-lg-4">
										<input name="stage_name" type="text" class="form-control" value="<?= $stage->stage_name;?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="required">Use Stage Name (instead of number):</label>
									<div class="col-lg-4">
										<input type="radio" name="display_stage_name" value="1" <?= ($stage->display_stage_name == 1? 'checked="checked"': '');?> id="display_stage_name1" class="display_stage_name"><label for="display_stage_name1">&nbsp; Yes </label> &nbsp;&nbsp;
										<input type="radio" name="display_stage_name" value="0" <?= ($stage->display_stage_name == 0? 'checked="checked"': '');?> id="display_stage_name0" class="display_stage_name"><label for="display_stage_name0">&nbsp; No </label>
									</div>
								</div><!-- End .form-group-->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="textareas">Title Release:</label>
									<div class="col-lg-4">
										<input name="title_release" type="text" class="form-control" value="<?= $stage->title_release;?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label" >Stage Icon:</label>
									<div class="col-lg-4">
										<input name="stage_icon" type="text" class="form-control" value="<?= $stage->stage_icon;?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label" >Stage Icon Width:</label>
									<div class="col-lg-4">
										<input name="stage_icon_width" type="text" class="form-control" value="<?= $stage->stage_icon_width;?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label" >Stage Icon Height :</label>
									<div class="col-lg-4">
										<input name="stage_icon_height" type="text" class="form-control" value="<?= $stage->stage_icon_height;?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label" >Stage Code:</label>
									<div class="col-lg-3">
										<input name="stage_code" type="text" class="form-control" value="<?= $stage->stage_code;?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="buttons">Stage Icon Coordinates:</label>
									<div class="col-lg-8">
										<div class="row">
											<div class="col-lg-3">
												<div class="input-group">
													<input name="stage_icon_latitude" type="text" class="form-control" style="width: 200px !important;" id="southwestcoordinates-latitude"
													value="<?= (float)$stage->stage_icon_latitude;?>">
												</div><!-- /input-group -->
											</div>
											<div class="col-lg-3">
											 <div class="input-group">
													<input name="stage_icon_longitude" type="text" class="form-control" style="width: 200px !important;" id="southwestcoordinates-longitude"
													value="<?= (float)$stage->stage_icon_longitude;?>">
												</div><!-- /input-group -->
											</div>
										</div>
									</div>
								</div><!-- End .form-group-->

								<div class="form-group">
									<label class="col-lg-3 control-label" for="buttons">Center Coordinates:</label>
									<div class="col-lg-8">
										<div class="row">
											<div class="col-lg-3">
												<div class="input-group">
													<input name="stage_center_latitude" type="text" class="form-control" style="width: 200px !important;" id="southwestcoordinates-latitude"
													value="<?= (float)$stage->stage_center_latitude;?>">
												</div><!-- /input-group -->
											</div>
											<div class="col-lg-3">
											 <div class="input-group">
													<input name="stage_center_longitute" type="text" class="form-control" style="width: 200px !important;" id="southwestcoordinates-longitude"
													value="<?= (float)$stage->stage_center_longitute;?>">
												</div><!-- /input-group -->
											</div>
										</div>
									</div>
								</div><!-- End .form-group-->

								<div class="form-group">
									<label class="col-lg-3 control-label">Zoom Level:</label>
									<div class="col-lg-2">
										<input name="stage_zoomlevel" type="text" class="form-control" id="stage_zoomlevel" value="<?= $stage->stage_zoomlevel;?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label" >Stage Icon - Show @ Zoom Level:</label>
									<div class="col-lg-2">
										<input name="stage_icon_zoomlevel_show" type="text" class="form-control" value="<?= $stage->stage_icon_zoomlevel_show;?>">
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label" >Stage Icon - Hide @ Zoom Level:</label>
									<div class="col-lg-2">
										<input name="stage_icon_zoomlevel_hide" type="text" class="form-control" value="<?= $stage->stage_icon_zoomlevel_hide;?>">
									</div>
								</div><!-- End .form-group  -->
                                
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" >Stage Polygon Clickable @ Zoom Level:</label>
                                    <div class="col-lg-2">
                                        <input name="stage_polygon_coords_clickable_at_zoom_level" type="text" class="form-control" value="<?= $stage->stage_polygon_coords_clickable_at_zoom_level;?>">
                                    </div>
                                </div><!-- End .form-group  -->
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" >Stage Polygon Un-Clickable @ Zoom Level:</label>
                                    <div class="col-lg-2">
                                        <input name="stage_polygon_coords_unclickable_at_zoom_level" type="text" class="form-control" value="<?= $stage->stage_polygon_coords_unclickable_at_zoom_level;?>">
                                    </div>
                                </div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-3 control-label">Stage PDF File:</label>
									<div class="col-lg-9">
										<div id="lots_pdf_file">
											<input type="file" name="stage_pdf_file" id="stage_pdf_file_field" value="" accept="application/pdf" >
											<?php if($stage->stage_pdf_file):?>
												<span id="current_file">
													<a href="<?= base_url().'../mpvs/templates/dev_stage_pdfs/'.$stage->stage_pdf_file;?>" title="Download File" download><span class="icon16 icomoon-icon-file-pdf"></span></a>
												</span>
												</br></br>
											<?php endif;?>
										</div>
									</div>
								</div><!-- End .form-group  -->

								<div class="col-lg-offset-3">
								 <br>
									<button type="submit" class="btn btn-info">Update</button>
									<a href="<?= $base_url.'viewstages/'.$stage->development_id; ?>"><button type="button" class="btn btn-default">Cancel</button></a>
									<br><br>
								</div>
								</form>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span3 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
