<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
$('#dialog_externalmarker').dialog({ autoOpen: false });

$('.view_type_btn').on('click', function(){
	var type_id = $(this).attr('type_id');
	showLoading();
	$.post('<?= base_url();?>admin/developments/viewexternalmarker/'+type_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_externalmarker').html(data);
				$('#dialog_externalmarker').dialog({
					title: 'Update External Marker',
					height: 340,
					width: 550,
					modal: true,
					resizable: false,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_externalmarker').html('');}
				});
				$('#dialog_externalmarker').dialog('open');
				$("input, textarea, select").not('.nostyle').uniform();
				hideLoading();
				$('.save_changes').click(function() {
					showLoading();
				});
				$('.cancel_changes').click(function() {
					$('#dialog_externalmarker').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
$('.delete_external_marker_btn').click(function() {
	if(!confirm('Are you sure you want to delete the external marker?')){
		return false;
	}
	showLoading();
});
</script>