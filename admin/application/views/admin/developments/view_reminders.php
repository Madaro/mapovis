<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					 <h3>Settings</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-alarm"></span>
									<span>Reminders</span>
								</h4>
							</div>
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>reminders/<?= $development->development_id;?>" role="form">
							<div class="panel-body">
								If a development has not been updated by its corresponding deadline:
								<br><br>
								<div class="alert alert-info">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<h3>Warning Level - 1</h3>
									Primary user will receive an EMAIL & SMS warning 'A' hour(s) after the update deadline has passed.
									<br><br>

									<div class="form-group">
										<label class="col-lg-1 control-label" for="normalInput" style="padding-top: 8px;">'A' Hour(s):</label>
										<div class="col-lg-1">
											<input name="level_1_a" type="text" class="form-control" value="<?= $reminder->level_1_a;?>">
										</div>
									</div>
								</div>

								<div class="alert alert-warning">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<h3>Warning Level - 2</h3>
									Primary & secondary users will receive an EMAIL & SMS warning 'B' hour(s) after the update deadline has passed.
									<br><br>
									<div class="form-group">
										<label class="col-lg-1 control-label" for="normalInput" style="padding-top: 8px;">'B' Hour(s):</label>
										<div class="col-lg-1">
											<input name="level_2_b" type="text" class="form-control" value="<?= $reminder->level_2_b;?>">
										</div>
									</div>
									<br>
									Primary & secondary users will continue to receive EMAIL & SMS warnings every 'C' hour(s), between 9am and 9pm, Monday to Friday.
									<br><br>
									<div class="form-group">
										<label class="col-lg-1 control-label" for="normalInput" style="padding-top: 8px;">'C' Hour(s):</label>
										<div class="col-lg-1">
											<input name="level_2_c" type="text" class="form-control" value="<?= $reminder->level_2_c;?>">
										</div>
									</div>
								</div>

								<div class="alert alert-danger">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<h3>Warning Level - 3</h3>
									The user's manager, primary and secondary user will receive an email notification 'D' hour(s) after the update deadline has passed.
									<br><br>

									<div class="form-group">
										<label class="col-lg-1 control-label" for="normalInput" style="padding-top: 8px;">'D' Hour(s):</label>
										<div class="col-lg-1">
											<input name="level_3_d" type="text" class="form-control" value="<?= $reminder->level_3_d;?>">
										</div>
									</div>
								</div>

								<div class="alert alert-problemuser">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<h3>Problem User</h3>
									Users account will be marked as a 'Problem Users' and an email sent to problemusers@mapovis.com.au, 'E' hour(s) after the deadline has passed.
									<br><br>

									<div class="form-group">
										<label class="col-lg-1 control-label" for="normalInput" style="padding-top: 8px;">'E' Hour(s):</label>
										<div class="col-lg-1">
											<input name="level_4_e" type="text" class="form-control" value="<?= $reminder->level_4_e;?>">
										</div>
									</div>
								</div>

								<div style="padding-left:10px;">
									<br>
									<button type="submit" class="btn btn-info">Update</button>
									<span style="padding-left:10px;"><a href="<?= $manage_development; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
									<br><br>
								</div>
							</div>
							</form>

						</div><!-- End .panel -->

					</div><!-- End .span3 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
