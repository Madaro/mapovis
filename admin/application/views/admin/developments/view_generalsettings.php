<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
<style>
	.ui-widget{
		font-size:13px;
		border: 0;
	}
	.ui-tabs {
		position: relative;/* position: relative prevents IE scroll bug (element with position: relative inside container with overflow: auto appear as "fixed") */
		padding: .2em;
	}
	.ui-tabs .ui-tabs-nav li.ui-tabs-active {
		margin-bottom: -1px;
		padding-bottom: 1px;
	}
	.ui-tabs .ui-tabs-nav {
		margin: 0;
		padding: .2em .2em 0;
		border-bottom: 1px solid #aaaaaa;
	}
	.ui-tabs .ui-tabs-nav li {
		list-style: none;
		float: left;
		position: relative;
		top: 0;
		margin: 1px .2em 0 0;
		border-bottom-width: 0;
		padding: 0;
		white-space: nowrap;
	}
	.ui-tabs .ui-tabs-nav .ui-tabs-anchor {
		float: left;
		padding: .5em 1em;
		font-weight: bold;
	}
	.ui-tabs .ui-tabs-panel {
		display: block;
		border: 1px solid #aaaaaa;
		border-top: 0;
		padding: 1em 1.4em;
		background: none;
	}
	.ui-widget-content a {
		font-size:14px;
		color: #222222;
	}
	.ui-widget-header {
		background: #ffffff;
	}
	.ui-state-default{
		background: #cfd6ec;
	}
	.ui-state-active{
		border: 1px solid #aaaaaa;
		background: #ffffff !important;
		color: #212121;
	}
</style>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					 <h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3> 
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4>
										<span class="icon16 entypo-icon-settings"></span>
										<span>Settings</span>
									</h4>
								</div>
								<div class="panel-body">
								<?= $validation_msg;?>
								<form id="generalsettinsForm" method="post" class="form-horizontal" action="<?= $base_url; ?>generalsettings/<?= $development->development_id;?>" role="form" enctype="multipart/form-data">
								<div id="tabs">
									<ul>
										<li><a href="#tabs-1">General Settings</a></li>
										<li><a href="#tabs-2">Colour Settings</a></li>
										<li><a href="#tabs-3">MAPOVIS Mobile</a></li>
										<li><a href="#tabs-4">MAPOVIS Mini</a></li>
										<li><a href="#tabs-5">Nearby Search</a></li>
										<li><a href="#tabs-6">Display Village</a></li>
										<li><a href="#tabs-7">Sales Office</a></li>
										<li><a href="#tabs-8">Lot Polygons</a></li>
										<li><a href="#tabs-9">Map Styling</a></li>
										<li><a href="#tabs-10">Facebook</a></li>
										<li><a href="#tabs-11">PDF Settings</a></li>
										<li><a href="#tabs-12">Legal</a></li>
										<li><a href="#tabs-13">Compass</a></li>
										<li><a href="#tabs-14">Cluster</a></li>
                                        <li><a href="#tabs-15">Status</a></li>
                                        <li><a href="#tabs-16">Client Links</a></li> 
                                        <li><a href="#tabs-17">Lot Icons</a></li>  
									</ul>
									<div id="tabs-1">
										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Name of the Development:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="nameofthedevelopment" name="development_name" value="<?= set_value('development_name', $development->development_name);?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">URI for the Development:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="uriforthedevelopment" name="development_uri" value="<?= $development->development_uri;?>">
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Development Icon:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" name="developmenticon" value="<?= $development->developmenticon;?>">
											</div>
										</div><!-- End .form-group-->
                                        
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label" for="required">Multiple Development Icon:</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" name="multipledevelopmenticon_url" value="<?= $development->multipledevelopmenticon_url;?>">
                                            </div>
                                        </div><!-- End .form-group-->
                                        
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label" for="required">Multiple Development Icon Width:</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" name="multipledevelopmenticon_width" value="<?= $development->multipledevelopmenticon_width;?>">
                                            </div>
                                        </div><!-- End .form-group-->
                                        
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label" for="required">Multiple Development Icon Height:</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" name="multipledevelopmenticon_height" value="<?= $development->multipledevelopmenticon_height;?>">
                                            </div>
                                        </div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Developer:</label>
											<div class="col-lg-4">
												<select name="developerid" class="form-control">
													<option>-- Select Developer --</option>
													<?php foreach($developers as $developer):?>
													<option value="<?= $developer->developer_id;?>" <?= ($developer->developer_id == $development->developerid)? 'selected="selected"': '';?>><?= $developer->developer;?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div><!-- End .form-group-->

										<div class="form-group">
											<label class="col-lg-3 control-label" for="required">State:</label>
											<div class="col-lg-4">
												<select name="state" class="form-control">
													<?php foreach($states as $state):?>
													<option value="<?= $state;?>" <?= ($state == $development->state)? 'selected="selected"' : '';?>><?= $state;?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div><!-- End .form-group-->

									<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Overlay File Name:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="overlay_filename" name="overlay_filename" value="<?= $development->overlay_filename;?>">
											</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Overlay South West Coordinates:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input name="southwest_latitude" type="text" class="form-control" style="width: 200px !important;" id="southwestcoordinates-latitude"
														value="<?= (float)$development->southwest_latitude;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<input name="southwest_longitude" type="text" class="form-control" style="width: 200px !important;" id="southwestcoordinates-longitude"
														value="<?= (float)$development->southwest_longitude;?>">
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Overlay North East Coordinates:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input name="northeast_latitude" type="text" class="form-control" style="width: 200px !important;" id="northeastcoordinates-latitude"
														value="<?= (float)$development->northeast_latitude;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<input name="northeast_longitude" type="text" class="form-control" style="width: 200px !important;" id="northeastcoordinates-longitude"
														value="<?= (float)$development->northeast_longitude;?>">
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Centre of Development Coordinates:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input name="centre_latitude" type="text" class="form-control" style="width: 200px !important;" id="centrecoordinates-latitude"
														value="<?= (float)$development->centre_latitude;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<input name="centre_longitude" type="text" class="form-control" style="width: 200px !important;" id="centrecoordinates-longitude"
														value="<?= (float)$development->centre_longitude;?>">
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Main Entry/Exit Road Coordinates:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input name="entryexit_latitude" type="text" class="form-control" style="width: 200px !important;" id="entryexitroadcoordinates-latitude"
														value="<?= (float)$development->entryexit_latitude;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<input name="entryexit_longitude" type="text" class="form-control" style="width: 200px !important;" id="entryexitroadcoordinates-longitude"
														value="<?= (float)$development->entryexit_longitude;?>">
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Zoom to Community Feature:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input name="zoom_to_community_feature_latitude" type="text" class="form-control" style="width: 200px !important;" 
														value="<?= (float)$development->zoom_to_community_feature_latitude;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<input name="zoom_to_community_feature_longitude" type="text" class="form-control" style="width: 200px !important;" 
														value="<?= (float)$development->zoom_to_community_feature_longitude;?>">
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">VIC Public Transport Origin:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="vic_public_transport_origin" name="vic_public_transport_origin" value="<?= $development->vic_public_transport_origin;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Google Analytics Tracking ID:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="ga_tracking_id" name="ga_tracking_id" value="<?= $development->ga_tracking_id;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">White List Domain:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="dev_domain" name="dev_domain" value="<?= $development->dev_domain;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
											<label class="col-lg-3 control-label" for="required">Access Key:</label>
											<div class="col-lg-4">
												<input type="text" class="form-control" id="access_key" name="access_key" value="<?= $development->access_key;?>">
											</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Enquiry Form Iframe URL:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="form_iframe_url" value="<?= $development->form_iframe_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Enquiry Form Iframe URL Height:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="form_iframe_url_height" value="<?= $development->form_iframe_url_height;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Enquiry Form Iframe Hidden Field (Lot Number):</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="form_iframe_hiddenfield_lot_number" value="<?= $development->form_iframe_hiddenfield_lot_number;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Enquiry Form Iframe Hidden Field (House Name):</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="form_iframe_hiddenfield_house_name" value="<?= $development->form_iframe_hiddenfield_house_name;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Enquiry Form Iframe Hidden Field (Builder):</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="form_iframe_hiddenfield_builder" value="<?= $development->form_iframe_hiddenfield_builder;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Land Enquiry Form Iframe URL:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="land_form_iframe_url" value="<?= $development->land_form_iframe_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Land Enquiry Form Iframe URL Height:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="land_form_iframe_url_height" value="<?= $development->land_form_iframe_url_height;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Land Starting From:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="land_starting_from" value="<?= round($development->land_starting_from, 2);?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">House & Land Enquiry Form Iframe URL:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="house_and_land_form_iframe_url" value="<?= $development->house_and_land_form_iframe_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">House & Land Enquiry Form Iframe URL Height:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="house_and_land_form_iframe_url_height" value="<?= $development->house_and_land_form_iframe_url_height;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Iframe Form - Show Scroll Bars:</label>
										<div class="col-lg-4">
											<input type="radio" name="form_iframe_show_scroll_bars" value="1" <?= ($development->form_iframe_show_scroll_bars == 1? 'checked="checked"': '');?> id="form_iframe_show_scroll_bars1" class="form_iframe_show_scroll_bars"><label for="form_iframe_show_scroll_bars1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="form_iframe_show_scroll_bars" value="0" <?= ($development->form_iframe_show_scroll_bars == 0? 'checked="checked"': '');?> id="form_iframe_show_scroll_bars0" class="form_iframe_show_scroll_bars"><label for="form_iframe_show_scroll_bars0"> Off </label>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">House & Land API:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="house_and_land_api" value="<?= $development->house_and_land_api;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">House & Land API URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="house_and_land_api_url" value="<?= $development->house_and_land_api_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">House & Land API Development ID:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="house_and_land_api_development_id" value="<?= $development->house_and_land_api_development_id;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Get Directions Example Address:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="get_directions_to_example_address" value="<?= $development->get_directions_to_example_address;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">URL Avatar Icon:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="development_url_icon" value="<?= $development->development_url_icon;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Search by Lot Price:</label>
										<div class="col-lg-5">
											<input type="radio" name="search_by_lot_price" value="1" <?= ($development->search_by_lot_price == 1? 'checked="checked"': '');?> id="search_by_price1"><label for="search_by_price1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="search_by_lot_price" value="0" <?= ($development->search_by_lot_price == 0? 'checked="checked"': '');?> id="search_by_price0"><label for="search_by_price0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Search by Lot Width:</label>
										<div class="col-lg-5">
											<input type="radio" name="search_by_lot_width" value="1" <?= ($development->search_by_lot_width == 1? 'checked="checked"': '');?> id="search_by_width1"><label for="search_by_width1"> On </label>&nbsp;&nbsp;
											<input type="radio" name="search_by_lot_width" value="0" <?= ($development->search_by_lot_width== 0? 'checked="checked"': '');?> id="search_by_width0"><label for="search_by_width0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Search by Lot Size:</label>
										<div class="col-lg-5">
											<input type="radio" name="search_by_lot_size" value="1" <?= ($development->search_by_lot_size == 1? 'checked="checked"': '');?> id="search_by_size1"><label for="search_by_size1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="search_by_lot_size" value="0" <?= ($development->search_by_lot_size == 0? 'checked="checked"': '');?> id="search_by_size0"><label for="search_by_size0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Zoom to Stage Panel:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_zoom_to_stage" value="1" <?= ($development->show_zoom_to_stage == 1? 'checked="checked"': '');?> id="show_zoom_to_stage1"><label for="show_zoom_to_stage1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_zoom_to_stage" value="0" <?= ($development->show_zoom_to_stage == 0? 'checked="checked"': '');?> id="show_zoom_to_stage0"><label for="show_zoom_to_stage0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Zoom to Stage level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="zoom_to_stage_level" value="<?= $development->zoom_to_stage_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Prices:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_pricing" value="1" <?= ($development->show_pricing == 1? 'checked="checked"': '');?> id="show_pricing1"><label for="show_pricing1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_pricing" value="0" <?= ($development->show_pricing == 0? 'checked="checked"': '');?> id="show_pricing0"><label for="show_pricing0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Telephone:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_telephone" value="1" <?= ($development->show_telephone == 1? 'checked="checked"': '');?> id="show_telephone1"><label for="show_telephone1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_telephone" value="0" <?= ($development->show_telephone == 0? 'checked="checked"': '');?> id="show_telephone0"><label for="show_telephone0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Stage Icons:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_stage_icons" value="1" <?= ($development->show_stage_icons == 1? 'checked="checked"': '');?> id="show_stage_icons1"><label for="show_stage_icons1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_stage_icons" value="0" <?= ($development->show_stage_icons == 0? 'checked="checked"': '');?> id="show_stage_icons0"><label for="show_stage_icons0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Statistics for Sales Team:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_statistics_sales_team" value="1" <?= ($development->show_statistics_sales_team == 1? 'checked="checked"': '');?> id="show_statistics_sales_team1" class="show_statistics_sales_team"><label for="show_statistics_sales_team1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_statistics_sales_team" value="0" <?= ($development->show_statistics_sales_team == 0? 'checked="checked"': '');?> id="show_statistics_sales_team0" class="show_statistics_sales_team"><label for="show_statistics_sales_team0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show statistics for media agency:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_statistics_media_agency" value="1" <?= ($development->show_statistics_media_agency == 1? 'checked="checked"': '');?> id="show_statistics_media_agency1" class="show_statistics_media_agency"><label for="show_statistics_media_agency1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_statistics_media_agency" value="0" <?= ($development->show_statistics_media_agency == 0? 'checked="checked"': '');?> id="show_statistics_media_agency0" class="show_statistics_media_agency"><label for="show_statistics_media_agency0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show External Amenities:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_external_amenities" value="1" <?= ($development->show_external_amenities == 1? 'checked="checked"': '');?> id="show_external_amenities1"><label for="show_external_amenities1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_external_amenities" value="0" <?= ($development->show_external_amenities == 0? 'checked="checked"': '');?> id="show_external_amenities0"><label for="show_external_amenities0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Get Directions From External Amenities:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_get_directions_from_external_amenities" value="1" <?= ($development->show_get_directions_from_external_amenities == 1? 'checked="checked"': '');?> id="show_get_directions_from_external_amenities1" class="show_get_directions_from_external_amenities"><label for="show_get_directions_from_external_amenities1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_get_directions_from_external_amenities" value="0" <?= ($development->show_get_directions_from_external_amenities == 0? 'checked="checked"': '');?> id="show_get_directions_from_external_amenities0" class="show_get_directions_from_external_amenities"><label for="show_get_directions_from_external_amenities0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">External Amenity Category Icons - Margin Right:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="external_amenity_category_icons_margin_right" value="<?= $development->external_amenity_category_icons_margin_right;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Nearby Developments:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_nearby_developments" value="1" <?= ($development->show_nearby_developments == 1? 'checked="checked"': '');?> id="show_nearby_developments1" class="show_nearby_developments"><label for="show_nearby_developments1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_nearby_developments" value="0" <?= ($development->show_nearby_developments == 0? 'checked="checked"': '');?> id="show_nearby_developments0" class="show_nearby_developments"><label for="show_nearby_developments0"> Off </label>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show MAPOVIS Tour:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_tour" value="1" <?= ($development->show_tour == 1? 'checked="checked"': '');?> id="show_tour1" class="show_tour"><label for="show_tour1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_tour" value="0" <?= ($development->show_tour == 0? 'checked="checked"': '');?> id="show_tour0" class="show_tour"><label for="show_tour0"> Off </label>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Geolocation Button:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_geolocation_button" value="1" <?= ($development->show_geolocation_button == 1? 'checked="checked"': '');?> id="show_geolocation_button1" class="show_geolocation_button"><label for="show_geolocation_button1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_geolocation_button" value="0" <?= ($development->show_geolocation_button == 0? 'checked="checked"': '');?> id="show_geolocation_button0" class="show_geolocation_button"><label for="show_geolocation_button0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show House Matches:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_house_matches" value="1" <?= ($development->show_house_matches == 1? 'checked="checked"': '');?> id="show_house_matches1" class="show_house_matches"><label for="show_house_matches1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_house_matches" value="0" <?= ($development->show_house_matches == 0? 'checked="checked"': '');?> id="show_house_matches0" class="show_house_matches"><label for="show_house_matches0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Highlighted Amenities Icons:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_highlighted_amenities" value="1" <?= ($development->show_highlighted_amenities == 1? 'checked="checked"': '');?> id="show_highlighted_amenities1" class="show_highlighted_amenities"><label for="show_highlighted_amenities1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_highlighted_amenities" value="0" <?= ($development->show_highlighted_amenities == 0? 'checked="checked"': '');?> id="show_highlighted_amenities0" class="show_highlighted_amenities"><label for="show_highlighted_amenities0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Internal Amenity Icons >= Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="show_internal_amenity_icon_at_and_above_zoom_level" value="<?= $development->show_internal_amenity_icon_at_and_above_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Radius Circle:</label>
										<div class="col-lg-5">
											<input type="radio" name="radius_circle" value="1" <?= ($development->radius_circle == 1? 'checked="checked"': '');?> id="radius_circle1" class="radius_circle"><label for="radius_circle1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="radius_circle" value="0" <?= ($development->radius_circle == 0? 'checked="checked"': '');?> id="radius_circle0" class="radius_circle"><label for="radius_circle0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Radius Circle Image URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="radius_circle_overlay_image" value="<?= $development->radius_circle_overlay_image;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Lot Search:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_lot_search" value="1" <?= ($development->show_lot_search == 1? 'checked="checked"': '');?> id="show_lot_search1" class="show_lot_search"><label for="show_lot_search1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_lot_search" value="0" <?= ($development->show_lot_search == 0? 'checked="checked"': '');?> id="show_lot_search0" class="show_lot_search"><label for="show_lot_search0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Zoom To:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_zoom_to" value="1" <?= ($development->show_zoom_to == 1? 'checked="checked"': '');?> id="show_zoom_to1" class="show_zoom_to"><label for="show_zoom_to1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_zoom_to" value="0" <?= ($development->show_zoom_to == 0? 'checked="checked"': '');?> id="show_zoom_to0" class="show_zoom_to"><label for="show_zoom_to0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Default Zoom Level:</label>
										<div class="col-lg-2">
											<input type="text" class="form-control" name="default_zoom_level" value="<?= $development->default_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Google Street View:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_street_view" value="1" <?= ($development->show_street_view == 1? 'checked="checked"': '');?> id="show_street_view1" class="show_street_view"><label for="show_street_view1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_street_view" value="0" <?= ($development->show_street_view == 0? 'checked="checked"': '');?> id="show_street_view0" class="show_street_view"><label for="show_street_view0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Satellite View:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_satellite" value="1" <?= ($development->show_satellite == 1? 'checked="checked"': '');?> id="show_satellite1" class="show_satellite"><label for="show_satellite1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_satellite" value="0" <?= ($development->show_satellite == 0? 'checked="checked"': '');?> id="show_satellite0" class="show_satellite"><label for="show_satellite0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">House & Land View:</label>
										<div class="col-lg-4">
											<select name="house_and_land_view" id="house_and_land_view" class="form-control">
												<option value="">-- Default View --</option>
												<?php foreach($house_and_land_views as $view_name):?>
												<option value="<?= $view_name;?>" <?= ($view_name == $development->house_and_land_view)? 'selected="selected"': '';?>><?= $view_name;?></option>
												<?php endforeach;?>
											</select>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label">House & Land - Multiple Images:</label>
										<div class="col-lg-5">
											<input type="radio" name="hnl_multi_images" value="1" <?= ($development->hnl_multi_images == 1? 'checked="checked"': '');?> id="hnl_multi_images1" class="hnl_multi_images"><label for="hnl_multi_images1">&nbsp; Yes </label> &nbsp;&nbsp;
											<input type="radio" name="hnl_multi_images" value="0" <?= ($development->hnl_multi_images == 0? 'checked="checked"': '');?> id="hnl_multi_images0" class="hnl_multi_images"><label for="hnl_multi_images0">&nbsp; No </label>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group" id="house_land_order_by_field" <?= ($development->house_and_land_view == '')? 'style="display:none"': '';?>>
										<label class="col-lg-3 control-label" for="required">House & Land Order By:</label>
										<div class="col-lg-4">
											<select name="house_land_order_by" class="form-control">
												<?php foreach($house_land_order_bys as $house_land_order_by):?>
												<option value="<?= $house_land_order_by;?>" <?= ($house_land_order_by == $development->house_land_order_by)? 'selected="selected"': '';?>><?= ucwords(str_replace('_', ' ', $house_land_order_by));?></option>
												<?php endforeach;?>
											</select>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Land View:</label>
										<div class="col-lg-4">
											<select name="land_view" id="land_view" class="form-control">
												<option value="">-- Default View --</option>
												<?php foreach($land_views as $view_name):?>
												<option value="<?= $view_name;?>" <?= ($view_name == $development->land_view)? 'selected="selected"': '';?>><?= $view_name;?></option>
												<?php endforeach;?>
											</select>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Land View - Show Lots :</label>
										<div class="col-lg-4">
											<input type="radio" name="land_show_sold_lots" value="1" <?= ($development->land_show_sold_lots == 1? 'checked="checked"': '');?> id="land_show_sold_lots1" class="land_show_sold_lots"><label for="land_show_sold_lots1">&nbsp; Available &amp; Sold </label> &nbsp;&nbsp;
											<input type="radio" name="land_show_sold_lots" value="0" <?= ($development->land_show_sold_lots == 0? 'checked="checked"': '');?> id="land_show_sold_lots0" class="land_show_sold_lots"><label for="land_show_sold_lots0">&nbsp; Available Only </label>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group" id="land_order_by_field" <?= ($development->land_view == '')? 'style="display:none"': '';?>>
										<label class="col-lg-3 control-label" for="required">Land Order By:</label>
										<div class="col-lg-4">
											<select name="land_order_by" class="form-control">
												<?php foreach($house_land_order_bys as $land_order_by):?>
												<option value="<?= $land_order_by;?>" <?= ($land_order_by == $development->land_order_by)? 'selected="selected"': '';?>><?= ucwords(str_replace('_', ' ', $land_order_by));?></option>
												<?php endforeach;?>
											</select>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Land PDF View:</label>
										<div class="col-lg-4">
											<select name="land_pdf_view" id="land_pdf_view" class="form-control">
												<option value="">-- Default View --</option>
												<?php foreach($land_pdf_views as $view_name):?>
												<option value="<?= $view_name;?>" <?= ($view_name == $development->land_pdf_view)? 'selected="selected"': '';?>><?= $view_name;?></option>
												<?php endforeach;?>
											</select>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group" id="land_pdf_order_by_field" <?= ($development->land_pdf_view == '')? 'style="display:none"': '';?>>
										<label class="col-lg-3 control-label" for="required">Land PDF Order By:</label>
										<div class="col-lg-4">
											<select name="land_pdf_order_by" class="form-control">
												<?php foreach($house_land_order_bys as $land_order_by):?>
												<option value="<?= $land_order_by;?>" <?= ($land_order_by == $development->land_pdf_order_by)? 'selected="selected"': '';?>><?= ucwords(str_replace('_', ' ', $land_order_by));?></option>
												<?php endforeach;?>
											</select>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Template View:</label>
										<div class="col-lg-4">
											<select name="template_view" id="template_view" class="form-control">
												<option value="">-- Default View --</option>
												<?php foreach($template_views as $view_name):?>
												<option value="<?= $view_name;?>" <?= ($view_name == $development->template_view)? 'selected="selected"': '';?>><?= $view_name;?></option>
												<?php endforeach;?>
											</select>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Slider Skin Image:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="slider_skin_image" value="<?= $development->slider_skin_image;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Overlay Filename for Ipad:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="overlay_filename_ipad" value="<?= $development->overlay_filename_ipad;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Mapovis Not Supported URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="mapovis_not_supported_url" value="<?= $development->mapovis_not_supported_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Type Kit ID or Google Font URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="custom_font_markup" value="<?= $development->custom_font_markup;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Custom Google Font Name:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="custom_google_font_name" value="<?= $development->custom_google_font_name;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Custom Typekit Font Name:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="custom_typekit_font_name" value="<?= $development->custom_typekit_font_name;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Font Size:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="font_size" value="<?= $development->font_size;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Tooltip Font Size:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="tooltip_font_size" value="<?= $development->tooltip_font_size;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Custom CSS Override URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="custom_css_override_url" value="<?= $development->custom_css_override_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">MAPOVIS Not Supported (old browser) URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="mapovis_not_supported_browser_url" value="<?= $development->mapovis_not_supported_browser_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Search for Nearby Places Placeholder Text:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="search_for_nearby_place_placeholder_text" value="<?= $development->search_for_nearby_place_placeholder_text;?>" maxlength="40">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Back To Button:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="back_to_button" value="<?= $development->back_to_button;?>" maxlength="25">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">MAPOVIS Website Link:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="mapovis_website_link" value="<?= $development->mapovis_website_link;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sort House Matches By:</label>
										<div class="col-lg-4">
											<select name="sort_house_matches_by" class="form-control">
												<?php foreach($house_match_sort_option as $option):?>
												<option value="<?= $option;?>" <?= ($option == $development->sort_house_matches_by)? 'selected="selected"': '';?>><?= ucwords(str_replace('_', ' ', $option));?></option>
												<?php endforeach;?>
											</select>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">White List Staging Domain:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="staging_domain" value="<?= $development->staging_domain;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Development Icon Width:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="developmenticon_width" value="<?= $development->developmenticon_width;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Development Icon Height:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="developmenticon_height" value="<?= $development->developmenticon_height ;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Directions Search - Decrease Zoom Level By:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="directions_search_zoom_level_decrease_by" value="<?= $development->directions_search_zoom_level_decrease_by;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Lot Details Template File:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="LotDetailsTemplate" value="<?= $development->LotDetailsTemplate;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Form Code:</label>
										<div class="col-lg-8">
											<textarea style="height: 200px;" class="form-control" id="form_code" name="form_code"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->form_code);?></textarea>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Public Transport Search:</label>
										<div class="col-lg-4">
											<input type="radio" name="show_public_transport_search" value="1" <?= ($development->show_public_transport_search == 1? 'checked="checked"': '');?> id="show_public_transport_search1" class="show_public_transport_search"><label for="show_public_transport_search1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_public_transport_search" value="0" <?= ($development->show_public_transport_search == 0? 'checked="checked"': '');?> id="show_public_transport_search0" class="show_public_transport_search"><label for="show_public_transport_search0"> Off </label>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Land PDF Font Family CSS:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="land_pdf_fontfamily_css" value="<?= $development->land_pdf_fontfamily_css;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Price List Email Template:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="price_list_email_template" value="<?= $development->price_list_email_template;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Price List Reply To Email:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="price_list_reply_to_email" value="<?= $development->price_list_reply_to_email;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Linked Development Logo:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="linked_development_logo" value="<?= $development->linked_development_logo;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Max Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="max_zoom_level" value="<?= $development->max_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Max Zoom Level (mobile):</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="max_zoom_level_mobile" value="<?= $development->max_zoom_level_mobile;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Development Icon Show at Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="developmenticon_show_at_zoom_level" value="<?= $development->developmenticon_show_at_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Development Icon Hide at Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="developmenticon_hide_at_zoom_level" value="<?= $development->developmenticon_hide_at_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Footer Text Icon Highlight:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="footer_text_icon_highlight" value="<?= $development->footer_text_icon_highlight;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Custom House Facade Width:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="custom_house_facade_width" value="<?= $development->custom_house_facade_width;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Custom House Facade Height:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="custom_house_facade_height" value="<?= $development->custom_house_facade_height;?>">
										</div>
									</div><!-- End .form-group-->
								</div>


								<div id="tabs-2">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Start HEX Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="start_hex_colour" name="start_hex_colour" value="#<?= $development->start_hex_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="start_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->start_hex_colour;?>;background-color: #<?= $development->start_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">End HEX Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="end_hex_colour" name="end_hex_colour" value="#<?= $development->end_hex_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<div id="end_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->end_hex_colour;?>;background-color: #<?= $development->end_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Text HEX Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="text_hex_colour" name="text_hex_colour" value="#<?= $development->text_hex_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<div id="text_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->text_hex_colour;?>;background-color: #<?= $development->text_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Lightbox Border HEX Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="lightbox_border_colour" name="lightbox_border_colour" value="#<?= $development->lightbox_border_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<div id="lightbox_border_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->lightbox_border_colour;?>;background-color: #<?= $development->lightbox_border_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Lightbox Highlight HEX Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="lightbox_highlight_colour" name="lightbox_highlight_colour" value="#<?= $development->lightbox_highlight_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<div id="lightbox_highlight_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->lightbox_highlight_colour;?>;background-color: #<?= $development->lightbox_highlight_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Zoom to Stage - Buttons:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="zoom_to_stage_button_hex" name="zoom_to_stage_button_hex" value="#<?= $development->zoom_to_stage_button_hex;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<div id="zoom_to_stage_button_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->zoom_to_stage_button_hex;?>;background-color: #<?= $development->zoom_to_stage_button_hex;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Zoom to Stage - Header:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="zoom_to_stage_header_hex" name="zoom_to_stage_header_hex" value="#<?= $development->zoom_to_stage_header_hex;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<div id="zoom_to_stage_header_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->zoom_to_stage_header_hex;?>;background-color: #<?= $development->zoom_to_stage_header_hex;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Loading Gradient Top:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" name="loading_gradient_top_hex" value="#<?= $development->loading_gradient_top_hex;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<div id="loading_gradient_top_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->loading_gradient_top_hex;?>;background-color: #<?= $development->loading_gradient_top_hex;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Loading Gradient Middle:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" name="loading_gradient_middle_hex" value="#<?= $development->loading_gradient_middle_hex;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<div id="loading_gradient_middle_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->loading_gradient_middle_hex;?>;background-color: #<?= $development->loading_gradient_middle_hex;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Loading Gradient Bottom:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" name="loading_gradient_bottom_hex" value="#<?= $development->loading_gradient_bottom_hex;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<div id="loading_gradient_bottom_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->loading_gradient_bottom_hex;?>;background-color: #<?= $development->loading_gradient_bottom_hex;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Dialog Start Hex:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="dialog_start_hex" name="dialog_start_hex" value="#<?= $development->dialog_start_hex;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="dialog_start_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->dialog_start_hex;?>;background-color: #<?= $development->dialog_start_hex;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Dialog End Hex:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="dialog_end_hex" name="dialog_end_hex" value="#<?= $development->dialog_end_hex;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="dialog_end_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->dialog_end_hex;?>;background-color: #<?= $development->dialog_end_hex;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Dialog Close Button - Background Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="dialog_close_button_background_hex" name="dialog_close_button_background_hex" value="#<?= $development->dialog_close_button_background_hex;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="dialog_close_button_background_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->dialog_close_button_background_hex;?>;background-color: #<?= $development->dialog_close_button_background_hex;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Dialog Close Button Foreground Hex:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="dialog_close_button_foreground_hex" name="dialog_close_button_foreground_hex" value="#<?= $development->dialog_close_button_foreground_hex;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="dialog_close_button_foreground_hex_disp" style="width:50px !important;border: 2px solid #<?= $development->dialog_close_button_foreground_hex;?>;background-color: #<?= $development->dialog_close_button_foreground_hex;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Panel Close Button Background Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="panel_close_background_colour" name="panel_close_background_colour" value="#<?= $development->panel_close_background_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="panel_close_background_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->panel_close_background_colour;?>;background-color: #<?= $development->panel_close_background_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Panel Close Button Foreground Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="panel_close_foreground_colour" name="panel_close_foreground_colour" value="#<?= $development->panel_close_foreground_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="panel_close_foreground_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->panel_close_foreground_colour;?>;background-color: #<?= $development->panel_close_foreground_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">House & Land Package - Download PDF Button:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="house_land_package_downloadpdf_button" name="house_land_package_downloadpdf_button" value="#<?= $development->house_land_package_downloadpdf_button;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="house_land_package_downloadpdf_button_disp" style="width:50px !important;border: 2px solid #<?= $development->house_land_package_downloadpdf_button;?>;background-color: #<?= $development->house_land_package_downloadpdf_button;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">House & Land Package - Download PDF Button Highlight:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="house_land_package_downloadpdf_button_highlight" name="house_land_package_downloadpdf_button_highlight" value="#<?= $development->house_land_package_downloadpdf_button_highlight;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="house_land_package_downloadpdf_button_highlight_disp" style="width:50px !important;border: 2px solid #<?= $development->house_land_package_downloadpdf_button_highlight;?>;background-color: #<?= $development->house_land_package_downloadpdf_button_highlight;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">House & Land Package - Enquire Now Button:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="house_land_package_enquirenow_button" name="house_land_package_enquirenow_button" value="#<?= $development->house_land_package_enquirenow_button;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="house_land_package_enquirenow_button_disp" style="width:50px !important;border: 2px solid #<?= $development->house_land_package_enquirenow_button;?>;background-color: #<?= $development->house_land_package_enquirenow_button;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">House & Land Package - Enquire Now Button Highlight:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="house_land_package_enquirenow_button_highlight" name="house_land_package_enquirenow_button_highlight" value="#<?= $development->house_land_package_enquirenow_button_highlight;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="house_land_package_enquirenow_button_highlight_disp" style="width:50px !important;border: 2px solid #<?= $development->house_land_package_enquirenow_button_highlight;?>;background-color: #<?= $development->house_land_package_enquirenow_button_highlight;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Google Control Buttons Background:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="google_control_buttons_background" name="google_control_buttons_background" value="#<?= $development->google_control_buttons_background;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="google_control_buttons_background_disp" style="width:50px !important;border: 2px solid #<?= $development->google_control_buttons_background;?>;background-color: #<?= $development->google_control_buttons_background;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Google Control Buttons Foreground:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="google_control_buttons_foreground" name="google_control_buttons_foreground" value="#<?= $development->google_control_buttons_foreground;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="google_control_buttons_foreground_disp" style="width:50px !important;border: 2px solid #<?= $development->google_control_buttons_foreground;?>;background-color: #<?= $development->google_control_buttons_foreground;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Tooltip Background Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="tooltip_background_colour" name="tooltip_background_colour" value="#<?= $development->tooltip_background_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="tooltip_background_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->tooltip_background_colour;?>;background-color: #<?= $development->tooltip_background_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Tooltip Text Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="tooltip_text_colour" name="tooltip_text_colour" value="#<?= $development->tooltip_text_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="tooltip_text_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->tooltip_text_colour;?>;background-color: #<?= $development->tooltip_text_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Directions From External Amenity Text Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="get_directions_from_external_amenities_text_colour" name="get_directions_from_external_amenities_text_colour" value="#<?= $development->get_directions_from_external_amenities_text_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="get_directions_from_external_amenities_text_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->get_directions_from_external_amenities_text_colour;?>;background-color: #<?= $development->get_directions_from_external_amenities_text_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">PDF Footer Header Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="pdf_footer_header_colour" name="pdf_footer_header_colour" value="#<?= $development->pdf_footer_header_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="pdf_footer_header_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->pdf_footer_header_colour;?>;background-color: #<?= $development->pdf_footer_header_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">PDF Footer Row Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="pdf_footer_row_colour" name="pdf_footer_row_colour" value="#<?= $development->pdf_footer_row_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="pdf_footer_row_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->pdf_footer_row_colour;?>;background-color: #<?= $development->pdf_footer_row_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Directions From External Amenity Text Highlight Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="get_directions_from_external_amenities_text_highlight_colour" name="get_directions_from_external_amenities_text_highlight_colour" value="#<?= $development->get_directions_from_external_amenities_text_highlight_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="get_directions_from_external_amenities_text_highlight_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->get_directions_from_external_amenities_text_highlight_colour;?>;background-color: #<?= $development->get_directions_from_external_amenities_text_highlight_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Directions Summary Background Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="directions_summary_background_colour" name="directions_summary_background_colour" value="#<?= $development->directions_summary_background_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="directions_summary_background_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->directions_summary_background_colour;?>;background-color: #<?= $development->directions_summary_background_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Directions Summary Text Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="directions_summary_text_colour" name="directions_summary_text_colour" value="#<?= $development->directions_summary_text_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="directions_summary_text_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->directions_summary_text_colour;?>;background-color: #<?= $development->directions_summary_text_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Get Direction Button Background Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="directions_get_direction_button_background_colour" name="directions_get_direction_button_background_colour" value="#<?= $development->directions_get_direction_button_background_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="directions_get_direction_button_background_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->directions_get_direction_button_background_colour;?>;background-color: #<?= $development->directions_get_direction_button_background_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Get Direction Button Text Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="directions_get_direction_button_text_colour" name="directions_get_direction_button_text_colour" value="#<?= $development->directions_get_direction_button_text_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="directions_get_direction_button_text_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->directions_get_direction_button_text_colour;?>;background-color: #<?= $development->directions_get_direction_button_text_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Nearby Places Sidebar Icon Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="nearby_places_sidebar_icon_colour" name="nearby_places_sidebar_icon_colour" value="#<?= $development->nearby_places_sidebar_icon_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="nearby_places_sidebar_icon_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->nearby_places_sidebar_icon_colour;?>;background-color: #<?= $development->nearby_places_sidebar_icon_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Nearby Places Sidebar Icon Colour Selected:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="nearby_places_sidebar_icon_colour_selected" name="nearby_places_sidebar_icon_colour_selected" value="#<?= $development->nearby_places_sidebar_icon_colour_selected;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="nearby_places_sidebar_icon_colour_selected_disp" style="width:50px !important;border: 2px solid #<?= $development->nearby_places_sidebar_icon_colour_selected;?>;background-color: #<?= $development->nearby_places_sidebar_icon_colour_selected;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">No Gradient HEX Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="no_gradient_hex_colour" name="no_gradient_hex_colour" value="#<?= $development->no_gradient_hex_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="no_gradient_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->no_gradient_hex_colour;?>;background-color: #<?= $development->no_gradient_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Header Footer Button Background Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="header_footer_button_bg_hex_colour" name="header_footer_button_bg_hex_colour" value="#<?= $development->header_footer_button_bg_hex_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="header_footer_button_bg_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->header_footer_button_bg_hex_colour;?>;background-color: #<?= $development->header_footer_button_bg_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Header Footer Button Background Colour Highlight:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="header_footer_button_bg_hex_colour_highlight" name="header_footer_button_bg_hex_colour_highlight" value="#<?= $development->header_footer_button_bg_hex_colour_highlight;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="header_footer_button_bg_hex_colour_highlight_disp" style="width:50px !important;border: 2px solid #<?= $development->header_footer_button_bg_hex_colour_highlight;?>;background-color: #<?= $development->header_footer_button_bg_hex_colour_highlight;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Lot Search Button Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="lot_search_button_hex_colour" name="lot_search_button_hex_colour" value="#<?= $development->lot_search_button_hex_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="lot_search_button_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->lot_search_button_hex_colour;?>;background-color: #<?= $development->lot_search_button_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Tour Buttons Background Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="tour_buttons_background_hex_colour" name="tour_buttons_background_hex_colour" value="#<?= $development->tour_buttons_background_hex_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="tour_buttons_background_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->tour_buttons_background_hex_colour;?>;background-color: #<?= $development->tour_buttons_background_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 
								</div>


								<div id="tabs-3">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Mobile Center Coordinates:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input name="mobile_centre_latitude" type="text" class="form-control" style="width: 200px !important;" id="mobile_centre-latitude"
														value="<?= (float)$development->mobile_centre_latitude;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<input name="mobile_centre_longitude" type="text" class="form-control" style="width: 200px !important;" id="mobile_centre-longitude"
														value="<?= (float)$development->mobile_centre_longitude;?>">
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Mobile Start Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="mobile_start_zoom_level" value="<?= $development->mobile_start_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Mobile Old iOS Link:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="mobile_old_ios_link" value="<?= $development->mobile_old_ios_link;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Mobile Old Android Link:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="mobile_old_android_link" value="<?= $development->mobile_old_android_link;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Default Zoom Level (mobile):</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="default_zoom_level_mobile" value="<?= $development->default_zoom_level_mobile;?>">
										</div>
									</div><!-- End .form-group-->
								</div>


								<div id="tabs-4">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Polygon Stroke Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="mapovis_mini_polygon_strokeColor" name="mapovis_mini_polygon_strokeColor" value="#<?= $development->mapovis_mini_polygon_strokeColor;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="mapovis_mini_polygon_strokeColor_disp" style="width:50px !important;border: 2px solid #<?= $development->mapovis_mini_polygon_strokeColor;?>;background-color: #<?= $development->mapovis_mini_polygon_strokeColor;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Polygon Stroke Opacity:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="mapovis_mini_polygon_strokeOpacity" value="<?= $development->mapovis_mini_polygon_strokeOpacity;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Polygon Stroke Weight:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="mapovis_mini_polygon_strokeWeight" value="<?= $development->mapovis_mini_polygon_strokeWeight;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Polygon Fill Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="mapovis_mini_polygon_fillColor" name="mapovis_mini_polygon_fillColor" value="#<?= $development->mapovis_mini_polygon_fillColor;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="mapovis_mini_polygon_fillColor_disp" style="width:50px !important;border: 2px solid #<?= $development->mapovis_mini_polygon_fillColor;?>;background-color: #<?= $development->mapovis_mini_polygon_fillColor;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Polygon Fill Opacity:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="mapovis_mini_polygon_fillOpacity" value="<?= $development->mapovis_mini_polygon_fillOpacity;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Polygon Fill Colour - Mouseover:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="mapovis_mini_polygon_fillColor_mouseover" name="mapovis_mini_polygon_fillColor_mouseover" value="#<?= $development->mapovis_mini_polygon_fillColor_mouseover;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="mapovis_mini_polygon_fillColor_mouseover_disp" style="width:50px !important;border: 2px solid #<?= $development->mapovis_mini_polygon_fillColor_mouseover;?>;background-color: #<?= $development->mapovis_mini_polygon_fillColor_mouseover;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Open Master Plan Button URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="mapovis_mini_open_interactive_masterplan_button_url" value="<?= $development->mapovis_mini_open_interactive_masterplan_button_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Open Master Plan Button Width:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="mapovis_mini_open_interactive_masterplan_button_width" value="<?= $development->mapovis_mini_open_interactive_masterplan_button_width;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Open Master Plan Button Height:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="mapovis_mini_open_interactive_masterplan_button_height" value="<?= $development->mapovis_mini_open_interactive_masterplan_button_height ;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Default Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="mapovismini_default_zoom_level" value="<?= $development->mapovismini_default_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Polygon Stroke Color Mouseover:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="mapovis_mini_polygon_strokeColor_mouseover" name="mapovis_mini_polygon_strokeColor_mouseover" value="#<?= $development->mapovis_mini_polygon_strokeColor_mouseover;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="mapovis_mini_polygon_strokeColor_mouseover_disp" style="width:50px !important;border: 2px solid #<?= $development->mapovis_mini_polygon_strokeColor_mouseover;?>;background-color: #<?= $development->mapovis_mini_polygon_strokeColor_mouseover;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Polygon Stroke Opacity Mouseover:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="mapovis_mini_polygon_strokeOpacity_mouseover" value="<?= $development->mapovis_mini_polygon_strokeOpacity_mouseover;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Polygon Stroke Weight Mouseover:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="mapovis_mini_polygon_strokeWeight_mouseover" value="<?= $development->mapovis_mini_polygon_strokeWeight_mouseover;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Office Marker Show At:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="mapovis_mini_sales_office_marker_show_at" value="<?= $development->mapovis_mini_sales_office_marker_show_at;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Office Marker Hide At:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="mapovis_mini_sales_office_marker_hide_at" value="<?= $development->mapovis_mini_sales_office_marker_hide_at;?>">
										</div>
									</div><!-- End .form-group-->
								</div>


								<div id="tabs-5">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Nearby Search Maximum Radius (meters):</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="nearby_search_radius" value="<?= $development->nearby_search_radius;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Map Marker (default) Icon URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="nearby_places_map_marker_default" value="<?= $development->nearby_places_map_marker_default;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Map Marker (selected) Icon URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="nearby_places_map_marker_selected" value="<?= $development->nearby_places_map_marker_selected;?>">
										</div>
									</div><!-- End .form-group-->
								</div>


								<div id="tabs-6">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Icon URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="display_village_icon_url" value="<?= $development->display_village_icon_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Icon Width:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="display_village_icon_width" value="<?= $development->display_village_icon_width;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Icon Height:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="display_village_icon_height" value="<?= $development->display_village_icon_height;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Position:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input name="display_village_latitude" type="text" class="form-control" style="width: 200px !important;" id="display_village-latitude"
														value="<?= (float)$development->display_village_latitude;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<input name="display_village_longitude" type="text" class="form-control" style="width: 200px !important;" id="display_village-longitude"
														value="<?= (float)$development->display_village_longitude;?>">
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Centre:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input name="display_village_center_latitude" type="text" class="form-control" style="width: 200px !important;" id="display_village_center-latitude"
														value="<?= (float)$development->display_village_center_latitude;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
												 <div class="input-group">
														<input name="display_village_center_longitude" type="text" class="form-control" style="width: 200px !important;" id="display_village_center-longitude"
														value="<?= (float)$development->display_village_center_longitude;?>">
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Centre Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="display_village_centre_zoom_level" value="<?= $development->display_village_centre_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show at Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="display_village_show_at_zoom_level" value="<?= $development->display_village_show_at_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Hide at Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="display_village_hide_at_zoom_level" value="<?= $development->display_village_hide_at_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Iframe URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="display_village_iframe_url" value="<?= $development->display_village_iframe_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Display Iframe Scrollbar:</label>
										<div class="col-lg-5">
											<input type="radio" name="display_village_iframe_scrollbar" value="1" <?= ($development->display_village_iframe_scrollbar == 1? 'checked="checked"': '');?> id="display_village_iframe_scrollbar1" ><label for="display_village_iframe_scrollbar1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="display_village_iframe_scrollbar" value="0" <?= ($development->display_village_iframe_scrollbar == 0? 'checked="checked"': '');?> id="display_village_iframe_scrollbar0" ><label for="display_village_iframe_scrollbar0"> Off </label>
										</div>
									</div><!-- End .form-group-->
								</div>


								<div id="tabs-7">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Office Title:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="sales_office_title" name="sales_office_title" value="<?= $development->sales_office_title;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Office Address:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="sales_office_address" name="sales_office_address" value="<?= $development->sales_office_address;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Telephone Number:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="sales_telephone_number" name="sales_telephone_number" value="<?= $development->sales_telephone_number;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Local Sales Telephone Number:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="local_sales_telephone_number" name="local_sales_telephone_number" value="<?= $development->local_sales_telephone_number;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Email Address:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="sales_email_address" name="sales_email_address" value="<?= $development->sales_email_address;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Person Name:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="sales_person_name" name="sales_person_name" value="<?= $development->sales_person_name;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Office Opening Hours:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="sales_office_opening_house" name="sales_office_opening_house" value="<?= $development->sales_office_opening_house;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Office Googlemaps Directions Url:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="sales_office_googlemaps_directions_url" name="sales_office_googlemaps_directions_url" value="<?= $development->sales_office_googlemaps_directions_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Sales Office Coordinates:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input name="sales_office_latitude" type="text" class="form-control" style="width: 200px !important;" id="sales_office_latitude"
														value="<?= (float)$development->sales_office_latitude;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
													   <input name="sales_office_longitude" type="text" class="form-control" style="width: 200px !important;" id="sales_office_longitude"
													   value="<?= (float)$development->sales_office_longitude;?>">
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Office HTML:</label>
										<div class="col-lg-8">
											<textarea style="height: 200px;" class="form-control" id="sales_office_html" name="sales_office_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->sales_office_html);?></textarea>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Office Icon:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="sales_office_icon" value="<?= $development->sales_office_icon;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Icon Width:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="sales_office_icon_width" value="<?= $development->sales_office_icon_width;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Icon Height:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="sales_office_icon_height" value="<?= $development->sales_office_icon_height;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show at Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="sales_office_icon_show_at_zoom_level" value="<?= $development->sales_office_icon_show_at_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Hide at Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="sales_office_icon_hide_at_zoom_level" value="<?= $development->sales_office_icon_hide_at_zoom_level;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Office Amenity Polygon Coords:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="sales_office_amenity_polygon_coords" value="<?= $development->sales_office_amenity_polygon_coords;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="col-lg-11 alert alert-warning">If you want to use a completely custom design you can provide an IFrame.</div>
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Sales Office Iframe HTML:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="sales_office_iframe_html" value="<?= htmlspecialchars($development->sales_office_iframe_html, ENT_QUOTES);?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Scrollbar:</label>
										<div class="col-lg-5">
											<input type="radio" name="sales_office_iframe_scrollbar" value="1" <?= ($development->sales_office_iframe_scrollbar == 1? 'checked="checked"': '');?> id="sales_office_iframe_scrollbar1" class="sales_office_iframe_scrollbar"><label for="sales_office_iframe_scrollbar1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="sales_office_iframe_scrollbar" value="0" <?= ($development->sales_office_iframe_scrollbar == 0? 'checked="checked"': '');?> id="sales_office_iframe_scrollbar0" class="sales_office_iframe_scrollbar"><label for="sales_office_iframe_scrollbar0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 
								</div>


								<div id="tabs-8">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Border ON/OFF:</label>
										<div class="col-lg-5">
											<input type="radio" name="lot_polygon_border_show" value="1" <?= ($development->lot_polygon_border_show == 1? 'checked="checked"': '');?> id="lot_polygon_border_show1" class="lot_polygon_border_show"><label for="lot_polygon_border_show1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="lot_polygon_border_show" value="0" <?= ($development->lot_polygon_border_show == 0? 'checked="checked"': '');?> id="lot_polygon_border_show0" class="lot_polygon_border_show"><label for="lot_polygon_border_show0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Border Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="lot_polygon_border_colour" name="lot_polygon_border_colour" value="#<?= $development->lot_polygon_border_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="lot_polygon_border_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->lot_polygon_border_colour;?>;background-color: #<?= $development->lot_polygon_border_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="buttons">Border Colour(deposited):</label>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control change_colour" id="lot_polygon_border_colour_deposited" name="lot_polygon_border_colour_deposited" value="#<?= $development->lot_polygon_border_colour_deposited;?>">
                                                    </div><!-- /input-group -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <div id="lot_polygon_border_colour_deposited_disp" style="width:50px !important;border: 2px solid #<?= $development->lot_polygon_border_colour_deposited;?>;background-color: #<?= $development->lot_polygon_border_colour_deposited;?>">&nbsp;&nbsp;&nbsp;</div>
                                                    </div><!-- /input-group -->
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="buttons">Border Colour on Zoom To Lot:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="lot_polygon_border_colour_zoom_to_lot" name="lot_polygon_border_colour_zoom_to_lot" value="#<?= $development->lot_polygon_border_colour_zoom_to_lot;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="lot_polygon_border_colour_zoom_to_lot_disp" style="width:50px !important;border: 2px solid #<?= $development->lot_polygon_border_colour_zoom_to_lot;?>;background-color: #<?= $development->lot_polygon_border_colour_zoom_to_lot;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group--> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Border Weight:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="lot_polygon_border_weight" value="<?= $development->lot_polygon_border_weight;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Border Opacity:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="lot_polygon_border_opacity" value="<?= $development->lot_polygon_border_opacity;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Border at Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="lot_polygon_border_show_from_zoomlevel" value="<?= $development->lot_polygon_border_show_from_zoomlevel;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Hide Border at Zoom Level:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="lot_polygon_border_show_to_zoomlevel" value="<?= $development->lot_polygon_border_show_to_zoomlevel;?>">
										</div>
									</div><!-- End .form-group-->
								</div>


								<div id="tabs-9">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Custom Google Maps Style:</label>
										<div class="col-lg-8">
											<textarea style="height: 150px;" class="form-control" name="custom_googlemaps_style"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->custom_googlemaps_style);?></textarea>
										</div>
									</div><!-- End .form-group  --> 
								</div>


								<div id="tabs-10">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Facebook Sharing:</label>
										<div class="col-lg-5">
											<input type="radio" name="facebook_sharing" value="1" <?= ($development->facebook_sharing == 1? 'checked="checked"': '');?> id="facebook_sharing1" class="facebook_sharing"><label for="facebook_sharing1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="facebook_sharing" value="0" <?= ($development->facebook_sharing == 0? 'checked="checked"': '');?> id="facebook_sharing0" class="facebook_sharing"><label for="facebook_sharing0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Share House &amp; Land Package Name:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="facebook_share_houseland_package_name" value="<?= $development->facebook_share_houseland_package_name;?>">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Share House &amp; Land Package Caption:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="facebook_share_houseland_package_caption" value="<?= $development->facebook_share_houseland_package_caption;?>">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Share House &amp; Land Package Link:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="facebook_share_houseland_package_link" value="<?= $development->facebook_share_houseland_package_link;?>">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Share House &amp; Land Package Description:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="facebook_share_houseland_package_description" value="<?= $development->facebook_share_houseland_package_description;?>">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Share Lot Name:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="facebook_share_lot__name" value="<?= $development->facebook_share_lot__name;?>">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Share Lot Caption:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="facebook_share_lot_caption" value="<?= $development->facebook_share_lot_caption;?>">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Share Lot Link:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="facebook_share_lot_link" value="<?= $development->facebook_share_lot_link;?>">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Share Lot Picture:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="facebook_share_lot_picture" value="<?= $development->facebook_share_lot_picture;?>">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Share Lot Description:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="facebook_share_lot_description" value="<?= $development->facebook_share_lot_description;?>">
										</div>
									</div><!-- End .form-group  --> 
								</div>


								<div id="tabs-11">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Stage Plan PDF:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="stageplanpdf" name="stageplanpdf" value="<?= $development->stageplanpdf;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">MAPOVIS Generated PDF:</label>
										<div class="col-lg-5">
											<input type="radio" name="generated_pdf" value="1" <?= ($development->generated_pdf == 1? 'checked="checked"': '');?> id="generated_pdf1" class="generated_pdf"><label for="generated_pdf1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="generated_pdf" value="0" <?= ($development->generated_pdf == 0? 'checked="checked"': '');?> id="generated_pdf0" class="generated_pdf"><label for="generated_pdf0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div id="lots_pdf_file_div" class="form-group" style="display:<?= ($development->generated_pdf == 1)? 'inline': 'none';?>">
										<label class="col-lg-3 control-label">PDF File:</label>
										<div class="col-lg-9">
											<div id="lots_pdf_file">
												<input type="file" name="lots_pdf_file" id="lots_pdf_file_field" value="" accept="application/pdf" >
												<?php if($development->lots_pdf_file):?>
													<span id="current_file">
														<a href="<?= base_url().'../mpvs/templates/dev_pdfs/'.$development->lots_pdf_file;?>" title="Download File" download><span class="icon16 icomoon-icon-file-pdf"></span></a>
													</span>
													</br></br>
												<?php endif;?>
											</div>
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Download PDF:</label>
										<div class="col-lg-5">
											<input type="radio" name="show_download_pdf" value="1" <?= ($development->show_download_pdf == 1? 'checked="checked"': '');?> id="show_download_pdf1" class="show_download_pdf"><label for="show_download_pdf1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_download_pdf" value="0" <?= ($development->show_download_pdf == 0? 'checked="checked"': '');?> id="show_download_pdf0" class="show_download_pdf"><label for="show_download_pdf0"> Off </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">PDF Link:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="pdf_link" name="pdf_link" value="<?= $development->pdf_link;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Lots PDF - HTML Header:</label>
										<div class="col-lg-8">
											<textarea style="height: 200px;" class="form-control" id="pdf_header_html" name="pdf_header_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->pdf_header_html);?></textarea>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Lots PDF - Header Margin:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="pdf_header_margin" value="<?= $development->pdf_header_margin;?>">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Lots PDF - HTML Footer:</label>
										<div class="col-lg-8">
											<textarea style="height: 200px;" class="form-control" id="pdf_footer_html" name="pdf_footer_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->pdf_footer_html);?></textarea>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Lots PDF - Footer Margin:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="pdf_footer_margin" value="<?= $development->pdf_footer_margin;?>">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Builder PDF - HTML Header:</label>
										<div class="col-lg-8">
											<textarea style="height: 200px;" class="form-control" id="builder_pdf_header_html" name="builder_pdf_header_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->builder_pdf_header_html);?></textarea>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Builder PDF - Header Margin:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="builder_pdf_header_margin" value="<?= $development->builder_pdf_header_margin;?>">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Builder PDF - HTML Footer:</label>
										<div class="col-lg-8">
											<textarea style="height: 200px;" class="form-control" id="builder_pdf_footer_html" name="builder_pdf_footer_html"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->builder_pdf_footer_html);?></textarea>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Builder PDF - Footer Margin:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="builder_pdf_footer_margin" value="<?= $development->builder_pdf_footer_margin;?>">
										</div>
									</div><!-- End .form-group  --> 
								</div>


								<div id="tabs-12">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Terms and Conditions:</label>
										<div class="col-lg-8">
											<textarea style="height: 200px;" class="form-control" id="terms_and_conditions" name="terms_and_conditions"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->terms_and_conditions);?></textarea>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Builder - Terms and Conditions:</label>
										<div class="col-lg-8">
											<textarea style="height: 200px;" class="form-control" id="terms_and_conditions_builder" name="terms_and_conditions_builder"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->terms_and_conditions_builder);?></textarea>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Builder - Disclaimer:</label>
										<div class="col-lg-8">
											<textarea style="height: 200px;" class="form-control" id="diclaimer_builder" name="diclaimer_builder"><?= str_replace('</textarea>', '&lt;/textarea&gt;', $development->diclaimer_builder);?></textarea>
										</div>
									</div><!-- End .form-group-->
								</div>


								<div id="tabs-13">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Compass:</label>
										<div class="col-lg-4">
											<input type="radio" name="show_compass" value="1" <?= ($development->show_compass == 1? 'checked="checked"': '');?> id="show_compass1" class="show_compass"><label for="show_compass1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_compass" value="0" <?= ($development->show_compass == 0? 'checked="checked"': '');?> id="show_compass0" class="show_compass"><label for="show_compass0"> Off </label>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Show Compass (mobile):</label>
										<div class="col-lg-4">
											<input type="radio" name="show_compass_mobile" value="1" <?= ($development->show_compass_mobile == 1? 'checked="checked"': '');?> id="show_compass_mobile1" class="show_compass_mobile"><label for="show_compass_mobile1"> On </label> &nbsp;&nbsp;
											<input type="radio" name="show_compass_mobile" value="0" <?= ($development->show_compass_mobile == 0? 'checked="checked"': '');?> id="show_compass_mobile0" class="show_compass_mobile"><label for="show_compass_mobile0"> Off </label>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Compass Width:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="compass_width" value="<?= $development->compass_width;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Compass Height:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="compass_height" value="<?= $development->compass_height;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Compass URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="compass_url" value="<?= $development->compass_url;?>">
										</div>
									</div><!-- End .form-group-->
								</div>


								<div id="tabs-14">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Default Cluster Circle URL:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" name="default_cluster_circle_url" value="<?= $development->default_cluster_circle_url;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Default Cluster Circle Width:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="default_cluster_circle_width" value="<?= $development->default_cluster_circle_width;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Default Cluster Circle Height:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="default_cluster_circle_height" value="<?= $development->default_cluster_circle_height;?>">
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Default Cluster Text Hex Colour:</label>
										<div class="col-lg-8">
											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<input type="text" class="form-control change_colour" id="default_cluster_text_hex_colour" name="default_cluster_text_hex_colour" value="#<?= $development->default_cluster_text_hex_colour;?>">
													</div><!-- /input-group -->
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<div id="default_cluster_text_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->default_cluster_text_hex_colour;?>;background-color: #<?= $development->default_cluster_text_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group-->

									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Default Cluster Text Size:</label>
										<div class="col-lg-1">
											<input type="text" class="form-control" name="default_cluster_text_size" value="<?= $development->default_cluster_text_size;?>">
										</div>
									</div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Multiple Developments Cluster Circle URL:</label>
                                        <div class="col-lg-1">
                                            <input type="text" class="form-control" name="multiple_developments_cluster_circle_url" value="<?= $development->multiple_developments_cluster_circle_url;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Multiple Developments Cluster Circle Width:</label>
                                        <div class="col-lg-1">
                                            <input type="text" class="form-control" name="multiple_developments_cluster_circle_width" value="<?= $development->multiple_developments_cluster_circle_width;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Multiple Developments Cluster Circle Height:</label>
                                        <div class="col-lg-1">
                                            <input type="text" class="form-control" name="multiple_developments_cluster_circle_height" value="<?= $development->multiple_developments_cluster_circle_height;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Multiple Developments Cluster Text HEX Colour:</label>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control change_colour" id="multiple_developments_cluster_text_hex_colour" name="multiple_developments_cluster_text_hex_colour" value="#<?= $development->multiple_developments_cluster_text_hex_colour;?>">
                                                    </div><!-- /input-group -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <div id="multiple_developments_cluster_text_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $development->multiple_developments_cluster_text_hex_colour;?>;background-color: #<?= $development->multiple_developments_cluster_text_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
                                                    </div><!-- /input-group -->
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Multiple Developments Cluster Circle Text Size:</label>
                                        <div class="col-lg-1">
                                            <input type="text" class="form-control" name="multiple_developments_cluster_text_size" value="<?= $development->multiple_developments_cluster_text_size;?>">
                                        </div>
                                    </div><!-- End .form-group-->
								</div>
                                
                                <div id="tabs-15">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Development Status:</label>
                                        <div class="col-lg-4">
                                            <select name="active" class="form-control">
                                                <option value="1" <?= ($development->active == 1)? 'selected="selected"': '';?>>Active</option>
                                                <option value="0" <?= ($development->active == 0)? 'selected="selected"': '';?>>Inactive</option>
                                            </select>
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Update Underway:</label>
                                        <div class="col-lg-4">
                                          <input type="radio" name="update_underway" value="1" <?= ($development->update_underway == 1? 'checked="checked"': '');?> id="update_underway1" class="update_underway"><label for="update_underway1"> On </label> &nbsp;&nbsp;
                                          <input type="radio" name="update_underway" value="0" <?= ($development->update_underway == 0? 'checked="checked"': '');?> id="update_underway0" class="update_underway"><label for="update_underway0"> Off </label>
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Update Message:</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="update_message" value="<?= $development->update_message;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                </div>
                                
                                <div id="tabs-16">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Interactive Master Plan URL:</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="interactive_master_plan_url" value="<?= $development->interactive_master_plan_url;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Land for Sale URL:</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="land_for_sale_url" value="<?= $development->land_for_sale_url;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">House & Land URL:</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="house_and_land_packages_url" value="<?= $development->house_and_land_packages_url;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">MAPOVIS Mini URL:</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="mapovis_mini_url" value="<?= $development->mapovis_mini_url;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                </div>
                                
                                <div id="tabs-17">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Show Icons >= to Zoom level:</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="show_icons_at_and_above_zoom_level" value="<?= $development->show_icons_at_and_above_zoom_level;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Available Icon URL (Zoom 17):</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="available_17" value="<?= $development->available_17;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Available Icon URL (Zoom 18):</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="available_18" value="<?= $development->available_18;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Available Icon URL (Zoom 19):</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="available_19" value="<?= $development->available_19;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Sold Icon URL (Zoom 17):</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="sold_17" value="<?= $development->sold_17;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Sold Icon URL (Zoom 18):</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="sold_18" value="<?= $development->sold_18;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Sold Icon URL (Zoom 19):</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="sold_19" value="<?= $development->sold_19;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Deposited Icon URL (Zoom 17):</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="deposited_17" value="<?= $development->deposited_17;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Deposited Icon URL (Zoom 18):</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="deposited_18" value="<?= $development->deposited_18;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="required">Deposited Icon URL (Zoom 19):</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="deposited_19" value="<?= $development->deposited_19;?>">
                                        </div>
                                    </div><!-- End .form-group-->
                                </div>

								<div class="col-lg-offset-2">
									<br>
									<button id="submit_form_btn" type="submit" class="btn btn-info">Update</button>
									<a href="<?= $manage_development;?>" style="padding-left:10px;"><button type="button" class="btn btn-default">Cancel</button></a>
									<br><br>
								</div>
								</div>
							</form>
							</div>

							</div><!-- End .panel -->

						</div><!-- End .span3 -->

				</div><!-- End .row --> 

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script>
	$(function(){
		$("#tabs").tabs();
	});

	$('#house_and_land_view').on('change', function(){
		if($(this).val() == ''){
			$('#house_land_order_by_field').css('display', 'none');
		}
		else{
			$('#house_land_order_by_field').css('display', 'block');
		}
	});
	$('#land_view').on('change', function(){
		if($(this).val() == ''){
			$('#land_order_by_field').css('display', 'none');
		}
		else{
			$('#land_order_by_field').css('display', 'block');
		}
	});
	$('#land_pdf_view').on('change', function(){
		if($(this).val() == ''){
			$('#land_pdf_order_by_field').css('display', 'none');
		}
		else{
			$('#land_pdf_order_by_field').css('display', 'block');
		}
	});
	$('.generated_pdf').on('change', function(){
		if($(this).val() == 1){
			$('#lots_pdf_file_div').css('display', 'inline');
		}
		else{
			$('#lots_pdf_file_div').css('display', 'none');
		}

	});
	$(document).ready(function() {
		$('.change_colour').on('change', function(){
			var color = $(this).val();
			var box_id = $(this).attr('name')+'_disp';
			$('#'+box_id).attr('style', 'width:50px !important;border: 2px solid '+color+';background-color:'+color+';')
		});
	});
</script>
