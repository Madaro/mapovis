<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';

$days_of_week       = array(
	1 => 'Monday',
	2 => 'Tuesday',
	3 => 'Wednesday',
	4 => 'Thursday',
	5 => 'Friday',
	6 => 'Saturday',
	7 => 'Sunday',
);
$conditions         = array(
	'always'   => 'Always',
	'onchange' => 'Only after changes',
);
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					 <h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3> 
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-alarm"></span>
									<span>Price List Notifications Schedule</span>
								</h4>
							</div>
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>pricelistschedule/<?= $development->development_id;?>" role="form">
							<div class="panel-body">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Days to Send Email:</label>
								</div>
								<?php foreach($days_of_week as $day_number => $day_label):?>
								<div class="form-group">
									<label class="col-lg-2 control-label" for="required"></label>
									<div class="col-lg-4">
										<input type="checkbox" name="days_of_week[]" value="<?= $day_number;?>" class="radio" id="days_of_week<?=$day_number;?>" <?= (in_array($day_number, $schedule->days_of_week_array))? 'checked="checked"': '';?> /><label for="days_of_week<?=$day_number;?>"> &nbsp;&nbsp; <?= $day_label?></label>
									</div>
								</div><!-- End .form-group-->
								<?php endforeach;?>
								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Time to Send:</label>
									<div class="col-lg-2">
										<select name="notification_time" class="form-control">
										<?php for($hour = 0; $hour < 24; $hour ++):?>
											<option value="<?= $hour;?>" <?= ($schedule->notification_time == $hour)? 'selected="selected"': '';?>><?= (($hour % 12) == 0? 12: $hour % 12).(floor($hour / 12) == 0? ' am': ' pm');?></option>
										<?php endfor;?>
										</select>
									</div>
								</div><!-- End .form-group-->
								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Condition:</label>
									<div class="col-lg-2">
										<select name="conditions" class="form-control">
										<?php foreach($conditions as $condition => $condition_label):?>
											<option value="<?= $condition;?>" <?= ($schedule->conditions == $condition)? 'selected="selected"': '';?>><?= $condition_label;?></option>
										<?php endforeach;?>
										</select>
									</div>
								</div><!-- End .form-group-->
								<div class="col-lg-offset-3">
									<br>
									<button type="submit" class="btn btn-info">Save changes</button>
									<span style="padding-left:10px;"><a href="<?= $manage_development; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
									<br><br>
								</div>
							</div>
							</form>

						</div><!-- End .panel -->

					</div><!-- End .span3 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
