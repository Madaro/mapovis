<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-location-2"></span>
									<span><?= $development->development_name;?> External Amenity Types</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($e_amenity_types)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Name</th>
											<th>Icon</th>
											<th>Icon Unselected</th>
											<th>Pin</th>
											<th></th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_amenity_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_icon" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_icon_unselected" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_pin" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($e_amenity_types as $type):?>
										<tr>
											<td><?= $type->external_amenity_type_id;?></td>
											<td><?= $type->e_amenity_type_name;?></td>
											<td><?= $type->e_amenity_icon;?></td>
											<td><?= $type->e_amenity_icon_unselected;?></td>
											<td><?= $type->e_amenity_pin;?></td>
											<td class="center">
												<button type="button" type_id="<?= $type->external_amenity_type_id;?>" class="btn btn-xs btn-default view_type_btn">Edit</button>
											</td>
											<td>
												<?php if($type->linked == 0):?>
												<a href="<?= "{$base_url}deleteexternalamenitytype/{$type->external_amenity_type_id}"; ?>">
													<button type="button" type_name="<?= $type->e_amenity_type_name;?>" class="delete_amenity_type_btn btn btn-xs btn-danger">Delete</button>
												</a>
												<?php endif;?>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not external amenity types in the system.</div>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

				<div>
					<a href="<?= $base_url; ?>addexternalamenitytype/<?= $development->development_id;?>">
						<button type="button" class="btn btn-info">Add Amenity Type</button>
					</a>
				</div>
			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Image field necesary to call Aviary and crop the images -->
<img id='imageupload' src='' style="display:none"/>
<!-- Dialog -->
<div id="dialog_amenity"></div>