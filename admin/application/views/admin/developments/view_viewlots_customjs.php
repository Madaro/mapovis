<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
/* automatically updating the lot square meters */
function updateSquareMeters(){
	var width = document.getElementById('width_lot').value;
	var depth = document.getElementById('depth_lot').value;
	if(width && depth){
		document.getElementById('squaremeters_lot').value = width * depth;
	}
}
$('.view_lot_btn').on('click', function(){
	var lot_id = $(this).attr('lot_id');
	var title = $(this).attr('title');
	showLoading();
	$.post('<?= base_url();?>admin/developments/viewlot/'+lot_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_lot').html(data);
				$('#dialog_lot').dialog({
					title: title,
					height: 600,
					width: 1000,
					modal: true,
					resizable: true,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_lot').html('');}
				});
				$('#dialog_lot').dialog('open');
				$("input, textarea, select").not('.nostyle').uniform();
				$('.cal_square_m').on('change', function(){updateSquareMeters()});
				hideLoading();
				$('#save_lot_changes').click(function() {
					showLoading();
				});
				$('#cancel_lot_changes').click(function() {
					$('#dialog_lot').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
$('#dialog_lot').dialog({autoOpen: false});
</script>