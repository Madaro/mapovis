<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<?= $alert_message;?>
					<?= $validation_msg;?>
					<div class="col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-file-excel"></span>
									<span>Export External Amenities (CSV Format)</span>
								</h4>
							</div>

							<div class="panel-body">
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>/exportexternalamenities/<?= $development->development_id;?>" role="form" enctype="multipart/form-data">
								<div class="col-lg-offset-2">
									<br>
									<button type="submit" class="btn btn-info">Export</button>
									<br><br>
								</div>

							</form>
							</div><!-- End .panel-body  -->
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-file-excel"></span>
									<span>Import External Amenities (CSV Format)</span>
								</h4>
							</div>

							<div class="panel-body">
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>importexternalamenitiesupload/<?= $development->development_id;?>" role="form" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-lg-3 control-label">Filename:</label>
									<div class="col-lg-3">
										<input type="file" name="csvfilename" size="20" accept=".csv" />
									</div>
								</div><!-- End .form-group  -->

								<div class="col-lg-offset-2">
									<br>
									<button type="submit" class="btn btn-info upload_btn">Upload</button>
									<br><br>
								</div>
							</form>
							</div><!-- End .panel-body  -->
						</div><!-- End .panel -->
					</div><!-- End .span3 -->
				</div><!-- End .row -->
			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
	$('.upload_btn').on('click', function(){
		showLoading();
	});
</script>
