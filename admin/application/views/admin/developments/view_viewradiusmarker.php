<?php
$base_url = base_url().'admin/developments/';
?>
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<div class="col-lg-12">
			<div class="panel-body">
				<form method="post" class="form-horizontal" action="<?= $base_url; ?>updateradiusmarker/<?= $radius_marker->radius_marker_id;?>" role="form">
					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Icon:</label>
						<div class="col-lg-7">
							<input type="text" name="icon" class="form-control" value="<?= $radius_marker->icon?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Longitude:</label>
						<div class="col-lg-7">
							<input type="text" name="longitude" class="form-control" value="<?= (float)$radius_marker->longitude?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Latitude:</label>
						<div class="col-lg-7">
							<input type="text" name="latitude" class="form-control" value="<?= (float)$radius_marker->latitude?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group" style="padding-top:10px">
						<div class="col-lg-offset-3 col-lg-9">
							<button type="submit" class="save_changes btn btn-info">Save Changes</button>
							<button type="button" class="cancel_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
						</div>
					</div><!-- End .form-group  -->
				</form>
			</div>
	</div><!-- End .span6 -->
