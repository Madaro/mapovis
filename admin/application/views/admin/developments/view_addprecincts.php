<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
	<!--Body content-->
	<div id="content" class="clearfix">
		<div class="contentwrapper"><!--Content wrapper-->
			<div class="heading">
				<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
				<div class="resBtnSearch">
					<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
				</div>
			 </div><!-- End .heading-->

			 <!-- Build page from here: -->
			 <div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-grid"></span>
									<span>Add New Precinct</span>
								</h4>
							</div>

							<div class="panel-body">
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>addprecincts/<?= $development->development_id;?>" role="form">
								<div class="form-group">
									<label class="col-lg-1 control-label">Precinct #:</label>
									<div class="col-lg-2">
										<input name="precinct_number" type="text" class="form-control" id="precinctnumber">
										<br>
										<span><button type="submit" class="btn btn-info">Add</button></span>
										<span style="padding-left:10px;"><a href="<?= $manage_development; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
										<br><br>
									</div>
								</div><!-- End .form-group  -->
								<input name="development_id" type="hidden" value="<?= $development->development_id;?>">
							</form>
							</div>
						</div><!-- End .panel -->

					</div><!-- End .span3 -->

			</div><!-- End .row -->

		</div><!-- End contentwrapper -->
	</div><!-- End #content -->

