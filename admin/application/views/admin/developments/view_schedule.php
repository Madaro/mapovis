<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3><a href="<?php echo $manage_development?>">Manage <?php echo $development->development_name;?></a> (<?php echo $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 brocco-icon-clock"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<?= $alert_message;?>
					<?= $validation_msg;?>
					<form method="post" class="form-horizontal" action="<?= $base_url; ?>schedule/<?= $development->development_id;?>" role="form">

					<?php for($pos = 0; $pos < NUMBER_SCHEDULES; $pos++):?>
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 brocco-icon-clock"></span>
									<span>Schedule <?= $pos+1;?></span>
								</h4>
							</div>
							<div class="panel-body">

								<div class="form-group">
									<label class="col-lg-1 control-label" for="required">Status:</label>
									<div class="col-lg-4">
										<select name="schedule_enabled[<?= $pos;?>]" class="form-control">
											<option value="0" <?= ($schedules[$pos]->schedule_enabled == 0)? 'selected="selected"': '';?> >Disabled</option>
											<option value="1" <?= ($schedules[$pos]->schedule_enabled == 1)? 'selected="selected"': '';?> >Enabled</option>
										</select>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-1 control-label" for="required">Day:</label>
									<div class="col-lg-4">
										<select name="schedule_day[<?= $pos;?>]" class="form-control">
											<option></option>
											<?php foreach($days_of_week as $day_id => $day_label):?>
												<option value="<?= $day_id;?>" <?= ($schedules[$pos]->schedule_day == $day_id)? 'selected="selected"': ''; ?> ><?= $day_label;?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div><!-- End .form-group  -->

								<div class="form-group">
									<label class="col-lg-1 control-label" for="required">By Time:</label>
									<div class="col-lg-4">
										<select name="schedule_time[<?= $pos;?>]" class="form-control">
											<option></option>
											<?php for($time = 0; $time < 24;$time++):?>
												<option value="<?= $time;?>" <?= ($schedules[$pos]->schedule_time == $time)? 'selected="selected"': ''; ?> ><?= (($time%12) == 0? 12: $time%12).':00 '.(floor($time/12) == 0? 'am': 'pm');?></option>
											<?php endfor;?>
										</select>
									</div>
								</div><!-- End .form-group  -->


								<div style="padding-left:50px;">
								   <br>
									<button type="submit" class="btn btn-info">Update</button>
									<span style="padding-left:10px;"><a href="<?= $manage_development; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
									<br><br>
								</div>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span3 -->
					<?php endfor;?>
					<input name="development_id" type="hidden" value="<?= $development->development_id;?>">
					</form>

			</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
