<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
$amenity_id         = 0;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Add External Amenity Type</span>
								</h4>
							</div>
							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>addexternalamenitytype/<?= $development->development_id;?>" role="form">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Name:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_type_name" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Icon:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_icon" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Icon Unselected:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_icon_unselected" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Icon Width:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_icon_width" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Icon Height:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_icon_height" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Pin:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_pin" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Pin Width:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_pin_width" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Pin Height:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_pin_height" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Map Center Longitude:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_center_longitude" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Map Center Latitude:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_center_latitude" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Map Zoom Level:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_zoom_level" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Cluster Circle URL:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_cluster_circle_url" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Cluster Circle Width:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_cluster_circle_width" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Cluster Circle Height:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_cluster_circle_height" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Cluster Circle Text Colour:</label>
									<div class="col-lg-5">
										<div class="row">
											<div class="col-lg-3">
												<div class="input-group">
													<input type="text" class="form-control change_colour" id="e_amenity_cluster_text_hex_colour" name="e_amenity_cluster_text_hex_colour" value="">
												</div><!-- /input-group -->
											</div>
											<div class="col-lg-3">
												<div class="input-group">
													<div id="e_amenity_cluster_text_hex_colour_disp">&nbsp;&nbsp;&nbsp;</div>
												</div><!-- /input-group -->
											</div>
										</div>
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Cluster Circle Text Size:</label>
									<div class="col-lg-5">
										<input type="text" name="e_amenity_cluster_text_size" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group" style="padding-top:10px">
									<div class="col-lg-offset-1 col-lg-9">
										<span><button id="action_continue_btn" type="submit" class="btn btn-primary save_new_amenity">Add Amenity Type & Continue</button></span>
										<span style="padding-left: 10px;"><button id="action_btn" type="submit" class="btn btn-info save_new_amenity">Add Amenity Type & Finish</button></span>
										<span style="padding-left: 10px;"><a href="<?= $base_url.'manageexternalamenitytypes/'.$development->development_id; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
									</div>
								</div><!-- End .form-group  -->
								<input id="action_continue" name="action_continue" type="hidden" value="0">
							</form>
							</div><!-- End .body -->

						</div><!-- End .panel -->
					</div><!-- End .span1 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
		<!-- Image field necesary to call Aviary and crop the images -->
		<img id='imageupload' src='' style="display:none"/>
<script type="text/javascript">
	$(document).ready(function() {
		$('.change_colour').on('change', function(){
			var color = $(this).val();
			var box_id = $(this).attr('name')+'_disp';
			$('#'+box_id).attr('style', 'width:50px !important;border: 2px solid '+color+';background-color:'+color+';')
		});
	});
</script>		