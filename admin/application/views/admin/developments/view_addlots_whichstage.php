<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-document-2"></span>
									<span>Add Lots to Which Stage in Precinct <?= $precinct->precinct_number;?>?</span>
								</h4>
							</div>
							<div class="panel-body">
							<?php if(count($stages)):?>
								<?= $validation_msg;?>
								<form method="post" class="form-horizontal" action="<?= $base_url; ?>addlotswhichstage/<?= $development->development_id;?>/<?= $precinct->precinct_id;?>" role="form">
									<div class="form-group">
										<label class="col-lg-1 control-label">Stage:</label>
										<div class="col-lg-2">
											<select name="stage_id" class="form-control">
												<option selected="selected"></option>
												<?php foreach($stages as $stage):?>
													<option value="<?= $stage->stage_id;?>"><?= $stage->stage_number;?></option>
												<?php endforeach;?>
											</select>
											<br> <br>
											<button type="submit" class="btn btn-info">Continue</button>
											<a href="<?= $manage_development; ?>"><button type="button" class="btn btn-default">Cancel</button></a>
											<br><br>
										</div>
									</div><!-- End .form-group  -->
								<input name="precinct_id" type="hidden" value="<?= $precinct->precinct_id;?>">
								</form>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not stages in the system.</div>
										<a href="<?= $base_url; ?>addstages/<?= $development->development_id;?>" style="padding-left: 20px;">
											<button type="button" class="btn btn-info">Add Stages</button>
										</a>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span3 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
