<link href="<?php echo base_url(); ?>assets/js/chromoselector-2.1.5/chromoselector.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/chromoselector-2.1.5/chromoselector.min.js"></script>
<script type="text/javascript">
$("#wizard_development").formwizard({
	formPluginEnabled: false,
	validationEnabled: false,
	focusFirstInput : true,
	disableUIStyles: true,
	showSteps: true //show the step
});
function updateStages(precinct_num, totalstages)
{
	/* If the precinct with the stages is not there it will add the html */
	if($("#precinct_stages_"+precinct_num).length == 0){
		var basic_html = '<div id="precinct_stages_'+precinct_num+'" class="allstages">';
		basic_html += '<div style="background-color: #e9e9e9; height: 40px;"><h4 style="padding-left: 50px; padding-top:10px">Precinct '+precinct_num+'</h4></div><br>';
		basic_html += '<div id="stages_for_'+precinct_num+'"></div><br></div>';
	}
	$("#allstages").append(basic_html);

	var stages_html = '<div class="form-group"><label class="col-lg-2">Stage #</label>';
	stages_html += '<label class="col-lg-2">Center Latitude</label>';
	stages_html += '<label class="col-lg-2">Center Longitute</label>';
	stages_html += '<label class="col-lg-2">Zoom Level</label></div>';
	for(var stg = 1; stg <= totalstages; stg++){
		stages_html += '<div class="form-group">';
		stages_html += '<label class="col-lg-2 control-label" for="required">'+stg+'</label>';
		stages_html += '<div class="col-lg-2"><input name="stage_center_latitude_'+precinct_num+'_'+stg+'" type="text" class="form-control"></div>';
		stages_html += '<div class="col-lg-2"><input name="stage_center_longitute_'+precinct_num+'_'+stg+'" type="text" class="form-control"></div>';
		stages_html += '<div class="col-lg-2"><input name="stage_zoomlevel_'+precinct_num+'_'+stg+'" type="text" class="form-control"></div>';
		stages_html += '</div><!-- End .form-group  -->';
	}
	$("#stages_for_"+precinct_num).html(stages_html);
}

/* This function will show the inputs for number of stages based on the number of precinft provided */
function updatePrecincts(totalprecincts){
	var precincts_html  = '';
	var precinct_html   = '';
	for(var num = 1; num <= totalprecincts; num++){
		precinct_html = '<div class="form-group allprecincts" precinct_id="' +num+'"> <label class="col-lg-2 control-label">Number of Stages in Precinct '+num+' </label>';
		precinct_html += '<div class="col-lg-1"><input type="text" class="form-control" name="precinct_stages['+num+']"></div></div><!-- End .form-group  -->';

		precincts_html += precinct_html;
	}
	$("#allprecincts").html(precincts_html);
	$('.allprecincts').each(function() {
		var precinct_num = $(this).attr('precinct_id');
		$(this).find('input').change(function(){
			$("#allstages").append('');
			updateStages(precinct_num, $(this).val());
		});
	});
}

/* If there is data already in the precinct number display inputs for number of stages */
updatePrecincts($("#totalprecincts").val())

$("#totalprecincts").change(function(){
	updatePrecincts($(this).val());
});
$(document).ready(function() {
	var updatePreview = function() {
		var color = $(this).chromoselector('getColor');
		var box_id = $(this).attr('name')+'_disp';
		var color_box = color.getHexString();
		$('#'+box_id).attr('style', 'width:50px !important;border: 2px solid '+color_box+';background-color:'+color_box+';')
	};
	jQuery.each($('.change_colour'), function(){
		$(this).chromoselector({
			preview: false,
			update: updatePreview
		});
	});
});
$('#house_and_land_view').on('change', function(){
	if($(this).val() == ''){
		$('#house_land_order_by_field').css('display', 'none');
	}
	else{
		$('#house_land_order_by_field').css('display', 'block');
	}
});
$('#land_view').on('change', function(){
	if($(this).val() == ''){
		$('#land_order_by_field').css('display', 'none');
	}
	else{
		$('#land_order_by_field').css('display', 'block');
	}
});
$('#land_pdf_view').on('change', function(){
	if($(this).val() == ''){
		$('#land_pdf_order_by_field').css('display', 'none');
	}
	else{
		$('#land_pdf_order_by_field').css('display', 'block');
	}
});
$('.generated_pdf').on('change', function(){
	if($(this).val() == 1){
		$('#lots_pdf_file_div').css('display', 'inline');
	}
	else{
		$('#lots_pdf_file_div').css('display', 'none');
	}
});
</script>
