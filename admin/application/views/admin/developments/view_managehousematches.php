<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-document-2"></span>
									<span><?= $development->development_name;?> Lots</span>
								</h4>
							</div>
							<?= $alert_message;?>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Precinct</th>
											<th>Stage</th>
											<th>Lot</th>
											<th>Status</th>
											<th>Price Range</th>
											<th></th>
										</tr>
										<!-- START - Modification by Seb : Adding Column Filtering for DataTables -->
										<tr>
											<td><input type="text" name="search_precinct" placeholder="Search Precincts" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_stage" placeholder="Search Stages" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_lot" placeholder="Search Lots" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_status" placeholder="Search Status" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
										<!-- END - Modification by Seb -->
									</thead>
									<tbody>
									<?php foreach($lots as $lot):?>
										<tr>
											<td class="center"><?= $lot->precinct_number;?></td>
											<td class="center"><?= $lot->stage_number;?></td>
											<td class="center"><?= $lot->lot_number;?></td>
											<td class="center"><?= $lot->status;?></td>
											<td class="center">$<?= number_format($lot->price_range_min, 0);?> to $<?= number_format($lot->price_range_max, 0);?></td>
											<td class="center <?= $lot->total_matches == 0 ? 'incomplete_warning': '';?>">
												<a href="<?php echo $base_url; ?>editlothouses/<?= $lot->lot_id;?>">
													<button type="button" class="btn btn-xs btn-default">Manage House Matches</button>
												</a>
											</td>
										</tr>
									<?php endforeach;?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->
					</div><!-- End .span12 -->
				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
