<?php
$base_url = base_url().'admin/developments/';
?>
	<!-- Image field necesary to call Aviary and crop the images -->
	<img id='imageupload' src='' style="display:none"/>
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<div class="col-lg-12">
			<div class="panel-body">
				<form method="post" class="form-horizontal" action="<?= $base_url; ?>updateexternalamenitytype/<?= $amenity_type->external_amenity_type_id;?>" role="form">
					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Amenity Type Name:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_type_name" class="form-control" value="<?= $amenity_type->e_amenity_type_name?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Icon:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_icon" class="form-control" value="<?= $amenity_type->e_amenity_icon?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Icon Unselected:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_icon_unselected" class="form-control" value="<?= $amenity_type->e_amenity_icon_unselected?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Icon Width:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_icon_width" class="form-control" value="<?= $amenity_type->e_amenity_icon_width?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Icon Height:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_icon_height" class="form-control" value="<?= $amenity_type->e_amenity_icon_height?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Pin:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_pin" class="form-control" value="<?= $amenity_type->e_amenity_pin?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Pin Width:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_pin_width" class="form-control" value="<?= $amenity_type->e_amenity_pin_width?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Pin Height:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_pin_height" class="form-control" value="<?= $amenity_type->e_amenity_pin_height?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Map Center Longitude:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_center_longitude" class="form-control" value="<?= (float)$amenity_type->e_amenity_center_longitude?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Map Center Latitude:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_center_latitude" class="form-control" value="<?= (float)$amenity_type->e_amenity_center_latitude?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Map Zoom Level:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_zoom_level" class="form-control" value="<?= $amenity_type->e_amenity_zoom_level?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Cluster Circle URL:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_cluster_circle_url" class="form-control" value="<?= $amenity_type->e_amenity_cluster_circle_url?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Cluster Circle Width:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_cluster_circle_width" class="form-control" value="<?= $amenity_type->e_amenity_cluster_circle_width?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Cluster Circle Height:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_cluster_circle_height" class="form-control" value="<?= $amenity_type->e_amenity_cluster_circle_height?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Cluster Circle Text Colour:</label>
						<div class="col-lg-7">
							<div class="row">
								<div class="col-lg-6">
									<div class="input-group">
										<input type="text" class="form-control change_colour" id="e_amenity_cluster_text_hex_colour" name="e_amenity_cluster_text_hex_colour" value="#<?= $amenity_type->e_amenity_cluster_text_hex_colour;?>">
									</div><!-- /input-group -->
								</div>
								<div class="col-lg-6">
									<div class="input-group">
										<div id="e_amenity_cluster_text_hex_colour_disp" style="width:50px !important;border: 2px solid #<?= $amenity_type->e_amenity_cluster_text_hex_colour;?>;background-color: #<?= $amenity_type->e_amenity_cluster_text_hex_colour;?>">&nbsp;&nbsp;&nbsp;</div>
									</div><!-- /input-group -->
								</div>
							</div>
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-4 control-label" for="required">Cluster Circle Text Size:</label>
						<div class="col-lg-7">
							<input type="text" name="e_amenity_cluster_text_size" class="form-control" value="<?= $amenity_type->e_amenity_cluster_text_size?>" >
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group" style="padding-top:10px">
						<div class="col-lg-offset-3 col-lg-9">
							<button type="submit" class="save_changes btn btn-info">Save Changes</button>
							<button type="button" class="cancel_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
						</div>
					</div><!-- End .form-group  -->
				</form>
			</div>
	</div><!-- End .span6 -->
