<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					<h3><a href="<?php echo $manage_development?>">Manage <?php echo $development->development_name;?></a> (<?php echo $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->
				<?= $alert_message;?>

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-grid"></span>
									<span><?php echo $development->development_name;?> Precincts</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($precincts)):?>
									<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
										<thead>
											<tr>
												<th>Precinct ID</th>
												<th>Precinct</th>
												<th></th>
											</tr>
											<tr>
											  <td><input type="text" name="search_precinct_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td><input type="text" name="search_precinct_number" placeholder="" class="search_init" style="width: 100%;" /></td>
											  <td></td>
											</tr>
										</thead>
										<tbody>
											<?php foreach($precincts as $precinct):?>
												<tr>
													<td class="center"><?php echo $precinct->precinct_id?></td>
													<td class="center"><?php echo $precinct->precinct_number?></td>
													<td class="center">
														<a href="<?= $base_url.'deleteprecinct/'.$precinct->precinct_id;?>"><button type="button" id="opener1" class="btn btn-xs btn-danger" onclick="if(!confirm('Are you sure you want to delete the Precinct with all its stages and lots?')){return false;}">Delete</button></a>
													</td>
												</tr>
											<?php endforeach;?>
										</tbody>
									</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not precincts for this development.</div>
										<a href="<?php echo $base_url; ?>addprecincts/<?php echo $development->development_id;?>" style="padding-left: 20px;">
											<button type="button" class="btn btn-info">Add Precincts</button>
										</a>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->
					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->
			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
