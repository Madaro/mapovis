<style>
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 500px; }
	.sortable li { margin: 10px 10px 10px 0; padding: 0px; float: left; width: 150px; height: 116px;  text-align: center; display:block; overflow:hidden;}
	.deleteimage{margin-top: 7px;}
	.img_container{overflow-y:hidden;}
	.ui-dialog { z-index: 65535; }
	form .btn { margin-left:10px; }
</style>
<script type="text/javascript" src="http://feather.aviary.com/js/feather.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/validate/jquery.validate.min.js"></script>
<script>
var featherEditor = new Aviary.Feather({
	apiKey: '84495f70872a7572',
	apiVersion: 3,
	tools: 'all',
	theme: 'light',
	onError: function(errorObj) {
		console.log(errorObj.message);
	}
});
var facade_image_width  = '<?= isset($development->custom_house_facade_width)? $development->custom_house_facade_width:'244';?>';
var facade_image_height = '<?= isset($development->custom_house_facade_height)? $development->custom_house_facade_height: '133';?>';

function initilizeHouseView(house_id){
	$(function(){
		$('#sortable_facade').sortable({containment: "#img_container_facade",tolerance: "pointer"});
		$('#sortable_facade').disableSelection();
		$('#sortable_floor').sortable({containment: "#img_container_floor",tolerance: "pointer"});
		$('#sortable_floor').disableSelection();
		$('#sortable_gallery').sortable({containment: "#img_container_gallery",tolerance: "pointer"});
		$('#sortable_gallery').disableSelection();
	});
	if($('#import_existing_house').length > 0){
		$('#house_builder_id').change(function(){
			var house_builder_id = $( "#house_builder_id" ).val();
			$.ajax({
				url: '<?= base_url(); ?>admin/globalhouses/developerhouses/'+house_builder_id+'/'+house_id,
				type: 'POST',
				processData: false,
				contentType: false
			}).done(function(data){
				$("#import_existing_house_div").html(data);
			});
		});
	}
	$('.image_files').on('change', function(){
		var input = $(this)[0];
		if (input.files && input.files[0]){
			if(!validateimagefiletype(input.files[0].type)){
				return false;
			}
		}
	});
	$('.deleteimage').on('click', function(e){
		deleteImageFunction($(this), true);
	});
	if(house_id > 0){
		init_tinymce();
		$("input, textarea, select, button").attr('tabindex', '-1');
		$("input, textarea, select").not('.nostyle').uniform();
	}
	$('.add_image_btn').on('click', function(e){
		var area            = $(this).attr('area');
		var house_id        = $(this).attr('house_id');
		var image_field_obj = $('#image_file'+area);
		var input           = image_field_obj[0];
		if (input.files && input.files[0]){
			if(!validateimagefiletype(input.files[0].type)){
				// show dialog message
				return false;
			}
			var reader = new FileReader();
			// set where you want to attach the preview
			reader.target_elem = $(input).parent().find('preview');
			reader.onload = function (e) {
				var force_crop = true;
				var img = document.createElement("img");
				img.onload = function(){
					if(area != '_facade' || img.width == facade_image_width && img.height == facade_image_height){
						force_crop = false;
					}
					$('#imageupload'+area).attr('src', e.target.result);
					launchEditor('imageupload'+area, e.target.result, input.files[0].type, house_id, force_crop,area);
				}
				img.src = e.target.result;
			};
			reader.readAsDataURL(input.files[0]);
		}
	});

	$('.add_existing_house').on('click', function(e){
		var current_house_id  = $(this).attr('house_id');
		var existing_house_id = $('#import_existing_house').val();
		$.ajax({
			url: '<?= base_url(); ?>admin/globalhouses/gethousefacadeimages/'+existing_house_id,
			type: 'POST',
			processData: false,
			contentType: false
		}).done(function(data){
			var result = jQuery.parseJSON(data);
			addImageHtmlFromHouse(current_house_id, existing_house_id, result, '_facade')
		});
	});

	$('#add_edit_house_form').validate({
		rules: {
			house_name: {
				required: true,
				remote:{
					url: '<?php echo base_url(); ?>admin/developments/validatehousename/<?= (isset($development->development_id)? $development->development_id: 0);?>',
					type: "post",
					data: {
						house_name: function() {
							return $( "#house_name" ).val();
						},
						builder_id: function() {
							return $( "#house_builder_id" ).val();
						},
						house_id: house_id
					}
				}
			},
			house_builder_id: {
				required: true,
				remote:{
					url: '<?php echo base_url(); ?>admin/developments/validatehousename/<?= (isset($development->development_id)? $development->development_id: 0);?>',
					type: "post",
					data: {
						house_name: function() {
							return $( "#house_name" ).val();
						},
						builder_id: function() {
							return $( "#house_builder_id" ).val();
						},
						house_id: house_id
					}
				}
			},
			image_file_facade:{accept: false},
			image_file_floor:{accept: false},
			image_file_gallery:{accept: false}
		},
		groups: {
			house_name: 'house_name, house_builder_id'
		},
		messages: {
			house_name: {
				required: 'Please provide the house name',
			}
		},
		submitHandler: function(form) {
			showLoading();
			if(house_id == 0){
				form.submit();
			}
			else{
				submitForm(form);
			}
		}
	});

	function submitForm(form){
		showLoading();
		$.ajax( {
			url: '<?= base_url(); ?>admin/developments/updatehouse/'+house_id,
			type: 'POST',
			data: new FormData( form ),
			processData: false,
			contentType: false
		}).done(function(data){
			hideLoading();
			var alert_msg = '';
			var result = jQuery.parseJSON(data);
			console.log(data)
			console.log(result)
			if(result.status == 1){
				alert_msg = '<div title="Success">'+result.msg+'</div>';
				$('#dialog_house').dialog('close');
				updateHouseRow(result.obj)
			}
			else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
			messageAlert(alert_msg);
		});
		return false;
	};
}
function updateHouseRow(obj){
	if(obj != false){
		$('#tr_houseid_'+obj.house_id).find("td").eq(1).html(obj.house_name);
		$('#tr_houseid_'+obj.house_id).find("td").eq(2).html(obj.builder_name);
	}
}
function deleteImageFunction(object, updatedb){
	var imageid = object.attr('image_id');
	var image_obj = object.parent();
	$("<div><span>Are you sure you want to delete this image permanently?</span></div>").dialog({
		resizable: false,
		height:140,
		modal: true,
		buttons: {
			"Delete Image": function() {
				if(updatedb){
					showLoading();
					$.post("<?= base_url().'admin/developments/deletehouseimage'?>",{
							house_image_id: imageid
						},
						function(data,status){
							hideLoading();
							var alert_msg = '';
							if(status === 'success'){
								var result = jQuery.parseJSON(data);
								if(result.status == 1){
									alert_msg = '<div title="Success">'+result.msg+'</div>';
									image_obj.remove();
								}
								else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
							}
							else{ alert_msg = '<div title="Fail">'+data+'</div>';}
							messageAlert(alert_msg);
					});
				}
				else{
					image_obj.remove();
					messageAlert('<div title="Success">The image was deleted successfully.</div>');
				}
				$(this).dialog("close");
			},
			Cancel: function() {
				$('#save_changes').focus();
				$(this).dialog( "close" );
			}
		}
	});
}

function launchEditor(id, src, filetype, house_id, force_crop, area){
	var editor_params = {
		image: id,
		url: src,
		fileFormat: filetype,
		onSave: function(imageID, newURL) {
			addNewImageHtml(house_id, newURL, newURL, area);
			var img = document.getElementById(imageID);
			img.src = newURL;
			featherEditor.close();
		}
	};
	if(force_crop){
		editor_params.forceCropPreset = ['Width: '+facade_image_width+'px, Height: '+facade_image_height+'px',facade_image_width+'x'+facade_image_height];
		editor_params.initTool = 'crop';
		editor_params.cropPresetsStrict = 'true';
	}
	else{
		editor_params.tools = 'enhance,effects,frames,stickers,focus,brightness,contrast,saturation,warmth,sharpness,colorsplash,draw,text,redeye,whiten,blemish';
	}
	featherEditor.launch(editor_params);
}

var position = 100000;
function addNewImageHtml(house_id, newURL, imagecontent, area){
	position++;
	if(area == '_facade'){
		var image_field_obj = $('#image_file'+area);
		var input           = image_field_obj[0];
		var form_data = new FormData();
		form_data.append('image_file_facade', input.files[0]);
		$.ajax( {
			url: '<?= base_url(); ?>admin/developments/uploadoriginalimage/',
			type: 'POST',
			data: form_data,
			processData: false,
			contentType: false
		}).done(function(data){
			var result = jQuery.parseJSON(data);
			if(result != false){
				var original_image = '<input type="hidden" name="original_image['+position+']" value="'+result+'">';
				$('#originalFiles').append(original_image)
			}
		});
	}

	var button_id = 'delete_img' + new Date().getTime();
	var new_image_html = '<li class="ui-state-default"><input name="image_names'+area+'[]" type="text" value="'+position+'||'+newURL+'" style="display:none"><div><img src="'+imagecontent+'" height="81" /></div>';
	new_image_html += '<button type="button" id="'+button_id+'" house_id="'+house_id+'" existing_house="0" image_id="'+newURL+'" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button></li>';
	$('#sortable'+area).append(new_image_html);
	$('#'+button_id).on('click', function(e){
		deleteImageFunction($(this), false);
	});
}

function addImageHtmlFromHouse(current_house_id, existing_house_id, house_images, area){
	var image_path = '<?= base_url('../mpvs/images/dev_houses/');?>';
	position++;
	for(var pos = 0; pos < house_images.length; pos++){
		var image_content = image_path+'/'+house_images[pos].file_name;
		var button_id = 'delete_img' + new Date().getTime();
		var new_image_html = '<li class="ui-state-default"><input name="image_names'+area+'[]" type="text" value="'+house_images[pos].house_image_id+'||copyExisting" style="display:none"><div><img src="'+image_content+'" height="81" /></div>';
		new_image_html += '<button type="button" id="'+button_id+'" house_id="'+current_house_id+'" existing_house="1" image_id="'+house_images[pos].house_image_id+'" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button></li>';
		$('#sortable'+area).append(new_image_html);
		$('#'+button_id).on('click', function(e){
			deleteImageFunction($(this), false);
		});
	}
}

$( document ).ready(function() {
	// create a new builder record
	if($('#dialog_builder').length > 0){
		$('#add_builder_btn').on('click', function(){
			if($('#builder_name').val() == '' || $('#builder_website').val() == ''){
				messageAlert('<div title="Fail">Please provide a Name and Website.</div>');
				return false;
			}
			showLoading();
			$.post("<?= base_url('admin/developments/addbuilder');?>",{
					builder_name: $('#builder_name').val(),
					builder_website: $('#builder_website').val(),
					builder_email: $('#builder_email').val(),
					builder_telephone: $('#builder_telephone').val()
				},
				function(data,status){
					var alert_msg = '';
					hideLoading();
					if(status === 'success'){
						var result = jQuery.parseJSON(data);
						if(result.status == 1){
							alert_msg = '<div title="Success">'+result.msg+'</div>';
							var new_builder_html = '<option value="'+result.builder_id+'" >'+result.builder_name+'</option>';
							$('#house_builder_id').append(new_builder_html).val(result.builder_id).change();
							$('#dialog_builder').dialog('close');
						}
						else{ alert_msg = '<div title="Fail">'+result.msg+'</div>';}
					}
					else{ alert_msg = '<div title="Fail">'+data+'</div>';}
					messageAlert(alert_msg);
			});
		});
		$('#dialog_builder').dialog({ autoOpen: false });
		$('#dialog_builder').dialog({
			title: 'Add New Builder',
			height: 350,
			width: 1000,
			modal: true,
			resizable: false,
			dialogClass: 'loading-dialog'
		});
		$('#new_builder').click(function() {
			$('#dialog_builder').dialog('open');
		});
	}
	if($('#action_continue_btn').length > 0){
		$('#action_continue_btn').click(function() {
			document.getElementById('action_continue').value = 1;
		});
		$('#action_btn').click(function() {
			document.getElementById('action_continue').value = 0;
		});
	}

});
</script>