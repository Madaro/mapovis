<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
$('#dialog_polyline').dialog({ autoOpen: false });

$('.view_type_btn').on('click', function(){
	var type_id = $(this).attr('type_id');
	showLoading();
	$.post('<?= base_url();?>admin/developments/viewpolyline/'+type_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_polyline').html(data);
				$('#dialog_polyline').dialog({
					title: 'Update Polyline',
					height: 340,
					width: 550,
					modal: true,
					resizable: false,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_polyline').html('');}
				});
				$('#dialog_polyline').dialog('open');
				$("input, textarea, select").not('.nostyle').uniform();
				hideLoading();
				$('.save_changes').click(function() {
					showLoading();
				});
				$('.cancel_changes').click(function() {
					$('#dialog_polyline').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
$('.delete_polyline_btn').click(function() {
	if(!confirm('Are you sure you want to delete the polyline?')){
		return false;
	}
	showLoading();
});
</script>