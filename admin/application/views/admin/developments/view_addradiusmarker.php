<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
$amenity_id         = 0;
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-home-6"></span>
									<span>Add Radius Marker</span>
								</h4>
							</div>
							<div class="panel-body">
							<?= $alert_message;?>
							<?= $validation_msg;?>
							<form method="post" class="form-horizontal" action="<?= $base_url; ?>addradiusmarker/<?= $development->development_id;?>" role="form">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Icon:</label>
									<div class="col-lg-4">
										<input type="text" name="icon" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  -->     

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Longitude:</label>
									<div class="col-lg-4">
										<input type="text" name="longitude" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group">
									<label class="col-lg-2 control-label" for="required">Latitude:</label>
									<div class="col-lg-4">
										<input type="text" name="latitude" class="form-control" value="" >
									</div>
								</div><!-- End .form-group  --> 

								<div class="form-group" style="padding-top:10px">
									<div class="col-lg-offset-1 col-lg-9">
										<span><button id="action_continue_btn" type="submit" class="btn btn-primary save_new_amenity">Add Radius Marker & Continue</button></span>
										<span style="padding-left: 10px;"><button id="action_btn" type="submit" class="btn btn-info save_new_amenity">Add Radius Marker & Finish</button></span>
										<span style="padding-left: 10px;"><a href="<?= $base_url.'manageradiusmarkers/'.$development->development_id; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
									</div>
								</div><!-- End .form-group  -->
								<input id="action_continue" name="action_continue" type="hidden" value="0">
							</form>
							</div><!-- End .body -->

						</div><!-- End .panel -->
					</div><!-- End .span1 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
		<!-- Image field necesary to call Aviary and crop the images -->
		<img id='imageupload' src='' style="display:none"/>
