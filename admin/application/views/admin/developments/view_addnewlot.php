<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>
									<span class="icon16 entypo-icon-document-2"></span>
									<span>Add Lot to Precinct <?= $precinct->precinct_number;?>, Stage <?= $stage->stage_number;?></span>
								</h4>
							</div>
							<div class="panel-body">
								<?= $alert_message;?>
								<?= $validation_msg;?>
								<form method="post" class="form-horizontal" action="<?= $base_url; ?>addnewlot/<?= $development->development_id;?>/<?= $precinct->precinct_id;?>/<?= $stage->stage_id;?>" role="form">
									<div class="form-group">
										<label class="col-lg-2 control-label" for="textareas">Lot Number:</label>
										<div class="col-lg-2">
											<input name="lot_number" type="text" class="form-control" id="lotnumber">
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="textareas">Street Name: <br><a href="#" id="newstreet_btn">[add new]</a></label>
										<div class="col-lg-2">
											<select id="street_name_id" name="street_name_id" class="form-control" id="status">
											<option></option>
											<?php foreach($street_names as $street_name):?>
												<option value="<?= $street_name->street_name_id;?>"><?= $street_name->street_name;?></option>
											<?php endforeach;?>
											</select>
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="textareas">Status:</label>
										<div class="col-lg-2">
											<select name="status" class="form-control" id="status">
											<?php foreach($lot_status as $status):?>
												<option value="<?= $status;?>"><?= $status;?></option>
											<?php endforeach;?>
											</select>
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="buttons">Price Range:</label>
										<div class="col-lg-6">
											<div class="row">
												<div class="col-lg-2">
												   <div class="input-group">
														<input name="price_range_min" type="text" class="form-control range" id="pricerange-from">
													</div><!-- /input-group -->
												</div>

												<div class="col-lg-1 pricerange-to"> to </div>
												<div class="col-lg-2">
													<div class="input-group">
														<input name="price_range_max" type="text" class="form-control range" id="pricerange-to">
													</div><!-- /input-group -->
												</div>
											</div>
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="textareas">Latitude:</label>
										<div class="col-lg-2">
										<input name="lot_latitude" type="text" class="form-control" id="latitude">
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="textareas">Longitude:</label>
										<div class="col-lg-2">
										<input name="lot_longitude" type="text" class="form-control" id="longitude">
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="textareas">Width (m):</label>
										<div class="col-lg-2">
											<input name="lot_width" type="text" class="form-control" id="width" onchange="updateSquareMeters();">
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="textareas">Depth (m):</label>
										<div class="col-lg-2">
										<input name="lot_depth" type="text" class="form-control" id="depth" onchange="updateSquareMeters();">
										</div>
									</div><!-- End .form-group  -->

									<div class="form-group">
										<label class="col-lg-2 control-label" for="textareas">Square Meters:</label>
										<div class="col-lg-2">
										<input name="lot_square_meters" type="text" class="form-control" id="squaremeters">
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-2 control-label" for="textareas">Is a Corner Lot?:</label>
										<div class="col-lg-9">
											<input type="radio" name="lot_corner" value="1"  id="lot_corner1" class="lot_corner"><label for="lot_corner1">&nbsp; Yes </label> &nbsp;&nbsp;
											<input type="radio" name="lot_corner" value="0" checked="checked" id="lot_corner0" class="lot_corner"><label for="lot_corner0">&nbsp; No </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-2 control-label" for="textareas">Tittle:</label>
										<div class="col-lg-9">
											<input type="radio" name="lot_titled" value="1" id="lot_titled1" class="lot_titled"><label for="lot_titled1">&nbsp; Yes </label> &nbsp;&nbsp;
											<input type="radio" name="lot_titled" value="0" checked="checked" id="lot_titled0" class="lot_titled"><label for="lot_titled0">&nbsp; No </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group">
										<label class="col-lg-2 control-label" for="textareas">Is an Irregular Lot?:</label>
										<div class="col-lg-9">
											<input type="radio" name="lot_irregular" value="1"  id="lot_irregular1" class="lot_irregular"><label for="lot_irregular1">&nbsp; Yes </label> &nbsp;&nbsp;
											<input type="radio" name="lot_irregular" value="0" checked="checked" id="lot_irregular0" class="lot_irregular"><label for="lot_irregular0">&nbsp; No </label>
										</div>
									</div><!-- End .form-group  --> 

									<div class="form-group" style="padding-top:10px">
										<div class="col-lg-offset-1 col-lg-9">
											<span><button id="action_continue_btn" type="submit" class="btn btn-primary">Add Lot & Continue</button></span>
											<span style="padding-left: 10px;"><button id="action_btn" type="submit" class="btn btn-info">Add Lot & Finish</button></a></span>
											<span style="padding-left: 10px;"><a href="<?= $manage_development; ?>"><button type="button" class="btn btn-default">Cancel</button></a></span>
										</div>
									</div><!-- End .form-group  -->
								<input name="precinct_id" type="hidden" value="<?= $precinct->precinct_id;?>">
								<input name="stage_id" type="hidden" value="<?= $stage->stage_id;?>">
								<input id="action_continue" name="action_continue" type="hidden" value="0">
								</form>

							</div>

						</div><!-- End .panel -->

					</div><!-- End .span3 -->

				</div><!-- End .row -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Dialog -->
<div id="dialog">
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<div class="col-lg-9">
			<div class="panel-body">
				<?php if(!empty($validtion_errors)):?>
					<div class="alert alert-danger"><?= validation_errors(); ?></div>
				<?php endif;?>
				<form method="post" class="form-horizontal" action="<?= $base_url; ?>addstreetname/<?= $development->development_id;?>" role="form">
					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Street Name:</label>
						<div class="col-lg-9">
						<input id="street_name" name="street_name" type="text" class="form-control">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group" style="padding-top:10px">
						<div class="col-lg-9" style="padding-left:145px;">
							<button id="add_street_btn" type="button" class="btn btn-info">Save Changes</button>
							<span id="cancel_street_btn" style="padding-left:10px;"><button type="button" class="btn btn-default" onclick="$('#dialog').dialog('close');">Cancel</button></span>
						</div>
					</div><!-- End .form-group  -->

				</form>
			</div>
	</div><!-- End .span6 -->
</div>
