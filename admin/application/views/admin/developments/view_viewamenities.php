<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$image_base_url     = base_url().'../mpvs/images/amenities/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-location-2"></span>
									<span><?= $development->development_name;?> Development Amenities</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($amenities)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>Amenity ID</th>
											<th>Name</th>
											<th>Latitude</th>
											<th>Longitude</th>
											<th></th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_amenity_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_amenity_name" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_latitude" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_longitude" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($amenities as $amenity):?>
										<tr>
											<td><?= $amenity->amenity_id;?></td>
											<td><?= $amenity->amenity_name;?></td>
											<td><?= (float)$amenity->amenity_latitude;?></td>
											<td><?= (float)$amenity->amenity_longitude;?></td>
											<td class="center">
												<button type="button" amenity_id="<?= $amenity->amenity_id;?>" class="btn btn-xs btn-default view_amenity_btn">Update</button>
											</td>
											<td>
												<a href="<?= "{$base_url}deleteamenity/{$amenity->amenity_id}"; ?>">
													<button type="button" amenity_name="<?= $amenity->amenity_name?>" class="delete_amenity_btns btn btn-xs btn-danger">Delete</button>
												</a>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not amenities in the system.</div>
										<a href="<?= $base_url; ?>addamenities/<?= $development->development_id;?>" style="padding-left: 20px;">
											<button typ="button" class="btn btn-info">Add Amenities</button>
										</a>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Dialog -->
<div id="dialog_amenity"></div>