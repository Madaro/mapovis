<?php
$base_url = base_url().'admin/developments/';
$lot_id   = $lot->lot_id;
?>
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<div class="col-lg-9">
			<div class="panel-body">
				<form method="post" class="form-horizontal" action="<?= $base_url; ?>updatelot/<?= $lot_id;?>" role="form">
					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Lot ID:</label>
						<div class="col-lg-9">
						<input type="text" class="form-control" value="<?= $lot_id;?>" disabled>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Lot Number:</label>
						<div class="col-lg-9">
						<input name="lot_number" type="text" class="form-control" value="<?= $lot->lot_number;?>" disabled>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Stage:</label>
						<div class="col-lg-9">
						<input type="text" class="form-control" value="<?= $lot->stage_number;?>" disabled>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Precinct:</label>
						<div class="col-lg-9">
						<input type="text" class="form-control" value="<?= $lot->precinct_number;?>" disabled>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Street Name:</label>
						<div class="col-lg-9">
							<select name="street_name_id" class="form-control" id="status">
							<option></option>
							<?php foreach($street_names as $street_name):?>
								<option value="<?= $street_name->street_name_id;?>" <?= ($lot->street_name_id == $street_name->street_name_id)? 'selected="selected"': ''; ?>><?= $street_name->street_name;?></option>
							<?php endforeach;?>
							</select>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Status:</label>
						<div class="col-lg-9">
							<select name="status" class="form-control" id="status">
							<?php foreach($lot_status as $status):?>
								<option value="<?= $status;?>" <?= ($lot->status == $status)? 'selected="selected"': ''; ?>><?= $status;?></option>
							<?php endforeach;?>
							</select>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="buttons">Price Range:</label>
						<div class="col-lg-9">
							<div class="row">
								<div class="col-lg-2">
								   <div class="input-group">
										<input name="price_range_min" value="<?= $lot->price_range_min;?>" type="text" class="form-control range" id="pricerange-from">
									</div><!-- /input-group -->
								</div>
								<div class="col-lg-1 pricerange-to"> to </div>
								<div class="col-lg-2">
									<div class="input-group">
										<input name="price_range_max" value="<?= $lot->price_range_max;?>" type="text" class="form-control range" id="pricerange-from">
									</div><!-- /input-group -->
								</div>
							</div>
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Latitude:</label>
						<div class="col-lg-9">
						<input name="lot_latitude" value="<?= (float)$lot->lot_latitude;?>" type="text" class="form-control" id="latitude">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Longitude:</label>
						<div class="col-lg-9">
						<input name="lot_longitude" value="<?= (float)$lot->lot_longitude;?>" type="text" class="form-control" id="longitude">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Width (m):</label>
						<div class="col-lg-9">
						<input name="lot_width" value="<?= (float)$lot->lot_width;?>" type="text" class="form-control cal_square_m" id="width_lot">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Depth (m):</label>
						<div class="col-lg-9">
						<input name="lot_depth" value="<?= (float)$lot->lot_depth;?>" type="text" class="form-control cal_square_m" id="depth_lot">
						</div>
					</div><!-- End .form-group  -->

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Square Meters:</label>
						<div class="col-lg-9">
						<input name="lot_square_meters" value="<?= ($lot->lot_square_meters)? $lot->lot_square_meters: $lot->lot_width *  $lot->lot_depth;?>" type="text" class="form-control" id="squaremeters_lot">
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Is a Corner Lot?:</label>
						<div class="col-lg-9">
							<input type="radio" name="lot_corner" value="1" <?= ($lot->lot_corner == 1? 'checked="checked"': '');?> id="lot_corner1" class="lot_corner"><label for="lot_corner1">&nbsp; Yes </label> &nbsp;&nbsp;
							<input type="radio" name="lot_corner" value="0" <?= ($lot->lot_corner == 0? 'checked="checked"': '');?> id="lot_corner0" class="lot_corner"><label for="lot_corner0">&nbsp; No </label>
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Tittle:</label>
						<div class="col-lg-9">
							<input type="radio" name="lot_titled" value="1" <?= ($lot->lot_titled == 1? 'checked="checked"': '');?> id="lot_titled1" class="lot_titled"><label for="lot_titled1">&nbsp; Yes </label> &nbsp;&nbsp;
							<input type="radio" name="lot_titled" value="0" <?= ($lot->lot_titled == 0? 'checked="checked"': '');?> id="lot_titled0" class="lot_titled"><label for="lot_titled0">&nbsp; No </label>
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textareas">Is an Irregular Lot?:</label>
						<div class="col-lg-9">
							<input type="radio" name="lot_irregular" value="1" <?= ($lot->lot_irregular == 1? 'checked="checked"': '');?> id="lot_irregular1" class="lot_irregular"><label for="lot_irregular1">&nbsp; Yes </label> &nbsp;&nbsp;
							<input type="radio" name="lot_irregular" value="0" <?= ($lot->lot_irregular == 0? 'checked="checked"': '');?> id="lot_irregular0" class="lot_irregular"><label for="lot_irregular0">&nbsp; No </label>
						</div>
					</div><!-- End .form-group  --> 

					<div class="form-group" style="padding-top:10px">
						<div class="col-lg-offset-3 col-lg-9">
							<button id="save_lot_changes" type="submit" class="btn btn-info">Save Changes</button>
							<button id="cancel_lot_changes" type="button" class="btn btn-default" style="margin-left:10px;">Cancel</button>
						</div>
					</div><!-- End .form-group  -->
				</form>
			</div>
	</div><!-- End .span6 -->
