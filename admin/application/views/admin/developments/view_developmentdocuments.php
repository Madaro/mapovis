<?php
$base_url = base_url().'salesteam/';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><?= $development->development_name;?> - Document Portal</h3> 
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<?php foreach($types as $type):?>
				<?php $type_title = ucwords(str_replace('_', ' ', $type));?>
				<div class="row">
					<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
										<span style="float:right;margin-right:35px;margin-top:2px;">
											<a href="" class="upload_document_btn" type="<?= $type;?>">
											<button type="button" class="btn btn-primary">Upload</button>
											</a>
										</span>
									<h4>
										<span class="icon16 icomoon-icon-file-3"></span>
										<span><?= $type_title;?></span>
									</h4>
									<!-- <a href="#" class="minimize">Minimize</a> -->
								</div>
								<div class="panel-body noPad">
									<?php if(isset($documents[$type]) && count($documents)):?>
									<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
										<thead>
										<tr>
											<th>File Name</th>
											<th>Description</th>
											<th>Date Uploaded</th>
											<th>Download</th>
											<th>Delete</th>
										</tr>
										</thead>
										<tbody>
											<?php foreach($documents[$type] as $document):?>
											<tr>
												<td>
													<a title="Download File" href="<?= base_url('../mpvs/documents/'.$document->file_name);?>" target="_blank">
														<?= $document->file_name;?>
													</a>
												</td>
												<td><?= $document->description;?></td>
												<td><?= date('d-m-Y', strtotime($document->upload_date));?></td>
												<td>
													<a title="Download File" href="<?= base_url('../mpvs/documents/'.$document->file_name);?>" target="_blank"><button class="btn btn-default btn-xs">View Document</button></a>
												</td>
												<td>
													<a title="Delete Document" href="<?= $base_url.'deletedevelopmentdocument/'.$document->development_id.'/'.$document->development_document_id;?>"><button class="delete_document_btn btn btn-danger btn-xs">Delete Document</button></a>
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
									<?php else:?>
										<h6 style="padding-top:10px; padding-left:10px;"> No Documents found</h6>
									<?php endif;?>
								</div>

							</div><!-- End .panel -->

						</div><!-- End .span6 -->

				</div><!-- End .row -->
				<h4>&nbsp;</h4> 
				<?php endforeach;?>

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<div id="dialog_document"></div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/forms/validate/jquery.validate.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() { 	
		$('.upload_document_btn').on('click', function(e){
			e.preventDefault();
			var development_id = '<?= $development->development_id;?>';
			var type = $(this).attr('type');
			showLoading();
			$.post('<?= $base_url; ?>uploaddevelopmentdocument/'+development_id+'/?type_name='+type,
				function(data,status){
					if(status === 'success'){
						$('#dialog_document').html(data);
						$('#dialog_document').dialog({
							title: 'Upload Document',
							height: 400,
							width: 680,
							modal: true,
							resizable: false,
							dialogClass: 'loading-dialog',
							close: function(ev,ui){remove_tinymce();$('#dialog_house').html('');}
						});
						$("input, textarea, select").not('.nostyle').uniform();
						$('#dialog_document').dialog('open');
						hideLoading();
						$('#cancel_changes').click(function() {
							$('#dialog_document').dialog('close');
						});
						$('#upload_document_form').validate({
							rules: {
								document_file: {
									required: true
								},
								description: {
									required: true,
									maxlength: 80
								},
							},
							messages: {
								description: {
									required: 'Please provide the document description',
									maxlength:'The maximum for the Description is 80 characters'
								},
							},
							submitHandler: function(form) {
								// check if there is at least one image uploaded
								var input = $('#document_file')[0];
								console.log(input.files);
								console.log($('#document_file')[0]);
								if (input.files && input.files[0]){
									if(!validateimagefiletype(input.files[0].type)){
										messageAlert('<div title="Fail">Please upload a valid document.</div>');
										return false;
									}
									showLoading();
									form.submit();
									return true;
								}
								else{
									messageAlert('<div title="Fail">Please upload the document.</div>');
									return false;
								}
							}
						});
						return true;
					}
					messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
			});
		});

		$('.delete_document_btn').click(function() {
			if(!confirm('Are you sure you want to delete the document?')){
				return false;
			}
			showLoading();
		});

		function validateimagefiletype(filetype){
			switch(filetype){
				case 'image/png':
				case 'image/gif':
				case 'image/jpeg':
				case 'image/pjpeg':
				case 'application/pdf':
				case 'application/vnd.ms-excel':
				case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
				case 'application/msword':
				case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
				case 'text/comma-separated-values':
					return true;
					break;
				default:
					return false;
					break;
			}
			return true;
		}
	});
</script>