<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$manage_links       = $base_url.'managedevelopmentlinks/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<div class="col-lg-10">
			<div class="panel-body">
			<?= $validation_msg;?>
			<form method="post" class="form-horizontal" action="<?= $base_url; ?>editdevelopmentlink/<?= $development_id1;?>/<?= $development_id2;?>" role="form">
				<div class="form-group">
					<label class="col-lg-4 control-label">Development:</label>
					<div class="col-lg-5">
						<?= $development_link->development_name;?>
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-4 control-label">Distance:</label>
					<div class="col-lg-4">
						<input name="distance" type="text" class="form-control" value="<?= round($development_link->distance,5);?>">
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-4 control-label">Suburb:</label>
					<div class="col-lg-8">
						<input name="suburb" type="text" class="form-control" id="suburb" value="<?= $development_link->suburb;?>">
					</div>
				</div><!-- End .form-group  -->

				<div class="col-lg-offset-6">
					<button id="save_link_changes" type="submit" class="btn btn-info">Update Link</button>
					<button id="cancel_link_changes" type="button" class="btn btn-default">Cancel</button>
					<br><br>
				</div>
				<input name="development_id" type="hidden" value="<?= $development->development_id;?>">
			</form>
		</div>
	</div><!-- End .panel -->