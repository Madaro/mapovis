<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->

				<div class="heading">
					 <h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3> 
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4>
										<span class="icon16 entypo-icon-settings"></span>
										<span>Duplicate Development</span>
									</h4>
								</div>
								<div class="panel-body">
								<?= $validation_msg;?>
								<form id="generalsettinsForm" method="post" class="form-horizontal" action="<?= $base_url; ?>duplicatedevelopment/<?= $development->development_id;?>" role="form" enctype="multipart/form-data">
									<div class="form-group">
										<label class="col-lg-3 control-label" for="required">Name of the New Development:</label>
										<div class="col-lg-4">
											<input type="text" class="form-control" id="nameofthedevelopment" name="development_name" value="<?= set_value('development_name', $development->development_name.' (Copy)');?>">
										</div>
									</div><!-- End .form-group-->

									<div class="col-lg-offset-2">
									 <br>
										<button id="submit_form_btn" type="submit" class="btn btn-info">Duplicate</button>
										<a href="<?= $manage_development;?>" style="padding-left:10px;"><button type="button" class="btn btn-default">Cancel</button></a>
										<br><br>
									</div>
								</form>
							</div>

							</div><!-- End .panel -->

						</div><!-- End .span3 -->

				</div><!-- End .row --> 

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
$('#submit_form_btn').on('click', function(){
	showLoading();
});
</script>