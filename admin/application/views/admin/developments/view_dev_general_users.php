<?php
$base_url           = base_url().'admin/developments/';
$manage_development = $base_url.'managedevelopmentid/'.$development->development_id;
$validtion_errors   = validation_errors();
$validation_msg     = (!empty($validtion_errors))? '<div class="alert alert-danger">'.$validtion_errors.'</div>': '';
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					 <h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3> 
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span>Development General Users</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>User ID</th>
											<th>Name</th>
											<th>Email</th>
											<th>Developer</th>
											<th>Office</th>
											<th>Active</th>
										</tr>
										<tr>
										  <td><input type="text" name="search_user_id" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_name" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_email" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_developer" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td><input type="text" name="search_office" placeholder="" class="search_init" style="width: 100%;" /></td>
										  <td></td>
										</tr>
									</thead>
									<tbody>

									<?php // Add in the USERS from the DB query to the Table
										foreach ($users_list as $user):?>
										<tr>
											<td><?= $user->user_id;?></td>
											<td><?= $user->name;?></td>
											<td><?= $user->email;?></td>
											<td><?= $user->developer;?></td>
											<td><?= $user->office;?></td>
											<td><input type="checkbox" name="general_user[]" value="1" class="general_users" user_id="<?= $user->user_id;?>" <?= (isset($general_users[$user->user_id]))? 'checked="checked"': ''?>/></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script type="text/javascript">
$('.general_users').on('change', function(){
	showLoading();
	var user_id = $(this).attr('user_id');
	var url = '';
	if($(this).prop('checked') == true){
		url = '<?= base_url().'admin/developments/adddevgeneralusers/'.$development->development_id;?>/'+user_id;
	}
	else{
		url = '<?= base_url().'admin/developments/deletedevgeneralusers/'.$development->development_id;?>/'+user_id;
	}
	$.post(url,{
		user_id: user_id
		},
		function(data,status){
			var alert_msg = '';
			hideLoading();
			if(status === 'success'){alert_msg = '<div title="Success">'+data+'</div>';}
			else{ alert_msg = '<div title="Fail">'+data+'</div>';}
			messageAlert(alert_msg);
	});	
})
</script>