<?php
$base_url       = base_url().'salesteam/';
?>

<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
<!-- Dialog -->
<div class="col-lg-10">
	<div class="panel-body">
		<form id="upload_document_form" method="post" class="form-horizontal" action="<?= $base_url; ?>uploaddevelopmentdocument/<?= $development->development_id;?>" role="form" ENCTYPE="multipart/form-data">
			<div class="form-group">
				<label class="col-lg-3 control-label" >Development:</label>
				<div class="col-lg-9">
					<?= $development->development_name;?>
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" >Document File:</label>
				<div class="col-lg-9">
					<div >
						<input type="file" name="document_file" class="form-control" value="" id="document_file">
						<label class="error" generated="true" for="document_file" style="display: none;"></label>
					</div>
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" >Description:</label>
				<div class="col-lg-9">
					<div class="form-row">
						<input type="text" name="description" class="form-control" value="" maxlength="80">
					</div>
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group">
				<label class="col-lg-3 control-label" >Document Type:</label>
				<div class="col-lg-6">
				   <select id="document_type" name="document_type" class="form-control">
						<?php foreach($types as $type):?>
					   <option value="<?= $type?>" <?= ($type == $default_type)? 'selected="selected"': '';?>><?= ucwords(str_replace('_', ' ', $type));?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div><!-- End .form-group  -->

			<div class="form-group" style="padding-top:10px">
				<div class="col-lg-offset-3 col-lg-9">
					<button id="save_changes" type="submit" class="btn btn-info" >Upload Document</button>
					<button id="cancel_changes" type="button" class="btn btn-default">Cancel</button>
				</div>
			</div><!-- End .form-group  -->

		</form>
	</div>
</div><!-- End .span6 -->
