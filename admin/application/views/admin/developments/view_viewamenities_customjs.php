<script>
function launchEditor(id, src, filetype, dialog, amenity_id, force_crop){
	var editor_params = {
		image: id,
		url: src,
		fileFormat: filetype,
		onSave: function(imageID, newURL) {
			addNewImageHtml(amenity_id, newURL, newURL);
			var img = document.getElementById(imageID);
			img.src = newURL;
			featherEditor.close();
		},
	};
	if(force_crop){
		editor_params.forceCropPreset = ['Width: 350px, Height: 222px','350x222'];
		editor_params.initTool = 'crop';
		editor_params.cropPresetsStrict = 'true';
	}
	else{
		editor_params.tools = 'enhance,effects,frames,stickers,focus,brightness,contrast,saturation,warmth,sharpness,colorsplash,draw,text,redeye,whiten,blemish';
	}
	featherEditor.launch(editor_params);
}
function initOpenDialog(amenity_id){
	initViewEntity();
	showhide_AddNew(amenity_id);
	init_tinymce();
	$("input, textarea, select, button").attr('tabindex', '-1');
	$("input, textarea, select").not('.nostyle').uniform();
	$('.add_image_btn').on('click', function(e){
		var image_field = $(this).attr('image_field');
		var amenity_id = $(this).attr('amenity_id');
		var image_field_obj = $('#'+image_field);
		var input = image_field_obj[0];
		if (input.files && input.files[0]) {
			if(!validateimagefiletype(input.files[0].type)){
				/* shows dialog message */
				return false;
			}
			var reader = new FileReader();
			// set where you want to attach the preview
			reader.target_elem = $(input).parent().find('preview');
			reader.onload = function (e) {
				var force_crop = true;
				var img = document.createElement("img");
				img.onload = function(){
					if(img.width == 350 && img.height == 222){
						force_crop = false;
					}
					$("#imageupload").attr('src', e.target.result);

					launchEditor('imageupload', e.target.result, input.files[0].type, $('#dialog_amenity'), amenity_id, force_crop);
				};
				img.src = e.target.result;
			};
			reader.readAsDataURL(input.files[0]);
		}
	});
}
$('#dialog_amenity').dialog({ autoOpen: false });
$('.view_amenity_btn').on('click', function(){
	var amenity_id = $(this).attr('amenity_id');
	showLoading();
	$.post('<?= base_url();?>admin/developments/viewamenity/'+amenity_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_amenity').html(data);
				$('#dialog_amenity').dialog({
					title: 'Update Amenity',
					height: 600,
					width: 1000,
					modal: true,
					resizable: false,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_amenity').html('');}
				});
				$('#dialog_amenity').dialog('open');
				initOpenDialog(amenity_id);
				hideLoading();
				$('.save_amenity_changes').click(function() {
					showLoading();
				});
				$('.cancel_amenity_changes').click(function() {
					$('#dialog_amenity').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
$('.delete_amenity_btns').click(function() {
	var amenity_name = $(this).attr('amenity_name');
	if(!confirm('Are you sure you want to delete amenity "'+amenity_name+'"?')){
		return false;
	}
	showLoading();
});
</script>