<?php
$base_url       = base_url().'admin/developments/';
$image_base_url = base_url().'../mpvs/images/amenities/';
$amenity_id     = $amenity->amenity_id;
?>
	<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
	<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
	<!-- Image field necesary to call Aviary and crop the images -->
	<img id='imageupload' src='' style="display:none"/>
	<div class="col-lg-9">
		<div class="panel-body">
			<form method="post" class="form-horizontal" action="<?= $base_url; ?>updateamenity/<?= $amenity_id;?>" role="form">
				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Amenity ID:</label>
					<div class="col-lg-9">
					<input type="text" class="form-control" value="<?= $amenity->amenity_id; ?>" disabled>
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Type:</label>
					<div class="col-lg-9">
					   <select name="amenity_type_id" class="form-control">
							<option></option>
							<?php foreach($amenity_types as $amenity_type):?>
								<option value="<?= $amenity_type->amenity_type_id;?>" <?= ($amenity->amenity_type_id == $amenity_type->amenity_type_id)? 'selected="selected"': ''?>><?= $amenity_type->amenity_type_name;?></option>
							<?php endforeach;?>
						</select>
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Name:</label>
					<div class="col-lg-9">
					<input name="amenity_name" type="text" class="form-control" value="<?= $amenity->amenity_name; ?>">
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Latitude:</label>
					<div class="col-lg-9">
					<input name="amenity_latitude" type="text" class="form-control" value="<?= (float)$amenity->amenity_latitude;?>">
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Longitude:</label>
					<div class="col-lg-9">
					<input name="amenity_longitude" type="text" class="form-control" value="<?= (float)$amenity->amenity_longitude;?>">
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Description:</label>
					<div class="col-lg-9">
						<div class="form-row">
							<textarea class="tinymce" name="amenity_description"><?= $amenity->amenity_description;?></textarea>
						</div>
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">More Info Link:</label>
					<div class="col-lg-9">
					<input name="amenity_moreinfo_url" type="text" class="form-control" value="<?= $amenity->amenity_moreinfo_url;?>">
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="textareas">Pictures:</label>
					<div class="col-lg-9">
						<div id="add_image_section<?= $amenity_id;?>">
							<input type="file" name="image_file" id="image_file<?= $amenity_id;?>" class="image_files" value="" accept="image/*" >
							<button type="button" id="add_image_btn" class="add_image_btn btn btn-success" image_field="image_file<?= $amenity_id;?>" amenity_id="<?= $amenity_id;?>" href="#">Add & Edit Photo</button>
							<br/><br/>
						</div>

						<div class="alert alert-info">
							Drag the pictures to change the order in which they will appear. <br/> If the web page only shows one image then the first image will be displayed.
						</div>
						<div id="img_container">
							<ul class="sortable" id="sortable_<?= $amenity_id;?>">
								<?php for($xyz = 1; $xyz <= 5; $xyz++):
									$image_field = 'amenity_picture'.$xyz;
									$image_name  = $amenity->$image_field;
									if(empty($image_name)){
										break;
									}
									$image_url   = "{$image_base_url}{$image_name}";
								?>
									<li class="ui-state-default" >
										<input name="image_names[]" type="text" value="<?= $image_name;?>" style="display:none">
										<img src="<?= $image_url;?>" width="135" height="96" />
										<button type="button" amenity_id="<?= $amenity_id;?>" image_id="<?= $image_name?>" class="btn btn-danger btn-xs deleteimage" href="#">Delete</button>
									</li>
								<?php endfor;?>
							</ul>
						</div>
					</div>
				</div><!-- End .form-group  -->

				<div class="form-group" style="padding-top:10px">
					<div class="col-lg-offset-3 col-lg-9">
						<button type="submit" class="save_amenity_changes btn btn-info">Save Changes</button>
						<button type="button" class="cancel_amenity_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
					</div>
				</div><!-- End .form-group  -->

				<div class="col-lg-11 alert alert-warning">If you want to use a completely custom design you can provide an IFrame.</div>
				<div class="form-group">
					<label class="col-lg-3 control-label" for="required">Amenities Iframe HTML:</label>
					<div class="col-lg-9">
						<input type="text" class="form-control" name="amenity_iframe_html" value="<?= htmlspecialchars($amenity->amenity_iframe_html, ENT_QUOTES);?>">
					</div>
				</div><!-- End .form-group-->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="required">Show Scrollbar:</label>
					<div class="col-lg-5">
						<input type="radio" name="amenity_iframe_scrollbar" value="1" <?= ($amenity->amenity_iframe_scrollbar == 1? 'checked="checked"': '');?> id="amenity_iframe_scrollbar1" class="amenity_iframe_scrollbar"><label for="amenity_iframe_scrollbar1"> On </label> &nbsp;&nbsp;
						<input type="radio" name="amenity_iframe_scrollbar" value="0" <?= ($amenity->amenity_iframe_scrollbar == 0? 'checked="checked"': '');?> id="amenity_iframe_scrollbar0" class="amenity_iframe_scrollbar"><label for="amenity_iframe_scrollbar0"> Off </label>
					</div>
				</div><!-- End .form-group  --> 

				<div class="form-group" style="padding-top:10px">
					<div class="col-lg-offset-3 col-lg-9">
						<button type="submit" class="save_amenity_changes btn btn-info">Save Changes</button>
						<button type="button" class="cancel_amenity_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
					</div>
				</div><!-- End .form-group  -->

			</form>

		</div>
	</div><!-- End .span6 -->
