<?php
$base_url = base_url().'admin/markers/';
?>
<div id="qLoverlaymessageDialog" class="qLoverlaymessage" style="display:none;"></div>
<div id="qLmessageDialog" class="qLmessage" style="display:none;"></div>
<div class="col-lg-14">
		<div class="panel-body">
			<form method="post" class="form-horizontal" action="<?= $base_url; ?>updateminimarker/<?= $mini_marker->id;?>" role="form">
				<div class="form-group">
					<label class="col-lg-3 control-label" for="buttons">Coordinates:</label>
					<div class="col-lg-9">
						<div class="row">
							<div class="col-lg-4">
								<div class="input-group">
									<input name="marker_latitude" type="text" class="form-control" style="width: 150px !important;" value="<?= (float)$mini_marker->marker_latitude;?>" placeholder="latitude">
								</div><!-- /input-group -->
							</div>
							<div class="col-lg-4">
								<div class="input-group">
									<input name="marker_longitude" type="text" class="form-control" style="width: 150px !important;" value="<?= (float)$mini_marker->marker_longitude;?>" placeholder="longitude">
								</div><!-- /input-group -->
							</div>
						</div>
					</div>
				</div><!-- End .form-group-->

				<div class="form-group">
					<label class="col-lg-3 control-label" for="required">Image URL:</label>
					<div class="col-lg-8">
						<input type="text" name="marker_image_url" class="form-control" value="<?= $mini_marker->marker_image_url;?>" >
					</div>
				</div><!-- End .form-group  --> 

				<div class="form-group">
					<label class="col-lg-3 control-label" for="required">Width:</label>
					<div class="col-lg-2">
						<input type="text" name="marker_width" class="form-control" value="<?= $mini_marker->marker_width;?>" >
					</div>
				</div><!-- End .form-group  --> 

				<div class="form-group">
					<label class="col-lg-3 control-label" for="required">Height:</label>
					<div class="col-lg-2">
						<input type="text" name="marker_height" class="form-control" value="<?= $mini_marker->marker_height;?>" >
					</div>
				</div><!-- End .form-group  --> 

				<div class="form-group">
					<label class="col-lg-3 control-label" for="required">Show From Zoom Level:</label>
					<div class="col-lg-2">
						<input type="text" name="marker_zoomlevel_show_from" class="form-control" value="<?= $mini_marker->marker_zoomlevel_show_from;?>" >
					</div>
				</div><!-- End .form-group  --> 

				<div class="form-group">
					<label class="col-lg-3 control-label" for="required">Show To Zoom Level:</label>
					<div class="col-lg-2">
						<input type="text" name="marker_zoomlevel_show_to" class="form-control" value="<?= $mini_marker->marker_zoomlevel_show_to;?>" >
					</div>
				</div><!-- End .form-group  --> 

				<div class="form-group">
					<label class="col-lg-3 control-label" for="required">Link:</label>
					<div class="col-lg-8">
						<input type="text" name="marker_link" class="form-control" value="<?= $mini_marker->marker_link;?>" >
					</div>
				</div><!-- End .form-group  --> 

				<div class="form-group">
					<label class="col-lg-3 control-label" for="required">Link Target:</label>
					<div class="col-lg-8">
						<input type="text" name="marker_link_target" class="form-control" value="<?= $mini_marker->marker_link_target;?>" >
					</div>
				</div><!-- End .form-group  --> 

				<div class="form-group" style="padding-top:10px">
					<div class="col-lg-offset-3 col-lg-9">
						<button type="submit" class="save_changes btn btn-info">Save Changes</button>
						<button type="button" class="cancel_changes btn btn-default" style="margin-left: 10px;">Cancel</button>
					</div>
				</div><!-- End .form-group  -->
			</form>
		</div>
</div><!-- End .span6 -->
