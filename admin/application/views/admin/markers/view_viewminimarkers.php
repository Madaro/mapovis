<?php
$base_url           = base_url().'admin/markers/';
$manage_development = base_url('admin/developments/managedevelopmentid/'.$development->development_id);
?>
		<!--Body content-->
		<div id="content" class="clearfix">
			<div class="contentwrapper"><!--Content wrapper-->
				<div class="heading">
					<h3><a href="<?= $manage_development?>">Manage <?= $development->development_name;?></a> (<?= $development->developer;?>)</h3>
					<div class="resBtnSearch">
						<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
					</div>
				</div><!-- End .heading-->

				<!-- Build page from here: -->
				<?= $alert_message;?>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default gradient">
							<div class="panel-heading">
								<h4>
									<span class="icon16 icomoon-icon-location-2"></span>
									<span><?= $development->development_name;?> Mini Markers</span>
								</h4>
							</div>
							<div class="panel-body noPad clearfix">
								<?php if(count($mini_markers)):?>
								<table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Image URL</th>
											<th>Latitude</th>
											<th>Longitude</th>
											<th>Width</th>
											<th>Height</th>
											<th>Show From Zoom Level</th>
											<th>Show To Zoom Level</th>
											<th>Link</th>
											<th>Link Target</th>
											<th></th>
											<th></th>
										</tr>
										<tr>
											<td><input type="text" name="search_id" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_image_url" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_latitude" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_longitude" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_width" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_height" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_zoomlevel_show_from" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_zoomlevel_show_to" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_link" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td><input type="text" name="search_link_target" placeholder="" class="search_init" style="width: 100%;" /></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($mini_markers as $mini_marker):?>
										<tr>
											<td><?= $mini_marker->id;?></td>
											<td><?= $mini_marker->marker_image_url;?></td>
											<td><?= (float)$mini_marker->marker_latitude;?></td>
											<td><?= (float)$mini_marker->marker_longitude;?></td>
											<td><?= $mini_marker->marker_width;?></td>
											<td><?= $mini_marker->marker_height;?></td>
											<td><?= $mini_marker->marker_zoomlevel_show_from;?></td>
											<td><?= $mini_marker->marker_zoomlevel_show_to;?></td>
											<td><?= $mini_marker->marker_link;?></td>
											<td><?= $mini_marker->marker_link_target;?></td>
											<td class="center">
												<button type="button" type_id="<?= $mini_marker->id;?>" class="btn btn-xs btn-default view_type_btn">Edit</button>
											</td>
											<td>
												<a href="<?= "{$base_url}deleteminimarker/{$mini_marker->id}"; ?>">
													<button type="button" class="delete_mini_marker_btn btn btn-xs btn-danger">Delete</button>
												</a>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<?php else:?>
									<div class="panel-body">
										<div class="alert alert-warning">There are not mini markers in the system.</div>
									</div>
								<?php endif;?>
							</div>

						</div><!-- End .panel -->

					</div><!-- End .span12 -->

				</div><!-- End .row -->
				<div>
					<a href="<?= $base_url; ?>addminimarker/<?= $development->development_id;?>">
						<button type="button" class="btn btn-info">Add Mini Marker</button>
					</a>
				</div>

				<!-- Page end here -->

			</div><!-- End contentwrapper -->
		</div><!-- End #content -->

<!-- Dialog -->
<div id="dialog_minimarker"></div>