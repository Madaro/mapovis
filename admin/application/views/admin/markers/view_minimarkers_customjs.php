<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/uploadimage.js"></script>
<script>
$('#dialog_minimarker').dialog({ autoOpen: false });

$('.view_type_btn').on('click', function(){
	var type_id = $(this).attr('type_id');
	showLoading();
	$.post('<?= base_url();?>admin/markers/viewminimarker/'+type_id,
		function(data,status){
			if(status === 'success'){
				$('#dialog_minimarker').html(data);
				$('#dialog_minimarker').dialog({
					title: 'Update Mini Marker',
					height: 550,
					width: 790,
					modal: true,
					resizable: false,
					dialogClass: 'loading-dialog',
					close: function(ev,ui){$('#dialog_minimarker').html('');}
				});
				$('#dialog_minimarker').dialog('open');
				hideLoading();
				$('.save_changes').click(function() {
					showLoading();
				});
				$('.cancel_changes').click(function() {
					$('#dialog_minimarker').dialog('close');
				});
				return true;
			}
			messageAlert('<div title="Fail">It was not possible to load the view please try again.</div>');
	});
});
$('.delete_mini_marker_btn').click(function() {
	if(!confirm('Are you sure you want to delete the mini marker?')){
		return false;
	}
	showLoading();
});
</script>