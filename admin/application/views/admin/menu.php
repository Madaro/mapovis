   <div id="wrapper">

        <!--Responsive navigation button-->  
        <div class="resBtn">
            <a href="#"><span class="icon16 minia-icon-list-3"></span></a>
        </div>
        
    

        <!--Sidebar background-->
        <div id="sidebarbg"></div>
        <!--Sidebar content-->
        <div id="sidebar">

                   

            <div class="sidenav">

             

                <div class="mainnav">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>admin/admindashboard"><span class="icon16 icomoon-icon-home-4"></span>Home</a></li>
               
                        <li>
                            <a href="#"><span class="icon16 icomoon-icon-home-7"></span>Developments</a>
                            <ul class="sub">
                                <li><a href="<?php echo base_url(); ?>admin/developments/managedevelopments"><span class="icon16 icomoon-icon-file-check"></span>Manage Developments</a></li>
                                <li><a href="adddevelopment.html"><span class="icon16 icomoon-icon-file-plus"></span>Add Development</a></li>                                
                             </ul>
                        </li>

                        <li>
                            <a href="<?php echo base_url(); ?>admin/users"><span class="icon16 icomoon-icon-users"></span>Users</a>
                        </li>                        


                        <li>
                            <a href="#"><span class="icon16 icomoon-icon-file-3"></span>Reports</a>
                            <ul class="sub">
                                <li><a href="changelog.html"><span class="icon16 icomoon-icon-file-7"></span>Lots Change Log</a></li>
                                <li><a href="reminderlog.html"><span class="icon16 icomoon-icon-file-7"></span>Reminder Log</a></li>
                                <li><a href="overdueuserreport.html"><span class="icon16 icomoon-icon-file-7"></span>Overdue User Report</a></li>
                                <li><a href="problemuserreport.html"><span class="icon16 icomoon-icon-file-7"></span>Problem User Report</a></li>
                            </ul>
                        </li>


                        <li>
                            <a href="#"><span class="icon16  icomoon-icon-cog-4"></span>Settings</a>
                            <ul class="sub">
                                <li><a href="welcomemessage.html"><span class="icon16 icomoon-icon-bubble-5"></span>Welcome Message</a></li>  
                                
                            </ul>
                        </li>    


                        <li>
                            <a href="https://mapovis.zendesk.com/agent/#/dashboard" target="_blank"><span class="icon16 icomoon-icon-bubbles-3"></span>Support</a>
                        </li>
                        
                    </ul>
                </div>
            </div><!-- End sidenav -->

            
        </div><!-- End #sidebar -->