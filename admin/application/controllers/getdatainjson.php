<?php
if( ! defined('BASEPATH')) exit('No direct script access allowed');

class Getdatainjson extends CI_Controller {

	function __construct() 
	{
		parent::__construct(); 
	}

	function development($development_id)
	{
		//DB Query - Get Development data for development_id
        $this->db->select('developments.*,multiple_developments_cluster_circles.*, update_messages.update_message as message, lot_icons.*,'); 
        $this->db->join('multiple_developments_cluster_circles', 'multiple_developments_cluster_circles.development_id = developments.development_id', 'left');
        $this->db->join('update_messages', 'update_messages.development_id = developments.development_id', 'left');
        $this->db->join('lot_icons', 'lot_icons.development_id = developments.development_id', 'left');  
        
        if($development_id){
            $this->db->where('developments.development_id', $development_id);
        }
        $query  = $this->db->get('developments');
        $result = $query->result();

        $this->output->set_content_type('application/json'); 
        $this->output->set_output(json_encode($result));
	}
    
    function getmapstyles($development_id)
    {
        $this->db->select('developments.custom_googlemaps_style');
        $this->db->where('developments.development_id', $development_id);
        $custom_googlemaps_style = $this->db->get('developments')->result_array(); 
        $this->output->set_content_type('application/json'); 
        $this->output->set_output(json_encode($custom_googlemaps_style));  
    }

    function all_activeDevelopments(){
        
        //DB Query - Get Development data for active = 1
        $this->db->select('developments.*,client_website_links.*'); 
        $this->db->join('client_website_links', 'client_website_links.development_id = developments.development_id', 'left');
        $developmentarray = $this->db->get_where('developments', array('active' => 1))->result_array();                                
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($developmentarray));
            
    }
    
    function roads($development_id){
        $this->db->select('*');
        $this->db->where('polylines.developmentID',$development_id);
        $roadsarray = $this->db->get('polylines')->result_array();
        $this->output->set_content_type('application/json'); 
        $this->output->set_output(json_encode($roadsarray));
    }
    
	function developer($developer_id)
	{
		$this->load->model('Model_development', '', TRUE);
		$developmentsarray = $this->Model_development->getDevelopmentsByDeveloper($developer_id);

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($developmentsarray));
	}

	function stages($development_id)
	{
		$this->load->model('Model_stage', '', TRUE);
		$stages        = $this->Model_stage->getStages($development_id);
		$full_title    = $this->multiplePrecincts($development_id);

		$stage_results = array();
		foreach($stages as $stage){
			$this->db->select('COUNT(lots.lot_id) AS total');
			$this->db->where('lots.stage_id', $stage->stage_id);
			$this->db->where('lots.status', 'Available');
			$available_lots = $this->db->get('lots')->row()->total;

			$this->db->select('COUNT(lots.lot_id) AS total');
			$this->db->where('lots.stage_id', $stage->stage_id);
			$this->db->where('lots.status', 'Sold');
			$sold_lots = $this->db->get('lots')->row()->total;

			$stage_results[] = array(
				'stage_id'               => $stage->stage_id,
				'precinct_id'            => $stage->precinct_id,
				'precinct_number'        => $stage->precinct_number,
				'stage_title'            => ($full_title)? $stage->precinct_number.'-'.$stage->stage_number: $stage->stage_number,
				'stage_number'           => $stage->stage_number,
				'stage_icon'             => $stage->stage_icon,
				'stage_icon_width'       => $stage->stage_icon_width,
				'stage_icon_height'      => $stage->stage_icon_height,
				'stage_code'             => $stage->stage_code,
				'stage_center_latitude'  => (float)$stage->stage_center_latitude,
				'stage_center_longitute' => (float)$stage->stage_center_longitute,
				'stage_zoomlevel'        => $stage->stage_zoomlevel,
				'stage_icon_latitude'	 => $stage->stage_icon_latitude,
				'stage_icon_longitude'	 => $stage->stage_icon_longitude,
				'stage_icon_zoomlevel_show' => $stage->stage_icon_zoomlevel_show,
				'stage_icon_zoomlevel_hide'	=> $stage->stage_icon_zoomlevel_hide,
				'stage_name'			=> $stage->stage_name,
				'display_stage_name'	=> $stage->display_stage_name,
				'stage_polygon_coords'	=> $stage->stage_polygon_coords,
				'lots_available'         => $available_lots,
				'lots_sold'              => $sold_lots,
                'stage_polygon_coords_clickable_at_zoom_level' => $stage->stage_polygon_coords_clickable_at_zoom_level,
                'stage_polygon_coords_unclickable_at_zoom_level' => $stage->stage_polygon_coords_unclickable_at_zoom_level,
				'percentage_lots_sold'   => (($sold_lots)? round(100 * $sold_lots/($sold_lots + $available_lots),2): 0).'%',
			);
		}

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($stage_results));
	}

	function lots($development_id)
	{
		//DB Query - Get Lots data for lots from the development (development_id)
		$this->db->select('lots.*, lots.lot_titled AS titled, stages.*, precincts.*,lot_icons.*, CONCAT(CEIL(price_range_min/1000), "K") AS price_range_min, CONCAT(CEIL(price_range_max/1000), "K") AS price_range_max ', FALSE);
		$this->db->select('REPLACE(ROUND(lots.lot_width,2),".00","") AS lot_width', FALSE);
		$this->db->select('IF(lots.lot_corner = 1, "Corner", "Normal") AS type', FALSE);
		$this->db->select('lots.lot_irregular AS irregular');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left');
        $this->db->join('lot_icons', 'lot_icons.development_id = precincts.development_id', 'left');
		$lotsarray = $this->db->get_where('lots', array('precincts.development_id' => $development_id, 'lots.status' => 'Available'))->result_array(); 
		foreach($lotsarray as $index => $row){
			$lotsarray[$index]['lot_width'] = round($row['lot_width'],2);
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($lotsarray));
	}
    
    
    function deposited_lots($development_id)
    {
        //DB Query - Get Lots data for lots from the development (development_id)
        $this->db->select('lots.*, lots.lot_titled AS titled, stages.*, precincts.*, lot_icons.*, CONCAT(CEIL(price_range_min/1000), "K") AS price_range_min, CONCAT(CEIL(price_range_max/1000), "K") AS price_range_max ', FALSE);
        $this->db->select('REPLACE(ROUND(lots.lot_width,2),".00","") AS lot_width', FALSE);
        $this->db->select('IF(lots.lot_corner = 1, "Corner", "Normal") AS type', FALSE);
        $this->db->select('lots.lot_irregular AS irregular');
        $this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
        $this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left');
        $this->db->join('lot_icons', 'lot_icons.development_id = precincts.development_id', 'left');
        $lotsarray = $this->db->get_where('lots', array('precincts.development_id' => $development_id, 'lots.status' => 'Deposited'))->result_array(); 
        foreach($lotsarray as $index => $row){
            $lotsarray[$index]['lot_width'] = round($row['lot_width'],2);
        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($lotsarray));
    }
    
    function all_lots($development_id)
    {
        //DB Query - Get Lots data for lots from the development (development_id)
        $this->db->select('lots.*, stages.*, precincts.*, lot_icons.*, CONCAT(CEIL(price_range_min/1000), "K") AS price_range_min, CONCAT(CEIL(price_range_max/1000), "K") AS price_range_max ', FALSE);
        $this->db->select('REPLACE(ROUND(lots.lot_width,2),".00","") AS lot_width', FALSE);
		$this->db->select('IF(lots.lot_corner = 1, "Corner", "Normal") AS type', FALSE);
		$this->db->select('lots.lot_irregular AS irregular');
        $this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
        $this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left');
        $this->db->join('lot_icons', 'lot_icons.development_id = precincts.development_id', 'left'); 
        $lotsarray = $this->db->get_where('lots', array('precincts.development_id' => $development_id))->result_array(); 
        foreach($lotsarray as $index => $row){
            $lotsarray[$index]['lot_width'] = round($row['lot_width'],2);
        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($lotsarray));
    }

	function lotsearch($stage_id)
	{
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		/*$this->db->where_in('lots.status', array('Available', 'Sold'));   */
        $this->db->where_in('lots.status', array('Available')); 
		$this->db->where('lots.stage_id', $stage_id);

		$filter_fields = array(
			'min_price'     => 'price_range_min >=',
			'min_lot_width' => 'lot_width >=',
			'max_lot_width' => 'lot_width <=',
			'min_lot_size'  => 'lot_square_meters >=',
			'max_lot_size'  => 'lot_square_meters <='
		);
		$form_data = $this->input->get();
		if($form_data){
			foreach($filter_fields as $field_id => $db_field){
				if(isset($form_data[$field_id]) && $form_data[$field_id]){
					$this->db->where($db_field, $form_data[$field_id]);
				}
			}
			if(isset($form_data['max_price']) && $form_data['max_price']){
				$max_value = (float)$form_data['max_price'];
				$this->db->where("( price_range_min <= {$max_value} OR price_range_max <= {$max_value})");
			}
		}
		$lotsarray = $this->db->get('lots')->result_array();

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($lotsarray));
	}

	function lotsearchstages($development_id)
	{
		$stage_title    = ($this->multiplePrecincts($development_id))? 'CONCAT(precincts.precinct_number, "-", stages.stage_number)': 'stages.stage_number';
		$this->db->select("stages.stage_id, stages.stage_number, {$stage_title} AS stage_title", FALSE);
		$this->db->select('stages.stage_code, stages.stage_name, stages.stage_center_latitude, stages.stage_center_longitute, stages.stage_zoomlevel, ');
		$this->db->select('COUNT(lots.lot_id) AS matches ');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
	/*	$this->db->where_in('lots.status', array('Available', 'Sold'));*/
        $this->db->where_in('lots.status', array('Available'));
		$this->db->where('precincts.development_id', $development_id);
		$this->db->group_by('stages.stage_id');
		$this->db->order_by('precincts.precinct_number');
		$this->db->order_by('stages.stage_number');

		$filter_fields = array(
			'min_price'     => 'price_range_min >=',
			'min_lot_width' => 'lot_width >=',
			'max_lot_width' => 'lot_width <=',
			'min_lot_size'  => 'lot_square_meters >=',
			'max_lot_size'  => 'lot_square_meters <='
		);
		$form_data = $this->input->get();
		if($form_data){
			foreach($filter_fields as $field_id => $db_field){
				if(isset($form_data[$field_id]) && $form_data[$field_id]){
					$this->db->where($db_field, $form_data[$field_id]);
				}
			}
			if(isset($form_data['max_price']) && $form_data['max_price']){
				$max_value = (float)$form_data['max_price'];
				$this->db->where("( price_range_min <= {$max_value} OR price_range_max <= {$max_value})");
			}
		}
		$stagesarray = $this->db->get('lots')->result_array();
        
        $volume = array();
                // Obtain a list of columns
        foreach ($stagesarray as $key => $row) {
            $volume[$key]  = $row['matches'];
        }

        // Sort the data with volume descending, edition ascending
        // Add $data as the last parameter, to sort by the common key
        array_multisort($volume, SORT_DESC, $stagesarray);
       
		$this->output->set_content_type('application/json');
		
		// START ANDY HACK  the below is a dirty hack from Andy to ensure that no data is returned if the user does not select any of the lot search check boxes in the mobile or tablet version. 
			if ( $_GET['min_price'] == "" && $_GET['max_price'] == "" && $_GET['min_lot_width'] == "" && $_GET['max_lot_width'] == "" && $_GET['min_lot_size'] == "" && $_GET['max_lot_size'] == "")
			 {  echo "[]"; }
			else { $this->output->set_output(json_encode($stagesarray)); }	
		// // END ANDY HACK
	}

	function soldlots($development_id)
	{
		//DB Query - Get Lots data for lots from the development (development_id)
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
        $this->db->join('lot_icons', 'lot_icons.development_id = precincts.development_id', 'left'); 
		$lotsarray = $this->db->get_where('lots', array('precincts.development_id' => $development_id, 'lots.status' => 'Sold'))->result_array(); 

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($lotsarray));
	} 

	function lotsapi($development_id, $developmentkey = '')
	{
		$this->load->model('Model_development', '', TRUE);
		if(!$this->Model_development->validateDevelomentKey($development_id, $developmentkey)){
			echo json_encode(array());
			exit();
		}

		//DB Query - Get Lots data for lots from the development (development_id)
		$this->db->select('lots.lot_id, stages.stage_number, , precincts.precinct_number, lots.lot_number, lots.status, ');
		$this->db->select('lots.price_range_min, lots.price_range_max, lots.lot_width, lots.lot_square_meters, street_names.street_name ');
		$this->db->select('IF(lots.lot_corner = 1, "Corner", "Normal") AS type, lots.lot_irregular AS irregular', FALSE);
		$this->db->join('street_names', 'street_names.street_name_id = lots.street_name_id', 'left');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->where_in('lots.status', array('Available', 'Sold'));
		$lotsarray = $this->db->get('lots')->result_array(); 

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($lotsarray));
	}

	function searchsliders($development_id)
	{
		$this->db->select('developments.search_by_lot_price, developments.search_by_lot_width, developments.search_by_lot_size, ');
		$this->db->select('MIN(lots.price_range_min) AS lot_price_range_min_cheapest, MAX(lots.price_range_max) AS lot_price_range_max_most_expensive, ');
		$this->db->select('MIN(lots.lot_width) AS lot_width_smallest, MAX(lots.lot_width) AS lot_width_largest, ');
		$this->db->select('MIN(lots.lot_square_meters) AS lot_square_meters_smallest, MAX(lots.lot_square_meters) AS lot_square_meters_largest');
		$this->db->join('stages', 'stages.stage_id = lots.stage_id');
		$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
		$this->db->join('developments', 'developments.development_id = precincts.development_id');
		$this->db->where('precincts.development_id', $development_id);
		$this->db->where_in('lots.status', array('Available', 'Sold'));
		$search_sliders = $this->db->get('lots')->row_array();
		if($search_sliders){
			$search_sliders['search_by_lot_price'] = ($search_sliders['search_by_lot_price'] == 1)? 'ON': 'OFF';
			$search_sliders['search_by_lot_width'] = ($search_sliders['search_by_lot_width'] == 1)? 'ON': 'OFF';
			$search_sliders['search_by_lot_size']  = ($search_sliders['search_by_lot_size'] == 1)? 'ON': 'OFF';
		}
		$search_sliders['lot_width_smallest']         = floor($search_sliders['lot_width_smallest']);
		$search_sliders['lot_width_largest']          = ceil($search_sliders['lot_width_largest']);
		$search_sliders['lot_square_meters_smallest'] = floor($search_sliders['lot_square_meters_smallest']/10)*10;
		$search_sliders['lot_square_meters_largest']  = ceil($search_sliders['lot_square_meters_largest']/10)*10;

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($search_sliders));
	}

	function amenities($development_id)
	{
		//DB Query - Get amenities for development_id
		$amenitiesarray = $this->db->get_where('amenities', array('development_id' => $development_id))->result_array();

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($amenitiesarray));
	}

	function externalamenities($development_id)
	{
		$this->load->model('Model_development', '', TRUE);
		$development    = $this->Model_development->getDevelopment($development_id);
		$amenitiesarray = array();
		if($development->show_external_amenities == 1){
			//DB Query - Get external amenities for development_id
			$this->db->select('external_amenity_id AS proposedExternalPlace_id, CASE WHEN external_amenities.e_amenity_icon != \'\' THEN external_amenities.e_amenity_icon ELSE external_amenity_types.e_amenity_pin END AS proposedExternalPlace_icon, e_amenity_name AS proposedExternalPlace_name, ', FALSE);
			$this->db->select('e_amenity_latitude AS proposedExternalPlace_latitude, e_amenity_longitude AS proposedExternalPlace_longitude, ');
			$this->db->select('e_amenity_description AS proposedExternalPlace_description, e_amenity_moreinfo_url AS proposedExternalPlace_moreinfolink ');
			$this->db->select('e_amenity_picture1 AS proposedExternalPlace_picture1, e_amenity_picture2 AS proposedExternalPlace_picture2, e_amenity_picture3 AS proposedExternalPlace_picture3, e_amenity_picture4 AS proposedExternalPlace_picture4, e_amenity_picture5 AS proposedExternalPlace_picture5, external_amenity_iframe_html AS external_amenity_iframe_html, external_amenity_iframe_scrollbar AS external_amenity_iframe_scrollbar');
			$this->db->select('external_amenity_types.e_amenity_center_longitude, external_amenity_types.e_amenity_center_latitude, external_amenity_types.e_amenity_zoom_level, external_amenity_types.e_amenity_mobile_zoom_level, external_amenities.e_amenity_address');
			$this->db->select('external_amenity_types.external_amenity_type_id,  external_amenity_types.e_amenity_type_name, external_amenity_types.e_amenity_pin_height, external_amenity_types.e_amenity_pin_width, external_amenity_types.e_amenity_cluster_circle_url, external_amenity_types.e_amenity_cluster_circle_height, external_amenity_types.e_amenity_cluster_circle_width, external_amenity_types.e_amenity_cluster_text_hex_colour, external_amenity_types.e_amenity_cluster_text_size');
			$this->db->select('CASE WHEN external_amenity_types.e_amenity_type_name = "ALWAYSON" THEN 1 ELSE 0 END AS ALWAYSON ', FALSE);
			$this->db->join('external_amenity_types', 'external_amenity_types.external_amenity_type_id = external_amenities.e_amenity_type_id', 'left');
			$this->db->where('external_amenities.development_id', $development_id);
			$this->db->where('show_e_amenity', 1);
			$query  = $this->db->get('external_amenities');
			$amenitiesarray = $query->result_array();
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($amenitiesarray));
	}

	/* This function will return the house matches for given lot.
	 */
	function housematches($development_id, $lot_id)
	{
		$this->load->model('Model_house', '', TRUE);

		$housematchesarray = $this->Model_house->getLotHouses($development_id, $lot_id);

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($housematchesarray));
	}

	public function amenitycategories($development_id)
	{
		$this->load->model('Model_external_amenity_type', '', TRUE);
		$e_amenity_types = $this->Model_external_amenity_type->getExternalAmenityTypes($development_id, FALSE);

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($e_amenity_types));
	}

	public function alwaysonexternalamenities($development_id)
	{
		$this->load->model('Model_external_amenity_type', '', TRUE);
		$e_amenity_types = $this->Model_external_amenity_type->getExternalAmenityTypes($development_id, TRUE);

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($e_amenity_types));
	}

	public function externalamenitiesbytype($development_id, $e_amenity_type_id = 0)
	{
		$this->load->model('Model_development', '', TRUE);
		$development = $this->Model_development->getDevelopment($development_id);
		$e_amenities = array();
		if($development->show_external_amenities == 1){
			//DB Query - Get external amenities for development_id
			$this->db->select('external_amenity_id AS proposedExternalPlace_id, CASE WHEN external_amenities.e_amenity_icon != \'\' THEN external_amenities.e_amenity_icon ELSE external_amenity_types.e_amenity_pin END AS proposedExternalPlace_icon, e_amenity_name AS proposedExternalPlace_name, ', FALSE);
			$this->db->select('e_amenity_latitude AS proposedExternalPlace_latitude, e_amenity_longitude AS proposedExternalPlace_longitude, ');
			$this->db->select('e_amenity_description AS proposedExternalPlace_description, e_amenity_moreinfo_url AS proposedExternalPlace_moreinfolink ');
			$this->db->select('e_amenity_picture1 AS proposedExternalPlace_picture1, e_amenity_picture2 AS proposedExternalPlace_picture2, e_amenity_picture3 AS proposedExternalPlace_picture3, e_amenity_picture4 AS proposedExternalPlace_picture4, e_amenity_picture5 AS proposedExternalPlace_picture5, external_amenity_iframe_html AS external_amenity_iframe_html, external_amenity_iframe_scrollbar AS external_amenity_iframe_scrollbar');
			$this->db->select('external_amenity_types.e_amenity_center_longitude, external_amenity_types.e_amenity_center_latitude, external_amenity_types.e_amenity_zoom_level, external_amenities.e_amenity_address');
			$this->db->join('external_amenity_types', 'external_amenity_types.external_amenity_type_id = external_amenities.e_amenity_type_id', 'left');
			$this->db->where('external_amenities.development_id', $development_id);
			$this->db->where('external_amenities.e_amenity_type_id', $e_amenity_type_id);
			$this->db->where('show_e_amenity', 1);
			$query  = $this->db->get('external_amenities');
			$e_amenities = $query->result_array();
		}

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($e_amenities));
	}

	public function radiusmarkers($development_id)
	{
		$this->load->model('Model_radius_marker', '', TRUE);
		$radius_markers = $this->Model_radius_marker->getRadiusMarkers($development_id);

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($radius_markers));
	}

	public function linkeddevelopments($development_id)
	{
		$this->load->model('Model_development_link', '', TRUE);
		$linked_developments = $this->Model_development_link->getDevelopmentLinks($development_id);

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($linked_developments));
	}

	private function multiplePrecincts($development_id)
	{
		$this->load->model('Model_precinct', '', TRUE);
		$precincts = $this->Model_precinct->getPrecincts($development_id);
		return (count($precincts) > 1);
	}



	// ***** START - Andrew getdatainjson functions *****
	function mapovisminimarkers($development_id)
	{
		//DB Query - Get Development data for development_id
		$mapovisminimarkersarray = $this->db->get_where('mapovis_mini_markers', array('development_id' => $development_id))->result_array();                                

		$this->output->set_header('Content-Type: application/json; charset=utf-8');

		//echo $developmentarray in JSON format, without the opening [ and closing ]
		//echo str_replace(array('[', ']'), '', htmlspecialchars(json_encode($mapovisminimarkersarray), ENT_NOQUOTES));
        
        
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($mapovisminimarkersarray));
	}

	function mapovisminipolygon($development_id)
	{
		//DB Query - Get Development data for development_id
		//$mapovisminipolygonarray = $this->db->get_where('mapovis_mini_polygon', array('development_id' => $development_id))->result_array();                                
        $points = array();
        
        $this->db->select('mapovis_mini_polygon.polygon_coordinates');
        $this->db->where('mapovis_mini_polygon.development_id', $development_id);
        $query  = $this->db->get('mapovis_mini_polygon');
        $mapovisminipolygonarray = $query->result_array();
        
        if(count($mapovisminipolygonarray) == 0){
           echo json_encode($mapovisminipolygonarray); exit;    
        }
        
        $coordinate_str = $mapovisminipolygonarray[0]['polygon_coordinates'];
        $coordinate_str = trim($coordinate_str);
        
        $coordinates = explode(" ",$coordinate_str);
        
        for($i = 0; $i < count($coordinates);$i++){
            $latlng = explode(":",$coordinates[$i]);
            $latlng_pair = array('lat' => $latlng[0], 'lng' => $latlng[1]);
            array_push($points,$latlng_pair);
        }
        
		echo json_encode($points); exit;
        
	}

    
    
    function amenities_polygons($development_id)
    {

        //NOTE from ANDY to MARTHA: Seb wanted a multi-dimensional JSON array. Silly me was having trouble finding a way to produce that. My code is probably quite inefficient as I'm doing multiple DB queries
        // so I can produce the JSON in the correct structure. Martha, dont laugh at me! ;) Feel free to fix this up in the future.                                 

            // First I'm finding out what amenity polygons have been defined
            $this->db->select('amenities.amenity_id, amenities.development_id,amenities.amentiy_polygon_coords');
            $this->db->where('amenities.development_id', $development_id);
            $query  = $this->db->get('amenities');
            $polygons = $query->result_array();

        
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($polygons));

    }





	function mapovisminidirections($development_id)
	{
		//DB Query - get data for development_id
		$mapovisminidirectionsarray = $this->db->get_where('mapovis_mini_directions', array('development_id' => $development_id))->result_array();                                

		$this->output->set_header('Content-Type: application/json; charset=utf-8');      
        
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($mapovisminidirectionsarray));
        
	}
    
    function salesoffices($development_id)
    {
        //DB Query - get data for development_id
          $this->db->select('developments.sales_office_iframe_scrollbar, developments.sales_office_icon,developments.sales_office_latitude, developments.sales_office_longitude, developments.sales_office_title, developments.sales_office_html, developments.sales_office_googlemaps_directions_url,developments.sales_office_iframe_html');
          $this->db->where('developments.development_id', $development_id);
          $query  = $this->db->get('developments');
          $polygons = $query->result_array();

        
          $this->output->set_content_type('application/json');
          $this->output->set_output(json_encode($polygons));
        
    }

    function precincts($development_id)
    {
		//DB Query - Get Development data for development_id
		$developmentarray = $this->db->get_where('precincts', array('development_id' => $development_id))->result_array();                                

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($developmentarray));
        
    }
    
    function precincts_polygons($development_id)
    {
        //DB Query - Get Development data for development_id
        $precincts_array = $this->db->get_where('precincts', array('development_id' => $development_id))->result_array();                                

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($precincts_array));
        
    }

    function external_markers($development_id)
    {
        //DB Query - Get Development data for development_id
        $external_markers_array = $this->db->get_where('external_markers', array('development_id' => $development_id))->result_array();                                

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($external_markers_array));
        
    }
    
	// ***** END - Andrew getdatainjson functions *****


}
?>