<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLoginBuilder extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Model_user','',TRUE);
	}

	function index()
	{
		//This method will have the credentials validation
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

		if($this->form_validation->run() == FALSE)
		{
			//Field validation failed.&nbsp; User redirected to login page
			$this->load->view('builder/login');
		}
		else
		{
			//Go to private area
			redirect('builder/dashboard');
		}
	}

	function check_database($password)
	{
		//Field validation succeeded.&nbsp; Validate against database
		$username = $this->input->post('username');

		//query the database
		$result = $this->Model_user->login($username, $password);
		var_dump($result);

		// verify that the user has admin role
		if($result && $result->user_role === 'Builder')
		{
			$this->load->library('mapovis_lib');
			$this->mapovis_lib->logInBuilder($result, $this);
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}
}
?>