<?php
  
  if( ! defined('BASEPATH')) exit('No direct script access allowed');    
  
  class Villawood_Stock_Analysis extends CI_Controller {

    function __construct() 
    {
        parent::__construct(); 
    }
    
    function stages($development_id)
    {
        $this->load->model('Model_stage', '', TRUE);
        $stages        = $this->Model_stage->getStages($development_id);
     //   $full_title    = $this->multiplePrecincts($development_id);

        $stage_results = array();
        foreach($stages as $stage){
            $this->db->select('COUNT(lots.lot_id) AS total');
            $this->db->where('lots.stage_id', $stage->stage_id);
            $this->db->where('lots.status', 'Available');
            $available_lots = $this->db->get('lots')->row()->total;

            $this->db->select('COUNT(lots.lot_id) AS total');
            $this->db->where('lots.stage_id', $stage->stage_id);
            $this->db->where('lots.status', 'Sold');
            $sold_lots = $this->db->get('lots')->row()->total;

            $stage_results[] = array(
                'stage_id'               => $stage->stage_id,
                'precinct_id'            => $stage->precinct_id,
                'precinct_number'        => $stage->precinct_number,
              //  'stage_title'            => ($full_title)? $stage->precinct_number.'-'.$stage->stage_number: $stage->stage_number,
                'stage_number'           => $stage->stage_number,
              /*  'stage_icon'             => $stage->stage_icon,
                'stage_icon_width'       => $stage->stage_icon_width,
                'stage_icon_height'      => $stage->stage_icon_height,     */
                'stage_code'             => $stage->stage_code,
           /*     'stage_center_latitude'  => (float)$stage->stage_center_latitude,
                'stage_center_longitute' => (float)$stage->stage_center_longitute,
                'stage_zoomlevel'        => $stage->stage_zoomlevel,
                'stage_icon_latitude'     => $stage->stage_icon_latitude,
                'stage_icon_longitude'     => $stage->stage_icon_longitude,
                'stage_icon_zoomlevel_show' => $stage->stage_icon_zoomlevel_show,
                'stage_icon_zoomlevel_hide'    => $stage->stage_icon_zoomlevel_hide,
                'stage_name'            => $stage->stage_name,
                'display_stage_name'    => $stage->display_stage_name,
                'lots_available'         => $available_lots,
                'lots_sold'              => $sold_lots,
                'percentage_lots_sold'   => (($sold_lots)? round(100 * $sold_lots/($sold_lots + $available_lots),2): 0).'%', */
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($stage_results));
        
        return $stage_results;
    }
    
    function unlistedlots_count($stage_id)
    {
        $this->db->select('lots.lot_id, lots.status, lots.lot_width, lots.lot_depth, lots.lot_square_meters, ');
        $this->db->join('stages', 'stages.stage_id = lots.stage_id');
        $this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
        $this->db->where_in('lots.status', array('Unlisted')); 
      
        $this->db->where('lots.stage_id', $stage_id);
        
        $lotsarray = $this->db->get('lots')->result_array();

        return count($lotsarray);
        
    }
    
    function getDevelopmentName($development_id)
    {
        //DB Query - Get Development data for development_id
        $this->db->select('developments.development_name');
        $result = $this->db->get_where('developments', array('development_id' => $development_id))->result_array();                                

        return $result[0]['development_name'];

    }
    
    function lots($development_id)
    {
        //DB Query - Get Lots data for lots from the development (development_id)
        $this->db->select('lots.lot_width, lots.lot_depth, lots.lot_square_meters, lots.lot_corner, lots.lot_irregular, lots.lot_titled, stages.stage_code, concat(REPLACE(ROUND(lots.lot_depth,2),".00",""),"-", REPLACE(ROUND(lots.lot_width,2),".00",""), "-", REPLACE(ROUND(lots.lot_square_meters,2),".00",""), "-", stages.stage_code) as complex, count(*) as count',FALSE);
        $this->db->select('REPLACE(ROUND(lots.lot_width,2),".00","") AS lot_width', FALSE);
        $this->db->select('REPLACE(ROUND(lots.lot_depth,2),".00","") AS lot_depth', FALSE); 
        $this->db->select('REPLACE(ROUND(lots.lot_square_meters,2),".00","") AS lot_square_meters', FALSE); 
        $this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
        $this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left');
        
        $this->db->group_by('complex'); 
        
        $this->db->order_by("lots.lot_depth", "asc");
        $this->db->order_by("lots.lot_width", "asc"); 
        $this->db->order_by("lots.lot_square_meters", "asc");
        
        $lotsarray = $this->db->get_where('lots', array('precincts.development_id' => $development_id, 'lots.status' => 'Available'))->result_array(); 
        
        return $lotsarray;
    }
    
    function unlisted_lots($development_id)
    {
        //DB Query - Get Lots data for lots from the development (development_id)
        $this->db->select('lots.lot_width, lots.lot_depth, lots.lot_square_meters, lots.lot_corner, lots.lot_irregular, lots.lot_titled, stages.stage_code');
        $this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
        $this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left'); 
        
        $lotsarray = $this->db->get_where('lots', array('precincts.development_id' => $development_id, 'lots.status' => 'Unlisted'))->result_array(); 
        
        return $lotsarray;
    }
    
    function getLotIds($development_id){
          //DB Query - Get Lots data for lots from the development (development_id)
        $this->db->select('lots.lot_id, lots.lot_number, concat(REPLACE(ROUND(lots.lot_depth,2),".00",""),"-", REPLACE(ROUND(lots.lot_width,2),".00",""), "-", REPLACE(ROUND(lots.lot_square_meters,2),".00",""), "-", stages.stage_code) as complex,',false);
        $this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
        $this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left');
  
        $this->db->order_by("lots.lot_depth", "asc");
        $this->db->order_by("lots.lot_width", "asc"); 
        $this->db->order_by("lots.lot_square_meters", "asc");
        
        $lot_ids = $this->db->get_where('lots', array('precincts.development_id' => $development_id, 'lots.status' => 'Available'))->result_array(); 
     
        return $lot_ids;
    }
    
    function unconventionalLotsDFA($development_id){
        
        $this->db->select(' lots.lot_depth,lots.lot_width, lots.lot_square_meters');
        $this->db->select('REPLACE(ROUND(lots.lot_width,2),".00","") AS lot_width', FALSE);
        $this->db->select('REPLACE(ROUND(lots.lot_depth,2),".00","") AS lot_depth', FALSE); 
        $this->db->select('REPLACE(ROUND(lots.lot_square_meters,2),".00","") AS lot_square_meters', FALSE); 
        $this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
        $this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left');
        
        $this->db->group_by(' lots.lot_depth, lots.lot_width, lots.lot_square_meters'); 
        
        $this->db->order_by("lots.lot_depth", "asc");
        $this->db->order_by("lots.lot_width", "asc"); 
        $this->db->order_by("lots.lot_square_meters", "asc");
        
        $this->db->where('precincts.development_id = '.$development_id.' and lots.status = "Available" and lots.lot_corner = 1 OR lots.lot_irregular = 1');
        
        $query =  $this->db->get('lots');
        
        $result =  $query->result_array();
       
        return $result; 
             
    }
    
    function conventionalLotsDFA($development_id){
        
        $this->db->select(' lots.lot_depth,lots.lot_width, lots.lot_square_meters');
        $this->db->select('REPLACE(ROUND(lots.lot_width,2),".00","") AS lot_width', FALSE);
        $this->db->select('REPLACE(ROUND(lots.lot_depth,2),".00","") AS lot_depth', FALSE); 
        $this->db->select('REPLACE(ROUND(lots.lot_square_meters,2),".00","") AS lot_square_meters', FALSE); 
        $this->db->join('stages', 'stages.stage_id = lots.stage_id', 'left');
        $this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id', 'left');
        
        $this->db->group_by(' lots.lot_depth, lots.lot_width, lots.lot_square_meters'); 
        
        $this->db->order_by("lots.lot_depth", "asc");
        $this->db->order_by("lots.lot_width", "asc"); 
        $this->db->order_by("lots.lot_square_meters", "asc");
        
        $this->db->where('precincts.development_id = '.$development_id.' and lots.status = "Available" and lots.lot_corner != 1 and lots.lot_irregular != 1');
        
        $query =  $this->db->get('lots');
        
        $result =  $query->result_array();
       
        
        return $result; 
             
    }
    

     
    function createReport($development_id){
        
        $downloadAccess = FALSE;
        $admin_role = $this->session->userdata('logged_in_role');
        $user_role = $this->session->userdata('user_logged_role');
  
        if($admin_role != "Admin" && $user_role != "User"){
           echo "You do not have permission to access to this page."; 
           exit;   
        }
        
        $user_id = $this->session->userdata('user_logged_id');
        $this->load->model('Model_development', '', TRUE); 
        
        if($user_id != null){
            $developments = $this->Model_development->getMyDevelopments($user_id,FALSE,FALSE);
            foreach($developments as $key => $development){
                if(intval($development->development_id) == intval($development_id)){
                    $downloadAccess = TRUE;
                    break;
                }
            }  
        }
         
        if($admin_role == "Admin"){
           $downloadAccess = TRUE; 
        }
        
        if($downloadAccess == FALSE){
            echo "You do not have permission to download this report.";
            exit;
        }
        
        $this->load->library('Excel');
        
        $developmentName = $this->getDevelopmentName($development_id);
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(80);
        $objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);   
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', $developmentName.' Stock Analysis');
        $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setName('Arial Narrow')->setBold(true)->setSize(12);
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D1:E1');
        $current_date=getdate(date("U"));

        $objPHPExcel->getActiveSheet()->setCellValue('D1',"$current_date[month] $current_date[mday], $current_date[year]");
        $objPHPExcel->getActiveSheet()->getStyle("D1")->getFont()->setName('Arial Narrow')->setBold(true)->setSize(11); 
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:C4');
        $objPHPExcel->getActiveSheet()->setCellValue('A4','Non-Conventional');
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F2:H2');
        $objPHPExcel->getActiveSheet()->setCellValue('F2','Titled Lots are in RED');
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray(
            array(
                'font' => array(
                  'color' => array('argb' => 'ffff0000'),
                  'name' => 'Arial Narrow',
                  'size' => '11',
            ))
        );
      
        $objPHPExcel->getActiveSheet()->getStyle(
            'A4')->applyFromArray(
            array(
                  'font' => array(
                  'color' => array('argb' => 'ffffffff'),
                  'name' => 'Arial Narrow',
                  'size' => '11',
                  'bold' => true
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                
                'fill'  => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'ff000000')
            )
            )
         );
         
        $objPHPExcel->getActiveSheet()->setCellValue('A5', "Depth"); // Add Column "Depth"  
        $objPHPExcel->getActiveSheet()->setCellValue('B5', "Frontage"); // Add Column "Frontage"  
        $objPHPExcel->getActiveSheet()->setCellValue('C5', "Area"); // Add Column "Area" 
         
        $stages = array();
        $stage_codes = array();
        
        $stages = $this->stages($development_id);
        
        foreach($stages as $key => $stage){
          $stage_code = "Stage".$stage['stage_code'];
          array_push($stage_codes,$stage['stage_code']);
          
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key+3, 5, $stage_code);       
        } 
        
        $totalColumnString=$objPHPExcel->getActiveSheet()->getHighestColumn();
        $total_colIndex = PHPExcel_Cell::columnIndexFromString($totalColumnString);
        
      
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($total_colIndex,5, "Total"); // Add Column "Total"  
        
        $totalColumnString=$objPHPExcel->getActiveSheet()->getHighestColumn();  
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($total_colIndex+1,5, "Total In Lot Category");  //Add Column "Total In Lot Category"
        
        $total_In_Lot_Category_ColumnString = PHPExcel_Cell::stringFromColumnIndex($total_colIndex+1);
        $objPHPExcel->getActiveSheet()->getColumnDimension($total_In_Lot_Category_ColumnString)->setWidth(13);
        $objPHPExcel->getActiveSheet()->getStyle($total_In_Lot_Category_ColumnString.'5')->getAlignment()->setWrapText(true);
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($total_colIndex+2,5, "% of Stock");  //Add Column "% of Stock"
        $percentStockColumnString = PHPExcel_Cell::stringFromColumnIndex($total_colIndex+2);  
        
        $objPHPExcel->getActiveSheet()->getStyle(
            'A5:'.$objPHPExcel->getActiveSheet()->getHighestColumn().'5')->applyFromArray(
            array(
                 'font' => array(
                  'bold' => true, 
                  'color' => array('argb' => 'ffffffff'),
                  'name' => 'Arial Narrow',
                  'size' => '11',
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_BOTTOM,
                ),
                
                'fill'  => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'ff000000')
                )
            )
         );
         
        $objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(30);
        $objPHPExcel->getActiveSheet()->getStyle($total_In_Lot_Category_ColumnString)->getAlignment()->setWrapText(true); 
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:C6');
        $objPHPExcel->getActiveSheet()->setCellValue('A6','Stock Withheld');
      
        $objPHPExcel->getActiveSheet()->getStyle(
            'A6')->applyFromArray(
            array(
                 'font' => array(
                  'color' => array('argb' => 'ffffffff'),
                  'name' => 'Arial Narrow',
                  'size' => '11',
                  'bold' => true
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_BOTTOM,
                ),
                
                'fill'  => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'ff000000')
                )
            )
         );
         
         $objPHPExcel->getActiveSheet()->getStyle(
            'D6:'.$totalColumnString.'6')->applyFromArray(
            array(
                  'font' => array(
                  'color' => array('argb' => 'ff000000'),
                  'name' => 'Arial Narrow',
                  'size' => '11',
                  'bold' => true
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_BOTTOM,
                    
                ),
                
                'fill'  => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'ff92D050')
                ),
                
                 'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'ff000000'),
                    ),
                ),
            )
         );
         
         // Stock on the 'Market Today cell' and next cell's styling
          $objPHPExcel->getActiveSheet()->setCellValue($total_In_Lot_Category_ColumnString.'6','Stock on the Market Today'); 
          $objPHPExcel->getActiveSheet()->getStyle(
            $total_In_Lot_Category_ColumnString.'6:'.$objPHPExcel->getActiveSheet()->getHighestColumn().'6')->applyFromArray(
            array(
                  'font' => array(
                  'color' => array('argb' => 'ff000000'),
                  'name' => 'Arial Narrow',
                  'size' => '11',
                  'bold' => true
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_BOTTOM,
                    
                ),
                
                'fill'  => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'ffFFC000')
                ),
                
                 'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'ff000000'),
                    ),
                ),
            )
         );
         
         
         
         $objPHPExcel->getActiveSheet()->getRowDimension('6')->setRowHeight(30);
          
         $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:C7');
         $objPHPExcel->getActiveSheet()->setCellValue('A7','Lot Availability by Numbers');
         $objPHPExcel->getActiveSheet()->getStyle(
            'A7')->applyFromArray(
            array(
                 'font' => array(
                  'color' => array('argb' => 'ffffffff'),
                  'name' => 'Arial Narrow',
                  'size' => '11',
                  'bold' => true
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_BOTTOM,
                ),
                
                'fill'  => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'ff000000')
                )
            )
         );
         
         $objPHPExcel->getActiveSheet()->getStyle(
            'D7:'.$objPHPExcel->getActiveSheet()->getHighestColumn().'7')->applyFromArray(
            array(
                  'font' => array(
                  'color' => array('argb' => 'ff000000'),
                  'name' => 'Arial Narrow',
                  'size' => '11',
                  'bold' => true,
                  
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_BOTTOM,
                ),
                
                'fill'  => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'ffffff00')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'ff000000'),
                    ),
                ),
                
            )
         );
         
         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($total_colIndex+1,7, "Months of Stock @ 15 sales per month");  
         $objPHPExcel->getActiveSheet()->getStyle("A7:".$objPHPExcel->getActiveSheet()->getHighestColumn()."7")->getAlignment()->setWrapText(true);  
         $objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(60);  
         
         $lotsarray = $this->lots($development_id);

         $lotid_array = $this->getLotIds($development_id);
         
         /*
          gererate non conventional lots Depth    Frontage    Area
         */
         
         $nonconventionalDFA_array = $this->unconventionalLotsDFA($development_id);
         $nonconventionalDFA_pairs = array();
         
         foreach($nonconventionalDFA_array as $row => $DFA){
                
            $depth = $DFA['lot_depth'];
            $frontage = $DFA['lot_width'];
            $area = $DFA['lot_square_meters'];
            
            $objPHPExcel->getActiveSheet()->setCellValue('A'.($row+8), $depth); 
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($row+8), $frontage);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($row+8), $area);
            
            $entry = array('triplevalue' => $depth.'-'.$frontage.'-'.$area, 'rowNum' => $row+8);
            array_push($nonconventionalDFA_pairs,$entry);
         }
         
         foreach($lotsarray as $row => $lot){
             
            if((intval($lot['lot_corner']) == 1) || (intval($lot['lot_irregular']) == 1)){
                
                $depth = $lot['lot_depth'];
                $frontage = $lot['lot_width'];
                $area = $lot['lot_square_meters'];
                $stage_code = $lot['stage_code'];
                
                $count = $lot['count'];
                
                $key = array_search($stage_code, $stage_codes); // array position of given stage code among stages codes.  
                $column_position = $key + 3; // in test excel sheet
                  
                $triple = $depth.'-'.$frontage.'-'.$area;
                for($c = 0; $c < count($nonconventionalDFA_pairs); $c++){
                    if($triple == $nonconventionalDFA_pairs[$c]['triplevalue']){
                        
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column_position, $nonconventionalDFA_pairs[$c]['rowNum'], $count);
                        
                                
                        /*
                          Titled lots mark using red color for non conventional lots.
                        */

                        if(intval($lot['lot_titled']) == 1){

                           $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($column_position,$nonconventionalDFA_pairs[$c]['rowNum'])->applyFromArray(
                                   array(
                                      'font' => array(
                                      'color' => array('argb' => 'ffff0000'),                           
                                    ),
                           ));  
                        }  
        
                        $cell = PHPExcel_Cell::stringFromColumnIndex($column_position) . $nonconventionalDFA_pairs[$c]['rowNum'];
                
                        foreach($lotid_array as $lotid){
                            if($lotid['complex'] == $lot['complex']){
                               $objPHPExcel->getActiveSheet()->getComment($cell)->getText()->createTextRun($lotid['lot_number']."\n");   // add comments to cells
                            }
                        } 
                            
                    }
                }
                
            } 
                        
        }
        
        
        for($c = 0; $c < count($nonconventionalDFA_pairs); $c++){ 
            $lastStageColumnString = PHPExcel_Cell::stringFromColumnIndex(count($stages) + 2);
            $objPHPExcel->getActiveSheet()->setCellValue($totalColumnString.($c+8), "=SUM(D".($c+8).":".$lastStageColumnString.($c+8).")");
        }
        
         
                                                                                
        $rowscount_nonconventional = count($nonconventionalDFA_array) - 1;
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells($total_In_Lot_Category_ColumnString.'8:'.$total_In_Lot_Category_ColumnString.($rowscount_nonconventional+8));
        $objPHPExcel->getActiveSheet()->setCellValue($total_In_Lot_Category_ColumnString.'8',"=SUM(".$totalColumnString."8:".$totalColumnString.($rowscount_nonconventional+8).")"); 
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells($objPHPExcel->getActiveSheet()->getHighestColumn().'8:'.$objPHPExcel->getActiveSheet()->getHighestColumn().($rowscount_nonconventional+8));
        
        $rowNumForConventional = $objPHPExcel->getActiveSheet()->getHighestRow()+1;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rowNumForConventional.':C'.$rowNumForConventional);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowNumForConventional,'Conventional');
        
        $objPHPExcel->getActiveSheet()->getStyle(
            'A'.$rowNumForConventional.':'.$objPHPExcel->getActiveSheet()->getHighestColumn().$rowNumForConventional)->applyFromArray(
            array(
                  'font' => array(
                  'color' => array('argb' => 'ffffffff'),
                  'name' => 'Arial Narrow',
                  'size' => '11',
                  'bold' => true
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                
                'fill'  => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'ff000000')
            )
            )
         );
         
        $objPHPExcel->getActiveSheet()->setCellValue('A'.($rowNumForConventional+1),'Depth');
        $objPHPExcel->getActiveSheet()->setCellValue('B'.($rowNumForConventional+1),'Frontage');
        $objPHPExcel->getActiveSheet()->setCellValue('C'.($rowNumForConventional+1),'Area');
        
        $objPHPExcel->getActiveSheet()->getStyle(
            'A'.($rowNumForConventional+1).':'.$objPHPExcel->getActiveSheet()->getHighestColumn().($rowNumForConventional+1))->applyFromArray(
            array(
                  'font' => array(
                  'color' => array('argb' => 'ffffffff'),
                  'name' => 'Arial Narrow',
                  'size' => '11',
                  'bold' => true
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                
                'fill'  => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'ff000000')
            )
            )
         );
         
         $styleArray = array(
            'font' => array(
              'name' => 'Arial',
              'size' => '11',
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'ff000000'),
                ),
            ),
            'fill'  => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' =>'C4BD97')
            )  
        );  
        
        $objPHPExcel->getActiveSheet()->getStyle(
            'A8:'.$objPHPExcel->getActiveSheet()->getHighestColumn() . ($rowNumForConventional-1))->applyFromArray($styleArray);  
            
         
         /*
          gererate conventional lots Depth    Frontage    Area
         */
         
         $conventionalDFA_array = $this->conventionalLotsDFA($development_id);
         $conventionalDFA_pairs = array();
         
         $k = 0;
         
         $startRowNumForConventional = $rowNumForConventional + 2;
         
         foreach($conventionalDFA_array as $row => $DFA){
                
            $depth = $DFA['lot_depth'];
            $frontage = $DFA['lot_width'];
            $area = $DFA['lot_square_meters'];
            
            $objPHPExcel->getActiveSheet()->setCellValue('A'.($row+$startRowNumForConventional), $depth); 
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($row+$startRowNumForConventional), $frontage);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($row+$startRowNumForConventional), $area);
            
            $entry = array('triplevalue' => $depth.'-'.$frontage.'-'.$area, 'rowNum' => $row+$startRowNumForConventional);
            array_push($conventionalDFA_pairs,$entry);
         }
         
         foreach($lotsarray as $row => $lot){
             
            if((intval($lot['lot_corner']) != 1) && (intval($lot['lot_irregular']) != 1)){
                
                $depth = $lot['lot_depth'];
                $frontage = $lot['lot_width'];
                $area = $lot['lot_square_meters'];
                $stage_code = $lot['stage_code'];
                
                $count = $lot['count'];
                
                $key = array_search($stage_code, $stage_codes); // array position of given stage code among stages codes.  
                $column_position = $key + 3; // in test excel sheet
                
                $triple = $depth.'-'.$frontage.'-'.$area;
                for($c = 0; $c < count($conventionalDFA_pairs); $c++){
                    if($triple == $conventionalDFA_pairs[$c]['triplevalue']){
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column_position, ($conventionalDFA_pairs[$c]['rowNum']), $count);
                        $cell = PHPExcel_Cell::stringFromColumnIndex($column_position) . ($conventionalDFA_pairs[$c]['rowNum']);
                
                        foreach($lotid_array as $lotid){
                            if($lotid['complex'] == $lot['complex']){
                               $objPHPExcel->getActiveSheet()->getComment($cell)->getText()->createTextRun($lotid['lot_number']."\n");   // add comments to cells
                            }
                        }     
                    }
                }
                  
            } 
                        
        }
        
        $k = count($conventionalDFA_array);  
        /*
            calculate Total sum for conventional lots.
        */
        for($c = 0; $c < count($conventionalDFA_pairs); $c++){ 
            $lastStageColumnString = PHPExcel_Cell::stringFromColumnIndex(count($stages) + 2);
            $objPHPExcel->getActiveSheet()->setCellValue($totalColumnString.($c+$startRowNumForConventional), "=SUM(D".($c+$startRowNumForConventional).":".$lastStageColumnString.($c+$startRowNumForConventional).")");
        }
          
        $filapar = new PHPExcel_Style();
        $filapar->applyFromArray(
            array(
                'font'  => array(
                'name'  => 'Arial', 
                'size'  => 11,              
                'color' => array(
                'rgb'   => '000000'
                )
            ),
            
             'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
                'fill'  => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' =>'999999')
            ),
                'borders' => array(
                'allborders'    => array(
                'style'   => PHPExcel_Style_Border::BORDER_THIN ,
                'color'   => array(
                'rgb'     => '000000'
                  )
               )             
            )
        ));
        
        $filanon = new PHPExcel_Style();
        
        $filanon->applyFromArray(
            array(
               'font'  => array(
                    'name'  => 'Arial',
                    'size'  => 11,              
                    'color' => array(
                    'rgb'   => '000000'
                    )
                ),
                        
                 'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                
                'fill'  => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'cccccc')
                ),
                
                'borders' => array(
                'allborders'    => array(
                'style'   => PHPExcel_Style_Border::BORDER_THIN ,
                'color'   => array(
                'rgb'     => '000000'
                    )
                )             
            )
        ));
        
        for($n = 0; $n < $k; $n++){

           for($j = $k - 1 + $startRowNumForConventional; $j > $startRowNumForConventional; $j--){
               
               if($objPHPExcel->getActiveSheet()->getCell("A".($n+$startRowNumForConventional))->getValue() == $objPHPExcel->getActiveSheet()->getCell("A".$j)->getValue()) {
                   $objPHPExcel->setActiveSheetIndex(0)->mergeCells($total_In_Lot_Category_ColumnString.($n+$startRowNumForConventional).':'.$total_In_Lot_Category_ColumnString.$j);
                   $objPHPExcel->getActiveSheet()->setCellValue($total_In_Lot_Category_ColumnString.($n+$startRowNumForConventional), "=SUM(".$totalColumnString.($n+$startRowNumForConventional).":".$totalColumnString.$j.")");     // Column "Total In Lot Category"  values setting

                   $objPHPExcel->setActiveSheetIndex(0)->mergeCells($percentStockColumnString.($n+$startRowNumForConventional).':'.$percentStockColumnString.$j); 
                   $objPHPExcel->getActiveSheet()->setCellValue($percentStockColumnString.($n+$startRowNumForConventional), '=100*'.$total_In_Lot_Category_ColumnString.($n+$startRowNumForConventional).'/'.$totalColumnString.($k + $startRowNumForConventional));   // Percent of Stock column values setting
                   
                   if ($n % 2 == 0) { 
                         $objPHPExcel->getActiveSheet()->setSharedStyle($filapar, "A".($n+$startRowNumForConventional).":".$objPHPExcel->getActiveSheet()->getHighestColumn().$j); 
                   } else { 
                         $objPHPExcel->getActiveSheet()->setSharedStyle($filanon, "A".($n+$startRowNumForConventional).":".$objPHPExcel->getActiveSheet()->getHighestColumn().$j); 
                   }     
            
                   break;
               } 
           }
        } 

        $lastRowNum =  $objPHPExcel->getActiveSheet()->getHighestRow()+1;

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$lastRowNum.':C'.$lastRowNum); 
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$lastRowNum,"Total Lots"); 
        
        /*
          in total lots(last row), calculate each cell's value using sum function
        */
         
        for($i = 0; $i < count($stages); $i++){
             $columnString = PHPExcel_Cell::stringFromColumnIndex($i + 3); 
             $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($i+3,$lastRowNum, "=SUM(".$columnString."8:".$columnString.($lastRowNum-1).")"); 
        } 
        
        $objPHPExcel->getActiveSheet()->getStyle(
            'A'.$lastRowNum.':'.$objPHPExcel->getActiveSheet()->getHighestColumn() . $lastRowNum)->applyFromArray(
            array(
            'font' => array(
              'color' => array('argb' => 'ff000000'),  
              'name' => 'Arial',
              'size' => '11',
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'ff000000'),
                ),
            ),
        ));     

        /*
          in 'Lot Availability by Numbers' line 7, fill out each cell's value
        */
        for($i = 0; $i < count($stages); $i++){
            $columnString = PHPExcel_Cell::stringFromColumnIndex($i + 3); 
            $lastRowCellValue = $objPHPExcel->getActiveSheet()->getCell($columnString.$lastRowNum)->getFormattedValue();
          
            if(intval($lastRowCellValue) == 0){
               $objPHPExcel->getActiveSheet()->setCellValue($columnString."7","SOLD OUT"); 
            }
            else{
               $objPHPExcel->getActiveSheet()->setCellValue($columnString."7",$lastRowCellValue); 
            }
            
        }
        
         /*
           Titled lots mark using red color for conventional lots.
         */
                        
        foreach($lotsarray as $row => $lot){
             
            if((intval($lot['lot_corner']) != 1) && (intval($lot['lot_irregular']) != 1)){
                
                $depth = $lot['lot_depth'];
                $frontage = $lot['lot_width'];
                $area = $lot['lot_square_meters'];
                $stage_code = $lot['stage_code'];
                
                $count = $lot['count'];
                
                $key = array_search($stage_code, $stage_codes); // array position of given stage code among stages codes.  
                $column_position = $key + 3; // in test excel sheet
                
                $triple = $depth.'-'.$frontage.'-'.$area;
                for($c = 0; $c < count($conventionalDFA_pairs); $c++){
                    if($triple == $conventionalDFA_pairs[$c]['triplevalue']){
                
                        if(intval($lot['lot_titled']) == 1){
                        
                           $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($column_position,$conventionalDFA_pairs[$c]['rowNum'])->applyFromArray(
                                   array(
                                      'font' => array(
                                      'color' => array('argb' => 'ffff0000'),                           
                                    ),
                           ));  
                        } 
                        
   
                    }
                }
                  
            } 
                        
        }
        
        /*
          in 'Stock Withheld' line 6, fill out each cell's value
        */
        
        $unlistedlots_totalCounts = 0;
        
        for($i = 0; $i < count($stages); $i++){
             
            $columnString = PHPExcel_Cell::stringFromColumnIndex($i + 3);
            $unlistedlots_count = $this->unlistedlots_count($stages[$i]['stage_id']);
            $unlistedlots_totalCounts += $unlistedlots_count;  
            $objPHPExcel->getActiveSheet()->setCellValue($columnString."6",$unlistedlots_count);
        }
        
        // in line 6, calculate Total column's value.
         $objPHPExcel->getActiveSheet()->setCellValue($totalColumnString."6",$unlistedlots_totalCounts);
 
         // get 'Total'  sum (including line 7's Total sum).
        $objPHPExcel->getActiveSheet()->setCellValue($totalColumnString.$lastRowNum, "=SUM(".$totalColumnString."8:".$totalColumnString.($lastRowNum-1).")");        
        $objPHPExcel->getActiveSheet()->setCellValue($totalColumnString."7", "=SUM(".$totalColumnString."8:".$totalColumnString.($lastRowNum-1).")");  
        
         // get 'Total In Lot Category' sum
        $objPHPExcel->getActiveSheet()->setCellValue($total_In_Lot_Category_ColumnString.$lastRowNum, "=SUM(".$totalColumnString."8:".$totalColumnString.($lastRowNum-1).")");
        
        // in line 6 Stock withheld, calculate last field's value (% of stock)
        $value = $objPHPExcel->getActiveSheet()->getCell($total_In_Lot_Category_ColumnString.$lastRowNum)->getFormattedValue() - $objPHPExcel->getActiveSheet()->getCell($totalColumnString."6")->getFormattedValue();
        $objPHPExcel->getActiveSheet()->setCellValue($percentStockColumnString."6",$value);
         $objPHPExcel->getActiveSheet()->setCellValue($percentStockColumnString."7",$value/15);   
        
        $objPHPExcel->getActiveSheet()->setCellValue($percentStockColumnString."8", "= 100 * (".$total_In_Lot_Category_ColumnString."8/".$totalColumnString.$lastRowNum.")");
        
        $objPHPExcel->getActiveSheet()->setCellValue($objPHPExcel->getActiveSheet()->getHighestColumn().$lastRowNum,'100');
        
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Stock Analysis for Development '.$developmentName.'.xlsx"');
        header('Cache-Control: max-age=0');
         
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');  
                                      
        exit;
        
    }  
    
  }
  
?>
