<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('adminmain_controller.php');

class Settings extends AdminMain_Controller{

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in_id'))
		{
			redirect('admin/login');
			exit;
		}
	}

	public function welcomemessage()
	{
		$this->load->model('Model_config', '', TRUE);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('welcome_message', 'Welcome Message', 'trim|required');

		if($this->form_validation->run() != FALSE){
			$form_data = $this->input->post();
			$this->Model_config->updateWelcomeMessage($form_data);
			$this->session->set_flashdata('alertmessage', 'The Settings has been updated successfully.');
			redirect('admin/settings/welcomemessage');
		}

		$settings = $this->Model_config->getConfig();
		$data         = array(
			'settings' => $settings,
		);
		$this->load_template('admin/settings/view_welcomemessage', $data);
	}

	public function welcomemessagebuilder()
	{
		$this->load->model('Model_config', '', TRUE);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('welcome_message_builder', 'Welcome Message', 'trim|required');

		if($this->form_validation->run() != FALSE){
			$form_data = $this->input->post();
			$this->Model_config->updateWelcomeMessageBuilder($form_data);
			$this->session->set_flashdata('alertmessage', 'The Settings has been updated successfully.');
			redirect('admin/settings/welcomemessagebuilder');
		}

		$settings = $this->Model_config->getConfig();
		$data         = array(
			'settings' => $settings,
		);
		$this->load_template('admin/settings/view_welcomemessagebuilder', $data);
	}

	public function globalconfig()
	{
		$this->load->model('Model_config', '', TRUE);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('number_logs', 'Number Lots Change Logs', 'integer');
		$this->form_validation->set_rules('support_email', 'Support Email', '');
		$this->form_validation->set_rules('contact_number', 'Contact Number', '');

		if($this->form_validation->run() != FALSE){
			$form_data = $this->input->post();
			$this->Model_config->updateConfig($form_data);
			$this->session->set_flashdata('alertmessage', 'The Settings has been updated successfully.');
			redirect('admin/settings/globalconfig');
		}

		$settings = $this->Model_config->getConfig();
		$data         = array(
			'settings' => $settings,
		);
		$this->load_template('admin/settings/view_globalconfig', $data);
	}

	public function manageaccount()
	{
		$this->load->model('Model_user','',TRUE);
		$current_admin = $this->Model_user->getAdminLoggedIn();
		$user_id       = $current_admin->user_id;

		//This method will have the credentials validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_check_user_details');

		if($this->form_validation->run() != FALSE){
			$form_data = $this->input->post();
			$this->Model_user->updateUser($user_id, $form_data);
			$this->session->set_flashdata('alertmessage', 'Your account has been updated successfully.');
			redirect('admin/settings/manageaccount');
		}
		$data         = array(
			'user' => $current_admin,
		);
		$this->load_template('admin/settings/view_manageaccount', $data);
	}

	function check_user_details()
	{
		$user_id          = $this->session->userdata('logged_in_id');
		$email_addres     = $this->input->post('email');
		$password         = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		$valid            = TRUE;
		$msg              = '';
		if(!$this->Model_user->isUnique($email_addres, $user_id)){
			$msg   = $msg.'The email address already exists in the system. ';
			$valid = FALSE;
		}

		if(!empty($password) && !empty($confirm_password)){
			if($password !== $confirm_password){
				$msg   = $msg.'Invalid password please try again.';
				$valid = FALSE;
			}
		}
		$this->form_validation->set_message('check_user_details', $msg);
		return $valid;
	}

}
?>