<?php
if( !defined('BASEPATH')) exit('No direct script access allowed');
require_once('adminmain_controller.php');

class GlobalHouses extends AdminMain_Controller{

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in_id'))
		{
			redirect('admin/login');
			exit();
		}
		$this->load->model('Model_developer', '', TRUE);
	}

	function index(){
		$this->viewhouses();
	}

	function viewhouses($default_house = null)
	{
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_builder', '', TRUE);

		$houses      = $this->Model_house->getHouses(null);
		$builders    = $this->Model_builder->getBuilders();
		$data        = array(
			'houses'        => $houses,
			'builders'      => $builders,
			'default_house' => $default_house
		);
		$footer = array('admin/footer_standard', 'admin/developments/view_houses_customjs', 'admin/globalhouses/view_viewhouses_customjs');
		$this->load_template('admin/globalhouses/view_viewhouses', $data, $footer);
	}

	function addhouses($builder_id = NULL)
	{
		$this->load->model('Model_builder', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);
		$this->load->model('Model_house', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('house_name', 'House Name', 'trim|required|xss_clean');

		if($this->form_validation->run() != FALSE)
		{
			$form_data                   = $this->input->post();
			$form_data['development_id'] = null;
			$redirecto_to                = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? 'addhouses': 'viewhouses';

			//Field validation success, add precinct
			if($this->Model_house->addHouse($form_data)){
				$this->session->set_flashdata('alertmessage', 'The house was added successfully.');
				redirect('admin/globalhouses/'.$redirecto_to);
			}
		}
		$builders    = $this->Model_builder->getBuilders();
		$data        = array('builders' => $builders, 'builder_id' => $builder_id);

		$footer      = array('admin/footer_standard', 'admin/developments/view_houses_customjs', 'admin/developments/view_actioncontinue_customjs');
		$this->load_template('admin/globalhouses/view_addhouses', $data, $footer);
	}

	function viewhouse($house_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_builder', '', TRUE);
		$this->load->model('Model_house_image', '', TRUE);

		$house          = $this->Model_house->getHouse($house_id);
		$builders       = $this->Model_builder->getBuilders();
		$builder_houses = ($house->builder_id)? $this->Model_house->getHousesByBuilder($house->builder_id, $house_id, TRUE): array();
		$data           = array(
			'house'          => $house,
			'builders'       => $builders,
			'house_images'   => $this->Model_house_image->getHouseImages($house_id, array('file_type' => 'facade')),
			'floor_plans'    => $this->Model_house_image->getHouseImages($house_id, array('file_type' => 'floor_plan')),
			'gallery'        => $this->Model_house_image->getHouseImages($house_id, array('file_type' => 'gallery')),
			'builder_houses' => $builder_houses
		);
		$this->load_ajaxtemplate('admin/globalhouses/view_viewhouse', $data);
	}

	function deletehouse($house_id)
	{
		$this->load->model('Model_house', '', TRUE);
		if($this->Model_house->deleteHouse($house_id)){
			$this->session->set_flashdata('alertmessage', 'The house was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the house.');
		}
		redirect('admin/globalhouses/viewhouses');
	}

	function developerhouses($builder_id, $house_id = null)
	{
		$this->load->model('Model_house', '', TRUE);
		$houses     = $this->Model_house->getHousesByBuilder($builder_id, $house_id, TRUE);
		$drop_down  = '<select id="import_existing_house" name="import_existing_house" class="form-control">';
		foreach($houses as $builder_house){
			$drop_down .= '<option value="'. $builder_house->house_id.'" >'.$builder_house->house_name.'</option>';
		}
		$drop_down .= '</select>';
		echo $drop_down;
		exit();
	}

	function gethousefacadeimages($house_id)
	{
		$this->load->model('Model_house_image', '', TRUE);
		$house_images   = $this->Model_house_image->getHouseImages($house_id, array('file_type' => 'facade'));
		echo json_encode($house_images);
		exit();
	}

	function impglobalhousescsv()
	{
		$this->load_template('admin/globalhouses/view_impglobalhousescsv');
	}

	function importedithousescsvfile()
	{
		$this->load->model('Model_import_house', '', TRUE);
		$config = array(
			'upload_path'   => FCPATH.'uploads'.DIRECTORY_SEPARATOR,
			'allowed_types' => 'csv',
			'max_size'	    => '100',
			'max_width'     => '1024',
			'max_height'    => '768'
		);
		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('csvfilename')){
			$this->session->set_flashdata('errormessage', $this->upload->display_errors());
			redirect('admin/developments/importdevelopmentscsv');

		}
		$upload_data  = $this->upload->data();
		$file_id      = $this->Model_import_house->addImportFileToUpdate($upload_data);
		$file_details = $this->Model_import_house->getImportedFile($file_id);
		$data        = array(
			'file_details' => $file_details,
			'notes'        => $file_details->import_notes,
		);
		$this->load_template('admin/globalhouses/view_importcsvdata', $data);
	}

/*	function fixOriginal($house_id, $folder_name)
	{
		$new_file_path = BASEPATH."../../mpvs/images/dev_houses/";
		var_dump($new_file_path);
		if (file_exists("{$new_file_path}{$folder_name}/")) {
			
			$files   = scandir("{$new_file_path}{$folder_name}/");
			$counter = 1;
			foreach($files as $file){
				@list($file, $extension) = explode('.', $file);
				if( $extension && $extension== 'jpg'){
					echo("$file ==>  $extension<br>");
					copy("{$new_file_path}/house_{$house_id}/x{$counter}.jpg", $file);
					$counter++;
				}
			}
			var_dump($files);
			
			
			
		}
		return;

		$form_data = $this->input->post();
		if($this->Model_house->updateHouse($house_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The house was updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the house, please make sure the data is correct.');
		}
		redirect('admin/globalhouses/viewhouses/');
	}//*/

}
?>