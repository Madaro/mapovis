<?php
if( !defined('BASEPATH')) exit('No direct script access allowed');
require_once('adminmain_controller.php');

class Developers extends AdminMain_Controller{

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in_id'))
		{
			redirect('admin/login');
			exit();
		}
		$this->load->model('Model_developer', '', TRUE);
	}

	public function managedevelopers()
	{
		$developers   = $this->Model_developer->getDevelopers();
		$data         = array(
			'developers'   => $developers
		);
		$this->load_template('admin/developers/view_managedevelopers', $data);
	}

	public function adddeveloper()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('developer', 'Developer Name', 'trim|required|xss_clean');
		if($this->form_validation->run() != FALSE)
		{
			//Field validation success, update developer
			$form_data = $this->input->post();
			if($this->Model_developer->addDeveloper($form_data)){
				$this->session->set_flashdata('alertmessage', 'The developer has been added successfully.');
				//Go to Manage area
				redirect('admin/developers/managedevelopers');
			}
		}
		$data        = array();
		$this->load_template('admin/developers/view_adddeveloper', $data);
	}

	public function editdeveloper($developer_id)
	{
		$developer = $this->Model_developer->getDeveloper($developer_id);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('developer', 'Developer Name', 'trim|required|xss_clean');
		if($this->form_validation->run() != FALSE)
		{
			//Field validation success, update developer
			$form_data = $this->input->post();
			if($this->Model_developer->updateDeveloper($developer_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The developer has been updated successfully.');
				//Go to Manage area
				redirect('admin/developers/managedevelopers');
			}
		}
		$data        = array(
			'developer'    => $developer,
		);
		$this->load_template('admin/developers/view_editdeveloper', $data);
	}

	public function deletedeveloper($developer_id)
	{
		if($this->Model_developer->deleteDeveloper($developer_id)){
			$this->session->set_flashdata('alertmessage', 'The developer has been deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the developer.');
		}
		//Go to Manage developers
		redirect('admin/developers/managedevelopers');
	}

}
?>