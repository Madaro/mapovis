<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('adminmain_controller.php');

class Reports extends AdminMain_Controller{

	function __construct() 
	{
		parent::__construct(); 
		if(!$this->session->userdata('logged_in_id'))
		{
			redirect('admin/login');
			exit;
		}
	}

	function changelogtemp()
	{
		echo base_url();
		$CI =& get_instance();
		if(!$CI->input->server('HTTPS'))
		{
			$CI->config->config['base_url'] = str_replace('http://', 'https://', $CI->config->config['base_url']);
			redirect($CI->uri->uri_string());
		}
		$this->load->model('Model_user', '', TRUE);
		$this->load->model('Model_change_log', '', TRUE);

		$change_logs  = $this->Model_change_log->getChangeLogs(NULL);
		$data         = array(
			'change_logs'  => $change_logs
		);
		$this->load_template('admin/reports/view_changelog', $data);
	}

	function changelog()
	{
		$this->load->model('Model_user', '', TRUE);
		$this->load->model('Model_change_log', '', TRUE);

		$change_logs  = $this->Model_change_log->getChangeLogs(NULL);
		$data         = array(
			'change_logs'  => $change_logs
		);
		$this->load_template('admin/reports/view_changelog', $data);
	}

	function builderpackagesreport($development_id = 0)
	{
		echo '<pre>';
		foreach(array(1 => 'ACTIVE', 0 => 'INACTIVE') AS $i => $label){
			$this->db->select('builders.*, developments.development_name, developments.development_id, COUNT(house_lot_packages.house_lot_package_id) AS total_packages', FALSE);
			$this->db->join('houses', 'houses.house_id = house_lot_packages.house_id');
			$this->db->join('builders', 'houses.house_builder_id = builders.builder_id');
			$this->db->join('lots', 'lots.lot_id = house_lot_packages.lot_id');
			$this->db->join('stages', 'stages.stage_id = lots.stage_id');
			$this->db->join('precincts', 'precincts.precinct_id = stages.precinct_id');
			$this->db->join('developments', 'developments.development_id = precincts.development_id');
			if($development_id){
				$this->db->where('developments.development_id', $development_id);
			}
			if($i == 1){
				$this->db->where('lots.status', 'Available');
				$this->db->where('house_lot_packages.active', $i);
			}
			else{
				$this->db->where_in('lots.status', array('Available', 'Sold'));
			}
			$this->db->group_by('builders.builder_id');
			$this->db->group_by('developments.development_id');
			$this->db->order_by('developments.development_name');
			$this->db->order_by('developments.development_id');
			$this->db->order_by('total_packages', 'desc');
			$this->db->order_by('builders.builder_name');

			$query  = $this->db->get('house_lot_packages');
			$result = ($query->num_rows() > 0)? $query->result(): array();
			$total_records = 0;
			echo "<strong>{$label}</strong><br><br><br>";
			if(count($result)){
				if($development_id){
					$dev = @current($result);
					echo "<strong>{$dev->development_id} {$dev->development_name}</strong><br><br><br>";
				}
				$tmp_dev       = null;
				foreach($result as $row){
					if($development_id){
						echo "{$row->builder_name}: {$row->total_packages}<br>";
					}
					else{
						if($tmp_dev != $row->development_id){
							if($tmp_dev){
								echo "<br>TOTAL RECORDS {$total_records}<br><br>";
								$total_records = 0;
							}
							$tmp_dev = $row->development_id;
							echo "<br><br><strong>{$row->development_id} {$row->development_name}</strong><br><br>";
						}
						echo "{$row->builder_name}: {$row->total_packages}<br>";
					}
					$total_records += $row->total_packages;
				}
			}
			echo "<br>TOTAL RECORDS {$total_records}<br><br>";
			echo '<br><br><br><hr><br><br>';
		}
		echo '</pre>';
	}

	function hnlchangelog()
	{
		$this->load->model('Model_user', '', TRUE);
		$this->load->model('Model_hnl_change_log', '', TRUE);

		$change_logs  = $this->Model_hnl_change_log->getHnLChangeLogs(NULL);
		$data         = array(
			'hnl_change_logs' => $change_logs
		);
		$this->load_template('admin/reports/view_hnlchangelog', $data);
	}

	function reminderlog()
	{
		$this->load->model('Model_reminder_log', '', TRUE);
		$reminder_logs = $this->Model_reminder_log->getReminderLogs(NULL);
		$data          = array(
			'reminder_logs'  => $reminder_logs
		);
		$this->load_template('admin/reports/view_reminderlog', $data);
	}

	function overdueusersreport()
	{
		$this->load->model('Model_reminder', '', TRUE);

		$overdue_developments = $this->Model_reminder->getOverdueDevelopments();
		$data          = array(
			'overdue_developments' => $overdue_developments
		);
		$this->load_template('admin/reports/view_overdueusersreport', $data);
	}

	function problemusersreport()
	{
		$this->load->model('Model_reminder', '', TRUE);

		$problem_users = $this->Model_reminder->getProblemUsers();
		$data          = array(
			'problem_users' => $problem_users
		);
		$this->load_template('admin/reports/view_problemusersreport', $data);
	}


	function availablelots()
	{
		$this->load->model('Model_development', '', TRUE);
		$developments = $this->Model_development->getDevelopments();
		$data            = array(
			'developments' => $developments,
		);
		$this->load_template('admin/reports/view_availablelots', $data);
	}

	function availablelotsreport($development_id)
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_availablelot', '', TRUE);

		$development            = $this->Model_development->getDevelopment($development_id);
		$available_lots         = $this->Model_availablelot->getAvailableLots($development_id, 3);
		$month3                 = date('Y-m-01', strtotime('-3 months', time()));
		$month6                 = date('Y-m-01', strtotime('-6 months', time()));
		$current_available_lots = $this->Model_availablelot->getTotalLots($development_id, $month3, $month6);
		$formated_labels        = array('Current');
		$formated_results       = array(
			'greaterthan90days'   => array($current_available_lots['lots_lessthan3mths']),
			'between90and180days' => array($current_available_lots['lots_btw3and6mths']),
			'greaterthan180days'  => array($current_available_lots['lots_morethan6mths']),
			'totals'              => array($current_available_lots['lots_lessthan3mths'] + $current_available_lots['lots_btw3and6mths'] + $current_available_lots['lots_morethan6mths']),
		);
		foreach($available_lots as $available_lot_data){
			$formated_labels[]                         = date('jS \o\f F Y', strtotime($available_lot_data->available_date));
			$formated_results['greaterthan90days'][]   = $available_lot_data->lots_lessthan3mths;
			$formated_results['between90and180days'][] = $available_lot_data->lots_btw3and6mths;
			$formated_results['greaterthan180days'][]  = $available_lot_data->lots_morethan6mths;
			$formated_results['totals'][]              = $available_lot_data->lots_lessthan3mths+$available_lot_data->lots_btw3and6mths+$available_lot_data->lots_morethan6mths;
		}

		$data = array(
			'development'      => $development,
			'formated_labels'  => $formated_labels,
			'formated_results' => $formated_results
		);
		$this->load_template('admin/reports/view_availablelotsreport', $data);
	}

	public function statisticsalldev()
	{
		$this->statisticsalldevview('statisticsreport', 'MAPOVIS Statistics');
	}

	function statisticsreport($development_id)
	{
		require_once(BASEPATH .'../assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php');
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_statistics_lotview', '', TRUE);
		$this->load->model('Model_statistics_placesearch', '', TRUE);
		$this->load->model('Model_statistics_directionsearch', '', TRUE);

		$development = $this->Model_development->getDevelopment($development_id);

		$this->load->library('form_validation');
		$start_date = date('Y-m-d', strtotime('-30 day'));
		$end_date   = date('Y-m-d 23:59:59', strtotime('-1 day'));
		$form_data  = $this->input->post('dates');
		if($form_data){
			$dates = explode(' to ', $form_data);
			if(strtotime($dates[0])){
				$start_date = date('Y-m-d', strtotime($dates[0]));
			}
			if(strtotime($dates[1])){
				$end_date   = date('Y-m-d 23:59:59', strtotime($dates[1]));
			}
		}
		$start_time     = strtotime($start_date);
		$end_time       = strtotime($end_date);
		$total_days     = ceil(abs(($end_time - $start_time)) / 86400);

		$skip_days      = ceil($total_days/30);
		$reduce_display = ($total_days > 32);
		$counter        = 0;
		for($date = $start_time; $date < $end_time; $date = strtotime('+1 day', $date)){
			if($reduce_display){
				// show weekly dates
				$display_date = ($counter % $skip_days == 0)? date('d/m', $date): '';
			}
			else{
				$display_date = date('d/m', $date);
			}
			$report_dates[date('Y-m-d', $date)] = $display_date;
			$counter++;
		}
		$lot_views_by_lot                 = $this->Model_statistics_lotview->getViews($development_id, $start_date, $end_date);
		$lot_views_by_date                = $this->Model_statistics_lotview->getViewsByDay($development_id, $start_date, $end_date);
		$place_search_views_by_phrase     = $this->Model_statistics_placesearch->getViews($development_id, $start_date, $end_date);
		$place_search_views_by_date       = $this->Model_statistics_placesearch->getViewsByDay($development_id, $start_date, $end_date);
		$direction_search_views_by_phrase = $this->Model_statistics_directionsearch->getViews($development_id, $start_date, $end_date);
		$direction_search_views_by_date   = $this->Model_statistics_directionsearch->getViewsByDay($development_id, $start_date, $end_date);
		$action_type                      = $this->input->post('action_type');
		if($action_type && $action_type != 'submit'){
			$tmp_file_name   = '/tmp/temp_sold_'.time().'.csv';
			$fp              = fopen($tmp_file_name, 'w');
			switch($action_type){
				case 'export_direcsearch':
					$file_name   = 'Directions_Searches';
					$csv_headers = array('Address', 'Percentage', 'Searches');
					fputcsv($fp, $csv_headers);
					
					$direction_views_total    = 0;
					foreach($direction_search_views_by_phrase as $phrase_view){
						$direction_views_total += $phrase_view->views;
					}
					foreach($direction_search_views_by_phrase as $phrase_view){
						$csv_data   = array($phrase_view->direction_address, (($direction_views_total)? round($phrase_view->views / $direction_views_total * 100, 1): 0).'%',$phrase_view->views);
						fputcsv($fp, $csv_data);
					}
					break;
				case 'export_placesearch':
					$file_name   = 'Places_Searches';
					$csv_headers = array('Place', 'Percentage', 'Searches');
					fputcsv($fp, $csv_headers);

					$place_views_total    = 0;
					foreach($place_search_views_by_phrase as $phrase_view){
						$place_views_total += $phrase_view->views;
					}
					foreach($place_search_views_by_phrase as $phrase_view){
						$csv_data   = array($phrase_view->search_phrase, (($place_views_total)? round($phrase_view->views/$place_views_total *100, 1): 0).'%', $phrase_view->views);
						fputcsv($fp, $csv_data);
					}
					break;
				case 'export_lotviews':
				default:
					$file_name   = 'Lot_Views';
					$csv_headers = array('Views', 'Percentage', 'Lot Number', 'Stage', 'Precinct');
					fputcsv($fp, $csv_headers);

					$lot_views_total    = 0;
					foreach($lot_views_by_lot as $lot_view){
						$lot_views_total += $lot_view->views;
					}
					foreach($lot_views_by_lot as $lot_view){
						$csv_data   = array($lot_view->views, (($lot_views_total)? round($lot_view->views / $lot_views_total * 100, 1): 0).'%', $lot_view->lot_number, $lot_view->stage_number, $lot_view->precinct_number);
						fputcsv($fp, $csv_data);
					}
					break;
			}

			fclose($fp);
			$csv_file_content = file_get_contents($tmp_file_name);
			$file_name        = $development->development_name.'_'.$file_name.'_'.date('Y-m-d').'.csv';
			if($csv_file_content){
				header('Content-Type: application/csv');
				header('Content-Disposition: attachement; filename="'.$file_name.'"');
				echo $csv_file_content;
				exit();
			}
		}

		$data = array(
			'development'                      => $development,
			'lot_views_by_lot'                 => $lot_views_by_lot,
			'lot_views_by_date'                => $lot_views_by_date,
			'place_search_views_by_phrase'     => $place_search_views_by_phrase,
			'place_search_views_by_date'       => $place_search_views_by_date,
			'direction_search_views_by_phrase' => $direction_search_views_by_phrase,
			'direction_search_views_by_date'   => $direction_search_views_by_date,
			'report_dates'                     => $report_dates,
			'start_date'                       => $start_date,
			'end_date'                         => $end_date,
			'total_days'                       => $total_days
		);
		$footer = array('admin/footer_standard', 'admin/reports/view_statisticsreport_customjs');
		$this->load_template('admin/reports/view_statisticsreport', $data, $footer);
	}
    
    public function alldevsstatisticsreport(){

        require_once(BASEPATH .'../assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php');
        $this->load->model('Model_development', '', TRUE); 
        $this->load->model('Model_statistics_lotview', '', TRUE);
        $this->load->model('Model_statistics_placesearch', '', TRUE);
        $this->load->model('Model_statistics_directionsearch', '', TRUE);
        $this->load->model('Model_statistics_externalamenityview', '', TRUE); 
        $this->load->model('Model_statistics_externalamenitycategoryview', '', TRUE);  
        $this->load->model('Model_statistics_interactivemasterplan', '', TRUE); 
        $this->load->model('Model_statistics_amenityview', '', TRUE);
        $this->load->model('Model_statistics_land', '', TRUE);
        $this->load->model('Model_statistics_house_land', '', TRUE);
        
        $this->load->library('mapovis_lib');  
        
        $chart_colors = $this->mapovis_lib->statisticsColorCodes();  
        
        $this->load->library('form_validation');  
        $start_date = date('Y-m-d', strtotime('-90 day'));
        $end_date   = date('Y-m-d 23:59:59', strtotime('-1 day'));
        
        $form_data  = $this->input->post('dates');
        if($form_data){
            $dates = explode(' to ', $form_data);
            if(strtotime($dates[0])){
                $start_date = date('Y-m-d', strtotime($dates[0]));
            }
            if(strtotime($dates[1])){
                $end_date   = date('Y-m-d 23:59:59', strtotime($dates[1]));
            }
        }

        $start_time     = strtotime($start_date);
        $end_time       = strtotime($end_date);
        $total_days     = ceil(abs(($end_time - $start_time)) / 86400);
        
        for($date = $start_time; $date < $end_time; $date = strtotime('+7 day', $date)){
            $display_date = date('d/m', $date);
            $report_dates[date('Y-m-d', $date)] = $display_date;
        }
        
        $active_developments = array();
        $all_active_developments = $this->Model_development->getDevelopments(1);
         
        $action_type = $this->input->post('action_type');
        
        if($action_type == 'submit'){
           
           $form_selected_developments = $this->input->post('selected_developments'); 
           
           if($form_selected_developments){
               foreach($form_selected_developments as $development_id => $development){
                   $lot_views_by_date_developments[$development_id] = $this->Model_statistics_lotview->getViewsByDay($development_id, $start_date, $end_date);   
                   $place_search_views_by_date_developments[$development_id]       = $this->Model_statistics_placesearch->getViewsByDay($development_id, $start_date, $end_date);
                   $direction_search_views_by_date_developments[$development_id]   = $this->Model_statistics_directionsearch->getViewsByDay($development_id, $start_date, $end_date);
                   $externalamenity_views_by_date_developments[$development_id]   = $this->Model_statistics_externalamenityview->getViewsByDay($development_id, $start_date, $end_date);
                   $externalamenitycategory_views_by_date_developments[$development_id]   = $this->Model_statistics_externalamenitycategoryview->getViewsByDay($development_id, $start_date, $end_date);
                   $interactivemasterplan_views_by_date_developments[$development_id]   = $this->Model_statistics_interactivemasterplan->getViewsByDay($development_id, $start_date, $end_date);
                   $amenity_views_by_date_developments[$development_id]   = $this->Model_statistics_amenityview->getViewsByDay($development_id, $start_date, $end_date);
                   $landsearches_by_date_developments[$development_id] = $this->Model_statistics_land->getViewsByDay($development_id, $start_date, $end_date);
                   $houselandsearches_by_date_developments[$development_id] = $this->Model_statistics_house_land->getViewsByDay($development_id, $start_date, $end_date); 
                   
                   $active_development = $this->Model_development->getDevelopment($development_id);
                   array_push($active_developments,$active_development);   
              }  
            
           }
           else{
                $active_developments = array();
                $lot_views_by_date_developments = array();
                $place_search_views_by_date_developments = array();
                $direction_search_views_by_date_developments = array();
                $externalamenity_views_by_date_developments = array();
                $externalamenitycategory_views_by_date_developments = array();
                $interactivemasterplan_views_by_date_developments = array();
                $amenity_views_by_date_developments = array();
                $landsearches_by_date_developments = array();
                $houselandsearches_by_date_developments = array();
           }
           
          
        }
        
        else{
            
            $active_developments = $this->Model_development->getDevelopments(1);  
            
            foreach($active_developments as $key => $development){
         
               $lot_views_by_date_developments[$development->development_id] = $this->Model_statistics_lotview->getViewsByDay($development->development_id, $start_date, $end_date);   
               $place_search_views_by_date_developments[$development->development_id]       = $this->Model_statistics_placesearch->getViewsByDay($development->development_id, $start_date, $end_date);
               $direction_search_views_by_date_developments[$development->development_id]   = $this->Model_statistics_directionsearch->getViewsByDay($development->development_id, $start_date, $end_date);
               $externalamenity_views_by_date_developments[$development->development_id]   = $this->Model_statistics_externalamenityview->getViewsByDay($development->development_id, $start_date, $end_date);
               $externalamenitycategory_views_by_date_developments[$development->development_id]   = $this->Model_statistics_externalamenitycategoryview->getViewsByDay($development->development_id, $start_date, $end_date);
               $interactivemasterplan_views_by_date_developments[$development->development_id]   = $this->Model_statistics_interactivemasterplan->getViewsByDay($development->development_id, $start_date, $end_date);
               $amenity_views_by_date_developments[$development->development_id]   = $this->Model_statistics_amenityview->getViewsByDay($development->development_id, $start_date, $end_date);
               $landsearches_by_date_developments[$development->development_id] = $this->Model_statistics_land->getViewsByDay($development->development_id, $start_date, $end_date);
               $houselandsearches_by_date_developments[$development->development_id] = $this->Model_statistics_house_land->getViewsByDay($development->development_id, $start_date, $end_date); 
            } 
        }
            
        $data = array(
            'developments'                     => $active_developments, 
            'all_active_developments' => $all_active_developments,
            'lot_views_by_date_developments'   => $lot_views_by_date_developments,
            'place_search_views_by_date_developments' => $place_search_views_by_date_developments,
            'direction_search_views_by_date_developments'   => $direction_search_views_by_date_developments,
            'externalamenity_views_by_date_developments'    => $externalamenity_views_by_date_developments,
            'externalamenitycategory_views_by_date_developments' => $externalamenitycategory_views_by_date_developments,
            'interactivemasterplan_views_by_date_developments' => $interactivemasterplan_views_by_date_developments,
            'amenity_views_by_date_developments' => $amenity_views_by_date_developments,
            'landsearches_by_date_developments' => $landsearches_by_date_developments,
            'houselandsearches_by_date_developments' => $houselandsearches_by_date_developments,
            'report_dates'                     => $report_dates, 
            'start_date'                       => $start_date,
            'end_date'                         => $end_date,
            'total_days'                       => $total_days,
            'chart_colors'                     => $chart_colors,  
        );
        
        $footer = array('admin/footer_standard', 'admin/reports/view_statisticsreport_customjs');
        $this->load_template('admin/reports/view_alldevsstatisticsgraph', $data,$footer);
    
    }

	public function lotviewsstatisticsalldev()
	{
		$this->statisticsalldevview('lotviewsstatisticsreport', 'Impressions by Lot Frontage');
	}

	function lotviewsstatisticsreport($development_id)
	{
		require_once(BASEPATH .'../assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php');
		$this->load->library('mapovis_lib');
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_statistics_lotview', '', TRUE);
		$this->load->library('form_validation');

		$development        = $this->Model_development->getDevelopment($development_id);
		$start_date         = date('Y-m-d', strtotime('-30 weeks'));
		$end_date           = date('Y-m-d 23:59:59', strtotime('last sunday'));
		$lot_views_by_width = $this->Model_statistics_lotview->getWeeklyViews($development_id, $start_date, $end_date);
		$chart_colors       = $this->mapovis_lib->statisticsColorCodes();
		$export_csv         = $this->input->post('export_csv');
		if($export_csv == 1){
			$tmp_file_name   = '/tmp/temp_impressions_'.time().'.csv';
			$fp              = fopen($tmp_file_name, 'w');
			$csv_headers     = array('Week Ending');
			foreach($lot_views_by_width['widths'] as $width){
				$csv_headers[] = $width.'m';
			}
			$csv_headers[] = 'Total';
			fputcsv($fp, $csv_headers);
			foreach($lot_views_by_width['results'] as $week => $week_results){
				$total_lots = 0;
				$csv_data   = array($week);
				foreach($lot_views_by_width['widths'] as $width){
					$lot_views   = (isset($week_results[$width]->number_lots))? (int)$week_results[$width]->number_lots: 0;
					$total_lots += $lot_views;
					$csv_data[]  = $lot_views;
				}
				$csv_data[] = $total_lots;
				fputcsv($fp, $csv_data);
			}
			fclose($fp);
			$csv_file_content = file_get_contents($tmp_file_name);
			$file_name        = $development->development_name.' Impressions by Lot Frontage '.$start_date.' to '.$start_date.'.csv';
			if($csv_file_content){
				header('Content-Type: application/csv');
				header('Content-Disposition: attachement; filename="'.$file_name.'"');
				echo $csv_file_content;
				exit();
			}
		}
		$data        = array(
			'development'                      => $development,
			'lot_views_by_width'               => $lot_views_by_width,
			'chart_colors'                     => $chart_colors,
		);
		$footer = array('admin/footer_standard', 'admin/reports/view_statisticsreport_customjs');
		$this->load_template('admin/reports/view_statisticslotviewsreport', $data, $footer);
	}

	public function availlotstatisticsalldev()
	{
		$this->statisticsalldevview('availlotstatisticsreport', 'Available Lots On Hand by Frontage');
	}

	function availlotstatisticsreport($development_id)
	{
		require_once(BASEPATH .'../assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php');
		$this->load->library('mapovis_lib');
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_availablelot_by_width', '', TRUE);

		$development             = $this->Model_development->getDevelopment($development_id);
		$available_lots_by_width = $this->Model_availablelot_by_width->getAvailableLotsFormatted($development_id);
		$chart_colors            = $this->mapovis_lib->statisticsColorCodes();
		$export_csv              = $this->input->post('export_csv');
		if($export_csv == 1){
			$tmp_file_name   = '/tmp/temp_available_'.time().'.csv';
			$fp              = fopen($tmp_file_name, 'w');
			$csv_headers     = array('Month');
			foreach($available_lots_by_width['widths'] as $width){
				$csv_headers[] = $width.'m';
			}
			$csv_headers[] = 'Total';
			fputcsv($fp, $csv_headers);

			foreach($available_lots_by_width['results'] as $month => $month_results){
				$total_lots = 0;
				$csv_data   = array(date('M Y', strtotime('-1 month', strtotime($month))));
				foreach($available_lots_by_width['widths'] as $width){
					$lot_views   = (isset($month_results[$width]->number_lots))? (int)$month_results[$width]->number_lots: 0;
					$total_lots += $lot_views;
					$csv_data[]  = $lot_views;
				}
				$csv_data[] = $total_lots;
				fputcsv($fp, $csv_data);
			}
			fclose($fp);
			$csv_file_content = file_get_contents($tmp_file_name);
			$file_name        = $development->development_name.'  Available Lots On Hand by Frontage'.date('Y-m-d').'.csv';
			if($csv_file_content){
				header('Content-Type: application/csv');
				header('Content-Disposition: attachement; filename="'.$file_name.'"');
				echo $csv_file_content;
				exit();
			}
		}

		$data   = array(
			'development'             => $development,
			'available_lots_by_width' => $available_lots_by_width,
			'chart_colors'            => $chart_colors,
		);
		$footer = array('admin/footer_standard', 'admin/reports/view_statisticsreport_customjs');
		$this->load_template('admin/reports/view_statisticsavaillotreport', $data, $footer);
	}

	public function soldlotstatisticsalldev()
	{
		$this->statisticsalldevview('soldlotstatisticsreport', 'Sales Rate by Lot Frontage');
	}

	function soldlotstatisticsreport($development_id)
	{
		require_once(BASEPATH .'../assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php');
		$this->load->library('mapovis_lib');
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_change_log', '', TRUE);

		$development             = $this->Model_development->getDevelopment($development_id);
		$sold_lots_by_width      = $this->Model_change_log->getSoldLotsFormatted($development_id);
		$export_csv              = $this->input->post('export_csv');
		if($export_csv == 1){
			$tmp_file_name   = '/tmp/temp_sold_'.time().'.csv';
			$fp              = fopen($tmp_file_name, 'w');
			$csv_headers     = array('Month');
			foreach($sold_lots_by_width['widths'] as $width){
				$csv_headers[] = $width.'m';
			}
			$csv_headers[] = 'Total';
			fputcsv($fp, $csv_headers);

			foreach($sold_lots_by_width['results'] as $month => $month_results){
				$total_lots = 0;
				$csv_data   = array(date('M Y', strtotime($month)));
				foreach($sold_lots_by_width['widths'] as $width){
					$lot_views   = (isset($month_results[$width]->number_lots))? (int)$month_results[$width]->number_lots: 0;
					$total_lots += $lot_views;
					$csv_data[]  = $lot_views;
				}
				$csv_data[] = $total_lots;
				fputcsv($fp, $csv_data);
			}
			fclose($fp);
			$csv_file_content = file_get_contents($tmp_file_name);
			$file_name        = $development->development_name.'  Available Lots On Hand by Frontage'.date('Y-m-d').'.csv';
			if($csv_file_content){
				header('Content-Type: application/csv');
				header('Content-Disposition: attachement; filename="'.$file_name.'"');
				echo $csv_file_content;
				exit();
			}
		}

		$data   = array(
			'development'             => $development,
			'sold_lots_by_width'      => $sold_lots_by_width,
			'chart_colors'            => $this->mapovis_lib->statisticsColorCodes(),
		);
		$footer = array('admin/footer_standard', 'admin/reports/view_statisticsreport_customjs');
		$this->load_template('admin/reports/view_statisticssoldlotreport', $data, $footer);
	}

	public function averagelotpricesreportalldev()
	{
		$this->statisticsalldevview('averagelotpricesreport', 'Average Listed Price ($\'000) per Lot Frontage');
	}

	public function averagelotpricesreport($development_id)
	{
		require_once(BASEPATH .'../assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php');
		$this->load->library('mapovis_lib');
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_average_lot_prices_by_width', '', TRUE);
		$development        = $this->Model_development->getDevelopment($development_id);
		$avg_price_by_width = $this->Model_average_lot_prices_by_width->getAverageLotPricesFormatted($development_id);
		$export_csv         = $this->input->post('export_csv');
		if($export_csv == 1){
			$tmp_file_name   = '/tmp/temp_avgprice_'.time().'.csv';
			$fp              = fopen($tmp_file_name, 'w');
			$csv_headers     = array('Month');
			foreach($avg_price_by_width['widths'] as $width){
				$csv_headers[] = $width.'m';
			}
			fputcsv($fp, $csv_headers);

			foreach($avg_price_by_width['results'] as $month => $month_results){
				$csv_data   = array(date('M Y', strtotime('-1 month', strtotime($month))));
				foreach($avg_price_by_width['widths'] as $width){
					$lot_views   = (isset($month_results[$width]->average_price))? number_format($month_results[$width]->average_price/1000): 0;
					$csv_data[]  = $lot_views;
				}
				fputcsv($fp, $csv_data);
			}
			fclose($fp);
			$csv_file_content = file_get_contents($tmp_file_name);
			$file_name        = $development->development_name.'  Average Listed Price per Lot Frontage'.date('Y-m-d').'.csv';
			if($csv_file_content){
				header('Content-Type: application/csv');
				header('Content-Disposition: attachement; filename="'.$file_name.'"');
				echo $csv_file_content;
				exit();
			}
		}

		$data   = array(
			'development'        => $development,
			'avg_price_by_width' => $avg_price_by_width,
			'chart_colors'       => $this->mapovis_lib->statisticsColorCodes(),
		);
		$footer = array('admin/footer_standard', 'admin/reports/view_statisticsreport_customjs');
		$this->load_template('admin/reports/view_averagelotpricesreport', $data, $footer);
	}

	private function statisticsalldevview($actionview, $titlelabel)
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_developer', '', TRUE);
		$developments = $this->Model_development->getDevelopments();
		$developers   = $this->Model_developer->getDevelopers(TRUE);
		$data         = array(
			'developments' => $developments,
			'developers'   => $developers,
			'actionview'   => $actionview,
			'titlelabel'   => $titlelabel
		);
		$this->load_template('admin/reports/view_allstatisticsalldev', $data);
	}

	function statisticwordreport($development_id)
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->library('form_validation');

		$development = $this->Model_development->getDevelopment($development_id);
		$start_date  = date('Y-m-d', strtotime('-30 day'));
		$end_date    = date('Y-m-d 23:59:59', strtotime('-1 day'));
		$form_data   = $this->input->post('dates');
		if($form_data){
			$dates = explode(' to ', $form_data);
			if(strtotime($dates[0])){
				$start_date = date('Y-m-d', strtotime($dates[0]));
			}
			if(strtotime($dates[1])){
				$end_date   = date('Y-m-d 23:59:59', strtotime($dates[1]));
			}
		}
		$action_type  = $this->input->post('action_type');
		$new_file_url = FALSE;
		if($action_type == 'submit'){
			$start_time = strtotime($start_date)*1000;
			$end_time   = strtotime($end_date)*1000;

			$this->db->where('statistics.timestamp >=', $start_time);
			$this->db->where('statistics.timestamp <=', $end_time);
			$this->db->where('statistics.developmentID_opened', $development_id);
			$query1  = $this->db->get('statistics');
			$interactive_master_plan = $query1->num_rows();

			$this->db->where('statistics_lotViews.timestamp >=', $start_time);
			$this->db->where('statistics_lotViews.timestamp <=', $end_time);
			$this->db->where('statistics_lotViews.developmentID', $development_id);
			$query2  = $this->db->get('statistics_lotViews');
			$click_on_lot = $query2->num_rows();

			$this->db->select('lots.lot_width AS label_name, COUNT(lots.lot_id) AS total_clicks');
			$this->db->join('lots', 'lots.lot_id = statistics_lotViews.lotID');
			$this->db->where('statistics_lotViews.timestamp >=', $start_time);
			$this->db->where('statistics_lotViews.timestamp <=', $end_time);
			$this->db->where('statistics_lotViews.developmentID', $development_id);
			$this->db->group_by('lots.lot_width');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('lots.lot_width');
			$query3  = $this->db->get('statistics_lotViews', 5);
			$click_on_lot_by_width = $query3->result();

			// internal amenities
			$this->db->where('statistics_amenitiesViews.datetime >=', $start_time);
			$this->db->where('statistics_amenitiesViews.datetime <=', $end_time);
			$this->db->where('statistics_amenitiesViews.development_id', $development_id);
			$query4  = $this->db->get('statistics_amenitiesViews');
			$click_on_amenities = $query4->num_rows();

			$this->db->select('amenities.amenity_name AS label_name, COUNT(amenities.amenity_id) AS total_clicks');
			$this->db->join('amenities', 'amenities.amenity_id = statistics_amenitiesViews.amenity_id');
			$this->db->where('statistics_amenitiesViews.datetime >=', $start_time);
			$this->db->where('statistics_amenitiesViews.datetime <=', $end_time);
			$this->db->where('statistics_amenitiesViews.development_id', $development_id);
			$this->db->group_by('amenities.amenity_name');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('amenities.amenity_name');
			$query5  = $this->db->get('statistics_amenitiesViews', 5);
			$click_on_amenities_each = $query5->result();

			// external amenities categories
			$this->db->where('statistics_externalAmenityCategoryClicks.datetime >=', $start_time);
			$this->db->where('statistics_externalAmenityCategoryClicks.datetime <=', $end_time);
			$this->db->where('statistics_externalAmenityCategoryClicks.development_id', $development_id);
			$query6  = $this->db->get('statistics_externalAmenityCategoryClicks');
			$click_on_cat_ex_amenities = $query6->num_rows();

			$this->db->select('external_amenity_types.e_amenity_type_name AS label_name, COUNT(external_amenity_types.external_amenity_type_id) AS total_clicks');
			$this->db->join('external_amenity_types', 'external_amenity_types.external_amenity_type_id = statistics_externalAmenityCategoryClicks.external_amenity_type_id');
			$this->db->where('statistics_externalAmenityCategoryClicks.datetime >=', $start_time);
			$this->db->where('statistics_externalAmenityCategoryClicks.datetime <=', $end_time);
			$this->db->where('statistics_externalAmenityCategoryClicks.development_id', $development_id);
			$this->db->group_by('external_amenity_types.e_amenity_type_name');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('external_amenity_types.e_amenity_type_name');
			$query7  = $this->db->get('statistics_externalAmenityCategoryClicks');
			$click_on_cat_ex_amenities_each = $query7->result();

			// external amenities
			$this->db->where('statistics_externalAmenitiesViews.datetime >=', $start_time);
			$this->db->where('statistics_externalAmenitiesViews.datetime <=', $end_time);
			$this->db->where('statistics_externalAmenitiesViews.development_id', $development_id);
			$query8  = $this->db->get('statistics_externalAmenitiesViews');
			$click_on_ex_amenities = $query8->num_rows();

			$this->db->select('external_amenities.e_amenity_name AS label_name, COUNT(external_amenities.external_amenity_id) AS total_clicks');
			$this->db->join('external_amenities', 'external_amenities.external_amenity_id = statistics_externalAmenitiesViews.externalAmenity_id');
			$this->db->where('statistics_externalAmenitiesViews.datetime >=', $start_time);
			$this->db->where('statistics_externalAmenitiesViews.datetime <=', $end_time);
			$this->db->where('statistics_externalAmenitiesViews.development_id', $development_id);
			$this->db->group_by('external_amenities.e_amenity_name');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('external_amenities.e_amenity_name');
			$query9  = $this->db->get('statistics_externalAmenitiesViews', 5);
			$click_on_ex_amenities_each = $query9->result();

			// Direction Searches
			$this->db->where('statistics_directionSearches.timestamp >=', $start_time);
			$this->db->where('statistics_directionSearches.timestamp <=', $end_time);
			$this->db->where('statistics_directionSearches.developmentID', $development_id);
			$query10  = $this->db->get('statistics_directionSearches');
			$click_on_direction_search = $query10->num_rows();

			$this->db->select('statistics_directionSearches.direction_address AS label_name, COUNT(statistics_directionSearches.ID) AS total_clicks');
			$this->db->where('statistics_directionSearches.timestamp >=', $start_time);
			$this->db->where('statistics_directionSearches.timestamp <=', $end_time);
			$this->db->where('statistics_directionSearches.developmentID', $development_id);
			$this->db->where('statistics_directionSearches.direction_address !=', '');
			$this->db->group_by('statistics_directionSearches.direction_address');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('statistics_directionSearches.direction_address');
			$query11  = $this->db->get('statistics_directionSearches', 5);
			$click_on_direction_search_each = $query11->result();

			// Places Searches
			$this->db->where('statistics_placesSearches.timestamp >=', $start_time);
			$this->db->where('statistics_placesSearches.timestamp <=', $end_time);
			$this->db->where('statistics_placesSearches.developmentID', $development_id);
			$query12  = $this->db->get('statistics_placesSearches');
			$click_on_place_search = $query12->num_rows();

			$this->db->select('statistics_placesSearches.search_phrase AS label_name, COUNT(statistics_placesSearches.ID) AS total_clicks');
			$this->db->where('statistics_placesSearches.timestamp >=', $start_time);
			$this->db->where('statistics_placesSearches.timestamp <=', $end_time);
			$this->db->where('statistics_placesSearches.developmentID', $development_id);
			$this->db->group_by('statistics_placesSearches.search_phrase');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('statistics_placesSearches.search_phrase');
			$query13  = $this->db->get('statistics_placesSearches', 5);
			$click_on_place_search_each = $query13->result();

			// Zoom to lot
			$this->db->where('statistics_zoomToLot.timestamp >=', $start_time);
			$this->db->where('statistics_zoomToLot.timestamp <=', $end_time);
			$this->db->where('statistics_zoomToLot.developmentID', $development_id);
			$query14           = $this->db->get('statistics_zoomToLot');
			$click_on_zoom_lot = $query14->num_rows();


			// land search
			$this->db->where('statistics_land.datetime >=', $start_date);
			$this->db->where('statistics_land.datetime <=', $end_date);
			$this->db->where('statistics_land.development_id', $development_id);
			$query15           = $this->db->get('statistics_land');
			$click_on_land_search = $query15->num_rows();

			$this->db->select('COUNT(IF(statistics_land.lot_width != "",1,NULL)) AS total_width, COUNT(IF(statistics_land.lot_square_meters != "",1,NULL)) AS total_size', FALSE);
			$this->db->where('statistics_land.datetime >=', $start_date);
			$this->db->where('statistics_land.datetime <=', $end_date);
			$this->db->where('statistics_land.development_id', $development_id);
			$query15                   = $this->db->get('statistics_land');
			$click_on_land_search_totals = $query15->row();

			$this->db->select('statistics_land.lot_width AS label_name, COUNT(statistics_land.statistics_land_id) AS total_clicks');
			$this->db->where('statistics_land.datetime >=', $start_date);
			$this->db->where('statistics_land.datetime <=', $end_date);
			$this->db->where('statistics_land.development_id', $development_id);
			$this->db->where('statistics_land.lot_width !=', '');
			$this->db->group_by('statistics_land.lot_width');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('statistics_land.lot_width');
			$query15  = $this->db->get('statistics_land', 5);
			$click_on_land_search_width = $query15->result();

			$this->db->select('statistics_land.lot_square_meters AS label_name, COUNT(statistics_land.statistics_land_id) AS total_clicks');
			$this->db->where('statistics_land.datetime >=', $start_date);
			$this->db->where('statistics_land.datetime <=', $end_date);
			$this->db->where('statistics_land.development_id', $development_id);
			$this->db->where('statistics_land.lot_square_meters !=', '');
			$this->db->group_by('statistics_land.lot_square_meters');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('statistics_land.lot_square_meters');
			$query15  = $this->db->get('statistics_land', 5);
			$click_on_land_search_size = $query15->result();

			// house and land search
			$this->db->where('statistics_house_land.datetime >=', $start_date);
			$this->db->where('statistics_house_land.datetime <=', $end_date);
			$this->db->where('statistics_house_land.development_id', $development_id);
			$query16             = $this->db->get('statistics_house_land');
			$click_on_hnl_search = $query16->num_rows();

			$this->db->select('COUNT(IF((statistics_house_land.houseland_price_from != "" AND statistics_house_land.houseland_price_from IS NOT null) OR (statistics_house_land.houseland_price_to != "" AND statistics_house_land.houseland_price_to IS NOT null),1,NULL)) AS total_price', FALSE);
			$this->db->select('COUNT(IF(statistics_house_land.lot_square_meters != "" AND statistics_house_land.lot_square_meters IS NOT null,1,NULL)) AS total_size', FALSE);
			$this->db->select('COUNT(IF(statistics_house_land.house_bedrooms != "" AND statistics_house_land.house_bedrooms IS NOT null,1,NULL)) AS total_bedrooms', FALSE);
			$this->db->select('COUNT(IF(statistics_house_land.house_bathrooms != "" AND statistics_house_land.house_bathrooms IS NOT null,1,NULL)) AS total_bathrooms', FALSE);
			$this->db->select('COUNT(IF(statistics_house_land.builder_id != "" AND statistics_house_land.builder_id IS NOT null,1,NULL)) AS total_builder', FALSE);
			$this->db->where('statistics_house_land.datetime >=', $start_date);
			$this->db->where('statistics_house_land.datetime <=', $end_date);
			$this->db->where('statistics_house_land.development_id', $development_id);
			$query16                    = $this->db->get('statistics_house_land');
			$click_on_hnl_search_totals = $query16->row();

			$this->db->select('statistics_house_land.lot_square_meters AS label_name, COUNT(statistics_house_land.statistics_house_land_id) AS total_clicks');
			$this->db->where('statistics_house_land.datetime >=', $start_date);
			$this->db->where('statistics_house_land.datetime <=', $end_date);
			$this->db->where('statistics_house_land.development_id', $development_id);
			$this->db->where('statistics_house_land.lot_square_meters !=', '');
			$this->db->where('statistics_house_land.lot_square_meters IS NOT null', NULL);
			$this->db->group_by('statistics_house_land.lot_square_meters');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('statistics_house_land.lot_square_meters');
			$query16  = $this->db->get('statistics_house_land');
			$click_on_hnl_search_size  = $query16->result();
			$formatted_hnl_search_size = array();
			foreach($click_on_hnl_search_size as $sizes){
				$multisizes = explode(',', $sizes->label_name);
				foreach($multisizes as $size){
					@$formatted_hnl_search_size[$size] += $sizes->total_clicks;
				}
			}
			arsort($formatted_hnl_search_size);

			$this->db->select('CONCAT_WS("|", statistics_house_land.houseland_price_from, statistics_house_land.houseland_price_to) AS label_name, COUNT(statistics_house_land.statistics_house_land_id) AS total_clicks', FALSE);
			$this->db->where('statistics_house_land.datetime >=', $start_date);
			$this->db->where('statistics_house_land.datetime <=', $end_date);
			$this->db->where('statistics_house_land.development_id', $development_id);
			$this->db->having('CONCAT_WS("|", statistics_house_land.houseland_price_from, statistics_house_land.houseland_price_to) != ""');
			$this->db->group_by('statistics_house_land.houseland_price_from, statistics_house_land.houseland_price_to');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('label_name');
			$query16  = $this->db->get('statistics_house_land', 5);
			$click_on_hnl_search_price = $query16->result();

			$this->db->select('builders.builder_name AS label_name, COUNT(statistics_house_land.statistics_house_land_id) AS total_clicks');
			$this->db->join('builders', 'builders.builder_id = statistics_house_land.builder_id');
			$this->db->where('statistics_house_land.datetime >=', $start_date);
			$this->db->where('statistics_house_land.datetime <=', $end_date);
			$this->db->where('statistics_house_land.development_id', $development_id);
			$this->db->where('statistics_house_land.builder_id IS NOT null', NULL);
			$this->db->group_by('statistics_house_land.builder_id');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('label_name');
			$query16  = $this->db->get('statistics_house_land', 5);
			$click_on_hnl_search_builder = $query16->result();

			$this->db->select('statistics_house_land.house_bedrooms AS label_name, COUNT(statistics_house_land.statistics_house_land_id) AS total_clicks');
			$this->db->where('statistics_house_land.datetime >=', $start_date);
			$this->db->where('statistics_house_land.datetime <=', $end_date);
			$this->db->where('statistics_house_land.development_id', $development_id);
			$this->db->where('statistics_house_land.house_bedrooms !=', '');
			$this->db->where('statistics_house_land.house_bedrooms IS NOT null', NULL);
			$this->db->group_by('statistics_house_land.house_bedrooms');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('label_name');
			$query16  = $this->db->get('statistics_house_land');
			$click_on_hnl_search_bedrooms = $query16->result();

			$this->db->select('statistics_house_land.house_bathrooms AS label_name, COUNT(statistics_house_land.statistics_house_land_id) AS total_clicks');
			$this->db->where('statistics_house_land.datetime >=', $start_date);
			$this->db->where('statistics_house_land.datetime <=', $end_date);
			$this->db->where('statistics_house_land.development_id', $development_id);
			$this->db->where('statistics_house_land.house_bathrooms !=', '');
			$this->db->where('statistics_house_land.house_bathrooms IS NOT null', NULL);
			$this->db->group_by('statistics_house_land.house_bathrooms');
			$this->db->order_by('total_clicks', 'desc');
			$this->db->order_by('label_name');
			$query16  = $this->db->get('statistics_house_land');
			$click_on_hnl_search_bathrooms = $query16->result();

			require_once APPPATH.'third_party/PHPWord-master/PhpWord/Autoloader.php';
			\PhpOffice\PhpWord\Autoloader::register();

			// Creating the new document...
			$phpWord = new \PhpOffice\PhpWord\PhpWord();
			$phpWord->setDefaultFontName('calibri');
			$phpWord->setDefaultFontSize(14);
			/* Note: any element you append to a document must reside inside of a Section. */
			// Adding an empty Section to the document...
			$section = $phpWord->addSection();

			$section->addText(
				htmlspecialchars(
					$development->development_name.' Statistics Report'
				),
				array('name' => 'Calibri', 'size' => 20, 'color' => '000000', 'bold' => true)
			);
			$section->addTextBreak(1);

			$styleTable        = array('borderSize' => 6, 'borderColor' => '000000', 'valign' => 'center', 'width' => 50 * 50, 'unit' => 'pct', 'align' => 'center');
			$noSpace           = array('spaceAfter' => 0);
			$styleFirstRow     = array('borderBottomSize' => 18, 'bgColor' => '404040');
			$datesStyleTable   = array('borderSize' => 6, 'borderColor' => '000000', 'unit' => 'pct', 'cellMargin' => 80);
			$dates_style       = array();
			$headerdates_style = array('bold' => true, 'color' => 'ffffff');
			$cellColSpan       = array('gridSpan' => 2, 'align' => 'center', 'valign' => 'center');

			$phpWord->addTableStyle('Date Range', $datesStyleTable, $styleFirstRow);
			$table         = $section->addTable('Date Range');
			$table->addRow();
			$table->addCell(9000, $cellColSpan)->addText(htmlspecialchars('Date Range'), $headerdates_style, $noSpace);

			$table->addRow();
			$table->addCell(800)->addText('From', $dates_style, $noSpace);
			$table->addCell(3200)->addText(htmlspecialchars(date('jS \o\f F, Y', strtotime($start_date))), $dates_style, $noSpace);

			$table->addRow();
			$table->addCell(800)->addText('To', $dates_style, $noSpace);
			$table->addCell(3200)->addText(htmlspecialchars(date('jS \o\f F, Y', strtotime($end_date))), $dates_style, $noSpace);

			$section->addTextBreak(1);

			$header_style = array('name' => 'Calibri', 'size' => 18, 'bold' => true, 'color' => 'ffffff', 'bgColor' => '5b9bd5', 'borderSize' => 6, 'borderColor' => '000000', 'cellMargin' => 80);
			$phpWord->addTableStyle('Interactive Master Plan', $header_style);
			$table_plan         = $section->addTable('Interactive Master Plan');
			$table_plan->addRow();
			$table_plan->addCell(9000, $cellColSpan)->addText(htmlspecialchars('Interactive Master Plan'), $header_style, $noSpace);
			$section->addTextBreak(1);

			$tables_style    = array();
			$style_text      = array();
			$style_error     = array('color' => 'D00000');
			$style_text_hd   = array('bold' => true, 'underline' => 'single');
			$cellHCentered   = array('align' => 'center', 'valign' => 'center', 'spaceAfter' => 0);
			$cellVCentered   = array('valign' => 'center');

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The Interactive Master Plan has been opened '.number_format($interactive_master_plan).' times.'),
				$style_text, $noSpace
			);
			$section->addTextBreak(1);

			$section->addText(
				htmlspecialchars('Lots'),
				$style_text_hd
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(htmlspecialchars('Users have clicked on a lot to find more information '.number_format($click_on_lot).' times.'), $style_text);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The width of lots which received the most clicks are:'),
				$style_text
			);
			$section->addTextBreak(1);

			if(count($click_on_lot_by_width)){
				$phpWord->addTableStyle('Lot Width', $styleTable);
				$table         = $section->addTable('Lot Width');
				foreach($click_on_lot_by_width as $index => $width_details){
					$table->addRow();
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars(round($width_details->label_name, 5).'m'), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($width_details->total_clicks).' clicks'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);

			$section->addText(
				htmlspecialchars('Internal Amenities'),
				$style_text_hd
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('Users have clicked to learn more about internal amenities within the development '.number_format($click_on_amenities).' times.'),
				$style_text
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular internal amenities are: '),
				$style_text
			);
			$section->addTextBreak(1);

			if(count($click_on_amenities_each)){
				$phpWord->addTableStyle('Internal Amenities', $styleTable);
				$table         = $section->addTable('Internal Amenities');
				foreach($click_on_amenities_each as $index => $amenity_details){
					$table->addRow();
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars($amenity_details->label_name), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($amenity_details->total_clicks).' clicks'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);

			$section->addText(
				htmlspecialchars('External Amenities'),
				$style_text_hd
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('Users have clicked on the categories of external amenities '.number_format($click_on_cat_ex_amenities).' times.'),
				$style_text
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular categories of external amenities are:'),
				$style_text
			);
			$section->addTextBreak(1);

			if(count($click_on_cat_ex_amenities_each)){
				$phpWord->addTableStyle('External Cat Amenities', $styleTable);
				$table         = $section->addTable('External Cat Amenities');
				foreach($click_on_cat_ex_amenities_each as $index => $item_details){
					$table->addRow();
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars($item_details->label_name), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($item_details->total_clicks).' clicks'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('Users have clicked on external amenities to find out more information '.number_format($click_on_ex_amenities).' times.'),
				$style_text
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular external amenities are:'),
				$style_text
			);
			$section->addTextBreak(1);

			if(count($click_on_ex_amenities_each)){
				$phpWord->addTableStyle('External Cat Amenities', $styleTable);
				$table         = $section->addTable('External Cat Amenities');
				foreach($click_on_ex_amenities_each as $index => $item_details){
					$table->addRow();
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars($item_details->label_name), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($item_details->total_clicks).' clicks'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);

			$section->addText(
				htmlspecialchars('Directions Searches'),
				$style_text_hd
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('Users have undertaken direction searches '.number_format($click_on_direction_search).' times.'),
				$style_text
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular direction searches are: '),
				$style_text
			);
			$section->addTextBreak(1);

			if(count($click_on_direction_search_each)){
				$phpWord->addTableStyle('Directions Searches', $styleTable);
				$table         = $section->addTable('Directions Searches');
				foreach($click_on_direction_search_each as $index => $item_details){
					$table->addRow();
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars($item_details->label_name), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($item_details->total_clicks).' clicks'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);

			$section->addText(
				htmlspecialchars('Nearby Places Searches'),
				$style_text_hd
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('Users have searched for nearby places '.number_format($click_on_place_search).' times.'),
				$style_text
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular nearby places searches are: '),
				$style_text
			);
			$section->addTextBreak(1);

			if(count($click_on_place_search_each)){
				$phpWord->addTableStyle('Nearby Places Searches', $styleTable);
				$table         = $section->addTable('Nearby Places Searches');
				foreach($click_on_place_search_each as $index => $item_details){
					$table->addRow();
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars($item_details->label_name), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($item_details->total_clicks).' clicks'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);

			$section->addText(
				htmlspecialchars('Zoom To Lot'),
				$style_text_hd
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('Users initiated the interactive master plan by zooming to a lot '.number_format($click_on_zoom_lot).' times.'),
				$style_text
			);
			$section->addTextBreak(1);

			$phpWord->addTableStyle('Land For Sale', $header_style);
			$table_plan         = $section->addTable('Land For Sale');
			$table_plan->addRow();
			$table_plan->addCell(9000, $cellColSpan)->addText(htmlspecialchars('Land For Sale'), $header_style, $noSpace);

			$section->addTextBreak(1);


			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('There have been '.number_format($click_on_land_search).' searches for land for sale.'),
				$style_text
			);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('User searches according to:'),
				$style_text
			);
			$search_styles = $styleTable;
			$search_styles['cellMargin'] = 80;

			$phpWord->addTableStyle('Lot Search', $search_styles);
			$table         = $section->addTable('Lot Search');

			$table->addRow();
			$table->addCell(3000)->addText(htmlspecialchars('Lot Width'), $tables_style, $noSpace);
			$table->addCell(3000)->addText(htmlspecialchars(number_format($click_on_land_search_totals->total_width).' times'), $tables_style, $noSpace);

			$table->addRow();
			$table->addCell(3000)->addText(htmlspecialchars('Lot Size'), $tables_style, $noSpace);
			$table->addCell(3000)->addText(htmlspecialchars(number_format($click_on_land_search_totals->total_size).' times'), $tables_style, $noSpace);
			$section->addTextBreak(1);


			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular searches by lot width were:'),
				$style_text
			);
			if(count($click_on_land_search_width)){
				$phpWord->addTableStyle('searches by lot width', $styleTable);
				$table         = $section->addTable('searches by lot width');
				foreach($click_on_land_search_width as $index => $item_details){
					$table->addRow();	
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars(str_replace('|', 'm to ', $item_details->label_name).'m'), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($item_details->total_clicks).' searches'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular searches by lot size were'),
				$style_text
			);
			if(count($click_on_land_search_size)){
				$phpWord->addTableStyle('searches by lot size', $styleTable);
				$table         = $section->addTable('searches by lot size');
				foreach($click_on_land_search_size as $index => $item_details){
					$table->addRow();	
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars(str_replace('|', 'm2 to ', $item_details->label_name).'m2'), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($item_details->total_clicks).' searches'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);

			$phpWord->addTableStyle('House & Land Packages', $header_style);
			$table_plan         = $section->addTable('House & Land Packages');
			$table_plan->addRow();
			$table_plan->addCell(9000, $cellColSpan)->addText(htmlspecialchars('House & Land Packages'), $header_style, $noSpace);
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('There have been '.number_format($click_on_hnl_search).' searches for house and land packages.'),
				$style_text
			);
			$section->addTextBreak();

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('User searches according to:'),
				$style_text
			);

			$hnl_filters = array(
				'total_size'      => 'Lot Size',
				'total_price'     => 'Price',
				'total_builder'   => 'Builder',
				'total_bedrooms'  => 'Bedrooms',
				'total_bathrooms' => 'Bathrooms',
			);
			$phpWord->addTableStyle('HnL by filter', $search_styles);
			$table         = $section->addTable('HnL by filter');
			foreach($hnl_filters as $field => $label){
				$table->addRow();
				$table->addCell(3000)->addText(htmlspecialchars($label), $tables_style, $noSpace);
				$table->addCell(3000)->addText(htmlspecialchars(number_format($click_on_hnl_search_totals->$field).' times'), $tables_style, $noSpace);
			}
			$section->addTextBreak(1);


			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular searches by lot size were:'),
				$style_text
			);
			if(count($formatted_hnl_search_size)){
				$phpWord->addTableStyle('searches by hnl size', $styleTable);
				$table         = $section->addTable('searches by hnl size');
				$index         = 1; 
				foreach($formatted_hnl_search_size as $label_name => $total_clicks){
					$table->addRow();	
					$table->addCell(800)->addText($index, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars(str_replace('|', 'm2 to ', $label_name).'m2'), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($total_clicks).' searches'), $tables_style, $cellHCentered);
					if($index >= 5){
						break;
					}
					$index++;
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);


			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular searches by price were:'),
				$style_text
			);
			if(count($click_on_hnl_search_price)){
				$phpWord->addTableStyle('searches by hnl price', $styleTable);
				$table         = $section->addTable('searches by hnl price');
				foreach($click_on_hnl_search_price as $index => $item_details){
					$prices = explode('|', $item_details->label_name);
					array_walk($prices, function(&$value, $index){
						$value = '$'.number_format($value);
					});
					$table->addRow();	
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars(implode(' to ', $prices)), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($item_details->total_clicks).' searches'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular searches by builder were:'),
				$style_text
			);
			if(count($click_on_hnl_search_builder)){
				$phpWord->addTableStyle('searches by hnl builder', $styleTable);
				$table         = $section->addTable('searches by hnl builder');
				foreach($click_on_hnl_search_builder as $index => $item_details){
					$table->addRow();	
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars($item_details->label_name), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($item_details->total_clicks).' searches'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak();

			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular searches by bedrooms were:'),
				$style_text
			);
			if(count($click_on_hnl_search_bedrooms)){
				$phpWord->addTableStyle('searches by hnl bed', $styleTable);
				$table         = $section->addTable('searches by hnl bed');
				foreach($click_on_hnl_search_bedrooms as $index => $item_details){
					$table->addRow();	
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars($item_details->label_name.' bedrooms'), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($item_details->total_clicks).' searches'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);


			$listItemRun = $section->addListItemRun();
			$listItemRun->addText(
				htmlspecialchars('The most popular searches by bathrooms were:'),
				$style_text
			);
			if(count($click_on_hnl_search_bathrooms)){
				$phpWord->addTableStyle('searches by hnl bath', $styleTable);
				$table         = $section->addTable('searches by hnl bath');
				foreach($click_on_hnl_search_bathrooms as $index => $item_details){
					$table->addRow();	
					$table->addCell(800)->addText($index+1, $tables_style, $cellHCentered);
					$table->addCell(3500)->addText(htmlspecialchars($item_details->label_name.' bathrooms'), $tables_style, $cellHCentered);
					$table->addCell(2000)->addText(htmlspecialchars(number_format($item_details->total_clicks).' searches'), $tables_style, $cellHCentered);
				}
			}
			else{
				$section->addText(htmlspecialchars('No results found.'),$style_error);
			}
			$section->addTextBreak(1);

			// Saving the document as OOXML file...
			$file_name    = 'Statistics_report_dev'.$development_id.'_'.date('Ymd', strtotime($start_date)).'_'.date('Ymd', strtotime($end_date)).'_'.time().'.docx';
			$new_file_url = base_url('../mpvs/templates/word_reports/'.$file_name);
			$objWriter    = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
			$objWriter->save(BASEPATH.'../../mpvs/templates/word_reports/'.$file_name);
		}

		$data = array(
			'development'        => $development,
			'start_date'         => $start_date,
			'end_date'           => $end_date,
			'new_file_url'       => $new_file_url
		);
		$this->load_template('admin/reports/view_statisticswordreport', $data);
	}

}
?>