<?php
if( !defined('BASEPATH')){exit('No direct script access allowed');}

class AdminMain_Controller extends CI_Controller {

	/**
	 * This function will load the header, content layout and footer
	 * the controller can call load_template and provide the view and data, if the title and footer are different they can be specified
	 * @param string $layout_content
	 * @param array $data
	 * @param array $footer
	 * @param string $title
	 */
	function load_template($layout_content, $data = array(), $footer = array("admin/footer_standard"), $title = 'Mapovis - Administrator')
	{
		$this->load->model('Model_user', '', TRUE);
		$section_segment                      = $this->uri->segment(2);
		$current_admin                        = $this->Model_user->getAdminLoggedIn();
		$warningmessage                       = $this->session->flashdata('warningmessage');
		$successmessage                       = $this->session->flashdata('alertmessage');
		$errormessage                         = $this->session->flashdata('errormessage');
		$success_message                      = (!empty($successmessage))? '<div class="alert alert-success">'.$successmessage.'</div>': '';
		$error_message                        = (!empty($errormessage))? '<div class="alert alert-danger">'.$errormessage.'</div>': '';
		$warning_message                      = (!empty($warningmessage))? '<div class="alert alert-warning">'.$warningmessage.'</div>': '';
		$validation_errors                    = validation_errors();
		$validation_msg                       = (!empty($validation_errors))? '<div class="alert alert-danger">'.$validation_errors.'</div>': '';

		$data['title']                        = $title;
		$data['layout_content']               = ($layout_content)? $layout_content: 'admin/dashboard/view_dashboard';
		$data['footer_custom_content_chunks'] = $footer;
		$data['alert_message']                = "{$success_message} {$error_message} {$warning_message}";
		$data['section_segment']              = $section_segment;
		$data['default_pagination']           = $current_admin->default_pagination;
		$data['validation_msg']               = $validation_msg;
		$this->load->view('admin/admintemplate', $data);
	}

	/**
	 * This function will load the content, user for ajax calls
	 * the controller can call load_template and provide the view and data
	 * @param string $layout_content
	 * @param array $data
	 */
	function load_ajaxtemplate($layout_content, $data = array())
	{
		$warningmessage        = $this->session->flashdata('warningmessage');
		$successmessage        = $this->session->flashdata('alertmessage');
		$errormessage          = $this->session->flashdata('errormessage');
		$success_message       = (!empty($successmessage))? '<div class="alert alert-success">'.$successmessage.'</div>': '';
		$error_message         = (!empty($errormessage))? '<div class="alert alert-danger">'.$errormessage.'</div>': '';
		$warning_message       = (!empty($warningmessage))? '<div class="alert alert-warning">'.$warningmessage.'</div>': '';

		$data['alert_message'] = "{$success_message} {$error_message} {$warning_message}";
		$this->load->view($layout_content, $data);
	}
}
?>