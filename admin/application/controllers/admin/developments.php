<?php
if( !defined('BASEPATH')) exit('No direct script access allowed');
require_once('adminmain_controller.php');

class Developments extends AdminMain_Controller{

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in_id'))
		{
			redirect('admin/login');
			exit();
		}
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_developer', '', TRUE);
	}

	public function managedevelopments()
	{
		$developments = $this->Model_development->getDevelopments();
		$developers   = $this->Model_developer->getDevelopers(TRUE);
		$data         = array(
			'developments' => $developments,
			'developers'   => $developers
		);
		$this->load_template('admin/developments/view_managedevelopments', $data);
	}

	public function managedevelopmentid($development_id)
	{
		$this->load->model('Model_development_link', '', TRUE);
		$development = $this->getDevelopmentValidate($development_id);
		$data        = array(
			'development'    => $development,
			'possible_links' => $this->Model_development_link->getPossibleDevelopments($development_id),
		);
		$this->load_template('admin/developments/view_managedevelopmentid', $data);
	}

	public function generalsettings($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->library('mapovis_lib');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('development_name', 'Name of Development', 'trim|required|xss_clean');

		if($this->form_validation->run() != FALSE)
		{
			//Field validation success, update development
			$form_data = $this->input->post();
			if($this->Model_development->updateDevelopment($development_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The development has been updated successfully.');
				//Go to Manage area
				redirect('admin/developments/managedevelopmentid/'.$development_id);
			}
		}

		$states      = $this->Model_development->getStates();
		$developers  = $this->Model_developer->getDevelopers(TRUE);
		$data        = array(
			'development'             => $development,
			'states'                  => $states,
			'developers'              => $developers,
			'house_match_sort_option' => $this->Model_development->houseMatchSortByOptions(),
			'house_land_order_bys'    => $this->Model_development->getHouseLandOrderBy(),
			'house_and_land_views'    => $this->mapovis_lib->getHouseAndLandViews(),
			'land_views'              => $this->mapovis_lib->getLandViews(),
			'template_views'          => $this->mapovis_lib->getTemplateViews(),
			'land_pdf_views'          => $this->mapovis_lib->getLandPdfViews(),
		);
		$this->load_template('admin/developments/view_generalsettings', $data);
	}

	public function duplicatedevelopment($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->library('mapovis_lib');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('development_name', 'Name of Development', 'trim|required|xss_clean');

		if($this->form_validation->run() != FALSE)
		{
			//Field validation success, update development
			$development_name   = $this->input->post('development_name');
			$new_development_id = $this->Model_development->duplicateDevelopment($development_id, $development_name);
			if($new_development_id){
				$this->session->set_flashdata('alertmessage', 'The development has been duplicated successfully.');
				//Go to Manage area
				redirect('admin/developments/managedevelopmentid/'.$new_development_id);
			}
		}
		$data = array(
			'development' => $development,
		);
		$this->load_template('admin/developments/view_duplicatedevelopment', $data);
	}

	public function deletedevelopment($development_id)
	{
		if($this->Model_development->deleteDevelopment($development_id)){
			$this->session->set_flashdata('alertmessage', 'The development has been deleted successfully.');
			$url = 'admin/developments/managedevelopments';
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the development.');
			$url = 'admin/developments/managedevelopmentid/'.$development_id;
		}
		//Go to Manage developments
		redirect($url);
	}

	function viewprecincts($development_id)
	{
		$this->load->model('Model_precinct', '', TRUE);

		$development = $this->getDevelopmentValidate($development_id);
		$precincts   = $this->Model_precinct->getPrecincts($development_id);
		$data        = array(
			'precincts'   => $precincts,
			'development' => $development
		);
		$this->load_template('admin/developments/view_viewprecincts', $data);
	}

	function deleteprecinct($precinct_id)
	{
		$this->load->model('Model_precinct', '', TRUE);
		$precinct = $this->Model_precinct->getPrecinct($precinct_id);
		if($this->Model_precinct->deletePrecinct($precinct_id)){
			$this->session->set_flashdata('alertmessage', 'The precinct has been deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the precinct.');
		}
		//Go to Manage precincts
		redirect('admin/developments/viewprecincts/'.$precinct->development_id);
	}

	function addprecincts($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_precinct', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('precinct_number', 'Precinct Number', 'trim|required|integer|callback_is_unique_precinct');

		if($this->form_validation->run() != FALSE)
		{
			$precinct_number = $this->input->post('precinct_number');
			//Field validation success, add precinct
			if($this->Model_precinct->addPrecinct($development_id, $precinct_number)){
				$this->session->set_flashdata('alertmessage', 'The precinct was added successfully.');
				redirect('admin/developments/viewprecincts/'.$development_id);
			}
		}
		$data        = array('development' => $development);
		$this->load_template('admin/developments/view_addprecincts', $data);
	}

	function viewstages($development_id)
	{
		$this->load->model('Model_stage', '', TRUE);

		$development = $this->getDevelopmentValidate($development_id);
		$stages      = $this->Model_stage->getStages($development_id);
		$data        = array(
			'stages'      => $stages,
			'development' => $development
		);
		$this->load_template('admin/developments/view_viewstages', $data);
	}

	function addstages($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_precinct', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('stage_number', 'Stage Number', 'trim|required|integer|callback_is_unique_stage');

		if($this->form_validation->run() != FALSE)
		{
			$form_data = $this->input->post();
			$precinct_id  = $this->input->post('precinct_id');

			//Field validation success, add precinct
			if($this->Model_stage->addStage($precinct_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The stage was added successfully.');
				redirect('admin/developments/viewstages/'.$development_id);
			}
		}
		$precincts   = $this->Model_precinct->getPrecincts($development_id);

		$data        = array('development' => $development, 'precincts' => $precincts);
		$this->load_template('admin/developments/view_addstages', $data);
	}

	function updatestage($stage_id)
	{
		$this->load->model('Model_precinct', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);
		$this->load->library('form_validation');

		$stage       = $this->Model_stage->getStage($stage_id);
		$development = $this->getDevelopmentValidate($stage->development_id);
		$form_data   = $this->input->post();
		if($form_data)
		{
			//Field validation success, add precinct
			if($this->Model_stage->updateStage($stage_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The stage was updated successfully.');
				redirect('admin/developments/viewstages/'.$stage->development_id);
			}
		}
		$data        = array('development' => $development, 'stage' => $stage);
		$this->load_template('admin/developments/view_updatestage', $data);
	}

	function deletestage($stage_id)
	{
		$this->load->model('Model_stage', '', TRUE);
		$stage = $this->Model_stage->getStage($stage_id);
		if($this->Model_stage->deleteStage($stage_id)){
			$this->session->set_flashdata('alertmessage', 'The stage has been deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the stage.');
		}
		//Go to Manage developments
		redirect('admin/developments/viewstages/'.$stage->development_id);
	}

	function managestagetitles($stage_id)
	{
		$this->load->model('Model_stage', '', TRUE);
		$stage       = $this->Model_stage->getStage($stage_id);
		$data         = array(
			'stage' => $stage,
		);
		$this->load_ajaxtemplate('admin/developments/view_managestagetitles', $data);
	}

	function updatestagetitles($stage_id, $titled)
	{
		$this->load->model('Model_stage', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);
		$stage       = $this->Model_stage->getStage($stage_id);

		if($this->Model_lot->updateLotTitleByStage($stage_id, $titled)){
			$this->session->set_flashdata('alertmessage', 'The Stage lots have been updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to udpate the staghe lots.');
		}
		redirect('admin/developments/viewstages/'.$stage->development_id);
	}

	function viewlots($development_id)
	{
		$this->load->model('Model_lot', '', TRUE);

		$development = $this->getDevelopmentValidate($development_id);
		$lots        = $this->Model_lot->getLots($development_id);
		$lot_status  = $this->Model_lot->getStatus();
		$data        = array(
			'lots'        => $lots,
			'lot_status'  => $lot_status,
			'development' => $development
		);
		$footer = array('admin/footer_standard', 'admin/developments/view_viewlots_customjs');
		$this->load_template('admin/developments/view_viewlots', $data, $footer);
	}

	function viewlot($lot_id)
	{
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_street_name', '', TRUE);

		$lot          = $this->Model_lot->getLot($lot_id);
		$lot_status   = $this->Model_lot->getStatus();
		$street_names = $this->Model_street_name->getStreetNames();
		$data         = array(
			'lot'          => $lot,
			'lot_status'   => $lot_status,
			'street_names' => $street_names
		);
		$this->load_ajaxtemplate('admin/developments/view_viewlot', $data);
	}

	function addlots($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_precinct', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('precinct_id', 'Precinct Number', 'required|integer');

		if($this->form_validation->run() != FALSE)
		{
			//Field validation success
			$precinct_id = $this->input->post('precinct_id');
			redirect("admin/developments/addlotswhichstage/{$development_id}/{$precinct_id}");
		}

		$precincts   = $this->Model_precinct->getPrecincts($development_id);
		$data        = array(
			'precincts'   => $precincts,
			'development' => $development
		);
		$this->load_template('admin/developments/view_addlots', $data);
	}

	function addlotswhichstage($development_id, $precinct_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_precinct', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('stage_id', 'Stage Number', 'required|integer');

		// skip validation when it comes from addnewlot
		if($this->form_validation->run() != FALSE)
		{
			$stage_id     = $this->input->post('stage_id');
			redirect("admin/developments/addnewlot/{$development_id}/{$precinct_id}/{$stage_id}");
		}

		$precinct    = $this->Model_precinct->getPrecinct($precinct_id);
		$stages      = $this->Model_stage->getStages($development_id, $precinct_id);
		$data        = array('development' => $development, 'precinct' => $precinct, 'stages' => $stages);
		$this->load_template('admin/developments/view_addlots_whichstage', $data);
	}

	function addnewlot($development_id, $precinct_id, $stage_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_precinct', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);
		$this->load->model('Model_street_name', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_house', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('lot_number', 'Lot Number', 'trim|required|xss_clean|callback_is_unique_lot');

		if($this->form_validation->run() != FALSE)
		{
			//Field validation success
			$form_data    = $this->input->post();
			$redirecto_to = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? "addnewlot/{$development_id}/{$precinct_id}/{$stage_id}": 'viewlots/'.$development_id;
			$this->Model_lot->addLot($form_data);
			$this->session->set_flashdata('alertmessage', 'The lot was added successfully.');
			redirect('admin/developments/'.$redirecto_to);
		}

		$precinct     = $this->Model_precinct->getPrecinct($precinct_id);
		$stage        = $this->Model_stage->getStage($stage_id);
		$street_names = $this->Model_street_name->getStreetNames();
		$lot_status   = $this->Model_lot->getStatus();
		$data         = array(
			'development'  => $development,
			'precinct'     => $precinct,
			'stage'        => $stage,
			'street_names' => $street_names,
			'lot_status'   => $lot_status,
		);
		$footer = array('admin/footer_standard', 'admin/developments/view_addnewlot_customjs', 'admin/developments/view_actioncontinue_customjs');
		$this->load_template('admin/developments/view_addnewlot', $data, $footer);
	}

	function addstreetname($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_street_name', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('street_name', 'StreetName', 'trim|required|xss_clean');
		$form_data  = $this->input->post();

		if($this->form_validation->run() != FALSE)
		{
			$form_data  = $this->input->post();
			//Field validation success, add precinct
			$street_name_id = $this->Model_street_name->addStreetName($form_data);
			if($street_name_id){
				$street_name = $this->Model_street_name->getStreetName($street_name_id);
				$result      = array('status' => 1, 'msg'=> 'The Street was added successfully.', 'street_name_id' => $street_name_id, 'street_name' => $street_name->street_name);
			}
			else{
				$result = array('status' => 0, 'msg'=> 'It was not possible to add the Street please try again.');
			}
		}
		else{
			$result = array('status' => 0, 'msg'=> 'Please provide a valid builder name and website.');
		}
		echo json_encode($result);
		exit();
	}

	function updatelot($lot_id)
	{
		$this->load->model('Model_lot', '', TRUE);
		$lot       = $this->Model_lot->getLot($lot_id);
		$form_data = $this->input->post();
		if($this->Model_lot->updateLot($lot_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The lot was updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the lot, please make sure the data is correct.');
		}
		redirect('admin/developments/viewlots/'.$lot->development_id);
	}

	function deletelot($lot_id)
	{
		$this->load->model('Model_lot', '', TRUE);
		$lot = $this->Model_lot->getLot($lot_id);
		if($this->Model_lot->deleteLot($lot_id)){
			$this->session->set_flashdata('alertmessage', 'The lot has been deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the lot.');
		}
		//Go to Manage lots
		redirect('admin/developments/viewlots/'.$lot->development_id);
	}

	function addhouses($development_id, $builder_id = NULL)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_builder', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);
		$this->load->model('Model_house', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('house_name', 'House Name', 'trim|required|xss_clean');

		if($this->form_validation->run() != FALSE)
		{
			$form_data                   = $this->input->post();
			$form_data['development_id'] = $development_id;
			$redirecto_to                = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? 'addhouses/'.$development_id: 'viewhouses/'.$development_id;

			//Field validation success, add precinct
			if($this->Model_house->addHouse($form_data)){
				$this->session->set_flashdata('alertmessage', 'The house was added successfully.');
				redirect('admin/developments/'.$redirecto_to);
			}
		}
		$builders    = $this->Model_builder->getBuilders();
		$data        = array('development' => $development, 'builders' => $builders, 'builder_id' => $builder_id);

		$footer      = array('admin/footer_standard', 'admin/developments/view_houses_customjs');
		$this->load_template('admin/developments/view_addhouses', $data, $footer);
	}

	function validatehousename($development_id = NULL)
	{
		$this->load->model('Model_house', '', TRUE);
		$house_name = $this->input->post('house_name');
		$house_id   = $this->input->post('house_id');
		$builder_id = $this->input->post('builder_id');
		$housematch = $this->Model_house->validateDuplicate($house_name, $builder_id, $development_id, $house_id);
		if(!$housematch){
			$result = true;
		}
		else{
			if($housematch->development_id == null){
				$result = 'A house with this name already exists in the MAPOVIS directory.';
			}
			elseif($housematch->development_id == $development_id){
				$result = 'A custom house with this name is already linked to this development.';
			}
			else{
				$result = 'A custom house with this name is already linked to a development.';
			}
		}
		echo json_encode($result);
		exit();
	}

	function uploadoriginalimage()
	{
		$this->load->model('Model_house_image', '', TRUE);
		$result = $this->Model_house_image->uploadoriginalimage();
		echo json_encode($result);
		exit();
	}

	function addbuilder()
	{
		$this->load->model('Model_builder', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('builder_name', 'Builder Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('builder_website', 'Builder Website', 'trim|required');
		$form_data  = $this->input->post();

		if($this->form_validation->run() != FALSE)
		{
			$form_data  = $this->input->post();
			//Field validation success, add builder
			if($this->Model_builder->duplicateName($form_data['builder_name'], null) == FALSE){
				$builder_id = $this->Model_builder->addBuilder($form_data);
				if($builder_id){
					$builder = $this->Model_builder->getBuilder($builder_id);
					$result = array('status' => 1, 'msg'=> 'The builder was added successfully.', 'builder_id' => $builder_id, 'builder_name' => $builder->builder_name);
				}
				else{
					$result = array('status' => 0, 'msg'=> 'It was not possible to add the builder please try again.');
				}
			}
			else{
				$result = array('status' => 0, 'msg'=> 'The builder name already exists in the system.');
			}
		}
		else{
			$result = array('status' => 0, 'msg'=> 'Please provide a valid builder name and website.');
		}
		echo json_encode($result);
		exit();
	}

	function viewhouses($development_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_builder', '', TRUE);

		$development = $this->getDevelopmentValidate($development_id);
		$houses      = $this->Model_house->getHouses($development_id);
		$builders    = $this->Model_builder->getBuilders();
		$data        = array(
			'houses'      => $houses,
			'builders'    => $builders,
			'development' => $development
		);
		$footer = array('admin/footer_standard', 'admin/developments/view_houses_customjs', 'admin/developments/view_viewhouses_customjs');
		$this->load_template('admin/developments/view_viewhouses', $data, $footer);
	}

	function viewhouse($house_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_builder', '', TRUE);
		$this->load->model('Model_house_image', '', TRUE);

		$house    = $this->Model_house->getHouse($house_id);
		$builders = $this->Model_builder->getBuilders();
		$data     = array(
			'house'        => $house,
			'builders'     => $builders,
			'house_images' => $this->Model_house_image->getHouseImages($house_id, array('file_type' => 'facade')),
			'floor_plans'  => $this->Model_house_image->getHouseImages($house_id, array('file_type' => 'floor_plan')),
			'gallery'      => $this->Model_house_image->getHouseImages($house_id, array('file_type' => 'gallery')),
		);
		$this->load_ajaxtemplate('admin/developments/view_viewhouse', $data);
	}

	function updatehouse($house_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$form_data = $this->input->post();
		if($this->Model_house->updateHouse($house_id, $form_data)){
			$house  = $this->Model_house->getHouse($house_id);
			$result = array('status' => 1, 'msg'=> 'The house was updated successfully','obj' => $house);
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to update the house, please make sure the data is correct.', 'obj' => FALSE);
		}
		echo json_encode($result);
		exit();
	}

	function deletehouseimage()
	{
		$this->load->model('Model_house_image', '', TRUE);
		$house_image_id   = $this->input->post('house_image_id');

		if($this->Model_house_image->deleteHouseImage($house_image_id)){
			$result = array('status' => 1, 'msg'=> 'The image was deleted successfully.');
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to delete the image, please try again.');
		}
		echo json_encode($result);
		exit();
	}

	function deletehouse($house_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$house = $this->Model_house->getHouse($house_id);

		if($this->Model_house->deleteHouse($house_id)){
			$this->session->set_flashdata('alertmessage', 'The house was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the house.');
		}
		redirect('admin/developments/viewhouses/'.$house->development_id);
	}

	function managehousematches($development_id)
	{
		$this->load->model('Model_lot', '', TRUE);
		$data        = array(
			'development' => $this->getDevelopmentValidate($development_id),
			'lots'        => $this->Model_lot->getLotsCountMatches($development_id),
		);
		$this->load_template('admin/developments/view_managehousematches', $data);
	}

	function editlothouses($lot_id)
	{
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_house_lot_package', '', TRUE);

		$lot           = $this->Model_lot->getLot($lot_id);
		$development   = $this->getDevelopmentValidate($lot->development_id);
		$data          = array(
			'development'        => $development,
			'lot'                => $lot,
			'houses'             => $this->Model_house->getHouses($development->development_id, TRUE),
			'lot_house_packages' => $this->Model_house_lot_package->getHouseLotPackages($lot_id)
		);
		$footer = array('admin/footer_standard', 'admin/developments/view_editlothouses_customjs');
		$this->load_template('admin/developments/view_editlothouses', $data, $footer);
	}

	function edithouselotpackage($lot_id, $house_id, $ajax = TRUE)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_builder_sales_person', '', TRUE);
		$this->load->model('Model_house_lot_order_image', '', TRUE);

		$lot               = $this->Model_lot->getLot($lot_id);
		$house             = $this->Model_house->getHouse($house_id);
		$house_lot_package = $this->Model_house_lot_package->getHouseLotPackage($lot_id, $house_id);
		$sales_people      = $this->Model_builder_sales_person->getSalesPeople($house->house_builder_id);
		$data              = array(
			'house'             => $house,
			'lot'               => $lot,
			'house_lot_package' => $house_lot_package,
			'sales_people'      => $sales_people,
			'house_images'      => $this->Model_house_lot_order_image->getHouseLotImages($house_id, $lot_id),
			'ajax'              => $ajax,
		);
		$this->load_ajaxtemplate('admin/developments/view_edithouselotpackage', $data);
	}

	function updatehouselotpackage($lot_id, $house_id, $ajax = TRUE)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);
		$this->load->model('Model_house_lot_order_image', '', TRUE);
		$this->load->model('Model_user', '', TRUE);

		$form_data     = $this->input->post();
		$current_admin = $this->Model_user->getAdminLoggedIn();
		if($this->Model_house_lot_package->updateHouseLot($lot_id, $house_id, $form_data, $current_admin)){
			if(isset($form_data['facade_images'])){
				$this->Model_house_lot_order_image->updatePackageImagesOrder($house_id, $lot_id, $form_data);
			}
			$house_lot_package                           = $this->Model_house_lot_package->getHouseLotPackage($lot_id, $house_id);
			$house_lot_package->formated_house_lot_price = number_format($house_lot_package->house_lot_price);
			$result                                      = array('status' => 1, 'msg'=> 'The House and Land package has been udpated.','obj' => $house_lot_package);
		}
		else{
			$result            = array('status' => 0, 'msg'=> 'It was not possible to update the House and Land package. Please check the details are correct.', 'obj' => FALSE);
		}
		if($ajax){
			echo json_encode($result);
			exit();
		}
		else{
			$this->load->model('Model_lot', '', TRUE);
			$lot          = $this->Model_lot->getLot($lot_id);
			$message_type = ($result['status'] == 0)? 'errormessage': 'alertmessage';
			$this->session->set_flashdata($message_type, $result['msg']);
			redirect('admin/developments/listpackages/'.$lot->development_id);
		}
	}

	function deletehouselotpdf($lot_id, $house_id)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);
		$this->load->model('Model_user', '', TRUE);

		$current_admin = $this->Model_user->getAdminLoggedIn();
		if($this->Model_house_lot_package->deleteHouseLotPdf($lot_id, $house_id, $current_admin)){
			$house_lot_package                           = $this->Model_house_lot_package->getHouseLotPackage($lot_id, $house_id);
			$house_lot_package->formated_house_lot_price = number_format($house_lot_package->house_lot_price);
			$result = array('status' => 1, 'msg'=> 'The House and Land PDF file has been DELETED.', 'obj' => $house_lot_package);
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to delete PDF File.', 'obj' => FALSE);
		}
		echo json_encode($result);
		exit();
	}

	function enablehouselotpackage($lot_id, $house_id)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);
		$this->load->model('Model_user', '', TRUE);

		$current_admin = $this->Model_user->getAdminLoggedIn();
		if($this->Model_house_lot_package->enableDisableHouseLotPdf($lot_id, $house_id, TRUE, $current_admin)){
			$house_lot_package                           = $this->Model_house_lot_package->getHouseLotPackage($lot_id, $house_id);
			$house_lot_package->formated_house_lot_price = number_format($house_lot_package->house_lot_price);
			$result = array('status' => 1, 'msg'=> 'The House has been enabled.', 'obj' => $house_lot_package);
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to enable the house.', 'obj' => FALSE);
		}
		echo json_encode($result);
		exit();
	}

	function disablehouselotpackage($lot_id, $house_id, $ajax = TRUE)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);

		$this->load->model('Model_user', '', TRUE);

		$current_admin = $this->Model_user->getAdminLoggedIn();
		if($this->Model_house_lot_package->enableDisableHouseLotPdf($lot_id, $house_id, FALSE, $current_admin)){
			$house_lot_package                           = $this->Model_house_lot_package->getHouseLotPackage($lot_id, $house_id);
			$house_lot_package->formated_house_lot_price = number_format($house_lot_package->house_lot_price);
			$result = array('status' => 1, 'msg'=> 'The House has been disabled.', 'obj' => $house_lot_package);
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to disable the house.', 'obj' => FALSE);
		}
		if($ajax){
			echo json_encode($result);
			exit();
		}
		else{
			$this->load->model('Model_lot', '', TRUE);
			$lot          = $this->Model_lot->getLot($lot_id);
			$message_type = ($result['status'] == 0)? 'errormessage': 'alertmessage';
			$this->session->set_flashdata($message_type, $result['msg']);
			redirect('admin/developments/listpackages/'.$lot->development_id);
		}
	}

	function listpackages($development_id)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);

		$development   = $this->getDevelopmentValidate($development_id);
		$data          = array(
			'development'        => $development,
			'lot_house_packages' => $this->Model_house_lot_package->getHouseLotPackagesByDevelopment($development_id)
		);
		$this->load_template('admin/developments/view_listlothouses', $data);
	}

	function addamenities($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_amenity', '', TRUE);
		$this->load->model('Model_amenity_type', '', TRUE);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('amenity_name', 'Amenity Name', 'trim|required|xss_clean');
		if($this->form_validation->run() != FALSE)
		{
			//Field validation success
			$form_data                    = $this->input->post();
			$form_data['development_id']  = $development_id;
			$redirecto_to                 = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? "addamenities/{$development_id}": 'viewamenities/'.$development_id;
			$this->Model_amenity->addAmenity($form_data);
			$this->session->set_flashdata('alertmessage', 'The Amenity was added successfully.');
			redirect('admin/developments/'.$redirecto_to);
		}

		$amenity_types = $this->Model_amenity_type->getAmenityTypes();
		$data          = array(
			'amenity_types' => $amenity_types,
			'development'   => $development
		);
		$footer        = array('admin/footer_standard', 'admin/developments/view_amenityimages_customjs', 'admin/developments/view_addamenities_customjs', 'admin/developments/view_actioncontinue_customjs');
		$this->load_template('admin/developments/view_addamenities', $data, $footer);
	}

	function viewamenities($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_amenity', '', TRUE);
		$amenities   = $this->Model_amenity->getAmenities($development_id);
		$data        = array(
			'amenities'   => $amenities,
			'development' => $development
		);
		$footer = array('admin/footer_standard', 'admin/developments/view_amenityimages_customjs', 'admin/developments/view_viewamenities_customjs');
		$this->load_template('admin/developments/view_viewamenities', $data, $footer);
	}

	function viewamenity($amenity_id)
	{
		$this->load->model('Model_amenity', '', TRUE);
		$this->load->model('Model_amenity_type', '', TRUE);
		$amenity       = $this->Model_amenity->getAmenity($amenity_id);
		$amenity_types = $this->Model_amenity_type->getAmenityTypes();
		$data          = array(
			'amenity'       => $amenity,
			'amenity_types' => $amenity_types,
		);
		$this->load_ajaxtemplate('admin/developments/view_viewamenity', $data);
	}

	function updateamenity($amenity_id)
	{
		$this->load->model('Model_amenity', '', TRUE);
		$amenity   = $this->Model_amenity->getAmenity($amenity_id);

		$form_data = $this->input->post();
		if($this->Model_amenity->updateAmenity($amenity_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The amenity was updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the amenity, please make sure the data is correct.');
		}
		redirect('admin/developments/viewamenities/'.$amenity->development_id);
	}

	function deleteamenityimage()
	{
		$this->load->model('Model_amenity', '', TRUE);
		$amenity_id = $this->input->post('amenityid');
		$image_name = $this->input->post('imagename');

		if($this->Model_amenity->deleteAmenityImage($amenity_id, $image_name)){
			$result = array('status' => 1, 'msg'=> 'The image was deleted successfully.');
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to delete the image, please try again.');
		}
		echo json_encode($result);
		exit();
	}

	function deleteamenity($amenity_id)
	{
		$this->load->model('Model_amenity', '', TRUE);
		$amenity   = $this->Model_amenity->getAmenity($amenity_id);

		if($this->Model_amenity->deleteAmenity($amenity_id)){
			$this->session->set_flashdata('alertmessage', 'The amenity was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the amenity.');
		}
		redirect('admin/developments/viewamenities/'.$amenity->development_id);
	}

	function addexternalamenities($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_external_amenity', '', TRUE);
		$this->load->model('Model_external_amenity_type', '', TRUE);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('e_amenity_name', 'Amenity Name', 'trim|required|xss_clean');
		if($this->form_validation->run() != FALSE)
		{
			//Field validation success
			$form_data                    = $this->input->post();
			$form_data['development_id']  = $development_id;
			$redirecto_to                 = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? "addexternalamenities/{$development_id}": 'viewexternalamenities/'.$development_id;
			$this->Model_external_amenity->addAmenity($form_data);
			$this->session->set_flashdata('alertmessage', 'The External Amenity was added successfully.');
			redirect('admin/developments/'.$redirecto_to);
		}

		$amenity_types = $this->Model_external_amenity_type->getExternalAmenityTypes($development_id);
		$data          = array(
			'amenity_types' => $amenity_types,
			'development'   => $development
		);
		$footer        = array('admin/footer_standard', 'admin/developments/view_externalamenityimages_customjs', 'admin/developments/view_addexternalamenities_customjs', 'admin/developments/view_actioncontinue_customjs');
		$this->load_template('admin/developments/view_addexternalamenities', $data, $footer);
	}

	function viewexternalamenities($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_external_amenity', '', TRUE);
		$e_amenities = $this->Model_external_amenity->getAmenities($development_id);
		$data        = array(
			'external_amenities' => $e_amenities,
			'development'        => $development
		);
		$footer = array('admin/footer_standard', 'admin/developments/view_externalamenityimages_customjs', 'admin/developments/view_viewexternalamenities_customjs');
		$this->load_template('admin/developments/view_viewexternalamenities', $data, $footer);
	}

	function viewexternalamenity($external_amenity_id)
	{
		$this->load->model('Model_external_amenity', '', TRUE);
		$this->load->model('Model_external_amenity_type', '', TRUE);
		$e_amenity     = $this->Model_external_amenity->getAmenity($external_amenity_id);
		$amenity_types = $this->Model_external_amenity_type->getExternalAmenityTypes($e_amenity->development_id);
		$data          = array(
			'external_amenity' => $e_amenity,
			'amenity_types'    => $amenity_types,
		);
		$this->load_ajaxtemplate('admin/developments/view_viewexternalamenity', $data);
	}

	function updateexternalamenity($external_amenity_id)
	{
		$this->load->model('Model_external_amenity', '', TRUE);
		$external_amenity   = $this->Model_external_amenity->getAmenity($external_amenity_id);

		$form_data = $this->input->post();
		if($this->Model_external_amenity->updateAmenity($external_amenity_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The external amenity was updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the externalamenity, please make sure the data is correct.');
		}
		redirect('admin/developments/viewexternalamenities/'.$external_amenity->development_id);
	}

	function deleteexternalamenityimage()
	{
		$this->load->model('Model_external_amenity', '', TRUE);
		$amenity_id = $this->input->post('amenityid');
		$image_name = $this->input->post('imagename');

		if($this->Model_external_amenity->deleteAmenityImage($amenity_id, $image_name)){
			$result = array('status' => 1, 'msg'=> 'The image was deleted successfully.');
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to delete the image, please try again.');
		}
		echo json_encode($result);
		exit();
	}

	function deleteexternalamenity($amenity_id)
	{
		$this->load->model('Model_external_amenity', '', TRUE);
		$amenity   = $this->Model_external_amenity->getAmenity($amenity_id);

		if($this->Model_external_amenity->deleteAmenity($amenity_id)){
			$this->session->set_flashdata('alertmessage', 'The amenity was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the amenity.');
		}
		redirect('admin/developments/viewexternalamenities/'.$amenity->development_id);
	}

	function manageexternalamenitytypes($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_external_amenity_type', '', TRUE);
		$e_amenity_types = $this->Model_external_amenity_type->getExternalAmenityTypes($development_id);
		$data            = array(
			'e_amenity_types' => $e_amenity_types,
			'development'     => $development
		);
		$footer = array('admin/footer_standard', 'admin/developments/view_externalamenitytypes_customjs');
			$this->load_template('admin/developments/view_viewexternalamenitytypes', $data, $footer);
	}

	function viewexternalamenitytype($external_amenity_type_id)
	{
		$this->load->model('Model_external_amenity_type', '', TRUE);
		$amenity_type = $this->Model_external_amenity_type->getExternalAmenityType($external_amenity_type_id);
		$data         = array(
			'amenity_type' => $amenity_type,
		);
		$this->load_ajaxtemplate('admin/developments/view_viewexternalamenitytype', $data);
	}

	function updateexternalamenitytype($external_amenity_type_id)
	{
		$this->load->model('Model_external_amenity_type', '', TRUE);
		$external_amenity_type = $this->Model_external_amenity_type->getExternalAmenityType($external_amenity_type_id);
		$form_data             = $this->input->post();
		if($external_amenity_type && $this->Model_external_amenity_type->updateExternalAmenityType($external_amenity_type_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The external amenity type was updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the external amenity type, please make sure the data is correct.');
		}
		redirect('admin/developments/manageexternalamenitytypes/'.$external_amenity_type->development_id);
	}

	function addexternalamenitytype($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_external_amenity_type', '', TRUE);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('e_amenity_type_name', 'Name', 'trim|required|xss_clean');
		if($this->form_validation->run() != FALSE)
		{
			//Field validation success
			$form_data                    = $this->input->post();
			$redirecto_to                 = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? "addexternalamenitytype/{$development_id}": 'manageexternalamenitytypes/'.$development_id;
			$this->Model_external_amenity_type->addExternalAmenityType($development_id, $form_data);
			$this->session->set_flashdata('alertmessage', 'The External Amenity Type was added successfully.');
			redirect('admin/developments/'.$redirecto_to);
		}
		$data          = array(
			'development' => $development
		);
		$footer        = array('admin/footer_standard', 'admin/developments/view_actioncontinue_customjs');
		$this->load_template('admin/developments/view_addexternalamenitytype', $data, $footer);
	}

	function deleteexternalamenitytype($external_amenity_type_id)
	{
		$this->load->model('Model_external_amenity_type', '', TRUE);
		$amenity   = $this->Model_external_amenity_type->getExternalAmenityType($external_amenity_type_id);

		if($this->Model_external_amenity_type->deleteExternalAmenityType($external_amenity_type_id)){
			$this->session->set_flashdata('alertmessage', 'The external amenity type was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the external amenity type.');
		}
		redirect('admin/developments/manageexternalamenitytypes/'.$amenity->development_id);
	}

	function importexternalamenities($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);
		$data        = array(
			'development' => $development,
		);
		$this->load_template('admin/developments/view_importexternalamenities', $data);
	}

	function exportexternalamenities($development_id)
	{
		$this->load->model('Model_import_e_amenities', '', TRUE);

		$csv_file_content = $this->Model_import_e_amenities->getCsvAmenities($development_id);
		$file_name        = 'external_amenities_dev'.$development_id.'_'.date('Ymd').'.csv';
		if($csv_file_content){
			header('Content-Type: application/csv');
			header('Content-Disposition: attachement; filename="'.$file_name.'"');
			echo $csv_file_content;
			exit();
		}
		$this->session->set_flashdata('alertmessage', 'It was not possible to generate the CSV file. Please try again.');
		redirect('admin/developments/importexternalamenities/'.$development_id);
		exit();
	}

	function importexternalamenitiesUpload($development_id)
	{
		$this->load->model('Model_import_e_amenities', '', TRUE);
		$development = $this->getDevelopmentValidate($development_id);
		$config = array(
			'upload_path'   => FCPATH.'uploads'.DIRECTORY_SEPARATOR,
			'allowed_types' => 'csv',
			'max_size'	    => '100',
			'max_width'     => '1024',
			'max_height'    => '768'
		);
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload('csvfilename')){
			$this->session->set_flashdata('errormessage', $this->upload->display_errors());
			redirect('admin/developments/importexternalamenities/'.$development_id);
		}
		$upload_data  = $this->upload->data();
		$file_id      = $this->Model_import_e_amenities->addImportFileToUpdate($development_id, $upload_data);
		$file_details = $this->Model_import_e_amenities->getImportedFile($file_id);
		$data         = array(
			'development' => $development,
			'filename'    => $file_details->filename,
			'notes'       => $file_details->import_notes,
		);
		$this->load_template('admin/developments/view_importedexternalamenities', $data);
	}

	function manageradiusmarkers($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_radius_marker', '', TRUE);
		$radius_markers = $this->Model_radius_marker->getRadiusMarkers($development_id);
		$data           = array(
			'radius_markers' => $radius_markers,
			'development'    => $development
		);
		$footer = array('admin/footer_standard', 'admin/developments/view_radiusmarkers_customjs');
			$this->load_template('admin/developments/view_viewradiusmarkers', $data, $footer);
	}
    
    function manageexternalmarkers($development_id)
    {
        $development = $this->getDevelopmentValidate($development_id);

        $this->load->model('Model_external_marker', '', TRUE);
        $external_markers = $this->Model_external_marker->getExternalMarkers($development_id);
        $data           = array(
            'external_markers' => $external_markers,
            'development'    => $development
        );
        $footer = array('admin/footer_standard', 'admin/developments/view_externalmarkers_customjs');
        $this->load_template('admin/developments/view_viewexternalmarkers', $data, $footer);
    }
    
    function managepolylines($development_id){
        $development = $this->getDevelopmentValidate($development_id);

        $this->load->model('Model_polyline', '', TRUE);
        $polylines = $this->Model_polyline->getPolylines($development_id);
        $data           = array(
            'polylines' => $polylines,
            'development'    => $development
        );    
        $footer = array('admin/footer_standard', 'admin/developments/view_polylines_customjs');
        $this->load_template('admin/developments/view_viewpolylines', $data, $footer);
    }
    
    function deletepolyline($polyline_id)
    {
        $this->load->model('Model_polyline', '', TRUE);
        $polyline   = $this->Model_polyline->getPolyline($polyline_id);

        if($this->Model_polyline->deletePolyline($polyline_id)){
            $this->session->set_flashdata('alertmessage', 'The polyline was deleted successfully.');
        }
        else{
            $this->session->set_flashdata('errormessage', 'It was not possible to delete the polyline.');
        }
        redirect('admin/developments/managepolylines/'.$polyline->developmentID);
    }
    
    function viewpolyline($polyline_id)
    {
        $this->load->model('Model_polyline', '', TRUE);
        $polyline = $this->Model_polyline->getPolyline($polyline_id);
        $data         = array(
            'polyline' => $polyline,
        );
        $this->load_ajaxtemplate('admin/developments/view_viewpolyline', $data);
    }
    

	function viewradiusmarker($radius_marker_id)
	{
		$this->load->model('Model_radius_marker', '', TRUE);
		$radius_marker = $this->Model_radius_marker->getRadiusMarker($radius_marker_id);
		$data         = array(
			'radius_marker' => $radius_marker,
		);
		$this->load_ajaxtemplate('admin/developments/view_viewradiusmarker', $data);
	}
    
    function viewexternalmarker($external_marker_id)
    {
        $this->load->model('Model_external_marker', '', TRUE);
        $external_marker = $this->Model_external_marker->getExternalMarker($external_marker_id);
        $data         = array(
            'external_marker' => $external_marker,
        );
        $this->load_ajaxtemplate('admin/developments/view_viewexternalmarker', $data);
    }

	function updateradiusmarker($radius_marker_id)
	{
		$this->load->model('Model_radius_marker', '', TRUE);
		$radius_marker = $this->Model_radius_marker->getRadiusMarker($radius_marker_id);
		$form_data     = $this->input->post();
		if($radius_marker && $this->Model_radius_marker->updateRadiusMarker($radius_marker_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The radius marker was updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the radius marker, please make sure the data is correct.');
		}
		redirect('admin/developments/manageradiusmarkers/'.$radius_marker->development_id);
	}
    
    function updatepolyline($polyline_id)
    {
        $this->load->model('Model_polyline', '', TRUE);
        $polyline = $this->Model_polyline->getPolyline($polyline_id);
        $form_data     = $this->input->post();
        if($polyline && $this->Model_polyline->updatePolyline($polyline_id, $form_data)){
            $this->session->set_flashdata('alertmessage', 'The polyline was updated successfully.');
        }
        else{
            $this->session->set_flashdata('errormessage', 'It was not possible to update the polyline, please make sure the data is correct.');
        }
        redirect('admin/developments/managepolylines/'.$polyline->developmentID);
    }
    
    function updateexternalmarker($external_marker_id)
    {
        $this->load->model('Model_external_marker', '', TRUE);
        $external_marker = $this->Model_external_marker->getExternalMarker($external_marker_id);
        $form_data     = $this->input->post();
        if($external_marker && $this->Model_external_marker->updateExternalMarker($external_marker_id, $form_data)){
            $this->session->set_flashdata('alertmessage', 'The external marker was updated successfully.');
        }
        else{
            $this->session->set_flashdata('errormessage', 'It was not possible to update the radius marker, please make sure the data is correct.');
        }
        redirect('admin/developments/manageexternalmarkers/'.$external_marker->development_id);
    }

	function addradiusmarker($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_radius_marker', '', TRUE);
		$form_data = $this->input->post();
		if($form_data != FALSE)
		{
			$redirecto_to                 = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? "addradiusmarker/{$development_id}": 'manageradiusmarkers/'.$development_id;
			$this->Model_radius_marker->addRadiusMarker($development_id, $form_data);
			$this->session->set_flashdata('alertmessage', 'The radius marker was added successfully.');
			redirect('admin/developments/'.$redirecto_to);
		}
		$data          = array(
			'development' => $development
		);
		$footer        = array('admin/footer_standard', 'admin/developments/view_actioncontinue_customjs');
		$this->load_template('admin/developments/view_addradiusmarker', $data, $footer);
	}
    
    function addexternalmarker($development_id)
    {
        $development = $this->getDevelopmentValidate($development_id);

        $this->load->model('Model_external_marker', '', TRUE);
        $form_data = $this->input->post();
        if($form_data != FALSE)
        {
            $redirecto_to                 = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? "addexternalmarker/{$development_id}": 'manageexternalmarkers/'.$development_id;
            $this->Model_external_marker->addExternalMarker($development_id, $form_data);
            $this->session->set_flashdata('alertmessage', 'The external marker was added successfully.');
            redirect('admin/developments/'.$redirecto_to);
        }
        $data          = array(
            'development' => $development
        );
        $footer        = array('admin/footer_standard', 'admin/developments/view_actioncontinue_customjs');
        $this->load_template('admin/developments/view_addexternalmarker', $data, $footer);
    }

	function deleteradiusmarker($radius_marker_id)
	{
		$this->load->model('Model_radius_marker', '', TRUE);
		$amenity   = $this->Model_radius_marker->getRadiusMarker($radius_marker_id);

		if($this->Model_radius_marker->deleteRadiusMarker($radius_marker_id)){
			$this->session->set_flashdata('alertmessage', 'The radius marker was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the radius marker.');
		}
		redirect('admin/developments/manageradiusmarkers/'.$amenity->development_id);
	}
    
    function deleteexternalmarker($external_marker_id)
    {
        $this->load->model('Model_external_marker', '', TRUE);
        $amenity   = $this->Model_external_marker->getExternalMarker($external_marker_id);

        if($this->Model_external_marker->deleteExternalMarker($external_marker_id)){
            $this->session->set_flashdata('alertmessage', 'The external marker was deleted successfully.');
        }
        else{
            $this->session->set_flashdata('errormessage', 'It was not possible to delete the external marker.');
        }
        redirect('admin/developments/manageexternalmarkers/'.$amenity->development_id);
    }

	function primaryuser($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);
		$this->load->model('Model_user', '', TRUE);

		//No validation added yet as there are not rules to apply, for now the system needs to check if the form was sent to update the records
		if($this->input->post('send_form')){
			$primary_user = $this->input->post('primary_user');
			if($this->Model_development->updatePrimaryUser($development_id, $primary_user)){
				$this->session->set_flashdata('alertmessage', 'The primary user has been added successfully.');
				//Go to Manage area
				redirect('admin/developments/managedevelopmentid/'.$development_id);
			}
		}
		$users = $this->Model_user->getUsers(array('user_role' => 'user', 'no_empty_name' => 1));
		$data  = array(
			'development' => $development,
			'users_list'  => $users
		);
		$this->load_template('admin/developments/view_primaryuser', $data);
	}

	function secondaryuser($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);
		$this->load->model('Model_user', '', TRUE);

		//No validation added yet as there are not rules to apply, for now the system needs to check if the form was sent to update the records
		if($this->input->post('send_form')){
			$secondary_user = $this->input->post('secondary_user');
			if($this->Model_development->updateSecondaryUser($development_id, $secondary_user)){
				$this->session->set_flashdata('alertmessage', 'The secondary user has been added successfully.');
				//Go to Manage area
				redirect('admin/developments/managedevelopmentid/'.$development_id);
			}
		}
		$users = $this->Model_user->getUsers(array('user_role' => 'user', 'no_empty_name' => 1));
		$data  = array(
			'development' => $development,
			'users_list'  => $users
		);
		$this->load_template('admin/developments/view_secondaryuser', $data);
	}

	function manageruser($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);
		$this->load->model('Model_user', '', TRUE);

		//No validation added yet as there are not rules to apply, for now the system needs to check if the form was sent to update the records
		if($this->input->post('send_form')){
			$manager_user = $this->input->post('manager_user');
			if($this->Model_development->updateManagerUser($development_id, $manager_user)){
				$this->session->set_flashdata('alertmessage', 'The manager user has been added successfully.');
				//Go to Manage area
				redirect('admin/developments/managedevelopmentid/'.$development_id);
			}
		}
		$users = $this->Model_user->getUsers(array('user_role' => 'user', 'no_empty_name' => 1));
		$data  = array(
			'development' => $development,
			'users_list'  => $users
		);
		$this->load_template('admin/developments/view_manageruser', $data);
	}

	function mediaagency($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);
		$this->load->model('Model_user', '', TRUE);

		//No validation added yet as there are not rules to apply, for now the system needs to check if the form was sent to update the records
		if($this->input->post('send_form')){
			$media_agency = $this->input->post('media_agency');
			if($this->Model_development->updateMediaAgency($development_id, $media_agency)){
				$this->session->set_flashdata('alertmessage', 'The media agency has been added successfully.');
				//Go to Manage area
				redirect('admin/developments/managedevelopmentid/'.$development_id);
			}
		}
		$users = $this->Model_user->getUsers(array('user_role' => 'user', 'no_empty_name' => 1));
		$data  = array(
			'development' => $development,
			'users_list'  => $users
		);
		$this->load_template('admin/developments/view_mediaagency', $data);
	}

	function devgeneralusers($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);
		$this->load->model('Model_user', '', TRUE);
		$this->load->model('Model_development_general_user', '', TRUE);

		//No validation added yet as there are not rules to apply, for now the system needs to check if the form was sent to update the records
		if($this->input->post('send_form')){
			$media_agency = $this->input->post('media_agency');
			if($this->Model_development->updateMediaAgency($development_id, $media_agency)){
				$this->session->set_flashdata('alertmessage', 'The media agency has been added successfully.');
				//Go to Manage area
				redirect('admin/developments/managedevelopmentid/'.$development_id);
			}
		}
		$users = $this->Model_user->getUsers(array('user_role' => 'user', 'no_empty_name' => 1));
		$data  = array(
			'general_users' => $this->Model_development_general_user->getDevelopmentUsers($development_id),
			'development'   => $development,
			'users_list'    => $users
		);
		$this->load_template('admin/developments/view_dev_general_users', $data);
	}

	function adddevgeneralusers($development_id, $user_id)
	{
		$this->load->model('Model_development_general_user', '', TRUE);
		$this->Model_development_general_user->addDevelopmentUser($development_id, $user_id);
		echo 'The user was added to the development.';
		exit();
	}

	function deletedevgeneralusers($development_id, $user_id)
	{
		$this->load->model('Model_development_general_user', '', TRUE);
		$this->Model_development_general_user->deleteDevelopmentUser($development_id, $user_id);
		echo 'The user was removed from the development.';
		exit();
	}

	function schedule($development_id)
	{
		$development  = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_schedule', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('development_id', 'Invalid Development', 'required|integer');

		if($this->form_validation->run() != FALSE)
		{
			$form_data = $this->input->post();
			//Field validation success, add or edit schedule
			if($this->Model_schedule->updateSchedules($development_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The schedule has been updated successfully.');
				redirect('admin/developments/schedule/'.$development_id);
			}
		}

		$schedules    = $this->Model_schedule->getSchedules($development_id);
		$days_of_week = $this->Model_schedule->getDaysOfWeek();
		$data         = array(
			'schedules'    => $schedules,
			'days_of_week' => $days_of_week,
			'development'  => $development
		);
		$this->load_template('admin/developments/view_schedule', $data);
	}

	function reminders($development_id)
	{
		$development  = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_reminder', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('level_1_a', 'Warning Level - 1 - A', 'integer');
		$this->form_validation->set_rules('level_2_b', 'Warning Level - 2 - B', 'integer');
		$this->form_validation->set_rules('level_2_c', 'Warning Level - 2 - C', 'integer');
		$this->form_validation->set_rules('level_3_d', 'Warning Level - 3 - D', 'integer');
		$this->form_validation->set_rules('level_4_e', 'Warning Level - 4 - E', 'integer');

		if($this->form_validation->run() != FALSE)
		{
			//Field validation success, add or edit reminder
			$form_data = $this->input->post();
			if($this->Model_reminder->updateReminder($development_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The reminder has been updated successfully.');
				redirect('admin/developments/managedevelopmentid/'.$development_id);
			}
		}
		$reminder     = $this->Model_reminder->getReminder($development_id);
		$data         = array(
			'development' => $development,
			'reminder'    => $reminder,
		);
		$this->load_template('admin/developments/view_reminders', $data);
	}

	function adddevelopment()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('development_name', 'Development Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('development_uri', '', '');
		$this->form_validation->set_rules('developerid', '', '');
		$this->form_validation->set_rules('state', '', '');
		$this->form_validation->set_rules('overlay_filename', '', '');
		$this->form_validation->set_rules('southwest_latitude', '', '');
		$this->form_validation->set_rules('southwest_longitude', '', '');
		$this->form_validation->set_rules('northeast_latitude', '', '');
		$this->form_validation->set_rules('northeast_longitude', '', '');
		$this->form_validation->set_rules('centre_latitude', '', '');
		$this->form_validation->set_rules('centre_longitude', '', '');
		$this->form_validation->set_rules('entryexit_latitude', '', '');
		$this->form_validation->set_rules('entryexit_longitude', '', '');
		$this->form_validation->set_rules('vic_public_transport_origin', '', '');
		$this->form_validation->set_rules('stageplanpdf', '', '');
		$this->form_validation->set_rules('ga_tracking_id', '', '');
		$this->form_validation->set_rules('start_hex_colour', '', '');
		$this->form_validation->set_rules('end_hex_colour', '', '');
		$this->form_validation->set_rules('zoom_to_stage_button_hex', '', '');
		$this->form_validation->set_rules('zoom_to_stage_header_hex', '', '');
		$this->form_validation->set_rules('dev_domain', '', '');
		$this->form_validation->set_rules('sales_office_address', '', '');
		$this->form_validation->set_rules('sales_office_latitude', '', '');
		$this->form_validation->set_rules('sales_office_longitude', '', '');
		$this->form_validation->set_rules('form_code', '', '');
		$this->form_validation->set_rules('pdf_link', '', '');
		$this->form_validation->set_rules('access_key', '', '');
		$this->form_validation->set_rules('terms_and_conditions', '', '');
		$this->form_validation->set_rules('search_by_lot_price', '', '');
		$this->form_validation->set_rules('search_by_lot_width', '', '');
		$this->form_validation->set_rules('search_by_lot_size', '', '');
		$this->form_validation->set_rules('house_and_land_view', '', '');
		$this->form_validation->set_rules('land_view', '', '');
		$this->form_validation->set_rules('land_pdf_view', '', '');
		$this->form_validation->set_rules('house_land_order_by', '', '');
		$this->form_validation->set_rules('land_order_by', '', '');
		$this->form_validation->set_rules('land_pdf_order_by', '', '');
		$this->form_validation->set_rules('slider_skin_image', '', '');
		$this->form_validation->set_rules('overlay_filename_ipad', '', '');
		$this->form_validation->set_rules('mapovis_not_supported_url', '', '');
		$this->form_validation->set_rules('sales_office_title', '', '');
		$this->form_validation->set_rules('show_price', '', '');
		$this->form_validation->set_rules('show_telephone', '', '');
		$this->form_validation->set_rules('show_stage_icons', '', '');
		$this->form_validation->set_rules('custom_font_markup', '', '');
		$this->form_validation->set_rules('dialog_start_hex', '', '');
		$this->form_validation->set_rules('dialog_end_hex', '', '');
		$this->form_validation->set_rules('sales_telephone_number', '', '');
		$this->form_validation->set_rules('land_starting_from', '', '');
		$this->form_validation->set_rules('pdf_header_html', '', '');
		$this->form_validation->set_rules('pdf_header_margin', '', '');
		$this->form_validation->set_rules('pdf_footer_html', '', '');
		$this->form_validation->set_rules('pdf_footer_margin', '', '');
		$this->form_validation->set_rules('builder_pdf_header_html', '', '');
		$this->form_validation->set_rules('builder_pdf_header_margin', '', '');
		$this->form_validation->set_rules('builder_pdf_footer_html', '', '');
		$this->form_validation->set_rules('builder_pdf_footer_margin', '', '');
		$this->form_validation->set_rules('terms_and_conditions_builder', '', '');
		$this->form_validation->set_rules('diclaimer_builder', '', '');

		if($this->form_validation->run() != FALSE)
		{
			$form_data      = $this->input->post();
			/* If the validation was successfull add development */
			$development_id = $this->Model_development->addDevelopment($form_data);
			if($development_id){
				$this->session->set_flashdata('alertmessage', '<strong>The development has been added to the database.</strong><br><br>You now need to continue setting up the development on the <a href="'. base_url().'admin/developments/managedevelopmentid/'.$development_id.'">Manage Developments</a> page.');
			}
			else{
				$this->session->set_flashdata('errormessage', 'It was not possible to add the Development, please try again.');
			}
			redirect('admin/developments/adddevelopment');
		}
		$this->load->library('mapovis_lib');
		$states      = $this->Model_development->getStates();
		$developers  = $this->Model_developer->getDevelopers(TRUE);
		$data        = array(
			'states'               => $states,
			'developers'           => $developers,
			'house_land_order_bys' => $this->Model_development->getHouseLandOrderBy(),
			'house_and_land_views' => $this->mapovis_lib->getHouseAndLandViews(),
			'land_views'           => $this->mapovis_lib->getLandViews(),
			'land_pdf_views'       => $this->mapovis_lib->getLandPdfViews(),
		);
		$footer = array('admin/footer_wizard', 'admin/developments/view_adddevelopment_customjs');
		$this->load_template('admin/developments/view_adddevelopment', $data, $footer);
	}

	function impexplotscsv()
	{
		$developments = $this->Model_development->getDevelopments();
		$data         = array(
			'developments' => $developments,
		);
		$this->load_template('admin/developments/view_impexplotscsv', $data);
	}

	function exportlotscsv()
	{
		$this->load->model('Model_import_export_file', '', TRUE);
		$development_id   = $this->input->post('development_id');

		$csv_file_content = $this->Model_import_export_file->getCsvLots($development_id);
		$file_name        = ($development_id)? 'lots_dev'.$development_id.'_'.date('Ymd').'.csv': 'lots_'.date('Ymd').'.csv';
		if($csv_file_content){
			header('Content-Type: application/csv');
			header('Content-Disposition: attachement; filename="'.$file_name.'"');
			echo $csv_file_content;
			exit();
		}
		$this->session->set_flashdata('alertmessage', 'It was not possible to generate the CSV file. Please try again.');
		redirect('admin/developments/impexplotscsv');
		exit();
	}

	function importeditlotscsvfile()
	{
		$this->load->model('Model_import_export_file', '', TRUE);
		$config = array(
			'upload_path'   => FCPATH.'uploads'.DIRECTORY_SEPARATOR,
			'allowed_types' => 'csv',
			'max_size'	    => '100',
			'max_width'     => '1024',
			'max_height'    => '768'
		);
		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('csvfilename')){
			$this->session->set_flashdata('errormessage', $this->upload->display_errors());
			redirect('admin/developments/importdevelopmentscsv');

		}
		$upload_data  = $this->upload->data();
		$file_id      = $this->Model_import_export_file->addImportFileToUpdate($upload_data);
		$file_details = $this->Model_import_export_file->getImportedFile($file_id);
		$data        = array(
			'file_details' => $file_details,
			'notes'        => $file_details->import_notes,
		);
		$this->load_template('admin/developments/view_importcsvdata', $data);
	}

	function managedevelopmentlinks($development_id)
	{
		$this->load->model('Model_development_link', '', TRUE);
		$data        = array(
			'development'         => $this->getDevelopmentValidate($development_id),
			'linked_developments' => $this->Model_development_link->getDevelopmentLinks($development_id),
		);
		$footer = array('admin/footer_standard', 'admin/developments/view_managedevelopmentlinks_customjs');
		$this->load_template('admin/developments/view_managedevelopmentlinks', $data, $footer);
	}

	function adddevelopmentlink($development_id)
	{
		$this->load->model('Model_development_link', '', TRUE);

		$development           = $this->getDevelopmentValidate($development_id);
		$possible_developments = $this->Model_development_link->getPossibleDevelopments($development_id);

		$form_data             = $this->input->post();
		if($form_data != FALSE)
		{
			//Field validation success, add precinct
			if($this->Model_development_link->updateDevelopmentLink($development_id, $form_data['development_id2'], $form_data['distance'], $form_data['suburb'])){
				$this->session->set_flashdata('alertmessage', 'The developments link has been added successfully.');
				redirect('admin/developments/managedevelopmentlinks/'.$development_id);
			}
		}
		$data        = array(
			'development'           => $development,
			'possible_developments' => $possible_developments,
		);
		$this->load_template('admin/developments/view_adddevelopmentlink', $data);
	}

	public function editdevelopmentlink($development_id1, $development_id2)
	{
		$this->load->model('Model_development_link', '', TRUE);
		$development      = $this->getDevelopmentValidate($development_id1);
		$development_link = $this->Model_development_link->getDevelopmentLink($development_id1, $development_id2);

		$form_data             = $this->input->post();
		if($form_data != FALSE)
		{
			//Field validation success, add precinct
			if($this->Model_development_link->updateDevelopmentLink($development_id1, $development_id2, $form_data['distance'], $form_data['suburb'])){
				$this->session->set_flashdata('alertmessage', 'The development link has been updated successfully.');
				redirect('admin/developments/managedevelopmentlinks/'.$development_id1);
			}
		}
		$data        = array(
			'development_id1'  => $development_id1,
			'development_id2'  => $development_id2,
			'development'      => $development,
			'development_link' => $development_link,
		);
		$this->load_ajaxtemplate('admin/developments/view_editdevelopmentlink', $data);
	}

	public function deletedevelopmentlink($development_id1, $development_id2)
	{
		$this->load->model('Model_development_link', '', TRUE);
		if($this->Model_development_link->deleteDevelopmentlink($development_id1, $development_id2)){
			$this->session->set_flashdata('alertmessage', 'The development link has been delete successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'The development link was not deleted.');
		}
		redirect('admin/developments/managedevelopmentlinks/'.$development_id1);
	}

	function deletestatisticsdata($development_id)
	{
		$this->load->model('Model_statistics_lotview', '', TRUE);
		$this->load->model('Model_statistics_placesearch', '', TRUE);
		$this->load->model('Model_statistics_directionsearch', '', TRUE);
		$this->load->model('Model_availablelot_by_width', '', TRUE);
		$this->load->model('Model_average_lot_prices_by_width', '', TRUE);
		$this->load->model('Model_change_log', '', TRUE);
		$this->load->model('Model_hnl_change_log', '', TRUE);

		$this->Model_statistics_lotview->deleteDevelopmentViews($development_id);
		$this->Model_statistics_placesearch->deleteDevelopmentViews($development_id);
		$this->Model_statistics_directionsearch->deleteDevelopmentViews($development_id);
		$this->Model_availablelot_by_width->deleteDevelopmentViews($development_id);
		$this->Model_average_lot_prices_by_width->deleteDevelopmentRows($development_id);
		$this->Model_change_log->deleteDevelopmentLogs($development_id);
		$this->Model_hnl_change_log->deleteDevelopmentLogs($development_id);
		
		$this->session->set_flashdata('alertmessage', 'The statistics data has been deleted successfully.');
		//Go to Manage area
		redirect('admin/developments/managedevelopmentid/'.$development_id);
	}

	function pricelistschedule($development_id)
	{
		$development  = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_pricelist_schedule', '', TRUE);

		$form_data = $this->input->post();
		if($form_data != FALSE)
		{
			//Field validation success, add or edit reminder
			if($this->Model_pricelist_schedule->updatePricelistSchedule($development_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The price list schedule has been updated successfully.');
				redirect('admin/developments/managedevelopmentid/'.$development_id);
			}
		}
		$schedule     = $this->Model_pricelist_schedule->getPricelistSchedule($development_id);
		$data         = array(
			'development' => $development,
			'schedule'    => $schedule,
		);
		$this->load_template('admin/developments/view_pricelistschedule', $data);
	}

	function managepricelistsubscribers($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$pricelist_subscribers = $this->Model_pricelist_subscriber->getPricelistSubscribers($development_id);
		$data           = array(
			'pricelist_subscribers' => $pricelist_subscribers,
			'development'           => $development
		);
		$footer = array('admin/footer_standard', 'admin/developments/view_pricelistsubscribers_customjs');
		$this->load_template('admin/developments/view_pricelistsubscribers', $data, $footer);
	}

	function viewpricelistsubscriber($pricelist_subscriber_id)
	{
		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$pricelist_subscriber = $this->Model_pricelist_subscriber->getPricelistSubscriber($pricelist_subscriber_id);
		$data         = array(
			'pricelist_subscriber' => $pricelist_subscriber,
		);
		$this->load_ajaxtemplate('admin/developments/view_pricelistsubscriber', $data);
	}

	function updatepricelistsubscriber($pricelist_subscriber_id)
	{
		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$pricelist_subscriber = $this->Model_pricelist_subscriber->getPricelistSubscriber($pricelist_subscriber_id);
		$form_data     = $this->input->post();
		if($pricelist_subscriber && $this->Model_pricelist_subscriber->updatePricelistSubscriber($pricelist_subscriber_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The pricelist subscriber was updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the pricelist subscriber, please make sure the data is correct.');
		}
		redirect('admin/developments/managepricelistsubscribers/'.$pricelist_subscriber->development_id);
	}

	function addpricelistsubscriber($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$form_data = $this->input->post();
		if($form_data != FALSE)
		{
			$redirecto_to                 = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? "addpricelistsubscriber/{$development_id}": 'managepricelistsubscribers/'.$development_id;
			$this->Model_pricelist_subscriber->addPricelistSubscriber($development_id, $form_data);
			$this->session->set_flashdata('alertmessage', 'The pricelist subscriber was added successfully.');
			redirect('admin/developments/'.$redirecto_to);
		}
		$data          = array(
			'development' => $development
		);
		$footer        = array('admin/footer_standard', 'admin/developments/view_actioncontinue_customjs');
		$this->load_template('admin/developments/view_addpricelistsubscriber', $data, $footer);
	}

	function deletepricelistsubscriber($pricelist_subscriber_id)
	{
		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$amenity   = $this->Model_pricelist_subscriber->getPricelistSubscriber($pricelist_subscriber_id);

		if($this->Model_pricelist_subscriber->deletePricelistSubscriber($pricelist_subscriber_id)){
			$this->session->set_flashdata('alertmessage', 'The pricelist subscriber was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the pricelist subscriber.');
		}
		redirect('admin/developments/managepricelistsubscribers/'.$amenity->development_id);
	}

	function pricelistpdf($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);
		$this->load->model('Model_apipdf', '', TRUE);
		$this->Model_apipdf->devpdfpricelist($development->development_id, FALSE);
	}

	function pricelistindivemail($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$pricelist_subscribers = $this->Model_pricelist_subscriber->getPricelistSubscribers($development_id);
		$data           = array(
			'pricelist_subscribers' => $pricelist_subscribers,
			'development'           => $development
		);
		$this->load_template('admin/developments/view_pricelistindivemail', $data);
	}

	function pricelistsendindivemail($subscriber_id)
	{
		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$this->load->model('Model_pricelist_schedule', '', TRUE);
		$pricelist_subscriber = $this->Model_pricelist_subscriber->getPricelistSubscriber($subscriber_id);
		$development          = $this->getDevelopmentValidate($pricelist_subscriber->development_id);
		$this->Model_pricelist_schedule->sendPricelistToSubscriber($development, $pricelist_subscriber);
		
		$this->session->set_flashdata('alertmessage', 'The Email has been sent successfully.');
		redirect('admin/developments/managedevelopmentid/'.$development->development_id);
	}

	function developmentdocuments($development_id)
	{
		$this->load->model('Model_development_document', '', TRUE);
		$development = $this->Model_development->getDevelopment($development_id);
		$documents   = $this->Model_development_document->getDevelopmentDocuments($development_id);
		$data        = array(
			'development' => $development,
			'documents'   => $documents,
			'types'       => $this->Model_development_document->getTypes(),
		);
		$this->load_template('admin/developments/view_developmentdocuments', $data);
	}

	function uploaddevelopmentdocument($development_id)
	{
		$this->load->model('Model_development_document', '', TRUE);
		$development = $this->Model_development->getDevelopment($development_id);
		$form_data = $this->input->post();
		if($form_data != FALSE){
			if($this->Model_development_document->addDevelopmentDocument($development_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The Document was added successfully.');
			}
			else{
				$this->session->set_flashdata('errormessage', 'It was not possible to added the document, please try again.');
			}
			redirect('admin/developments/developmentdocuments/'.$development_id);
		}
		$data        = array(
			'development'  => $development,
			'types'        => $this->Model_development_document->getTypes(),
			'default_type' => $this->input->get('type_name'),
		);
		$this->load_ajaxtemplate('admin/developments/view_developmentdocumentupload', $data);
	}

	function deletedevelopmentdocument($development_id, $document_id)
	{
		$this->load->model('Model_development_document', '', TRUE);
		// make sure the document belongs to given development
		if($this->Model_development_document->deleteDevelopmentDocuments($document_id)){
			$this->session->set_flashdata('alertmessage', 'The Document was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the document, please try again.');
		}
		redirect('admin/developments/developmentdocuments/'.$development_id);
	}

	/**
	 * Returns the development object and validates if it exists if not it will redirect the user to Manage Developments
	 * @param int $development_id
	 * @return object Development
	 */
	private function getDevelopmentValidate($development_id)
	{
		$development = $this->Model_development->getDevelopment($development_id);
		if($development_id != 'plugins' && !$development){
			$this->session->set_flashdata('alertmessage', 'The development was not found.');
			redirect('admin/developments/managedevelopments');
		}
		return $development;
	}

	/*********** CALLBACK FUNCTIONS *************/

	/**
	 * Validates if the precinct number provided is unique in a development
	 * @return boolean
	 */
	public function is_unique_precinct()
	{
		$precinct_number   = $this->input->post('precinct_number');
		$development_id = $this->input->post('development_id');
		if(!$this->Model_precinct->isUnique($development_id, $precinct_number)){
			$this->form_validation->set_message('is_unique_precinct', 'The Precinct Number already exists in the database.');
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * Validates if the stage number provided is unique in a precinct
	 * @return boolean
	 */
	public function is_unique_stage()
	{
		$precinct_id  = $this->input->post('precinct_id');
		$stage_number = $this->input->post('stage_number');
		if(!$this->Model_stage->isUnique($precinct_id, $stage_number)){
			$this->form_validation->set_message('is_unique_stage', 'The Stage Number already exists in the database.');
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * Validates if the lot number provided is unique in a stage
	 * @return boolean
	 */
	public function is_unique_lot()
	{
		$stage_id     = $this->input->post('stage_id');
		$lot_number = $this->input->post('lot_number');
		if(!$this->Model_lot->isUnique($stage_id, $lot_number)){
			$this->form_validation->set_message('is_unique_lot', 'The Lot Number already exists in the database.');
			return FALSE;
		}
		return TRUE;
	}

	/* Use this function to export the data from house matches to house_lot_packages */
	function updatematches()
	{
		$this->load->model('Model_user', '', TRUE);
		$current_admin = $this->Model_user->getAdminLoggedIn();
		$query         = $this->db->get('house_matches');
		$house_matches = array();
		if ($query->num_rows() > 0)
		{
			$house_matches = $query->result();
		}
		foreach($house_matches as $house_match){
			echo "$house_match->lot_id, $house_match->house_id<br>";
			$this->Model_house_lot_package->enableDisableHouseLotPdf($house_match->lot_id, $house_match->house_id, TRUE, $current_admin);
		}
	}

}
?>