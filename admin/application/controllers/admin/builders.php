<?php
if( !defined('BASEPATH')) exit('No direct script access allowed');
require_once('adminmain_controller.php');

class Builders extends AdminMain_Controller{

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in_id'))
		{
			redirect('admin/login');
			exit();
		}
		$this->load->model('Model_builder', '', TRUE);
	}

	function index(){
		$this->viewbuilders();
	}

	function viewbuilders()
	{
		$this->load->model('Model_builder_user', '', TRUE);
		$builders       = $this->Model_builder->getBuilders();
		$builders_users = $this->Model_builder_user->getAllBuilderUsers();
		$data           = array(
			'builders'       => $builders,
			'builders_users' => $builders_users
		);
		$this->load_template('admin/builders/view_viewbuilders', $data);
	}

	function managebuilder($builder_id)
	{
		$builder = $this->Model_builder->getBuilder($builder_id);
		if(!$builder){
			$this->session->set_flashdata('errormessage', 'Invalid builder.');
			redirect('admin/builders/');
		}
		$data    = array(
			'builder' => $builder,
		);
		$this->load_template('admin/builders/view_managebuilder', $data);
	}

	function addbuilder()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('builder_name', 'Builder Name', 'trim|required|xss_clean|callback_is_unique_builder');
		$this->form_validation->set_rules('builder_website', 'Builder Website', '');
		$this->form_validation->set_rules('builder_email', 'Builder Email', '');
		$this->form_validation->set_rules('builder_telephone', 'Builder Telephone', '');

		if($this->form_validation->run() != FALSE)
		{
			$form_data  = $this->input->post();
			$builder_id = $this->Model_builder->addBuilder($form_data);
			if($builder_id){
				$this->session->set_flashdata('alertmessage', 'The builder was added successfully.');
				redirect('admin/builders/managebuilder/'.$builder_id);
			}
		}
		$this->load_template('admin/builders/view_addbuilder');
	}

	function editbuilder($builder_id)
	{
		$builder     = $this->Model_builder->getBuilder($builder_id);
		if(!$builder){
			$this->session->set_flashdata('errormessage', 'Invalid builder.');
			redirect('admin/builders/');
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('builder_name', 'Builder Name', 'trim|required|xss_clean|callback_is_unique_builder');
		$this->form_validation->set_rules('builder_website', 'Builder Website', '');
		$this->form_validation->set_rules('builder_email', 'Builder Email', '');
		$this->form_validation->set_rules('builder_telephone', 'Builder Telephone', '');

		if($this->form_validation->run() != FALSE)
		{
			$form_data = $this->input->post();
			if($this->Model_builder->updateBuilder($builder_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The builder was updated successfully.');
			}
			else{
				$this->session->set_flashdata('errormessage', 'It was not possible to update the builder, please make sure the data is correct.');
			}
			redirect('admin/builders/managebuilder/'.$builder_id);
		}
		$data    = array(
			'builder' => $builder,
		);
		$this->load_template('admin/builders/view_editbuilder', $data);
	}

	function deletebuilder($builder_id)
	{
		if($this->Model_builder->deleteBuilder($builder_id)){
			$this->session->set_flashdata('alertmessage', 'The builder was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the builder.');
		}
		redirect('admin/builders/viewbuilders');
	}

	function builderusers($builder_id)
	{
		$builder = $this->Model_builder->getBuilder($builder_id);
		$this->load->model('Model_user', '', TRUE);
		$this->load->model('Model_builder_user', '', TRUE);

		$data  = array(
			'builder'       => $builder,
			'builder_users' => $this->Model_builder_user->getBuilderUsersPossible($builder_id),
		);
		$this->load_template('admin/builders/view_builder_users', $data);
	}

	function addbuilderusers($builder_id, $user_id)
	{
		$this->load->model('Model_builder_user', '', TRUE);
		$this->Model_builder_user->addBuilderUser($builder_id, $user_id);
		echo 'The user was added to the builder.';
		exit();
	}

	function deletebuilderusers($builder_id, $user_id)
	{
		$this->load->model('Model_builder_user', '', TRUE);
		$this->Model_builder_user->deleteBuilderUser($user_id);
		echo 'The user was removed from the builder.';
		exit();
	}

	function salespeople($builder_id){
		$this->load->model('Model_builder_sales_person', '', TRUE);
		$builder      = $this->Model_builder->getBuilder($builder_id);
		$sales_people = $this->Model_builder_sales_person->getSalesPeople($builder_id);

		$data  = array(
			'builder'      => $builder,
			'sales_people' => $sales_people,
		);
		$footer = array('admin/footer_standard', 'admin/builders/view_salespeople_customjs');
		$this->load_template('admin/builders/view_sales_people', $data, $footer);
	}

	function addsalesperson($builder_id)
	{
		$this->load->model('Model_builder_sales_person', '', TRUE);
		$builder = $this->Model_builder->getBuilder($builder_id);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

		if($this->form_validation->run() != FALSE)
		{
			$form_data = $this->input->post();
			//Field validation success, add precinct
			if($this->Model_builder_sales_person->addSalesPerson($builder_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The builder was added successfully.');
				redirect('admin/builders/salespeople/'.$builder_id);
			}
		}
		$data   = array('builder' => $builder);
		$footer = array('admin/footer_standard', 'admin/builders/view_salespeople_customjs');
		$this->load_template('admin/builders/view_addsalesperson', $data, $footer);
	}

	function editsalesperson($sales_person_id)
	{
		$this->load->model('Model_builder_sales_person', '', TRUE);
		$sales_person = $this->Model_builder_sales_person->getSalesPerson($sales_person_id);
		$builder      = $this->Model_builder->getBuilder($sales_person->builder_id);
		$data         = array(
			'builder'      => $builder,
			'sales_person' => $sales_person
		);
		$this->load_ajaxtemplate('admin/builders/view_editsalesperson', $data);
	}

	function updatesalesperson($sales_person_id)
	{
		$this->load->model('Model_builder_sales_person', '', TRUE);
		$sales_person = $this->Model_builder_sales_person->getSalesPerson($sales_person_id);

		$form_data = $this->input->post();
		if($this->Model_builder_sales_person->updateSalesPerson($sales_person_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The sales person was udpated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the sales person, please make sure the data is correct.');
		}
		redirect('admin/builders/salespeople/'.$sales_person->builder_id);
	}

	function deletesalespersonimage($sales_person_id)
	{
		$this->load->model('Model_builder_sales_person', '', TRUE);

		if($this->Model_builder_sales_person->deleteImage($sales_person_id)){
			$result = array('status' => 1, 'msg'=> 'The image was deleted successfully.');
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to delete the image, please try again.');
		}
		echo json_encode($result);
		exit();
	}

	function deletesalesperson($sales_person_id)
	{
		$this->load->model('Model_builder_sales_person', '', TRUE);
		$sales_person = $this->Model_builder_sales_person->getSalesPerson($sales_person_id);
		if($this->Model_builder_sales_person->deleteSalesPerson($sales_person_id)){
			$this->session->set_flashdata('alertmessage', 'The sales person was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the sales person, please try again.');
		}
		redirect('admin/builders/salespeople/'.$sales_person->builder_id);
	}

	/**
	 * Validates if the precinct number provided is unique in a development
	 * @return boolean
	 */
	public function is_unique_builder()
	{
		$builder_name   = $this->input->post('builder_name');
		$builder_id = $this->input->post('builder_id');
		if($this->Model_builder->duplicateName($builder_name, $builder_id) != FALSE){
			$this->form_validation->set_message('is_unique_builder', 'The builder name already exists in the system.');
			return FALSE;
		}
		return TRUE;
	}
}
?>