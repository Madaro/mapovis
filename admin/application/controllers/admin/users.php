<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('adminmain_controller.php');

class Users extends AdminMain_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in_id'))
		{
			redirect('admin/login');
			exit;
		 }
		$this->load->model('Model_user', '', TRUE);
	}

	public function index()
	{
		$data  = array();
		$this->load_template('admin/users/view_users', $data);
	}

	public function view()
	{
		$users = $this->Model_user->getUsers(array('user_role' => 'User'));
		$data  = array(
			'users' => $users,
		);
		$this->load_template('admin/users/view_users_view', $data);
	}

	public function add()
	{
		$this->load->library('form_validation');
		$this->load->library('mapovis_lib');

		/* There are some empty rules because this way the form can be refilled with given data
		 * so the user doesn't have to type the data again. */
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[6]|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_rules('developer', 'Developer', '');
		$this->form_validation->set_rules('office', 'Office', '');
		$this->form_validation->set_rules('officephone', 'Office Phone', '');
		$this->form_validation->set_rules('mobilephone', 'Mobile Phone', '');

		if($this->form_validation->run() != FALSE)
		{
			//Field validation success, update development
			$form_data = $this->input->post();
			if($this->Model_user->addUser($form_data)){
				$this->session->set_flashdata('alertmessage', 'The user has been added successfully.');
				//Go to Manage area
				redirect('admin/users/view');
			}
		}
		$this->load->model('Model_developer');

		$data               = array();
		$data['developers'] = $this->Model_developer->getDevelopers();
		$this->load_template('admin/users/view_users_add', $data);
	}

	public function updateuser($user_id)
	{
		// Load Model
		$this->load->model('Model_developer');
		$this->load->library('mapovis_lib');

		$data  = array(
			'user'       => $this->Model_user->getUser($user_id),
			'developers' => $this->Model_developer->getDevelopers(),
		);
		$this->load_template('admin/users/view_users_update', $data);
	}

	public function updateuser_submit($user_id)
	{
		// Setup User Data for Database UPDATE
		 $user = array(
			'username'    => $this->input->post('username'),
			'name'        => $this->input->post('name'),
			'email'       => $this->input->post('email'),
			'developer'   => $this->input->post('developer'),
			'office'      => $this->input->post('office'),
			'officephone' => $this->input->post('officephone'),
			'mobilephone' => $this->input->post('mobilephone'),
		);

		// If Password HAS CHANGED, add new password to array
		if ($this->input->post('password') != "")
		{
			$user['password'] = md5($this->input->post('password'));
		}

		// Run DB Query
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $user);
		$this->session->set_flashdata('alertmessage', 'The user was updated successfully.');

		// Redirect to 'view'
		redirect('/admin/users/view');
	}

	function deleteuser($user_id)
	{
		if($this->Model_user->deleteUser($user_id)){
			$this->session->set_flashdata('alertmessage', 'The user has been deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the user.');
		}
		//Go to Manage developments
		redirect('admin/users/view/');
	}

	public function viewadmins()
	{
		$users = $this->Model_user->getUsers(array('user_role' => 'Admin'));
		$data  = array(
			'users' => $users,
		);
		$this->load_template('admin/users/view_view_admins', $data);
	}

	public function viewbuilders()
	{
		$users = $this->Model_user->getUsers(array('user_role' => 'Builder'), TRUE);
		$data  = array(
			'users' => $users,
		);
		$this->load_template('admin/users/view_view_builders', $data);
	}

	public function addbuilderuser()
	{
		$this->load->library('form_validation');
		$this->load->library('mapovis_lib');

		/* There are some empty rules because this way the form can be refilled with given data
		 * so the user doesn't have to type the data again. */
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[6]|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_rules('developer', 'Developer', '');
		$this->form_validation->set_rules('office', 'Office', '');
		$this->form_validation->set_rules('officephone', 'Office Phone', '');
		$this->form_validation->set_rules('mobilephone', 'Mobile Phone', '');

		if($this->form_validation->run() != FALSE)
		{
			//Field validation success, update development
			$form_data = $this->input->post();
			if($this->Model_user->addUser($form_data, 'Builder')){
				$this->session->set_flashdata('alertmessage', 'The user has been added successfully.');
				//Go to Manage area
				redirect('admin/users/viewbuilders');
			}
		}
		$this->load->model('Model_builder', '', TRUE);

		$data               = array();
		$data['builders']   = $this->Model_builder->getBuilders();
		$this->load_template('admin/users/view_builderusers_add', $data);
	}

	public function updatebuilderuser($user_id)
	{
		// Load Model
		$this->load->model('Model_builder');
		$this->load->model('Model_builder_user', '', TRUE);
		$this->load->library('mapovis_lib');

		$data  = array(
			'user'     => $this->Model_user->getUser($user_id, TRUE),
			'builders' => $this->Model_builder->getBuilders(),
		);
		$this->load_template('admin/users/view_builderusers_update', $data);
	}

	public function updatebuilderuser_submit($user_id)
	{
		// Setup User Data for Database UPDATE
		 $user = array(
			'username'    => $this->input->post('username'),
			'name'        => $this->input->post('name'),
			'email'       => $this->input->post('email'),
			'developer'   => $this->input->post('developer'),
			'office'      => $this->input->post('office'),
			'officephone' => $this->input->post('officephone'),
			'mobilephone' => $this->input->post('mobilephone'),
		);

		// If Password HAS CHANGED, add new password to array
		if ($this->input->post('password') != "")
		{
			$user['password'] = md5($this->input->post('password'));
		}

		$user_obj = $this->Model_user->getUser($user_id);
		// Run DB Query
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $user);
		$builder_id = $this->input->post('builder_id');
		if($user_obj->user_role == 'Builder' && $builder_id){
			$this->load->model('Model_builder_user', '', TRUE);
			$this->Model_builder_user->deleteBuilderUser($user_id);
			$this->Model_builder_user->addBuilderUser($builder_id, $user_id);
		}
		$this->session->set_flashdata('alertmessage', 'The user was updated successfully.');

		// Redirect to 'view'
		redirect('/admin/users/viewbuilders');
	}

	function deletebuilderuser($user_id)
	{
		if($this->Model_user->deleteUser($user_id)){
			$this->session->set_flashdata('alertmessage', 'The user has been deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the user.');
		}
		//Go to Manage developments
		redirect('admin/users/viewbuilders/');
	}

	public function check_user_details()
	{
		$user_id   = $this->input->post('user_id');
		$username  = $this->input->post('username');
		$email     = $this->input->post('email');
		$valid     = TRUE;
		$msg       = '';
		if(!$this->Model_user->isUnique($email, $user_id)){
			$msg  += 'The email address already exists in the system.';
			$valid = FALSE;
		}
		if(!$this->Model_user->isUniqueUsername($username, $user_id)){
			$msg  += 'The username address already exists in the system.';
			$valid = FALSE;
		}
		$this->form_validation->set_message('check_user_details', $msg);
		return $valid;
	}

	public function proxy($user_id)
	{
		$current_admin = $this->Model_user->getAdminLoggedIn();
		$current_user  = $this->Model_user->getUser($user_id);
		if($current_admin && $current_user && $current_user->user_role == 'User'){
			$this->load->library('mapovis_lib');
			$this->mapovis_lib->logInUser($current_user, $this);
			$this->session->set_flashdata('alertmessage', 'You have successfully logged in as '.$current_user->name);
			redirect('salesteam/dashboard');
		}
		$this->session->set_flashdata('errormessage', 'Invalid user.');
		redirect('admin/users/view');
	}

	public function builderproxy($user_id)
	{
		$current_admin = $this->Model_user->getAdminLoggedIn();
		$current_user  = $this->Model_user->getUser($user_id);
		if($current_admin && $current_user && $current_user->user_role == 'Builder'){
			$this->load->library('mapovis_lib');
			$this->mapovis_lib->logInBuilder($current_user, $this);
			$this->session->set_flashdata('alertmessage', 'You have successfully logged in as '.$current_user->name);
			redirect('builder/dashboard');
		}
		$this->session->set_flashdata('errormessage', 'Invalid user.');
		redirect('admin/users/view');
	}

}
?>