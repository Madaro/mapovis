<?php
if( !defined('BASEPATH')) exit('No direct script access allowed');
require_once('adminmain_controller.php');

class Markers extends AdminMain_Controller{

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in_id'))
		{
			redirect('admin/login');
			exit();
		}
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_mini_marker', '', TRUE);
	}

	function manageminimarkers($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_mini_marker', '', TRUE);
		$mini_markers = $this->Model_mini_marker->getMiniMarkers($development_id);
		$data         = array(
			'mini_markers' => $mini_markers,
			'development'  => $development
		);
		$footer = array('admin/footer_standard', 'admin/markers/view_minimarkers_customjs');
		$this->load_template('admin/markers/view_viewminimarkers', $data, $footer);
	}

	function viewminimarker($mini_marker_id)
	{
		$this->load->model('Model_mini_marker', '', TRUE);
		$mini_marker = $this->Model_mini_marker->getMiniMarker($mini_marker_id);
		$data         = array(
			'mini_marker' => $mini_marker,
		);
		$this->load_ajaxtemplate('admin/markers/view_viewminimarker', $data);
	}

	function updateminimarker($mini_marker_id)
	{
		$this->load->model('Model_mini_marker', '', TRUE);
		$mini_marker = $this->Model_mini_marker->getMiniMarker($mini_marker_id);
		$form_data     = $this->input->post();
		if($mini_marker && $this->Model_mini_marker->updateMiniMarker($mini_marker_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The mini marker was updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the mini marker, please make sure the data is correct.');
		}
		redirect('admin/markers/manageminimarkers/'.$mini_marker->development_id);
	}

	function addminimarker($development_id)
	{
		$development = $this->getDevelopmentValidate($development_id);

		$this->load->model('Model_mini_marker', '', TRUE);
		$form_data = $this->input->post();
		if($form_data != FALSE)
		{
			$redirecto_to                 = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? "addminimarker/{$development_id}": 'manageminimarkers/'.$development_id;
			$this->Model_mini_marker->addMiniMarker($development_id, $form_data);
			$this->session->set_flashdata('alertmessage', 'The mini marker was added successfully.');
			redirect('admin/markers/'.$redirecto_to);
		}
		$data          = array(
			'development' => $development
		);
		$footer        = array('admin/footer_standard', 'admin/developments/view_actioncontinue_customjs');
		$this->load_template('admin/markers/view_addminimarker', $data, $footer);
	}

	function deleteminimarker($mini_marker_id)
	{
		$this->load->model('Model_mini_marker', '', TRUE);
		$amenity   = $this->Model_mini_marker->getMiniMarker($mini_marker_id);

		if($this->Model_mini_marker->deleteMiniMarker($mini_marker_id)){
			$this->session->set_flashdata('alertmessage', 'The mini marker was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the mini marker.');
		}
		redirect('admin/markers/manageminimarkers/'.$amenity->development_id);
	}

	/**
	 * Returns the development object and validates if it exists if not it will redirect the user to Manage Developments
	 * @param int $development_id
	 * @return object Development
	 */
	private function getDevelopmentValidate($development_id)
	{
		$development = $this->Model_development->getDevelopment($development_id);
		if($development_id != 'plugins' && !$development){
			$this->session->set_flashdata('alertmessage', 'The development was not found.');
			redirect('admin/developments/managedevelopments');
		}
		return $development;
	}
}
?>