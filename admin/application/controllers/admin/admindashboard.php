<?php
if( !defined('BASEPATH')) exit('No direct script access allowed');
require_once('adminmain_controller.php');

class AdminDashboard extends AdminMain_Controller {

	function __construct() 
	{
		parent::__construct(); 
		if(!$this->session->userdata('logged_in_id'))
		{
		   redirect('admin/login');
		   exit;
		}
		$this->load->model('Model_config', '', TRUE);
	}

	function index()
	{
		$this->load->model('Model_reminder', '', TRUE);
		$this->load->model('Model_user', '', TRUE);
		$this->load->model('Model_change_log', '', TRUE);

		$current_admin        = $this->Model_user->getAdminLoggedIn();
		$settings             = $this->Model_config->getConfig();
		$recent_logs          = $this->Model_change_log->getChangeLogs(array(), TRUE, $settings->number_logs);
		$overdue_developments = $this->Model_reminder->getOverdueDevelopments();
		$problem_users        = $this->Model_reminder->getProblemUsers();
		$data                 = array(
			'current_user'       => $current_admin,
			'recent_logs'        => $recent_logs,
			'total_overdue'      => count($overdue_developments),
			'total_problem_user' => count($problem_users),
			'total_tickets'      => 4,// This needs to be changed to get accurate data
		);
		$this->load_template('admin/dashboard/view_dashboard', $data);
	 }

	 function savedefaultpagination()
	{
		$this->load->model('Model_user', '', TRUE);
		$current_admin      = $this->Model_user->getAdminLoggedIn();
		$default_pagination = $this->input->post('default_pagination');
		$this->Model_user->setDefaultPagination($current_admin->user_id, $default_pagination);
		echo 'save_'.$default_pagination;
	}
}
?>