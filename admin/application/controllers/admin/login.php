<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		/* if user is already logged in, redirect to main page */
		if($this->session->userdata('logged_in_id'))
		{
			redirect('admin/admindashboard');
			exit;
		}
		$this->load->view('admin/login');
	}

	function logout()
	{
		//log out admin user
		$this->load->helper('cookie');
		delete_cookie("admin_logged_role");
		$this->session->unset_userdata('logged_in_id');
		$this->session->unset_userdata('logged_in_role');
		$this->session->unset_userdata('logged_in_username');
		redirect('admin/login');
	}

}
?>