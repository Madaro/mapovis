<?php
if( !defined('BASEPATH')){exit('No direct script access allowed');}

class SalesTeamMain_Controller extends CI_Controller {

	/**
	 * This function will load the header, content layout and footer
	 * the controller can call load_template and provide the view and data, if the title and footer are different they can be specified
	 * @param string $layout_content
	 * @param array $data
	 * @param array $footer
	 * @param string $title
	 */
	function load_template($layout_content, $data = array(), $footer = array('user/footer_standard'), $title = 'Mapovis')
	{
		$this->load->model('Model_user', '', TRUE);
		$this->load->model('Model_development', '', TRUE);
		$current_user                         = $this->Model_user->getUserLogged();
		$show_statistics                      = $this->Model_development->showStatistics();
		$development_icon                     = $this->Model_development->UserDevAvatar();
		if(empty($development_icon)){
			$development_icon = base_url(). 'assets/images/mapovis-avatar.gif';
		}

		$section_segment                      = $this->uri->segment(2);
		$warningmessage                       = $this->session->flashdata('warningmessage');
		$successmessage                       = $this->session->flashdata('alertmessage');
		$errormessage                         = $this->session->flashdata('errormessage');
		$success_message                      = (!empty($successmessage))? '<div class="alert alert-success">'.$successmessage.'</div>': '';
		$error_message                        = (!empty($errormessage))? '<div class="alert alert-danger">'.$errormessage.'</div>': '';
		$warning_message                      = (!empty($warningmessage))? '<div class="alert alert-warning">'.$warningmessage.'</div>': '';

		$data['avatar_icon']                  = $development_icon;
		$data['title']                        = $title;
		$data['layout_content']               = ($layout_content)? $layout_content: 'user/view_dashboard';
		$data['footer_custom_content_chunks'] = $footer;
		$data['alert_message']                = "{$success_message} {$error_message} {$warning_message}";
		$data['section_segment']              = $section_segment;
		$data['show_statistics']              = $show_statistics;
		$data['default_pagination']           = $current_user->default_pagination;
		$this->load->view('user/usertemplate', $data);
	}

	function load_ajaxtemplate($layout_content, $data = array())
	{
		$warningmessage        = $this->session->flashdata('warningmessage');
		$successmessage        = $this->session->flashdata('alertmessage');
		$errormessage          = $this->session->flashdata('errormessage');
		$success_message       = (!empty($successmessage))? '<div class="alert alert-success">'.$successmessage.'</div>': '';
		$error_message         = (!empty($errormessage))? '<div class="alert alert-danger">'.$errormessage.'</div>': '';
		$warning_message       = (!empty($warningmessage))? '<div class="alert alert-warning">'.$warningmessage.'</div>': '';

		$data['alert_message'] = "{$success_message} {$error_message} {$warning_message}";
		$this->load->view($layout_content, $data);
	}
}
?>