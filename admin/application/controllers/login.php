<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		/* if user is already logged in, redirect to main page */
		if($this->session->userdata('user_logged_id'))
		{
			redirect('salesteam');
			exit;
		}
		$loginmessage  = $this->session->flashdata('loginmessage');
		$login_message = (!empty($loginmessage))? '<div class="alert alert-warning">'.$loginmessage.'</div>': '';
		$this->load->view('user/login', array('login_message' => $login_message));
	}

	function forgotpassword()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('emailaddress', 'Email Address', 'trim|required|valid_email|callback_finduser');
		if($this->form_validation->run() != FALSE){
			$email_address = $this->input->post('emailaddress');
			if($this->Model_user->resetPasswordSendNotification($email_address)){
				$this->session->set_flashdata('loginmessage', 'The password has beem reset and an email notificatoin should arrive shortly.');
				echo 'The password has beem reset and an email notificatoin should arrive shortly.';
			}
			else{
				$this->session->set_flashdata('loginmessage', 'It was not possible to send the email notification.');
				echo 'It was not possible to send the email notification.';
			}
			redirect('login');
		}
		$this->load->view('user/forgotpassword');
	}

	function finduser(){
		$this->load->model('Model_user', '', TRUE);
		$email_address = $this->input->post('emailaddress');
		$user          = $this->Model_user->findUserEmailAdress($email_address);
		if(!$user){
			$this->form_validation->set_message('finduser', 'Email address not found');
			return FALSE;
		}
		return TRUE;
	}

	function logout()
	{
		//log out sales team user
		$this->load->helper('cookie');
		delete_cookie("user_logged_role");
		$this->session->unset_userdata('user_logged_id');
		$this->session->unset_userdata('user_logged_role');
		$this->session->unset_userdata('user_logged_username');
		redirect('login');
	}

}
?>