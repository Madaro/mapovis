<?php
if( ! defined('BASEPATH')) exit('No direct script access allowed');
class apihouseandland extends CI_Controller {

	function __construct(){
		parent::__construct(); 
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_house', '', TRUE);
	}

	function index($developmentkey = '')
	{
		$form_data = $this->input->post();
		if(!$form_data){
			$form_data = array();
		}
		$data      = array(
			'form_data'         => $form_data,
			'developmentkey'    => $developmentkey,
			'development_id'    => 8,
		);
		$this->load->view('houseandland/index', $data);
	}

	function houseandland($development_id, $developmentkey = '')
	{
		$form_data   = $this->input->get();
		if(!$form_data){
			$form_data = array();
		}
		$development = $this->Model_development->getDevelopment($development_id);
		// validate if the access key is valid
		if(!$this->Model_development->validateDevelomentKey($development_id, $developmentkey)){
			$lots          = array();
			$houses        = array();
			$houses_images = array();
		}
		else{
			$results       = $this->Model_lot->getHouseAndLandSearchResults($development_id, $form_data);
			$lots          = $results['lots'];
			$houses        = $results['houses'];
			$houses_images = $results['houses_images'];
		}
		$data        = array(
			'houses'            => $houses,
			'houses_images'     => $houses_images,
			'lots'              => $lots,
			'development'       => $development,
			'search_parameters' => $this->getSearchParameters($development_id),
			'form_data'         => $form_data
		);
		$page_view = $this->Model_development->getHouseAndLandView($development->house_and_land_view);
		$this->load->view($page_view, $data);
	}

	function houseandlandbyhs($development_id, $developmentkey = '')
	{
		$this->load->library('mapovis_lib');
		$this->load->model('Model_stage', '', TRUE);
		$form_data   = $this->input->get();
		if(!$form_data){
			$form_data = array();
		}
		$development   = $this->Model_development->getDevelopment($development_id);
		$stages        = $this->Model_stage->getStagesActiveLots($development_id);
		$stages_format = array();
		foreach($stages as $stage){
			$stage->word_of_number           = $this->mapovis_lib->convert_number_to_words($stage->stage_number);
			$stages_format[$stage->stage_id] = $stage;
		}
		// validate if the access key is valid
		if(!$this->Model_development->validateDevelomentKey($development_id, $developmentkey)){
			$lots          = array();
			$houses        = array();
			$houses_images = array();
		}
		else{
			$results       = $this->Model_lot->getHouseAndLandSearchResults($development_id, $form_data, FALSE, array_keys($stages_format));
			$lots          = $results['lots'];
			$houses        = $results['houses'];
			$houses_images = $results['houses_images'];
		}
		$data        = array(
			'houses'            => $houses,
			'houses_images'     => $houses_images,
			'lots'              => $lots,
			'development'       => $development,
			'search_parameters' => $this->getSearchParameters($development_id),
			'stages'            => $stages_format,
			'form_data'         => $form_data
		);
		$page_view = $this->Model_development->getHouseAndLandView($development->house_and_land_view);
		$this->load->view($page_view, $data);
	}

	function houseandlandbylots($development_id, $developmentkey = '')
	{
		$this->load->library('mapovis_lib');
		$this->load->model('Model_stage', '', TRUE);
		$form_data   = $this->input->get();
		if(!$form_data){
			$form_data = array();
		}
		$development   = $this->Model_development->getDevelopment($development_id);
		// validate if the access key is valid
		if(!$this->Model_development->validateDevelomentKey($development_id, $developmentkey)){
			$lots          = array();
			$houses_images = array();
			$stages_format = array();
		}
		else{
			$stages            = $this->Model_stage->getStagesActiveLots($development_id);
			$stages_format     = array();
			foreach($stages as $stage){
				$stage->word_of_number           = $this->mapovis_lib->convert_number_to_words($stage->stage_number);
				$stages_format[$stage->stage_id] = $stage;
			}
			$results       = $this->Model_lot->getHouseAndLandSearchResultsActiveLots($development_id, $form_data);
			$lots          = $results['lots'];
			$houses_images = $results['houses_images'];
		}
		$data        = array(
			'houses_images'     => $houses_images,
			'lots'              => $lots,
			'development'       => $development,
			'stages'            => $stages_format,
			'search_parameters' => $this->getSearchParameters($development_id),
			'form_data'         => $form_data
		);
		$page_view = $this->Model_development->getHouseAndLandView($development->house_and_land_view);
		$this->load->view($page_view, $data);
	}

	function houseandland_full($development_id, $developmentkey = '')
	{
		$form_data         = $this->input->post();
		if(!$form_data){
			$form_data = array();
		}
		$development       = $this->Model_development->getDevelopment($development_id);
		$search_parameters = $this->Model_house->houseLandSearchParameters($development_id);
		// validate if the access key is valid
		if(!$this->Model_development->validateDevelomentKey($development_id, $developmentkey)){
			$lots          = array();
			$houses        = array();
			$houses_images = array();
		}
		else{
			$results       = $this->Model_lot->getHouseAndLandSearchResults($development_id, $form_data);
			$lots          = $results['lots'];
			$houses        = $results['houses'];
			$houses_images = $results['houses_images'];
		}
		$data        = array(
			'houses'            => $houses,
			'houses_images'     => $houses_images,
			'lots'              => $lots,
			'development'       => $development,
			'developmentkey'    => $developmentkey,
			'search_parameters' => $search_parameters,
			'form_data'         => $form_data,
			'page_view'         => $this->Model_development->getHouseAndLandView($development->house_and_land_view)
		);
		$this->load->view('houseandland/houseandland-full', $data);
	}

	function template($development_id, $developmentkey = '')
	{
		$this->load->library('mapovis_lib');
		$this->load->model('Model_stage', '', TRUE);
		$form_data         = $this->input->get();
		if(!$form_data){
			$form_data = array();
		}
		$development       = $this->Model_development->getDevelopment($development_id);
		$stages            = $this->Model_stage->getStagesActiveLots($development_id);
		$stages_format     = array();
		foreach($stages as $stage){
			$stage->word_of_number           = $this->mapovis_lib->convert_number_to_words($stage->stage_number);
			$stages_format[$stage->stage_id] = $stage;
		}
		$search_parameters = $this->getSearchParameters($development_id);
		$data              = array(
			'development'       => $development,
			'developmentkey'    => $developmentkey,
			'search_parameters' => $search_parameters,
			'form_data'         => $form_data,
			'stages'            => $stages_format,
		);
		$page_view = $this->Model_development->getTemplateView($development->template_view);
		$this->load->view($page_view, $data);
	}

	private function getSearchParameters($development_id){
		$hnl_search_parameters  = $this->Model_house->houseLandSearchParameters($development_id);
		$land_search_parameters = $this->Model_lot->landSearchParameters($development_id);
		return array_merge($hnl_search_parameters, $land_search_parameters);
	}

}
?>