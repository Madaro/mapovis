<?php
if( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('salesteammain_controller.php');

class Salesteam extends SalesTeamMain_Controller {

	function __construct() 
	{
		parent::__construct(); 
		if(!$this->session->userdata('user_logged_id'))
		{
			redirect('login');
			exit;
		}
		$this->load->model('Model_user', '', TRUE);
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);
	}

	function index()
	{
		redirect('salesteam/dashboard');
	}

	function dashboard()
	{
		$this->load->model('Model_config', '', TRUE);
		$this->load->model('Model_house_lot_package_approval', '', TRUE);

		$current_user     = $this->Model_user->getUserLogged();
		$my_developments  = $this->Model_development->getMyDevelopments($current_user->user_id);
		$developments_w_s = $this->scheduleForDevelopments($my_developments);
		$welcome_message  = $this->Model_config->getWelcomeMessage($current_user);
		$pending_packages = (count($developments_w_s))? $this->Model_house_lot_package_approval->getDevelopmentsTotalPackages(array_keys($developments_w_s)): array();
		$data             = array(
			'current_user'    => $current_user,
			'my_developments' => $developments_w_s,
			'pending_packages'=> $pending_packages,
			'welcome_message' => $welcome_message,
		);
		$this->load_template('user/view_dashboard', $data);
	}

	function mydevelopments()
	{
		$this->load->model('Model_house_lot_package_approval', '', TRUE);
		$current_user     = $this->Model_user->getUserLogged();
		$my_developments  = $this->Model_development->getMyDevelopments($current_user->user_id);
		$developments_w_s = $this->scheduleForDevelopments($my_developments);
		$pending_packages = (count($developments_w_s))? $this->Model_house_lot_package_approval->getDevelopmentsTotalPackages(array_keys($developments_w_s)): array();
		$data             = array(
			'current_user'    => $current_user,
			'my_developments' => $developments_w_s,
			'pending_packages'=> $pending_packages,
		);
		$this->load_template('user/view_mydevelopments', $data);
	}

	function updatedevelopmentid($development_id)
	{
		$tmpdevelopment = $this->getDevelopmentAndValidate($development_id);
		$development    = $this->scheduleForDevelopment($tmpdevelopment);

		$lots        = $this->Model_lot->getLots($development->development_id);
		$lot_status  = $this->Model_lot->getStatus();
		$data        = array(
			'development'  => $development,
			'lots'         => $lots,
			'lot_status'   => $lot_status
		);
		$footer = array('user/footer_standard', 'user/view_updatedevelopmentid_customjs');
		$this->load_template('user/view_updatedevelopmentid', $data, $footer);
	}

	function viewlot($lot_id)
	{
		$lot         = $this->Model_lot->getLot($lot_id);
		$development = $this->getDevelopmentAndValidate($lot->development_id);

		$lot_status  = $this->Model_lot->getStatus();
		$data        = array(
			'development' => $development,
			'lot'         => $lot,
			'lot_status'  => $lot_status
		);
		$this->load_ajaxtemplate('user/view_viewlot', $data);
	}

	function updatelot($lot_id)
	{
		$this->load->model('Model_lot', '', TRUE);
		$lot       = $this->Model_lot->getLot($lot_id);
		$form_data = $this->input->post();
		if($this->Model_lot->updateBasicLot($lot_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The lot was updated successfully. Refresh the MAPOVIS master plan to view the changes.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the lot, please make sure the data is correct.');
		}
		redirect('salesteam/updatedevelopmentid/'.$lot->development_id);
	}

	function noupdates($development_id)
	{
		$this->Model_development->updateLastUpdated($development_id);
		$this->session->set_flashdata('alertmessage', 'No updates set for this development.');
		redirect('salesteam/updatedevelopmentid/'.$development_id);
	}

	function changelog()
	{
		$this->load->model('Model_change_log', '', TRUE);

		$current_user    = $this->Model_user->getUserLogged();
		$my_developments = $this->Model_development->getMyDevelopments($current_user->user_id, TRUE);
		$change_logs     = $this->Model_change_log->getChangeLogs(array('development_ids' => array_keys($my_developments)));
		$data            = array(
			'change_logs'  => $change_logs
		);
		$this->load_template('user/view_changelog', $data);
	}

	function hnlchangelog()
	{
		$this->load->model('Model_hnl_change_log', '', TRUE);

		$current_user    = $this->Model_user->getUserLogged();
		$my_developments = $this->Model_development->getMyDevelopments($current_user->user_id, TRUE);
		$change_logs     = $this->Model_hnl_change_log->getHnLChangeLogs(array('development_ids' => array_keys($my_developments)));
		$data            = array(
			'change_logs'  => $change_logs
		);
		$this->load_template('user/view_hnlchangelog', $data);
	}

	function availablelots()
	{
		$current_user    = $this->Model_user->getUserLogged();
		$my_developments = $this->Model_development->getMyDevelopments($current_user->user_id);
		$data            = array(
			'my_developments' => $my_developments,
		);
		$this->load_template('user/view_availablelots', $data);
	}

	function availablelotsreport($development_id)
	{
		$this->load->model('Model_availablelot', '', TRUE);
		$development            = $this->getDevelopmentAndValidate($development_id);
		$available_lots         = $this->Model_availablelot->getAvailableLots($development_id, 3);
		$month3                 = date('Y-m-01', strtotime('-3 months', time()));
		$month6                 = date('Y-m-01', strtotime('-6 months', time()));
		$current_available_lots = $this->Model_availablelot->getTotalLots($development_id, $month3, $month6);
		$formated_labels        = array('Current');
		$formated_results       = array(
			'greaterthan90days'   => array($current_available_lots['lots_lessthan3mths']),
			'between90and180days' => array($current_available_lots['lots_btw3and6mths']),
			'greaterthan180days'  => array($current_available_lots['lots_morethan6mths']),
			'totals'              => array($current_available_lots['lots_lessthan3mths'] + $current_available_lots['lots_btw3and6mths'] + $current_available_lots['lots_morethan6mths']),
		);
		foreach($available_lots as $available_lot_data){
			$formated_labels[]                         = date('jS \o\f F Y', strtotime($available_lot_data->available_date));
			$formated_results['greaterthan90days'][]   = $available_lot_data->lots_lessthan3mths;
			$formated_results['between90and180days'][] = $available_lot_data->lots_btw3and6mths;
			$formated_results['greaterthan180days'][]  = $available_lot_data->lots_morethan6mths;
			$formated_results['totals'][]              = $available_lot_data->lots_lessthan3mths+$available_lot_data->lots_btw3and6mths+$available_lot_data->lots_morethan6mths;
		}

		$data = array(
			'development'      => $development,
			'formated_labels'  => $formated_labels,
			'formated_results' => $formated_results
		);
		$this->load_template('user/view_availablelotsreport', $data);
	}

	function contactus()
	{
		$this->load->model('Model_config', '', TRUE);

		$settings      = $this->Model_config->getConfig();
		$current_user  = $this->Model_user->getUserLogged();
		$data          = array(
			'current_user' => $current_user,
			'settings'     => $settings,
		);
		$this->load_template('user/view_contactus', $data);
	} 

	function manageaccount()
	{
		$this->load->library('mapovis_lib');
		$current_user = $this->Model_user->getUserLogged();
		$user_id      = $current_user->user_id;

		//This method will have the credentials validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_check_user_details');

		if($this->form_validation->run() != FALSE){
			$form_data = $this->input->post();
			$this->Model_user->updateUser($user_id, $form_data);
			$this->session->set_flashdata('alertmessage', 'Your account has been updated successfully.');
			redirect('salesteam/manageaccount');
		}
		$data         = array(
			'user'            => $current_user,
			'my_developments' => $this->Model_development->getMyDevelopments($current_user->user_id, true, true),
		);
		$this->load_template('user/view_manageaccount', $data);
	}

	function check_user_details()
	{
		$user_id          = $this->session->userdata('user_logged_id');
		$email_addres     = $this->input->post('email');
		$password         = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		$valid            = TRUE;
		$msg              = '';
		if(!$this->Model_user->isUnique($email_addres, $user_id)){
			$msg  = $msg.'The email address already exists in the system. ';
			$valid = FALSE;
		}

		if(!empty($password) && !empty($confirm_password)){
			if($password !== $confirm_password){
				$msg = $msg.'Invalid password please try again.';
				$valid = FALSE;
			}
		}
		$this->form_validation->set_message('check_user_details', $msg);
		return $valid;
	}

	private function scheduleForDevelopments($developments)
	{
		$this->load->model('Model_schedule', '', TRUE);
		$this->load->library('mapovis_lib');

		$tmp_results = array();
		foreach($developments as $development){
			$tmp_results[$development->development_id] = $this->scheduleForDevelopment($development);
		}
		return $tmp_results;
	}

	private function scheduleForDevelopment($development)
	{
		$this->load->model('Model_schedule', '', TRUE);
		$this->load->library('mapovis_lib');

		$today                           = $this->mapovis_lib->getToday($development->state);
		$development->next_schedule      = $this->Model_schedule->getNextSchedule($development);
		$development->next_schedule_date = '-';
		$development->remaining_time     = '-';
		$development->overdue            = FALSE;
		$development->remaining_class    = '';
		$development->remaining_hours    = FALSE;

		if($development->next_schedule){
			$remaining                       = date_diff($today, $development->next_schedule);
			$remaining_hours                 = $remaining->days * 24 + $remaining->h;
			$development->next_schedule_date = date_format($development->next_schedule, 'l jS \o\f M, \b\y h:ia');
			$development->remaining_time     = $this->mapovis_lib->timeDiffFormat($remaining);
			$development->remaining_hours    = $remaining_hours;
			if($remaining->invert == 1){
				$development->overdue         = TRUE;
				$development->remaining_class = 'overdue';
				$development->remaining_time  = 'OVERDUE BY '.$development->remaining_time;
			}
			elseif($remaining_hours <= 8){
				$development->remaining_class = 'upcoming';
			}
		}
		return $development;
	}

	function managedevelopmentid($development_id)
	{
		$this->load->model('Model_house_lot_package_approval', '', TRUE);
		$development  = $this->getDevelopmentAndValidate($development_id);
		$media_agency = $this->Model_development->isMediaAGency($development_id);
		$manager      = $this->Model_development->isManager($development_id);
		$pending_packages = $this->Model_house_lot_package_approval->getDevelopmentsTotalPackages(array($development->development_id));
		$data         = array(
			'development'  => $development,
			'media_agency' => $media_agency,
			'manager'      => $manager,
			'pending_packages' => $pending_packages,
		);
		$this->load_template('user/view_managedevelopmentid', $data);
	}

	function editdevelopment($development_id)
	{
		$development  = $this->getDevelopmentAndValidate($development_id);
		$media_agency = $this->Model_development->isMediaAGency($development_id);

		$form_data    = $this->input->post();
		if($form_data)
		{
			if($this->Model_development->updateOfficeDevelopment($development_id, $form_data, $media_agency)){
				$this->session->set_flashdata('alertmessage', 'The development has been updated successfully.');
				//Go to Manage area
				redirect('salesteam/managedevelopmentid/'.$development_id);
			}
		}
		$data = array(
			'development'             => $development,
			'house_match_sort_option' => $this->Model_development->houseMatchSortByOptions(),
			'media_agency'            => $media_agency
		);
		$this->load_template('user/view_editdevelopment', $data);
	}

	function editsalesoffice($development_id)
	{
		$development  = $this->getDevelopmentAndValidate($development_id);
		$media_agency = $this->Model_development->isMediaAGency($development_id);
		$form_data    = $this->input->post();
		if($form_data)
		{
			if($this->Model_development->updateOfficeDevelopment($development_id, $form_data, $media_agency)){
				$this->session->set_flashdata('alertmessage', 'The development has been updated successfully.');
				//Go to Manage area
				redirect('salesteam/managedevelopmentid/'.$development_id);
			}
		}
		if($media_agency){
			redirect('salesteam/editdevelopment/'.$development_id);
		}
		$data = array(
			'development'             => $development,
		);
		$this->load_template('user/view_editsalesoffice', $data);
	}

	function managehouses($development_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_builder', '', TRUE);

		$development = $this->getDevelopmentAndValidate($development_id);
		$hide_global = ($development->hide_global_houses == 0);
		$houses      = $this->Model_house->getHouses($development_id, $hide_global);
		$builders    = $this->Model_builder->getBuilders();
		$data        = array(
			'houses'      => $houses,
			'builders'    => $builders,
			'development' => $development
		);

		$footer = array('user/footer_standard', 'user/view_houses_customjs', 'user/view_managehouses_customjs');
		$this->load_template('user/view_managehouses', $data, $footer);
	}

	function viewhouse($house_id, $development_id = null)
	{
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_builder', '', TRUE);
		$this->load->model('Model_house_image', '', TRUE);

		$house     = $this->Model_house->getHouse($house_id);
		$builders  = $this->Model_builder->getBuilders();
		$data      = array(
			'house_id'     => $house_id,
			'house'        => $house,
			'builders'     => $builders,
			'house_images' => $this->Model_house_image->getHouseImages($house_id, array('file_type' => 'facade')),
			'floor_plans'  => $this->Model_house_image->getHouseImages($house_id, array('file_type' => 'floor_plan')),
			'gallery'      => $this->Model_house_image->getHouseImages($house_id, array('file_type' => 'gallery')),
			'development_id'=> $development_id,
		);
		$view = ($house->development_id)? 'view_edithouse': 'view_viewhouse';
		$this->load_ajaxtemplate('user/'.$view, $data);
	}

	function updateglobalhouseimages($house_id, $development_id)
	{
		$this->load->model('Model_house_image', '', TRUE);
		$form_data = $this->input->post();
		if($form_data != FALSE){
			if($this->Model_house_image->updateGlobalHouseImages($house_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The facade images were uploaded successfully.');
			}
			else{
				$this->session->set_flashdata('errormessage', 'It was not possible to upload the facade image(s), please make sure the data is correct.');
			}
		}
		else{
			$this->session->set_flashdata('errormessage', 'No facade images were uploaded, please try again.');
		}
		redirect('salesteam/managehouses/'.$development_id);
	}

	function updatehouse($house_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$house     = $this->Model_house->getHouse($house_id);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('house_name', 'House Name', 'trim|required|xss_clean');

		if($this->form_validation->run() != FALSE){
			$form_data = $this->input->post();
			if($this->Model_house->updateHouse($house_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The house was updated successfully.');
			}
			else{
				$this->session->set_flashdata('errormessage', 'It was not possible to update the house, please make sure the data is correct.');
			}
		}
		else{
			$this->session->set_flashdata('errormessage', 'The house name was not provided, please try again.');
		}
		redirect('salesteam/managehouses/'.$house->development_id);
	}

	function requesthousechange($house_id, $development_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$house     = $this->Model_house->getHouse($house_id);
		$data      = array(
			'house_id'       => $house_id,
			'house'          => $house,
			'development_id' => $development_id
		);
		$this->load_ajaxtemplate('user/view_requesthousechange', $data);
	}

	function submitchangerequest($house_id, $development_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('request_message', 'request_message', 'trim|required|xss_clean');
		if($this->form_validation->run() != FALSE){
			$form_data = $this->input->post();
			if($this->Model_house->requestChange($house_id, $development_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The change request was submitted successfully.');
			}
			else{
				$this->session->set_flashdata('errormessage', 'It was not possible to submit your change request, please make sure you provide a message.');
			}
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to submit your change request, please make sure you provide a message.');
		}
		redirect('salesteam/managehouses/'.$development_id);
	}

	function validatehousename($development_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$house_name = $this->input->post('house_name');
		$house_id   = $this->input->post('house_id');
		$builder_id = $this->input->post('builder_id');
		$housematch = $this->Model_house->validateDuplicate($house_name, $builder_id, $development_id, $house_id);
		if(!$housematch){
			$result = true;
		}
		else{
			if($housematch->development_id == null){
				$result = 'A house with this name already exists in the MAPOVIS directory.';
			}
			else{
				$result = 'A custom house with this name is already linked to this development.';
			}
		}
		echo json_encode($result);
		exit();
	}

	function uploadoriginalimage()
	{
		$this->load->model('Model_house_image', '', TRUE);
		$result = $this->Model_house_image->uploadoriginalimage();
		echo json_encode($result);
		exit();
	}

	function deletehouseimage()
	{
		$this->load->model('Model_house_image', '', TRUE);
		$house_image_id   = $this->input->post('house_image_id');

		if($this->Model_house_image->deleteHouseImage($house_image_id)){
			$result = array('status' => 1, 'msg'=> 'The image was deleted successfully.');
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to delete the image, please try again.');
		}
		echo json_encode($result);
		exit();
	}

	function deletehouse($house_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$house = $this->Model_house->getHouse($house_id);

		if($house->development_id && $this->Model_house->deleteHouse($house_id)){
			$this->session->set_flashdata('alertmessage', 'The house was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the house.');
		}
		redirect('salesteam/managehouses/'.$house->development_id);
	}

	function addhouse($development_id, $builder_id = NULL)
	{
		$development = $this->getDevelopmentAndValidate($development_id);

		$this->load->model('Model_builder', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);
		$this->load->model('Model_house', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('house_name', 'House Name', 'trim|required|xss_clean');

		if($this->form_validation->run() != FALSE)
		{
			$form_data                   = $this->input->post();
			$form_data['development_id'] = $development_id;
			$redirecto_to                = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? 'addhouse/'.$development_id: 'managehouses/'.$development_id;

			//Field validation success, add precinct
			if($this->Model_house->addHouse($form_data)){
				$this->session->set_flashdata('alertmessage', 'The house was added successfully.');
				redirect('salesteam/'.$redirecto_to);
			}
		}
		$builders    = $this->Model_builder->getBuilders();
		$stages      = $this->Model_stage->getStages($development_id);
		$data        = array('development' => $development, 'stages' => $stages, 'builders' => $builders, 'builder_id' => $builder_id);

		$footer      = array('user/footer_standard', 'user/view_houses_customjs');
		$this->load_template('user/view_addhouse', $data, $footer);
	}

	function addbuilder($development_id)
	{
		$development = $this->getDevelopmentAndValidate($development_id);

		$this->load->model('Model_builder', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('builder_name', 'Builder Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('builder_website', 'Builder Website', 'trim|required');

		if($this->form_validation->run() != FALSE)
		{
			//Field validation success, add builder
			$form_data  = $this->input->post();

			if($this->Model_builder->duplicateName($form_data['builder_name'], null) == FALSE){
				$builder_id = $this->Model_builder->addBuilder($form_data);
				if($builder_id){
					$builder = $this->Model_builder->getBuilder($builder_id);
					$result = array('status' => 1, 'msg'=> 'The builder was added successfully.', 'builder_id' => $builder_id, 'builder_name' => $builder->builder_name);
				}
				else{
					$result = array('status' => 0, 'msg'=> 'It was not possible to add the builder please try again.');
				}
			}
			else{
				$result = array('status' => 0, 'msg'=> 'The builder name already exists in the system.');
			}
		}
		else{
			$result = array('status' => 0, 'msg'=> 'Please provide a valid builder name and website.');
		}
		echo json_encode($result);
		exit();
	}

	function viewamenities($development_id)
	{
		$development = $this->getDevelopmentAndValidate($development_id);

		$this->load->model('Model_amenity', '', TRUE);
		$amenities   = $this->Model_amenity->getAmenities($development_id);
		$data        = array(
			'amenities'   => $amenities,
			'development' => $development
		);
		$footer = array('user/footer_standard', 'user/view_amenityimages_customjs', 'user/view_viewamenities_customjs');
		$this->load_template('user/view_viewamenities', $data, $footer);
	}

	function viewamenity($amenity_id)
	{
		$this->load->model('Model_amenity', '', TRUE);
		$amenity      = $this->Model_amenity->getAmenity($amenity_id);
		$media_agency = $this->Model_development->isMediaAGency($amenity->development_id);
		$data    = array(
			'amenity'      => $amenity,
			'media_agency' => $media_agency,
		);
		$this->load_ajaxtemplate('user/view_viewamenity', $data);
	}

	function updateamenity($amenity_id)
	{
		$this->load->model('Model_amenity', '', TRUE);
		$amenity   = $this->Model_amenity->getAmenity($amenity_id);

		$form_data = $this->input->post();
		if($this->Model_amenity->updateAmenitySales($amenity_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The amenity was updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the amenity, please make sure the data is correct.');
		}
		redirect('salesteam/viewamenities/'.$amenity->development_id);
	}

	function deleteamenityimage()
	{
		$this->load->model('Model_amenity', '', TRUE);
		$amenity_id = $this->input->post('amenityid');
		$image_name = $this->input->post('imagename');

		if($this->Model_amenity->deleteAmenityImage($amenity_id, $image_name)){
			$result = array('status' => 1, 'msg'=> 'The image was deleted successfully.');
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to delete the image, please try again.');
		}
		echo json_encode($result);
		exit();
	}

	function viewexternalamenities($development_id)
	{
		$development = $this->getDevelopmentAndValidate($development_id);

		$this->load->model('Model_external_amenity', '', TRUE);
		$e_amenities = $this->Model_external_amenity->getAmenities($development_id);
		$data        = array(
			'external_amenities' => $e_amenities,
			'development'        => $development
		);
		$footer = array('user/footer_standard', 'user/view_amenityimages_customjs', 'user/view_viewexternalamenities_customjs');
		$this->load_template('user/view_viewexternalamenities', $data, $footer);
	}

	function viewexternalamenity($external_amenity_id)
	{
		$this->load->model('Model_external_amenity', '', TRUE);
		$e_amenity    = $this->Model_external_amenity->getAmenity($external_amenity_id);
		$media_agency = $this->Model_development->isMediaAGency($e_amenity->development_id);
		$data      = array(
			'external_amenity' => $e_amenity,
			'media_agency'     => $media_agency,
		);
		$this->load_ajaxtemplate('user/view_viewexternalamenity', $data);
	}

	function updateexternalamenity($external_amenity_id)
	{
		$this->load->model('Model_external_amenity', '', TRUE);
		$external_amenity   = $this->Model_external_amenity->getAmenity($external_amenity_id);

		$form_data = $this->input->post();
		if($this->Model_external_amenity->updateAmenitySales($external_amenity_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The external amenity was updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the externalamenity, please make sure the data is correct.');
		}
		redirect('salesteam/viewexternalamenities/'.$external_amenity->development_id);
	}

	function deleteexternalamenityimage()
	{
		$this->load->model('Model_external_amenity', '', TRUE);
		$amenity_id = $this->input->post('amenityid');
		$image_name = $this->input->post('imagename');

		if($this->Model_external_amenity->deleteAmenityImage($amenity_id, $image_name)){
			$result = array('status' => 1, 'msg'=> 'The image was deleted successfully.');
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to delete the image, please try again.');
		}
		echo json_encode($result);
		exit();
	}

	function managehousematches($development_id)
	{
		$data        = array(
			'development' => $this->getDevelopmentAndValidate($development_id),
			'lots'        => $this->Model_lot->getLotsCountMatches($development_id),
		);
		$this->load_template('user/view_managehousematches', $data);
	}

	public function editlothouses($lot_id)
	{
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_house_lot_package', '', TRUE);

		$lot           = $this->Model_lot->getLot($lot_id);
		$development   = $this->getDevelopmentAndValidate($lot->development_id);
		$hide_global   = ($development->hide_global_houses == 0);
		$data          = array(
			'development'        => $development,
			'lot'                => $lot,
			'houses'             => $this->Model_house->getHouses($development->development_id, $hide_global),
			'lot_house_packages' => $this->Model_house_lot_package->getHouseLotPackages($lot_id)

		);
		$footer = array('user/footer_standard', 'user/view_editlothouses_customjs');
		$this->load_template('user/view_editlothouses', $data, $footer);
	}

	function edithouselotpackage($lot_id, $house_id, $ajax = TRUE)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_house_lot_order_image', '', TRUE);
		$this->load->model('Model_builder_sales_person', '', TRUE);

		$lot               = $this->Model_lot->getLot($lot_id);
		$house             = $this->Model_house->getHouse($house_id);
		$house_lot_package = $this->Model_house_lot_package->getHouseLotPackage($lot_id, $house_id);
		$sales_people      = $this->Model_builder_sales_person->getSalesPeople($house->house_builder_id);
		$data              = array(
			'house'             => $house,
			'lot'               => $lot,
			'house_lot_package' => $house_lot_package,
			'sales_people'      => $sales_people,
			'house_images'      => (!$house->development_id)?$this->Model_house_lot_order_image->getHouseLotImages($house_id, $lot_id): array(),
			'ajax'              => $ajax,
		);
		$this->load_ajaxtemplate('user/view_edithouselotpackage', $data);
	}

	function updatehouselotpackage($lot_id, $house_id, $ajax = TRUE)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);
		$this->load->model('Model_house_lot_order_image', '', TRUE);

		$form_data           = $this->input->post();
		$form_data['active'] = 1;
		$current_user        = $this->Model_user->getUserLogged();
		if($this->Model_house_lot_package->updateHouseLot($lot_id, $house_id, $form_data, $current_user)){
			if(isset($form_data['facade_images'])){
				$this->Model_house_lot_order_image->updatePackageImagesOrder($house_id, $lot_id, $form_data);
			}
			$house_lot_package                           = $this->Model_house_lot_package->getHouseLotPackage($lot_id, $house_id);
			$house_lot_package->formated_house_lot_price = number_format($house_lot_package->house_lot_price);
			$result                                      = array('status' => 1, 'msg'=> 'The House and Land package has been udpated.','obj' => $house_lot_package);
		}
		else{
			$result            = array('status' => 0, 'msg'=> 'It was not possible to update the House and Land package. Please check the details are correct.', 'obj' => FALSE);
		}
		if($ajax){
			echo json_encode($result);
			exit();
		}
		else{
			$this->load->model('Model_lot', '', TRUE);
			$lot          = $this->Model_lot->getLot($lot_id);
			$message_type = ($result['status'] == 0)? 'errormessage': 'alertmessage';
			$this->session->set_flashdata($message_type, $result['msg']);
			redirect('salesteam/listpackages/'.$lot->development_id);
		}
	}

	function enablehouselotpackage($lot_id, $house_id)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);
		$this->load->model('Model_user', '', TRUE);
		
		$current_user      = $this->Model_user->getUserLogged();
		$house_lot_package = $this->Model_house_lot_package->getHouseLotPackage($lot_id, $house_id);
		if(empty($house_lot_package->house_lot_price) || empty($house_lot_package->file_name)){
			$result = array('status' => 2, 'msg'=> 'Please fill the house and land data.', 'obj' => FALSE);
		}
		elseif($this->Model_house_lot_package->enableDisableHouseLotPdf($lot_id, $house_id, TRUE, $current_user)){
			$house_lot_package                           = $this->Model_house_lot_package->getHouseLotPackage($lot_id, $house_id);
			$house_lot_package->formated_house_lot_price = number_format($house_lot_package->house_lot_price);
			$result = array('status' => 1, 'msg'=> 'The House has been enabled.', 'obj' => $house_lot_package);
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to enable the house.', 'obj' => FALSE);
		}
		echo json_encode($result);
		exit();
	}

	function disablehouselotpackage($lot_id, $house_id, $ajax = TRUE)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);
		$this->load->model('Model_user', '', TRUE);
		
		$current_user     = $this->Model_user->getUserLogged();
		if($this->Model_house_lot_package->enableDisableHouseLotPdf($lot_id, $house_id, FALSE, $current_user)){
			$house_lot_package                           = $this->Model_house_lot_package->getHouseLotPackage($lot_id, $house_id);
			$house_lot_package->formated_house_lot_price = number_format($house_lot_package->house_lot_price);
			$house_lot_package->house_type               = ($house_lot_package->development_id)? 'Custom House': 'Global House';
			$result = array('status' => 1, 'msg'=> 'The House has been disabled.', 'obj' => $house_lot_package);
		}
		else{
			$result = array('status' => 0, 'msg'=> 'It was not possible to disable the house.', 'obj' => FALSE);
		}
		if($ajax){
			echo json_encode($result);
			exit();
		}
		else{
			$this->load->model('Model_lot', '', TRUE);
			$lot          = $this->Model_lot->getLot($lot_id);
			$message_type = ($result['status'] == 0)? 'errormessage': 'alertmessage';
			$this->session->set_flashdata($message_type, $result['msg']);
			redirect('salesteam/listpackages/'.$lot->development_id);
		}
	}

	function listpackages($development_id)
	{
		$this->load->model('Model_house_lot_package', '', TRUE);
		$development  = $this->getDevelopmentAndValidate($development_id);
		$data          = array(
			'development'        => $development,
			'lot_house_packages' => $this->Model_house_lot_package->getHouseLotPackagesByDevelopment($development_id)
		);
		$this->load_template('user/view_listlothouses', $data);
	}

	public function packagesforapproval($development_id)
	{
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_house', '', TRUE);
		$this->load->model('Model_house_lot_package_approval', '', TRUE);
		$development  = $this->getDevelopmentAndValidate($development_id);
		$packages_for_approval = $this->Model_house_lot_package_approval->getDevelopmentPackages($development_id, array('status' => 'Pending'));
		$data    = array(
			'development'           => $development,
			'packages_for_approval' => $packages_for_approval,
		);
		$this->load_template('user/view_houselandpackageapproval', $data);
	}

	function approvehouselotpackage($development_id, $package_id)
	{
		$this->load->model('Model_house_lot_package_approval', '', TRUE);
		$current_user      = $this->Model_user->getUserLogged();
		$pending_package   = $this->Model_house_lot_package_approval->getHouseLotPackageById($package_id);
		$this->getDevelopmentAndValidate($pending_package->development_id);
		if($pending_package->status != 'Pending'){
			$this->session->set_flashdata('errormessage', 'The package has been previously '.$pending_package->status);
		}
		elseif($this->Model_house_lot_package_approval->approvePackage($package_id, $current_user)){
			$this->session->set_flashdata('alertmessage', 'The House and Land Package has been Approved.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to approve the House and Land Package.');
		}
		redirect('salesteam/packagesforapproval/'.$development_id);
	}

	function rejecthouselotpackage($development_id, $package_id)
	{
		$this->load->model('Model_house_lot_package_approval', '', TRUE);
		$current_user      = $this->Model_user->getUserLogged();
		$pending_package   = $this->Model_house_lot_package_approval->getHouseLotPackageById($package_id);
		$this->getDevelopmentAndValidate($pending_package->development_id);
		if($pending_package->status != 'Pending'){
			$this->session->set_flashdata('errormessage', 'The package has been previously '.$pending_package->status);
		}
		elseif($this->Model_house_lot_package_approval->rejectPackage($package_id, $current_user)){
			$this->session->set_flashdata('alertmessage', 'The House and Land Package has been Rejected.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to reject the House and Land Package.');
		}
		redirect('salesteam/packagesforapproval/'.$development_id);
	}

	public function viewstages($development_id)
	{
		$this->load->model('Model_stage', '', TRUE);

		$development  = $this->getDevelopmentAndValidate($development_id);
		$stages       = $this->Model_stage->getStages($development_id);
		$current_user = $this->Model_user->getUserLogged();
		$media_agency = ($development->media_agency == $current_user->user_id);
		$data         = array(
			'stages'      => $stages,
			'media_agency'=> $media_agency,
			'development' => $development
		);
		$this->load_template('user/view_viewstages', $data);
	}

	public function updatestage($stage_id)
	{
		$this->load->model('Model_precinct', '', TRUE);
		$this->load->model('Model_stage', '', TRUE);
		$this->load->library('form_validation');

		$stage        = $this->Model_stage->getStage($stage_id);
		$development  = $this->getDevelopmentAndValidate($stage->development_id);
		$current_user = $this->Model_user->getUserLogged();
		$media_agency = ($development->media_agency == $current_user->user_id);
		$form_data    = $this->input->post();
		if($form_data)
		{
			//Field validation success, update stage
			if($this->Model_stage->updateStage($stage_id, $form_data, !$media_agency)){
				$this->session->set_flashdata('alertmessage', 'The stage was updated successfully.');
				redirect('salesteam/viewstages/'.$stage->development_id);
			}
		}
		$data        = array(
			'development'  => $development,
			'stage'        => $stage,
			'media_agency' => $media_agency,
		);
		$this->load_template('user/view_updatestage', $data);
	}

	function managestagetitles($stage_id)
	{
		$this->load->model('Model_stage', '', TRUE);
		$stage       = $this->Model_stage->getStage($stage_id);
		$data         = array(
			'stage' => $stage,
		);
		$this->load_ajaxtemplate('user/view_managestagetitles', $data);
	}

	function updatestagetitles($stage_id, $titled)
	{
		$this->load->model('Model_stage', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);
		$stage       = $this->Model_stage->getStage($stage_id);

		if($this->Model_lot->updateLotTitleByStage($stage_id, $titled)){
			$this->session->set_flashdata('alertmessage', 'The Stage lots have been updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to udpate the staghe lots.');
		}
		redirect('salesteam/viewstages/'.$stage->development_id);
	}

	public function statisticsalldev()
	{
		$this->statisticsalldevview('statisticsreport', 'MAPOVIS Statistics');
	}

	function statisticsreport($development_id)
	{
		require_once(BASEPATH .'../assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php');
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_statistics_lotview', '', TRUE);
		$this->load->model('Model_statistics_placesearch', '', TRUE);
		$this->load->model('Model_statistics_directionsearch', '', TRUE);

		$development = $this->getDevelopmentAndValidate($development_id);
		$this->validatestatisticsaccess($development_id);

		$this->load->library('form_validation');
		$start_date = date('Y-m-d', strtotime('-30 day'));
		$end_date   = date('Y-m-d 23:59:59', strtotime('-1 day'));
		$form_data  = $this->input->post('dates');
		if($form_data){
			$dates = explode(' to ', $form_data);
			if(strtotime($dates[0])){
				$start_date = date('Y-m-d', strtotime($dates[0]));
			}
			if(strtotime($dates[1])){
				$end_date   = date('Y-m-d 23:59:59', strtotime($dates[1]));
			}
		}
		$start_time     = strtotime($start_date);
		$end_time       = strtotime($end_date);
		$total_days     = ceil(abs(($end_time - $start_time)) / 86400);

		$skip_days      = ceil($total_days/30);
		$reduce_display = ($total_days > 32);
		$counter        = 0;
		for($date = $start_time; $date < $end_time; $date = strtotime('+1 day', $date)){
			if($reduce_display){
				// show weekly dates
				$display_date = ($counter % $skip_days == 0)? date('d/m', $date): '';
			}
			else{
				$display_date = date('d/m', $date);
			}
			$report_dates[date('Y-m-d', $date)] = $display_date;
			$counter++;
		}

		$lot_views_by_lot                 = $this->Model_statistics_lotview->getViews($development_id, $start_date, $end_date);
		$lot_views_by_date                = $this->Model_statistics_lotview->getViewsByDay($development_id, $start_date, $end_date);
		$place_search_views_by_phrase     = $this->Model_statistics_placesearch->getViews($development_id, $start_date, $end_date);
		$place_search_views_by_date       = $this->Model_statistics_placesearch->getViewsByDay($development_id, $start_date, $end_date);
		$direction_search_views_by_phrase = $this->Model_statistics_directionsearch->getViews($development_id, $start_date, $end_date);
		$direction_search_views_by_date   = $this->Model_statistics_directionsearch->getViewsByDay($development_id, $start_date, $end_date);

		$action_type                      = $this->input->post('action_type');
		if($action_type && $action_type != 'submit'){
			$tmp_file_name   = '/tmp/temp_sold_'.time().'.csv';
			$fp              = fopen($tmp_file_name, 'w');
			switch($action_type){
				case 'export_direcsearch':
					$file_name   = 'Directions_Searches';
					$csv_headers = array('Address', 'Percentage', 'Searches');
					fputcsv($fp, $csv_headers);
					
					$direction_views_total    = 0;
					foreach($direction_search_views_by_phrase as $phrase_view){
						$direction_views_total += $phrase_view->views;
					}
					foreach($direction_search_views_by_phrase as $phrase_view){
						$csv_data   = array($phrase_view->direction_address, (($direction_views_total)? round($phrase_view->views / $direction_views_total * 100, 1): 0).'%',$phrase_view->views);
						fputcsv($fp, $csv_data);
					}
					break;
				case 'export_placesearch':
					$file_name   = 'Places_Searches';
					$csv_headers = array('Place', 'Percentage', 'Searches');
					fputcsv($fp, $csv_headers);

					$place_views_total    = 0;
					foreach($place_search_views_by_phrase as $phrase_view){
						$place_views_total += $phrase_view->views;
					}
					foreach($place_search_views_by_phrase as $phrase_view){
						$csv_data   = array($phrase_view->search_phrase, (($place_views_total)? round($phrase_view->views/$place_views_total *100, 1): 0).'%', $phrase_view->views);
						fputcsv($fp, $csv_data);
					}
					break;
				case 'export_lotviews':
				default:
					$file_name   = 'Lot_Views';
					$csv_headers = array('Views', 'Percentage', 'Lot Number', 'Stage', 'Precinct');
					fputcsv($fp, $csv_headers);

					$lot_views_total    = 0;
					foreach($lot_views_by_lot as $lot_view){
						$lot_views_total += $lot_view->views;
					}
					foreach($lot_views_by_lot as $lot_view){
						$csv_data   = array($lot_view->views, (($lot_views_total)? round($lot_view->views / $lot_views_total * 100, 1): 0).'%', $lot_view->lot_number, $lot_view->stage_number, $lot_view->precinct_number);
						fputcsv($fp, $csv_data);
					}
					break;
			}

			fclose($fp);
			$csv_file_content = file_get_contents($tmp_file_name);
			$file_name        = $development->development_name.'_'.$file_name.'_'.date('Y-m-d').'.csv';
			if($csv_file_content){
				header('Content-Type: application/csv');
				header('Content-Disposition: attachement; filename="'.$file_name.'"');
				echo $csv_file_content;
				exit();
			}
		}

		$data = array(
			'development'                      => $development,
			'lot_views_by_lot'                 => $lot_views_by_lot,
			'lot_views_by_date'                => $lot_views_by_date,
			'place_search_views_by_phrase'     => $place_search_views_by_phrase,
			'place_search_views_by_date'       => $place_search_views_by_date,
			'direction_search_views_by_phrase' => $direction_search_views_by_phrase,
			'direction_search_views_by_date'   => $direction_search_views_by_date,
			'report_dates'                     => $report_dates,
			'start_date'                       => $start_date,
			'end_date'                         => $end_date,
			'total_days'                       => $total_days
		);
		$footer = array('user/footer_standard', 'user/view_statisticsreport_customjs');
		$this->load_template('user/view_statisticsreport', $data, $footer);
	}

	public function lotviewsstatisticsalldev()
	{
		$this->statisticsalldevview('lotviewsstatisticsreport', 'Impressions by Lot Frontage');
	}

	function lotviewsstatisticsreport($development_id)
	{
		require_once(BASEPATH .'../assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php');
		$this->load->library('mapovis_lib');
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_statistics_lotview', '', TRUE);
		$this->load->model('Model_statistics_placesearch', '', TRUE);
		$this->load->model('Model_statistics_directionsearch', '', TRUE);
		$this->load->library('form_validation');

		$development = $this->getDevelopmentAndValidate($development_id);
		$this->validatestatisticsaccess($development_id);

		$start_date         = date('Y-m-d', strtotime('-30 weeks'));
		$end_date           = date('Y-m-d 23:59:59', strtotime('last sunday'));
		$lot_views_by_width = $this->Model_statistics_lotview->getWeeklyViews($development_id, $start_date, $end_date);
		$chart_colors       = $this->mapovis_lib->statisticsColorCodes();

		$export_csv         = $this->input->post('export_csv');
		if($export_csv == 1){
			$tmp_file_name   = '/tmp/temp_impressions_'.time().'.csv';
			$fp              = fopen($tmp_file_name, 'w');
			$csv_headers     = array('Week Ending');
			foreach($lot_views_by_width['widths'] as $width){
				$csv_headers[] = $width.'m';
			}
			$csv_headers[] = 'Total';
			fputcsv($fp, $csv_headers);
			foreach($lot_views_by_width['results'] as $week => $week_results){
				$total_lots = 0;
				$csv_data   = array($week);
				foreach($lot_views_by_width['widths'] as $width){
					$lot_views   = (isset($week_results[$width]->number_lots))? (int)$week_results[$width]->number_lots: 0;
					$total_lots += $lot_views;
					$csv_data[]  = $lot_views;
				}
				$csv_data[] = $total_lots;
				fputcsv($fp, $csv_data);
			}
			fclose($fp);
			$csv_file_content = file_get_contents($tmp_file_name);
			$file_name        = $development->development_name.' Impressions by Lot Frontage '.$start_date.' to '.$start_date.'.csv';
			if($csv_file_content){
				header('Content-Type: application/csv');
				header('Content-Disposition: attachement; filename="'.$file_name.'"');
				echo $csv_file_content;
				exit();
			}
		}
		$data = array(
			'development'        => $development,
			'lot_views_by_width' => $lot_views_by_width,
			'chart_colors'       => $chart_colors,
		);
		$footer = array('user/footer_standard', 'user/view_statisticsreport_customjs');
		$this->load_template('user/view_statisticslotviewsreport', $data, $footer);
	}

	public function availlotstatisticsalldev()
	{
		$this->statisticsalldevview('availlotstatisticsreport', 'Available Lots On Hand by Frontage');
	}

	function availlotstatisticsreport($development_id)
	{
		require_once(BASEPATH .'../assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php');
		$this->load->library('mapovis_lib');
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_availablelot_by_width', '', TRUE);
		$development = $this->getDevelopmentAndValidate($development_id);
		$this->validatestatisticsaccess($development_id);

		$available_lots_by_width = $this->Model_availablelot_by_width->getAvailableLotsFormatted($development_id);
		$export_csv              = $this->input->post('export_csv');
		if($export_csv == 1){
			$tmp_file_name   = '/tmp/temp_available_'.time().'.csv';
			$fp              = fopen($tmp_file_name, 'w');
			$csv_headers     = array('Month');
			foreach($available_lots_by_width['widths'] as $width){
				$csv_headers[] = $width.'m';
			}
			$csv_headers[] = 'Total';
			fputcsv($fp, $csv_headers);

			foreach($available_lots_by_width['results'] as $month => $month_results){
				$total_lots = 0;
				$csv_data   = array(date('M Y', strtotime('-1 month', strtotime($month))));
				foreach($available_lots_by_width['widths'] as $width){
					$lot_views   = (isset($month_results[$width]->number_lots))? (int)$month_results[$width]->number_lots: 0;
					$total_lots += $lot_views;
					$csv_data[]  = $lot_views;
				}
				$csv_data[] = $total_lots;
				fputcsv($fp, $csv_data);
			}
			fclose($fp);
			$csv_file_content = file_get_contents($tmp_file_name);
			$file_name        = $development->development_name.'  Available Lots On Hand by Frontage'.date('Y-m-d').'.csv';
			if($csv_file_content){
				header('Content-Type: application/csv');
				header('Content-Disposition: attachement; filename="'.$file_name.'"');
				echo $csv_file_content;
				exit();
			}
		}

		$data        = array(
			'development'             => $development,
			'available_lots_by_width' => $available_lots_by_width,
			'chart_colors'            => $this->mapovis_lib->statisticsColorCodes(),
		);
		$footer = array('user/footer_standard', 'user/view_statisticsreport_customjs');
		$this->load_template('user/view_statisticsavaillotreport', $data, $footer);
	}

	public function soldlotstatisticsalldev()
	{
		$this->statisticsalldevview('soldlotstatisticsreport', 'Sales Rate by Lot Frontage');
	}

	function soldlotstatisticsreport($development_id)
	{
		require_once(BASEPATH .'../assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php');
		$this->load->library('mapovis_lib');
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_change_log', '', TRUE);
		$development = $this->getDevelopmentAndValidate($development_id);
		$this->validatestatisticsaccess($development_id);

		$sold_lots_by_width      = $this->Model_change_log->getSoldLotsFormatted($development_id);
		$export_csv              = $this->input->post('export_csv');
		if($export_csv == 1){
			$tmp_file_name   = '/tmp/temp_sold_'.time().'.csv';
			$fp              = fopen($tmp_file_name, 'w');
			$csv_headers     = array('Month');
			foreach($sold_lots_by_width['widths'] as $width){
				$csv_headers[] = $width.'m';
			}
			$csv_headers[] = 'Total';
			fputcsv($fp, $csv_headers);

			foreach($sold_lots_by_width['results'] as $month => $month_results){
				$total_lots = 0;
				$csv_data   = array(date('M Y', strtotime($month)));
				foreach($sold_lots_by_width['widths'] as $width){
					$lot_views   = (isset($month_results[$width]->number_lots))? (int)$month_results[$width]->number_lots: 0;
					$total_lots += $lot_views;
					$csv_data[]  = $lot_views;
				}
				$csv_data[] = $total_lots;
				fputcsv($fp, $csv_data);
			}
			fclose($fp);
			$csv_file_content = file_get_contents($tmp_file_name);
			$file_name        = $development->development_name.'  Available Lots On Hand by Frontage'.date('Y-m-d').'.csv';
			if($csv_file_content){
				header('Content-Type: application/csv');
				header('Content-Disposition: attachement; filename="'.$file_name.'"');
				echo $csv_file_content;
				exit();
			}
		}

		$data        = array(
			'development'             => $development,
			'sold_lots_by_width'      => $sold_lots_by_width,
			'chart_colors'            => $this->mapovis_lib->statisticsColorCodes(),
		);
		$footer = array('user/footer_standard', 'user/view_statisticsreport_customjs');
		$this->load_template('user/view_statisticssoldlotreport', $data, $footer);
	}

	public function averagelotpricesreportalldev()
	{
		$this->statisticsalldevview('averagelotpricesreport', 'Average Listed Price ($\'000) per Lot Frontage');
	}

	public function averagelotpricesreport($development_id)
	{
		require_once(BASEPATH .'../assets/plugins/kendoui/wrappers/php/lib/Kendo/Autoload.php');
		$this->load->library('mapovis_lib');
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_average_lot_prices_by_width', '', TRUE);
		$development = $this->getDevelopmentAndValidate($development_id);
		$this->validatestatisticsaccess($development_id);

		$avg_price_by_width = $this->Model_average_lot_prices_by_width->getAverageLotPricesFormatted($development_id);
		$export_csv         = $this->input->post('export_csv');
		if($export_csv == 1){
			$tmp_file_name   = '/tmp/temp_avgprice_'.time().'.csv';
			$fp              = fopen($tmp_file_name, 'w');
			$csv_headers     = array('Month');
			foreach($avg_price_by_width['widths'] as $width){
				$csv_headers[] = $width.'m';
			}
			fputcsv($fp, $csv_headers);

			foreach($avg_price_by_width['results'] as $month => $month_results){
				$csv_data = array(date('M Y', strtotime('-1 month', strtotime($month))));
				foreach($avg_price_by_width['widths'] as $width){
					$lot_views   = (isset($month_results[$width]->average_price))? number_format($month_results[$width]->average_price/1000): 0;
					$csv_data[]  = $lot_views;
				}
				fputcsv($fp, $csv_data);
			}
			fclose($fp);
			$csv_file_content = file_get_contents($tmp_file_name);
			$file_name        = $development->development_name.'  Average Listed Price per Lot Frontage'.date('Y-m-d').'.csv';
			if($csv_file_content){
				header('Content-Type: application/csv');
				header('Content-Disposition: attachement; filename="'.$file_name.'"');
				echo $csv_file_content;
				exit();
			}
		}

		$data        = array(
			'development'        => $development,
			'avg_price_by_width' => $avg_price_by_width,
			'chart_colors'       => $this->mapovis_lib->statisticsColorCodes(),
		);
		$footer = array('user/footer_standard', 'user/view_statisticsreport_customjs');
		$this->load_template('user/view_averagelotpricesreport', $data, $footer);
	}

	function pricelistschedule($development_id)
	{
		$development  = $this->getDevelopmentAndValidate($development_id);

		$this->load->model('Model_pricelist_schedule', '', TRUE);

		$form_data = $this->input->post();
		if($form_data != FALSE)
		{
			//Field validation success, add or edit reminder
			if($this->Model_pricelist_schedule->updatePricelistSchedule($development_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The price list schedule has been updated successfully.');
				redirect('salesteam/managedevelopmentid/'.$development_id);
			}
		}
		$schedule     = $this->Model_pricelist_schedule->getPricelistSchedule($development_id);
		$data         = array(
			'development' => $development,
			'schedule'    => $schedule,
		);
		$this->load_template('user/view_pricelistschedule', $data);
	}

	function pricelistpdf($development_id)
	{
		$development  = $this->getDevelopmentAndValidate($development_id);
		$this->load->model('Model_apipdf', '', TRUE);
		$this->Model_apipdf->devpdfpricelist($development->development_id, FALSE);
	}

	function managepricelistsubscribers($development_id)
	{
		$development = $this->getDevelopmentAndValidate($development_id);

		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$pricelist_subscribers = $this->Model_pricelist_subscriber->getPricelistSubscribers($development_id);
		$data                  = array(
			'pricelist_subscribers' => $pricelist_subscribers,
			'development'           => $development
		);
		$footer = array('user/footer_standard', 'user/view_pricelistsubscribers_customjs');
		$this->load_template('user/view_pricelistsubscribers', $data, $footer);
	}

	function viewpricelistsubscriber($pricelist_subscriber_id)
	{
		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$pricelist_subscriber = $this->Model_pricelist_subscriber->getPricelistSubscriber($pricelist_subscriber_id);
		$data                 = array(
			'pricelist_subscriber' => $pricelist_subscriber,
		);
		$this->load_ajaxtemplate('user/view_pricelistsubscriber', $data);
	}

	function updatepricelistsubscriber($pricelist_subscriber_id)
	{
		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$pricelist_subscriber = $this->Model_pricelist_subscriber->getPricelistSubscriber($pricelist_subscriber_id);
		$form_data            = $this->input->post();
		if($pricelist_subscriber && $this->Model_pricelist_subscriber->updatePricelistSubscriber($pricelist_subscriber_id, $form_data)){
			$this->session->set_flashdata('alertmessage', 'The pricelist subscriber was updated successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to update the pricelist subscriber, please make sure the data is correct.');
		}
		redirect('salesteam/managepricelistsubscribers/'.$pricelist_subscriber->development_id);
	}

	function addpricelistsubscriber($development_id)
	{
		$development = $this->getDevelopmentAndValidate($development_id);

		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('subscriber_name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('subscriber_email', 'Email', 'trim|required|valid_email');

		if($this->form_validation->run() != FALSE)
		{
			$form_data    = $this->input->post();
			$redirecto_to = (isset($form_data['action_continue']) && $form_data['action_continue'] == 1)? "addpricelistsubscriber/{$development_id}": 'managepricelistsubscribers/'.$development_id;
			$this->Model_pricelist_subscriber->addPricelistSubscriber($development_id, $form_data);
			$this->session->set_flashdata('alertmessage', 'The pricelist subscriber was added successfully.');
			redirect('salesteam/'.$redirecto_to);
		}
		$data          = array(
			'development' => $development
		);
		$footer        = array('user/footer_standard', 'admin/developments/view_actioncontinue_customjs');
		$this->load_template('user/view_addpricelistsubscriber', $data, $footer);
	}

	function deletepricelistsubscriber($pricelist_subscriber_id)
	{
		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$amenity   = $this->Model_pricelist_subscriber->getPricelistSubscriber($pricelist_subscriber_id);

		if($this->Model_pricelist_subscriber->deletePricelistSubscriber($pricelist_subscriber_id)){
			$this->session->set_flashdata('alertmessage', 'The pricelist subscriber was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the pricelist subscriber.');
		}
		redirect('salesteam/managepricelistsubscribers/'.$amenity->development_id);
	}

	function pricelistindivemail($development_id)
	{
		$development = $this->getDevelopmentAndValidate($development_id);

		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$pricelist_subscribers = $this->Model_pricelist_subscriber->getPricelistSubscribers($development_id);
		$data           = array(
			'pricelist_subscribers' => $pricelist_subscribers,
			'development'           => $development
		);
		$this->load_template('user/view_pricelistindivemail', $data);
	}

	function pricelistsendindivemail($subscriber_id)
	{
		$this->load->model('Model_pricelist_subscriber', '', TRUE);
		$this->load->model('Model_pricelist_schedule', '', TRUE);
		$pricelist_subscriber = $this->Model_pricelist_subscriber->getPricelistSubscriber($subscriber_id);
		$development          = $this->getDevelopmentAndValidate($pricelist_subscriber->development_id);
		$this->Model_pricelist_schedule->sendPricelistToSubscriber($development, $pricelist_subscriber);
		
		$this->session->set_flashdata('alertmessage', 'The Email has been sent successfully.');
		redirect('salesteam/managedevelopmentid/'.$development->development_id);
	}

	function developmentdocuments($development_id)
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_development_document', '', TRUE);
		$development = $this->getDevelopmentAndValidate($development_id);
		$documents   = $this->Model_development_document->getDevelopmentDocuments($development_id);
		$data        = array(
			'development' => $development,
			'documents'   => $documents,
			'types'       => $this->Model_development_document->getTypes(),
		);
		$this->load_template('user/view_developmentdocuments', $data);
	}

	function uploaddevelopmentdocument($development_id)
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_development_document', '', TRUE);
		$development = $this->getDevelopmentAndValidate($development_id);
		$form_data = $this->input->post();
		if($form_data != FALSE){
			if($this->Model_development_document->addDevelopmentDocument($development_id, $form_data)){
				$this->session->set_flashdata('alertmessage', 'The Document was added successfully.');
			}
			else{
				$this->session->set_flashdata('errormessage', 'It was not possible to added the document, please try again.');
			}
			redirect('salesteam/developmentdocuments/'.$development_id);
		}
		$data        = array(
			'development'  => $development,
			'types'        => $this->Model_development_document->getTypes(),
			'default_type' => $this->input->get('type_name'),
		);
		$this->load_ajaxtemplate('user/view_developmentdocumentupload', $data);
	}

	function deletedevelopmentdocument($development_id, $document_id)
	{
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_development_document', '', TRUE);
		$development = $this->getDevelopmentAndValidate($development_id);
		$document    = $this->Model_development_document->getDevelopmentDocument($document_id);
		// make sure the document belongs to given development
		if($development->development_id == $document->development_id && $this->Model_development_document->deleteDevelopmentDocuments($document_id)){
			$this->session->set_flashdata('alertmessage', 'The Document was deleted successfully.');
		}
		else{
			$this->session->set_flashdata('errormessage', 'It was not possible to delete the document, please try again.');
		}
		redirect('salesteam/developmentdocuments/'.$development_id);
	}

	/**
	 * Validates if the development exists and of the user has access to it otherwise it will redirect the user to My Developments
	 * @param int $development_id
	 * @return object Development 
	 */
	private function getDevelopmentAndValidate($development_id)
	{
		$development  = $this->Model_development->getDevelopment($development_id);
		$current_user = $this->Model_user->getUserLogged();

		if((is_int($development_id) && $development) || !$this->Model_development->HasUserPermissions($development, $current_user->user_id)){
			$this->session->set_flashdata('errormessage', 'You do not have permissions to access the development.');
			//Go to my developments
			redirect('salesteam/mydevelopments');
		}
		return $development;
	}

	private function statisticsalldevview($actionview, $titlelabel)
	{
		$this->load->model('Model_developer', '', TRUE);
		$my_developments  = $this->Model_development->getMyDevelopmentForStatistics();
		if(count($my_developments) == 1){
			$dev_id = current($my_developments)->development_id;
			redirect('salesteam/'.$actionview.'/'.$dev_id);
		}

		$developers   = $this->Model_developer->getDevelopers(TRUE);
		$data         = array(
			'developments' => $my_developments,
			'developers'   => $developers,
			'actionview'   => $actionview,
			'titlelabel'   => $titlelabel
		);
		$this->load_template('user/view_statisticsalldev', $data);
	}

	function savedefaultpagination()
	{
		$current_user       = $this->Model_user->getUserLogged();
		$default_pagination = $this->input->post('default_pagination');
		$this->Model_user->setDefaultPagination($current_user->user_id, $default_pagination);
		echo 'save_'.$default_pagination;
	}

	private function validatestatisticsaccess($development_id){
		if(!$this->Model_development->showStatistics($development_id)){
			$this->session->set_flashdata('errormessage', 'You do not have permissions to access the development statistics.');
			//Go to my developments
			redirect('salesteam/mydevelopments');
		}
	}
}
?>