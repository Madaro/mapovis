<?php
if( ! defined('BASEPATH')) exit('No direct script access allowed');
class apiland extends CI_Controller {

	function __construct(){
		parent::__construct(); 
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_lot', '', TRUE);
		$this->load->model('Model_house', '', TRUE);
	}

	function index($developmentkey = '')
	{
		$development = $this->Model_development->getDevelopment(8);
		$form_data   = $this->input->post();
		if(!$form_data){
			$form_data = array();
		}
		$data        = array(
			'form_data'         => $form_data,
			'developmentkey'    => $developmentkey,
			'development'       => $development,
		);
		$this->load->view('land/index', $data);
	}

	function land($development_id, $developmentkey = '')
	{
		$form_data         = $this->input->get();
		if(!$form_data){
			$form_data = array();
		}
		$development       = $this->Model_development->getDevelopment($development_id);
		$search_parameters = $this->getSearchParameters($development_id);
		// validate if the access key is valid
		if(!$this->Model_development->validateDevelomentKey($development_id, $developmentkey)){
			$active_stages = array();
		}
		else{
			$active_stages = $this->Model_lot->getLandSearchResults($development_id, $form_data);
		}
			
		$data        = array(
			'stages'            => $active_stages,
			'development'       => $development,
			'developmentkey'    => $developmentkey,
			'search_parameters' => $search_parameters,
			'form_data'         => $form_data
		);
		$page_view = $this->Model_development->getLandView($development->land_view);
		$this->load->view($page_view, $data);
	}

	function land_full($development_id, $developmentkey = '')
	{
		$form_data         = $this->input->post();
		if(!$form_data){
			$form_data = array();
		}
		$development       = $this->Model_development->getDevelopment($development_id);
		$search_parameters = $this->Model_lot->landSearchParameters($development_id);
		// validate if the access key is valid
		if(!$this->Model_development->validateDevelomentKey($development_id, $developmentkey)){
			$active_stages = array();
		}
		else{
			$active_stages = $this->Model_lot->getLandSearchResults($development_id, $form_data);
		}
			
		$data        = array(
			'stages'            => $active_stages,
			'development'       => $development,
			'developmentkey'    => $developmentkey,
			'search_parameters' => $search_parameters,
			'form_data'         => $form_data,
			'page_view'         => $this->Model_development->getLandView($development->land_view)
		);
		$this->load->view('land/land-full', $data);
	}

	private function getSearchParameters($development_id){
		$hnl_search_parameters  = $this->Model_house->houseLandSearchParameters($development_id);
		$land_search_parameters = $this->Model_lot->landSearchParameters($development_id);
		return array_merge($hnl_search_parameters, $land_search_parameters);
	}

}
?>