<?php
if( ! defined('BASEPATH')) exit('No direct script access allowed');

class cron extends CI_Controller {

	public function __construct() 
	{
		parent::__construct(); 
		$this->load->model('Model_user', '', TRUE);
		$this->load->model('Model_development', '', TRUE);
		$this->load->model('Model_schedule', '', TRUE);
		$this->load->library('mapovis_lib');
		$this->load->library('sms_sender');
	}

	/**
	 * Use to send notifications to users
	 */
	public function index()
	{
		$this->recordavailablelots();
		$this->sendreminders();
		$this->recordavailablelotsbywidth();
		$this->recordaveragepricesbywidth();
	}

	public function sendreminders()
	{
		$this->load->model('Model_reminder', '', TRUE);
		$this->Model_reminder->sendRemindersAllDevelopments();
	}

	public function sendpricelists()
	{
		$this->load->model('Model_pricelist_schedule', '', TRUE);
		$this->Model_pricelist_schedule->sendPriceListsAllDevelopments();
	}

	/**
	 * Use to record available lots at the beginning of each month
	 */
	public function recordavailablelots()
	{
		$this->load->model('Model_availablelot', '', TRUE);
		$this->Model_availablelot->addAvailableLots();
	}

	/**
	 * Use to record available lots at the beginning of each month
	 */
	public function recordavailablelotsbywidth()
	{
		$this->load->model('Model_availablelot_by_width', '', TRUE);
		$this->Model_availablelot_by_width->addAvailableLots();
	}

	/**
	 * Use to record available lots at the beginning of each month
	 */
	public function recordaveragepricesbywidth()
	{
		$this->load->model('Model_average_lot_prices_by_width', '', TRUE);
		$this->Model_average_lot_prices_by_width->addAverageLotPrices();
	}

	/**
	 * This can be used to test notifications for a specific development
	 * @param type $development_id
	 * @param type $number_hours
	 */
	public function testingreminders($development_id, $number_hours)
	{
		$this->load->model('Model_reminder', '', TRUE);
		$this->Model_reminder->sendRemindersAllDevelopmentsCicleTest($development_id, $number_hours);
	}

}
?>