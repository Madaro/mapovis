<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="styles/kendo.common.min.css" rel="stylesheet" />
    <link href="styles/kendo.default.min.css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
    <script src="js/kendo.all.min.js"></script>
</head>
<body>
    <?php
require_once 'wrappers/php/lib/Kendo/Autoload.php';


$greaterthan90days = new \Kendo\Dataviz\UI\ChartSeriesItem();
$greaterthan90days->name('<90 Days')
         ->stack('Listed')
         ->data(array(15, 20, 10));

$between90and180days = new \Kendo\Dataviz\UI\ChartSeriesItem();
$between90and180days->name('90-180 Days')
         ->stack('Listed')
         ->data(array(10, 15, 20));

$greaterthan180days = new \Kendo\Dataviz\UI\ChartSeriesItem();
$greaterthan180days->name('>180 Days')
       ->stack('Listed')
       ->data(array(20, 15, 20));

$valueAxis = new \Kendo\Dataviz\UI\ChartValueAxisItem();



$categoryAxis = new \Kendo\Dataviz\UI\ChartCategoryAxisItem();
$categoryAxis->categories(array("January 2014","February 2014","March 2014"))
             ->majorGridLines(array('visible' => false));

$tooltip = new \Kendo\Dataviz\UI\ChartTooltip();
$tooltip->visible(true)
        ->template('#= series.name #: #= value #');



$chart = new \Kendo\Dataviz\UI\Chart('chart');
$chart->title(array('text' => 'Calleya - Available Lots Report'))
      ->legend(array('visible' => true))
      ->addSeriesItem($greaterthan90days, $between90and180days, $greaterthan180days)
      ->addValueAxisItem($valueAxis)
      ->addCategoryAxisItem($categoryAxis)
      ->tooltip($tooltip)
      ->seriesColors(array('#009bd7', '#4db9e3', '#99d7ef'))
      ->seriesDefaults(array('type' => 'column'));


echo $chart->render();
?>


</body>
</html>