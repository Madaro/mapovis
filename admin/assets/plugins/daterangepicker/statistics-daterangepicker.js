$(function()
{
	$('#date-range').dateRangePicker({ 
		inline:true,
		container: '#date-range-container',
		alwaysOpen:true,
		shortcuts: 	{
			'prev-days': [7,30,60,90],
			//'next-days': [3,5,7],
			'prev' : ['week','month','year'],
			//'next' : ['week','month','year']
		},
		showShortcuts:true
	});
});
