function showLoading(){
	if($('#qLoverlaymessageDialog').length){
		$('#qLoverlaymessageDialog').show();
		$('#qLmessageDialog').show();
	}
	else{
		$('#qLoverlaymessage').show();
		$('#qLmessage').show();
	}
}
function hideLoading(){
	if($('#qLoverlaymessageDialog').length){
		$('#qLoverlaymessageDialog').hide();
		$('#qLmessageDialog').hide();
	}
	$('#qLoverlaymessage').hide();
	$('#qLmessage').hide();
}
function messageAlert(msg_txt){
	// show dialog message
	$(msg_txt).dialog({
		height:    140,
		modal:     true,
		resizable: false,
		buttons: {'Ok': function(){$(this).dialog("close");}}
	});
}
function validateimagefiletype(filetype){
	switch(filetype){
		case 'image/png':
		case 'image/gif':
		case 'image/jpeg':
		case 'image/pjpeg':
			return true;
			break;
		default:
			$('<div title="Fail">Invalid file type!!</div>').dialog({
				height:    140,
				modal:     true,
				resizable: false,
				buttons: {'Ok': function(){$(this).dialog("close");}}
			});
			return false;
			break;
	}
	return true;
}
