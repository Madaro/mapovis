var lightbox_highlight_colour;
var lightbox_border_colour;
var start_hex_colour;
var end_hex_colour;
var developmentId = 1;
var zoom_to_lot = 101;
var zoom_level = 15;

var mapovis_not_supported_url;
var mapovis_not_supported_browser_url;


var MYLIBRARY = MYLIBRARY || (function() {
    var _args = {}; // private

    return {
        init: function(Args) {
            _args = Args;
            // some other initialising
            developmentId = parseInt(_args[0]);
            

        },
        helloWorld: function() {
            alert('Hello World! -' + _args[0]);
        }
    };
}());

jQuery(function($) {

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    
    loadJS = function(src) {
         var jsLink = $("<script type='text/javascript' src='"+src+"'>");
         jsLink.appendTo($('head'));
      }; 

    loadJS('http://stage3.mapovis.com.au/mpvs/assets/bowser/bowser.min.js');
    loadJS('//wurfl.io/wurfl.js');  
    
    if(typeof (WURFL) === 'undefined'){
        WURFL = {form_factor:"Desktop"};
    }

    var contentType = "application/x-www-form-urlencoded; charset=utf-8";

    if (window.XDomainRequest) //for IE8,IE9
        contentType = "text/plain";
        
        
    var browser = navigator.userAgent;
    var IEversion = 99; //Give a default value for non-IE browsers
     
    if (browser.indexOf("MSIE") > 1) { //Detects if IE
    
        IEversion = parseInt(browser.substr(browser.indexOf("MSIE")+5, 5));
        
        
        
        if (IEversion < 10) {    
            
            
                   
        xdr = new XDomainRequest();   // Creates a new XDR object.
        xdr.open("POST", 'http://stage3.mapovis.com.au/mapovis/get_colorsvalue.php'); // Creates a cross-domain connection with our target server using GET method. 
        xdr.send("developmentId="+developmentId); //Send string data to server
        xdr.onload = function () { //After load, parse data returned by xdr.responseText            
           
            
            var result = jQuery.parseJSON(xdr.responseText);
           
           lightbox_highlight_colour = '#' + result.lightbox_highlight_colour;
           lightbox_border_colour = '#' + result.lightbox_border_colour;
           start_hex_colour = '#' + result.start_hex_colour;
           end_hex_colour = '#' + result.end_hex_colour;
           mapovis_not_supported_browser_url = result.mapovis_not_supported_browser_url;
           
           
        };              
      }
    }
    
    else{
        
            $.ajax({
            url: "http://stage3.mapovis.com.au/mapovis/get_colorsvalue.php",
            type: "POST",
            data: "developmentId="+developmentId, 
            contentType: contentType,
            success: function(data) {
                
               

               var result = jQuery.parseJSON(data);
              
               lightbox_highlight_colour = '#' + result.lightbox_highlight_colour;
               lightbox_border_colour = '#' + result.lightbox_border_colour;
               start_hex_colour = '#' + result.start_hex_colour;
               end_hex_colour = '#' + result.end_hex_colour;
               mapovis_not_supported_browser_url = result.mapovis_not_supported_browser_url;
               mapovis_not_supported_url = result.mapovis_not_supported_url;
                
             },
             
             error:function(jqXHR,textStatus,errorThrown)
             {
                
                 alert("You can not send Cross Domain AJAX requests: "+errorThrown);
            
             }

        });
        
    }


    $('.lightbox').click(function(e) {
        

        e.preventDefault();
        
        var id = $(this).attr('id');

        zoom_to_lot = $('#'+id).attr('zoom_to_lot');
        zoom_level = $('#'+id).attr('zoom_level');
    
         
        if(isMobile.any() == "iPad")
        {   
           if(!isNaN(zoom_to_lot) && !isNaN(zoom_level)){
               
              var open = window.open('http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + developmentId, '_blank');
              
              if (open == null || typeof(open)=='undefined'){
                
                alert("Turn off your pop-up blocker!");
                
              }
                
              return;   
            
           }
           
           else{
              
              var open = window.open('http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + developmentId, '_blank');
              
              if (open == null || typeof(open)=='undefined'){
                
                alert("Turn off your pop-up blocker!");
                
              }
                
              return;   
              
           }
           
        }
        
        else if((WURFL.form_factor == "Tablet") && ($(window).width() < 800))
        {
              var open = window.open('http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + developmentId, '_blank');
              
              if (open == null || typeof(open)=='undefined'){
                
                alert("Turn off your pop-up blocker!");
                
              }
                
              return;   
           
        }
        
         else if(WURFL.form_factor == "Tablet")
        {
           if(!isNaN(zoom_to_lot) && !isNaN(zoom_level)){
               
              var open = window.open('http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + developmentId, '_blank');
              
              if (open == null || typeof(open)=='undefined'){
                
                alert("Turn off your pop-up blocker!");
                
              }
                
              return;   
            
           }
           
           else{
               
              var open = window.open('http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + developmentId, '_blank');
              
              if (open == null || typeof(open)=='undefined'){
                
                alert("Turn off your pop-up blocker!");
                
              }
                
              return;  
              
                    
           }
           
        }
        

        else if(WURFL.form_factor == "Smartphone")

        {
              var open = window.open('http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + developmentId, '_blank');
              
              if (open == null || typeof(open)=='undefined'){
                
                alert("Turn off your pop-up blocker!");
                
              }
                
              return;   
        }
        
        else if(WURFL.form_factor == "Feature Phone")
        {
              var open = window.open('http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + developmentId, '_blank');
              
              if (open == null || typeof(open)=='undefined'){
                
                alert("Turn off your pop-up blocker!");
                
              }
                
              return;   
        }


  
        
        else if( (bowser.msie && bowser.version < 10) || (bowser.firefox && bowser.version < 16))
        {
             window.open(mapovis_not_supported_browser_url, '_self');
             return;
        }
        

        else if($(window).width() >= 999 && $(window).width() < 1280)
        {
            if(!isNaN(zoom_to_lot) && !isNaN(zoom_level)){
               
                window.open('http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + developmentId + '&zoom_to_lot=' + zoom_to_lot + '&zoom_level=' + zoom_level, '_blank');
                return;
            
           }
           
           else{
               
              window.open('http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + developmentId, '_blank');
              return;  
              
           }
        }


        
        else 
        {
            // According to browser size or screen resolution change, lightbox height changed dynamically.
            var height = ($(window).height() + 23) * 0.9;
    
            if(!isNaN(zoom_to_lot) && !isNaN(zoom_level)){

                $('.lightbox').prop('href', 'http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + developmentId + '&zoom_to_lot=' + zoom_to_lot + '&zoom_level=' + zoom_level + '&lightbox[iframe]=true&lightbox[modal]=false&lightbox[move]=false&lightbox[width]=90p&lightbox[height]=' + height);
                
            }
            else{
                
               $('.lightbox').prop('href', 'http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + developmentId + '&lightbox[iframe]=true&lightbox[modal]=false&lightbox[move]=false&lightbox[width]=90p&lightbox[height]=' + height);
                
            }
            
             
            
            $('.lightbox').lightbox({'onClose' : function(){
                         
        
              }
              
            });

            $('.jquery-lightbox-border-top-left').css({
                'background': 'none',
                'border-top-left-radius': '5px',
                'border-top': '7px solid ' + lightbox_border_colour,
                'border-left': '7px  solid ' + lightbox_border_colour
            });
            $('.jquery-lightbox-border-top-middle').css('background-color', lightbox_border_colour);
            $('.jquery-lightbox-border-top-right').css({
                'background': 'none',
                'border-top-right-radius': '5px',
                'border-top': '7px solid ' + lightbox_border_colour,
                'border-right': '7px  solid ' + lightbox_border_colour
            });

            $('.jquery-lightbox-html').css("border-color", lightbox_border_colour);

            $('.jquery-lightbox-border-bottom-left').css({
                'background': 'none',
                'border-bottom-left-radius': '5px',
                'border-bottom': '7px solid ' + lightbox_border_colour,
                'border-left': '7px  solid ' + lightbox_border_colour
            });
            $('.jquery-lightbox-border-bottom-middle').css('background-color', lightbox_border_colour);
            $('.jquery-lightbox-border-bottom-right').css({
                'background': 'none',
                'border-bottom-right-radius': '5px',
                'border-bottom': '7px solid ' + lightbox_border_colour,
                'border-right': '7px  solid ' + lightbox_border_colour
            });


            $(".jquery-lightbox-button-close").on({
                mouseenter: function() {
                    //stuff to do on mouse enter
                    $(this).css("background-color", lightbox_highlight_colour);

                },

                mouseleave: function() {
                    //stuff to do on mouse leave
                    $(this).css("background-color", "");

                }
            });


        }

    });


    //Setting the display height of the content

    jQuery('.more-mask').css('height', 'auto');

    if (jQuery('.more-mask').size()) {
        var maskHeight = jQuery('.more-mask').height();
        var moreHeight = jQuery('.more-mask .holder').height();
        jQuery('.btn-more').click(function() {
            jQuery(this).toggleClass('down')
        }).toggle(function() {
            jQuery('.more-mask').animate({
                'height': moreHeight
            }, 400);
            jQuery(this).find('span').text('Show less');
        }, function() {
            jQuery('.more-mask').animate({
                'height': maskHeight
            }, 400);
            jQuery(this).find('span').text('Show more');
        });
    }

});




