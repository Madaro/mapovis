<?php
  
    include ('db_config.php');
    
    header('Access-Control-Allow-Origin: *'); 
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
  
  // Get Development ID     
    if(isset($HTTP_RAW_POST_DATA)) {
      parse_str($HTTP_RAW_POST_DATA); // here you will get variable $foo
       $developmentId = $developmentId;
    }
    else{
        $developmentId = $_POST['developmentId'];
    }

    // Ask MySQL DB for Google Analytics Tracking code for 'developmentID'
    $sql = "SELECT dialog_start_hex,mapovis_not_supported_browser_url,show_tour,show_nearby_developments, mapovis_not_supported_url,custom_googlemaps_style,lightbox_border_colour,loading_gradient_top_hex,loading_gradient_middle_hex,loading_gradient_bottom_hex, lightbox_highlight_colour, start_hex_colour, end_hex_colour, text_hex_colour, zoom_to_stage_header_hex, zoom_to_stage_button_hex FROM developments WHERE development_id='$developmentId' limit 1";
    $result = mysql_query($sql);
    $value = mysql_fetch_assoc($result);

    $lightbox_highlight_colour = $value['lightbox_highlight_colour']; // lightbox highlight color
    $lightbox_border_colour = $value['lightbox_border_colour'];  // lightbox border color.
    
    $end_hex_colour = $value['end_hex_colour'];  // header&footer gradient end hex color value.
    $start_hex_colour = $value['start_hex_colour'];  // header&footer gradient start hex color value
    
    $text_hex_colour = $value['text_hex_colour'];  // font color hex value
    
    $zoom_to_stage_header_hex = $value['zoom_to_stage_header_hex'];
    $zoom_to_stage_button_hex = $value['zoom_to_stage_button_hex'];
    
    $loading_gradient_top_hex = $value['loading_gradient_top_hex'];
    $loading_gradient_middle_hex = $value['loading_gradient_middle_hex'];
    $loading_gradient_bottom_hex = $value['loading_gradient_bottom_hex'];
    
    $custom_googlemaps_style = $value['custom_googlemaps_style'];
    $mapovis_not_supported_url = $value['mapovis_not_supported_url'];
    $show_nearby_developments = $value['show_nearby_developments'];
    $mapovis_not_supported_browser_url = $value['mapovis_not_supported_browser_url'];
    $show_tour = $value['show_tour'];
    $dialog_start_hex = $value['dialog_start_hex'];
    
    $colors = array('lightbox_highlight_colour' => $lightbox_highlight_colour, 'lightbox_border_colour' => $lightbox_border_colour,
    'start_hex_colour' => $start_hex_colour, 'end_hex_colour' => $end_hex_colour,
    'text_hex_colour' => $text_hex_colour,'zoom_to_stage_header_hex' => $zoom_to_stage_header_hex,
    'zoom_to_stage_button_hex' => $zoom_to_stage_button_hex,
    'loading_gradient_top_hex' => $loading_gradient_top_hex,
    'loading_gradient_middle_hex' => $loading_gradient_middle_hex,
    'loading_gradient_bottom_hex' => $loading_gradient_bottom_hex,
    'custom_googlemaps_style' => $custom_googlemaps_style,
    'mapovis_not_supported_url' => $mapovis_not_supported_url,
    'show_nearby_developments' => $show_nearby_developments,
    'show_tour' => $show_tour,
    'mapovis_not_supported_browser_url' => $mapovis_not_supported_browser_url,
    'dialog_start_hex' => $dialog_start_hex);
    
     echo json_encode($colors);
     exit;
?>
