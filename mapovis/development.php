<?php

    include('db_config.php');

    // Get Development ID 
    $developmentId = $_GET['developmentId'];

    // Ask MySQL DB for Google Analytics Tracking code for 'developmentID'
    $sql = "SELECT  ga_tracking_id FROM developments WHERE development_id='$developmentId' limit 1";
    $result = mysql_query($sql);
    $value = mysql_fetch_assoc($result);

    $ga_tracking_id = $value['ga_tracking_id'];
    
    $useragent=$_SERVER['HTTP_USER_AGENT'];
    
    // in case of mobile devices, site redirects into mobile version
   
    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
        header('Location: ../mapovismobile/index.html?developmentId='.$developmentId);
        exit;
    }                

?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <!--<meta http-equiv="X-UA-Compatible" content="IE=8" />-->
        <meta charset="utf-8">
        <title></title>
        <!--MAIN Style Sheet-->
        <link href="../mpvs/css/styles.css" rel="stylesheet">

        <!--Directions Style Sheet-->
        <link href="../mpvs/css/directions-styles.css" rel="stylesheet">


        <!-- Jquery -->
        <script src="../mpvs/assets/jquery/jquery-1.10.0.js"></script>
        <!-- Font Awesome -->
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">


        <script type="text/javascript" src="../mpvs/css/bootstrap/bootstrap-select.js"></script>
        <link rel="stylesheet" type="text/css" href="../mpvs/css/bootstrap/bootstrap-select.css">


        <!-- Jquery UI -->
        <link rel="stylesheet" href="../mpvs/css/jqueryui_css/jquery-ui.css">
        <script src="../mpvs/assets/jquery/jquery-ui.js"></script>
        <script src="../mpvs/assets/jquery/jquery.ui.position.js"></script>

        <!-- Bootstrap -->
        <script type="text/javascript" src="../mpvs/assets/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../mpvs/assets/bootstrap/js/bootstrap-tooltip.js"></script>


        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc2/css/bootstrap-glyphicons.css" rel="stylesheet">
        <link href="../mpvs/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!--[if lt IE 8]><link rel="stylesheet" href="../mpvs/assets/bootstrap/css/bootstrap-ie7buttonfix.css"><![endif]-->
        <!--[if IE 8]><link rel="stylesheet" href="../mpvs/assets/bootstrap/css/bootstrap-ie8buttonfix.css"><![endif]-->


        <!-- ion.RangeSlider -->
        <script src="../mpvs/assets/ion.rangeSlider/ion.rangeSlider.min.js"></script>
        <link href="../mpvs/css/slidercss/normalize.min.css" rel="stylesheet"> 
        <link href="../mpvs/css/slidercss/ion.rangeSlider.css" rel="stylesheet"> 
        <link href="../mpvs/css/slidercss/ion.rangeSlider.skinNice.css" rel="stylesheet">

        <!-- Pullout Panel -->
        <link rel="stylesheet" href="../mpvs/assets/pulloutpanel/css/pulloutpanel.css" />
        <!--  <link rel="stylesheet" href="../mpvs/assets/pulloutpanel/font/css/font-awesome.css">-->
        <!--[if IE 7]>
        <link rel="stylesheet" href="../mpvs/assets/pulloutpanel/font/css/font-awesome-ie7.css">
        <![endif]-->
        <link href="../mpvs/assets/pulloutpanel/css/scrollbars.css" rel="stylesheet" /> 
        <script src='../mpvs/assets/pulloutpanel/js/pulloutpanel.js'></script>

        <link href="../mpvs/assets/imageslider/responsiveslides.css" rel="stylesheet" />      


        <!-- Responsive Slides -->
        <script src='../mpvs/assets/imageslider/responsiveslides.min.js'></script>

        <!-- Bowser -->
        <script type="text/javascript" src="../mpvs/assets/bowser/bowser.min.js"></script>

        <!-- Jquery Cookie -->
        <script type="text/javascript" src="../mpvs/assets/jquerycookie/jquery.cookie.js"></script>

        <script type="text/javascript" src="//js.maxmind.com/js/apis/geoip2/v2.0/geoip2.js"></script>

        <script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
        <link rel="stylesheet" href="../mpvs/css/tour_css/powertour.2.2.0.css"/>
        <link rel="stylesheet" href="../mpvs/css/tour_css/powertour-style-clean.css"/><!-- Styling CSS file -->
        <link rel="stylesheet" href="../mpvs/css/tour_css/powertour-connectors.css"/> <!-- Extra styling elements CSS file -->
        <link rel="stylesheet" href="../mpvs/css/tour_css/animate.min.css"/>          <!-- Animation core CSS file -->
    
        <script src="../mpvs/assets/tour/powertour.2.2.0.js"></script>

        <link rel="shortcut icon" type="image/ico" href="../mpvs/images/favicon.ico" />

        <!-- jquery star rate plugin -->
        <link rel="stylesheet" href="../mpvs/assets/raty/jquery.raty.css"/>
        <script type="text/javascript" src="../mpvs/assets/raty/jquery.raty.js"></script>


        <!-- Google Maps API -->
        <script src="http://maps.google.com/maps/api/js?key=AIzaSyC7v5tPcwo4wXY1mU3PUtXpcxReRcrjsXE&sensor=false&libraries=places,geometry,drawing" type="text/javascript"></script>  
        <script type="text/javascript">
          var script = '<script type="text/javascript" src="../mpvs/assets/markercluster/markerclusterer';
          if (document.location.search.indexOf('compiled') !== -1) {
            script += '_compiled';
          }
          script += '.js"><' + '/script>';
          document.write(script);
        </script>
        <script type="text/javascript" src="../mpvs/assets/jquery/geolocationmarker-compiled.js"></script> 
        <script type="text/javascript" src="../mpvs/assets/tooltip/custom_map_tooltip.js"></script> 

        <script type="text/javascript" src="../mpvs/assets/bootstrap/js/respond.js"></script> 
        <script type='text/javascript' src="//wurfl.io/wurfl.js"></script>   
        <script type='text/javascript' src="../mpvs/assets/spin/spin.min.js"></script> 
        <script type='text/javascript' src="../mpvs/assets/progress/progressbar.js"></script>   
        <!-- Google Analytics -->    
        <script>
            
            if(window.location.hostname == "app.mapovis.com.au"){

                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                ga('create', '<?php echo $ga_tracking_id; ?>', 'mapovis.com.au');
                ga('require', 'displayfeatures');
                ga('send', 'pageview');

            }

        </script>

        <!--Pingdom Real User Monitoring-->
        <script>
            var _prum = [['id', '5350f16fabe53d5b393db096'],
            ['mark', 'firstbyte', (new Date()).getTime()]];
            (function() {
                var s = document.getElementsByTagName('script')[0]
                , p = document.createElement('script');
                p.async = 'async';
                p.src = '//rum-static.pingdom.net/prum.min.js';
                s.parentNode.insertBefore(p, s);
            })();
        </script>
        <script>
            if(typeof (WURFL) === 'undefined'){
                WURFL = {form_factor:"Desktop"};
            }
        </script>     

        <!-- START MAPOVIS -->
        <script src="../mpvs/start-mapovis.js" type="text/javascript"></script>
      
    </head>

    <body>
        <script src="ZeroClipboard.js"></script>
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '1587324584834279',
              xfbml      : true,
              version    : 'v2.2'
            });
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>

        <!-- START PULLOUT PANEL -->  
        <div id="pocp1" class="pocp_right"><!-- BEGIN PULL OUT CONTENT PANEL -->

            <div class="pocp"><!-- BEGIN PANEL CONTAINER -->

                <a href="#" id="panel_close_custom_bt"><span class="glyphicon glyphicon-remove-circle" style="font-size:16px;"></span><span>Close</span></a>

                <div class="pocp_content"><!-- BEGIN PANEL INNER CONTENT -->

                    <!-- BEGIN PANEL GOOGLE MAP DIRECTION MODE -->
                    <h2></h2>

                    <div class="btn-group" data-toggle-name="direction_mode_options"  id="modes">
                        <button title="By Car" type="button" value="DRIVING" data-toggle="button" class="btn btn-default by_car active"></button>
                        <button title="By public transit" type="button" value="TRANSIT" data-toggle="button" class="btn btn-default by_transit"></button>
                        <button title="Walking" type="button" value="WALKING" data-toggle="button" class="btn btn-default by_walk"></button>
                        <button title="Bicycling" type="button" value="BICYCLING" data-toggle="button" class="btn btn-default by_bicycling"></button>
                    </div>

                    <div class="directions_wps">
                        <img src="../mpvs/images/directions/circle_greenA.png" id="origin_marker" />
                        <input type="text" class="form-control"  id="origin" disabled="disabled" >
                        <img src="../mpvs/images/directions/circle_greenB.png" id="destination_marker"  />
                        <input type="text" class="form-control" placeholder="Enter the destination address." id="destination">
                        <input type="hidden" id="development_coordinate" value="">
                        <input type="hidden" id="external_amenity_coordinate" value="">
                        <input type="hidden" id="external_amenity_address" value="">
                        <button type="button" class="btn btn-default reverse_direction"></button>
                    </div>

                    <button type="button" class="btn btn-primary" id="get_direction">GET DIRECTIONS</button>

                    <div id="summary_distance_time">
                        <div class="division_part distance_part">
                            <i class="fa fa-car"></i><span id="directions_distance"></span>
                        </div>
                        <div class="division_part">
                            <i class="fa fa-clock-o"></i></span><span id="directions_time"></span>
                        </div>
                    </div>

                    <div id="directionsPanel"></div>   
                    <!-- END  PANEL GOOGLE MAP DIRECTION MODE--> 
                </div><!-- END PANEL INNER CONTENT -->

                <div class="searchp_content">
                    <div class="title_box">
                        <span class="glyphicon glyphicon-search" style="font-size:20px;"></span><span class="note">Lot Search</span>
                    </div>
                    <div class="box">
                        <span class="glyphicon glyphicon-usd" style="font-size:16px;"></span><span class="note">PRICE</span>
                        <div style="margin-top: 8px;" class="slider_box">
                            <input type="text" id="price_range_slider" name="price_range_slider" value="" />
                        </div>
                        
                        <div style="margin-top: 8px;" id="price_range_dropdown_tablet">
                           <span>From: </span>
                           <select name="from" id="price_from">
                            
                           </select>
                           
                           <span>&nbsp;&nbsp;To: </span>
                           <select name="to" id="price_to">
                            
                           </select>
                        </div>
                        
                    </div>

                    <div class="box">
                        <span class="glyphicon glyphicon-resize-horizontal" style="font-size:16px;"></span><span class="note">LOT FRONTAGE m</span>
                        <div style="margin-top: 8px;" class="slider_box">
                            <input type="text" id="lotwidth_range_slider" name="lotwidth_range_slider" value="" />
                        </div>
                        
                         <div style="margin-top: 8px;" id="lot_width_checkbox_group_tablet">
                          <label class="checkbox"><input type="checkbox" name="lot_width" value="8,10">8m - 10m</label>
                          <label class="checkbox"><input type="checkbox" name="lot_width" value="10,12">10m - 12m</label>
                          <label class="checkbox"><input type="checkbox" name="lot_width" value="12,14">12m - 14m</label>
                          <label class="checkbox"><input type="checkbox" name="lot_width" value="14,16">14m - 16m</label>
                          <label class="checkbox"><input type="checkbox" name="lot_width" value="16,10000">> 16m</label> 
                         </div>  
                    </div>

                    <div class="box">
                        <span class="glyphicon glyphicon-fullscreen" style="font-size:16px;"></span><span class="note">LOT SIZE m<sup>2</sup></span>
                        <div style="margin-top: 8px;" class="slider_box">
                            <input type="text" id="lotsize_range_slider" name="lotsize_range_slider" value="" />
                        </div> 
                        
                         <div style="margin-top: 8px; margin-bottom: 10px;" id="lot_size_checkbox_group_tablet">
                          <label class="checkbox"><input type="checkbox" name="lot_size" value="0,300"> < 300m2</label>
                          <label class="checkbox"><input type="checkbox" name="lot_size" value="300,400">300m2 - 400m2</label>
                          <label class="checkbox"><input type="checkbox" name="lot_size" value="400,500">400m2 - 500m2</label>
                          <label class="checkbox"><input type="checkbox" name="lot_size" value="500,600">500m2 - 600m2</label>
                          <label class="checkbox"><input type="checkbox" name="lot_size" value="600,700">600m2 - 700m2</label>
                          <label class="checkbox"><input type="checkbox" name="lot_size" value="700,10000">> 700m2</label>  
                        </div> 
                        
                    </div>
                    <button type="button" class="btn btn-primary" id="lot_search">Search</button>

                    <div id="lot_search_list">

                    </div>
                </div>


                <div class="nearplaces_content">
                    <div class="title_box">
                        <span class="glyphicon glyphicon-map-marker" style="font-size:20px;"></span><span class="nearplaces_panel_title">Nearby Places</span>
                    </div>  

                    <div id="nearbyplaces_group">

                    </div>

                </div>
                
                <div class="linkeddevelopments_content">
                   <div class="title_box">
                        <span class="glyphicon glyphicon-map-marker" style="font-size:20px;"></span><span class="nearplaces_panel_title">Nearby Developments</span>
                    </div>
                    <ul>
                    </ul>
                </div>

            </div>     


        </div><!-- END PANEL CONTAINER -->


        </div>
        <!-- END PULLOUT PANEL -->  


        <!-- LOADING MODAL --> 
        <div id="dialog-modal" title="" class="loadingscreen" id="loadingscreen">
            <div id="progressbar" class="progressbar">
              
            </div> 
        </div>

        <!-- HEADER --> 
        <div id="placespanel">
            <div id="search_nearby_tip">
                <span class="placesSearchText">Search for Nearby Places:</span><input id="target" type="text" placeholder="e.g. schools, restaurants, shopping">
            </div>

            <span id="note_lot_see"><span class="glyphicon glyphicon-info-sign" style="position:relative; top:2px;"></span>

                <span id="tip"></span>
                <span id="goto_first_stage">Zoom IN to See Lot Availability </span>
                <span id="zoom_to_center">& Community Features</span></span>

            <div id="getdirections">
                <span id="getdirectionstext"></span>
                <span id="getdirectionsinput"><input id="direction_address" type="text" placeholder=""></span>
                <span id="getdirectionsbutton"><a class="button small" href="#"><span>Search</span></a></span>
            </div>

        </div>

        <div id="map_zoombar_tip"></div>

        <button id="streetview_close">&times;</button>

        <div id="show_mylocation">
            <span class="glyphicon glyphicon-screenshot"></span>
        </div>
        <div id="amenities_selection_bar">
            <ul>
            </ul>
        </div>

        <div id="update_message" class="alert alert-warning alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <span id="alert_message"><strong>Warning!</strong> Better check yourself, you're not looking too good.</span>
        </div>

        <div id="map-canvas">

        </div>  
        
        <div id="lotpolygon_coordinates_panel">
            <p id="message_lot_polygon"></p>
            <label for="lots_list">Lot:</label>
            <select id="lots_list">
            </select>    
            
            <ul>
                 
            </ul>
            <button id="polygon_coords_send">Submit</button>
            <button id="clear_coords">Clear</button>
        </div>
        
        <div id="polyline_coordinates_panel">
            <p id="message_road_polyline"></p>
            <label for="polylines_list">Polyline:</label>
            <select id="polylines_list">
            </select>
                
            <ul>
              <li>
                <label for="polyline_name">Polyline name:</label>
                <input type="text" id="polyline_name">
              </li>   
              <li>
                <label for="polyline_stroke_color">Stroke Colour:</label>
                <input type="text" id="polyline_stroke_color">
              </li>  
              <li>
                <label for="polyline_stroke_opacity">Stroke Opacity:</label>
                <input type="text" id="polyline_stroke_opacity">
              </li>  
              <li>
                <label for="polyline_stroke_weight">Stroke Weight:</label>
                <input type="text" id="polyline_stroke_weight">  
              </li>       
            </ul>
            
            <ul id="polyline_coordinates">
            </ul>
            <button id="new_polyline">New</button>
            <button id="polyline_coords_send">Submit</button>
            <button id="clear_polyline_coords">Clear</button>
        </div>
        
        <div id="lots_coordinates_panel">
            <p id="message_lot_coordinate"></p>
            <label for="lots_coordinate_list">Lot:</label>
            <select id="lots_coordinate_list">
            </select>    
            
            <label for="lot_lat">Lat:</label>
            <input type="text" id="lot_lat">
            <label for="lot_lng">Lng:</label>
            <input type="text" id="lot_lng">
            
            <button id="lot_coords_send">Submit</button>
            <button id="lot_coords_clear">Clear</button>
        </div>
        
        
        <div id="amenitypolygon_coordinates_panel">
            <p id="message_amenity_polygon"></p>  
            <label for="amenities_list">Amenities:</label>
            <select id="amenities_list">
            </select>
            
            <ul>
                 
            </ul>
            <button id="amenity_polygon_coords_send">Submit</button>
            <button id="amenity_clear_coords">Clear</button>
        </div>
        
        <div id="define_precinct_polygon_panel">
            <p id="message_precinct_polygon"></p>  
            <label for="precincts_list">Precincts:</label>
            <select id="precincts_list">
            </select>
            
            <ul>
                 
            </ul>
            <button id="precinct_polygon_coords_send">Submit</button>
            <button id="precinct_clear_coords">Clear</button>
        </div>
        
        <div id="define_stage_polygon_panel">
            <p id="message_stage_polygon"></p>  
            <label for="stages_array">Stages:</label>
            <select id="stages_array">
            </select>
            
            <ul>
                 
            </ul>
            <button id="stage_polygon_coords_send">Submit</button>
            <button id="stage_clear_coords">Clear</button>
        </div>
        
         <div id="salesoffice_polygon_coordinates_panel">
           
            <ul>
                 
            </ul>
            <button id="salesoffice_polygon_coords_send">Submit</button>
            <button id="sopolygon_clear_coords">Clear</button>
        </div>


        <div id="custom_zoom_control">
            <div id="custom_zoom_in">
                <span class="glyphicon glyphicon-plus"></span>
            </div>
            <div id="custom_zoom_out">
                <span class="glyphicon glyphicon-minus"></span>
            </div>
        </div>

        <div id="controlUI">
            <div id="back_to_development">
                <b></b>
            </div>
        </div>

        <div id="phonenum_container"><span class="glyphicon glyphicon-phone-alt" style="font-size:23px;"></span><span id="phone_num"></span></div>

        <div class="stage_container ui-widget-content" id="draggable">
            <div class="header"><span class="glyphicon glyphicon-zoom-in" style="font-size:20px;"></span><span class="title">Zoom to Stage</span>
                <span class="glyphicon glyphicon-minus" style="float: right;" id="icon_switch"></span>
            </div>
            <div class="content">
                <ul class="nav nav-pills nav-stacked" id="stages">
                </ul>
            </div>
        </div>

        <div id="box-intro-1" class="single-step">
            <span class="connectorarrow-rm"><!-- custom --></span> 
            <p><b>Zoom Out</b> to see where <span class="development_name"></span> is located.</p>
            <p><b>Zoom In</b> to learn about <span class="development_name"></span> & see lots for sale.</p>
             <footer>
                <a href="#" class="btn btn-default" data-powertour-action="stop">Cancel</a>
                <a href="#" class="btn btn-default pull-right" data-powertour-action="next">Continue</a>
            </footer> 
        </div>
        
         <div id="box-intro-1-tablet" class="single-step">
            <span class="connectorarrow-lm"><!-- custom --></span> 
            <p><b>Zoom Out</b> to see where <span class="development_name"></span> is located.</p>
            <p><b>Zoom In</b> to learn about <span class="development_name"></span> & see lots for sale.</p>
             <footer>
                <a href="#" class="btn btn-default" data-powertour-action="stop">Cancel</a>
                <a href="#" class="btn btn-default pull-right" data-powertour-action="next">Continue</a>
            </footer> 
        </div>
        
        <div id="feaaturedbar_help" class="single-step">
            <span class="connectorarrow-tm"><!-- custom --></span> 
            <p>Click on these categories to see important places which are near <span class="development_name"></span>.</p>
            <footer>
                <a href="#" class="btn btn-default" data-powertour-action="stop">Cancel</a>
                <a href="#" class="btn btn-default" data-powertour-action="prev">Prev</a>  
                <a href="#" class="btn btn-default pull-right" data-powertour-action="next">Next</a>
            </footer>
        </div>

        <div id="box-intro0" class="single-step">
            <span class="connectorarrow-tm"><!-- custom --></span>  
            <p>Search here to find what is located near <span class="development_name"></span>.</p>
            <footer>
                <a href="#" class="btn btn-default" data-powertour-action="stop">Cancel</a>
                <a href="#" class="btn btn-default" data-powertour-action="prev">Prev</a>
                <a href="#" class="btn btn-default pull-right" data-powertour-action="next">Next</a>
            </footer>
        </div>

        <div id="box-intro" class="single-step">
            <span class="connectorarrow-tr"><!-- custom --></span> 
            <p>Search here to find how far it is to your work etc.</p>
            <footer>
                <a href="#" class="btn btn-default" data-powertour-action="stop">Cancel</a>
                 <a href="#" class="btn btn-default" data-powertour-action="prev">Prev</a>
                <a href="#" class="btn btn-default pull-right" data-powertour-action="next">Next</a>
            </footer>
        </div>

        <div id="box-intro1" class="single-step">
            <span class="connectorarrow-bl"><!-- custom --></span> 
            <p>Search for land which fit your requirements.</p>
            <footer>
                <a href="#" class="btn btn-default" data-powertour-action="stop">Cancel</a>
                <a href="#" class="btn btn-default" data-powertour-action="prev">Prev</a>  
                <a href="#" class="btn btn-default pull-right" data-powertour-action="next">Next</a>
            </footer>
        </div>

        <div id="box-intro2" class="single-step">
            <span class="connectorarrow-bm"><!-- custom --></span> 
            <p>Print PDF files showing what land is available.</p>
            <footer>
                <a href="#" class="btn btn-default" data-powertour-action="prev">Prev</a>  
                <a href="#" class="btn btn-default pull-right" data-powertour-action="stop">Close</a>
            </footer>
        </div>

        <div id="box-intro3" class="single-step">
            <span class="connectorarrow-bm"><!-- custom --></span> 
            <p>Zoom directly to a stage where lots are available for sale.</p>
            <footer>
                <a href="#" class="btn btn-default" data-powertour-action="stop">Cancel</a>
                <a href="#" class="btn btn-default" data-powertour-action="prev">Prev</a>  
                <a href="#" class="btn btn-default pull-right" data-powertour-action="next">Next</a>
            </footer>
        </div>

        <div id="logo_container">
            <a href="#" target="_blank"><img src="http://86ad9bd1d74a8c1c2762-b4502b2a77e65f014b79a7b3606c7673.r39.cf4.rackcdn.com/mapovis-map-logo-v4.png"></a>
        </div>

        <div id="nearby_developments">
          <b>Nearby Developments</b>
        </div>
        
        <div id="compass_mark">
            <img src="" alt="">
        </div>

        <!-- FOOTER --> 
        <div id="footer">
            <div class="lot_search_container"></div>
            <div id="lot_search_container">
                <span class="glyphicon glyphicon-search" style="margin-right:4px; font-size:19px;"><span id="lot_search_bt">Lot&#32;Search</span></span>
            </div>

            <div id="zoom_to_container">
                <span class="glyphicon glyphicon-zoom-in align" style="font-size:20px; width:25px;"></span>
                <span style="vertical-align: top;" class="align">Zoom To:</span>
                <select id="stages_select" class="selectpicker dropup" data-width="133px" data-style="zoomto" name="type">
                    <optgroup id="stages_list" label="STAGES" data-subtext="">
                    </optgroup>   
                </select>
                <a class="button small" href="#" id="go_given_stage"><span>Go</span></a>
            </div>

            <div id="dialog_link"> 
                <span class="glyphicon glyphicon-question-sign"  style="font-size:22px;cursor:pointer;"></span>
            </div>

            <span class="glyphicon glyphicon-envelope" id="enquire_envelop"><span>&#32;Enquire</span></span>

            <div id="pdf_download"> 
                <span style="font-size:14px;" id="selectpdf_instruction">Select PDF:</span>

                <select id="pdfs_select" class="selectpicker dropup" data-width="133px" data-style="zoomto" name="type">
                    <optgroup id="pdfs_list" label="STAGES" data-subtext="">
                    </optgroup>   
                </select>

                <a class="button small" href="#" id="download_stagepdf"><span>Download</span></a>
            </div>

        </div>

        <!-- HELP (?) DIALOG -->  
        <div id="dialog" style="display:none; text-align: center;">
        <div id="dialogcontainer" style="display: inline-block;">
        <div class="help-video">
            <a href="http://www.youtube.com" target="_blank">Help Video</a>
        </div>
        <br>
        <div>Learn more at <a href="http://www.mapovis.com.au" target="_blank">www.mapovis.com.au</a><div>
                <br>
            </div>
        </div>


    <!--JSPM   
    <script src="https://jspm.io/loader.js"></script>

    <script>
        window.addEventListener('load', function() {
        })
        jspm.import('./../mpvs/main', function() {
        });
    </script>-->

     
<!-- CONTINUE MAPOVIS - Bundle.js (instead of JSPM)--> 
<script src="../mpvs/bundle.js"></script>

</body>
</html>







