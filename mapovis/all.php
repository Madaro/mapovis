 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>All Developments</title>
    <!-- Jquery -->
    <script src="../mpvs/assets/jquery/jquery-1.10.0.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyC7v5tPcwo4wXY1mU3PUtXpcxReRcrjsXE&sensor=false" type="text/javascript"></script>  
        
    <style>
        #map_canvas{
            width:80%;
            height:100%;
            position:fixed !important;
            top:0;
            right:0;
            left:0;
            bottom:0;
            margin:0;
            padding:0;
        }
        #list_panel{
            float:right;
            width:20%;
            height:100%;
            background-color: white;
            z-index:5;
            right:0;
            bottom:0;
            margin:0;
            top:0;
            overflow:scroll;
            
        }
        
        #list_panel ul{
            list-style-type: none;
        }
        
        #list_panel ul > li{
            margin-bottom:10px;
        }
        
        #list_panel ul a{
            text-decoration: none;
            margin-right: 15px;
        }
    </style>
    <script type="text/javascript">
        var development_polygon_drawn = false;
    
        $(document).ready(function(){
           
            var mapOptions = {
              center: { lat: -24.407137917727653, lng: 132.7587890625},
              zoom: 5
            };
           map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
           
           var url = document.domain;
           
           $.ajax({
            url: '../admin/getdatainjson/all_activeDevelopments/',
            type: "GET",
            dataType: "json",
            complete: function(data_response) {

                var developments = data_response.responseJSON;

                for(var i = 0; i < developments.length; i++){
                    
                    var development = developments[i];
                     var latlng = new google.maps.LatLng(
                      parseFloat(development.centre_latitude),
                      parseFloat(development.centre_longitude));
                    
                    var development_Icon = {
                        url: development.developmenticon,
                        size: null,
                        origin: null,
                        anchor: null,
                        scaledSize: new google.maps.Size(parseInt(development.developmenticon_width),parseInt(development.developmenticon_height))
                    };     

                    var developmentMarker =  new google.maps.Marker({
                        position: latlng,
                        map: map,
                        zIndex:4,
                        title:development.development_name,
                        icon: development_Icon, 
                        development_polygon_drawn:false,
                        developmentId: development.development_id,
                        visible:true
                    });
                    
                     google.maps.event.addListener(developmentMarker, 'click', function(e){

                        if(url == 'app.mapovis.com.au'){
                          window.open('http://app.mapovis.com.au/mapovis/development.php?developmentId=' + this.developmentId, '_blank');
                          return;  
                        }
                        else{
                            window.open('http://stage3.mapovis.com.au/mapovis/development.php?developmentId=' + this.developmentId, '_blank');
                            return;
                        } 
                        

                    });
                    
                    if(url == 'app.mapovis.com.au'){
                        var development_entry = "<li><a href='http://app.mapovis.com.au/mapovis/development.php?developmentId="+development.development_id+"' target='_blank'>"+development.development_name+"</a>"                                                                               
                        if((development.land_for_sale_url != '') && (development.land_for_sale_url != null)){
                            development_entry += "<a href='"+development.land_for_sale_url+"' target='_blank'>Land</a>";
                        }
                        
                        if((development.house_and_land_packages_url  != '') && (development.house_and_land_packages_url  != null)){
                            development_entry += "<a href='"+development.house_and_land_packages_url +"' target='_blank'>H&L</a>";
                        }
                        
                        if((development.mapovis_mini_url  != '') && (development.mapovis_mini_url  != null)){
                            development_entry += "<a href='"+development.mapovis_mini_url +"' target='_blank'>Mini</a>";
                        }
                        
                        development_entry += '</li>';
                    }
                    else{
                        var development_entry = "<li><a href='http://stage3.mapovis.com.au/mapovis/development.php?developmentId="+development.development_id+"' target='_blank'>"+development.development_name+"</a>";                                                                                
                        if((development.land_for_sale_url != '') && (development.land_for_sale_url != null)){
                            development_entry += "<a href='"+development.land_for_sale_url+"' target='_blank'>Land</a>";
                        }
                        
                        if((development.house_and_land_packages_url  != '') && (development.house_and_land_packages_url  != null)){
                            development_entry += "<a href='"+development.house_and_land_packages_url +"' target='_blank'>H&L</a>";
                        }
                        
                        if((development.mapovis_mini_url  != '') && (development.mapovis_mini_url  != null)){
                            development_entry += "<a href='"+development.mapovis_mini_url +"' target='_blank'>Mini</a>";
                        }
                        
                        development_entry += '</li>';
                    }
                    
                    $(development_entry).appendTo('#list_panel ul');
                    
                    


                   google.maps.event.addListener(map, 'zoom_changed', getDevelopmentMarkerZoomEventHandler(development,developmentMarker));
                    
                   function getDevelopmentMarkerZoomEventHandler(development,developmentMarker){

                        return function(){
                            $.ajax({
                                url: '../admin/getdatainjson/mapovisminipolygon/' + development.development_id,
                                type: "GET",
                                dataType: "json",
                                complete: function(data_response) {

                                    var points = data_response.responseJSON;
                                    if(points.length == 0){
                                        return;
                                    }

                                    if(developmentMarker.development_polygon_drawn == true){
                                        return;
                                    }


                                    polygon_points = [];

                                    for(var i = 0; i < points.length; i++){

                                        var point = points[i];    
                                        pt = new google.maps.LatLng(parseFloat(point.lat), parseFloat(point.lng));
                                        polygon_points.push(pt);

                                    }

                                    polygon = new google.maps.Polygon({
                                        paths: polygon_points,
                                        strokeColor: '#'+development.mapovis_mini_polygon_strokeColor,
                                        strokeOpacity: parseFloat(development.mapovis_mini_polygon_strokeOpacity),
                                        strokeWeight: parseInt(development.mapovis_mini_polygon_strokeWeight),
                                        fillColor: '#'+development.mapovis_mini_polygon_fillColor,
                                        fillOpacity: parseFloat(development.mapovis_mini_polygon_fillOpacity),
                                        flat:true
                                    });       

                                    polygon.setMap(map);
                                    developmentMarker.development_polygon_drawn = true;

                                }  

                            }); 
                        }
                    }
                }
                
                
                
            }
             
            
           });
           
        });
           
   </script>
</head>

<body>
    <div id="map_canvas"></div>
    <div id="list_panel">
        <ul>
            
        </ul>
    </div>
</body>
</html>

