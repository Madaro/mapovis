<?php
	
	require_once 'lib/PHPExcel.php';
	require_once 'lib/PHPExcel/IOFactory.php';

	set_time_limit(0);
	// Create new PHPExcel object
    
    $data = '';

    $radius = intval($_REQUEST['radius']);
    $lat = floatval($_REQUEST['lat']);
    $lng = floatval($_REQUEST['lng']);
    $category =  $_REQUEST['category1'];

    $development_name = $_REQUEST['development_name'];
    $types = $_REQUEST['category1_types'];
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->setActiveSheetIndex(0);
    
    $results_places = array();
    
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(60);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
    

    $objPHPExcel->getActiveSheet()->getStyle('A:G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32);
    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(32);
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:Q1');
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:Q2'); 
    
    $styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'ff000000'),
        ),
    ),
    );
    
    
    $styleArrayFor1Row = array(
        'font' => array(
          'color' => array('argb' => 'ffffffff'),
          'name' => 'Arial',
          'size' => '20',
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ),
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'ff000000'),
            ),
        ),
        
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
              'argb' => 'ff6FB1E2',
               ),
        ),
    );

    $objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($styleArrayFor1Row); 
    $objPHPExcel->getActiveSheet()->setCellValue('A1', $development_name.' - Proposed External Amenities'); 
    
    $styleArrayFor2Row = array(
        'font' => array(
          'color' => array('argb' => 'ff575757'),
          'name' => 'Arial',
          'size' => '14',
          'bold' => true, 
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ),
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'ff000000'),
            ),
        ),
        
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
              'argb' => 'FFEDEDED',
               ),
        ),
    );

    $objPHPExcel->getActiveSheet()->getStyle('A2:Q2')->applyFromArray($styleArrayFor2Row); 
    $objPHPExcel->getActiveSheet()->setCellValue('A2', $category );  
    
    $styleArrayFor4Row = array(
        'font' => array(
          'bold' => true,
          'color' => array('argb' => 'ffffffff'),
        ),
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'ff000000'),
            ),
        ),
        
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
              'argb' => '00000000',
               ),
        ),
    ); 
    
    $objPHPExcel->getActiveSheet()->getStyle('A4:G4')->applyFromArray($styleArrayFor4Row);
    
    $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Rank');
    $objPHPExcel->getActiveSheet()->setCellValue('B4', 'Name');
    $objPHPExcel->getActiveSheet()->setCellValue('C4', 'Latitude');
    $objPHPExcel->getActiveSheet()->setCellValue('D4', 'Longitude');
    $objPHPExcel->getActiveSheet()->setCellValue('E4', 'Address');
    $objPHPExcel->getActiveSheet()->setCellValue('F4', 'URL');
    $objPHPExcel->getActiveSheet()->setCellValue('G4', 'Type');
    
	// Rename sheet
	$objPHPExcel->getActiveSheet()->setTitle($_REQUEST['category1']);


	

	
	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => 'https://maps.googleapis.com/maps/api/place/radarsearch/json?location='.$lat.','.$lng.'&radius='.$radius.'&types='.$types.'&key=AIzaSyC7v5tPcwo4wXY1mU3PUtXpcxReRcrjsXE',
		CURLOPT_USERAGENT => 'Codular Sample cURL Request'
	));

	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    

	// Send the request & save response to $resp
	$resp = curl_exec($curl);
	$resp = json_decode($resp,true);

	// Close request to clear up some resources
	curl_close($curl);
	
	$places = $resp['results'];
    
    if(count($places) == 0){
        echo "no result";
        exit;
    }
    
    function getPlaceDetails($i,$places){
        global $objPHPExcel,$development_name,$category,$results_places;
        $placeid = $places[$i]['place_id'];

        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://maps.googleapis.com/maps/api/place/details/json?placeid='.$placeid.'&key=AIzaSyC7v5tPcwo4wXY1mU3PUtXpcxReRcrjsXE',
            CURLOPT_USERAGENT => 'Codular Sample cURL Request1'
        ));

        curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, false); 
           
        // Send the request & save response to $resp
        $resp1 = curl_exec($curl1);
        $resp1 = json_decode($resp1,true);

        $status = $resp1['status']; // status code
         
        $website = !empty($resp1['result']['website']) ? $resp1['result']['website'] : "";
        $name = $resp1['result']['name']; //name 
        $address = $resp1['result']['formatted_address']; //formal address

        $type = $resp1['result']['types'][0];

        
        
        $lat = $resp1['result']['geometry']['location']['lat']; // lat
        $lng = $resp1['result']['geometry']['location']['lng'];  // lng
        
            
            if($status == "OK"){
                
                $k = $i+5; 
                $j = $i+1;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$k, $j);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$k, $name);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$k, $lat);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$k, $lng);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$k, $address);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$k, $website);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$k, $type);
                
                
                $entry = array('lat' => $lat, 'lng' => $lng, 'name'=> $name, 'address' => $address, 'website' => $website, 'number' => $j);
                
                array_push($results_places,$entry);
                
            }
            
            else{
                echo $status;
                exit;
            }

    }
     
    for($i = 0; $i < count($places); $i++){
        
        getPlaceDetails($i,$places);

    }
    
    $k = count($places) + 5;
    
    $objPHPExcel->getActiveSheet()->getStyle('A5:G'.$k)->applyFromArray($styleArray); 
    
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $objWriter->save($development_name."_".$category. "_external_amenities.xlsx");
            
            // Echo done
    $download_url = "http://stage3.mapovis.com.au/mapovis/generate_excel/".$development_name."_".$category. "_external_amenities.xlsx";
    array_push($results_places,array('download_url' => $download_url));
    
    echo json_encode($results_places);
            
    exit;
	
?>