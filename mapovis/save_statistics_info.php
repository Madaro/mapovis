<?php
  
   include ('db_config.php');   
   // Whenever someone opens mapovis interface, its analytical information will be saved in statistics table.
   if($_SERVER['SERVER_NAME'] != "app.mapovis.com.au"){
       exit;
   }
      
   if( isset($_POST["flag"]) && ($_POST["flag"] == "statictics") )
   {
      $browserinfo = $_POST['browserinfo'];
      $developmentid = $_POST['developmentId'];
      $ip_address = $_POST['ip_address'];
      $timestamp = $_POST['timestamp'];
      
      $result = mysql_query("SELECT * FROM statistics WHERE timestamp = '$timestamp' ");
      if( mysql_num_rows($result) > 0) {
         exit;
      }
      else{
         $query = "INSERT INTO statistics (developmentID_opened,timestamp,ip_address,user_agent) VALUES ($developmentid,'$timestamp','$ip_address','$browserinfo')";
      }
      
      $retval = mysql_query($query,$link);
      if(! $retval ){ 
        die('Could not enter data: ' . mysql_error());  
      }
      $query1 = "select * from developments where development_id = $developmentid";
      $rs = mysql_query($query1,$link); 
       
      while( $row = mysql_fetch_assoc($rs)){
       $result['state'] = $row['state'];
       $result['origin'] = $row['vic_public_transport_origin'];
       $result['stageplanpdf'] = $row['stageplanpdf'];
      }
        
      echo json_encode($result);
   }
  
   if( isset($_POST["flag"]) && ($_POST["flag"] == "statistics_zoomtolot") ){ 
      $browserinfo = $_POST['browserinfo'];
      $developmentid = $_POST['developmentId'];
      $ip_address = $_POST['ip_address'];
      $timestamp = $_POST['timestamp'];
      $lotNumber = intval($_POST['lotNumber']);
      
      $result = mysql_query("SELECT * FROM statistics_zoomToLot WHERE timestamp = '$timestamp' ");

      if( mysql_num_rows($result) > 0) {
         exit;
      }
      else{
         $query = "INSERT INTO statistics_zoomToLot(developmentID,timestamp,ip_address,user_agent,lotNumber) VALUES ($developmentid,'$timestamp','$ip_address','$browserinfo',$lotNumber)";
      }
  
      $retval = mysql_query($query,$link);
      if(! $retval ){  
        die('Could not enter data: ' . mysql_error());
      }
   }
   
   if( isset($_POST["flag"]) && ($_POST["flag"] == "statistics_salesOffice") ){
      $browserinfo = $_POST['browserinfo'];
      $developmentid = $_POST['developmentId'];
      $ip_address = $_POST['ip_address'];
      $timestamp = $_POST['timestamp'];
      
      $result = mysql_query("SELECT * FROM statistics_salesOffice WHERE timestamp = '$timestamp' ");
      if( mysql_num_rows($result) > 0) {
         exit;
      }
      else{
         $query = "INSERT INTO statistics_salesOffice(developmentID,timestamp,ip_address,user_agent) VALUES ($developmentid,'$timestamp','$ip_address','$browserinfo')";
      }
  
      $retval = mysql_query($query,$link);
      if(! $retval ){
       die('Could not enter data: ' . mysql_error());
      }
   }
   
   if( isset($_POST["flag"]) && ($_POST["flag"] == "statictics_enquire") ){
      $browserinfo = $_POST['browserinfo'];
      $developmentid = $_POST['developmentId'];
      $ip_address = $_POST['ip_address'];
      $timestamp = $_POST['timestamp'];
        
      $query = "INSERT INTO statistics_enquire(developmentId,timestamp,visitor_ip,browser) VALUES ($developmentid,'$timestamp','$ip_address','$browserinfo')";
      $retval = mysql_query($query,$link);

      if(! $retval ){   
       die('Could not enter data: ' . mysql_error());
      }
   }
   
   if( isset($_POST["flag"]) && ($_POST["flag"] == "statistics_fb_share") ){
      
      $browserinfo = $_POST['browserinfo'];
      $developmentid = intval($_POST['developmentId']);
      $ip_address = $_POST['ip_address'];
      $timestamp = $_POST['timestamp'];
      $type = $_POST['type'];
      
      $result = mysql_query("SELECT * FROM statistics_facebook_share WHERE timestamp = '$timestamp' ");
      if( mysql_num_rows($result) > 0) {
         exit;
      }
      else{
         $query = "INSERT INTO statistics_facebook_share(developmentID,timestamp,ip_address,user_agent,type) VALUES ($developmentid,'$timestamp','$ip_address','$browserinfo','$type')";
      }
  
      $retval = mysql_query($query,$link);
      if(! $retval ){
          die('Could not enter data: ' . mysql_error());
      }
     
   }
   // Whenever someone does a direction button, address that they searched for will be saved in statictics_directionSearches table.
   
   if( $_POST["flag"] == "statistics_directionSearches" )
   {
      $browserinfo = $_POST['browserinfo'];
      $developmentId = $_POST['developmentId'];
      $ip_address = $_POST['ip_address'];
      $timestamp = $_POST['timestamp'];
      
      $destination_address = $_POST['destination'];    
      $query = "INSERT INTO statistics_directionSearches (developmentID,direction_address,timestamp,ip_address,user_agent) VALUES ($developmentId,'$destination_address', '$timestamp','$ip_address','$browserinfo')";
      
      $retval = mysql_query($query,$link);
      if(! $retval ){
          die('Could not enter data: ' . mysql_error()); 
      }
   }
   
   // Whenever lot clicked, its lot id will be saved in statistics_lotViews table.
   
   if( $_POST["flag"] == "statistics_lotViews" )
   {
      $browserinfo = $_POST['browserinfo'];
      $developmentid = $_POST['developmentId'];
      $ip_address = $_POST['ip_address'];
      $timestamp = $_POST['timestamp'];
      $lotId = $_POST['lotId'];        
      
      $query = "INSERT INTO statistics_lotViews (developmentID,lotID,timestamp,ip_address,user_agent) VALUES ($developmentid,'$lotId', '$timestamp','$ip_address','$browserinfo')";
      $retval = mysql_query($query,$link);
      if(! $retval ){
       die('Could not enter data: ' . mysql_error());   
      } 
   }
   
   // Whenever someone does place search , search keyword will be saved in statistics_placeSearches table.
   if( $_POST["flag"] == "statistics_placeSearches" )
   {
      $browserinfo = $_POST['browserinfo'];
      $developmentid = $_POST['developmentId'];
      $ip_address = $_POST['ip_address'];
      $timestamp = $_POST['timestamp'];
      
      $placeSearch = $_POST['place_phrase'];
      $query = "INSERT INTO statistics_placesSearches (developmentID,search_phrase,timestamp,ip_address,user_agent) VALUES ($developmentid,'$placeSearch', '$timestamp','$ip_address','$browserinfo')";
      
      $retval = mysql_query($query,$link);
      if(! $retval ){
        echo ('Could not enter data: ' . mysql_error());
      }
   }
   
   if($_POST["flag"] == "statistics_lotsearch"){
        $ip_address = $_POST['ip_address'];
        $lotwidth_from = floatval($_POST['lotwidth_from']);
        $lotwidth_to = floatval($_POST['lotwidth_to']);
        $developmentId = floatval($_POST['developmentId']);
        $timestamp = $_POST['timestamp'];
        
        $browserinfo = $_POST['browser'];
        $price_from = floatval($_POST['price_from']);
        $price_to = floatval($_POST['price_to']);
        $size_from = floatval($_POST['size_from']);
        $size_to = floatval($_POST['size_to']);
        
        $query = "INSERT INTO lot_search_logs(development_id,browser,datetime,width_from,width_to,price_from,price_to,size_from,size_to,ipaddress) VALUES ($developmentId,'$browserinfo',$timestamp,$lotwidth_from,$lotwidth_to,$price_from,$price_to,$size_from,$size_to,'$ip_address')";
        $retval = mysql_query($query,$link);
        if(! $retval ){    
          die('Could not enter data: ' . mysql_error());
        }
   }
   
   if($_POST["flag"] == "statistics_amenityViews"){
        $ip_address = $_POST['ip_address'];
        $developmentId = floatval($_POST['developmentId']);
        $timestamp = $_POST['timestamp'];
        $browserinfo = $_POST['browser'];
        $amenity_id = floatval($_POST['amenity_id']);
        $query = "INSERT INTO statistics_amenitiesViews(development_id,browser,datetime,amenity_id,ipaddress) VALUES ($developmentId,'$browserinfo',$timestamp,$amenity_id,'$ip_address')";
   
        $retval = mysql_query($query,$link);
        if(! $retval ){           
          die('Could not enter data: ' . mysql_error());
        }
   }
   
   if($_POST["flag"] == "statistics_externalAmenitiesViews"){    
        $ip_address = $_POST['ip_address'];
        $developmentId = floatval($_POST['developmentId']);
        $timestamp = $_POST['timestamp'];
        $browserinfo = $_POST['browser'];
        $externalAmenity_id = floatval($_POST['externalAmenity_id']);      
        $query = "INSERT INTO statistics_externalAmenitiesViews(development_id,browser,datetime,externalAmenity_id,ipaddress) VALUES ($developmentId,'$browserinfo',$timestamp,$externalAmenity_id,'$ip_address')";
      
        $retval = mysql_query($query,$link);
        if(! $retval ){
         die('Could not enter data: ' . mysql_error());     
        }
      
   }
   
    if($_POST["flag"] == "statistics_externalAmenityCategoryClicks"){  
        $developmentId = floatval($_POST['developmentId']);
        $timestamp = $_POST['timestamp'];
        $external_amenity_type_id = intval($_POST['externalAmenity_type_id']);
        $query = "INSERT INTO statistics_externalAmenityCategoryClicks(development_id,datetime,external_amenity_type_id) VALUES ($developmentId,$timestamp,$external_amenity_type_id)";
      
        $retval = mysql_query($query,$link);
        if(! $retval ){
          die('Could not enter data: ' . mysql_error());    
        }
        echo $query;
   }
   
   if($_POST["flag"] == "statistics_helpViews"){
       
        $ip_address = $_POST['ip_address'];
        $developmentId = floatval($_POST['developmentId']);
        $timestamp = $_POST['timestamp'];
        $query = "INSERT INTO statistics_helpViews(development_id,datetime,ipaddress) VALUES ($developmentId,$timestamp,'$ip_address')";
      
        $retval = mysql_query($query,$link);
        if(! $retval ){    
          die('Could not enter data: ' . mysql_error());     
        }  
   }
   
   if($_POST["flag"] == "statistics_pdfDownloads"){
        $ip_address = $_POST['ip_address'];
        $developmentId = floatval($_POST['developmentId']);
        $timestamp = $_POST['timestamp'];
        
        if(isset($_POST['stage_id'])){
            $stage_id = intval($_POST['stage_id']);
        }
        else{
            $stage_id = 99999;
        }
        
        $stage_type = $_POST['pdf_type'];
        $query = "INSERT INTO statistics_pdfDownloads(development_id,datetime,stage_id,ipaddress,pdf_type) VALUES ($developmentId,$timestamp,$stage_id,'$ip_address','$stage_type')";
        
        $retval = mysql_query($query,$link);
        if(! $retval ){    
          die('Could not enter data: ' . mysql_error());     
        } 
   }
   
   if(isset($_POST["flag"]) && ($_POST["flag"] == "statistics_externalmarkers")){
        $ip_address = $_POST['ip_address'];
        $developmentId = intval($_POST['developmentId']);
        $timestamp = $_POST['timestamp'];
        $browserinfo = $_POST['browser'];
        $external_markerid = intval($_POST['marker_id']);
        
        $query = "INSERT INTO statistics_externalmarkers(development_id,useragent,datetime,external_markerid,ipaddress) VALUES ($developmentId,'$browserinfo','$timestamp',$external_markerid,'$ip_address')";
        $retval = mysql_query($query,$link);
        if(! $retval ){
           die('Could not enter data: ' . mysql_error());     
        } 
   }
       
   mysql_close($link);
   exit;
    
?>
