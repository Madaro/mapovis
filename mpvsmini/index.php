<?php
     $developmentId = $_GET['developmentId'];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
        <meta charset="utf-8">
        <title></title>
        <!--MAIN Style Sheet--> 
        <link href="../mpvs/css/styles.css" rel="stylesheet">   
        <link href="css/style_mini.css" rel="stylesheet">
         
        <!-- Jquery -->
        <script src="assets/jquery/jquery-1.10.0.js"></script>
        <!-- Font Awesome -->
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">

        <!-- Jquery UI -->
        <link rel="stylesheet" href="css/jqueryui_css/jquery-ui.css">
        <script src="assets/jquery/jquery-ui.js"></script>
        <script src="assets/jquery/jquery.ui.position.js"></script>
        
         <!-- Jquery Cookie -->
        <script type="text/javascript" src="../mpvs/assets/jquerycookie/jquery.cookie.js"></script>


        <!-- Bootstrap -->
        <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/bootstrap/js/bootstrap-tooltip.js"></script>


        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc2/css/bootstrap-glyphicons.css" rel="stylesheet">
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!--[if lt IE 8]><link rel="stylesheet" href="assets/bootstrap/css/bootstrap-ie7buttonfix.css"><![endif]-->
        <!--[if IE 8]><link rel="stylesheet" href="assets/bootstrap/css/bootstrap-ie8buttonfix.css"><![endif]-->

        <script type="text/javascript" src="//js.maxmind.com/js/apis/geoip2/v2.0/geoip2.js"></script>

        <script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>

        <link rel="shortcut icon" type="image/ico" href="images/favicon.ico" />

        <!-- Google Maps API -->
        <script src="http://maps.google.com/maps/api/js?key=AIzaSyC7v5tPcwo4wXY1mU3PUtXpcxReRcrjsXE&sensor=false&libraries=places,geometry,drawing" type="text/javascript"></script>  
        <script type="text/javascript" src="assets/poly/v3_epoly.js"></script>
        <script type="text/javascript">
          var script = '<script type="text/javascript" src="assets/markercluster/markerclusterer';
          if (document.location.search.indexOf('compiled') !== -1) {
            script += '_compiled';
          }
          script += '.js"><' + '/script>';
          document.write(script);
        </script>
        
        <script type="text/javascript" src="assets/tooltip/custom_map_tooltip.js"></script> 

        <script type="text/javascript" src="assets/bootstrap/js/respond.js"></script> 
        <script type='text/javascript' src="//wurfl.io/wurfl.js"></script>     
        <script type="text/javascript" src="assets/progress/progressbar.js"></script>
      
        <!-- START MAPOVIS -->
        <script src="start-mapovis.js" type="text/javascript"></script>

    </head>

    <body>
        <div id="preloader">
            <div id="load">
            
            </div>
        </div>
        <div id="amenities_selection_bar">
            <ul>
            </ul>
        </div>
        
        <div id="direction_howfar_panel">
            <div class="header">
                <img src="images/how_far_to.png">
                <span>How Far To?</span>
                <img src="images/close.png" id="directions_collapse_expand_panel_bt">
            </div>
            <ul>
            
            </ul>
            <div id="expand_panel_bt">
                <img src="images/how_far_to.png">   
            </div>
        </div>
        
        <a id="mini_open_interactive_masterplan_button" href='#' target="_blank"><img src="#" alt=""></a>
        <div id="compass_mark">
            <img src="" alt="">
        </div>

        <a href="javascript:void(0)"  style="visibility: hidden;" target="_parent" id="fakebox" onclick="parent.postMessage('<?php echo $developmentId; ?>', '*');"></a> 
       
        <div id="map-canvas">

        </div>  
        
        <div id="development_minipolygon_coordinates_panel"> 
            <ul>
                 
            </ul>
            <button id="polygon_coords_send">Submit</button>
            <button id="clear_coords">Clear</button>
        </div>

        <div id="custom_zoom_control">
            <div id="custom_zoom_in">
                <span class="glyphicon glyphicon-plus"></span>
            </div>
            <div id="custom_zoom_out">
                <span class="glyphicon glyphicon-minus"></span>
            </div>
        </div>

        <div id="controlUI_mapovismini">
            <div id="back_to_development">
                <b></b>
            </div>
        </div>

        <div id="logo_container">
            <a href="#" target="_blank"><img src="http://86ad9bd1d74a8c1c2762-b4502b2a77e65f014b79a7b3606c7673.r39.cf4.rackcdn.com/mapovis-map-logo-v4.png"></a>
        </div>
        

<!--JSPM  
<script src="https://jspm.io/loader.js"></script>
    
<script>
    window.addEventListener('load', function() {
     })
    jspm.import('./../mpvsmini/main', function() {
     });
</script> --> 

<!-- CONTINUE MAPOVIS - Bundle.js (instead of JSPM) -->
<script src="./../mpvsmini/bundle.js"></script> 

</body>

</html>
