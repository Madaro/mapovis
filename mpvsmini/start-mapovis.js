$(document).ready(function($){

    var ip;

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };


    function get(name){
        if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
            return decodeURIComponent(name[1]);
    }

    var developmentId = get( "developmentId" );
 //   $.cookie('developmentid',developmentId);

    var mobile_proceed = get("mobile");

    var browser_proceed = get("browser");

    loadJS = function(src) {
        var jsLink = $("<script type='text/javascript' src='"+src+"'>");
        jsLink.appendTo($('head'));
    }; 

    /**
    * function to load a given css file 
    */ 

    loadCSS = function(href) {
        var cssLink = $("<link rel='stylesheet' type='text/css' href='"+href+"'>");
        cssLink.appendTo($('head')); 
    };

    $.post('responseFromMapovis.php',{target:'development',developmentId: developmentId},function(data_response){ 
            var rs = JSON.parse(data_response);
            var developmentEntry = rs[0];
            developmentExityEntryRoad = new google.maps.LatLng(developmentEntry.entryexit_latitude,developmentEntry.entryexit_longitude);
            developmentName = developmentEntry.development_name;

            var custom_css_override_url = developmentEntry.custom_css_override_url;

            var mapovis_not_supported_browser_url = developmentEntry.mapovis_not_supported_browser_url;

            if(custom_css_override_url != ""){

                loadCSS(custom_css_override_url);

            }
            
            var coordinate = developmentEntry.entryexit_latitude +","+developmentEntry.entryexit_longitude;

            $('#development_coordinate').val(coordinate);
            $('#origin').val(developmentName);

            var mapovis_not_supported_url = developmentEntry.mapovis_not_supported_url;


            if((mapovis_not_supported_url != "") && (parseInt($(window).width()) < 979) && (mobile_proceed != "proceed")){

                if((isMobile.any() != "iPad") && (isMobile.any() != "Android") &&(isMobile.any() != "iPhone")){

                window.open(mapovis_not_supported_url, '_self');
                return;

                }

            }

            if(developmentEntry.custom_font_markup != ""){

                var src = developmentEntry.custom_font_markup;



                if(src.indexOf('google') >= 0){

                    loadCSS(src);
                    $('*').not('.fa, .pocp_button_right > i').css("font-family", developmentEntry.custom_google_font_name+", serif");

                }

                else{

                    WebFontConfig = {
                        typekit: { id: src }
                    };

                    var wf = document.createElement('script');
                    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                    '://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js';
                    wf.type = 'text/javascript';
                    wf.async = 'true';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(wf, s);

                    $('*').not('.fa, .pocp_button_right > i').css("font-family", developmentEntry.custom_typekit_font_name+", serif");


                }


            }

        });

});