(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){


/** @constructor */
function USGSOverlay(bounds, image, map) {

  // Now initialize all properties.
  this.bounds_ = bounds;
  this.image_ = image;
  this.map_ = map;
  
  // We define a property to hold the image's div. We'll
  // actually create this div upon receipt of the onAdd()
  // method so we'll leave it null for now.
  this.div_ = null;

  // Explicitly call setMap on this overlay
  this.setMap(map);
}

USGSOverlay.prototype = new google.maps.OverlayView();

USGSOverlay.prototype.onAdd = function() {

  // Note: an overlay's receipt of onAdd() indicates that
  // the map's panes are now available for attaching
  // the overlay to the map via the DOM.

  // Create the DIV and set some basic attributes.
  var div = document.createElement('div');
  div.style.borderStyle = 'none';
  div.style.borderWidth = '2px';
  div.style.position = 'absolute';

  // Create an IMG element and attach it to the DIV.
  var img = document.createElement('img');
  img.src = this.image_;
  img.style.width = '100%';
  img.style.height = '100%';
  img.style.webkitTransform = "-webkit-transform: translateZ(0px);";
  img.style.position = 'absolute';
  div.appendChild(img);

  // Set the overlay's div_ property to this DIV
  this.div_ = div;

  // We add an overlay to a map via one of the map's panes.
  // We'll add this overlay to the overlayLayer pane.
  var panes = this.getPanes();
  panes.overlayLayer.appendChild(div);
  //panes.overlayImage.appendChild(this.div_);
}

USGSOverlay.prototype.draw = function() {

  // Size and position the overlay. We use a southwest and northeast
  // position of the overlay to peg it to the correct position and size.
  // We need to retrieve the projection from this overlay to do this.
  var overlayProjection = this.getProjection();

  // Retrieve the southwest and northeast coordinates of this overlay
  // in latlngs and convert them to pixels coordinates.
  // We'll use these coordinates to resize the DIV.
  var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
  var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

  // Resize the image's DIV to fit the indicated dimensions.
  var div = this.div_;
  div.style.left = sw.x + 'px';
  div.style.top = ne.y + 'px';
  div.style.width = (ne.x - sw.x) + 'px';
  div.style.webkitTransform = "-webkit-transform: translateZ(0px);";
  div.style.height = (sw.y - ne.y) + 'px';
}

USGSOverlay.prototype.onRemove = function() {
  this.div_.parentNode.removeChild(this.div_);
  this.div_ = null;
}

USGSOverlay.prototype.hide = function() {
    
  
  if (this.div_) {
    // The visibility property must be a string enclosed in quotes.
      this.div_.style.display = 'none'; 
    
  }
}

USGSOverlay.prototype.show = function() {
  if (this.div_) {
    this.div_.style.display = 'block';  
  }
}

USGSOverlay.prototype.toggle = function() {
  if (this.div_) {
    if (this.div_.style.display == 'none') {  
      this.show();
    } else {
      this.hide();
    }
  }
}

module.exports = USGSOverlay;



 


},{}],2:[function(require,module,exports){
var map;
var controlUI_mapovismini;

var devlopment_polygon = null;

var markers = [];
var db_markers = [];
var external_markers = [];

var overlay_circle =  null;
var overlay;

var radius_circle_overlay_image;
var radius_circle = 0;

var radius_Markers = [];

var sale_offices = [];
var development_polygon_drawn;

var srcImage = null;


var currentinfowindow = null;
var start_hex_colour = '';
var end_hex_colour = '';

var externalPlaceDetailsView;

var text_hex_colour = '';
   
var country_name = "";       

var development_name;

var dev_center = null;

var entryexit_lat = null;
var entryexit_lng = null;

var default_bounds = null;

var custom_googlemaps_style = '';

var tooltip_background_colour = '';
var tooltip_text_colour = '';
var tooltip_font_size = 12;

var feature_amenities = false;
var direction_to_place_clicked = false;

var state_name = "";
var tooltip = null;

var visitor_ip = "";
var developmenticon_height;

var google_control_buttons_foreground = '';
var google_control_buttons_background = '';

var fontfamily_name;

var typekit_font_name;

var defaultZoom = 16;
var tooltip;

var totalDist = 0;
var totalTime = 0;
var polyline = null;
var end_marker = null;

var show_highlighted_amenities = 0;

var show_street_view = 0;

var back_to_button ="Back to Development";

var developmentName;
var developmenticon;
    
var google_font_name;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();

var midpointMarker = null;

var update_timeout = null;

var mapovis_mini_polygon_strokeColor = null;
var mapovis_mini_polygon_strokeOpacity = null;
var mapovis_mini_polygon_strokeWeight = null;
var mapovis_mini_polygon_fillColor = null;
var mapovis_mini_polygon_fillOpacity = null;

var mapovis_mini_open_interactive_masterplan_button_url = null;
var mapovis_mini_open_interactive_masterplan_button_width = null;
var mapovis_mini_open_interactive_masterplan_button_height = null;

var mapovis_mini_polygon_strokeColor_mouseover = null;
var mapovis_mini_polygon_strokeOpacity_mouseover = null;
var mapovis_mini_polygon_strokeWeight_mouseover = null;

var external_amenity_category_icons_margin_right = 6;

var mapovis_mini_polygon_fillColor_mouseover = '#ffff00';
var markerCluster; 
var salesoffice_marker = null;
var sales_office_icon_width = 0;
var sales_office_icon_height = 0;
var sales_office_icon_hide_at_zoom_level = 0;
var sales_office_icon_show_at_zoom_level = 0;

var sales_office_address = "";
var local_sales_telephone_number = "";
var sales_office_opening_house = "";

var mapovis_mini_sales_office_marker_show_at;
var mapovis_mini_sales_office_marker_hide_at;

var circle = new ProgressBar.Circle('#load', {
    color: '#555',
    trailColor: '#eee',
    strokeWidth: 10,
    duration: 2500,
    easing: 'easeInOut',
    text: {
        value: '0%'
    },
    step: function(state, bar) {
        bar.setText((bar.value() * 100).toFixed(0)+"%");
    }
});

circle.animate(1, function() {
});


var USGSOverlay = require("./USGSOverlay"); 
infoWindow = new google.maps.InfoWindow();   

/*
function name : get
parameter : name
description: THis function is one getting its paramter value from parameter name in url.
For example, if paramter name is developmetId, we can get its value such as 1.
*/

function get(name) {
    if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search))
        return decodeURIComponent(name[1]);
}

$.fn.textWidth = function(text, font) {
    if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
    $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css('font', font || this.css('font'));
    return $.fn.textWidth.fakeEl.width();
};



var developmentId = get("developmentId");
var define_mapovis_mini_polygon = get("define_mapovis_mini_polygon");

if((define_mapovis_mini_polygon == 1) && ($.cookie('admin_logged_role') == 'Admin')){
    $('#development_minipolygon_coordinates_panel').show();
}

zoom_level = get("zoom_level");

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};


/*
  This ajax part is for getting development name from developmentId.  
*/
$.post('responseFromMapovis.php',{target:'development',developmentId: developmentId},function(data_response){
        var developmentData = JSON.parse(data_response);
        state_name = developmentData.state; // save state name in cookie state_name
        developmenticon = developmentData.developmenticon;
        defaultZoom = developmentData.mapovismini_default_zoom_level;
        var src = developmentData.custom_font_markup;

        show_highlighted_amenities = developmentData.show_highlighted_amenities;
        developmenticon_height = parseInt(developmentData.developmenticon_height);

        showFeaturedAmenitiesBar();

        tooltip_background_colour = '#' + developmentData.tooltip_background_colour;
        tooltip_text_colour = '#' + developmentData.tooltip_text_colour;
        tooltip_font_size = developmentData.tooltip_font_size;
             
        sales_office_icon_width = developmentData.sales_office_icon_width;
        sales_office_icon_height = developmentData.sales_office_icon_height;
        
        sales_office_icon_show_at_zoom_level = developmentData.sales_office_icon_show_at_zoom_level;
        sales_office_icon_hide_at_zoom_level = developmentData.sales_office_icon_hide_at_zoom_level;
        
        sales_office_address = developmentData.sales_office_address;
        local_sales_telephone_number = developmentData.local_sales_telephone_number;
        sales_office_opening_house = developmentData.sales_office_opening_house;
        
        entryexit_lat = developmentData.entryexit_latitude; 
        entryexit_lng = developmentData.entryexit_longitude;
        
        mapovis_mini_sales_office_marker_show_at = parseInt(developmentData.mapovis_mini_sales_office_marker_show_at);
        mapovis_mini_sales_office_marker_hide_at = parseInt(developmentData.mapovis_mini_sales_office_marker_hide_at);
   
        $('<style>.arrow_box:after{border-bottom-color:'+tooltip_background_colour+' !important;}</style>').appendTo('head'); 
        
        google_control_buttons_foreground = '#' + developmentData.google_control_buttons_foreground;
        google_control_buttons_background = '#' + developmentData.google_control_buttons_background;

        $('#custom_zoom_control').css({'background-color':google_control_buttons_background,'color':google_control_buttons_foreground});
        $('#custom_zoom_in').css('border','1px solid '+google_control_buttons_foreground);
        $('#custom_zoom_out').css('border','1px solid '+google_control_buttons_foreground);
        
        mapovis_website_link = developmentData.mapovis_website_link;
   
        $('#logo_container > a').attr('href',"http://"+mapovis_website_link);
        $('#logo_div > a').attr('href',"http://"+mapovis_website_link);  // iPhone logo link

        show_street_view = developmentData.show_street_view;

        show_satellite = developmentData.show_satellite;

        back_to_button = developmentData.back_to_button;
        
        $('#controlUI_mapovismini > #back_to_development b').text(back_to_button);

        staging_domain = developmentData.staging_domain;

        if(src.indexOf('google') >= 0){

            fontfamily_name  = developmentData.custom_google_font_name;

        }

        else{

            fontfamily_name = developmentData.custom_typekit_font_name;

        }
        
        swBound = new google.maps.LatLng(developmentData.southwest_latitude, developmentData.southwest_longitude);
        neBound = new google.maps.LatLng(developmentData.northeast_latitude, developmentData.northeast_longitude);

        default_bounds = new google.maps.LatLngBounds(swBound, neBound);
        srcImage = developmentData.overlay_filename;
        
        external_amenity_category_icons_margin_right = developmentData.external_amenity_category_icons_margin_right;

        /*
        Front End - Custom AVALIABLE/SOLD icons per development
        */
      
        development_name = developmentData.development_name;
   
        var localPhone_number = developmentData.local_sales_telephone_number;
        var salesphone_number = developmentData.sales_telephone_number;
        
        developmentName = developmentData.development_name;
        document.title = developmentName; 
        
        show_compass = parseInt(developmentData.show_compass);
        if(show_compass == 1){
            
            $('#compass_mark').show();
            compass_url = developmentData.compass_url;
            compass_height = developmentData.compass_height;
            compass_width = developmentData.compass_width;
            
            $('#compass_mark > img').attr('src',compass_url);
            $('#compass_mark > img').css({'width':compass_width,'height':compass_height});
            
        }
        
        google_font_name = developmentData.custom_google_font_name;
        
        radius_circle_overlay_image = developmentData.radius_circle_overlay_image;

        radius_circle = developmentData.radius_circle;
        
        mapovis_mini_polygon_strokeColor = '#'+developmentData.mapovis_mini_polygon_strokeColor;              // polygon stroke color
        mapovis_mini_polygon_strokeOpacity = parseFloat(developmentData.mapovis_mini_polygon_strokeOpacity);  // polygon stroke opacity
        mapovis_mini_polygon_strokeWeight = parseInt(developmentData.mapovis_mini_polygon_strokeWeight);      // polygon stroke weight
        mapovis_mini_polygon_fillColor = '#'+developmentData.mapovis_mini_polygon_fillColor;                 // polygon fill color
        mapovis_mini_polygon_fillOpacity = parseFloat(developmentData.mapovis_mini_polygon_fillOpacity);     // polygon fillout opacity
        
        mapovis_mini_polygon_strokeColor_mouseover = '#' + developmentData.mapovis_mini_polygon_strokeColor_mouseover;               // polygon mouse over stroke color
        mapovis_mini_polygon_strokeOpacity_mouseover = parseFloat(developmentData.mapovis_mini_polygon_strokeOpacity_mouseover);    // polygon mouse over stroke opacity
        mapovis_mini_polygon_strokeWeight_mouseover = parseInt(developmentData.mapovis_mini_polygon_strokeWeight_mouseover);        // polygon mouse over stroke weight
        
        mapovis_mini_open_interactive_masterplan_button_url  =  developmentData.mapovis_mini_open_interactive_masterplan_button_url;     // interactive master plan url
        mapovis_mini_open_interactive_masterplan_button_width = developmentData.mapovis_mini_open_interactive_masterplan_button_width;    // interactive master plan button width
        mapovis_mini_open_interactive_masterplan_button_height = developmentData.mapovis_mini_open_interactive_masterplan_button_height;   // interactive master plan button height
        
        $('#mini_open_interactive_masterplan_button > img').attr('src',mapovis_mini_open_interactive_masterplan_button_url);
        $('#mini_open_interactive_masterplan_button > img').css({'width':mapovis_mini_open_interactive_masterplan_button_width+'px','height':mapovis_mini_open_interactive_masterplan_button_height+'px'});
        $('#mini_open_interactive_masterplan_button').attr('href','http://app.mapovis.com.au/mapovis/development.php?developmentId='+developmentId)
        mapovis_mini_polygon_fillColor_mouseover = '#' + developmentData.mapovis_mini_polygon_fillColor_mouseover;   // polygon mouse over fillout color

        var onSuccess = function(location){

            visitor_ip = location.traits.ip_address;

            country_name = location.country.names.en;

            if(country_name == "Australia"){

                $('#phone_num').text(localPhone_number);
            }

            else{
                $('#phone_num').text(salesphone_number);
            }

           

        };

        var onError = function(error){

            $('#phone_num').text(salesphone_number);
        };

        geoip2.city(onSuccess, onError);      // gets geolocation info of visitor i,e ip address, country name etc
        
        var developmentMap = new DevelopmentMap(developmentData);               
        
    });
 
/*
  From REST APIs, this function generates featured amenities bar on map.
*/         

function showFeaturedAmenitiesBar(){

    $.post('responseFromMapovis.php',{target:'amenitycategories',developmentId: developmentId},function(data_response){  

            if(show_highlighted_amenities == 0){

                return;

            }

            var developmentData = JSON.parse(data_response);

            amenity_types = developmentData.length;

            for(var i = 0; i < developmentData.length; i++){

                var amenity_type = "<li><img src='"+developmentData[i].e_amenity_icon_unselected+"' id='amenity_type_id_"+i+"' class='tooltip-show unselected'  title='"+developmentData[i].e_amenity_type_name+"'  selected_image = '"+developmentData[i].e_amenity_icon+"' unselected_image = '"+developmentData[i].e_amenity_icon_unselected+"' type_id='"+developmentData[i].external_amenity_type_id+"' center_lat='"+developmentData[i].e_amenity_center_latitude+"' center_lng = '"+developmentData[i].e_amenity_center_longitude+"' + center_zoomlevel='"+developmentData[i].e_amenity_zoom_level+"' /></li>";
                $(amenity_type).appendTo('#amenities_selection_bar ul');


                var amenity_type_mobile = "<li><img src='"+developmentData[i].e_amenity_icon+"' id='amenity_type_id_"+i+"' class='unselected'  title='"+developmentData[i].e_amenity_type_name+"'  selected_image = '"+developmentData[i].e_amenity_icon+"' unselected_image = '"+developmentData[i].e_amenity_icon_unselected+"' type_id='"+developmentData[i].external_amenity_type_id+"' center_lat='"+developmentData[i].e_amenity_center_latitude+"' center_lng = '"+developmentData[i].e_amenity_center_longitude+"' + center_zoomlevel='"+developmentData[i].e_amenity_zoom_level+"' /><span>"+developmentData[i].e_amenity_type_name+"</span></li>";

                $(amenity_type_mobile).appendTo('#amenities_feature_panel ul'); 
             
                var parentdiv_fontfamily = $('#amenities_feature_panel').css('font-family');

                $('#amenities_feature_panel li > span').css('font-family',parentdiv_fontfamily);


                if(WURFL.form_factor == "Tablet"){
                    $('#amenity_type_id_'+i).tooltip({placement: 'bottom',trigger:'click'});
                }

                else if(WURFL.form_factor == "Desktop"){

                    $('#amenity_type_id_'+i).tooltip({placement: 'bottom',trigger:'hover'});
                }

                $('#amenity_type_id_'+i).click(function(e){

                    e.preventDefault();
                    infoWindow.close();    // close opened infowindow.
                    
                    if(!$(this).hasClass('selected')){
                       $('#controlUI_mapovismini').trigger('click'); 
                    }
                    
                    if(overlay_circle != null){
                        
                        overlay_circle.show();  // draws overlay circle    
                        
                        for(var num = 0; num < radius_Markers.length; num++){

                            radius_Markers[num].setVisible(true);                       // plot radius markers such as 5km,10km

                        } 

                    }

                    feature_amenities = true; // make feature_amenities active
                    if(feature_amenities == true){
                       $('#directions_collapse_expand_panel_bt').trigger('click'); 
                    }

                    var id = $(this).attr('id');
                    var num = id.slice(-1);

                    $('#amenities_selection_bar li > img').not('#amenity_type_id_'+num).removeClass('selected');
                    $('#amenities_selection_bar li > img').not('#amenity_type_id_'+num).tooltip('hide');

                    $(this).toggleClass('selected'); 

                    for(var n = 0; n < developmentData.length; n++){

                        $('#amenities_selection_bar li > img#amenity_type_id_'+n).attr('src',$('#amenity_type_id_'+n).attr('unselected_image'));


                    }

                    for(var k = 0; k < external_markers.length; k++){

                        external_markers[k].setVisible(false); 

                    }   
 
                    if($(this).hasClass('selected')){
                        
                        if(isMobile.any() != "iPhone"){

                            $('#amenity_type_id_'+num).tooltip('show');  
                        }

                        $(this).attr('src',$(this).attr("selected_image"));
                        
                        var clustermarkers = [];
                        
                        if(markerCluster != null){
                            markerCluster.clearMarkers();
                            markerCluster.repaint(); 
                        }
                        

                        for(var k = 0; k < external_markers.length; k++){

                            if(external_markers[k].type_id == $(this).attr('type_id')){
                                external_markers[k].setVisible(true); 
                                clustermarkers.push(external_markers[k]);
                            }

                        }
                        
                         var styles = [{
                            url: clustermarkers[0].cluster_icon,
                            width: parseInt(clustermarkers[0].cluster_icon_width),
                            height: parseInt(clustermarkers[0].cluster_icon_height),
                            textColor: clustermarkers[0].cluster_text_color,
                            textSize:  parseInt(clustermarkers[0].cluster_icon_fontSize)
                          }];
  
                        markerCluster = new MarkerClusterer(map, clustermarkers,{
                          maxZoom: 15,
                          title:'Click for More',
                          styles: styles 
                        });

                        var zoom_level = $(this).attr('center_zoomlevel'); 
                        var center_pt = new google.maps.LatLng(parseFloat($(this).attr('center_lat')), parseFloat($(this).attr('center_lng')));

                        map.setCenter(center_pt);
                        map.setZoom(parseInt(zoom_level));

                    }

                    else{

                        $('#controlUI_mapovismini').trigger('click');

                    }

                });

            }
            
            $('#amenities_selection_bar li > img').css({'width':developmentData[0].e_amenity_icon_width/2,'height':developmentData[0].e_amenity_icon_height/2});
            $('#amenities_selection_bar ul > li').css('margin-right',external_amenity_category_icons_margin_right+"px"); 

            $('.tooltip-show').mouseover(function () {
                // do something�
                $('#amenities_selection_bar li > .tooltip > .tooltip-inner').css({'background-color': tooltip_background_colour,'color':tooltip_text_colour});
                $('#amenities_selection_bar li > .tooltip > .tooltip-arrow').css({'border-bottom-color': tooltip_background_colour,'color':tooltip_text_colour});

            });

            $('#amenities_selection_bar li > .tooltip > .tooltip-inner').css({'background-color': tooltip_background_colour,'color':tooltip_text_colour});
            $('#amenities_selection_bar li > .tooltip > .tooltip-arrow').css({'border-bottom-color': tooltip_background_colour,'color':tooltip_text_colour});


        });

}

function getRadiusMarkersZoomLevelHandler(marker){

    return function(){

        var zoomLevel = map.getZoom();
        
        if(feature_amenities == true){
            
            if(zoomLevel <= 10){

                marker.setVisible(false);

            }

            else{

                marker.setVisible(true);

            } 
            
        }
    }

}


function DevelopmentMap(developmentData) {

    var development = new google.maps.LatLng(developmentData.centre_latitude, developmentData.centre_longitude);
  
    function handle(delta) {
        if (delta < 0)
            map.setZoom(map.getZoom() - 1);
        else
            map.setZoom(map.getZoom() + 1);

    }

    /** Event handler for mouse wheel event.
    */

    function wheel(event) {
        var delta = 0;
        if (!event) /* For IE. */
            event = window.event;
        if (event.wheelDelta) { /* IE/Opera. */
            delta = event.wheelDelta / 120;
        } else if (event.detail) { /** Mozilla case. */
            /** In Mozilla, sign of delta is different than in IE.
            * Also, delta is multiple of 3.
            */
            delta = -event.detail / 3;
        }
        /** If delta is nonzero, handle it.
        * Basically, delta is now positive if wheel was scrolled up,
        * and negative, if wheel was scrolled down.
        */
        if (delta)
            handle(delta);
        /** Prevent default actions caused by mouse wheel.
        * That might be ugly, but we handle scrolls somehow
        * anyway, so don't bother here..
        */
        if (event.preventDefault)
            event.preventDefault();
        event.returnValue = false;
    }
    
    
    function triggerAmenityMarkerClick(amenity_id){
              
        return function(){
             update_timeout = setTimeout(function(){
                    for(var k = 0; k < amenity_markers.length; k++){
                 
                    if(amenity_markers[k].amenity_id == amenity_id){
                         
                       google.maps.event.trigger(amenity_markers[k], 'click');    
                          
                    } 
                    
                }  
           }, 200); 
                   
        }
        
    }


    function initialize() {

        var styles = [{
            "featureType": "administrative.land_parcel",
            "stylers": [{
                "visibility": "off"
            }]

        }];

        var centerPos = development;
        var streetViewControl = false;
        var mapTypeControl = false;

        if(show_street_view == 1){
            streetViewControl = true;
        }

        if(show_satellite == 1){
            mapTypeControl = true; 
        }

        showZoomControl = false;
        maxZoom = 19;

        var mapOptions = {
            zoom: parseInt(defaultZoom),
            maxZoom: maxZoom,
            center: centerPos,

            panControl: false,
            mapTypeControl: mapTypeControl,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_RIGHT
            },

            streetViewControl: streetViewControl,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.RIGHT_TOP
            },
            zoomControl: showZoomControl,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false // disableScrollingWithMouseWheel as default
        };
        

        var mapDiv = document.getElementById('map-canvas');
        polyline = new google.maps.Polyline({
            path: [],
            strokeColor: '#FF0000',
            strokeWeight: 3,
            strokeOpacity:0.0
        });

        /*
        In IE browser, google maps mouse wheel event enable
        */
        if (mapDiv.addEventListener) {
            mapDiv.addEventListener('mousewheel', wheel);
            mapDiv.addEventListener('DOMMouseScroll', wheel);
        } else {
            mapDiv.attachEvent('mousewheel', wheel, false);
            mapDiv.attachEvent('DOMMouseScroll', wheel, false);
        }      

        map = new google.maps.Map(mapDiv, mapOptions);
        
     if((define_mapovis_mini_polygon == 1) && ($.cookie('admin_logged_role') == 'Admin')){
                 
            overlay = new USGSOverlay(default_bounds, srcImage, map);   // development overlay image display      

            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: null,
                drawingControl: true,
                drawingControlOptions: {
                  position: google.maps.ControlPosition.TOP_CENTER,
                  drawingModes: [
                    google.maps.drawing.OverlayType.POLYGON

                  ]
                }
              });
            
            drawingManager.setMap(map);
            
            google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
                
                var coordinates = (polygon.getPath().getArray());
                development_polygon_drawn = polygon;
                for(var i = 0; i < coordinates.length; i++){
                       
                  var latlng_pair = "<li><label>#"+i+"</label><input class='lat_field' type='text' value='"+coordinates[i].lat()+"'><input class='lng_field' type='text' value='"+coordinates[i].lng()+"'></li>";      
                  $(latlng_pair).appendTo('#development_minipolygon_coordinates_panel  ul');
                       
                }
                
              });  
              
            google.maps.event.addListener(drawingManager, 'drawingmode_changed', function (polygon) {
            
                 if(devlopment_polygon != null){
                     devlopment_polygon.setVisible(false);
                 }
                
            });   
            
        }  
        
        directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
        function enableScrollingWithMouseWheel() {
            map.setOptions({ scrollwheel: true });
        }

        function disableScrollingWithMouseWheel() {
            map.setOptions({ scrollwheel: false });
        }
        
        google.maps.event.addListener(map, 'mousedown', function(){
            enableScrollingWithMouseWheel()
        });
        
        $('body').on('mousedown', function(event) {
            var clickedInsideMap = $(event.target).parents('#map-canvas').length > 0;

            if(!clickedInsideMap) {
                disableScrollingWithMouseWheel();
            }
        });
        
        $(window).scroll(function() {
            disableScrollingWithMouseWheel();
        }); 
        
        google.maps.event.addListener(map, 'idle', function() {  
             
           $('#preloader').remove();      // stops loading animation
           var zoomLevel = map.getZoom(); 
           
           if(zoomLevel >= mapovis_mini_sales_office_marker_show_at){
                if(salesoffice_marker != null){
                    salesoffice_marker.setVisible(true);
                }

            }

            if(zoomLevel <= mapovis_mini_sales_office_marker_hide_at){    
                if(salesoffice_marker != null){
                    salesoffice_marker.setVisible(false);
                }
            }
           
        });
        
        google.maps.event.addListener(map, 'zoom_changed', function() {

            var zoomLevel = map.getZoom();
         
        });
        
        
        /*
          From REST api, this ajax part draws map polygon regards on given development.
        */
        
          $.post('responseFromMapovis.php',{target:'mapovisminipolygon',developmentId: developmentId},function(data_response){  
                    
                    var points = JSON.parse(data_response);
                     
                    if(points.length == 0){
                        return;
                    }

                    polygon_points = [];
                    
                    for(var i = 0; i < points.length; i++){
                        
                        var point = points[i];    
                        pt = new google.maps.LatLng(parseFloat(point.lat), parseFloat(point.lng));
                        polygon_points.push(pt);
                            
                    }
                           
                    devlopment_polygon = new google.maps.Polygon({
                        paths: polygon_points,
                        strokeColor: mapovis_mini_polygon_strokeColor,
                        strokeOpacity: mapovis_mini_polygon_strokeOpacity,
                        strokeWeight: mapovis_mini_polygon_strokeWeight,
                        fillColor: mapovis_mini_polygon_fillColor,
                        fillOpacity: mapovis_mini_polygon_fillOpacity,
                        flat:true
                      });       
                   
                     devlopment_polygon.setMap(map);
                    
                     devlopment_polygon.infoWindow = new google.maps.InfoWindow({pixelOffset: new google.maps.Size(-90, -14-developmenticon_height)});
                    
                     google.maps.event.addListener(devlopment_polygon, 'mouseover', function(ev){
                          
                         this.setOptions({fillColor: mapovis_mini_polygon_fillColor_mouseover,
                         strokeColor:mapovis_mini_polygon_strokeColor_mouseover,
                         strokeOpacity:mapovis_mini_polygon_strokeOpacity_mouseover,
                         strokeWeight:mapovis_mini_polygon_strokeWeight_mouseover});

                         var latLng = new google.maps.LatLng(
                                            dev_center.lat(),
                                            dev_center.lng());
                                            
                         var w = 220 + 7*developmentName.length;
                         var content = '<div style="position: absolute;left: -15px;top: -10px;height:60px;z-index:99999; display: table;vertical-align: middle;margin:0; overflow: hidden;-webkit-user-select: none;background-color: '+tooltip_background_colour+';color: '+tooltip_text_colour+'; font-family: '+$('body').css('font-family')+';font-size:'+tooltip_font_size+'px; text-align:center; padding: 10px; width: '+w+'px;border-radius: 4px;"><span style="vertical-align:middle;display: table-cell;">Open '+ developmentName +' Interactive Master Plan</span></div>';

                    
                         setTimeout(function(){
                           
                           devlopment_polygon.infoWindow.setPosition(latLng);   
                           devlopment_polygon.infoWindow.setContent(content); 
                           devlopment_polygon.infoWindow.open(map);

                         },100);       
                                         
                          
                    });

                    google.maps.event.addListener(devlopment_polygon, 'mouseout', function(ev){
                  
                      this.setOptions({fillColor: mapovis_mini_polygon_fillColor, fillOpacity: mapovis_mini_polygon_fillOpacity,
                      strokeColor:mapovis_mini_polygon_strokeColor,
                      strokeOpacity:mapovis_mini_polygon_strokeOpacity,
                      strokeWeight:mapovis_mini_polygon_strokeWeight});
                      
                      setTimeout(function(){
                        devlopment_polygon.infoWindow.close();
                      },100);
                    
                    });
                    /*
                      Once user clicks polygon, this shows full mapovis lightbox.
                    */
                    google.maps.event.addListener(devlopment_polygon, 'click', function(ev){
                             $('#fakebox').trigger('click');
                    });

                }); 
         
         /*
           From REST APIs, this ajax part shows radius markers such as 5km, 10km
         */  
         
            $.post('responseFromMapovis.php',{target:'radiusmarkers',developmentId: developmentId},function(data_response){  

                var radiusMarkers = JSON.parse(data_response);


                for(var i = 0; i < radiusMarkers.length; i++){

                    var radiusMarker_latlng = new google.maps.LatLng(parseFloat(radiusMarkers[i].latitude),parseFloat(radiusMarkers[i].longitude));

                    var myIcon = new google.maps.MarkerImage(radiusMarkers[i].icon, null,
                    new google.maps.Point(0, 0),
                    new google.maps.Point(21, 11),
                    new google.maps.Size(42,22));

                    var radiusMarker =  new google.maps.Marker({
                        position: radiusMarker_latlng,
                        map: map,
                        zIndex:4,
                        icon: myIcon,
                        title: radiusMarkers[i].radius_marker_id

                    });  


                    db_markers.push(radiusMarker);

                    radius_Markers.push(radiusMarker);

                    if(radius_circle == 1){          // regards on dev id, if radius_circle is 1, shows radius markers.

                        radiusMarker.setVisible(true);

                    }

                    else{
                        radiusMarker.setVisible(false); 
                    }   

                    google.maps.event.addListener(map, 'zoom_changed', getRadiusMarkersZoomLevelHandler(radiusMarker));

                }

            });   
        
         /*
            From REST APIs, this ajax part generates mini directions panel on map
         */
        
          $.post('responseFromMapovis.php',{target:'mapovisminidirections',developmentId: developmentId},function(data_response){  

                var directions = JSON.parse(data_response);
                
                for(var i = 0; i < directions.length; i++){
                    
                    var direction_place = "<li><span class='place_name'>"+directions[i].location_name+"</span><button id='directionplace_"+i+"' lat='"+directions[i].latitude+"' lng='"+directions[i].longitude+"' type='button' class='btn btn-default btn-sm right-place'>Show</button></li>";
                    
                    $(direction_place).appendTo('#direction_howfar_panel > ul');
                    
                    $('#directionplace_'+i).click(function(e){
                       if($(this).css('color') == 'rgb(255, 255, 255)'){
                            $('#controlUI_mapovismini').trigger('click');
                       }
                       else{
                            $('#controlUI_mapovismini').trigger('click');
                            var lat = $(this).attr('lat');
                            var lng = $(this).attr('lng');
                        
                            $(this).css({'background-color':'#6cacf6','color':'white','border-color':'#6cacf6'});
                            
                            direction_to_place_clicked = true;
                            
                            var dest = new google.maps.LatLng(parseFloat(lat),parseFloat(lng));
                            var entryexit = new google.maps.LatLng(parseFloat(entryexit_lat),parseFloat(entryexit_lng));
                            calcRoute(entryexit,dest);
                            
                       } 
              
                    });
                    
                }

            });
        
        function getSalesOfficeZoomLevelHandler(marker) {

            return function() {
                var zoomLevel = map.getZoom();
                if (zoomLevel <= parseInt(mapovis_mini_sales_office_marker_hide_at)) {
                    marker.setVisible(false);       //hide

                } if(zoomLevel >= parseInt(mapovis_mini_sales_office_marker_show_at)) {

                    marker.setVisible(true); // show

                }

            }

        }
        
          /*
        this module is for displaying sales offices on map  
        */

          $.post('responseFromMapovis.php',{target:'salesoffices',developmentId: developmentId},function(data_response){  

                $result = JSON.parse(data_response);

                for (var i = 0; i < $result.length; i++) {

                    var sales_office = $result[i];
                    
                    var latLng = new google.maps.LatLng(parseFloat($result[i].sales_office_latitude), parseFloat($result[i].sales_office_longitude));

                    if($result[i].sales_office_icon != ""){

                        var myIcon = new google.maps.MarkerImage(
                        $result[i].sales_office_icon,
                        null,
                        null,
                        null, 
                        new google.maps.Size(parseInt(sales_office_icon_width),parseInt(sales_office_icon_height)));

                        salesoffice_marker = new google.maps.Marker({
                            position: latLng,
                            map: map,
                            icon: myIcon,
                            zIndex: 100,
                            arrow_box: "Sales Office",  
                            sales_office_title: sales_office.sales_office_title, 
                            flat:true
                        });

                    }

                    else{

                        salesoffice_marker = new google.maps.Marker({
                            position: latLng,
                            map: map,
                            zIndex: 100,
                            title: "Sales Office",
                            arrow_box: "Sales Office",   
                            flat:true
                        });

                    }

                    if(WURFL.form_factor == "Desktop"){
                        google.maps.event.addListener(salesoffice_marker, 'mouseover', function() { 
                            tooltip = new Tooltip({map: map}, this);
                            tooltip.bindTo("text", this, "arrow_box");
                            tooltip.addTip();
                            $('.arrow_box').css({'width':'100px', 'background-color':tooltip_background_colour,'color':tooltip_text_colour,'font-family': $('body').css('font-family'),'fontSize':tooltip_font_size+"px"});
                            tooltip.getPos3(this.getPosition(),50);
                        });

                        google.maps.event.addListener(salesoffice_marker, 'mouseout', function() {
                            if(tooltip != null){
                               tooltip.removeTip();  
                            }
                            
                        });
                    }

                    google.maps.event.addListener(salesoffice_marker, 'click', function(event){
                          
                          var content = '<div><b>'+this.sales_office_title+'</b><br/><br/>Address: '+sales_office_address+'<br/>Telephone: '+local_sales_telephone_number+'<br/>Open: '+sales_office_opening_house+'</div>';   
                          infoWindow.setContent(content); 
                          infoWindow.open(map,this);
                          
                    });   

                    google.maps.event.addListener(map, 'zoom_changed', getSalesOfficeZoomLevelHandler(salesoffice_marker));
                     
                   
                }
            });
  

        /*
          REA given dev id, this styles map according to map stype options..
        */   
        
        $.post('responseFromMapovis.php',{target:'mapstyles',developmentId: developmentId},function(data_response){ 
             var result = JSON.parse(data_response);  
             if(result[0].custom_googlemaps_style != ''){
                map.setOptions({styles:JSON.parse(result[0].custom_googlemaps_style)});  
           }

            else{
                map.setOptions({styles:styles}); 
            }           
        });     
         
        setMarkers(map);

        dev_center = new google.maps.LatLng(developmentData.centre_latitude,developmentData.centre_longitude);
        /*
          If radius cicle is set regards on development id, this part draws radius circle on map.(its radius is 10km)
        */
        var circle_bound = new google.maps.Circle({center: dev_center, radius: 10000}).getBounds();
        overlay_circle = new USGSOverlay(circle_bound, developmentData.radius_circle_overlay_image, map);  
        
        setTimeout(function(){
           overlay_circle.hide(); 
          
        },2000);
        
        $('#clear_coords').click(function(e){
    
            $('#development_minipolygon_coordinates_panel ul').empty();
                  
            if(development_polygon_drawn != null){
                development_polygon_drawn.setMap(null);  
            }
                   
        });
        
        $('#polygon_coords_send').click(function(e){

              var coords = "";
              $('#development_minipolygon_coordinates_panel').find('li').each(function(i, li){
                    
                   $lat = $(this).find('.lat_field').val();
                   $lng = $(this).find('.lng_field').val();
                   
                   coords += $lat + ":" + $lng + " ";
                    
                });
                 
              $.post( 
                "http://stage3.mapovis.com.au/mapovis/save_cookie_stats.php",
                { development_id: developmentId, coords: coords, flag_coords: "dev_polygon_coords" },
                function(data,status) {
                   if(status == "success"){
                      $('#clear_coords').trigger('click');
                   
                   }
                  
              });        
                
        });

       $('body').on('click', '#controlUI_mapovismini', function () { 

            $('#amenities_selection_bar li > img').tooltip('hide');
            $('#amenities_selection_bar li > img').removeClass('selected');
            
            $('#direction_howfar_panel li').removeClass('selected');
            $('#direction_howfar_panel li').css('background-color','white');
            
            $('.right-place').css({'background-color':'#fff','border-color':'#ccc','color':'#333'});
            
            salesoffice_marker.setVisible(true);

          
            if(direction_to_place_clicked == false){
               $('#expand_panel_bt').trigger('click');  
            }
            
            if(direction_to_place_clicked == true){
                $('#expand_panel_bt').trigger('click');
                direction_to_place_clicked = false; 
            } // make direction to place button non-selected
            
            infoWindow.close();

            if(overlay_circle != null){
                overlay_circle.hide();
            }  
            
            if(midpointMarker != null){
                midpointMarker.setMap(null);
            }
            
            if(end_marker != null){
                end_marker.setMap(null);
            }
            
            if(polyline != null){
               polyline.setMap(null); 
            }          
            
            
            if(markerCluster != null){
                markerCluster.clearMarkers();
                markerCluster.repaint(); 
            }
            
            for(var num = 0; num < radius_Markers.length; num++){

                radius_Markers[num].setVisible(false);

            }
 
            if(feature_amenities == true){


                for(var n = 0; n < amenity_types; n++){

                    $('#amenities_selection_bar li > img#amenity_type_id_'+n).attr('src',$('#amenity_type_id_'+n).attr('unselected_image'));

                }


                for(var i = 0; i < external_markers.length; i++){

                    external_markers[i].setVisible(false);

                }

                for(var num = 0; num < radius_Markers.length; num++){

                    radius_Markers[num].setVisible(false);

                } 
                
                $('#expand_panel_bt').trigger('click');   

                feature_amenities = false; // make feature_amenities inactive

            }

            for(var i = 0; i < external_markers.length; i++){

                if(external_markers[i].ALWAYSON == "1"){

                    external_markers[i].setVisible(true);

                }   

            }
            
            directionsDisplay.setMap(null);
            
            directionsDisplay = new google.maps.DirectionsRenderer(); 
            directionsDisplay.setMap(map);
            
            map.setZoom(parseInt(defaultZoom));
            map.setCenter(dev_center);   

        });
        
        loadMiniMarker(developmentData); 
   
    }
    
    function createMarker(latlng, totalTime, totalDist) {
    
            var contentString = '<div style="font-weight:bold;font-size:14px;height:34px; padding-top:10px;"><i class="fa fa-car"></i>&#32;<span style="color:#5b9024">'+totalTime+'&#32;min</span>&#32;<span>('+totalDist+' km)</span></div>';
            midpointMarker = new google.maps.Marker({
                position: latlng,
                map: map,
                visible:false,
                zIndex: Math.round(latlng.lat()*-100000)<<5
            });
            
            infoWindow.setContent(contentString); 
            infoWindow.open(map,midpointMarker);
            
            return midpointMarker;
        }

        
         function computeTotalDistance(result) {
              totalDist = 0;
              totalTime = 0;
              var myroute = result.routes[0];
              for (i = 0; i < myroute.legs.length; i++) {
                totalDist += myroute.legs[i].distance.value;
                totalTime += myroute.legs[i].duration.value;      
              }
              putMarkerOnRoute(50);
           
             
          }

        
          function putMarkerOnRoute(percentage) {
            
              var distance = (percentage/100) * totalDist;
              totalTime = (totalTime/60).toFixed(0);
              totalDist = (totalDist / 1000).toFixed(1);
              midpointMarker = createMarker(polyline.GetPointAtDistance(distance),totalTime, totalDist);                                                     
              
          }

    
    /*
      get direction route between origin and destination
    */
    
    function calcRoute(origin,destination) {
        
      directionsDisplay.setMap(null);

      directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true}); 
      directionsDisplay.setMap(map);  
   
      var request = {
          origin: origin,
          destination: destination,
          // Note that Javascript allows us to access the constant
          // using square brackets and a string value as its
          // "property."
          travelMode: google.maps.TravelMode.DRIVING
      };
      directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            polyline.setPath([]);
            var bounds = new google.maps.LatLngBounds();
            startLocation = new Object();
            endLocation = new Object();
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var end =route.legs[0].end_location;
            
            end_marker = new google.maps.Marker({
              position: end, 
              map: map, 
              icon: 'images/finish-marker.png'
            });
           
            // For each route, display summary information.
            var path = response.routes[0].overview_path;
            var legs = response.routes[0].legs;
            for (i=0;i<legs.length;i++) {
                  var steps = legs[i].steps;
                  for (j=0;j<steps.length;j++) {
                    var nextSegment = steps[j].path;
                    for (k=0;k<nextSegment.length;k++) {
                      polyline.getPath().push(nextSegment[k]);
                      bounds.extend(nextSegment[k]);
                    }
                  }
            }

            polyline.setMap(map);

            computeTotalDistance(response);
            
        }
      });
    }
    
    if(WURFL.form_factor == "Desktop"){
         
        $('#mini_open_interactive_masterplan_button').click(function(e){
       
             e.preventDefault();
             $('#fakebox').trigger('click'); 
         
        });      
        
    }
    
    function setMarkers(map) {
     
        // Making marker visibility conditional on some level

        google.maps.event.addListener(map, 'zoom_changed', function() {

            var zoomLevel = map.getZoom();
         
        });
        

        function getExternalPlaceZoomLevelHandler(marker) {

            return function() {

                var zoomLevel = map.getZoom();

                if(feature_amenities){
                    marker.setMap(map);
                }

                else{
                    if (zoomLevel <= 12) {

                        if(marker.ALWAYSON == "1"){
                            marker.setMap(map);
                        }
                        else{
                            marker.setMap(null); // hide
                        }

                    } else {
                        marker.setMap(map); // show
                    }

                }

            }
            
        }

        
        /*
        When user clicks external amenity marker, this handler shows amenity details with customized dialog.
        */

        loadExternalPlaces(function(places) {

            for(var i = 0; i < places.length; i++) {
               
                var place = places[i];

                var myLatLng = new google.maps.LatLng(place.proposedExternalPlace_latitude, place.proposedExternalPlace_longitude);

                var placeIcon = new google.maps.MarkerImage(place.proposedExternalPlace_icon, null, null, null, new google.maps.Size(39,50));

                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    icon: placeIcon,
                    arrow_box: place.proposedExternalPlace_name,
                    zIndex: 4,
                    type_name:place.e_amenity_type_name,
                    type_id:place.external_amenity_type_id,
                    address:place.e_amenity_address,
                    name:place.proposedExternalPlace_name,
                    website:place.proposedExternalPlace_moreinfolink,
                    ALWAYSON : place.ALWAYSON,
                    cluster_icon: place.e_amenity_cluster_circle_url,
                    cluster_text_color: '#'+place.e_amenity_cluster_text_hex_colour,
                    cluster_icon_width:  place.e_amenity_cluster_circle_width,
                    cluster_icon_height: place.e_amenity_cluster_circle_height,
                    cluster_icon_fontSize:  place.e_amenity_cluster_text_size,
                    flat:true
                });

                if(show_highlighted_amenities == 1){
                   marker.setVisible(false);
                }

                else{
                    marker.setVisible(true);
                }

                if(place.ALWAYSON == "1"){
                    marker.setVisible(true);
                }

                db_markers.push(marker);

                external_markers.push(marker);

                if(WURFL.form_factor == "Desktop"){
                      google.maps.event.addListener(marker, 'mouseover', function() { 
                        tooltip = new Tooltip({map: map}, this);
                        tooltip.bindTo("text", this, "arrow_box");
                        tooltip.addTip();
                        var strWidth = $('.arrow_box').textWidth()+20;
                        $('.arrow_box').css({'width': strWidth+'px','background-color':tooltip_background_colour,'color':tooltip_text_colour,'font-family': $('body').css('font-family'),'fontSize':tooltip_font_size+"px"});
                        tooltip.getPos3(this.getPosition(),parseInt(strWidth)/2);
                    });
                  
                    google.maps.event.addListener(marker, 'mouseout', function() {
                        if(tooltip != null){
                           tooltip.removeTip(); 
                        }
                        
                    });
                }

                google.maps.event.addListener(map, 'zoom_changed', getExternalPlaceZoomLevelHandler(marker));
                

                google.maps.event.addListener(marker, 'click', function(ev){
                    
                     if(this.website != ""){
                         var content = '<div style="z-index:100;"><p style="font-weight:bold;">'+this.name+'</p><span>Address:</span><br/><p>'+this.address+'</p><span>Website:</span><br/><p><a href="'+this.website+'" target="_blank">'+this.website+'</a></p></div>'; 
                     }
                     
                     else{
                          var content = '<div><p style="font-weight:bold;">'+this.name+'</p><span>Address:</span><br/><p>'+this.address+'</p></div>';
                     } 
                    
                     infoWindow.setPosition(this.position);   
                     infoWindow.setContent(content); 
                     infoWindow.open(map,this);
                     
                });
               
            }

        });

    }
    
    // external amenities map zoom event handler

    function getExternalPlaceZoomLevelHandler(marker) {

        return function() {
            var zoomLevel = map.getZoom();

            if (zoomLevel <= 11) {

                marker.setMap(null); // hide
            } else {
                marker.setMap(map); // show
            } 

        }
        
    }

    /*
      From REST APIs, this loads external amenities on map
    */

    function loadExternalPlaces(callback) {

       var developmentId = developmentData.development_id;
       $.post('responseFromMapovis.php',{target:'externalamenities',developmentId: developmentId},function(data_response){     
                callback(JSON.parse(data_response));

       });

    }
    /*
      From REST APIs, this function plots mini markers on map
    */

    function loadMiniMarker(developmentData) {
            
            var latLng = new google.maps.LatLng(parseFloat(developmentData.centre_latitude), parseFloat(developmentData.centre_longitude));
            if(developmentData.developmenticon != ""){

                var myIcon = new google.maps.MarkerImage(developmentData.developmenticon, null, null, null, new google.maps.Size(parseInt(developmentData.developmenticon_width),parseInt(developmentData.developmenticon_height)));

                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    arrow_box: "Open " + developmentData.development_name + " Interactive Master Plan",
                    icon: myIcon
                });
            
            }

            else{

                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    arrow_box: "Click for Lots", 
                    flat:true
                 
                });

            }
            
            if(WURFL.form_factor == "Desktop"){

                google.maps.event.addListener(marker, 'mouseover', function() { 

                    tooltip = new Tooltip({map: map}, this);
                    tooltip.bindTo("text", this, "arrow_box");
                    tooltip.addTip();
                    var strWidth = $('.arrow_box').textWidth();
                    $('.arrow_box').css({'width': strWidth+'px','background-color':tooltip_background_colour,'color':tooltip_text_colour,'font-family': $('body').css('font-family'),'fontSize':tooltip_font_size+"px"});
                    tooltip.getPos3(this.getPosition(),parseInt(strWidth)/2);

                });

                google.maps.event.addListener(marker, 'mouseout', function() {
                    if(tooltip != null){
                      tooltip.removeTip(); 
                    }
                });

            }


            google.maps.event.addListener(marker, 'click', getMiniMarkerClickHandler(marker));

            google.maps.event.addListener(map, 'zoom_changed', getMiniMarkerZoomLevelHandler(marker)); 

    
    }

    function getMiniMarkerClickHandler(miniMarker) {
        return function() {

           $('#fakebox').trigger('click');      // this trigger full mapovis lightbox
             
        }
        
    }

    function getMiniMarkerZoomLevelHandler(marker) {

        return function() {
            var zoomLevel = map.getZoom();
        }

    }

    $('#custom_zoom_in').click(function(e){
        map.setZoom(map.getZoom()+1);
    });


    $('#custom_zoom_out').click(function(e){
        map.setZoom(map.getZoom()-1);
    });
    
    $('#directions_collapse_expand_panel_bt').click(function(e){
        
         if(direction_to_place_clicked == true){
            $('#controlUI_mapovismini').trigger('click');
         }  
         
         $("#direction_howfar_panel").animate({left:'-260px'}, 0);
         $(this).hide();
         $('#expand_panel_bt').show();
        
    });
    
    $('#expand_panel_bt').click(function(e){
         $("#direction_howfar_panel").animate({left:'10px'}, 0);
         $('#directions_collapse_expand_panel_bt').show();
         $(this).hide();
    });
    
    initialize();

}

},{"./USGSOverlay":1}]},{},[2]);
