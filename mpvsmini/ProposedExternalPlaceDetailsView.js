var font_name = "";
var dialog_start_hex = "";
var dialog_end_hex = "";
var text_hex_colour = "";
var dialog_close_button_hex = "";
var dialog_close_button_foreground_hex = "";
var show_get_directions_from_external_amenities = 0;
var get_directions_from_external_amenities_text_colour = '';

var development_name = '';

var get_directions_from_external_amenities_text_highlight_colour = '';


var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function ProposedExternalPlaceDetailsView(developmentId,fontfamily_name) {

    var self = this;

    if(fontfamily_name != ""){
        font_name = fontfamily_name;
    }

    $.post('responseFromMapovis.php',{target:'development',developmentId: developmentId},function(data_response){

            var developmentData = JSON.parse(data_response);
            dialog_end_hex =  developmentData.dialog_end_hex;
            dialog_start_hex =  developmentData.dialog_start_hex;
            text_hex_colour = '#' + developmentData.text_hex_colour;
            dialog_close_button_hex = '#' + developmentData.dialog_close_button_background_hex;
            dialog_close_button_foreground_hex = '#' + developmentData.dialog_close_button_foreground_hex;
            show_get_directions_from_external_amenities = developmentData.show_get_directions_from_external_amenities;
            get_directions_from_external_amenities_text_highlight_colour = '#'+developmentData.get_directions_from_external_amenities_text_highlight_colour;


            get_directions_from_external_amenities_text_colour = '#' + developmentData.get_directions_from_external_amenities_text_colour;
            development_name = developmentData.development_name;


        });

    if(WURFL.form_factor == "Smartphone"){

        this.element = $('#proposedexternalplace_detail_view_container_panel_noiframe');
        $(this.element)

        .hide()
        .load('../mpvs/templates/ProposedExternalPlaceDetails.html', function() {
            self.loaded = true;
            if (self.details) {
                self.setExternalPlaceDetails(self.details);

            }
        });

    }

    else{

        this.element = document.createElement('div');
        
        $(this.element)

        .hide()
        .load('../mpvs/templates/ProposedExternalPlaceDetails.html', function() {
            self.loaded = true;
            if (self.details) {

                self.setExternalPlaceDetails(self.details);

            }
        });

    }





}

function setPos(varWidth,varHeight){

    var win = $(window);
    var left = (win.width() - varWidth) / 2;


    if(isMobile.any() == "iPad"){

        if($(window).width() <= 768){

            pos = "center";

        }

        else{

            if(window.innerHeight < 650){ //the case that bookmark bar is on in horizontal mode

                pos = [left,69];  

            }

            else{

                pos = [left,82];   

            } 
        }


    }

    else{

        pos = "center";

    }

    return pos;

}


ProposedExternalPlaceDetailsView.prototype.setExternalPlaceDetails = function(placeDetails,visitor_ip,developmentId) {


    if(placeDetails.external_amenity_iframe_html != ""){
        
        var pos = setPos(700,500);

        var dlg_width = 700;

        if(isMobile.any() == "iPhone"){
            dlg_width = 300;
        }

        var dialog = $("<div class='iframe_site_div' style='width:700px;height:500px;'></div>").append($(placeDetails.external_amenity_iframe_html))
        .dialog({
            width: dlg_width,
            height: 500,
            dialogClass: 'externalAmenity_dlg',
            position: pos,
            open: function(event, ui) {

                $('.stage_container').hide();
                $('#phonenum_container').hide();

                $('.iframe_site_div').css('padding','0');


                var milliseconds = new Date().getTime();  // event timestamp

                $.post( 
                "../mapovis/save_statistics_info.php",
                { browser: bowser.name+" "+bowser.version, externalAmenity_id: placeDetails.proposedExternalPlace_id, timestamp: milliseconds, ip_address: visitor_ip, developmentId: developmentId, flag: "statistics_externalAmenitiesViews"  },
                function(data) {

                }
                );


                if(placeDetails.external_amenity_iframe_scrollbar == 0){

                    $('.ui-dialog .ui-dialog-content').css('overflow','hidden'); 

                }

                $('.ui-dialog .ui-dialog-titlebar-close').css('background-color',dialog_close_button_hex);
                $('.ui-dialog .ui-dialog-titlebar-close').blur();

                $('.ui-dialog .ui-dialog-titlebar').css('background', 'linear-gradient(to bottom, #' + dialog_start_hex + ' 0%, #' + dialog_end_hex + ' 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');



                if((bowser.msie && bowser.version < 10) || (bowser.firefox && bowser.version < 16)){


                    $('.ui-dialog .ui-dialog-titlebar').css('background', '#'+dialog_start_hex);


                }

                else{

                    $('.ui-dialog .ui-dialog-titlebar').css('filter', 'progid:DXImageTransform.Microsoft.gradient(startColorstr = #' + dialog_start_hex + ',endColorstr = #' + dialog_end_hex + ')');


                }

                if(show_get_directions_from_external_amenities == 1){

                    $('.externalAmenity_dlg .ui-dialog-title').html("<i class='fa fa-car' style='font-size:22px;margin-right:3px;'></i>How far from "+development_name+"?");
                    $('.externalAmenity_dlg .ui-dialog-title').css('color',get_directions_from_external_amenities_text_colour);

                    var externalAmenityPos = placeDetails.proposedExternalPlace_latitude + "," + placeDetails.proposedExternalPlace_longitude;



                    $('.externalAmenity_dlg .ui-dialog-title').click(function(e){

                        $('#external_amenity_coordinate').val(externalAmenityPos); 
                        $('#external_amenity_address').val(placeDetails.e_amenity_address);

                        $('#direction_address').val(placeDetails.proposedExternalPlace_name);
                        $('#getdirectionsbutton > a').trigger('click');

                        $('.ui-dialog-titlebar-close').trigger('click');
                        $('.feature_panel').css('right','-1000px');
                        $('#directions_container_panel').css({'right':'0px','top':'50px','display':'block'});
                        $('#direction_panel_show_hide').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');

                        $('.close-sidebar-left').trigger('click');


                        $('#reset > span').text("Back");
                        $('#get_direction').trigger('click'); 


                    }); 

                    $('.externalAmenity_dlg .ui-dialog-title').hover(
                    function() {

                        $(this).css('color',get_directions_from_external_amenities_text_highlight_colour);
                    }, function() {

                        $( this ).css('color',get_directions_from_external_amenities_text_colour);
                    }
                    );

                }





            },

            close: function(event, ui){

                $('.stage_container').show();
                $('#phonenum_container').show();

            }

        });


        dialog.show();

        return;

    } 

    var place_imagesdiv = $(this.element).find("#place_images #place_slider");
    place_imagesdiv.html("");  



    if (this.loaded) {
        $(this.element)
        .find("#place_title")
        .html(placeDetails.proposedExternalPlace_name);


        var image_mainpath = "../mpvs/images/external_amenities/";

        var arr = [placeDetails.proposedExternalPlace_picture1, placeDetails.proposedExternalPlace_picture2, placeDetails.proposedExternalPlace_picture3, placeDetails.proposedExternalPlace_picture4, placeDetails.proposedExternalPlace_picture5]; 
        var newArray = [];

        for (var n = 0; n < arr.length; n++) {
            if (arr[n]) {

                var li = "<li><img src='"+image_mainpath+arr[n]+"'></li>";

                $(li).appendTo($('#place_images #place_slider'));

            }
        }


        $("#place_slider").responsiveSlides({
            maxwidth: 350,
            speed: 800
        });

        $(this.element)
        .find("#description")
        .html(placeDetails.proposedExternalPlace_description);

        if(placeDetails.proposedExternalPlace_moreinfolink != ""){
            $(this.element)
            .find("#more_info").css("display","block");

            $(this.element)
            .find("#more_info")
            .attr("href",placeDetails.proposedExternalPlace_moreinfolink);
        }

        else{
            $(this.element)
            .find("#more_info").css("display","none");
        }


    } else {
        this.details = placeDetails;
    }


    if(WURFL.form_factor == "Smartphone"){

        $(this.element).show();

    }

    else{


        pos = setPos(700,500);

        $(this.element)
        .show()


        // TODO: Jorge, dialog( "open" ); cannot accept any arguments. Therefore I cannot specify the width, height etc and it gets automatically set. See http://api.jqueryui.com/dialog/#method-open. 
        // I therefore changed to using http://api.jqueryui.com/dialog/#event-open. Please check this and see if its OKAY. 
        .dialog({
            width: 700,
            height: 500,
            dialogClass: 'externalAmenity_dlg',
            position: pos,
            open: function(event, ui) {


                $('.stage_container').hide();

                $(this).scrollTop(0);

                $('#phonenum_container').hide();


                var milliseconds = new Date().getTime();  // event timestamp

                $.post( 
                "../mapovis/save_statistics_info.php",
                { browser: bowser.name+" "+bowser.version, externalAmenity_id: placeDetails.proposedExternalPlace_id, timestamp: milliseconds, ip_address: visitor_ip, developmentId: developmentId, flag: "statistics_externalAmenitiesViews"  },
                function(data) {

                }
                );



                $('.ui-dialog .ui-dialog-titlebar-close').blur();

                if(font_name != ""){
                    $('*').not('.fa, .pocp_button_right > i').css("font-family", font_name+", serif");
                }

                $('.ui-dialog .ui-dialog-titlebar').css('background', 'linear-gradient(to bottom, #' + dialog_start_hex + ' 0%, #' + dialog_end_hex + ' 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');


                $('.ui-dialog .ui-dialog-titlebar').css('filter', 'progid:DXImageTransform.Microsoft.gradient(startColorstr = #' + dialog_start_hex + ',endColorstr = #' + dialog_end_hex + ')');


            },


            close: function(event, ui){

                $('.stage_container').show();

                if(parseInt($('.pocp_right').css('right')) == 0){
                   $('#phonenum_container').hide();  
                }
                else{
                  $('#phonenum_container').show();   
                }

                $('.ui-dialog .ui-dialog-titlebar-close').blur();

                if(font_name != ""){
                    $('*').not('.fa, .pocp_button_right > i').css("font-family", font_name+", serif");
                }

                $('.ui-dialog .ui-dialog-titlebar').css('background', 'linear-gradient(to bottom, #' + dialog_start_hex + ' 0%, #' + dialog_end_hex + ' 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');


                $('.ui-dialog .ui-dialog-titlebar').css('filter', 'progid:DXImageTransform.Microsoft.gradient(startColorstr = #' + dialog_start_hex + ',endColorstr = #' + dialog_end_hex + ')');

                if(show_get_directions_from_external_amenities == 1){

                    $('.externalAmenity_dlg .ui-dialog-title').html("<i class='fa fa-car' style='font-size:22px;margin-right:3px;'></i>How far from "+development_name+"?");
                    $('.externalAmenity_dlg .ui-dialog-title').css('color',get_directions_from_external_amenities_text_colour);

                    var externalAmenityPos = placeDetails.proposedExternalPlace_latitude + "," + placeDetails.proposedExternalPlace_longitude;



                    $('.externalAmenity_dlg .ui-dialog-title').click(function(e){


                        $('#external_amenity_coordinate').val(externalAmenityPos); 

                        $('#external_amenity_address').val(placeDetails.e_amenity_address);

                        $('#direction_address').val(placeDetails.proposedExternalPlace_name);
                        $('#getdirectionsbutton > a').trigger('click');

                    }); 


                    $('.externalAmenity_dlg .ui-dialog-title').hover(

                    function() {

                        $(this).css('color',get_directions_from_external_amenities_text_highlight_colour);
                    }, function() {

                        $( this ).css('color',get_directions_from_external_amenities_text_colour);
                    }
                    );

                }



            }
        });

    }

}


module.exports = ProposedExternalPlaceDetailsView;
