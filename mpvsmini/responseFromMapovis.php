<?php
  
    set_time_limit(0);
    header('Access-Control-Allow-Origin: *'); 
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

    function multiRequest($data, $options = array()) {
 
      // array of curl handles
      $curly = array();
      // data to be returned
      $result = array();
     
      // multi handle
      $mh = curl_multi_init();
     
      // loop through $data and create curl handles
      // then add them to the multi-handle
      foreach ($data as $id => $d) {
     
        $curly[$id] = curl_init();
     
        $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
        curl_setopt($curly[$id], CURLOPT_URL,            $url);
        curl_setopt($curly[$id], CURLOPT_HEADER,         0);
        curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curly[$id], CURLOPT_SSL_VERIFYPEER, false);
     
        // post?
        if (is_array($d)) {
          if (!empty($d['post'])) {
            curl_setopt($curly[$id], CURLOPT_POST,       1);
            curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
          }
        }
     
        // extra options?
        if (!empty($options)) {
          curl_setopt_array($curly[$id], $options);
        }
     
        curl_multi_add_handle($mh, $curly[$id]);
      }
     
      // execute the handles
      $running = null;
      do {
        curl_multi_exec($mh, $running);
      } while($running > 0);
     
     
      // get content and remove handles
      foreach($curly as $id => $c) {
        $result[$id] = curl_multi_getcontent($c);
        curl_multi_remove_handle($mh, $c);
      }
     
      // all done
      curl_multi_close($mh);
     
      return $result;

    }
    
    if(isset($_POST['developmentId'])){
        $developmentId = $_POST['developmentId']; 
    }
    
    if(isset($_POST['target']) && $_POST['target'] == 'development'){
        
        $url = array(
           'http://stage3.mapovis.com.au/admin/getdatainjson/development/'.$developmentId
        );
        
        $r = multiRequest($url);
        echo $r[0];
        exit;
    }
    
    if(isset($_POST['target']) && $_POST['target'] == 'amenitycategories'){
        $url = array(
           'http://stage3.mapovis.com.au/admin/getdatainjson/amenitycategories/'.$developmentId
        );
        
        $r = multiRequest($url);
        echo $r[0];
        exit;
    }
    
    if(isset($_POST['target']) && $_POST['target'] == 'mapovisminipolygon'){
        $url = array(
           'http://stage3.mapovis.com.au/admin/getdatainjson/mapovisminipolygon/'.$developmentId
        );
        
        $r = multiRequest($url);
        echo $r[0];
        exit;
    }
    
    if(isset($_POST['target']) && $_POST['target'] == 'mapstyles'){
        $url = array(
           'http://stage3.mapovis.com.au/admin/getdatainjson/getmapstyles/'.$developmentId
        );
        
        $r = multiRequest($url);
        echo $r[0];
        exit;
    }
    
    if(isset($_POST['target']) && $_POST['target'] == 'radiusmarkers'){
        $url = array(
           'http://stage3.mapovis.com.au/admin/getdatainjson/radiusmarkers/'.$developmentId
        );
        
        $r = multiRequest($url);
        echo $r[0];
        exit;
    }
    
    if(isset($_POST['target']) && $_POST['target'] == 'mapovisminidirections'){
        $url = array(
           'http://stage3.mapovis.com.au/admin/getdatainjson/mapovisminidirections/'.$developmentId
        );
        
        $r = multiRequest($url);
        echo $r[0];
        exit;
    }
    
    if(isset($_POST['target']) && $_POST['target'] == 'salesoffices'){
        $url = array(
           'http://stage3.mapovis.com.au/admin/getdatainjson/salesoffices/'.$developmentId
        );
        
        $r = multiRequest($url);
        echo $r[0];
        exit;
    }
    
    if(isset($_POST['target']) && $_POST['target'] == 'externalamenities'){
        $url = array(
           'http://stage3.mapovis.com.au/admin/getdatainjson/externalamenities/'.$developmentId
        );
        
        $r = multiRequest($url);
        echo $r[0];
        exit;
    }

    
?>
