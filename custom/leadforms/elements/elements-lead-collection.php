<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ElementsBS</title>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" media="all" href="http://www.elements.com.au/wp-content/themes/elementsbscoredevelopment/css/bootstrap.css" type="text/css">
	<link rel="stylesheet" media="all" href="http://www.elements.com.au/wp-content/themes/elementsbscoredevelopment/style.css" type="text/css">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <script type="text/javascript" src="http://www.tfaforms.com/wForms/3.7/js/wforms.js"></script>
    <script type="text/javascript">
        wFORMS.behaviors.prefill.skip = false;
    </script>
    <script type="text/javascript" src="http://www.tfaforms.com/wForms/3.7/js/localization-en_GB.js"></script>

<style>
#tfa_14 { display: none; } 
#tfa_19 { display: none; }
#tfa_20 { display: none; }
</style>

</head>
<body class="popup-form" style="margin-top:-10px;">
	<div class="container">
					<div class="row">
						<div class="col-lg-8 col-xs-12 col-lg-push-2">
							<main id="content" role="main">
                                <section class="content-section">
                                <h2 style="color:#6a2d83;">Enquire</h2>
                                    <form method="post" action="http://www.tfaforms.com/responses/processor" id="tfa_0" class="validation" target="_blank">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <select class="form-control required-select default-value" name="tfa_1"  id="tfa_1" >
                                                        <option class="hideme" value="">Title*</option>
                                                        <option value ="tfa_9" id="tfa_9" class="">Mr</option>
                                                        <option value ="tfa_10" id="tfa_10" class="">Mrs</option>
                                                        <option value ="tfa_11" id="tfa_11" class="">Ms</option>
                                                        <option value ="tfa_12" id="tfa_12" class="">Miss</option>
                                                    </select>
                                                    <input class="form-control required" type="text" id="tfa_2" name="tfa_2" placeholder="First Name*">
                                                    <input class="form-control required" type="text" id="tfa_3" name="tfa_3" placeholder="Last Name*">
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <input class="form-control validate-email required" type="email" id="tfa_4" name="tfa_4" placeholder="Email*">
                                                    <input class="form-control required" type="tel" id="tfa_5" name="tfa_5" placeholder="Phone*">
                                                    <input class="form-control" type="text" id="tfa_13" name="tfa_13" placeholder="Postcode">
                                                </div>


                                                 <div class="col-lg-6 col-md-6 col-xs-12">
													<select id="tfa_14" name="tfa_14" class="required form-control required-select default-value drop-blue-style">
													<option value="">How did you hear about us?</option>
													<option value="tfa_15" id="tfa_15" class="">Agent referral </option>
													<option value="tfa_16" id="tfa_16" class="">Builder referral</option>
													<option value="tfa_17" id="tfa_17" class="">Family Fun Day</option>
													<option value="tfa_18" id="tfa_18" class="">Family/Friend referral</option>
													<option value="tfa_19" id="tfa_19" class="">Google</option>
													<option value="tfa_20" id="tfa_20" class="">Herald Sun</option>
													<option value="tfa_21" id="tfa_21" class="">Realestate.com.au</option>
													<option value="tfa_22" id="tfa_22" class="">Signage</option>
													<option value="tfa_23" id="tfa_23" class="">Radio</option>
													</select>
												</div>

                                                <div class="col-xs-12"><button data-loading-text="Sending..." type="submit" data-ga-action="submit" data-ga-category="Contact" data-ga-title="form submit" class="btn btn-primary" id="submit">Submit</button></div>
                                            	</div>

                                            <!-- Hidden fields for Lot ID, House Name and Builder Name.  -->
                                            <!-- <input class="" id="tfa_14" name="tfa_14" type="text" value="">
											<input class="" id="tfa_19" name="tfa_19" type="text" value="">			
											<input class="" id="tfa_20" name="tfa_20" type="text" value=""> -->



                                            <input type="hidden" value="346567" name="tfa_dbFormId" id="tfa_dbFormId">
                                            <input type="hidden" value="" name="tfa_dbResponseId" id="tfa_dbResponseId">
                                            <input type="hidden" value="e340a157aaa8f2ccdb59ef863cc0c352" name="tfa_dbControl" id="tfa_dbControl">
                                            <input type="hidden" value="1" name="tfa_dbVersionId" id="tfa_dbVersionId">
                                            <input type="hidden" value="" name="tfa_switchedoff" id="tfa_switchedoff">
                                            </form>
								</section>
							</main>
						</div>
					</div>
				</div>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript">window.jQuery || document.write('<script src="http://www.elements.com.au//wp-content/themes/elementsbscoredevelopment/js/jquery-1.11.1.min.js"><\/script>')</script>
	<script type="text/javascript" src="http://www.elements.com.au//wp-content/themes/elementsbscoredevelopment/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://www.elements.com.au//wp-content/themes/elementsbscoredevelopment/js/jquery.main.js"></script>
	<script type="text/javascript">
		if (navigator.userAgent.match(/IEMobile\/10\.0/) || navigator.userAgent.match(/MSIE 10.*Touch/)) {
			var msViewportStyle = document.createElement('style')
			msViewportStyle.appendChild(
				document.createTextNode(
					'@-ms-viewport{width:auto !important}'
				)
			)
			document.querySelector('head').appendChild(msViewportStyle)
		}
	</script>
</body>
</html>