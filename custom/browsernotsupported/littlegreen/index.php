<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Little Green</title>

    <!-- Bootstrap -->
    	<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

		<!-- Custom Styling -->
		<link rel="stylesheet" href="styles.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container-fluid">
  
  <div class="row header">
  		<div id="logo">
    		<img src="logo.png">
    	</div>
  </div>

  <div class="row alert-message">

	  	<div class="alert-container">
	  		<div class="alert alert-warning text-center" role="alert" style="padding-bottom:30px;">
	  		<h1>YOUR BROWSER IS VERY OLD</h1>
	  		<span class="alert-message">To view the interactive master plan we recommend using a modern internet browser.</span>
	  		</div>
	  	<div>
  </div>


<div class="row">
    <div class="col-md-4">
        <a href="javascript:history.back()"><button type="button" class="btn btn-default">
        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
        <div class="button-text">Go Back</div>
        </button></a>
    </div>

    <div class="col-md-4">
        <a href="http://www.browsehappy.com/" target="_blank"><button type="button" class="btn btn-default">
        <span class="glyphicon glyphicon-download" aria-hidden="true"></span>
        <div class="button-text">Upgrade Your Browser</div>
        </button></a>
    </div>

    <div class="col-md-4">
         <a href="http://app.mapovis.com.au/mapovis/development.php?developmentId=72&browser=proceed"><button type="button" class="btn btn-default">
        <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
        <div class="button-text">
          Proceed with Current Browser
          </br> 
          <span style="font-size:12px;">(not recommended)</span>
        </div>
        </button></a>
    </div>
</div>


 




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>


	<!-- TypeKit -->
    <script src="//use.typekit.net/owq7jui.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>

  </body>
</html>

