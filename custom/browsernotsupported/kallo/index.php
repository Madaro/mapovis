<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta charset="utf-8">

    <title>Kallo - Your Browser is TOO OLD</title>
    <link href="//cloud.typography.com/6734672/728384/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="http://www.kallo.com.au/wp-content/themes/kallo/css/bootstrap.min.css" media="all" rel="stylesheet">
    <link href="http://www.kallo.com.au/wp-content/themes/kallo/style.css" media="all" rel="stylesheet">
    <script src='http://www.kallo.com.au/wp-content/themes/kallo/js/comment-reply.js?ver=3.9.1' type='text/javascript'></script>
    <script src='http://www.kallo.com.au/wp-includes/js/jquery/jquery.js?ver=1.11.0' type='text/javascript'></script>
    <script src='http://www.kallo.com.au/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1' type='text/javascript'></script>
    <link href="http://www.kallo.com.au/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml">
    <link href="http://www.kallo.com.au/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml">
    <link href='http://www.kallo.com.au/land-for-sale' rel='prev' title='Land for sale'>
    <link href='http://www.kallo.com.au/privacy-policy' rel='next' title='Privacy Policy'>
    <link href='http://www.kallo.com.au/house-land' rel='canonical'>
    <link href='http://www.kallo.com.au/?p=15' rel='shortlink'>
    <script src="http://www.kallo.com.au/wp-content/themes/kallo/js/jquery.main.js" type="text/javascript"></script>
    <script src="//use.typekit.net/gzh4eil.js" type="text/javascript"></script>
    <script type="text/javascript">
try{Typekit.load();}catch(e){}
    </script><!--[if lt IE 9]>
        <link media="screen" rel="stylesheet" href="http://www.kallo.com.au/wp-content/themes/kallo/css/ie.css">
        <script src="http://www.kallo.com.au/wp-content/themes/kallo/js/ie.js"></script>
    <![endif]-->

    <style>

    .option-buttons
    {
    padding-top: 20px;
    }

    .more-link
    {
    width: 450px;    
    }

    .not-perfect
    {
    font-size: 0.8em;
    color: #FFFFFF;    
    }

    </style>
</head>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48544234-4', 'auto');
  ga('send', 'pageview');

</script>

<body style="background: #000000;">
    <div id="wrapper">
        <header id="header">
            <div class="container row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-5">
                    <h1 class="logo"><a href="http://www.kallo.com.au">Kallo</a></h1>
                </div>


            </div>
        </header>

        <div id="main" style="background: #000000;">
            <div class="top-block">
                <section class="container">
                    <div class="row col-lg-12 col-md-12 block-content">
                        <center>
                        <h2>Your Browser is Very Old</h2>
                        <h3>To view the interactive master plan we recommend using a modern internet browser.</h3>
                        </center>
                    </div>
                </section>
            </div>

            <div class="container row col-lg-12 main-block row container row download-container">

                <div class="row options">
                    <div class="option-buttons">
                        <center><a class="more-link" href="http://browsehappy.com/" target="_blank">UPGRADE YOUR BROWSER</a></center>
                    </div>

                    <div class="option-buttons">
                        <center><a class="more-link" href="http://app.mapovis.com.au/mapovis/development.php?developmentId=14&browser=proceed">OPEN WITH CURRENT BROWSER
                        <span class="not-perfect"></br>The system may not work perfectly</span>
                        </a>
                        
                        </center>
                    </div>

                    <div class="option-buttons">
                        <center><a class="more-link" href="javascript:history.back()">BACK</a></center>
                    </div>
                </div>
            </div>

        </div>

 
    </div>
</body>
</html>