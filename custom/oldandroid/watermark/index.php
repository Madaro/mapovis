<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Watermark</title>

    <!-- Bootstrap -->
    	<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

		<!-- Custom Styling -->
		<link rel="stylesheet" href="styles.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container-fluid">
  
  <div class="row header">
  		<div id="logo">
    		<img src="logo.png">
    	</div>
  </div>

  <div class="row alert-message">

	  	<div class="alert-container">
	  		<div class="alert alert-warning text-center" role="alert" style="padding-bottom:30px;">
	  		<h1>SORRY, YOUR PHONE IS TOO OLD.</h1>
	  		<span class="alert-message">Your phone is running a very old version of the Android operating system. 
        <br/><br/>
        To use the interactive master plan you need to be using at least Android version 4.0. 
        <br/><br/>Most people have updated their phones software and we strongly suggest you do too!
        <br/><br/>
        <a href="http://www.pcadvisor.co.uk/how-to/google-android/3347925/how-update-android-smartphone-tablet/" target="_blank">Click here for instructions.</a></span>
	  		</div>
	  	<div>
  </div>

<div class="row options">
	<a href="http://www.watermarkgeelong.com.au"><button type="button" class="btn btn-default">Go Back</button></a>
</div>

<div class="row options">
	<a href="http://app.mapovis.com.au/admin/apipdf/devpdffilewithlots/75" target="_blank"><button type="button" class="btn btn-default">Download PDF Master Plan</button></a>
</div>


</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>


	<!-- TypeKit -->
    <script src="//use.typekit.net/atg3rgi.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>

  </body>
</html>

